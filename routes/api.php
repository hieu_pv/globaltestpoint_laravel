<?php

$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api) {
    /**
     * Admin API
     */
    $api->group(['prefix' => 'admin', 'middleware' => 'web'], function ($api) {
        $api->resource('user-status', 'App\Http\Controllers\API\Admin\UserStatusController');
        $api->put('users/{id}/change-status/employer', 'App\Http\Controllers\API\Admin\UserController@employerChangeStatus');
        $api->put('users/{id}/images', 'App\Http\Controllers\API\Admin\UserController@images');
        $api->put('users/{id}/employer', 'App\Http\Controllers\API\Admin\UserController@updateEmployerProfile');
        $api->post('users/employer', 'App\Http\Controllers\API\Admin\UserController@employer');
        $api->delete('users/{id}/company', 'App\Http\Controllers\API\Admin\UserController@destroyCompany');
        $api->get('users/employers', 'App\Http\Controllers\API\Admin\UserController@employers');
        $api->resource('users', 'App\Http\Controllers\API\Admin\UserController');

        $api->put('industries/change-status-all-items', 'App\Http\Controllers\API\Admin\IndustryController@changeStatusAllItems');
        $api->get('industries/all', 'App\Http\Controllers\API\Admin\IndustryController@getAll');
        $api->resource('industries', 'App\Http\Controllers\API\Admin\IndustryController');

        $api->put('job_titles/change-status-all-items', 'App\Http\Controllers\API\Admin\JobTitleController@changeStatusAllItems');
        $api->get('job_titles/all', 'App\Http\Controllers\API\Admin\JobTitleController@getAll');
        $api->resource('job_titles', 'App\Http\Controllers\API\Admin\JobTitleController');

        $api->put('advertisements/change-status-all-items', 'App\Http\Controllers\API\Admin\AdvertisementController@changeStatusAllItems');
        $api->put('advertisements/{id}/image', 'App\Http\Controllers\API\Admin\AdvertisementController@updateImage');
        $api->resource('advertisements', 'App\Http\Controllers\API\Admin\AdvertisementController');;

        $api->put('nationalities/change-status-all-items', 'App\Http\Controllers\API\Admin\NationalityController@changeStatusAllItems');
        $api->get('nationalities/all', 'App\Http\Controllers\API\Admin\NationalityController@getAll');
        $api->resource('nationalities', 'App\Http\Controllers\API\Admin\NationalityController');

        $api->get('countries/all', 'App\Http\Controllers\API\Admin\CountryController@getAll');
        $api->get('countries/{id}/states', 'App\Http\Controllers\API\Admin\CountryController@getStates');
        
        $api->get('states/{id}/cities', 'App\Http\Controllers\API\Admin\StateController@getCities');
        $api->get('states/all', 'App\Http\Controllers\API\Admin\StateController@getAll');
        $api->put('states/change-status-all-items', 'App\Http\Controllers\API\Admin\StateController@changeStatusAllItems');
        $api->resource('states', 'App\Http\Controllers\API\Admin\StateController');

        $api->put('cities/change-status-all-items', 'App\Http\Controllers\API\Admin\CityController@changeStatusAllItems');
        $api->resource('cities', 'App\Http\Controllers\API\Admin\CityController');

        $api->put('experiences/change-status-all-items', 'App\Http\Controllers\API\Admin\ExperienceController@changeStatusAllItems');
        $api->get('experiences/all', 'App\Http\Controllers\API\Admin\ExperienceController@getAll');
        $api->resource('experiences', 'App\Http\Controllers\API\Admin\ExperienceController');

        $api->put('qualifications/change-status-all-items', 'App\Http\Controllers\API\Admin\QualificationController@changeStatusAllItems');
        $api->get('qualifications/all', 'App\Http\Controllers\API\Admin\QualificationController@getAll');
        $api->resource('qualifications', 'App\Http\Controllers\API\Admin\QualificationController');

        $api->put('tags/change-status-all-items', 'App\Http\Controllers\API\Admin\TagController@changeStatusAllItems');
        $api->get('tags/all', 'App\Http\Controllers\API\Admin\TagController@getAll');
        $api->resource('tags', 'App\Http\Controllers\API\Admin\TagController');

        // Job API
        $api->get('jobs/slug/{slug}', 'App\Http\Controllers\API\Admin\JobController@getJobBySlug');
        $api->put('jobs/change-status-all-items', 'App\Http\Controllers\API\Admin\JobController@changeStatusAllItems');
        $api->resource('jobs', 'App\Http\Controllers\API\Admin\JobController');
    });

    /**
     * Frontend API
     */
    $api->get('roles', 'App\Http\Controllers\API\RoleController@index');
    $api->get('exams', 'App\Http\Controllers\API\ExamController@index');
    $api->get('exam/{id}', 'App\Http\Controllers\API\ExamController@show');
    $api->get('question/{id}', 'App\Http\Controllers\API\QuestionController@show');
    $api->post('checkEmailAdmin', 'App\Http\Controllers\API\CheckEmailController@checkEmailAdmin');
    $api->resource('partners', 'App\Http\Controllers\API\PartnerController');
    $api->put('partners/{id}/active', 'App\Http\Controllers\API\PartnerController@active');
    $api->resource('users', 'App\Http\Controllers\API\UserController');

    $api->post('file/upload', 'App\Http\Controllers\API\FilesController@upload');
    $api->resource('countries', 'App\Http\Controllers\API\CountryController');

    $api->resource('states', 'App\Http\Controllers\API\StateController');
    $api->resource('cities', 'App\Http\Controllers\API\CityController');
    $api->resource('industries', 'App\Http\Controllers\API\IndustryController');
    $api->resource('job_titles', 'App\Http\Controllers\API\JobTitleController');
    $api->resource('qualifications', 'App\Http\Controllers\API\QualificationController');
    $api->resource('experiences', 'App\Http\Controllers\API\ExperiencesController');

    $api->get('advertisements/all', 'App\Http\Controllers\API\AdvertisementController@getAll');
    $api->resource('advertisements', 'App\Http\Controllers\API\AdvertisementController');

    $api->group(['middleware' => 'web'], function ($api) {
        $api->post('has-logged-in', 'App\Http\Controllers\API\UserController@hasLoggedIn');
    });

    $api->get('jobs/all', 'App\Http\Controllers\API\JobController@getAll');
    $api->get('jobs/slug/{slug}', 'App\Http\Controllers\API\JobController@getJobBySlug');
    $api->get('jobs/{id}/similar-jobs', 'App\Http\Controllers\API\JobController@getSimilarJobs');
    $api->resource('jobs', 'App\Http\Controllers\API\JobController');

    $api->get('options/link-fanpages', 'App\Http\Controllers\API\OptionController@getLinkFanpages');
});
