<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */
Route::get('testing', 'TestController@index');

Route::get('check', 'PaymentsController@test');
Route::get('test-email', function () {
    Mail::send('mails.test1', ['user' => 'garung'], function ($message) {
        $message->from('vicoders.daily.report@gmail.com', 'Globaltestpoint');
        $message->to('canmotcaiten1993@gmail.com')->subject('Registration successful ! globaltestpoint');
    });
    // Mail::send('mails.userRegister', ['user' => $register], function ($message) use ($register) {
    //         $message->from('vicoders.daily.report@gmail.com', 'Globaltestpoint');
    //         $message->to($register->email)->subject('Registration successful ! globaltestpoint');
    //         $register['image'] = $message->embed(asset($register->image));
    //     });
});
Route::group(['middleware' => 'maintenance'], function () {
    Route::get('/', 'HomeController@index');
    Route::get('/verified/{email}/{cc_token}', 'UserController@emailVerified');
    Route::get('/forum', 'WantdoneController@forum');
    Route::get('/install/index.php', 'WantdoneController@installForum');

    Route::post('file/upload', 'FileController@upload');

    Route::get('/contact-us', 'OptionController@feedback');
    Route::post('/contact-us', 'OptionController@getfeedback');

    Route::get('/term-and-policy', 'OptionController@termpolicy');

    Route::group(['middleware' => 'web'], function () {
        Route::post('checkLogin', 'Frontend\FrontendController@checkLogin');
    });
    Route::group(['prefix' => 'testyourself', ['middleware' => ['web']]], function () {
        Route::get('/', ['as' => 'testyourself.index', 'uses' => 'TestyourselfController@index']);

        Route::any('/blank-page/job-vacancies/{path?}', function () {
            return view('app.spa');
        })->where("path", ".+");

        Route::get('/blank-page/{slug}', ['as' => 'testyourself.blankpage.show', 'uses' => 'BlankpageController@show']);

        Route::get('exam/{slug}', ['as' => 'testyourself.getExam', 'uses' => 'examListController@getExam']);

        Route::post('exam/{slug}', ['as' => 'testyourself.postExam', 'uses' => 'examListController@postExam']);

        Route::post('checkBuyExam', 'examListController@checkBuyExam');

        Route::post('saveExam', 'examListController@saveExam');
        Route::get('checkExam', 'examListController@checkExam');

        Route::post('userChoose', 'examListController@userChoose');
        Route::get('checkUserChoose', 'examListController@checkUserChoose');

        Route::get('exam-list/{slug}', ['as' => 'testyourself.exam.examList', 'uses' => 'examListController@examList']);

        Route::get('exam-start/{slug}', ['as' => 'testyourself.exam.examStart', 'uses' => 'examListController@examStart']);

        Route::get('/exam-result', ['as' => 'testyourself.exam.examResult', 'uses' => 'examListController@examResult']);

        Route::get('exams', 'examListController@index');

        Route::get('/document/{slug?}', ['as' => 'testyourself.document.indexshow', 'uses' => 'DocumentsController@index']);
        Route::post('/document/{slug?}', ['as' => 'testyourself.document.indexshow', 'uses' => 'DocumentsController@index']);

        Route::get('buy-document', 'DocumentsController@buyDocument');
        Route::get('payment-list-documents', 'PaymentsController@multiPayment');
        Route::post('payment-list-documents', 'PaymentsController@multiPayment');
        Route::post('payment-list-exam', 'PaymentsController@handleMultiPaymentExam');
        Route::post('update-card', 'PaymentsController@updateCard');
        Route::post('payment-multi-exam', 'PaymentsController@choiceExams');
        Route::post('update-card-multi', 'PaymentsController@updateCardMulti');
        Route::get('multi-payment-exam-with-paystack-callback', 'PaymentsController@handleMultiPaymentExamCallback');
        Route::get('payment-with-paystack-callback/{type}/{id}', 'PaymentsController@handlePaystackCallback');
        Route::post('payment-with-paystack/{type}', 'PaymentsController@handlePaystack');

        Route::get('/document/download/{slug}{id}', ['as' => 'testyourself.document.download', 'uses' => 'DocumentsController@download']);
        Route::get('school-ranking', 'SchoolRankingController@index');
        Route::get('school/{slug}', 'SchoolRankingController@show');
        // Route::get('faq', function () {
        //     return view('testyourself.faq');
        // });
        Route::post('/document/urlBeforePayment', 'DocumentsController@urlBeforePayment');
        Route::post('saveFullURL', 'Controller@saveFullURL');
        Route::get('show-faq', 'FaqsController@show');

        Route::get('switch-currency', ['as' => 'testyourself.payment.currency', 'uses' => 'PaymentsController@switcherCurrencyAjax']);
    });
    Route::group(['prefix' => 'testyourself'], function () {
        //================ payment ===================
        Route::get('/payment/{type}-{id}', 'PaymentsController@index');
        Route::post('/payment/{type}-{id}', 'PaymentsController@payment');
    });

    Route::group(['prefix' => 'do-you-want-done'], function () {
        route::get('/', 'WantdoneController@index');
        route::get('/blank-page/{slug}', ['as' => 'youwantdone.blankpage.show', 'uses' => 'BlankpageController@showYouwantdone']);
    });

    /**
     * Forgot password
     */
    Route::get('forgot-password', 'Auth\AuthController@forgot_password');
    Route::post('password/email', 'Auth\PasswordController@postEmail');
    Route::post('password/reset', 'Auth\PasswordController@postReset');
    Route::get('password/reset', 'Auth\PasswordController@showResetForm');

    Route::group(['prefix' => 'print-pdf'], function () {
        Route::get('print-result/{id}', 'PDFController@printResult');
        Route::get('print-all-result', 'PDFController@printAllResult');
        Route::get('print-exam', 'PDFController@printExam');
        Route::get('result-pdf', 'PDFController@resultPDF');
    });

    Route::group(['prefix' => 'login'], function () {
        Route::get('/', ['as' => 'getLoginUser', 'uses' => 'Auth\AuthController@getLoginUser']);
        Route::post('/', ['as' => 'postLoginUser', 'uses' => 'Auth\AuthController@postLoginUser']);

        // Social Login
        Route::get('{provider?}', [
            'uses' => 'Auth\AuthController@getSocialAuth',
            'as'   => 'auth.getSocialAuth',
        ]);
        Route::get('callback/{provider?}', [
            'uses' => 'Auth\AuthController@getSocialAuthCallback',
            'as'   => 'auth.getSocialAuthCallback',
        ]);
    });
    Route::get('logout', 'Auth\AuthController@logout');
    Route::get('sign_up', ['as' => 'getSignUp', 'uses' => 'UserController@getSignUp']);
    Route::post('sign_up', ['as' => 'postSignUp', 'uses' => 'UserController@postSignUp']);

    Route::group(['prefix' => 'user'], function () {
        Route::get('profile', ['as' => 'user.GetProfile', 'uses' => 'UserController@UserGetProfile']);
        Route::group(['prefix' => 'edit'], function () {
            Route::get('profile', ['as' => 'user.edit.getProfile', 'uses' => 'UserController@UserEditGetProfile']);
            Route::post('profile', ['as' => 'user.edit.postProfile', 'uses' => 'UserController@UserEditPostProfile']);

            Route::get('changePassword', ['as' => 'getChangePassword', 'uses' => 'UserController@getChangePassword']);
            Route::post('changePassword', ['as' => 'postChangePassword', 'uses' => 'UserController@postChangePassword']);
        });
    });
});

/**
 * Backend
 */
Route::group(['prefix' => 'admin'], function () {
    Route::get('login', ['as' => 'admin.getLogin', 'uses' => 'Backend\AuthController@getLogin']);

    Route::post('login', ['as' => 'admin.postLogin', 'uses' => 'Backend\AuthController@postLogin']);

    Route::get('logout', ['as' => 'admin.postLogin', 'uses' => 'Backend\AdminController@logout']);

    Route::get('/', ['as' => 'admin.dashboard', 'uses' => 'Backend\AdminController@admin_dashboard']);

    Route::get('/profile', ['as' => 'admin.profile', 'uses' => 'Backend\AdminController@showProfile']);

    Route::post('/profile', ['as' => 'admin.updateProfile', 'uses' => 'Backend\AdminController@updateProfile']);

    Route::get('mailletter', ['as' => 'admin.mailletter', 'uses' => 'Backend\MailletterController@index']);

    Route::post('mailletter', ['as' => 'admin.mailletter', 'uses' => 'Backend\MailletterController@store']);

    Route::group(['prefix' => 'users'], function () {
        Route::get('lists', ['as' => 'admin.users.list', 'uses' => 'Backend\UserController@lists']);
        Route::get('delete/{id}', ['as' => 'admin.users.getDelete', 'uses' => 'Backend\UserController@getDelete']);
        Route::get('create', ['as' => 'admin.users.create', 'uses' => 'Backend\UserController@create']);
        Route::get('edit/{id}', ['as' => 'admin.users.getEdit', 'uses' => 'Backend\UserController@getEdit']);
        Route::post('edit/{id}', ['as' => 'admin.users.postEdit', 'uses' => 'Backend\UserController@postEdit']);
    });

    Route::group(['prefix' => 'exam'], function () {
        Route::get('add', ['as' => 'admin.exam.getAdd', 'uses' => 'Backend\ExamController@getAdd']);
        Route::post('add', ['as' => 'admin.exam.postAdd', 'uses' => 'Backend\ExamController@postAdd']);
        Route::get('lists', ['as' => 'admin.exam.lists', 'uses' => 'Backend\ExamController@lists']);
        Route::get('delete/{id}', ['as' => 'admin.exam.getDelete', 'uses' => 'Backend\ExamController@getDelete']);
        Route::get('edit/{id}', ['as' => 'admin.exam.getEdit', 'uses' => 'Backend\ExamController@getEdit']);
        Route::post('edit/{id}', ['as' => 'admin.exam.postEdit', 'uses' => 'Backend\ExamController@postEdit']);
    });

    Route::group(['prefix' => 'question'], function () {
        Route::get('add/{id}', ['as' => 'admin.question.getAdd', 'uses' => 'Backend\QuestionController@getAdd']);
        Route::post('add/{id}', ['as' => 'admin.question.postAdd', 'uses' => 'Backend\QuestionController@postAdd']);
        Route::post('addQuestionByCSVFile/{id}', 'Backend\QuestionController@addQuestionByCSVFile');
        Route::get('update/{id}', ['as' => 'admin.question.getUpdate', 'uses' => 'Backend\QuestionController@getUpdate']);
        Route::post('update/{id}', ['as' => 'admin.question.postUpdate', 'uses' => 'Backend\QuestionController@postUpdate']);
        Route::post('updateQuestionAndAnswer', ['as' => 'admin.exam.updateQuestionAndAnswer', 'uses' => 'Backend\QuestionController@updateQuestionAndAnswer']);
        Route::get('lists', ['as' => 'admin.question.lists', 'uses' => 'Backend\QuestionController@lists']);
        Route::get('delete/{id}', ['as' => 'admin.question.getDelete', 'uses' => 'Backend\QuestionController@getDelete']);
        Route::get('edit/{id}', ['as' => 'admin.question.getEdit', 'uses' => 'Backend\QuestionController@getEdit']);
        Route::post('edit/{id}', ['as' => 'admin.question.postEdit', 'uses' => 'Backend\QuestionController@postEdit']);
    });
    Route::group(['prefix' => 'answer'], function () {
        Route::get('delete/{id}', ['as' => 'admin.answer.getDelete', 'uses' => 'Backend\AnswerController@getDelete']);
    });

    //============================ Category =============================
    Route::get('/category', ['as' => 'admin.category.getList', 'uses' => 'Backend\CategoriesController@index']);
    // add Category
    Route::get('/add-category', 'Backend\CategoriesController@create');
    Route::post('/add-category', 'Backend\CategoriesController@store');
    // edit category
    Route::get('/edit-category/{id}', 'Backend\CategoriesController@edit');
    Route::put('/edit-category/{id}', 'Backend\CategoriesController@update');
    // delete category
    Route::get('category/delete/{id}', ['as' => 'admin.category.destroy', 'uses' => 'Backend\CategoriesController@destroy']);
    //===================================================================

    Route::group(['prefix' => 'blankpage'], function () {
        Route::get('/', 'Backend\BlankpageController@index');
        Route::get('/add', 'Backend\BlankpageController@create');
        Route::post('/add', 'Backend\BlankpageController@store');

        Route::get('/edit/{id}', 'Backend\BlankpageController@edit');
        Route::post('/edit/{id}', 'Backend\BlankpageController@update');

        route::post('/', 'Backend\BlankpageController@destroy');

    });
    //=========== video && flash ==============
    Route::group(['prefix' => 'videoflash'], function () {
        Route::get('/', 'Backend\VideoflashController@index');
        Route::post('/', 'Backend\VideoflashController@update');
        Route::get('add', 'Backend\VideoflashController@create');
        Route::post('add', 'Backend\VideoflashController@store');
    });

    //=========== Option website ==============
    Route::group(['prefix' => 'option'], function () {
        Route::get('/', ['as' => 'getOption', 'uses' => 'Backend\OptionController@index']);
        Route::post('/', ['as' => '/', 'uses' => 'Backend\OptionController@update']);
    });

    //========== slider =========================
    Route::group(['prefix' => 'slider'], function () {
        Route::get('/', 'Backend\SliderController@index');
        Route::post('/', 'Backend\SliderController@destroy');
        Route::get('/add', 'Backend\SliderController@create');
        Route::post('/add', 'Backend\SliderController@store');
        Route::get('/edit/{id}', 'Backend\SliderController@edit');
        Route::post('/edit/{id}', 'Backend\SliderController@update');
        Route::post('/', 'Backend\SliderController@destroy');
    });

    Route::resource('country', 'Backend\CountriesController');
    Route::resource('state', 'Backend\StatesController');
    Route::resource('document', 'Backend\DocumentsController');
    Route::resource('school_type', 'Backend\SchoolTypesController');
    Route::resource('school_degree', 'Backend\SchoolDegreesController');
    Route::resource('school', 'Backend\SchoolsController');

    // Route::group(['prefix' => 'exam-result'], function () {
    //     Route::get('/', 'Backend\ExamResultController@index');
    // });
    Route::resource('exam-result', 'Backend\ExamResultController', ['as' => 'admin']);

    Route::group(['prefix' => 'partners'], function () {
        Route::get('/', 'Backend\PartnerController@listPartner');
    });

    Route::get('maintenance', 'Backend\MaintenanceController@index');
    Route::post('maintenance', 'Backend\MaintenanceController@maintenance');
    //=========================== FAQ - 20161216 ========================
    Route::group(['prefix' => 'faq', 'middleware' => 'web'], function () {
        Route::get('', ['as' => 'admin.faq.index', 'uses' => 'FaqsController@index']);
        Route::get('add-new', ['as' => 'admin.faq.create', 'uses' => 'FaqsController@create']);
        Route::post('add-new', ['as' => 'admin.faq.store', 'uses' => 'FaqsController@store']);
        Route::get('update/{id}', ['as' => 'admin.faq.edit', 'uses' => 'FaqsController@edit']);
        Route::post('update/{id}', ['as' => 'admin.faq.update', 'uses' => 'FaqsController@update']);
        Route::get('delete/{id}', ['as' => 'admin.faq.destroy', 'uses' => 'FaqsController@destroy']);
    });

    Route::any('{path?}', function () {
        return view("admin.spa");
    })->where("path", ".+");
});

Route::get('/session', function () {
    dd(Session::all());
});

Route::get('clearSession', function (Request $request) {
    $request->session()->flush();
});

Route::get('school/getStatesByCountry/{country_id}', 'Controller@getStatesByCountry');

Route::get('test', function (Request $request) {
    $register        = \App\Entities\User::first();
    $register->email = $request->get('email');
    Mail::send('mails.userRegister', ['register' => $register], function ($message) use ($register) {
        $message->from('vicoders.daily.report@gmail.com', 'Globaltestpoint');
        $message->to($register->email)->subject('Registration successful ! globaltestpoint');
    });
});

Route::get('/api/me', 'API\UserController@me');
Route::get('partner', ['middleware' => 'auth', 'uses' => 'PartnerController@partner']);
Route::get('become-partner', 'PartnerController@becomePartner');
