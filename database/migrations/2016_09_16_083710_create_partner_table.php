<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePartnerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partners', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name', 40);
            $table->string('last_name', 40);
            $table->string('business_name', 40);
            $table->string('address');
            $table->string('about');
            $table->string('city', 40);
            $table->string('email', 40);
            $table->integer('country_id')->unsigned();
            $table->integer('state_id')->unsigned();
            $table->string('review_links');
            $table->string('website');
            $table->string('how_did_here');
            $table->string('how_do_intend_to_reach');
            $table->string('interest');
            $table->string('number_of_users');
            $table->string('target');
            $table->string('perference');
            $table->boolean('active')->default(1);
            $table->boolean('accept');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('partners');
    }
}
