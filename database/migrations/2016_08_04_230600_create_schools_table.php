<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->string('image');
            $table->integer('rank')->unsigned();
            $table->integer('state_id')->unsigned();
            $table->foreign('state_id')->references('id')->on('states')->onDelete('cascade');
            $table->integer('school_type_id')->unsigned();
            $table->foreign('school_type_id')->references('id')->on('school_types')->onDelete('cascade');
            $table->integer('school_degree_id')->unsigned();
            $table->foreign('school_degree_id')->references('id')->on('school_degrees')->onDelete('cascade');
            $table->integer('order');
            $table->boolean('active')->default(1);
            $table->text('summary');
            $table->text('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('schools');
    }
}
