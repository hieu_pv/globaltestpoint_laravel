<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug');
            $table->string('image');
            $table->string('address');
            $table->string('min_salary', 20);
            $table->string('max_salary', 20);
            $table->string('zipcode');
            $table->string('currency', 4);
            $table->text('certificate');
            $table->boolean('gender')->nullable();
            $table->integer('experience_id')->unsigned()->references('id')->on('experiences')->onDelete('cascade');
            $table->integer('employer_id')->unsigned()->references('id')->on('employers')->onDelete('cascade');
            $table->integer('city_id')->unsigned();
            $table->integer('state_id')->unsigned();
            $table->integer('country_id')->unsigned();
            $table->integer('nationality_id')->unsigned()->references('id')->on('nationalities')->onDelete('cascade');
            $table->integer('job_title_id')->unsigned()->references('id')->on('job_titles')->onDelete('cascade');
            $table->integer('qualification_id')->unsigned()->references('id')->on('qualifications')->onDelete('cascade');
            $table->text('short_desc');
            $table->longText('description');
            $table->text('desired_candidate_profile');
            $table->string('type')->default(1);
            $table->text('location');
            $table->integer('order');
            $table->boolean('active')->default(1);
            $table->boolean('archive')->default(0);
            $table->boolean('complete')->default(0);
            $table->boolean('draft')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
