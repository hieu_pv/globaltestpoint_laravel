<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('users', function (Blueprint $table) {
			$table->increments('id');
			$table->string('first_name');
			$table->string('middle_name');
			$table->string('last_name');
			$table->string('email')->unique();
			$table->string('password');
			$table->string('image');
			$table->string('mobile');
			$table->date('birth');
			$table->string('user_type');
			$table->string('sumary');
			$table->string('address');
			$table->boolean('gender')->comment('0: female, 1: male');
			$table->string('city', 100);
			$table->string('country', 40);
			$table->boolean('active')->comment('0: no active, 1: active');
			$table->string('email_verified')->comment('0: no verify, 1:verify');
			$table->string('account_type')->default('normal');
			$table->string('social_id');
			$table->rememberToken();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('users');
	}
}