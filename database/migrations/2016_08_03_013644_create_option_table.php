<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('options', function(Blueprint $table){
            $table->increments('id');
            $table->string('logo');
            $table->integer('page')->comment('0: test yourself; 1: You want done');
            $table->string('title');
            $table->string('email');
            $table->string('phone');
            $table->string('address');
            $table->string('fax');
            $table->string('facebook');
            $table->string('twitter');
            $table->string('image_login');
            $table->string('image_register');
            $table->string('image_login_register');
            $table->integer('viewsite')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('options');
    }
}
