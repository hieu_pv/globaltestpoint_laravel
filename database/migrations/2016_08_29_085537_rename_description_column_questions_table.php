<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class RenameDescriptionColumnQuestionsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('questions', function (Blueprint $table) {
			$table->renameColumn('description', 'solution');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('questions', function (Blueprint $table) {
			$table->renameColumn('solution', 'description');
		});
	}
}
