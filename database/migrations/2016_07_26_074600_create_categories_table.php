<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->integer('parent_id')->unsigned();
            $table->boolean('active')->default(1);
            $table->integer('order')->unsigned();
            $table->string('slug')->unique();
            $table->string('type', 20);
            $table->string('position', 50);
            $table->integer('page_id')->comment('1: menu; 2: testyourself; 3: youwantdone');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories');
    }
}
