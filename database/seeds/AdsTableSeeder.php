<?php

use App\Entities\Ads;
use Illuminate\Database\Seeder;

// composer require laracasts/testdummy

class AdsTableSeeder extends Seeder
{
    public function run()
    {
        $ads            = new Ads;
        $ads->link   = "https://www.youtube.com/watch?v=3NE_k60Bzyk";
        $ads->active    = 1;
        $ads->page = 0;
        $ads->type      = 0;
        $ads->author = 1;
        $ads->save();

        $ads            = new Ads;
        $ads->link   = "https://www.youtube.com/watch?v=3NE_k60Bzyk";
        $ads->active    = 1;
        $ads->page = 1;
        $ads->type      = 0;
        $ads->author = 1;
        $ads->save();

        for ($i=0; $i < 2 ; $i++) { 
            $ads            = new Ads;
            $ads->link   = "/uploads/default_post.png";
            $ads->active    = 1;
            $ads->page = 0;
            $ads->type      = 1;
            $ads->author = 1;
            $ads->save();

            $ads            = new Ads;
            $ads->link   = "/uploads/default_post.png";
            $ads->active    = 1;
            $ads->page = 1;
            $ads->type      = 1;
            $ads->author = 1;
            $ads->save();
        }
    }
}
