<?php

use App\Entities\Country;
use Illuminate\Database\Seeder;

class UpdateCountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $nigeria = Country::where('name', 'Nigeria')->first();
        if ($nigeria) {
            $nigeria->is_supported_phone_area = 1;
            $nigeria->save();
        }
    }
}
