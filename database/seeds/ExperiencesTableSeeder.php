<?php

use App\Entities\Experience;
use Illuminate\Database\Seeder;

class ExperiencesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $experiences = [
            [
                'label' => '<1 year',
            ],
            [
                'label' => '1-2 years',
            ],
            [
                'label' => '2-3 years',
            ],
            [
                'label' => '3-5 years',
            ],
            [
                'label' => '>5 years',
            ],
        ];

        foreach ($experiences as $experience) {
            $new_experience = Experience::create([
                'label' => $experience['label'],
            ], $experience);
            $new_experience->status = 1;
            $new_experience->save();
        }
    }
}
