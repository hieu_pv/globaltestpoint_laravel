<?php

use App\Entities\Employer;
use App\Entities\User;
use Illuminate\Database\Seeder;

class EmployerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = new Faker\Generator();
        $faker->addProvider(new Faker\Provider\en_US\Person($faker));
        $faker->addProvider(new Faker\Provider\en_US\Company($faker));
        $faker->addProvider(new Faker\Provider\en_SG\PhoneNumber($faker));
        $faker->addProvider(new Faker\Provider\en_SG\Address($faker));
        $faker->addProvider(new Faker\Provider\en_US\Company($faker));
        $faker->addProvider(new Faker\Provider\DateTime($faker));
        $faker->addProvider(new Faker\Provider\Lorem($faker));
        $faker->addProvider(new Faker\Provider\Internet($faker));
        $users = User::whereHas('roles', function ($q) {
            $q->where('slug', 'employer');
        })->get();

        foreach ($users as $key => $user) {
            $company_name = $faker->company();
            $data         = [
                'company_name'        => $company_name,
                'slug'                => str_slug($company_name . $key),
                'registration_number' => '123456789',
                'fax'                 => '71937729',
                'website'             => 'https://www.google.com.vn',
                'company_address'     => $faker->address(),
                'company_about'       => $faker->paragraph(),
                'number_of_outlets'   => 'multiple outlet company',
                'contact_name'        => 'Skype: chungnd',
                'note'                => 'Private note',
            ];
            $employer = Employer::create($data);
            $employer->users()->sync([$user->id]);
        }
    }
}
