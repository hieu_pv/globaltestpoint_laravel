<?php

use App\Entities\Document;
use Illuminate\Database\Seeder;

class DocumentsTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$faker = new Faker\Generator();
		$faker->addProvider(new Faker\Provider\en_US\Person($faker));
		$faker->addProvider(new Faker\Provider\en_SG\Address($faker));
		$faker->addProvider(new Faker\Provider\en_US\PhoneNumber($faker));
		$faker->addProvider(new Faker\Provider\en_US\Company($faker));
		$faker->addProvider(new Faker\Provider\Lorem($faker));
		$faker->addProvider(new Faker\Provider\DateTime($faker));
		$faker->addProvider(new Faker\Provider\Internet($faker));

		for ($i = 1; $i <= 50; $i++) {
			$document           = new Document;
			$document->name     = $faker->sentence();
			$document->image    = 'uploads/default_post.png';
			$document->document = 'uploads/documents/default_beginhtml.pdf';
			$document->download = 1;
			$document->price    = mt_rand(10, 30);
			$document->cate_id  = mt_rand(72, 76);
			$document->order    = 0;
			$document->active   = 1;
			$document->save();
		}
	}
}
