<?php

use App\Entities\Industry;
use Illuminate\Database\Seeder;

class IndustriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $industries = [
            [
                'name' => 'Oil & Gas',
            ],
            [
                'name' => 'Hospitality',
            ],
            [
                'name' => 'Banking',
            ],
            [
                'name' => 'IT-Software Services',
            ],
            [
                'name' => 'Healthcare',
            ],
            [
                'name' => 'Civil Engineering',
            ],
            [
                'name' => 'Education',
            ],
        ];

        foreach ($industries as $industry) {
            $industry['slug']     = str_slug($industry['name']);
            $new_industry         = Industry::create($industry);
            $new_industry->status = 1;
            $new_industry->save();
        }
    }
}
