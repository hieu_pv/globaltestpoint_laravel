<?php

use App\Entities\City;
use App\Entities\State;
use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = [
            [
                'name'       => 'Umuahia',
                'state_name' => 'Abia State',
            ],
            [
                'name'       => 'Yola',
                'state_name' => 'Adamawa State',
            ],
            [
                'name'       => 'Uyo',
                'state_name' => 'Akwa Ibom State',
            ],
            [
                'name'       => 'Awka',
                'state_name' => 'Anambra State',
            ],
            [
                'name'       => 'Bauchi',
                'state_name' => 'Bauchi State',
            ],
            [
                'name'       => 'Yenagoa',
                'state_name' => 'Bayelsa State',
            ],
            [
                'name'       => 'Makurdi',
                'state_name' => 'Benue State',
            ],
            [
                'name'       => 'Maiduguri',
                'state_name' => 'Borno State',
            ],
            [
                'name'       => 'Calabar',
                'state_name' => 'Cross River State',
            ],
            [
                'name'       => 'Asaba',
                'state_name' => 'Delta State',
            ],
            [
                'name'       => 'Abakaliki',
                'state_name' => 'Ebonyi State',
            ],
            [
                'name'       => 'Benin',
                'state_name' => 'Edo State',
            ],
            [
                'name'       => 'Ado-Ekiti',
                'state_name' => 'Ekiti State',
            ],
            [
                'name'       => 'Enugu',
                'state_name' => 'Enugu State',
            ],
            [
                'name'       => 'Gombe',
                'state_name' => 'Gombe State',
            ],
            [
                'name'       => 'Owerri',
                'state_name' => 'Imo State',
            ],
            [
                'name'       => 'Dutse',
                'state_name' => 'Jigawa State',
            ],
            [
                'name'       => 'Kaduna',
                'state_name' => 'Kaduna State',
            ],
            [
                'name'       => 'Kano',
                'state_name' => 'Kano State',
            ],
            [
                'name'       => 'Katsina',
                'state_name' => 'Katsina State',
            ],
            [
                'name'       => 'Birnin Kebbi',
                'state_name' => 'Kebbi State',
            ],
            [
                'name'       => 'Lokoja',
                'state_name' => 'Kogi State',
            ],
            [
                'name'       => 'Ilorin',
                'state_name' => 'Kwara State',
            ],
            [
                'name'       => 'Ikeja',
                'state_name' => 'Lagos State',
            ],
            [
                'name'       => 'Lafia',
                'state_name' => 'Nasarawa State',
            ],
            [
                'name'       => 'Minna',
                'state_name' => 'Niger State',
            ],
            [
                'name'       => 'Abeokuta',
                'state_name' => 'Ogun State',
            ],
            [
                'name'       => 'Akure',
                'state_name' => 'Ondo State',
            ],
            [
                'name'       => 'Oshogbo',
                'state_name' => 'Osun State',
            ],
            [
                'name'       => 'Ibadan',
                'state_name' => 'Oyo State',
            ],
            [
                'name'       => 'Jos',
                'state_name' => 'Plateau State',
            ],
            [
                'name'       => 'Port Harcourt',
                'state_name' => 'Rivers State',
            ],
            [
                'name'       => 'Sokoto',
                'state_name' => 'Sokoto State',
            ],
            [
                'name'       => 'Jalingo',
                'state_name' => 'Taraba State',
            ],
            [
                'name'       => 'Damaturu',
                'state_name' => 'Yobe State',
            ],
            [
                'name'       => 'Gusau',
                'state_name' => 'Zamfara',
            ],
            [
                'name'       => 'Abuja',
                'state_name' => 'FCT',
            ],
        ];

        foreach ($cities as $city) {
            $state = State::where('name', $city['state_name'])->first();
            if ($state) {
                $city_data = [
                    'name' => $city['name'],
                ];

                $new_city           = City::updateOrCreate(['name' => $city_data['name']], $city_data);
                $new_city->state_id = $state->id;
                $new_city->status   = 1;
                $new_city->save();
            }
        }
    }
}
