<?php

use App\Entities\SchoolType;
use Illuminate\Database\Seeder;

class SchoolTypesSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$schoolTypes = [
			[
				'name' => 'Economy',
			],
			[
				'name' => 'Engineering',
			],
			[
				'name' => 'Education',
			],
			[
				'name' => 'Information Technology',
			],
		];

		foreach ($schoolTypes as $item) {
			$schoolType       = new SchoolType;
			$schoolType->name = $item['name'];
			$schoolType->save();
		}
	}
}
