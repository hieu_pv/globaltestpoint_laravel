<?php

use App\Entities\Exam;
use Illuminate\Database\Seeder;

class ExamsTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$durations = [5, 10, 15, 20, 25, 30];
		$actives   = ['yes', 'no'];
		$grade     = [
			[
				'title' => 'A',
				'min'   => 70,
				'max'   => 100,
			],
			[
				'title' => 'B',
				'min'   => 60,
				'max'   => 69,
			],
			[
				'title' => 'C',
				'min'   => 50,
				'max'   => 59,
			],
			[
				'title' => 'E',
				'min'   => 40,
				'max'   => 49,
			],
			[
				'title' => 'F',
				'min'   => 0,
				'max'   => 39,
			],
		];
		$grade = json_encode($grade);

		$slug_index = 0;
		for ($i = 1; $i <= 5; $i++) {
			$title              = "Exam " . $i;
			$exam               = new Exam;
			$exam->name         = $title;
			$exam->slug         = str_slug($title);
			$exam->duration     = $durations[mt_rand(0, 5)];
			$exam->price        = mt_rand(0, 3);
			$exam->pass_percent = mt_rand(60, 80);
			$exam->grade        = $grade;
			$exam->active       = 'yes';
			$exam->category_id  = 4;
			$exam->save();
		}
	}
}
