<?php

use App\Entities\JobTitle;
use Illuminate\Database\Seeder;

class JobTitlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $job_titles = [
            [
                'name'      => 'Roustabout',
                'job_title' => 'Roustabout',
            ],
            [
                'name'      => 'Relationship Officer',
                'job_title' => 'Relationship Officer',
            ],
            [
                'name'      => 'Floorman',
                'job_title' => 'Floorman',
            ],
            [
                'name'      => 'Surveyor',
                'job_title' => 'Surveyor',
            ],
            [
                'name'      => 'Plumber',
                'job_title' => 'Plumber',
            ],
            [
                'name'      => 'Travel Manager',
                'job_title' => 'Travel Manager',
            ],
            [
                'name'      => 'Driver',
                'job_title' => 'Driver',
            ],
        ];

        foreach ($job_titles as $job_title) {
            $job_title['job_description'] = 'Description';

            $new_job_title = JobTitle::create([
                'name' => $job_title['name'],
            ], $job_title);
            $new_job_title->status = 1;
            $new_job_title->save();
        }
    }
}
