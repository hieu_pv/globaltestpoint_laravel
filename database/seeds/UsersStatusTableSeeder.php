<?php

use App\Entities\UserStatus;
use Illuminate\Database\Seeder;

class UsersStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = [
            [
                'id'          => 1,
                'status'      => 'Pending',
                'description' => 'just registered and nothing has been done',
                'active'      => 1,
            ],
            [
                'id'          => 2,
                'status'      => 'In complete',
                'description' => 'No name on the profile',
                'active'      => 0,
            ],
            [
                'id'          => 3,
                'status'      => 'SMS Sent',
                'description' => 'called but no answer and SMS sent to him to arrange call',
                'active'      => 0,
            ],
            [
                'id'          => 4,
                'status'      => 'No Interest',
                'description' => 'don\'t want to use us anymore',
                'active'      => 1,
            ],
            [
                'id'          => 5,
                'status'      => 'Active',
                'description' => 'Been interviewed and it\'s suitable for job',
                'active'      => 1,
            ],
            [
                'id'          => 6,
                'status'      => 'Banned',
                'description' => 'Didn\'t show up for job',
                'active'      => 1,
            ],
            [
                'id'          => 7,
                'status'      => 'Try 1',
                'description' => 'US tried to contact candidate first time',
                'active'      => 0,
            ],
            [
                'id'          => 8,
                'status'      => 'Try 2',
                'description' => 'US tried to contact candidate second time',
                'active'      => 0,
            ],
            [
                'id'          => 9,
                'status'      => 'Try 3',
                'description' => 'US tried to contact candidate third time',
                'active'      => 0,
            ],
            [
                'id'          => 10,
                'status'      => 'Unreachable',
                'description' => 'US tried to contact candidate 3 time but no reply',
                'active'      => 0,
            ],
            [
                'id'          => 11,
                'status'      => 'Not Eligible',
                'description' => 'the candidate is not eligible',
                'active'      => 1,
            ],
            [
                'id'          => 12,
                'status'      => 'Waiting for Approval',
                'description' => 'waiting for approval',
                'active'      => 0,
            ],
            [
                'id'          => 13,
                'status'      => '2nd chance',
                'description' => '2nd chance',
                'active'      => 1,
            ],
            [
                'id'          => 14,
                'status'      => 'Suspended',
                'description' => 'Suspended',
                'active'      => 1,
            ],
        ];

        foreach ($status as $item) {
            UserStatus::updateOrCreate([
                'status' => $item['status'],
            ], $item);
        }
    }
}
