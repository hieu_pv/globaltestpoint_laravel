<?php

use App\Entities\School;
use Illuminate\Database\Seeder;

class SchoolsSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$schools = [
			[
				'name'  => 'Stowe School',
				'image' => '/uploads/schools/top_uk_private_school_stowe_exterior-01-302.jpg',
			],
			[
				'name'  => 'Oakham School',
				'image' => '/uploads/schools/private_boarding_school_oakham_pupils-302.jpg',
			],
			[
				'name'  => 'Bilton Grande',
				'image' => '/uploads/schools/preparatory_schools_in_england_bilton_grange_gardening-302.jpg',
			],
			[
				'name'  => "D'OVERBROECK'S",
				'image' => '/uploads/schools/best_school_d_overbroecks_students-302.jpg',
			],
			[
				'name'  => 'Wycliffe School',
				'image' => '/uploads/schools/top_british_private_schools_wycliffe_studying_outside-302.jpg',
			],
			[
				'name'  => 'Sevenoaks School',
				'image' => '/uploads/schools/boarding_school_sevenoaks_school_students_sailing-942.jpg',
			],
			[
				'name'  => "Queen's College",
				'image' => '/uploads/schools/best_boarding_schools_england_queens_college_pupils_outside-302.jpg',
			],
			[
				'name'  => 'Oxford Tutorial College',
				'image' => '/uploads/schools/oxford_tutorial_college_exterior-302.jpg',
			],
			[
				'name'  => 'Woldingham School',
				'image' => '/uploads/schools/best_private_schools_in_england_woldingham_school_students_coffee_bar-302.jpg',
			],
			[
				'name'  => "Marymount International School",
				'image' => '/uploads/schools/marymount_school_london_ib_girls_school_girlfriends-302.jpg',
			],
		];

		$faker = new Faker\Generator();
		$faker->addProvider(new Faker\Provider\Lorem($faker));

		$description = '';
		foreach ($faker->paragraphs() as $paragraph) {
			$description .= '<p>' . $paragraph . '</p>';
		}

		foreach ($schools as $key => $item) {
			$school                   = new School;
			$school->name             = $item['name'];
			$school->image            = $item['image'];
			$school->rank             = intval($key + 1);
			$school->state_id         = mt_rand(1, 20);
			$school->school_type_id   = mt_rand(1, 4);
			$school->school_degree_id = mt_rand(1, 2);
			$school->summary          = $faker->paragraph();
			$school->description      = $description;
			$school->save();
		}
	}
}
