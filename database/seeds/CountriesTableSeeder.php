<?php

use App\Entities\Country;
use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = [
            [
                "name"            => "Israel",
                "phone_area_code" => "972",
                "code"            => "IL",
                "order"           => 4,

            ], [
                "name"            => "Afghanistan",
                "phone_area_code" => "93",
                "code"            => "AF",
                "order"           => 4,

            ], [
                "name"            => "Albania",
                "phone_area_code" => "355",
                "code"            => "AL",
                "order"           => 3,
            ], [
                "name"            => "Algeria",
                "phone_area_code" => "213",
                "code"            => "DZ",
                "order"           => 4,
            ], [
                "name"            => "AmericanSamoa",
                "phone_area_code" => "1 684",
                "code"            => "AS",
                "order"           => 5,
            ], [
                "name"            => "Andorra",
                "phone_area_code" => "376",
                "code"            => "AD",
                "order"           => 6,
            ], [
                "name"            => "Angola",
                "phone_area_code" => "244",
                "code"            => "AO",
                "order"           => 7,
            ], [
                "name"            => "Anguilla",
                "phone_area_code" => "1 264",
                "code"            => "AI",
                "order"           => 8,
            ], [
                "name"            => "Antigua and Barbuda",
                "phone_area_code" => "1268",
                "code"            => "AG",
                "order"           => 9,
            ], [
                "name"            => "Argentina",
                "phone_area_code" => "54",
                "code"            => "AR",
                "order"           => 10,
            ], [
                "name"            => "Armenia",
                "phone_area_code" => "374",
                "code"            => "AM",
                "order"           => 11,
            ], [
                "name"            => "Aruba",
                "phone_area_code" => "297",
                "code"            => "AW",
                "order"           => 12,
            ], [
                "name"            => "Australia",
                "phone_area_code" => "61",
                "code"            => "AU",
                "order"           => 13,
            ], [
                "name"            => "Austria",
                "phone_area_code" => "43",
                "code"            => "AT",
                "order"           => 14,
            ], [
                "name"            => "Azerbaijan",
                "phone_area_code" => "994",
                "code"            => "AZ",
                "order"           => 15,
            ], [
                "name"            => "Bahamas",
                "phone_area_code" => "1 242",
                "code"            => "BS",
                "order"           => 16,
            ], [
                "name"            => "Bahrain",
                "phone_area_code" => "973",
                "code"            => "BH",
                "order"           => 17,
            ], [
                "name"            => "Bangladesh",
                "phone_area_code" => "880",
                "code"            => "BD",
                "order"           => 18,
            ], [
                "name"            => "Barbados",
                "phone_area_code" => "1 246",
                "code"            => "BB",
                "order"           => 19,
            ], [
                "name"            => "Belarus",
                "phone_area_code" => "375",
                "code"            => "BY",
                "order"           => 20,
            ], [
                "name"            => "Belgium",
                "phone_area_code" => "32",
                "code"            => "BE",
                "order"           => 21,
            ], [
                "name"            => "Belize",
                "phone_area_code" => "501",
                "code"            => "BZ",
                "order"           => 22,
            ], [
                "name"            => "Benin",
                "phone_area_code" => "229",
                "code"            => "BJ",
                "order"           => 23,
            ], [
                "name"            => "Bermuda",
                "phone_area_code" => "1 441",
                "code"            => "BM",
                "order"           => 24,
            ], [
                "name"            => "Bhutan",
                "phone_area_code" => "975",
                "code"            => "BT",
                "order"           => 25,
            ], [
                "name"            => "Bosnia and Herzegovina",
                "phone_area_code" => "387",
                "code"            => "BA",
                "order"           => 26,
            ], [
                "name"            => "Botswana",
                "phone_area_code" => "267",
                "code"            => "BW",
                "order"           => 27,
            ], [
                "name"            => "Brazil",
                "phone_area_code" => "55",
                "code"            => "BR",
                "order"           => 28,
            ], [
                "name"            => "British Indian Ocean Territory",
                "phone_area_code" => "246",
                "code"            => "IO",
                "order"           => 29,
            ], [
                "name"            => "Bulgaria",
                "phone_area_code" => "359",
                "code"            => "BG",
                "order"           => 30,
            ], [
                "name"            => "Burkina Faso",
                "phone_area_code" => "226",
                "code"            => "BF",
                "order"           => 31,
            ], [
                "name"            => "Burundi",
                "phone_area_code" => "257",
                "code"            => "BI",
                "order"           => 32,
            ], [
                "name"            => "Cambodia",
                "phone_area_code" => "855",
                "code"            => "KH",
                "order"           => 33,
            ], [
                "name"            => "Cameroon",
                "phone_area_code" => "237",
                "code"            => "CM",
                "order"           => 34,
            ], [
                "name"            => "Canada",
                "phone_area_code" => "1",
                "code"            => "CA",
                "order"           => 35,
            ], [
                "name"            => "Cape Verde",
                "phone_area_code" => "238",
                "code"            => "CV",
                "order"           => 36,
            ], [
                "name"            => "Cayman Islands",
                "phone_area_code" => " 345",
                "code"            => "KY",
                "order"           => 37,
            ], [
                "name"            => "Central African Republic",
                "phone_area_code" => "236",
                "code"            => "CF",
                "order"           => 38,
            ], [
                "name"            => "Chad",
                "phone_area_code" => "235",
                "code"            => "TD",
                "order"           => 39,
            ], [
                "name"            => "Chile",
                "phone_area_code" => "56",
                "code"            => "CL",
                "order"           => 40,
            ], [
                "name"            => "China",
                "phone_area_code" => "86",
                "code"            => "CN",
                "order"           => 41,
            ], [
                "name"            => "Christmas Island",
                "phone_area_code" => "61",
                "code"            => "CX",
                "order"           => 42,
            ], [
                "name"            => "Colombia",
                "phone_area_code" => "57",
                "code"            => "CO",
                "order"           => 43,
            ], [
                "name"            => "Comoros",
                "phone_area_code" => "269",
                "code"            => "KM",
                "order"           => 44,
            ], [
                "name"            => "Congo",
                "phone_area_code" => "242",
                "code"            => "CG",
                "order"           => 45,
            ], [
                "name"            => "Cook Islands",
                "phone_area_code" => "682",
                "code"            => "CK",
                "order"           => 46,
            ], [
                "name"            => "Costa Rica",
                "phone_area_code" => "506",
                "code"            => "CR",
                "order"           => 47,
            ], [
                "name"            => "Croatia",
                "phone_area_code" => "385",
                "code"            => "HR",
                "order"           => 48,
            ], [
                "name"            => "Cuba",
                "phone_area_code" => "53",
                "code"            => "CU",
                "order"           => 49,
            ], [
                "name"            => "Cyprus",
                "phone_area_code" => "537",
                "code"            => "CY",
                "order"           => 50,
            ], [
                "name"            => "Czech Republic",
                "phone_area_code" => "420",
                "code"            => "CZ",
                "order"           => 51,
            ], [
                "name"            => "Denmark",
                "phone_area_code" => "45",
                "code"            => "DK",
                "order"           => 52,
            ], [
                "name"            => "Djibouti",
                "phone_area_code" => "253",
                "code"            => "DJ",
                "order"           => 53,
            ], [
                "name"            => "Dominica",
                "phone_area_code" => "1 767",
                "code"            => "DM",
                "order"           => 54,
            ], [
                "name"            => "Dominican Republic",
                "phone_area_code" => "1 849",
                "code"            => "DO",
                "order"           => 55,
            ], [
                "name"            => "Ecuador",
                "phone_area_code" => "593",
                "code"            => "EC",
                "order"           => 56,
            ], [
                "name"            => "Egypt",
                "phone_area_code" => "20",
                "code"            => "EG",
                "order"           => 57,
            ], [
                "name"            => "El Salvador",
                "phone_area_code" => "503",
                "code"            => "SV",
                "order"           => 58,
            ], [
                "name"            => "Equatorial Guinea",
                "phone_area_code" => "240",
                "code"            => "GQ",
                "order"           => 59,
            ], [
                "name"            => "Eritrea",
                "phone_area_code" => "291",
                "code"            => "ER",
                "order"           => 60,
            ], [
                "name"            => "Estonia",
                "phone_area_code" => "372",
                "code"            => "EE",
                "order"           => 61,
            ], [
                "name"            => "Ethiopia",
                "phone_area_code" => "251",
                "code"            => "ET",
                "order"           => 62,
            ], [
                "name"            => "Faroe Islands",
                "phone_area_code" => "298",
                "code"            => "FO",
                "order"           => 63,
            ], [
                "name"            => "Fiji",
                "phone_area_code" => "679",
                "code"            => "FJ",
                "order"           => 64,
            ], [
                "name"            => "Finland",
                "phone_area_code" => "358",
                "code"            => "FI",
                "order"           => 65,
            ], [
                "name"            => "France",
                "phone_area_code" => "33",
                "code"            => "FR",
                "order"           => 66,
            ], [
                "name"            => "French Guiana",
                "phone_area_code" => "594",
                "code"            => "GF",
                "order"           => 67,
            ], [
                "name"            => "French Polynesia",
                "phone_area_code" => "689",
                "code"            => "PF",
                "order"           => 68,
            ], [
                "name"            => "Gabon",
                "phone_area_code" => "241",
                "code"            => "GA",
                "order"           => 69,
            ], [
                "name"            => "Gambia",
                "phone_area_code" => "220",
                "code"            => "GM",
                "order"           => 70,
            ], [
                "name"            => "Georgia",
                "phone_area_code" => "995",
                "code"            => "GE",
                "order"           => 71,
            ], [
                "name"            => "Germany",
                "phone_area_code" => "49",
                "code"            => "DE",
                "order"           => 72,
            ], [
                "name"            => "Ghana",
                "phone_area_code" => "233",
                "code"            => "GH",
                "order"           => 73,
            ], [
                "name"            => "Gibraltar",
                "phone_area_code" => "350",
                "code"            => "GI",
                "order"           => 74,
            ], [
                "name"            => "Greece",
                "phone_area_code" => "30",
                "code"            => "GR",
                "order"           => 75,
            ], [
                "name"            => "Greenland",
                "phone_area_code" => "299",
                "code"            => "GL",
                "order"           => 76,
            ], [
                "name"            => "Grenada",
                "phone_area_code" => "1 473",
                "code"            => "GD",
                "order"           => 77,
            ], [
                "name"            => "Guadeloupe",
                "phone_area_code" => "590",
                "code"            => "GP",
                "order"           => 78,
            ], [
                "name"            => "Guam",
                "phone_area_code" => "1 671",
                "code"            => "GU",
                "order"           => 79,
            ], [
                "name"            => "Guatemala",
                "phone_area_code" => "502",
                "code"            => "GT",
                "order"           => 80,
            ], [
                "name"            => "Guinea",
                "phone_area_code" => "224",
                "code"            => "GN",
                "order"           => 81,
            ], [
                "name"            => "Guinea-Bissau",
                "phone_area_code" => "245",
                "code"            => "GW",
                "order"           => 82,
            ], [
                "name"            => "Guyana",
                "phone_area_code" => "595",
                "code"            => "GY",
                "order"           => 83,
            ], [
                "name"            => "Haiti",
                "phone_area_code" => "509",
                "code"            => "HT",
                "order"           => 84,
            ], [
                "name"            => "Honduras",
                "phone_area_code" => "504",
                "code"            => "HN",
                "order"           => 85,
            ], [
                "name"            => "Hungary",
                "phone_area_code" => "36",
                "code"            => "HU",
                "order"           => 86,
            ], [
                "name"            => "Iceland",
                "phone_area_code" => "354",
                "code"            => "IS",
                "order"           => 87,
            ], [
                "name"            => "India",
                "phone_area_code" => "91",
                "code"            => "IN",
                "order"           => 162,
            ], [
                "name"            => "Indonesia",
                "phone_area_code" => "62",
                "code"            => "ID",
                "order"           => 89,
            ], [
                "name"            => "Iraq",
                "phone_area_code" => "964",
                "code"            => "IQ",
                "order"           => 90,
            ], [
                "name"            => "Ireland",
                "phone_area_code" => "353",
                "code"            => "IE",
                "order"           => 91,
            ], [
                "name"            => "Italy",
                "phone_area_code" => "39",
                "code"            => "IT",
                "order"           => 93,
            ], [
                "name"            => "Jamaica",
                "phone_area_code" => "1 876",
                "code"            => "JM",
                "order"           => 94,
            ], [
                "name"            => "Japan",
                "phone_area_code" => "81",
                "code"            => "JP",
                "order"           => 95,
            ], [
                "name"            => "Jordan",
                "phone_area_code" => "962",
                "code"            => "JO",
                "order"           => 96,
            ], [
                "name"            => "Kazakhstan",
                "phone_area_code" => "7 7",
                "code"            => "KZ",
                "order"           => 97,
            ], [
                "name"            => "Kenya",
                "phone_area_code" => "254",
                "code"            => "KE",
                "order"           => 98,
            ], [
                "name"            => "Kiribati",
                "phone_area_code" => "686",
                "code"            => "KI",
                "order"           => 99,
            ], [
                "name"            => "Kuwait",
                "phone_area_code" => "965",
                "code"            => "KW",
                "order"           => 100,
            ], [
                "name"            => "Kyrgyzstan",
                "phone_area_code" => "996",
                "code"            => "KG",
                "order"           => 101,
            ], [
                "name"            => "Latvia",
                "phone_area_code" => "371",
                "code"            => "LV",
                "order"           => 102,
            ], [
                "name"            => "Lebanon",
                "phone_area_code" => "961",
                "code"            => "LB",
                "order"           => 103,
            ], [
                "name"            => "Lesotho",
                "phone_area_code" => "266",
                "code"            => "LS",
                "order"           => 104,
            ], [
                "name"            => "Liberia",
                "phone_area_code" => "231",
                "code"            => "LR",
                "order"           => 105,
            ], [
                "name"            => "Liechtenstein",
                "phone_area_code" => "423",
                "code"            => "LI",
                "order"           => 106,
            ], [
                "name"            => "Lithuania",
                "phone_area_code" => "370",
                "code"            => "LT",
                "order"           => 107,
            ], [
                "name"            => "Luxembourg",
                "phone_area_code" => "352",
                "code"            => "LU",
                "order"           => 108,
            ], [
                "name"            => "Madagascar",
                "phone_area_code" => "261",
                "code"            => "MG",
                "order"           => 109,
            ], [
                "name"            => "Malawi",
                "phone_area_code" => "265",
                "code"            => "MW",
                "order"           => 110,
            ], [
                "name"            => "Malaysia",
                "phone_area_code" => "60",
                "code"            => "MY",
                "order"           => 111,
            ], [
                "name"            => "Maldives",
                "phone_area_code" => "960",
                "code"            => "MV",
                "order"           => 112,
            ], [
                "name"            => "Mali",
                "phone_area_code" => "223",
                "code"            => "ML",
                "order"           => 113,
            ], [
                "name"            => "Malta",
                "phone_area_code" => "356",
                "code"            => "MT",
                "order"           => 114,
            ], [
                "name"            => "Marshall Islands",
                "phone_area_code" => "692",
                "code"            => "MH",
                "order"           => 115,
            ], [
                "name"            => "Martinique",
                "phone_area_code" => "596",
                "code"            => "MQ",
                "order"           => 116,
            ], [
                "name"            => "Mauritania",
                "phone_area_code" => "222",
                "code"            => "MR",
                "order"           => 117,
            ], [
                "name"            => "Mauritius",
                "phone_area_code" => "230",
                "code"            => "MU",
                "order"           => 118,
            ], [
                "name"            => "Mayotte",
                "phone_area_code" => "262",
                "code"            => "YT",
                "order"           => 119,
            ], [
                "name"            => "Mexico",
                "phone_area_code" => "52",
                "code"            => "MX",
                "order"           => 120,
            ], [
                "name"            => "Monaco",
                "phone_area_code" => "377",
                "code"            => "MC",
                "order"           => 121,
            ], [
                "name"            => "Mongolia",
                "phone_area_code" => "976",
                "code"            => "MN",
                "order"           => 122,
            ], [
                "name"            => "Montenegro",
                "phone_area_code" => "382",
                "code"            => "ME",
                "order"           => 123,
            ], [
                "name"            => "Montserrat",
                "phone_area_code" => "1664",
                "code"            => "MS",
                "order"           => 124,
            ], [
                "name"            => "Morocco",
                "phone_area_code" => "212",
                "code"            => "MA",
                "order"           => 125,
            ], [
                "name"            => "Myanmar",
                "phone_area_code" => "95",
                "code"            => "MM",
                "order"           => 126,
            ], [
                "name"            => "Namibia",
                "phone_area_code" => "264",
                "code"            => "NA",
                "order"           => 127,
            ], [
                "name"            => "Nauru",
                "phone_area_code" => "674",
                "code"            => "NR",
                "order"           => 128,
            ], [
                "name"            => "Nepal",
                "phone_area_code" => "977",
                "code"            => "NP",
                "order"           => 129,
            ], [
                "name"            => "Netherlands",
                "phone_area_code" => "31",
                "code"            => "NL",
                "order"           => 130,
            ], [
                "name"            => "Netherlands Antilles",
                "phone_area_code" => "599",
                "code"            => "AN",
                "order"           => 131,
            ], [
                "name"            => "New Caledonia",
                "phone_area_code" => "687",
                "code"            => "NC",
                "order"           => 132,
            ], [
                "name"            => "New Zealand",
                "phone_area_code" => "64",
                "code"            => "NZ",
                "order"           => 133,
            ], [
                "name"            => "Nicaragua",
                "phone_area_code" => "505",
                "code"            => "NI",
                "order"           => 134,
            ], [
                "name"            => "Niger",
                "phone_area_code" => "227",
                "code"            => "NE",
                "order"           => 135,
            ], [
                "name"            => "Nigeria",
                "phone_area_code" => "234",
                "code"            => "NG",
                "order"           => 136,
            ], [
                "name"            => "Niue",
                "phone_area_code" => "683",
                "code"            => "NU",
                "order"           => 137,
            ], [
                "name"            => "Norfolk Island",
                "phone_area_code" => "672",
                "code"            => "NF",
                "order"           => 138,
            ], [
                "name"            => "Northern Mariana Islands",
                "phone_area_code" => "1 670",
                "code"            => "MP",
                "order"           => 139,
            ], [
                "name"            => "Norway",
                "phone_area_code" => "47",
                "code"            => "NO",
                "order"           => 140,
            ], [
                "name"            => "Oman",
                "phone_area_code" => "968",
                "code"            => "OM",
                "order"           => 141,
            ], [
                "name"            => "Pakistan",
                "phone_area_code" => "92",
                "code"            => "PK",
                "order"           => 142,
            ], [
                "name"            => "Palau",
                "phone_area_code" => "680",
                "code"            => "PW",
                "order"           => 143,
            ], [
                "name"            => "Panama",
                "phone_area_code" => "507",
                "code"            => "PA",
                "order"           => 144,
            ], [
                "name"            => "Papua New Guinea",
                "phone_area_code" => "675",
                "code"            => "PG",
                "order"           => 145,
            ], [
                "name"            => "Paraguay",
                "phone_area_code" => "595",
                "code"            => "PY",
                "order"           => 146,
            ], [
                "name"            => "Peru",
                "phone_area_code" => "51",
                "code"            => "PE",
                "order"           => 147,
            ], [
                "name"            => "Philippines",
                "phone_area_code" => "63",
                "code"            => "PH",
                "order"           => 148,
            ], [
                "name"            => "Poland",
                "phone_area_code" => "48",
                "code"            => "PL",
                "order"           => 149,
            ], [
                "name"            => "Portugal",
                "phone_area_code" => "351",
                "code"            => "PT",
                "order"           => 150,
            ], [
                "name"            => "Puerto Rico",
                "phone_area_code" => "1 939",
                "code"            => "PR",
                "order"           => 151,
            ], [
                "name"            => "Qatar",
                "phone_area_code" => "974",
                "code"            => "QA",
                "order"           => 152,
            ], [
                "name"            => "Romania",
                "phone_area_code" => "40",
                "code"            => "RO",
                "order"           => 153,
            ], [
                "name"            => "Rwanda",
                "phone_area_code" => "250",
                "code"            => "RW",
                "order"           => 154,
            ], [
                "name"            => "Samoa",
                "phone_area_code" => "685",
                "code"            => "WS",
                "order"           => 155,
            ], [
                "name"            => "San Marino",
                "phone_area_code" => "378",
                "code"            => "SM",
                "order"           => 156,
            ], [
                "name"            => "Saudi Arabia",
                "phone_area_code" => "966",
                "code"            => "SA",
                "order"           => 157,
            ], [
                "name"            => "Senegal",
                "phone_area_code" => "221",
                "code"            => "SN",
                "order"           => 158,
            ], [
                "name"            => "Serbia",
                "phone_area_code" => "381",
                "code"            => "RS",
                "order"           => 159,
            ], [
                "name"            => "Seychelles",
                "phone_area_code" => "248",
                "code"            => "SC",
                "order"           => 160,
            ], [
                "name"            => "Sierra Leone",
                "phone_area_code" => "232",
                "code"            => "SL",
                "order"           => 161,
            ], [
                "name"            => "Singapore",
                "phone_area_code" => "65",
                "code"            => "SG",
                "order"           => 88,
            ], [
                "name"            => "Slovakia",
                "phone_area_code" => "421",
                "code"            => "SK",
                "order"           => 163,
            ], [
                "name"            => "Slovenia",
                "phone_area_code" => "386",
                "code"            => "SI",
                "order"           => 164,
            ], [
                "name"            => "Solomon Islands",
                "phone_area_code" => "677",
                "code"            => "SB",
                "order"           => 165,
            ], [
                "name"            => "South Africa",
                "phone_area_code" => "27",
                "code"            => "ZA",
                "order"           => 166,
            ], [
                "name"            => "South Georgia and the South Sandwich Islands",
                "phone_area_code" => "500",
                "code"            => "GS",
                "order"           => 167,
            ], [
                "name"            => "Spain",
                "phone_area_code" => "34",
                "code"            => "ES",
                "order"           => 168,
            ], [
                "name"            => "Sri Lanka",
                "phone_area_code" => "94",
                "code"            => "LK",
                "order"           => 169,
            ], [
                "name"            => "Sudan",
                "phone_area_code" => "249",
                "code"            => "SD",
                "order"           => 170,
            ], [
                "name"            => "Suriname",
                "phone_area_code" => "597",
                "code"            => "SR",
                "order"           => 171,
            ], [
                "name"            => "Swaziland",
                "phone_area_code" => "268",
                "code"            => "SZ",
                "order"           => 172,
            ], [
                "name"            => "Sweden",
                "phone_area_code" => "46",
                "code"            => "SE",
                "order"           => 173,
            ], [
                "name"            => "Switzerland",
                "phone_area_code" => "41",
                "code"            => "CH",
                "order"           => 174,
            ], [
                "name"            => "Tajikistan",
                "phone_area_code" => "992",
                "code"            => "TJ",
                "order"           => 175,
            ], [
                "name"            => "Thailand",
                "phone_area_code" => "66",
                "code"            => "TH",
                "order"           => 176,
            ], [
                "name"            => "Togo",
                "phone_area_code" => "228",
                "code"            => "TG",
                "order"           => 177,
            ], [
                "name"            => "Tokelau",
                "phone_area_code" => "690",
                "code"            => "TK",
                "order"           => 178,
            ], [
                "name"            => "Tonga",
                "phone_area_code" => "676",
                "code"            => "TO",
                "order"           => 179,
            ], [
                "name"            => "Trinidad and Tobago",
                "phone_area_code" => "1 868",
                "code"            => "TT",
                "order"           => 180,
            ], [
                "name"            => "Tunisia",
                "phone_area_code" => "216",
                "code"            => "TN",
                "order"           => 181,
            ], [
                "name"            => "Turkey",
                "phone_area_code" => "90",
                "code"            => "TR",
                "order"           => 182,
            ], [
                "name"            => "Turkmenistan",
                "phone_area_code" => "993",
                "code"            => "TM",
                "order"           => 183,
            ], [
                "name"            => "Turks and Caicos Islands",
                "phone_area_code" => "1 649",
                "code"            => "TC",
                "order"           => 184,
            ], [
                "name"            => "Tuvalu",
                "phone_area_code" => "688",
                "code"            => "TV",
                "order"           => 185,
            ], [
                "name"            => "Uganda",
                "phone_area_code" => "256",
                "code"            => "UG",
                "order"           => 186,
            ], [
                "name"            => "Ukraine",
                "phone_area_code" => "380",
                "code"            => "UA",
                "order"           => 187,
            ], [
                "name"            => "United Arab Emirates",
                "phone_area_code" => "971",
                "code"            => "AE",
                "order"           => 188,
            ], [
                "name"            => "United Kingdom",
                "phone_area_code" => "44",
                "code"            => "GB",
                "order"           => 189,
            ], [
                "name"            => "United States",
                "phone_area_code" => "1",
                "code"            => "US",
                "order"           => 190,
            ], [
                "name"            => "Uruguay",
                "phone_area_code" => "598",
                "code"            => "UY",
                "order"           => 191,
            ], [
                "name"            => "Uzbekistan",
                "phone_area_code" => "998",
                "code"            => "UZ",
                "order"           => 192,
            ], [
                "name"            => "Vanuatu",
                "phone_area_code" => "678",
                "code"            => "VU",
                "order"           => 193,
            ], [
                "name"            => "Wallis and Futuna",
                "phone_area_code" => "681",
                "code"            => "WF",
                "order"           => 194,
            ], [
                "name"            => "Yemen",
                "phone_area_code" => "967",
                "code"            => "YE",
                "order"           => 195,
            ], [
                "name"            => "Zambia",
                "phone_area_code" => "260",
                "code"            => "ZM",
                "order"           => 196,
            ], [
                "name"            => "Zimbabwe",
                "phone_area_code" => "263",
                "code"            => "ZW",
                "order"           => 197,
            ], [
                "name"            => "land Islands",
                "phone_area_code" => "",
                "code"            => "AX",
                "order"           => 198,
            ], [
                "name"            => "Antarctica",
                "phone_area_code" => null,
                "code"            => "AQ",
                "order"           => 199,
            ], [
                "name"            => "Bolivia, Plurinational State of",
                "phone_area_code" => "591",
                "code"            => "BO",
                "order"           => 200,
            ], [
                "name"            => "Brunei Darussalam",
                "phone_area_code" => "673",
                "code"            => "BN",
                "order"           => 201,
            ], [
                "name"            => "Cocos (Keeling) Islands",
                "phone_area_code" => "61",
                "code"            => "CC",
                "order"           => 202,
            ], [
                "name"            => "Congo, The Democratic Republic of the",
                "phone_area_code" => "243",
                "code"            => "CD",
                "order"           => 203,
            ], [
                "name"            => "Cote d'Ivoire",
                "phone_area_code" => "225",
                "code"            => "CI",
                "order"           => 204,
            ], [
                "name"            => "Falkland Islands (Malvinas)",
                "phone_area_code" => "500",
                "code"            => "FK",
                "order"           => 205,
            ], [
                "name"            => "Guernsey",
                "phone_area_code" => "44",
                "code"            => "GG",
                "order"           => 206,
            ], [
                "name"            => "Holy See (Vatican City State)",
                "phone_area_code" => "379",
                "code"            => "VA",
                "order"           => 207,
            ], [
                "name"            => "Hong Kong",
                "phone_area_code" => "852",
                "code"            => "HK",
                "order"           => 208,
            ], [
                "name"            => "Iran, Islamic Republic of",
                "phone_area_code" => "98",
                "code"            => "IR",
                "order"           => 209,
            ], [
                "name"            => "Isle of Man",
                "phone_area_code" => "44",
                "code"            => "IM",
                "order"           => 210,
            ], [
                "name"            => "Jersey",
                "phone_area_code" => "44",
                "code"            => "JE",
                "order"           => 211,
            ], [
                "name"            => "Korea, Democratic People's Republic of",
                "phone_area_code" => "850",
                "code"            => "KP",
                "order"           => 212,
            ], [
                "name"            => "Korea, Republic of",
                "phone_area_code" => "82",
                "code"            => "KR",
                "order"           => 213,
            ], [
                "name"            => "Lao People's Democratic Republic",
                "phone_area_code" => "856",
                "code"            => "LA",
                "order"           => 214,
            ], [
                "name"            => "Libyan Arab Jamahiriya",
                "phone_area_code" => "218",
                "code"            => "LY",
                "order"           => 215,
            ], [
                "name"            => "Macao",
                "phone_area_code" => "853",
                "code"            => "MO",
                "order"           => 216,
            ], [
                "name"            => "Macedonia, The Former Yugoslav Republic of",
                "phone_area_code" => "389",
                "code"            => "MK",
                "order"           => 217,
            ], [
                "name"            => "Micronesia, Federated States of",
                "phone_area_code" => "691",
                "code"            => "FM",
                "order"           => 218,
            ], [
                "name"            => "Moldova, Republic of",
                "phone_area_code" => "373",
                "code"            => "MD",
                "order"           => 219,
            ], [
                "name"            => "Mozambique",
                "phone_area_code" => "258",
                "code"            => "MZ",
                "order"           => 220,
            ], [
                "name"            => "Palestinian Territory, Occupied",
                "phone_area_code" => "970",
                "code"            => "PS",
                "order"           => 221,
            ], [
                "name"            => "Pitcairn",
                "phone_area_code" => "872",
                "code"            => "PN",
                "order"           => 222,
            ], [
                "name"            => "R\u00e9union",
                "phone_area_code" => "262",
                "code"            => "RE",
                "order"           => 223,
            ], [
                "name"            => "Russia",
                "phone_area_code" => "7",
                "code"            => "RU",
                "order"           => 224,
            ], [
                "name"            => "Saint Barth\u00e9lemy",
                "phone_area_code" => "590",
                "code"            => "BL",
                "order"           => 225,
            ], [
                "name"            => "Saint Helena, Ascension and Tristan Da Cunha",
                "phone_area_code" => "290",
                "code"            => "SH",
                "order"           => 226,
            ], [
                "name"            => "Saint Kitts and Nevis",
                "phone_area_code" => "1 869",
                "code"            => "KN",
                "order"           => 227,
            ], [
                "name"            => "Saint Lucia",
                "phone_area_code" => "1 758",
                "code"            => "LC",
                "order"           => 228,
            ], [
                "name"            => "Saint Martin",
                "phone_area_code" => "590",
                "code"            => "MF",
                "order"           => 229,
            ], [
                "name"            => "Saint Pierre and Miquelon",
                "phone_area_code" => "508",
                "code"            => "PM",
                "order"           => 230,
            ], [
                "name"            => "Saint Vincent and the Grenadines",
                "phone_area_code" => "1 784",
                "code"            => "VC",
                "order"           => 231,
            ], [
                "name"            => "Sao Tome and Principe",
                "phone_area_code" => "239",
                "code"            => "ST",
                "order"           => 232,
            ], [
                "name"            => "Somalia",
                "phone_area_code" => "252",
                "code"            => "SO",
                "order"           => 233,
            ], [
                "name"            => "Svalbard and Jan Mayen",
                "phone_area_code" => "47",
                "code"            => "SJ",
                "order"           => 234,
            ], [
                "name"            => "Syrian Arab Republic",
                "phone_area_code" => "963",
                "code"            => "SY",
                "order"           => 235,
            ], [
                "name"            => "Taiwan, Province of China",
                "phone_area_code" => "886",
                "code"            => "TW",
                "order"           => 236,
            ], [
                "name"            => "Tanzania, United Republic of",
                "phone_area_code" => "255",
                "code"            => "TZ",
                "order"           => 237,
            ], [
                "name"            => "Timor-Leste",
                "phone_area_code" => "670",
                "code"            => "TL",
                "order"           => 238,
            ], [
                "name"            => "Venezuela, Bolivarian Republic of",
                "phone_area_code" => "58",
                "code"            => "VE",
                "order"           => 239,
            ], [
                "name"            => "Vietnam",
                "phone_area_code" => "84",
                "code"            => "VN",
                "order"           => 2,
            ], [
                "name"            => "Virgin Islands, British",
                "phone_area_code" => "1 284",
                "code"            => "VG",
                "order"           => 241,
            ], [
                "name"            => "Virgin Islands, U.S.",
                "phone_area_code" => "1 340",
                "code"            => "VI",
                "order"           => 242,
            ]];

        $countries_db = Country::all();

        foreach ($countries as $country) {
            $check = $countries_db->search(function ($item) use ($country) {
                return $item->name == $country['name'];
            });
            if ($check === false) {
                if ($country['name'] == 'Singapore') {
                    $country['is_default'] = 1;
                } else {
                    $country['is_default'] = 0;
                }

                if ($country['code'] == 'SG' || $country['code'] == 'VN' || $country['code'] == 'NG') {
                    $country['is_supported_phone_area'] = 1;
                } else {
                    $country['is_supported_phone_area'] = 0;
                }

                if (empty($country['phone_area_code'])) {
                    $country['phone_area_code'] = '';
                }

                $country['slug'] = str_slug($country['name']);
                $i               = 1;
                while (!Country::where('slug', $country['slug'])->get()->isEmpty()) {
                    $country['slug'] = str_slug($country['name']) . '-' . $i++;
                }
                Country::create($country);
            } else {
                $c = Country::where('name', $country['name'])->first();
                if (!empty($country['order'])) {
                    $c->order = $country['order'];
                    $c->save();
                }
            }
        }
    }
}
