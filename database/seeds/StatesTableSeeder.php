<?php

use App\Entities\Country;
use App\Entities\State;
use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $delete_states = State::all();
        if (!$delete_states->isEmpty()) {
            State::destroy($delete_states->pluck('id')->all());
        }

        $country = Country::where('name', 'Nigeria')->first();

        $states = [
            [
                'name' => 'Abia State',
            ],
            [
                'name' => 'Adamawa State',
            ],
            [
                'name' => 'Akwa Ibom State',
            ],
            [
                'name' => 'Anambra State',
            ],
            [
                'name' => 'Bauchi State',
            ],
            [
                'name' => 'Bayelsa State',
            ],
            [
                'name' => 'Benue State',
            ],
            [
                'name' => 'Borno State',
            ],
            [
                'name' => 'Cross River State',
            ],
            [
                'name' => 'Delta State',
            ],
            [
                'name' => 'Ebonyi State',
            ],
            [
                'name' => 'Edo State',
            ],
            [
                'name' => 'Ekiti State',
            ],
            [
                'name' => 'Enugu State',
            ],
            [
                'name' => 'Gombe State',
            ],
            [
                'name' => 'Imo State',
            ],
            [
                'name' => 'Jigawa State',
            ],
            [
                'name' => 'Kaduna State',
            ],
            [
                'name' => 'Kano State',
            ],
            [
                'name' => 'Katsina State',
            ],
            [
                'name' => 'Kebbi State',
            ],
            [
                'name' => 'Kogi State',
            ],
            [
                'name' => 'Kwara State',
            ],
            [
                'name' => 'Lagos State',
            ],
            [
                'name' => 'Nasarawa State',
            ],
            [
                'name' => 'Niger State',
            ],
            [
                'name' => 'Ogun State',
            ],
            [
                'name' => 'Ondo State',
            ],
            [
                'name' => 'Osun State',
            ],
            [
                'name' => 'Oyo State',
            ],
            [
                'name' => 'Plateau State',
            ],
            [
                'name' => 'Rivers State',
            ],
            [
                'name' => 'Sokoto State',
            ],
            [
                'name' => 'Taraba State',
            ],
            [
                'name' => 'Yobe State',
            ],
            [
                'name' => 'Zamfara',
            ],
            [
                'name' => 'FCT',
            ],
        ];

        foreach ($states as $state) {
            $state['country_id'] = $country->id;
            $new_state           = State::updateOrCreate(['name' => $state['name']], $state);
            $new_state->active   = 1;
            $new_state->save();
        }
    }
}
