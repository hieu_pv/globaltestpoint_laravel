<?php

use App\Entities\Slider;
use Illuminate\Database\Seeder;

class SlidersTableSeeder extends Seeder {
	public function run() {
		$homeSliders = [
			'/assets/frontend/images/slider_home/imgpsh_fullsize_2.jpg',
			'/assets/frontend/images/slider_home/imgpsh_fullsize.jpg',
			'/assets/frontend/images/slider_home/sinh-vien-my-reuters_iekn.jpg',
		];

		foreach ($homeSliders as $homeSlider) {
			$slider         = new Slider;
			$slider->page   = 1;
			$slider->sort   = mt_rand(1, 10);
			$slider->image  = $homeSlider;
			$slider->active = 1;
			$slider->save();
		}

		for ($i = 1; $i <= 5; $i++) {
			$slider         = new Slider;
			$slider->page   = mt_rand(2, 3);
			$slider->sort   = ($i);
			$slider->image  = '/uploads/default_post.png';
			$slider->active = mt_rand(0, 1);
			$slider->save();
		}
	}
}
