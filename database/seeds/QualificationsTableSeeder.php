<?php

use App\Entities\Qualification;
use Illuminate\Database\Seeder;

class QualificationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $qualifications = [
            [
                'name' => 'Bsc',
            ],
            [
                'name' => 'MSc',
            ],
        ];

        foreach ($qualifications as $qualification) {
            $new_qualification = Qualification::create([
                'name' => $qualification['name'],
            ], $qualification);
            $new_qualification->status = 1;
            $new_qualification->save();
        }
    }
}
