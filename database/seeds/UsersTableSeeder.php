<?php

use App\Entities\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use NF\Roles\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole    = Role::find(1);
        $userRole     = Role::find(2);
        $testerRole   = Role::find(3);
        $employerRole = Role::find(4);

        $users = [
            'admin'    => [
                'user_type'      => 'student',
                'email'          => 'admin@gmail.com',
                'role'           => 'adminRole',
                'active'         => 1,
                'email_verified' => 1,
            ],
            'chungnd'  => [
                'user_type'      => 'tutor',
                'email'          => 'nguyenchung26011992@gmail.com',
                'role'           => 'userRole',
                'active'         => 1,
                'email_verified' => 1,
            ],
            'tester1'  => [
                'user_type'      => 'student',
                'email'          => 'honglien94.pt@gmail.com',
                'role'           => 'testerRole',
                'active'         => 1,
                'email_verified' => 1,
            ],
            'tester2'  => [
                'user_type'      => 'student',
                'email'          => 'nguyenloi296@gmail.com',
                'role'           => 'testerRole',
                'active'         => 1,
                'email_verified' => 1,
            ],
            'tester3'  => [
                'user_type'      => 'student',
                'email'          => 'dohueatk@gmail.com',
                'role'           => 'testerRole',
                'active'         => 1,
                'email_verified' => 1,
            ],
            'user'     => [
                'user_type'      => 'student',
                'email'          => 'user@gmail.com',
                'role'           => 'userRole',
                'active'         => 1,
                'email_verified' => 1,
            ],
            'employer' => [
                'user_type'      => 'student',
                'email'          => 'employer@globaltestpoint.com',
                'role'           => 'employerRole',
                'active'         => 1,
                'email_verified' => 1,
            ],
        ];
        foreach ($users as $u) {
            $faker = new Faker\Generator();
            $faker->addProvider(new Faker\Provider\en_US\Person($faker));
            $faker->addProvider(new Faker\Provider\en_US\Address($faker));
            $faker->addProvider(new Faker\Provider\en_US\PhoneNumber($faker));
            $faker->addProvider(new Faker\Provider\en_US\Company($faker));
            $faker->addProvider(new Faker\Provider\DateTime($faker));
            $faker->addProvider(new Faker\Provider\Internet($faker));
            $faker->addProvider(new Faker\Provider\Lorem($faker));

            $gender_db = mt_rand(0, 1);
            if ($gender_db == 0) {
                $gender = 'male';
            } else {
                $gender = 'female';
            }

            $user_data = [
                'email'      => $u['email'],
                'gender'     => $gender_db,
                'first_name' => $faker->firstName($gender),
                'last_name'  => $faker->lastName($gender),
                'password'   => Hash::make('secret'),
                'image'      => '/uploads/default_avatar.png',
                'mobile'     => $faker->PhoneNumber(),
                'sumary'     => $faker->text(),
                'birth'      => $faker->dateTimeThisCentury->format('Y-m-d'),
                'address'    => $faker->address(),
                'city'       => $faker->city(),
                'country'    => mt_rand(1, 2),
                'user_type'  => $u['user_type'],
            ];

            $user = User::updateOrCreate([
                'email' => $u['email'],
            ], $user_data);

            $user->active         = $u['active'];
            $user->email_verified = $u['email_verified'];
            $user->status         = 5; // Active
            $user->save();
            $user->attachRole(${$u['role']});
        }
    }

}
