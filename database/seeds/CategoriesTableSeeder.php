<?php

use App\Entities\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder {
	public function run() {

		function addCategory($category) {
			$cate            = new Category;
			$cate->name      = $category['name'];
			$cate->slug      = $category['slug'];
			$cate->type      = $category['type'];
			$cate->parent_id = $category['parent_id'];
			$cate->page_id   = $category['page_id'];
			$cate->active    = 1;
			$order           = isset($category['order']) && $category['order'] != null ? $category['order'] : 10;
			$cate->order     = $order;
			$cate->save();
			if (!empty($category['sub']) && count($category['sub']) > 0) {
				foreach ($category['sub'] as $key => $sub) {
					addCategory($sub, $key);
				}
			}
		}

		$examCategory = [
			[
				'id'        => 1,
				'name'      => 'Academic Test',
				'slug'      => str_slug('Academic Test'),
				'type'      => 'Exam',
				'parent_id' => 0,
				'order'     => 99,
				'page_id'   => 2,
				'sub'       => [
					[
						'id'        => 2,
						'name'      => 'Entrance Tests',
						'slug'      => str_slug('Entrance Tests'),
						'type'      => 'Exam',
						'parent_id' => 1,
						'page_id'   => 2,
					],
					[
						'id'        => 3,
						'name'      => 'University',
						'slug'      => str_slug('University'),
						'type'      => 'Exam',
						'parent_id' => 1,
						'page_id'   => 2,
						'sub'       => [
							[
								'id'        => 4,
								'name'      => 'BSc',
								'slug'      => str_slug('BSc'),
								'type'      => 'Exam',
								'parent_id' => 3,
								'page_id'   => 2,
							],
							[
								'id'        => 5,
								'name'      => 'MSc.',
								'slug'      => str_slug('MSc.'),
								'type'      => 'Exam',
								'parent_id' => 3,
								'page_id'   => 2,
							],
							[
								'id'        => 6,
								'name'      => 'PhD',
								'slug'      => str_slug('PhD'),
								'type'      => 'Exam',
								'parent_id' => 3,
								'page_id'   => 2,
							],
							[
								'id'        => 7,
								'name'      => 'Open Universities',
								'slug'      => str_slug('Open Universities'),
								'type'      => 'Exam',
								'parent_id' => 3,
								'page_id'   => 2,
							],
						],
					],
					[
						'id'        => 8,
						'name'      => 'Polytechnics & Colleges',
						'slug'      => str_slug('polytechnics-colleges'),
						'type'      => 'Exam',
						'parent_id' => 1,
						'page_id'   => 2,
						'sub'       => [
							[
								'id'        => 9,
								'name'      => 'UTEM',
								'slug'      => str_slug('UTEM'),
								'type'      => 'Exam',
								'parent_id' => 8,
								'page_id'   => 2,
							],
							[
								'id'        => 10,
								'name'      => 'JAMB',
								'slug'      => str_slug('JAMB'),
								'type'      => 'Exam',
								'parent_id' => 8,
								'page_id'   => 2,
							],
							[
								'id'        => 11,
								'name'      => 'UTIME',
								'slug'      => str_slug('UTIME'),
								'type'      => 'Exam',
								'parent_id' => 8,
								'page_id'   => 2,
							],
						],
					],
					[
						'id'        => 12,
						'name'      => 'High School',
						'slug'      => str_slug('high-school'),
						'type'      => 'Exam',
						'parent_id' => 1,
						'page_id'   => 2,
						'sub'       => [
							[
								'id'        => 13,
								'name'      => 'GCE',
								'slug'      => str_slug('GCE'),
								'type'      => 'Exam',
								'parent_id' => 12,
								'page_id'   => 2,
							],
							[
								'id'        => 14,
								'name'      => 'SSCE',
								'slug'      => str_slug('SSCE'),
								'type'      => 'Exam',
								'parent_id' => 12,
								'page_id'   => 2,
							],
							[
								'id'        => 15,
								'name'      => 'WAEC',
								'slug'      => str_slug('WAEC'),
								'type'      => 'Exam',
								'parent_id' => 12,
								'page_id'   => 2,
							],
							[
								'id'        => 16,
								'name'      => 'NECO',
								'slug'      => str_slug('NECO'),
								'type'      => 'Exam',
								'parent_id' => 12,
								'page_id'   => 2,
							],
							[
								'id'        => 17,
								'name'      => 'SAT',
								'slug'      => str_slug('SAT'),
								'type'      => 'Exam',
								'parent_id' => 12,
								'page_id'   => 2,
							],
							[
								'id'        => 18,
								'name'      => 'GRE',
								'slug'      => str_slug('GRE'),
								'type'      => 'Exam',
								'parent_id' => 12,
								'page_id'   => 2,
							],
							[
								'id'        => 19,
								'name'      => 'GMAT',
								'slug'      => str_slug('GMAT'),
								'type'      => 'Exam',
								'parent_id' => 12,
								'page_id'   => 2,
							],
							[
								'id'        => 20,
								'name'      => 'TOEFL',
								'slug'      => str_slug('TOEFL'),
								'type'      => 'Exam',
								'parent_id' => 12,
								'page_id'   => 2,
							],
							[
								'id'        => 21,
								'name'      => 'IELTS',
								'slug'      => str_slug('IELTS'),
								'type'      => 'Exam',
								'parent_id' => 12,
								'page_id'   => 2,
							],
							[
								'id'        => 22,
								'name'      => 'JSS',
								'slug'      => str_slug('JSS'),
								'type'      => 'Exam',
								'parent_id' => 12,
								'page_id'   => 2,
							],
						],
					],
					[
						'id'        => 23,
						'name'      => 'Primary School',
						'slug'      => str_slug('primary-school'),
						'type'      => 'Exam',
						'parent_id' => 6,
						'page_id'   => 2,
						'sub'       => [
							[
								'id'        => 24,
								'name'      => 'Grade 9',
								'slug'      => str_slug('Grade-9'),
								'type'      => 'Exam',
								'parent_id' => 23,
								'page_id'   => 2,
							],
							[
								'id'        => 25,
								'name'      => 'Grade 8',
								'slug'      => str_slug('Grade-8'),
								'type'      => 'Exam',
								'parent_id' => 23,
								'page_id'   => 2,
							],
							[
								'id'        => 26,
								'name'      => 'Grade 7',
								'slug'      => str_slug('Grade-7'),
								'type'      => 'Exam',
								'parent_id' => 23,
								'page_id'   => 2,
							],
							[
								'id'        => 27,
								'name'      => 'Grade 6',
								'slug'      => str_slug('Grade-6'),
								'type'      => 'Exam',
								'parent_id' => 23,
								'page_id'   => 2,
							],
							[
								'id'        => 28,
								'name'      => 'Grade 5',
								'slug'      => str_slug('Grade-5'),
								'type'      => 'Exam',
								'parent_id' => 23,
								'page_id'   => 2,
							],
							[
								'id'        => 29,
								'name'      => 'Grade 4',
								'slug'      => str_slug('Grade-4'),
								'type'      => 'Exam',
								'parent_id' => 23,
								'page_id'   => 2,
							],
						],
					],
					[
						'id'        => 30,
						'name'      => 'Distance Learning',
						'slug'      => str_slug('Distance-Learning'),
						'type'      => 'Exam',
						'parent_id' => 6,
						'page_id'   => 2,
						'sub'       => [
							[
								'id'        => 31,
								'name'      => 'Adult Education',
								'slug'      => str_slug('Adult-Education'),
								'type'      => 'Exam',
								'parent_id' => 30,
								'page_id'   => 2,
							],
							[
								'id'        => 32,
								'name'      => 'Mass Literacy',
								'slug'      => str_slug('Mass Literacy'),
								'type'      => 'Exam',
								'parent_id' => 30,
								'page_id'   => 2,
							],
						],
					],
				],
			],
			[
				'id'        => 33,
				'name'      => 'Professional Test',
				'slug'      => str_slug('protessional-test'),
				'type'      => 'Exam',
				'order'     => 98,
				'parent_id' => 0,
				'page_id'   => 2,
				'sub'       => [
					[
						'id'        => 34,
						'name'      => 'IEEE',
						'slug'      => str_slug('ieee'),
						'type'      => 'Exam',
						'parent_id' => 33,
						'page_id'   => 2,
					],
					[
						'id'        => 35,
						'name'      => 'ACA',
						'slug'      => str_slug('aca'),
						'type'      => 'Exam',
						'parent_id' => 33,
						'page_id'   => 2,
					],
					[
						'id'        => 36,
						'name'      => 'AACE',
						'slug'      => str_slug('aace'),
						'type'      => 'Exam',
						'parent_id' => 33,
						'page_id'   => 2,
					],
					[
						'id'        => 37,
						'name'      => 'PMP',
						'slug'      => str_slug('pmp'),
						'type'      => 'Exam',
						'parent_id' => 33,
						'page_id'   => 2,
					],
					[
						'id'        => 38,
						'name'      => 'NSE',
						'slug'      => str_slug('nse'),
						'type'      => 'Exam',
						'parent_id' => 33,
						'page_id'   => 2,
					],
					[
						'id'        => 39,
						'name'      => 'COREN',
						'slug'      => str_slug('coren'),
						'type'      => 'Exam',
						'parent_id' => 33,
						'page_id'   => 2,
					],
					[
						'id'        => 40,
						'name'      => 'ICAN',
						'slug'      => str_slug('ican'),
						'type'      => 'Exam',
						'parent_id' => 33,
						'page_id'   => 2,
					],
					[
						'id'        => 41,
						'name'      => 'City & Gills',
						'slug'      => str_slug('city-gills'),
						'type'      => 'Exam',
						'parent_id' => 33,
						'page_id'   => 2,
					],
					[
						'id'        => 42,
						'name'      => 'CICMA',
						'slug'      => str_slug('cicma'),
						'type'      => 'Exam',
						'parent_id' => 33,
						'page_id'   => 2,
					],
					[
						'id'        => 43,
						'name'      => 'CIMA',
						'slug'      => str_slug('cima'),
						'type'      => 'Exam',
						'parent_id' => 33,
						'page_id'   => 2,
					],
					[
						'id'        => 44,
						'name'      => 'NIESV',
						'slug'      => str_slug('niesv'),
						'type'      => 'Exam',
						'parent_id' => 33,
						'page_id'   => 2,
					],
					[
						'id'        => 45,
						'name'      => 'MDCAN',
						'slug'      => str_slug('mdcan'),
						'type'      => 'Exam',
						'parent_id' => 33,
						'page_id'   => 2,
					],
					[
						'id'        => 46,
						'name'      => 'NIQS',
						'slug'      => str_slug('niqs'),
						'type'      => 'Exam',
						'parent_id' => 33,
						'page_id'   => 2,
					],
					[
						'id'        => 47,
						'name'      => 'CIA',
						'slug'      => str_slug('cia'),
						'type'      => 'Exam',
						'parent_id' => 33,
						'page_id'   => 2,
					],
					[
						'id'        => 48,
						'name'      => 'CCNA',
						'slug'      => str_slug('ccna'),
						'type'      => 'Exam',
						'parent_id' => 33,
						'page_id'   => 2,
					],
					[
						'id'        => 49,
						'name'      => 'MSCE',
						'slug'      => str_slug('msce'),
						'type'      => 'Exam',
						'parent_id' => 33,
						'page_id'   => 2,
					],
					[
						'id'        => 50,
						'name'      => 'Etc.',
						'slug'      => str_slug('etc.'),
						'type'      => 'Exam',
						'parent_id' => 33,
						'page_id'   => 2,
					],
				],
			],
			[
				'id'        => 51,
				'name'      => 'Job Aptitude & Interview',
				'slug'      => str_slug('job-aptitude-interview'),
				'type'      => 'Exam',
				'order'     => 97,
				'parent_id' => 0,
				'page_id'   => 2,
				'sub'       => [
					[
						'id'        => 52,
						'name'      => 'Banking',
						'slug'      => str_slug('banking'),
						'type'      => 'Exam',
						'parent_id' => 51,
						'page_id'   => 2,
					],
					[
						'id'        => 53,
						'name'      => 'Oil and Gas',
						'slug'      => str_slug('oil-and-gas'),
						'type'      => 'Exam',
						'parent_id' => 51,
						'page_id'   => 2,
					],
					[
						'id'        => 54,
						'name'      => 'IT',
						'slug'      => str_slug('it'),
						'type'      => 'Exam',
						'parent_id' => 51,
						'page_id'   => 2,
					],
					[
						'id'        => 55,
						'name'      => 'Telecommunication',
						'slug'      => str_slug('telecommunication'),
						'type'      => 'Exam',
						'parent_id' => 51,
						'page_id'   => 2,
					],
					[
						'id'        => 56,
						'name'      => 'Armed Forces',
						'slug'      => str_slug('armed-forces'),
						'type'      => 'Exam',
						'parent_id' => 51,
						'page_id'   => 2,
					],
					[
						'id'        => 57,
						'name'      => 'Insurance',
						'slug'      => str_slug('insurance'),
						'type'      => 'Exam',
						'parent_id' => 51,
						'page_id'   => 2,
					],
					[
						'id'        => 58,
						'name'      => 'Para millitary',
						'slug'      => str_slug('para-millitary'),
						'type'      => 'Exam',
						'parent_id' => 51,
						'page_id'   => 2,
					],
					[
						'id'        => 59,
						'name'      => 'Medical',
						'slug'      => str_slug('medical'),
						'type'      => 'Exam',
						'parent_id' => 51,
						'page_id'   => 2,
					],
					[
						'id'        => 60,
						'name'      => 'Consulting',
						'slug'      => str_slug('consulting'),
						'type'      => 'Exam',
						'parent_id' => 51,
						'page_id'   => 2,
					],
					[
						'id'        => 61,
						'name'      => 'Public Service',
						'slug'      => str_slug('public-service'),
						'type'      => 'Exam',
						'parent_id' => 51,
						'page_id'   => 2,
					],
				],
			],
			[
				'id'        => 62,
				'name'      => 'IQ/Personality Test',
				'slug'      => str_slug('iq-personal-test'),
				'type'      => 'Exam',
				'order'     => 96,
				'parent_id' => 0,
				'page_id'   => 2,
			],
		];

		$blankPageCategory = [
			[
				'id'        => 63,
				'name'      => 'About Us',
				'slug'      => str_slug('About Us'),
				'type'      => 'blank-page',
				'order'     => 100,
				'parent_id' => 0,
				'page_id'   => 2,
			],
			[
				'id'        => 64,
				'name'      => 'No category',
				'slug'      => str_slug('No category'),
				'type'      => 'blank-page',
				'parent_id' => 0,
				'page_id'   => 1,
			],
			[
				'id'        => 65,
				'name'      => 'IT and Computing',
				'slug'      => str_slug('IT-and-Computing'),
				'type'      => 'blank-page',
				'parent_id' => 0,
				'page_id'   => 3,
				'sub'       => [],
			],
			[
				'id'        => 66,
				'name'      => 'Energy',
				'slug'      => str_slug('Energy'),
				'type'      => 'blank-page',
				'parent_id' => 0,
				'page_id'   => 3,
				'sub'       => [],
			],
			[
				'id'        => 67,
				'name'      => 'Engineering',
				'slug'      => str_slug('Engineering'),
				'type'      => 'blank-page',
				'parent_id' => 0,
				'page_id'   => 3,
				'sub'       => [],
			],
			[
				'id'        => 68,
				'name'      => 'Business',
				'slug'      => str_slug('Business'),
				'type'      => 'blank-page',
				'parent_id' => 0,
				'page_id'   => 3,
				'sub'       => [],
			],
			[
				'id'        => 69,
				'name'      => 'Agriculture',
				'slug'      => str_slug('Agriculture'),
				'type'      => 'blank-page',
				'parent_id' => 0,
				'page_id'   => 3,
				'sub'       => [],
			],
			[
				'id'        => 70,
				'name'      => 'Job Interview',
				'slug'      => str_slug('Job Interview'),
				'type'      => 'blank-page',
				'order'     => 95,
				'parent_id' => 0,
				'page_id'   => 2,
				'sub'       => [],
			],
		];

		$documentCategory = [
			[
				'id'        => 71,
				'name'      => 'Downloads',
				'slug'      => str_slug('Downloads'),
				'type'      => 'document',
				'parent_id' => 0,
				'order'     => 9,
				'page_id'   => 2,
				'sub'       => [
					[
						'id'        => 72,
						'name'      => 'Offline CBT',
						'slug'      => str_slug('Offline CBT'),
						'type'      => 'document',
						'parent_id' => 71,
						'order'     => 8,
						'page_id'   => 2,
					],
					[
						'id'        => 73,
						'name'      => 'Ebook on Agric',
						'slug'      => str_slug('Ebook on Agric'),
						'type'      => 'document',
						'parent_id' => 71,
						'order'     => 7,
						'page_id'   => 2,
					],
					[
						'id'        => 74,
						'name'      => 'Ebooks on',
						'slug'      => str_slug('Ebooks on'),
						'type'      => 'document',
						'parent_id' => 71,
						'order'     => 6,
						'page_id'   => 2,
					],
					[
						'id'        => 75,
						'name'      => 'Engineering',
						'slug'      => str_slug('Engineering-1'),
						'type'      => 'document',
						'parent_id' => 71,
						'order'     => 5,
						'page_id'   => 2,
					],
					[
						'id'        => 76,
						'name'      => 'Popular Dowloads',
						'slug'      => str_slug('Popular Dowloads'),
						'type'      => 'document',
						'parent_id' => 71,
						'order'     => 4,
						'page_id'   => 2,
					],
				],
			],
		];
		foreach ($examCategory as $category) {
			addCategory($category);
		}
		foreach ($blankPageCategory as $category) {
			addCategory($category);
		}
		foreach ($documentCategory as $category) {
			addCategory($category);
		}
	}
}
