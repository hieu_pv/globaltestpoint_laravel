<?php

use Illuminate\Database\Seeder;
use NF\Roles\Models\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Employer
        Permission::updateOrCreate([
            'slug' => 'view.companies',
        ], [
            'name'        => 'View Companies',
            'slug'        => 'view.companies',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'create.companies',
        ], [
            'name'        => 'Create Companies',
            'slug'        => 'create.companies',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'update.companies',
        ], [
            'name'        => 'Update Companies',
            'slug'        => 'update.companies',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'delete.companies',
        ], [
            'name' => 'Delete Companies',
            'slug' => 'delete.companies',
        ]);

        // User
        Permission::updateOrCreate([
            'slug' => 'view.users',
        ], [
            'name'        => 'View Users',
            'slug'        => 'view.users',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'create.users',
        ], [
            'name'        => 'Create Users',
            'slug'        => 'create.users',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'update.users',
        ], [
            'name'        => 'Update Users',
            'slug'        => 'update.users',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'delete.users',
        ], [
            'name' => 'Delete Users',
            'slug' => 'delete.users',
        ]);

        // Industry
        Permission::updateOrCreate([
            'slug' => 'view.industries',
        ], [
            'name'        => 'View Industries',
            'slug'        => 'view.industries',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'create.industries',
        ], [
            'name'        => 'Create Industries',
            'slug'        => 'create.industries',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'update.industries',
        ], [
            'name'        => 'Update Industries',
            'slug'        => 'update.industries',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'delete.industries',
        ], [
            'name' => 'Delete Industries',
            'slug' => 'delete.industries',
        ]);

        // Job
        Permission::updateOrCreate([
            'slug' => 'view.jobs',
        ], [
            'name'        => 'View Jobs',
            'slug'        => 'view.jobs',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'create.jobs',
        ], [
            'name'        => 'Create Jobs',
            'slug'        => 'create.jobs',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'update.jobs',
        ], [
            'name'        => 'Update Jobs',
            'slug'        => 'update.jobs',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'delete.jobs',
        ], [
            'name' => 'Delete Jobs',
            'slug' => 'delete.jobs',
        ]);

        // Job Title
        Permission::updateOrCreate([
            'slug' => 'view.job.titles',
        ], [
            'name'        => 'View Job Titles',
            'slug'        => 'view.job.titles',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'create.job.titles',
        ], [
            'name'        => 'Create Job Titles',
            'slug'        => 'create.job.titles',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'update.job.titles',
        ], [
            'name'        => 'Update Job Titles',
            'slug'        => 'update.job.titles',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'delete.job.titles',
        ], [
            'name' => 'Delete Job Titles',
            'slug' => 'delete.job.titles',
        ]);

        // Advertisement
        Permission::updateOrCreate([
            'slug' => 'view.advertisements',
        ], [
            'name'        => 'View Advertisements',
            'slug'        => 'view.advertisements',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'create.advertisements',
        ], [
            'name'        => 'Create Advertisements',
            'slug'        => 'create.advertisements',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'update.advertisements',
        ], [
            'name'        => 'Update Advertisements',
            'slug'        => 'update.advertisements',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'delete.advertisements',
        ], [
            'name' => 'Delete Advertisements',
            'slug' => 'delete.advertisements',
        ]);

        // Tags
        Permission::updateOrCreate([
            'slug' => 'view.tags',
        ], [
            'name'        => 'View Tags',
            'slug'        => 'view.tags',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'create.tags',
        ], [
            'name'        => 'Create Tags',
            'slug'        => 'create.tags',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'update.tags',
        ], [
            'name'        => 'Update Tags',
            'slug'        => 'update.tags',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'delete.tags',
        ], [
            'name' => 'Delete Tags',
            'slug' => 'delete.tags',
        ]);

        // Nationalities
        Permission::updateOrCreate([
            'slug' => 'view.nationalities',
        ], [
            'name'        => 'View Nationalities',
            'slug'        => 'view.nationalities',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'create.nationalities',
        ], [
            'name'        => 'Create Nationalities',
            'slug'        => 'create.nationalities',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'update.nationalities',
        ], [
            'name'        => 'Update Nationalities',
            'slug'        => 'update.nationalities',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'delete.nationalities',
        ], [
            'name' => 'Delete Nationalities',
            'slug' => 'delete.nationalities',
        ]);

        // States
        Permission::updateOrCreate([
            'slug' => 'view.states',
        ], [
            'name'        => 'View States',
            'slug'        => 'view.states',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'create.states',
        ], [
            'name'        => 'Create States',
            'slug'        => 'create.states',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'update.states',
        ], [
            'name'        => 'Update States',
            'slug'        => 'update.states',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'delete.states',
        ], [
            'name' => 'Delete States',
            'slug' => 'delete.states',
        ]);

        // Cities
        Permission::updateOrCreate([
            'slug' => 'view.cities',
        ], [
            'name'        => 'View Cities',
            'slug'        => 'view.cities',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'create.cities',
        ], [
            'name'        => 'Create Cities',
            'slug'        => 'create.cities',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'update.cities',
        ], [
            'name'        => 'Update Cities',
            'slug'        => 'update.cities',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'delete.cities',
        ], [
            'name' => 'Delete Cities',
            'slug' => 'delete.cities',
        ]);

        // Experiences
        Permission::updateOrCreate([
            'slug' => 'view.experiences',
        ], [
            'name'        => 'View Experiences',
            'slug'        => 'view.experiences',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'create.experiences',
        ], [
            'name'        => 'Create Experiences',
            'slug'        => 'create.experiences',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'update.experiences',
        ], [
            'name'        => 'Update Experiences',
            'slug'        => 'update.experiences',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'delete.experiences',
        ], [
            'name' => 'Delete Experiences',
            'slug' => 'delete.experiences',
        ]);

        // Qualifications
        Permission::updateOrCreate([
            'slug' => 'view.qualifications',
        ], [
            'name'        => 'View Qualifications',
            'slug'        => 'view.qualifications',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'create.qualifications',
        ], [
            'name'        => 'Create Qualifications',
            'slug'        => 'create.qualifications',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'update.qualifications',
        ], [
            'name'        => 'Update Qualifications',
            'slug'        => 'update.qualifications',
            'description' => '',
        ]);

        Permission::updateOrCreate([
            'slug' => 'delete.qualifications',
        ], [
            'name' => 'Delete Qualifications',
            'slug' => 'delete.qualifications',
        ]);
    }
}
