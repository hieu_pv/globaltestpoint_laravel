<?php

use Illuminate\Database\Seeder;
use NF\Roles\Models\Permission;
use NF\Roles\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = Permission::all();

        $admin = Role::updateOrCreate([
            'name' => 'Admin',
            'slug' => 'admin',
        ]);

        $admin->permissions()->sync($permissions->pluck('id'));

        $user = Role::updateOrCreate([
            'name' => 'User',
            'slug' => 'user',
        ]);

        $tester = Role::updateOrCreate([
            'name' => 'Tester',
            'slug' => 'tester',
        ]);

        $employer = Role::updateOrCreate([
            'name' => 'Employer',
            'slug' => 'employer',
        ]);

    }
}
