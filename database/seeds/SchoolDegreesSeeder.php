<?php

use App\Entities\SchoolDegree;
use Illuminate\Database\Seeder;

class SchoolDegreesSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		$schoolDegrees = [
			[
				'name' => 'University',
			],
			[
				'name' => 'College',
			],
		];

		foreach ($schoolDegrees as $item) {
			$schoolDegree       = new SchoolDegree;
			$schoolDegree->name = $item['name'];
			$schoolDegree->save();
		}
	}
}
