<?php

use App\Entities\Link;
use App\Entities\Post;
use Illuminate\Database\Seeder;

class UpdateLinksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = new Faker\Generator();
        $faker->addProvider(new Faker\Provider\en_US\Person($faker));
        $faker->addProvider(new Faker\Provider\DateTime($faker));
        $faker->addProvider(new Faker\Provider\Internet($faker));
        $faker->addProvider(new Faker\Provider\Lorem($faker));

        $blank_homepage = [
            [
                'title'   => 'School Ranking',
                'slug'    => str_slug('School Ranking'),
                'content' => $faker->text(),
                'author'  => 1,
                'type'    => 'blankpage',
                'order'   => 1000,
            ],
        ];

        foreach ($blank_homepage as $blank) {
            $post         = Post::updateOrCreate(['title' => $blank['title']], $blank);
            $post->active = 1;
            $post->save();

            if ($post->link->isEmpty()) {
                $link                 = new Link;
                $link->leftlink       = $faker->url();
                $link->rightlink      = $faker->url();
                $link->sublink        = $faker->url();
                $link->position       = "homepage";
                $link->order          = 0;
                $link->post_id        = $post->id;
                $link->parent_cate_id = 0;
                $link->page           = 2;
                $link->image          = '/uploads/default_post.png';
                $link->image_cover    = '/uploads/default_cover_post.jpg';
                $link->active         = 1;
                $link->save();
            }
        }
    }
}
