<?php

use App\Entities\Link;
use App\Entities\Post;
use Illuminate\Database\Seeder;

class LinksTableSeeder extends Seeder {
	public function run() {
		$parent_id = [51, 65, 68, 67, 69];
		$title     = [
			[
				'title'   => 'Web Design',
				'cate_id' => 65,
			],
			[
				'title'   => 'Website Hosting',
				'cate_id' => 65,
			],
			[
				'title'   => 'App Design',
				'cate_id' => 65,
			],
			[
				'title'   => 'Online Maketing',
				'cate_id' => 65,
			],
			[
				'title'   => 'Online',
				'cate_id' => 65,
			],
			[
				'title'   => 'Advertisement',
				'cate_id' => 65,
			],
			[
				'title'   => 'Consulting',
				'cate_id' => 65,
			],
			[
				'title'   => 'Oil and Gas',
				'cate_id' => 51,
			],
			[
				'title'   => 'Power',
				'cate_id' => 51,
			],
			[
				'title'   => 'Transportation',
				'cate_id' => 51,
			],
			[
				'title'   => 'Constructioins',
				'cate_id' => 67,
			],
			[
				'title'   => 'Electrical',
				'cate_id' => 67,
			],
			[
				'title'   => 'Mechanical',
				'cate_id' => 67,
			],
			[
				'title'   => 'Chemical',
				'cate_id' => 67,
			],
			[
				'title'   => 'Industrial',
				'cate_id' => 67,
			],
			[
				'title'   => 'Environment',
				'cate_id' => 67,
			],
			[
				'title'   => 'Cost Engineering',
				'cate_id' => 67,
			],
			[
				'title'   => 'Oil and Gas 2',
				'cate_id' => 68,
			],
			[
				'title'   => 'Solid Minerals',
				'cate_id' => 68,
			],
			[
				'title'   => 'Power 2',
				'cate_id' => 68,
			],
			[
				'title'   => 'Consultancy',
				'cate_id' => 68,
			],
			[
				'title'   => 'Online 2',
				'cate_id' => 68,
			],
			[
				'title'   => 'Opportunities',
				'cate_id' => 68,
			],
			[
				'title'   => 'Catering',
				'cate_id' => 68,
			],
			[
				'title'   => 'Security services',
				'cate_id' => 68,
			],
			[
				'title'   => 'Fish Farming',
				'cate_id' => 69,
			],
			[
				'title'   => 'Poultry',
				'cate_id' => 69,
			],
			[
				'title'   => 'Grass Cutter',
				'cate_id' => 69,
			],
			[
				'title'   => 'Snail',
				'cate_id' => 69,
			],
			[
				'title'   => 'Piggery',
				'cate_id' => 69,
			],
			[
				'title'   => 'Husbandry',
				'cate_id' => 69,
			],
			[
				'title'   => 'Gardening',
				'cate_id' => 69,
			],
			[
				'title'   => 'Dry Season',
				'cate_id' => 69,
			],
			[
				'title'   => 'Farming',
				'cate_id' => 69,
			],
			[
				'title'   => 'Irrigation',
				'cate_id' => 69,
			],
			[
				'title'   => 'Etc 2',
				'cate_id' => 69,
			],
		];
		$aboutus   = [
			[
				'title'   => 'Aim',
				'cate_id' => 63
			],
			[
				'title'   => 'Mission',
				'cate_id' => 63
			],
			[
				'title'   => 'Vision',
				'cate_id' => 63
			],
			[
				'title'   => 'Managerment Team',
				'cate_id' => 63
			]
		];
		foreach ($aboutus as $keyabout => $about) {
			$faker = new Faker\Generator();
			$faker->addProvider(new Faker\Provider\en_US\Person($faker));
			$faker->addProvider(new Faker\Provider\DateTime($faker));
			$faker->addProvider(new Faker\Provider\Internet($faker));
			$faker->addProvider(new Faker\Provider\Lorem($faker));
			$post          = new Post;
			$post->title   = $about['title'];
			$post->content = $faker->text();
			$post->slug    = str_slug($post->title);
			$post->active  = 1;
			$post->author  = 1;
			$post->type    = 'blankpage';
			$post->save();
			$post->categories()->attach($about['cate_id']);
			$link                 = new Link;
			$link->leftlink       = $faker->url();
			$link->rightlink      = $faker->url();
			$link->sublink        = $faker->url();
			$link->position       = "post";
			$link->order          = 0;
			$link->post_id        = $post->id;
			$link->parent_cate_id = $about['cate_id'];
			$link->page           = mt_rand(2,3);
			$link->image          = '/uploads/default_post.png';
			$link->image_cover    = '/uploads/default_cover_post.jpg';
			$link->active         = 1;
			$link->save();
		}
		foreach ($title as $key => $val) {
			$faker = new Faker\Generator();
			$faker->addProvider(new Faker\Provider\en_US\Person($faker));
			$faker->addProvider(new Faker\Provider\DateTime($faker));
			$faker->addProvider(new Faker\Provider\Internet($faker));
			$faker->addProvider(new Faker\Provider\Lorem($faker));
			$post          = new Post;
			$post->title   = $val['title'];
			$post->content = $faker->text();
			$post->slug    = str_slug($post->title);
			$post->active  = 1;
			$post->author  = 1;
			$post->type    = 'blankpage';
			$post->save();
			$post->categories()->attach($val['cate_id']);
			$link                 = new Link;
			$link->leftlink       = $faker->url();
			$link->rightlink      = $faker->url();
			$link->sublink        = $faker->url();
			$link->position       = "post";
			$link->order          = 0;
			$link->post_id        = $post->id;
			$link->parent_cate_id = $val['cate_id'];
			$link->page           = mt_rand(2,3);
			$link->image          = '/uploads/default_post.png';
			$link->image_cover    = '/uploads/default_cover_post.jpg';
			$link->active         = 1;
			$link->save();
		}
		$blankhomepage = [
			['title' => 'Job vacancies'],
			['title' => 'Scholarships'],
			['title' => 'Bulk SMS Services'],
			['title' => 'Data Services'],
			['title' => 'Website design'],
			['title' => 'Web Hosting'],
			['title' => 'IT Consultancy'],
			['title' => 'Marketing and Strategy'],
		];
		foreach ($blankhomepage as $blank) {
			$faker = new Faker\Generator();
			$faker->addProvider(new Faker\Provider\en_US\Person($faker));
			$faker->addProvider(new Faker\Provider\DateTime($faker));
			$faker->addProvider(new Faker\Provider\Internet($faker));
			$faker->addProvider(new Faker\Provider\Lorem($faker));
			$post          = new Post;
			$post->title   = $blank['title'];
			$post->content = $faker->text();
			$post->slug    = str_slug($post->title);
			$post->active  = 1;
			$post->author  = 1;
			$post->type    = 'blankpage';
			$post->save();
			$link                 = new Link;
			$link->leftlink       = $faker->url();
			$link->rightlink      = $faker->url();
			$link->sublink        = $faker->url();
			$link->position       = "homepage";
			$link->order          = 0;
			$link->post_id        = $post->id;
			$link->parent_cate_id = 0;
			$link->page           = 2;
			$link->image          = '/uploads/default_post.png';
			$link->image_cover    = '/uploads/default_cover_post.jpg';
			$link->active         = 1;
			$link->save();
		}
	}
}
