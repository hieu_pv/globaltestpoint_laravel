<?php

use App\Entities\Industry;
use App\Entities\Job;
use App\Entities\Tag;
use App\Entities\User;
use Illuminate\Database\Seeder;

class JobsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = new Faker\Generator();
        $faker->addProvider(new Faker\Provider\en_US\Person($faker));
        $faker->addProvider(new Faker\Provider\en_SG\Address($faker));
        $faker->addProvider(new Faker\Provider\en_US\PhoneNumber($faker));
        $faker->addProvider(new Faker\Provider\en_US\Company($faker));
        $faker->addProvider(new Faker\Provider\Lorem($faker));
        $faker->addProvider(new Faker\Provider\DateTime($faker));
        $faker->addProvider(new Faker\Provider\Internet($faker));

        if (env('APP_ENV') != 'production') {
            $employer = User::where('email', 'employer@globaltestpoint.com')->first();

            $tags       = Tag::where('active', 1)->get();
            $industries = Industry::where('status', 1)->get();

            $description = '';
            foreach ($faker->paragraphs() as $paragraph) {
                $description .= $paragraph;
            }

            for ($i = 0; $i < 100; $i++) {
                $job_title  = $faker->sentence();
                $slug       = str_slug($job_title);
                $slug_index = 1;
                while (!Job::where('slug', $slug)->get()->isEmpty()) {
                    $slug = $slug . '-' . $slug_index;
                    $slug_index++;
                }

                $job = [
                    'employer_id'               => $employer->id,
                    'title'                     => $job_title,
                    'slug'                      => $slug,
                    'image'                     => '/uploads/default_job_image.jpg',
                    'address'                   => $faker->address(),
                    'min_salary'                => 0,
                    'max_salary'                => 5000,
                    'certificate'               => 'Certificate',
                    'gender'                    => mt_rand(0, 1),
                    'experience_id'             => mt_rand(1, 5),
                    'city_id'                   => mt_rand(1, 37),
                    'country_id'                => mt_rand(1, 20),
                    'nationality_id'            => mt_rand(1, 5),
                    'job_title_id'              => mt_rand(1, 7),
                    'qualification_id'          => mt_rand(1, 2),
                    'short_desc'                => $faker->paragraph(),
                    'description'               => $description,
                    'desired_candidate_profile' => $faker->paragraph(),
                    'type'                      => '1',
                ];
                $new_job           = Job::create($job);
                $new_job->active   = 1;
                $new_job->complete = 0;
                $new_job->save();

                $new_job->tags()->sync($tags->pluck('id')->all());
                $new_job->industries()->sync($industries->pluck('id')->all());
            }

            // National job
            for ($i = 0; $i < 10; $i++) {
                $job_title  = $faker->sentence();
                $slug       = str_slug($job_title);
                $slug_index = 1;
                while (!Job::where('slug', $slug)->get()->isEmpty()) {
                    $slug = $slug . '-' . $slug_index;
                    $slug_index++;
                }

                $job = [
                    'employer_id'               => $employer->id,
                    'title'                     => $job_title,
                    'slug'                      => $slug,
                    'image'                     => '/uploads/default_job_image.jpg',
                    'address'                   => $faker->address(),
                    'min_salary'                => 0,
                    'max_salary'                => 5000,
                    'certificate'               => 'Certificate',
                    'gender'                    => mt_rand(0, 1),
                    'experience_id'             => mt_rand(1, 5),
                    'nationality_id'            => mt_rand(1, 5),
                    'job_title_id'              => mt_rand(1, 2),
                    'qualification_id'          => mt_rand(1, 2),
                    'short_desc'                => $faker->paragraph(),
                    'description'               => $description,
                    'desired_candidate_profile' => $faker->paragraph(),
                    'type'                      => '2',
                    'location'                  => $faker->address(),
                ];
                $new_job           = Job::create($job);
                $new_job->active   = 1;
                $new_job->complete = 0;
                $new_job->save();

                $new_job->tags()->sync($tags->pluck('id')->all());
                $new_job->industries()->sync($industries->pluck('id')->all());
            }
        }
    }
}
