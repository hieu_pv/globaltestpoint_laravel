<?php

use App\Entities\Advertisement;
use Illuminate\Database\Seeder;

class AdvertisementsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $advertisements = [
            [
                'name'     => 'Google',
                'url'      => 'https://www.google.com.vn',
                'position' => 1,
            ],
            [
                'name'     => 'Youtube',
                'url'      => 'https://www.youtube.com/',
                'position' => 1,
            ],
            [
                'name'     => 'BBC',
                'url'      => 'http://www.bbc.com/news',
                'position' => 2,
            ],
            [
                'name'     => 'Facebook',
                'url'      => 'https://www.facebook.com/',
                'position' => 2,
            ],
        ];

        foreach ($advertisements as $advertisement) {
            $advertisement['slug']     = str_slug($advertisement['name']);
            $advertisement['image']    = url('uploads/default_advertisement.jpg');
            $new_advertisement         = Advertisement::create($advertisement);
            $new_advertisement->status = 1;
            $new_advertisement->save();
        }
    }
}
