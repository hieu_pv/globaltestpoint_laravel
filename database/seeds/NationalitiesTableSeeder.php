<?php

use App\Entities\Nationality;
use Illuminate\Database\Seeder;

class NationalitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $nationalities = [
            [
                'name' => 'Indian, Any Nationality',
            ],
            [
                'name' => 'Any Nationality',
            ],
            [
                'name' => 'Any Arabic National, Any European National, Indian, Filipino',
            ],
            [
                'name' => 'Indian, Filipino',
            ],
            [
                'name' => 'Russian, Libyan, Any European National',
            ],
        ];

        foreach ($nationalities as $nationality) {
            $new_nationality = Nationality::create([
                'name' => $nationality['name'],
            ], $nationality);
            $new_nationality->status = 1;
            $new_nationality->save();
        }
    }
}
