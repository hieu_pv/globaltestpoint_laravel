<?php

use App\Entities\Category;
use Illuminate\Database\Seeder;

class UpdateFooterLinkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $footer_area_1 = [
            [
                'name'     => 'Job Vacancies',
                'position' => 1,
                'page_id'  => 2,
                'type'     => 'blank-page',
            ],
            [
                'name'     => 'Job Interview Tips',
                'position' => 1,
                'page_id'  => 2,
                'type'     => 'blank-page',
            ],
            [
                'name'     => 'CV - Free Review',
                'position' => 1,
                'page_id'  => 2,
                'type'     => 'blank-page',
            ],
            [
                'name'     => 'Job Aptitude Tests',
                'position' => 1,
                'page_id'  => 2,
                'type'     => 'blank-page',
            ],
            [
                'name'     => 'Scholarships',
                'position' => 1,
                'page_id'  => 2,
                'type'     => 'blank-page',
            ],
        ];

        $footer_area_2 = [
            [
                'name'     => 'Business Plans',
                'position' => 2,
                'page_id'  => 2,
                'type'     => 'blank-page',
            ],
            [
                'name'     => 'Visas',
                'position' => 2,
                'page_id'  => 2,
                'type'     => 'blank-page',
            ],
            [
                'name'     => 'Travel Advise',
                'position' => 2,
                'page_id'  => 2,
                'type'     => 'blank-page',
            ],
            [
                'name'     => 'Marketing and Strategy',
                'position' => 2,
                'page_id'  => 2,
                'type'     => 'blank-page',
            ],
            [
                'name'     => 'Consultancy Services',
                'position' => 2,
                'page_id'  => 2,
                'type'     => 'blank-page',
            ],
        ];

        $footer_area_3 = [
            [
                'name'     => 'E-Books',
                'position' => 3,
                'page_id'  => 2,
                'type'     => 'blank-page',
            ],
            [
                'name'     => 'Softwares',
                'position' => 3,
                'page_id'  => 2,
                'type'     => 'blank-page',
            ],
            [
                'name'     => 'Business Proposals',
                'position' => 3,
                'page_id'  => 2,
                'type'     => 'blank-page',
            ],
            [
                'name'     => 'News and Information',
                'position' => 3,
                'page_id'  => 2,
                'type'     => 'blank-page',
            ],
            [
                'name'     => 'Career Advice',
                'position' => 3,
                'page_id'  => 2,
                'type'     => 'blank-page',
            ],
        ];

        foreach ($footer_area_1 as $item) {
            $matching_item = [
                'name'     => $item['name'],
                'position' => $item['position'],
                'type'     => $item['type'],
                'page_id'  => $item['page_id'], // testyourself
            ];
            Category::updateOrCreate($matching_item, $item);
        }

        foreach ($footer_area_2 as $item) {
            $matching_item = [
                'name'     => $item['name'],
                'position' => $item['position'],
                'type'     => $item['type'],
                'page_id'  => $item['page_id'],
            ];
            Category::updateOrCreate($matching_item, $item);
        }

        foreach ($footer_area_3 as $item) {
            $matching_item = [
                'name'     => $item['name'],
                'position' => $item['position'],
                'type'     => $item['type'],
                'page_id'  => $item['page_id'], // testyourself
            ];
            Category::updateOrCreate($matching_item, $item);
        }
    }
}
