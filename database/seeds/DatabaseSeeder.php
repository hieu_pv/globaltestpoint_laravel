<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PermissionsTableSeeder::class);
        $this->call(ConfigsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(EmployerTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        // $this->call(PostsTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(StatesTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(SlidersTableSeeder::class);
        $this->call(SchoolTypesSeeder::class);
        $this->call(SchoolDegreesSeeder::class);
        $this->call(SchoolsSeeder::class);
        $this->call(LinksTableSeeder::class);
        $this->call(TypeTableSeeder::class);
        $this->call(OptionTableSeeder::class);
        $this->call(AdsTableSeeder::class);
        $this->call(ExamsTableSeeder::class);
        $this->call(QuestionsTableSeeder::class);
        $this->call(DocumentsTableSeeder::class);
        $this->call(MailletterTableSeeder::class);
        $this->call(IndustriesTableSeeder::class);
        $this->call(UsersStatusTableSeeder::class);
        $this->call(TagsTableSeeder::class);
        $this->call(NationalitiesTableSeeder::class);
        $this->call(ExperiencesTableSeeder::class);
        $this->call(QualificationsTableSeeder::class);
        $this->call(JobTitlesTableSeeder::class);
        // $this->call(JobsTableSeeder::class);
        $this->call(AdvertisementsTableSeeder::class);
    }
}
