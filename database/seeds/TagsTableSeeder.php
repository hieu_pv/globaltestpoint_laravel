<?php

use App\Entities\Tag;
use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tags = [
            [
                'name' => 'Retail Sales',
            ],
            [
                'name' => 'Retail Sales Executive',
            ],
            [
                'name' => 'Sales Executive',
            ],
        ];

        foreach ($tags as $tag) {
            $tag['slug'] = str_slug($tag['name']);
            $i           = 1;
            while (!Tag::where('slug', $tag['slug'])->get()->isEmpty()) {
                $tag['slug'] = str_slug($tag['name']) . '-' . $i++;
            }

            $new_tag = Tag::updateOrCreate([
                'name' => $tag['name'],
            ], $tag);
            $new_tag->active = 1;
            $new_tag->save();
        }
    }
}
