$(document).ready(function() {
	//==== for show form update ===
	$('#formUpdatePaystack').css('display','none');
    $('.clicktoupdate').click(function(){
        $('#formUpdatePaystack').css('display','block');
    });
    // == for click button cancel update ==
    $('#cancelUpdate').click(function(){
    	console.log('ok');
    	$('#formUpdatePaystack').css('display','none');
    });

	// for update infomation paystack 
	$formpaystack = $('#formpaystack');
	$('.spinload').css('display', 'none');
	$('#updateCustomerAjax').click(function(){
		var url = baseURL + '/testyourself/update-card-multi';
		var _token = $('._token').val();
		var first_name = $('#first_name_p').val();
		var last_name = $('#last_name_p').val();
		var email = $('#email_p').val();
		var phone = $('#phone_p').val();
		$.ajax({
			url:url,
			type: "POST",
			data: {
				_token: _token,
				first_name: first_name,
				last_name: last_name,
				email: email,
				phone: phone
			},
			beforeSend: function() {
				$('.spinload').css('display', 'block');
			},
			complete: function() {
				$('.spinload').css('display', 'none');
			},
			success: function(response) {
				$('#formUpdatePaystack').css('display', 'none');
				$('.message-paystack').removeClass('hide');
				$('.alert-paystack').css('display', 'block');
				var res = jQuery.parseJSON(response);
				if(res.code === 1) {
					$('.alert-paystack').addClass('alert-success').css('display', 'block');
				} else {
					$('.alert-paystack').addClass('alert-danger').css('display', 'block');
				}
				if(res.data !== null) {
					if(res.data.first_name !== null) {
						$('.f_name_p').text(res.data.first_name);
					}
					if(res.data.last_name !== null) {
						$('.l_name_p').text(res.data.last_name);
					}
					if(res.data.email !== null) {
						$('.email_p').text(res.data.email);
					}
					if(res.data.phone !== null) {
						$('.phone_p').text(res.data.phone);
					}
				}
				$('.alert-paystack').text(res.message);
			}
		});
	});
});