$(document).ready(function() {
    // Stripe.setPublishableKey('pk_test_24OgPTzdlZNa8hG75ZJ082Eo');
    Stripe.setPublishableKey(STRIPE_KEY);
    var $form = $('#payment-form');
    $form.submit(function(event) {
        var typepayment = 'Stripe';
        if ($('.inp-stripe').prop('checked')) {
            typepayment = $('.inp-stripe').val();
        }
        if ($('.inp-paystack').prop('checked')) {
            typepayment = $('.inp-paystack').val();
        }
        if (typepayment === 'Stripe') {
            // Disable the submit button to prevent repeated clicks:
            $form.find('.submit').prop('disabled', true);
            // Request a token from Stripe:
            Stripe.card.createToken($form, stripeResponseHandler);
            // Prevent the form from being submitted:
        } else {
            $form.submit();
        }
        return false;
    });

    function stripeResponseHandler(status, response) {
        // Grab the form:
        var $form = $('#payment-form');
        if (response.error) { // Problem!
            // Show the errors on the form:
            $form.find('.stripe-errors').removeClass('hide');
            $form.find('.payment-errors').text(response.error.message).show();
            $form.find('.submit').prop('disabled', false); // Re-enable submission
        } else { // Token was created!
            // Get the token ID:
            var token = response.id;
            // Insert the token ID into the form so it gets submitted to the server:
            $form.append($('<input type="hidden" name="stripeToken">').val(token));
            // Submit the form:
            $form.get(0).submit();
        }
    }
});
