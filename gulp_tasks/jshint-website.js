var gulp = require('gulp');
var jshint = require('gulp-jshint');
module.exports = function() {
    return gulp.src([
            './resources/assets/js/**/*.js',
            './resources/assets/angular/**/*.js'
        ])
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
}