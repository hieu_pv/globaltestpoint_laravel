var gulp = require('gulp');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var concat = require("gulp-concat");

module.exports = function() {
    return gulp.src([
            './webapp/bower_components/bootstrap/dist/css/bootstrap.min.css',
            './webapp/bower_components/bootstrap/dist/css/bootstrap-theme.min.css',
            './webapp/bower_components/font-awesome/css/font-awesome.min.css',
            './webapp/bower_components/datatables/media/css/dataTables.bootstrap.min.css',
            './webapp/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css',
            './webapp/bower_components/bootstrap-fileinput/css/fileinput.css',
            './webapp/bower_components/angular-dialog-service/dist/dialogs.css',
            './webapp/bower_components/ng-notify/dist/ng-notify.min.css',
            './webapp/bower_components/select2/dist/css/select2.css',
            './webapp/bower_components/owl.carousel1/owl-carousel/owl.carousel.css',
            './webapp/bower_components/owl.carousel1/owl-carousel/owl.theme.css',
            './webapp/bower_components/owl.carousel1/owl-carousel/owl.transitions.css',
        ])
        .pipe(cssmin())
        .pipe(rename({ suffix: '.min' }))
        .pipe(concat("libraries.min.css"))
        .pipe(gulp.dest('public/assets/css'));
};
