var gulp = require('gulp');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var concat = require("gulp-concat");
var bust = require('gulp-buster');
module.exports = [
    ['concat-admin'],
    function() {
        gulp.src([
                './public/assets/css/admin.all.css'
            ])
            .pipe(cssmin())
            .pipe(rename({
                suffix: '.min'
            }))
            .pipe(concat("admin.all.min.css"))
            .pipe(gulp.dest('public/assets/css'))
            .pipe(bust({relativePath: 'public'}))
            .pipe(gulp.dest('.'));
    }
];
