var gulp = require('gulp');
var minify = require('gulp-minify');
var rename = require('gulp-rename');
var concat = require("gulp-concat");

module.exports = function() {
    gulp.src([
            './webapp/bower_components/jquery/dist/jquery.min.js',
            './webapp/bower_components/bootstrap/dist/js/bootstrap.min.js',
            './webapp/bower_components/bootstrap-fileinput/js/fileinput.min.js',
            './webapp/bower_components/datatables/media/js/jquery.dataTables.min.js',
            './webapp/bower_components/datatables/media/js/dataTables.bootstrap.min.js',
            './webapp/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
            './webapp/bower_components/select2/dist/js/select2.full.js',
            './webapp/bower_components/iCheck/icheck.min.js',
            './webapp/bower_components/lodash/dist/lodash.js',
            './webapp/bower_components/owl.carousel1/owl-carousel/owl.carousel.js',
            './public/assets/js/paystack.js',
            './public/assets/js/countdown.js',
            './resources/assets/js/**/*.js',
        ])
        .pipe(concat("website.js"))
        .pipe(gulp.dest('./public/assets/js/'))
}
