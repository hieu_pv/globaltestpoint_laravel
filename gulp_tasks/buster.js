var gulp = require('gulp');
var bust = require('gulp-buster');

module.exports = function() {
    return gulp.src(["./public/assets/js/app.js",
            "./public/assets/js/admin.js",
            "./public/assets/css/app.all.css",
            "./public/assets/css/admin.all.css"
        ])
        .pipe(bust({
            relativePath: 'public'
        }))
        .pipe(gulp.dest('.'));
}
