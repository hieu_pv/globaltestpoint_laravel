var gulp = require('gulp');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var concat = require("gulp-concat");
module.exports = [
    ['sass-admin'],
    function() {
        gulp.src([
                './webapp/bower_components/bootstrap/dist/css/bootstrap.min.css',
                './webapp/bower_components/bootstrap/dist/css/bootstrap-theme.min.css',
                './webapp/bower_components/font-awesome/css/font-awesome.css',
                './webapp/bower_components/angular-fx/dist/angular-fx.min.css',
                './webapp/bower_components/dropzone/dist/min/dropzone.min.css',
                './webapp/bower_components/angular-xeditable/dist/css/xeditable.css',
                './webapp/bower_components/trix/dist/trix.css',
                './webapp/bower_components/angular-notify/dist/angular-notify.min.css',
                './webapp/bower_components/select2/dist/css/select2.css',
                './webapp/bower_components/angular-ui-select/dist/select.css',
                './webapp/bower_components/trix/dist/trix.css',
                './webapp/bower_components/pickadate/lib/themes/classic.css',
                './webapp/bower_components/pickadate/lib/themes/classic.date.css',
                './webapp/bower_components/pickadate/lib/themes/classic.time.css',
                './public/assets/css/admin.css',
            ])
            .pipe(concat("admin.all.css"))
            .pipe(gulp.dest('public/assets/css'));
    }
];
