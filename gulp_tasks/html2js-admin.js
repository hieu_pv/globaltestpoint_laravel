var gulp = require('gulp');
var ngHtml2Js = require("gulp-ng-html2js");
var concat = require("gulp-concat");
module.exports = function() {
    return gulp.src("webapp/js/admin/**/*.tpl.html")
        .pipe(ngHtml2Js({
            moduleName: "app.template",
            prefix: "/webapp/js/admin/"
        }))
        .pipe(concat("templates.js"))
        .pipe(gulp.dest("./webapp/js/admin"));
}
