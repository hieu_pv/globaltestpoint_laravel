var gulp = require('gulp');
var ngHtml2Js = require("gulp-ng-html2js");
var concat = require("gulp-concat");
module.exports = function() {
    return gulp.src("webapp/js/app/**/*.tpl.html")
        .pipe(ngHtml2Js({
            moduleName: "app.template",
            prefix: "/webapp/js/app/"
        }))
        .pipe(concat("templates.js"))
        .pipe(gulp.dest("./webapp/js/app"));
}
