var gulp = require('gulp');
var jshint = require('gulp-jshint');
module.exports = function() {
    return gulp.src(['./webapp/js/admin/**/*.js', '!./webapp/js/admin/template.js'])
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
};
