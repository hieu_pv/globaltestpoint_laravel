var gulp = require('gulp');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var concat = require("gulp-concat");
module.exports = [
    ['sass'],
    function() {
        gulp.src([
                './webapp/bower_components/bootstrap/dist/css/bootstrap.css',
                './webapp/bower_components/font-awesome/css/font-awesome.css',
                './webapp/bower_components/dropzone/dist/min/dropzone.min.css',
                './webapp/bower_components/animate.css/animate.min.css',
                './webapp/bower_components/trix/dist/trix.css',
                './webapp/bower_components/pickadate/lib/themes/classic.css',
                './webapp/bower_components/pickadate/lib/themes/classic.date.css',
                './webapp/bower_components/pickadate/lib/themes/classic.time.css',
                './webapp/bower_components/angular-input-stars-directive/angular-input-stars.css',
                './webapp/bower_components/nya-bootstrap-select/dist/css/nya-bs-select.css',
                './webapp/bower_components/angular-ui-select/dist/select.css',
                './webapp/bower_components/cropperjs/dist/cropper.min.css',
                './webapp/bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css',
                './public/assets/css/app.css',
            ])
            .pipe(concat("app.all.css"))
            .pipe(gulp.dest('public/assets/css'));
    }
];
