var gulp = require('gulp');
var jshint = require('gulp-jshint');
module.exports = function() {
    return gulp.src(['./webapp/js/app/**/*.js', '!./webapp/js/app/template.js'])
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
};
