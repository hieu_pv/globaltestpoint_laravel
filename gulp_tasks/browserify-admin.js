var gulp = require('gulp');
var browserify = require('gulp-browserify');
var uglify = require('gulp-uglify');
module.exports = function() {
    return gulp.src('./webapp/js/admin/admin.js')
        .pipe(browserify({
            presets: ['es2015']
        }))
        .pipe(gulp.dest('public/assets/js'));
};
