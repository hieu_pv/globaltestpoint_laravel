var gulp = require('gulp');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var bust = require('gulp-buster');
module.exports = [
    ['browserify-admin-babel'],
    function() {
        return gulp.src('./public/assets/js/admin.js')
            .pipe(uglify({
                mangle: false
            }))
            .pipe(rename({
                extname: ".min.js"
            }))
            .pipe(gulp.dest('public/assets/js'))
            .pipe(bust({relativePath: 'public'}))
            .pipe(gulp.dest('.'));
    }
];
