var gulp = require('gulp');
var sass = require('gulp-sass');
var sassGlob = require('gulp-sass-glob');
var rename = require('gulp-rename');

module.exports = function() {
    return gulp.src('./resources/assets/sass/frontend/app.scss')
        .pipe(sassGlob())
        .pipe(sass().on('error', sass.logError))
        .pipe(rename("website.css"))
        .pipe(gulp.dest('./public/assets/css'));
}