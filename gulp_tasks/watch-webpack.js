var gulp = require('gulp');
var watch = require('gulp-watch');

module.exports = function() {
    watch('./resources/assets/js/**/*.js', function() {
        gulp.run([
            'jshint-website',
            'minify-js-website'
        ]);
    });
    watch('./resources/assets/angular/**/*.js', function() {
        gulp.run([
            'jshint-website',
            'browserify-website'
        ]);
    });
    watch('./public/assets/js/paystack.js', function() {
        gulp.run([
            'jshint-website',
            'browserify-website'
        ]);
    });
    watch(['webapp/js/app/**/*.js', 'webapp/js/api/**/*.js', 'webapp/js/models/**/*.js'], function() {
        gulp.run([
            'jshint',
            'webpack-app'
        ]);
    });
    watch(['webapp/js/admin/**/*.js', 'webapp/js/api/**/*.js', 'webapp/js/models/**/*.js'], function() {
        gulp.run([
            'jshint-admin',
            'webpack-admin'
        ]);
    });

    watch('webapp/js/app/**/*.tpl.html', function() {
        gulp.run([
            'html2js',
        ]);
    });
    watch('webapp/js/admin/**/*.tpl.html', function() {
        gulp.run([
            'html2js-admin',
        ]);
    });

    gulp.watch('./resources/assets/sass/frontend/**/*.scss', ['sass-website']);
    gulp.watch('./resources/assets/sass/backend/**/*.scss', ['sass-backend']);
    gulp.watch('./webapp/css/scss/app/**/*.scss', ['concat']);
    gulp.watch('./webapp/css/scss/admin/**/*.scss', ['concat-admin']);
}
