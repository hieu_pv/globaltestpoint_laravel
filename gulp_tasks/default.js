module.exports = [
    [
        'sass-website',
        'sass-backend',

        'minify-css-website',
        'minify-js-website',

        'jshint',
        'jshint-website',
        'jshint-admin',

        'html2js',
        'html2js-admin',

        'browserify-website',

        'concat-admin',
        'concat'
    ]
];