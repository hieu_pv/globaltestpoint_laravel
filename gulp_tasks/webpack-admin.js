var gulp = require('gulp');
var gulpwebpack = require('gulp-webpack');
var browserify = require('gulp-browserify');
var uglify = require('gulp-uglify');

var path = require('path');
var webpack = require('webpack');

var data = require('../package.json');

var browser = data.browser;
var shimming = swap(data['browserify-shim']);


for (var k in browser) {
    if (k == 'moment' || k == 'moment-timezone' || k == 'lodash') {
        delete(browser[k]);
    } else {
        browser[k] = path.resolve(browser[k]);
    }

}


function swap(json) {
    var ret = {};
    for (var key in json) {
        ret[json[key]] = key;
    }
    return ret;
}


module.exports = function() {
    return gulp.src('./webapp/js/app/app.js')
        .pipe(gulpwebpack({
            entry: {
                admin: "./webapp/js/admin/admin.js",
            },
            output: {
                path: path.resolve(__dirname, "public/assets/js"),
                filename: "[name].js"
            },
            resolve: {
                alias: browser,
                extensions: ['', '.json', '.jsx', '.js']
            },
            plugins: [
                new webpack.ProvidePlugin({
                    $: "jquery",
                    _: "lodash",
                    jQuery: "jquery",
                    "window.jQuery": "jquery",
                    moment: "moment",
                    bootstrap: "bootstrap",
                    twix: "twix",
                    select2: "select2"
                }),
            ],
            module: {
                rules: [{
                    test: /\.json$/,
                    loaders: ["json-loader"]
                }, {
                    test: /\.js$/,
                    exclude: /(node_modules|bower_components)/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['es2015']
                        }
                    }
                }]
            }
        }))
        .pipe(gulp.dest('public/assets/js'));
};
