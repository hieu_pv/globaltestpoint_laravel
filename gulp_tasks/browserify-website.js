var gulp = require('gulp');
var browserify = require('gulp-browserify');
var rename = require("gulp-rename");
module.exports = function() {
    return gulp.src('./resources/assets/angular/app.js')
        .pipe(browserify({
            insertGlobals: true
        }))
        .pipe(rename('angular.js'))
        .pipe(gulp.dest('public/assets/js'));
}
