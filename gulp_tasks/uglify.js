var gulp = require('gulp');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var bust = require('gulp-buster');

module.exports = [
    ['browserify-babel'],
    function() {
        return gulp.src('./public/assets/js/app.js')
            .pipe(uglify({
                mangle: false
            }))
            .pipe(rename({
                extname: ".min.js"
            }))
            .pipe(gulp.dest('public/assets/js'))
            .pipe(bust({relativePath: 'public'}))
            .pipe(gulp.dest('.'));
    }
];
