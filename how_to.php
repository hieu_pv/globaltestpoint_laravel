<?php include('includes/header.php'); ?>
<div class="header_test_yourself">
    <div class="container_site">
        <div class="col-md-12">
            <div class="logo_register">
                <div class="col-md-6 col-sm-6 col-xs-6 logo_test">
                    <a href="index.php">
                        <div class="logo" style="background: url('assets/images/testyourself/logo.png') no-repeat;">
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 register_col">
                    <div class="register">
                        <p><a href="#" class="member_name">Helo! Guest</a> | <a href="#" class="login">Login</a> | <a href="#" class="sign_up">Sign Up</a></p>
                    </div>
                </div>
            </div>
            <nav class="navbar navbar-default navbar-static-top">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand dropdown" href="#">Home</a>
                    </div>
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav">
                            <li class="dropdown"><a href="#">Academic Test</a></li>
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Professional Test <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Page 1-1</a></li>
                                    <li><a href="#">Page 1-2</a></li>
                                    <li><a href="#">Page 1-3</a></li>
                                </ul>
                            </li>
                            <li class="dropdown"><a href="#">Job aptitude Test</a></li>
                            <li class="dropdown"><a href="#">Non academic</a></li>
                            <li class="dropdown"><a href="#">Career</a></li>
                            <li class="dropdown"><a href="#">Schools</a></li>
                            <li class="dropdown"><a href="#">Other</a></li>
                        </ul>
                        <!-- <ul class="nav navbar-nav navbar-right">
                        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                    </ul> -->
                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>
<div class="feeture_test_yourself">
    <div class="container_site">
        <div class="col-md-9 col-sm-9 col-xs-12">
            <div class="slider_test_yourself">
                slider
            </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="menu_test_yourself_right">
                <ul>
                    <li><a href="">Important Exam Dates JAMB, WAEC, TOE</a></li>
                    <li><a href="">How to register for JAMB, SET ect.</a></li>
                    <li><a href="">How to pass your Exams</a></li>
                    <li><a href="">Download pass Question papers</a></li>
                    <li><a href="">Guide to Scholarships</a></li>
                    <li><a href="">Making the best out of NYSC</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="content_testyourself">
    <div class="container_site">
        <div class="free_share">
            <p>We have <span class="number">35,000+</span> Free Practice Questions in Over <span class="subject">20 Subjects</span> that will Make you Pass your tests With Flying Colours ...</p>
        </div>
        <div class="flash_video">
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="list_step item1">
                    <div class="img" style="background-image: url('assets/images/flash_video/b4eimg201505291700505615.jpg');">
                        <img src="assets/images/blank-image.png" alt="">
                    </div>
                    <div class="title"><a href="">Click here to register</a></div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="list_step item2">
                    <div class="img" style="background-image: url('assets/images/flash_video/8HBaMaiKieuLien_d73f1.jpg');">
                        <img src="assets/images/blank-image.png" alt="">
                    </div>
                    <div class="title"><a href="">Click here to login</a></div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="list_step item3">
                    <div class="img" style="background-image: url('assets/images/flash_video/samsungvina-co-tong-giam-doc-moi-1.jpg');">
                        <img src="assets/images/blank-image.png" alt="">
                    </div>
                    <div class="title"><a href="">Click here to login/register</a></div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="list_step item1">
                    <div class="img3" style="">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/9jTo6hTZmiQ" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="list_step item2">
                    <div class="img2" style="background-image: url('assets/images/flash_video/phaohoa.gif');">
                        <img src="assets/images/blank-image.png" alt="">
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="list_step item3">
                    <div class="img2" style="background-image: url('assets/images/flash_video/phaohoa.gif');">
                        <img src="assets/images/blank-image.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="big_black_title">
                <h2>A great place <span class="for_learning">for learning</span></h2>
                <p>Weac, Jamb, NECO, Post UME, ICAN, NABTEB, Job Listing, Job Aptitude Test,</p>
                <p>Schools, Scholarships, Career Advise etc.</p>
            </div>
        </div>
    </div>
</div>
<div class="footer_test_yourself">
    <div class="container_site">
        <div class="col-md-12">
            <div class="container-fluid">
                <h4 class="hits"><a href="">124 hits</a></h4>
                <h4 class="contactUs"><a href="">Contact Us</a></h4>
                <div class="footer_icon">
                    <div class="row bottom_contact_us_section">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col-sm-6">
                                        <figure>
                                            <img src="assets/images/contact_ico.png" alt="contact">
                                        </figure>
                                        <a href="mailto:contact@demo.org">contact@demo.org</a>
                                    </div>
                                    <div class="col-sm-6">
                                        <figure>
                                            <img src="assets/images/mob_ico.png" alt="mobile">
                                        </figure>
                                        Tele Phone - <a href="tel:+91 9803 009 332">+91 9803 009 332</a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="col-sm-6">
                                        <figure>
                                            <img src="assets/images/addr_ico.png" alt="address">
                                        </figure>
                                        Kolkata, 26/B R.D Rd
                                    </div>
                                    <div class="col-sm-6">
                                        <figure>
                                            <img src="assets/images/fax_ico.png" alt="fax">
                                        </figure>
                                        FAX: +1 800 889 9898
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer_test">
                <div class="col-md-6 col-sm-6 col-xs-12 footer_left">
                    <p>© <a href="">www.testinghub.com </a> All Rights Reserved.</p>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 footer_right">
                    <p>Stay connect with <a href=""><i class="fa fa-facebook-official icon_fb" aria-hidden="true"></i> <i class="fa fa-twitter icon_twitter" aria-hidden="true"></i></a></p>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('includes/footer.php'); ?>
