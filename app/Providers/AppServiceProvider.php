<?php

namespace App\Providers;

use App\Entities\Advertisement;
use App\Entities\Category;
use App\Entities\Job;
use App\Entities\Option;
use App\Entities\SchoolDegree;
use App\Entities\User;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Request $request)
    {
        View::composer('*', function ($view) {
            $buster = (array) json_decode(file_get_contents(base_path('busters.json')));
            $view->with('buster', $buster);
        });

        Relation::morphMap([
            'users'          => User::class,
            'jobs'           => Job::class,
            'advertisements' => Advertisement::class,
        ]);

        if (!app()->runningInConsole()) {
            if (!$request->is('api/*') && !$request->is('testyourself/blank-page/job-vacancies/*') && !$request->is('assets/*')) {
                $img_logo = Option::first();

                $cate_footer1 = Category::where('position', 1)->where('active', 1)->where('parent_id', 0)->where('page_id', 2)->orderBy('order')->get();
                $cate_footer2 = Category::where('position', 2)->where('active', 1)->where('parent_id', 0)->where('page_id', 2)->orderBy('order')->get();
                $cate_footer3 = Category::where('position', 3)->where('active', 1)->where('parent_id', 0)->where('page_id', 2)->orderBy('order')->get();

                $categories = Category::where('page_id', 2)->where('active', 1)->orderBy('order', 'desc')->orderBy('id', 'desc')->get();
                if (!$categories->isEmpty()) {

                    // get Category has child and no child
                    $categoryHasChild = array();
                    foreach ($categories as $key => $category) {
                        if ($category['type'] != 'blank-page') {
                            if ($category['parent_id'] != 0) {
                                if (!in_array($category['parent_id'], $categoryHasChild)) {
                                    $categoryHasChild[] = $category['parent_id'];
                                }
                            }
                        } elseif ($category['type'] == 'blank-page') {
                            if (!$category->posts->isEmpty()) {
                                if (!in_array($category['parent_id'], $categoryHasChild)) {
                                    $categoryHasChild[] = $category['id'];
                                }
                            }
                        }
                    }
                    foreach ($categories as $key => $category) {
                        if (in_array($category['id'], $categoryHasChild)) {
                            $categories[$key]['hasChild'] = true;
                        } else {
                            $categories[$key]['hasChild'] = false;
                        }
                    }
                    $category_parents = $categories->filter(function ($item) {
                        return $item->parent_id == 0;
                    });
                    View()->share('category_parents', $category_parents);
                    View()->share('cate_footer1', $cate_footer1);
                    View()->share('cate_footer2', $cate_footer2);
                    View()->share('cate_footer3', $cate_footer3);
                    // end get Category child
                }

                $school_degrees = SchoolDegree::where('active', 1)->orderBy('order', 'desc')->orderBy('id', 'desc')->get();
                View()->share('school_degrees', $school_degrees);
                View()->share('categories', $categories);
                View()->share('img_logo', $img_logo);
            }
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\App\Repositories\UserRepository::class, \App\Repositories\UserRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CategoriesRepository::class, \App\Repositories\CategoriesRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ExamRepository::class, \App\Repositories\ExamRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ExamResultRepository::class, \App\Repositories\ExamResultRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CountryRepository::class, \App\Repositories\CountryRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\StateRepository::class, \App\Repositories\StateRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\SchoolTypeRepository::class, \App\Repositories\SchoolTypeRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\SchoolDegreeRepository::class, \App\Repositories\SchoolDegreeRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\SchoolRepository::class, \App\Repositories\SchoolRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\DocumentRepository::class, \App\Repositories\DocumentRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\FaqRepository::class, \App\Repositories\FaqRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PaymentRepository::class, \App\Repositories\PayStackRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\IndustryRepository::class, \App\Repositories\IndustryRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\JobTitleRepository::class, \App\Repositories\JobTitleRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\AdvertisementRepository::class, \App\Repositories\AdvertisementRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\JobRepository::class, \App\Repositories\JobRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\UserStatusRepository::class, \App\Repositories\UserStatusRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\TagRepository::class, \App\Repositories\TagRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\NationalityRepository::class, \App\Repositories\NationalityRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CityRepository::class, \App\Repositories\CityRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ExperienceRepository::class, \App\Repositories\ExperienceRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\QualificationRepository::class, \App\Repositories\QualificationRepositoryEloquent::class);
    }
}
