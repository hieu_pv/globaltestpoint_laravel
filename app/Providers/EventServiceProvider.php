<?php

namespace App\Providers;

use App\Events\AcceptNewPartner;
use App\Events\Admin\AdminChangeStatusEmployer;
use App\Events\Admin\AdminCreateUser;
use App\Events\Admin\AdminDeleteUser;
use App\Events\Admin\AdminUpdateUser;
use App\Events\CancelPartner;
use App\Events\FileUpload;
use App\Events\JobCreated;
use App\Events\JobDeleted;
use App\Events\JobUpdated;
use App\Events\UpdateTotalJobsFieldEvent;
use App\Events\UserBecomePartner;
use App\Listeners\AcceptNewPartnerListener;
use App\Listeners\CancelPartnerListener;
use App\Listeners\UpdateTotalJobsFieldListener;
use App\Listeners\UserBecomePartnerListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        AcceptNewPartner::class          => [
            AcceptNewPartnerListener::class,
        ],
        UserBecomePartner::class         => [
            UserBecomePartnerListener::class,
        ],
        CancelPartner::class             => [
            CancelPartnerListener::class,
        ],
        FileUpload::class                => [
        ],
        AdminCreateUser::class           => [
        ],
        AdminUpdateUser::class           => [
        ],
        AdminDeleteUser::class           => [
        ],
        AdminChangeStatusEmployer::class => [
        ],
        JobCreated::class                => [
        ],
        JobUpdated::class                => [
        ],
        UpdateTotalJobsFieldEvent::class => [
            UpdateTotalJobsFieldListener::class,
        ],
        JobDeleted::class                => [
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}
