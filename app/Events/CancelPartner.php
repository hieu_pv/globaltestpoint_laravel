<?php

namespace App\Events;

use App\Entities\User;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;

class CancelPartner extends Event
{
    use SerializesModels;

    public $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
