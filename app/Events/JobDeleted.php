<?php

namespace App\Events;

use App\Entities\Job;
use App\Entities\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Queue\SerializesModels;

class JobDeleted
{
    use InteractsWithSockets, SerializesModels;

    public $job;
    public $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Job $job, User $user)
    {
        $this->job  = $job;
        $this->user = $user;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
