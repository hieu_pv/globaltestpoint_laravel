<?php

namespace App\Events\Admin;

use App\Entities\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Queue\SerializesModels;

class AdminDeleteUser
{
    use InteractsWithSockets, SerializesModels;

    public $admin;
    public $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $admin, User $user)
    {
        $this->admin = $admin;
        $this->user  = $user;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
