<?php

namespace App\Events\Admin;

use App\Entities\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Queue\SerializesModels;

class AdminChangeStatusEmployer
{
    use InteractsWithSockets, SerializesModels;

    public $admin;
    public $user;
    public $status_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $admin, User $user, $status_id)
    {
        $this->admin     = $admin;
        $this->user      = $user;
        $this->status_id = $status_id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
