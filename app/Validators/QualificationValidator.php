<?php

namespace App\Validators;

use App\Validators\AbstractValidator;

class QualificationValidator extends AbstractValidator
{

    protected $rules = [
        'RULE_CREATE'             => [
            'name' => ['required'],
        ],

        'RULE_UPDATE'             => [
            'name' => ['required'],
        ],

        'CHANGE_STATUS_ALL_ITEMS' => [
            'item_ids' => ['required'],
            'status'   => ['required'],
        ],
    ];
}
