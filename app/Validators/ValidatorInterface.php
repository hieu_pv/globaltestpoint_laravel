<?php

namespace App\Validators;

interface ValidatorInterface
{
	const RULE_CREATE = 'RULE_CREATE';
	const RULE_UPDATE = 'RULE_UPDATE';
	const RULE_LIST   = 'RULE_LIST';
	
    public function isValid($data, $action);
}
