<?php

namespace App\Validators\Admin;

use App\Validators\AbstractValidator;
use App\Validators\ValidatorInterface;

class AdminJobValidator extends AbstractValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'title'        => ['required'],
            'address'      => ['required'],
            'short_desc'   => ['required'],
            'description'  => ['required'],
            'employer_id'  => ['required', 'integer'],
            'industry_ids' => ['required'],
        ],

        ValidatorInterface::RULE_UPDATE => [],

        'CHANGE_STATUS_ALL_ITEMS'       => [
            'item_ids' => ['required'],
            'status'   => ['required'],
        ],
    ];
}
