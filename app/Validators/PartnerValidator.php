<?php

namespace App\Validators;

use App\Validators\AbstractValidator;

class PartnerValidator extends AbstractValidator
{

    protected $rules = [
        'active' => [
            'accept' => ['required', 'boolean'],
        ],
    ];
}
