<?php

namespace App\Validators;

use App\Validators\AbstractValidator;

class ExperienceValidator extends AbstractValidator
{

    protected $rules = [
        'RULE_CREATE'             => [
            'label' => ['required'],
        ],

        'RULE_UPDATE'             => [
            'label' => ['required'],
        ],

        'CHANGE_STATUS_ALL_ITEMS' => [
            'item_ids' => ['required'],
            'status'   => ['required'],
        ],
    ];
}
