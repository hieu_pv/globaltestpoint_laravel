<?php

namespace App\Validators;

use App\Validators\AbstractValidator;
use App\Validators\ValidatorInterface;

class CountryValidator extends AbstractValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [],
        ValidatorInterface::RULE_UPDATE => [],
    ];
}
