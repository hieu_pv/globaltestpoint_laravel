<?php

namespace App\Validators;

use App\Validators\AbstractValidator;

class AdvertisementValidator extends AbstractValidator
{

    protected $rules = [
        'RULE_CREATE'             => [
            'name' => ['required'],
            'url'  => ['required'],
        ],

        'RULE_UPDATE'             => [
            'name' => ['required'],
            'url'  => ['required'],
        ],

        'UPDATE_IMAGE'            => [
            'url' => ['required', 'url'],
        ],

        'CHANGE_STATUS_ALL_ITEMS' => [
            'item_ids' => ['required'],
            'status'   => ['required'],
        ],
    ];
}
