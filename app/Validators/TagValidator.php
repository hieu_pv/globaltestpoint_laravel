<?php

namespace App\Validators;

use App\Validators\AbstractValidator;
use App\Validators\ValidatorInterface;

class TagValidator extends AbstractValidator
{

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'name'        => ['required', 'max:40'],
            'description' => ['max:255'],
        ],

        ValidatorInterface::RULE_UPDATE => [
            'name'        => ['required', 'max:40'],
            'description' => ['max:255'],
        ],

        ValidatorInterface::RULE_LIST   => [],

        'CHANGE_STATUS_ALL_ITEMS'       => [
            'item_ids' => ['required'],
            'status'   => ['required'],
        ],
    ];
}
