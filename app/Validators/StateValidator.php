<?php

namespace App\Validators;

use App\Validators\AbstractValidator;

class StateValidator extends AbstractValidator
{

    protected $rules = [
        'RULE_CREATE'             => [
            'country_id' => ['required'],
            'name'       => ['required'],
        ],

        'RULE_UPDATE'             => [
            'country_id' => ['required'],
            'name'       => ['required'],
        ],

        'CHANGE_STATUS_ALL_ITEMS' => [
            'item_ids' => ['required'],
            'status'   => ['required'],
        ],
    ];
}
