<?php

namespace App\Validators;

use App\Validators\AbstractValidator;

class JobTitleValidator extends AbstractValidator
{

    protected $rules = [
        'RULE_CREATE'             => [
            'name'      => ['required'],
            'job_title' => ['required'],
        ],

        'RULE_UPDATE'             => [
            'name'      => ['required'],
            'job_title' => ['required'],
        ],

        'CHANGE_STATUS_ALL_ITEMS' => [
            'item_ids' => ['required'],
            'status'   => ['required'],
        ],
    ];
}
