<?php

namespace App\Validators;

use App\Validators\AbstractValidator;

class UserValidator extends AbstractValidator
{

    protected $rules = [
        'index'                         => [
            'user_ids' => ['required'],
        ],
        ValidatorInterface::RULE_CREATE => [
            'first_name'      => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'last_name'       => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'country'         => ['regex:/[a-zA-Z\s]*/i', 'required'],
            'email'           => ['required', 'email'],
            'password'        => ['required', 'min:6', 'max:30'],
            'phone_area_code' => ['required', 'regex:/^\d+$/', 'max:5'],
            'mobile'          => ['required', 'regex:/^\d+$/', 'max:15'],
            'referral_code'   => [],
            'username'        => ['regex:/[a-z0-9\s]*/i', 'max:40'],
        ],
        'ADMIN_CREATE_USER'             => [
            'first_name' => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'last_name'  => ['required', 'regex:/[a-z0-9\s]*/i', 'max:20'],
            'email'      => ['required', 'email'],
            'password'   => ['required', 'min:6', 'max:30'],
            'role'       => ['required'],
        ],
        ValidatorInterface::RULE_UPDATE => [
            'first_name'               => ['regex:/[a-z0-9\s]*/i', 'max:20'],
            'last_name'                => ['regex:/[a-z0-9\s]*/i', 'max:20'],
            'gender'                   => ['regex:/[male|female]/'],
            'address'                  => ['max:255'],
            'zipcode'                  => [],
            'mobile'                   => ['regex:/[0-9\s|+]*/', 'max:20'],
            'phone_area_code'          => ['regex:/[0-9\s|+]*/', 'max:5'],
            'city'                     => ['regex:/[a-z\s]*/i', 'max:40'],
            'country'                  => ['regex:/[a-z\s]*/i', 'max:40'],
            'birth'                    => ['date_format:Y-m-d'],
            'allow_to_work_in_sg'      => ['boolean'],
            'kind_of_work'             => ['regex:/[parttime|fulltime]/'],
            'hours_per_week'           => ['integer'],
            'industry_id'              => ['integer'],
            'working_experience_id'    => ['integer'],
            'preferred_position_id'    => ['integer'],
            'preferred_position_ids'   => ['array'],
            'preferred_position_ids.*' => ['integer'],
            'education'                => ['max:40'],
        ],
        'employer'                      => [
            'company_name'        => ['required', 'max:64'],
            'registration_number' => ['max:20'],
            'contact_name'        => ['max:40'],
            'email'               => ['required', 'email'],
            'phone_area_code'     => ['regex:/[0-9]*/', 'max:5'],
            'mobile'              => ['required', 'regex:/[0-9\s]*/', 'max:20'],
            'password'            => ['required', 'min:6', 'max:20'],
            'industries'          => ['array'],
        ],
        'UPDATE_ADMIN_PROFILE'          => [
            'first_name' => ['regex:/[a-z0-9\s]*/i', 'max:20'],
            'last_name'  => ['regex:/[a-z0-9\s]*/i', 'max:20'],
            'email'      => ['required', 'email'],
            'role'       => ['required'],
        ],
        'UPDATE_EMPLOYEE_PROFILE'       => [
            'first_name'               => ['regex:/[a-z0-9\s]*/i', 'max:20'],
            'last_name'                => ['regex:/[a-z0-9\s]*/i', 'max:20'],
            'gender'                   => ['regex:/[male|female]/'],
            'address'                  => ['max:255'],
            'zipcode'                  => [],
            'mobile'                   => ['regex:/[0-9\s|+]*/', 'max:20'],
            'city'                     => ['regex:/[a-z\s]*/i', 'max:40'],
            'country'                  => ['regex:/[a-z\s]*/i', 'max:40'],
            'birth'                    => ['date_format:Y-m-d'],
            'allow_to_work_in_sg'      => ['boolean'],
            'kind_of_work'             => ['regex:/[parttime|fulltime]/'],
            'hours_per_week'           => ['integer'],
            'industries'               => ['array'],
            'working_experience_id'    => ['integer'],
            'preferred_position_id'    => ['integer'],
            'preferred_location_ids'   => ['array'],
            'preferred_location_ids.*' => ['integer'],
            'education'                => ['max:40'],
            'industries'               => ['array'],
            'availabilities'           => ['array'],
        ],
        'images'                        => [
            'type' => ['required', 'regex:/avatar|visa|id|license/'],
            'url'  => ['required', 'url'],
        ],
        'updateRole'                    => [
            'role' => ['required', 'regex:/[employer|employee]/'],
        ],
        'verifyPhoneNumber'             => [
            'code' => ['required'],
            'type' => ['regex:/[update]/'],
        ],
        'updateActiveBanned'            => [
            'status' => ['required'],
        ],
        'changeStatusAllUser'           => [
            'status'   => ['required'],
            'user_ids' => ['required'],
        ],
        'employeeAppliedShifts'         => [
            'page'        => ['integer'],
            'upcoming'    => ['regex:/[true|false]/'],
            'constraints' => ['JSON'],
            'order_by'    => ['JSON'],
        ],
        'checkRole'                     => [
            'email' => ['required', 'email'],
        ],
        'update_role'                   => [
            'role' => ['required'],
        ],
        'update_tags'                   => [
            'tags' => ['required', 'array'],
        ],
        'updateAvailabilities'          => [
            'availabilities' => ['array'],
        ],
        'updateIndustries'              => [
            'industries' => ['required', 'array'],
        ],
        'markNotificationsAsRead'       => [
            'notifications' => ['array'],
        ],
        'verifyIdImage'                 => [
            'status' => ['required', 'regex:/[pending|verified|rejected]/'],
        ],
        'storeVideo'                    => [
            'video'    => ['required', 'max:255', 'regex:/^\/(.*)$/'],
            'playlist' => ['max:255', 'regex:/^\/(.*)$/'],
        ],
        'updateVideoStatus'             => [
            'status' => ['required', 'regex:/pending/'],
        ],
        'SEND_SMS'                      => [
            'employee_id' => ['required'],
            'send_type'   => ['required'],
        ],
        'CHANGE_PASSWORD'               => [
            'email'        => ['required'],
            'old_password' => ['required'],
            'new_password' => ['required'],
        ],
        'FAVORITE_CANDIDATE'            => [
            'employee_id' => ['required'],
        ],
        'ADMIN_CHANGE_PASSWORD_USER'    => [
            'id'           => ['required'],
            'new_password' => ['required'],
        ],
        'LOGIN_AS_USER'                 => [
            'id' => ['required'],
        ],
        'updateSubscribeStatus'         => [
            'user_ids' => ['required', 'array'],
            'data'     => ['required', 'boolean'],
            'field'    => ['required', 'regex:/[enable_job_email|enable_marketing_email]/'],
        ],
    ];
}
