<?php

namespace App\Transformers;

use App\Entities\Country;
use League\Fractal\TransformerAbstract;

/**
 * Class CountryTransformer
 * @package namespace App\Transformers;
 */
class CountryTransformer extends TransformerAbstract
{

    /**
     * Transform the \Country entity
     * @param \Country $model
     *
     * @return array
     */
    public function transform(Country $model)
    {
        return [
            'id'                      => (int) $model->id,
            'name'                    => $model->name,
            'slug'                    => $model->slug,
            'code'                    => $model->code,
            'phone_area_code'         => $model->phone_area_code,
            'order'                   => $model->order,
            'is_default'              => (bool) $model->is_default,
            'is_supported_phone_area' => (bool) $model->is_supported_phone_area,
            'active'                  => (bool) $model->active,
            'created_at'              => $model->created_at,
            'updated_at'              => $model->updated_at,
        ];
    }
}
