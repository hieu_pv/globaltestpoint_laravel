<?php

namespace App\Transformers;

use App\Entities\City;
use App\Transformers\JobTransformer;
use App\Transformers\StateTransformer;
use League\Fractal\TransformerAbstract;

/**
 * Class CityTransformer
 * @package namespace App\Transformers;
 */
class CityTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'state',
        'jobs',
    ];

    public function __construct($includes = [])
    {
        $this->setDefaultIncludes($includes);
    }

    /**
     * Transform the \City entity
     * @param \City $model
     *
     * @return array
     */
    public function transform(City $model)
    {
        return [
            'id'         => (int) $model->id,
            'name'       => $model->name,
            'slug'       => $model->slug,
            'state_id'   => (int) $model->state_id,
            'total_jobs' => (int) $model->total_jobs,
            'order'      => (int) $model->order,
            'status'     => (int) $model->status,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at,
        ];
    }

    public function includeState(City $model)
    {
        if ($model->state) {
            return $this->item($model->state, new StateTransformer);
        }
    }

    public function includeJobs(City $model)
    {
        if ($model->jobs) {
            return $this->item($model->jobs, new JobTransformer);
        }
    }
}
