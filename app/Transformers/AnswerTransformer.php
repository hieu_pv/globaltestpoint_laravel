<?php

namespace App\Transformers;

use App\Entities\Answer;
use League\Fractal\TransformerAbstract;

/**
 * Class AnswerTransformer
 * @package namespace App\Transformers;
 */
class AnswerTransformer extends TransformerAbstract
{

    /**
     * Transform the \Answer entity
     * @param \Answer $model
     *
     * @return array
     */
    public function transform(Answer $model)
    {
        return [
            'id'          => (int) $model->id,
            'answer'      => $model->answer,
            'description' => $model->description,
            'is_correct'  => $model->is_correct == 1 ? true : false,
        ];
    }
}
