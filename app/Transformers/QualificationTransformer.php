<?php

namespace App\Transformers;

use App\Entities\Qualification;
use League\Fractal\TransformerAbstract;

/**
 * Class QualificationTransformer
 * @package namespace App\Transformers;
 */
class QualificationTransformer extends TransformerAbstract
{

    /**
     * Transform the \Qualification entity
     * @param \Qualification $model
     *
     * @return array
     */
    public function transform(Qualification $model)
    {
        return [
            'id'         => (int) $model->id,
            'name'       => $model->name,
            'total_jobs' => (int) $model->total_jobs,
            'order'      => (int) $model->order,
            'status'     => (int) $model->status,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at,
        ];
    }
}
