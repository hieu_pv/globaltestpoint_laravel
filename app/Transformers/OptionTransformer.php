<?php

namespace App\Transformers;

use App\Entities\Option;
use League\Fractal\TransformerAbstract;

/**
 * Class OptionTransformer
 * @package namespace App\Transformers;
 */
class OptionTransformer extends TransformerAbstract
{

    /**
     * Transform the \Option entity
     * @param \Option $model
     *
     * @return array
     */
    public function transform(Option $model)
    {
        return [
            'id'                       => (int) $model->id,
            'logo'                     => $model->logo,
            'page'                     => $model->page == 0 ? 'testyourself' : 'youwantdone',
            'title'                    => $model->title,
            'email'                    => $model->email,
            'phone'                    => $model->phone,
            'address'                  => $model->address,
            'fax'                      => $model->fax,
            'facebook'                 => $model->facebook,
            'twitter'                  => $model->twitter,
            'youtube'                  => $model->youtube,
            'instagram'                => $model->instagram,
            'linkedin'                 => $model->linkedin,
            'image_login'              => $model->image_login,
            'image_register'           => $model->image_register,
            'image_login_register'     => $model->image_login_register,
            'viewsite'                 => (int) $model->viewsite,
            'termpolicy'               => $model->termpolicy,
            'introduction_book'        => $model->introduction_book,
            'introduction_variousexam' => $model->introduction_variousexam,
            'introduction_exams'       => $model->introduction_exams,
            'created_at'               => $model->created_at,
            'updated_at'               => $model->updated_at,
        ];
    }
}
