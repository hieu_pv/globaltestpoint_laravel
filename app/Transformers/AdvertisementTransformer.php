<?php

namespace App\Transformers;

use App\Entities\Advertisement;
use League\Fractal\TransformerAbstract;

/**
 * Class AdvertisementTransformer
 * @package namespace App\Transformers;
 */
class AdvertisementTransformer extends TransformerAbstract
{

    /**
     * Transform the \Advertisement entity
     * @param \Advertisement $model
     *
     * @return array
     */
    public function transform(Advertisement $model)
    {
        return [
            'id'         => (int) $model->id,
            'name'       => $model->name,
            'slug'       => $model->slug,
            'image'      => $model->image,
            'url'        => $model->url,
            'position'   => (int) $model->position,
            'order'      => (int) $model->order,
            'status'     => (int) $model->status,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at,
        ];
    }
}
