<?php

namespace App\Transformers;

use App\Entities\JobTitle;
use League\Fractal\TransformerAbstract;

/**
 * Class JobTitleTransformer
 * @package namespace App\Transformers;
 */
class JobTitleTransformer extends TransformerAbstract
{

    /**
     * Transform the \JobTitle entity
     * @param \JobTitle $model
     *
     * @return array
     */
    public function transform(JobTitle $model)
    {
        return [
            'id'              => (int) $model->id,
            'name'            => $model->name,
            'job_title'       => $model->job_title,
            'job_description' => $model->job_description,
            'total_jobs'      => (int) $model->total_jobs,
            'order'           => (int) $model->order,
            'status'          => (int) $model->status,
            'created_at'      => $model->created_at,
            'updated_at'      => $model->updated_at,
        ];
    }
}
