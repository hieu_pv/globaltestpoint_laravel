<?php

namespace App\Transformers;

use App\Entities\Image;
use League\Fractal\TransformerAbstract;

/**
 * Class ImageTransformer
 * @package namespace App\Transformers;
 */
class ImageTransformer extends TransformerAbstract
{

    /**
     * Transform the \Image entity
     * @param \Image $model
     *
     * @return array
     */
    public function transform(Image $model)
    {
        return [
            'type' => $model->type,
            'url'  => filter_var($model->url, FILTER_VALIDATE_URL) === false ? asset($model->url) : $model->url,
        ];
    }
}
