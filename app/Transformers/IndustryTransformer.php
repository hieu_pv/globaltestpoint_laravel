<?php

namespace App\Transformers;

use App\Entities\Industry;
use League\Fractal\TransformerAbstract;

/**
 * Class IndustryTransformer
 * @package namespace App\Transformers;
 */
class IndustryTransformer extends TransformerAbstract
{

    /**
     * Transform the \Industry entity
     * @param \Industry $model
     *
     * @return array
     */
    public function transform(Industry $model)
    {
        return [
            'id'         => (int) $model->id,
            'name'       => $model->name,
            'slug'       => $model->slug,
            'total_jobs' => (int) $model->total_jobs,
            'order'      => (int) $model->order,
            'status'     => (int) $model->status,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at,
        ];
    }
}
