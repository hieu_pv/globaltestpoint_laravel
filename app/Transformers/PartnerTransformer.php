<?php

namespace App\Transformers;

use App\Entities\Partner;
use App\Transformers\CountryTransformer;
use App\Transformers\StateTransformer;
use League\Fractal\TransformerAbstract;

/**
 * Class PartnerTransformer
 * @package namespace App\Transformers;
 */
class PartnerTransformer extends TransformerAbstract
{
    protected $defaultIncludes = ['country', 'state'];

    /**
     * Transform the \Partner entity
     * @param \Partner $model
     *
     * @return array
     */
    public function transform(Partner $model)
    {
        return [
            'id'            => (int) $model->id,
            'first_name'    => $model->first_name,
            'last_name'     => $model->last_name,
            'business_name' => $model->business_name,
            'address'       => $model->address,
            'about'         => $model->about,
            'city'          => $model->city,
            'email'         => $model->email,
            'country_id'    => $model->country_id,
            'state_id'      => $model->state_id,
            'review_links'  => $model->review_links,
            'website'       => $model->website,
            'active'        => $model->active ? true : false,
            'accept'        => $model->accept ? true : false,

            'created_at'    => $model->created_at,
            'updated_at'    => $model->updated_at,
        ];
    }

    public function includeCountry(Partner $model)
    {
        $country = $model->country;
        return isset($country) ? $this->item($country, new CountryTransformer) : null;
    }

    public function includeState(Partner $model)
    {
        $state = $model->state;
        return isset($state) ? $this->item($state, new StateTransformer) : null;
    }
}
