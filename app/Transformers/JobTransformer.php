<?php

namespace App\Transformers;

use App\Entities\Job;
use App\Transformers\CityTransformer;
use App\Transformers\CountryTransformer;
use App\Transformers\ExperienceTransformer;
use App\Transformers\IndustryTransformer;
use App\Transformers\JobTitleTransformer;
use App\Transformers\NationalityTransformer;
use App\Transformers\QualificationTransformer;
use App\Transformers\TagTransformer;
use App\Transformers\UserTransformer;
use League\Fractal\TransformerAbstract;

/**
 * Class JobTransformer
 * @package namespace App\Transformers;
 */
class JobTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'industries',
        'employer',
        'tags',
        'city',
        'state',
        'country',
        'nationality',
        'jobTitle',
        'experience',
        'qualification',
    ];

    protected $user_with_application_includes = [
        'images',
    ];

    protected $employer_includes = [
        'images',
        'employer',
        'roles',
    ];

    public function __construct($includes = [])
    {
        $this->setDefaultIncludes($includes);
    }

    /**
     * Transform the \Job entity
     * @param \Job $model
     *
     * @return array
     */
    public function transform(Job $model)
    {
        switch ($model->gender) {
            case 1:
                $gender = 'male';
                break;

            case 2:
                $gender = 'female';
                break;

            default:
                $gender = '';
                break;
        }

        return [
            'id'                        => (int) $model->id,
            'title'                     => $model->title,
            'slug'                      => $model->slug,
            'image'                     => $model->image,
            'address'                   => $model->address,
            'location'                  => $model->location,
            'min_salary'                => $model->min_salary,
            'max_salary'                => $model->max_salary,
            'zipcode'                   => $model->zipcode,
            'currency'                  => $model->currency,
            'certificate'               => $model->certificate,
            'gender'                    => $gender,
            'experience_id'             => $model->experience_id,
            'employer_id'               => $model->employer_id,
            'city_id'                   => $model->city_id,
            'state_id'                  => $model->state_id,
            'country_id'                => $model->country_id,
            'nationality_id'            => $model->nationality_id,
            'job_title_id'              => $model->job_title_id,
            'qualification_id'          => $model->qualification_id,
            'short_desc'                => $model->short_desc,
            'description'               => $model->description,
            'desired_candidate_profile' => $model->desired_candidate_profile,
            'type'                      => (int) $model->type,
            'order'                     => (int) $model->order,
            'active'                    => (bool) $model->active,
            'archive'                   => $model->archive ? true : false,
            'complete'                  => $model->complete ? true : false,
            'draft'                     => $model->draft ? true : false,

            'timestamps'                => [
                'created_at' => $model->created_at,
                'updated_at' => $model->updated_at,
            ],
        ];
    }

    public function setUserWithApplicationIncludes($includes)
    {
        $this->user_with_application_includes = $includes;
    }

    public function setEmployerIncludes($includes)
    {
        $this->employer_includes = $includes;
    }

    public function includeEmployer(Job $model)
    {
        $userTransformer = new UserTransformer($this->employer_includes);
        return $this->item($model->employer, $userTransformer);
    }

    public function includeIndustries(Job $model)
    {
        return $this->collection($model->industries, new IndustryTransformer);
    }

    public function includeCity(Job $model)
    {
        if ($model->city) {
            return $this->item($model->city, new CityTransformer);
        }
    }

    public function includeCountry(Job $model)
    {
        if ($model->country) {
            return $this->item($model->country, new CountryTransformer);
        }
    }

    public function includeNationality(Job $model)
    {
        return $this->item($model->nationality, new NationalityTransformer);
    }

    public function includeJobTitle(Job $model)
    {
        return $this->item($model->jobTitle, new JobTitleTransformer);
    }

    public function includeExperience(Job $model)
    {
        return $this->item($model->experience, new ExperienceTransformer);
    }

    public function includeQualification(Job $model)
    {
        return $this->item($model->qualification, new QualificationTransformer);
    }

    public function includeTags(Job $model)
    {
        return $this->collection($model->tags, new TagTransformer);
    }
}
