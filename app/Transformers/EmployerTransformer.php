<?php

namespace App\Transformers;

use App\Entities\Employer;
use League\Fractal\TransformerAbstract;

/**
 * Class EmployerTransformer
 * @package namespace App\Transformers;
 */
class EmployerTransformer extends TransformerAbstract
{

    /**
     * Transform the \Company entity
     * @param \Company $model
     *
     * @return array
     */
    public function transform(Employer $model)
    {
        return [
            'id'                  => (int) $model->id,
            'company_name'        => $model->company_name,
            'slug'                => $model->slug,
            'fax'                 => $model->fax,
            'website'             => $model->website,
            'company_address'     => $model->company_address,
            'company_about'       => $model->company_about,
            'number_of_outlets'   => $model->number_of_outlets,
            'registration_number' => $model->registration_number,
            'contact_name'        => $model->contact_name,
            'contact_position'    => $model->contact_position,
            'note'                => $model->note,
            'created_at'          => $model->created_at,
            'updated_at'          => $model->updated_at,
        ];
    }
}
