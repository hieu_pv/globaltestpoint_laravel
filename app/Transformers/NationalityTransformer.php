<?php

namespace App\Transformers;

use App\Entities\Nationality;
use League\Fractal\TransformerAbstract;

/**
 * Class NationalityTransformer
 * @package namespace App\Transformers;
 */
class NationalityTransformer extends TransformerAbstract
{

    /**
     * Transform the \Nationality entity
     * @param \Nationality $model
     *
     * @return array
     */
    public function transform(Nationality $model)
    {
        return [
            'id'         => (int) $model->id,
            'name'       => $model->name,
            'slug'       => $model->slug,
            'order'      => (int) $model->order,
            'status'     => (int) $model->status,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at,
        ];
    }
}
