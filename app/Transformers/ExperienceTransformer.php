<?php

namespace App\Transformers;

use App\Entities\Experience;
use League\Fractal\TransformerAbstract;

/**
 * Class ExperienceTransformer
 * @package namespace App\Transformers;
 */
class ExperienceTransformer extends TransformerAbstract
{

    /**
     * Transform the \Experience entity
     * @param \Experience $model
     *
     * @return array
     */
    public function transform(Experience $model)
    {
        return [
            'id'         => (int) $model->id,
            'label'      => $model->label,
            'total_jobs' => (int) $model->total_jobs,
            'order'      => (int) $model->order,
            'status'     => (int) $model->status,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at,
        ];
    }
}
