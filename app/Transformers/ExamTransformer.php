<?php

namespace App\Transformers;

use App\Entities\Exam;
use App\Transformers\QuestionTransformer;
use League\Fractal\TransformerAbstract;

/**
 * Class ExamTransformer
 * @package namespace App\Transformers;
 */
class ExamTransformer extends TransformerAbstract {

	protected $defaultIncludes = ['questions'];

	/**
	 * Transform the \Exam entity
	 * @param \Exam $model
	 *
	 * @return array
	 */
	public function transform(Exam $model) {

		return [
			'id'           => (int) $model->id,
			'name'         => $model->name,
			'price'        => (float) $model->price,
			'duration'     => (int) $model->duration,
			'pass_percent' => (int) $model->pass_percent,
			'grade'        => $model->grade,
			'created_at'   => $model->created_at,
			'updated_at'   => $model->updated_at,
		];
	}

	public function includeQuestions(Exam $model) {
		return $this->collection($model->questions, new QuestionTransformer);
	}
}