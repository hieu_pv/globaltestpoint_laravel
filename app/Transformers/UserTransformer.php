<?php

namespace App\Transformers;

use App\Entities\User;
use App\Transformers\EmployerTransformer;
use App\Transformers\ImageTransformer;
use App\Transformers\RoleTransformer;
use League\Fractal\TransformerAbstract;

/**
 * Class ExamTransformer
 * @package namespace App\Transformers;
 */
class UserTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'roles',
        'employer',
        'images',
    ];

    public $role_includes;

    public function __construct($includes = [])
    {
        $this->setDefaultIncludes($includes);
    }

    /**
     * Transform the \Exam entity
     * @param \Exam $model
     *
     * @return array
     */
    public function transform(User $model)
    {
        $gender = '';
        if ($model->gender !== null) {
            if ($model->gender == 0) {
                $gender = 'male';
            } else if ($model->gender == 1) {
                $gender = 'female';
            }
        }

        $data = [
            'id'                       => (int) $model->id,
            'first_name'               => $model->first_name,
            'middle_name'              => $model->middle_name,
            'last_name'                => $model->last_name,
            'fullname'                 => isset($model->middle_name) && $model->middle_name != '' ? "{$model->first_name} {$model->middle_name} {$model->last_name}" : "{$model->first_name} {$model->last_name}",
            'email'                    => $model->email,
            'image'                    => $model->image,
            'phone_area_code'          => $model->phone_area_code,
            'mobile'                   => $model->mobile,
            'phone_number_verified'    => $model->phone_number_verified,
            'optional_phone_area_code' => $model->optional_phone_area_code,
            'optional_phone_number'    => $model->optional_phone_number,
            'birth'                    => $model->birth == '0000-00-00' ? '' : $model->birth,
            'user_type'                => $model->user_type,
            'sumary'                   => $model->sumary,
            'address'                  => $model->address,
            'gender'                   => $gender,
            'city'                     => $model->city,
            'country'                  => $model->country,
            'active'                   => (bool) $model->active,
            'email_verified'           => $model->email_verified,
            'status'                   => (int) $model->status,
            'created_at'               => $model->created_at,
            'updated_at'               => $model->updated_at,
        ];

        return $data;
    }

    public function setRoleTransformer($includes = [])
    {
        $this->role_includes = $includes;
    }

    public function includeRoles(User $model)
    {
        return $this->collection($model->roles, new RoleTransformer($this->role_includes));
    }

    public function includeEmployer(User $model)
    {
        if (!empty($model->employers)) {
            return $this->collection($model->employers, new EmployerTransformer);
        }
    }

    public function includeImages(User $model)
    {
        return $this->collection($model->images, new ImageTransformer);
    }
}
