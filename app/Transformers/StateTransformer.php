<?php

namespace App\Transformers;

use App\Entities\State;
use League\Fractal\TransformerAbstract;

/**
 * Class StateTransformer
 * @package namespace App\Transformers;
 */
class StateTransformer extends TransformerAbstract
{

    /**
     * Transform the \State entity
     * @param \State $model
     *
     * @return array
     */
    public function transform(State $model)
    {
        return [
            'id'         => (int) $model->id,
            'name'       => $model->name,
            'country_id' => (int) $model->country_id,
            'total_jobs' => (int) $model->total_jobs,
            'order'      => (int) $model->order,
            'active'     => (bool) $model->active,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at,
        ];
    }
}
