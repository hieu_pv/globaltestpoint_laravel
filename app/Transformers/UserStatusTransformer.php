<?php

namespace App\Transformers;

use App\Entities\UserStatus;
use League\Fractal\TransformerAbstract;

/**
 * Class UserStatusTransformer
 * @package namespace App\Transformers;
 */
class UserStatusTransformer extends TransformerAbstract
{

    /**
     * Transform the \UserStatus entity
     * @param \UserStatus $model
     *
     * @return array
     */
    public function transform(UserStatus $model)
    {
        return [
            'id'          => (int) $model->id,
            'status'      => $model->status,
            'description' => $model->description,
            'active'      => $model->active == 1 ? true : false,

        ];
    }
}
