<?php

namespace App\Listeners;

use App\Entities\Option;
use App\Events\UserBecomePartner;
use Illuminate\Support\Facades\Mail;

class UserBecomePartnerListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserBecomePartner  $event
     * @return void
     */
    public function handle(UserBecomePartner $event)
    {
        $user = $event->user;
        $option_tys  = Option::where('page', 0)->first();
        Mail::queue('mails.user_become_partner', ['user' => $user, 'option_tys' => $option_tys], function ($message) use ($user) {
            $message->from(getenv('MAIL_USERNAME'), 'Globaltestpoint');
            $message->to($user->email)->subject('Your application has been accept! globaltestpoint');
        });
    }
}
