<?php

namespace App\Listeners;

use App\Entities\Experience;
use App\Entities\Industry;
use App\Entities\Job;
use App\Entities\JobTitle;
use App\Entities\Qualification;
use App\Entities\State;
use App\Events\UpdateTotalJobsFieldEvent;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class UpdateTotalJobsFieldListener implements ShouldQueue
{
    use Queueable;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdateTotalJobsFieldEvent  $event
     * @return void
     */
    public function handle(UpdateTotalJobsFieldEvent $event)
    {
        $states = State::with('jobs')->get();
        foreach ($states as $state) {
            if (!$state->jobs->isEmpty()) {
                $state->total_jobs = $state->jobs()->where(Job::STATE['JOB_ACTIVE'])->count();
                $state->save();
            }
        }

        $industries = Industry::with('jobs')->get();
        foreach ($industries as $industry) {
            if (!$industry->jobs->isEmpty()) {
                $industry->total_jobs = $industry->jobs()->where(Job::STATE['JOB_ACTIVE'])->count();
                $industry->save();
            }
        }

        $job_titles = JobTitle::with('jobs')->get();
        foreach ($job_titles as $job_title) {
            if (!$job_title->jobs->isEmpty()) {
                $job_title->total_jobs = $job_title->jobs()->where(Job::STATE['JOB_ACTIVE'])->count();
                $job_title->save();
            }
        }

        $qualifications = Qualification::with('jobs')->get();
        foreach ($qualifications as $qualification) {
            if (!$qualification->jobs->isEmpty()) {
                $qualification->total_jobs = $qualification->jobs()->where(Job::STATE['JOB_ACTIVE'])->count();
                $qualification->save();
            }
        }

        $experiences = Experience::with('jobs')->get();
        foreach ($experiences as $experience) {
            if (!$experience->jobs->isEmpty()) {
                $experience->total_jobs = $experience->jobs()->where(Job::STATE['JOB_ACTIVE'])->count();
                $experience->save();
            }
        }
    }
}
