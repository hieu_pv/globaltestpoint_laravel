<?php

namespace App\Listeners;

use App\Entities\Option;
use App\Events\AcceptNewPartner;
use Illuminate\Support\Facades\Mail;

class AcceptNewPartnerListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AcceptNewPartner  $event
     * @return void
     */
    public function handle(AcceptNewPartner $event)
    {
        $user     = $event->user;
        $password = $event->password;
        $option_tys  = Option::where('page', 0)->first();
        Mail::queue('mails.new_partner_was_accepted', ['user' => $user, 'password' => $password, 'option_tys' => $option_tys], function ($message) use ($user) {
            $message->from(getenv('MAIL_USERNAME'), 'Globaltestpoint');
            $message->to($user->email)->subject('Your application has been accept! globaltestpoint');
        });
    }
}
