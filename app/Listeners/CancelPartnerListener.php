<?php

namespace App\Listeners;

use App\Entities\Option;
use App\Events\CancelPartner;
use Illuminate\Support\Facades\Mail;

class CancelPartnerListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CancelPartner  $event
     * @return void
     */
    public function handle(CancelPartner $event)
    {
        $user = $event->user;
        $option_tys  = Option::where('page', 0)->first();
        Mail::queue('mails.cancel_partner', ['user' => $user, 'option_tys' => $option_tys], function ($message) use ($user) {
            $message->from(getenv('MAIL_USERNAME'), 'Globaltestpoint');
            $message->to($user->email)->subject('Your partner account has been cancel! globaltestpoint');
        });
    }
}
