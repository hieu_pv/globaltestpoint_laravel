<?php

namespace App\Repositories;

use App\Entities\Category;
use App\Entities\Exam;
use App\Entities\Paid;
use App\Entities\Question;
use App\Repositories\ExamRepository;
use Carbon\Carbon;
use Exception;
use Mail;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class ExamRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ExamRepositoryEloquent extends BaseRepository implements ExamRepository {
	/**
	 * Specify Model class name
	 *
	 * @return string
	 */
	public function model() {
		return Exam::class;
	}

	/**
	 * Boot up the repository, pushing criteria
	 */
	public function boot() {
		$this->pushCriteria(app(RequestCriteria::class));
	}

	public function getAll() {
		$exams = Exam::where('active', 'yes')->orderBy('id', 'desc')->get();
		return $exams;
	}

	public function getExamByCategory($category_id) {
		$category = Category::find($category_id);
		$exams    = $category->exams()->where('active', 'yes')->orderBy('id', 'desc')->get();
		return $exams;
	}

	public function getExam($slug) {
		$exam = Exam::where('slug', $slug)->first();
		return $exam;
	}

	public function getQuestions($exam_id) {
		$exam = Exam::find($exam_id);
		if (!$exam) {
			throw new Exception("Id exam doesn't exists", 1);
		}
		return $exam->questions;
	}

	public function checkBuyExam($user_id, $exam_id) {
		$buyExam = Paid::where('user_id', $user_id)->where('product_id', $exam_id)->where('type', 'exam')->first();
		if ($buyExam == null) {
			$buyExam = 0;
		} else {
			$buyExam = 1;
		}
		return $buyExam;
	}

	public function getResultExam($exam) {
		$result = [
			'timeTaken'     => $exam['timeTaken'],
			'exam'          => $exam,
			'totalQuestion' => count($exam['questions']['data']),
		];

		$correctAnswers = 0;
		if (isset($exam['questions']) && count($exam['questions']['data']) > 0) {
			$questions = $exam['questions']['data'];
			foreach ($questions as $question) {
				$question_id    = $question['id'];
				$questionDB     = Question::find($question_id);
				$answersDB      = $questionDB->answers;
				$countAnswersDB = count($answersDB);

				if (isset($question['answers']) && count($question['answers']['data']) > 0) {
					$answers      = $question['answers']['data'];
					$countAnswers = 0;
					foreach ($answers as $key => $answer) {
						$answer['id']                  = intval($answer['id']);
						$answer['is_correct']          = intval($answer['is_correct']);
						$answersDB[$key]['id']         = intval($answersDB[$key]['id']);
						$answersDB[$key]['is_correct'] = intval($answersDB[$key]['is_correct']);

						if ($answer['id'] == $answersDB[$key]['id'] && $answer['is_correct'] == $answersDB[$key]['is_correct']) {
							$countAnswers++;
						}
					}
					if ($countAnswers == $countAnswersDB) {
						$correctAnswers++;
					}
				}
			}
		}
		$result['correctAnswers'] = $correctAnswers;
		$result['score']          = round(($correctAnswers / $result['totalQuestion']) * 100, 2);
		$grades                   = json_decode($exam['grade'], true);
		$result['grade'] = 'F';
		if (!empty($grades)) {
			foreach ($grades as $grade) {
				if ($result['score'] >= $grade['min'] && $result['score'] <= $grade['max']) {
					$result['grade'] = $grade['title'];
					break;
				}
			}
		}
		$examDB = Exam::find($exam['id']);
		if ($result['score'] >= $examDB->pass_percent) {
			$result['remark'] = 'Pass';
		} else {
			$result['remark'] = 'Fail';
		}
		$result['examDate'] = Carbon::now();
		$result['examDate'] = $result['examDate']->format('d/m/Y, h:i:s A');
		return $result;
	}

	/**
	 * @param  Array $questions
	 * @return Collection of Question
	 */
	public function getAnswerCorrect($questions) {
		$questions = $questions->map(function ($question) {
			$answers       = $question['answers'];
			$alphas        = range('A', 'Z');
			$answerCorrect = array();
			foreach ($answers as $key => $answer) {
				if ($answer['is_correct'] == 1) {
					$answerCorrect[] = $alphas[$key];
				}
			}
			$question['answerCorrect'] = implode(', ', $answerCorrect);
			return $question;
		});
		return $questions;
	}

	/**
	 * @param  Array $questions
	 * @return Collection of Question
	 */
	public function getYourChoice($questions, $bindingQuestion, $alphas) {
		foreach ($questions as $keyQuestion => $question) {
			if (isset($question['answers']) && count($question['answers']['data']) > 0) {
				$answers    = $question['answers']['data'];
				$yourChoice = array();
				foreach ($answers as $keyAnswer => $answer) {
					if ($answer['is_correct'] == 1) {
						$yourChoice[] = $alphas[$keyAnswer];
					}
				}
				$bindingQuestion[$keyQuestion]['yourChoice'] = implode(', ', $yourChoice);
			}
		}
		return $bindingQuestion;
	}

	/**
	 * [saveExamToDB description]
	 * @param  UserLogged $user
	 * @param  mixed $exam information of user do exam
	 */
	public function saveExamToDB($user, $examResult) {
		$decode = json_encode($examResult);
		$examID = $examResult['exam']['id'];
		$user->exams()->attach([$examID => ['data' => $decode]]);
	}

	public function sendMail($user, $exam) {
		Mail::queue('mails.doneExam', ['user' => $user, 'exam' => $exam], function ($message) use ($user, $exam) {
			$message->from('vicoders.daily.report@gmail.com', 'Globaltestpoint');
			if ($user->gender == 1) {
				$message->to($user->email)->subject('Congratulations Mr. ' . $user->first_name);
			} else {
				$message->to($user->email)->subject('Congratulations Mrs. ' . $user->first_name);
			}
		});
	}
}