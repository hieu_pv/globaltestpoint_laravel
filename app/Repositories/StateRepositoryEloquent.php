<?php

namespace App\Repositories;

use App\Entities\Country;
use App\Entities\State;
use App\Repositories\StateRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class StateRepositoryEloquent
 * @package namespace App\Repositories;
 */
class StateRepositoryEloquent extends BaseRepository implements StateRepository {
	/**
	 * Specify Model class name
	 *
	 * @return string
	 */
	public function model() {
		return State::class;
	}

	/**
	 * Boot up the repository, pushing criteria
	 */
	public function boot() {
		$this->pushCriteria(app(RequestCriteria::class));
	}

	public function getAll() {
		$states = State::orderBy('order', 'desc')->orderBy('id', 'desc')->get();
		return $states;
	}

	public function getCountryofState($state_id) {
		$state = State::find($state_id);
		return $state->country;
	}

	public function getActiveCountry() {
		$countries = Country::where('active', 1)->get();
		return $countries;
	}
}
