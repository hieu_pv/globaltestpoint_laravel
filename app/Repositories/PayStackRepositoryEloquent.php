<?php

namespace App\Repositories;

use App\Entities\Payment;
use App\Repositories\PayStackRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Yabacon\Paystack;
use Yabacon\Paystack\Routes\Customer;

/**
 * Class PayStackRepositoryEloquent
 * @package namespace App\Repositories;
 */
class PayStackRepositoryEloquent extends BaseRepository implements PaymentRepository
{
    protected $paystack;
    public function __construct()
    {
        $this->paystack = new Paystack(env('PAYSTACK_SECRET'));
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Payment::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function createCustomer($params)
    {
        $response = $this->paystack->customer->create([
            'first_name' => $params['first_name'],
            'last_name'  => $params['last_name'],
            'email'      => $params['email'],
            'phone'      => $params['phone'],
        ]);
        return $response;
    }

    public function retrieveCustomer($id_cus)
    {
        $response = $this->paystack->customer($id_cus);
        return $response;
    }

    public function updateCustomer($id, $params)
    {
        $params['id'] = $id;
        if(!empty($params['first_name'])) {
            $params['first_name'] = $params['first_name'];
        }
        if(!empty($params['last_name'])){
            $params['last_name'] = $params['last_name'];
        }
        if(!empty($params['phone'])){
            $params['phone'] = $params['phone'];
        }
        $response = $this->paystack->customer->update($params);
        return $response;
    }

    public function deleteCustomers($id_cus) {}

    /**
     * [createCharge initialize a charge, with callback url]
     * @param  [type] $params [description]
     * @return [type]         [description]
     */
    public function createCharge($params)
    {
        if(isset($params['metadata'])) {
            $init = $this->paystack->transaction->initialize([
                'callback_url' => $params['callback_url'],
                'reference' => time(),
                'amount'    => $params['amount'], // in kobo
                'email'     => $params['email'],
                'metadata'  => $params['metadata'] // in json 
            ]);
        } else {
            $init = $this->paystack->transaction->initialize([
                'callback_url' => $params['callback_url'],
                'reference' => time(),
                'amount'    => $params['amount'], // in kobo
                'email'     => $params['email']
            ]);
        }
        return $init;
    }

    public function retrieveCharge($id_charge)
    {
        $response = $this->paystack->transaction($id_charge);
        return $response;
    }

    public function updateCharge($id_charge) {}

    public function deleteCharge($id_charge) {}

    public function changeToken($params)
    {
        $response = $this->paystack->transaction->chargeToken([
            'reference' => $params['reference'],
            'token'     => $params['token'],
            'email'     => $params['email'],
            'amount'    => $params['amount'], // in kobo
        ]);
        return $response;
    }

    public function verify($unique_refencecode)
    {
        $response = $this->paystack->transaction->verify([
            'reference' => $unique_refencecode,
        ]);
        return $response;
    }

    public function chargeAuthorization()
    {
        $response = $this->paystack->transaction->chargeAuthorization();
        return $response;
    }
}
