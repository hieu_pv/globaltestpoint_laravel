<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface JobTitleRepository
 * @package namespace App\Repositories;
 */
interface JobTitleRepository extends RepositoryInterface
{

}
