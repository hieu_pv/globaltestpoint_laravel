<?php

namespace App\Repositories;

use App\Entities\Industry;
use App\Repositories\IndustryRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class IndustryRepositoryEloquent
 * @package namespace App\Repositories;
 */
class IndustryRepositoryEloquent extends BaseRepository implements IndustryRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Industry::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
