<?php

namespace App\Repositories;

use App\Entities\JobTitle;
use App\Repositories\JobTitleRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class JobTitleRepositoryEloquent
 * @package namespace App\Repositories;
 */
class JobTitleRepositoryEloquent extends BaseRepository implements JobTitleRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return JobTitle::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
