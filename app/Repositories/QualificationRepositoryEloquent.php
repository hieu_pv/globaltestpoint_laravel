<?php

namespace App\Repositories;

use App\Entities\Qualification;
use App\Repositories\QualificationRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class QualificationRepositoryEloquent
 * @package namespace App\Repositories;
 */
class QualificationRepositoryEloquent extends BaseRepository implements QualificationRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Qualification::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
