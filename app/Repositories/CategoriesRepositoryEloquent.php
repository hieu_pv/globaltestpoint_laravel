<?php

namespace App\Repositories;

use App\Entities\Category;
use App\Repositories\categoriesRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class CategoriesRepositoryEloquent
 * @package namespace App\Repositories;
 */
class CategoriesRepositoryEloquent extends BaseRepository implements CategoriesRepository {
	/**
	 * Specify Model class name
	 *
	 * @return string
	 */
	public function model() {
		return Category::class;
	}

	/**
	 * Boot up the repository, pushing criteria
	 */
	public function boot() {
		$this->pushCriteria(app(RequestCriteria::class));
	}

	/**
	 * [getAll get all category in db]
	 * @return array data array
	 */
	public function getAll() {
		return Category::orderBy('order', 'desc')->get();
	}

	/**
	 * [getOne description]
	 * @param  int $cate_id find category by id
	 * @return boolean          find or no
	 */
	public function getOne($cate_id) {
		return $this->findOrFail($cate_id);
	}

	/**
	 * Create new a Category
	 * @return int return id of new category
	 */
	public function createCategory($data = []) {
		return Categories::create($data);
	}

	// ================= ??? tra ve gi + tra ve id vua them - cach viet nao dung ================
	/**
	 * [updateCategory Update information category by id]
	 * @param  array  $data    information to update for category
	 * @param  int $cate_id update category by id
	 * @return boolean
	 */
	public function updateCategory($data = [], $cate_id) {
		if (empty($data)) {
			return false;
		}
		$cate = $this->find($cate_id);
		return $cate->save($data);
	}

	/**
	 * [deleteCategory delete a category by id]
	 * @return boolean TRUE is success
	 */
	public function deleteCategory($cate_id) {
		$cate = $this->find($cate_id);
		return $cate->delete();
	}

	public function getChildCategory($category_id) {
		$childCategories = Category::where('parent_id', $category_id)->get();
        return $childCategories;
	}
}
