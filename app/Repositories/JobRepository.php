<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface JobRepository
 * @package namespace App\Repositories;
 */
interface JobRepository extends RepositoryInterface {

	/**
	 * get all resource of Job
	 *
	 * @param array
	 * @return Collection
	 */
	public function getAll($conditions = []);

	/**
	 * Employee apply job
	 *
	 * @param int $job_id
	 * @param int $user_id
	 * @param string $request
	 * @param array $shift_ids
	 * @return boolean
	 */
	// public function apply($job_id, $user_id, $request, $shift_ids);

	/**
	 * Cancel pending job by employee
	 * @param  int $user_id
	 * @param  int $job_id
	 * @param  int $employee_id
	 * @return \Entities\User
	 */
	public function cancelPendingJob($user_id, $job_id);

	/**
	 * update shifts of a job
	 *
	 *
	 */
	public function updateShifts($job, $shifts);

	/**
	 * save shifts to job
	 *
	 * @param \App\Entities\Job $job
	 * @param Array \App\Entities\Shift $shifts
	 * @return void
	 */
	public function saveShifts($job, $shifts);

	/**
	 * accept an application of user
	 *
	 * @param \App\Entities\Shift $shift
	 * @param int @employee_id
	 * @return \App\Entities\Shift
	 */
	public function acceptApplication($shift, $employee_id);

	/**
	 * cancel an application of user
	 *
	 * @param \App\Entities\Shift $shift
	 * @param int @employee_id
	 * @return \App\Entities\Shift
	 */
	public function cancelApplication($shift, $employee_id);

	/**
	 * complete an application of user
	 *
	 * @param \App\Entities\Shift $shift
	 * @param int @employee_id
	 * @return \App\Entities\Shift
	 */
	public function completeApplication($shift, $employee_id);

}
