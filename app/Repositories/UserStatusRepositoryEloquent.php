<?php

namespace App\Repositories;

use App\Entities\UserStatus;
use App\Repositories\UserStatusRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class UserStatusRepositoryEloquent
 * @package namespace App\Repositories;
 */
class UserStatusRepositoryEloquent extends BaseRepository implements UserStatusRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserStatus::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
