<?php

namespace App\Repositories;

use App\Entities\Employer;
use App\Entities\User;
use App\Repositories\CanFlushCache;
use App\Repositories\UserRepository;
use Exception;
use NF\Roles\Models\Role;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class UserRepositoryEloquent
 * @package namespace App\Repositories;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository, CacheableInterface
{
    use CacheableRepository;
    use CanFlushCache;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function attachRole($role, $id)
    {
        $user = $this->find($id);
        if (!$user->roles->isEmpty()) {
            throw new Exception("can not change the role of user", 1027);
        }
        $role = Role::where('slug', $role)->first();
        $user->attachRole($role);
        return $user;
    }

    public function createEmployerProfile($attributes = [], $id)
    {
        $user                  = $this->find($id);
        $attributes['user_id'] = $id;
        $attributes['slug']    = str_slug($attributes['company_name']);
        $i                     = 1;
        while (Employer::where('slug', $attributes['slug'])->first()) {
            $attributes['slug'] = str_slug($attributes['company_name']) . '-' . $i++;
        }
        $employer = Employer::create($attributes);
        return $user->employers()->attach($employer['id']);
    }

    public function updateEmployerProfile($attributes = [], $id)
    {
        $user      = $this->find($id);
        $employers = $user->employers;
        if (!$employers->isEmpty()) {
            foreach ($employers as $item) {
                $employer = Employer::find($item['id']);
                $employer->update($attributes);
            }
        }
        return $user;
    }

    public function existsEmail($id, $email)
    {
        $user = $this->findWhere([
            'email' => $email,
            ['id', '!=', $id],
        ])->first();
        if (!$user) {
            return false;
        } else {
            return $user;
        }
    }
}
