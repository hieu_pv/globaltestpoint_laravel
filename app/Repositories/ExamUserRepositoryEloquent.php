<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ExamUserRepository;
use App\Entities\ExamUser;
use App\Validators\ExamUserValidator;

/**
 * Class ExamUserRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ExamUserRepositoryEloquent extends BaseRepository implements ExamUserRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ExamUser::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
