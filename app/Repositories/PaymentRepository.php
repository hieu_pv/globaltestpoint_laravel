<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PaymentRepository
 * @package namespace App\Repositories;
 */
interface PaymentRepository extends RepositoryInterface
{
    public function createCustomer($params);
    public function retrieveCustomer($id_cus);
    public function updateCustomer($id, $params);
    public function deleteCustomers($id_cus);

    public function createCharge($params);
    public function retrieveCharge($id_charge);
    public function updateCharge($id_charge);
    public function deleteCharge($id_charge);
}
