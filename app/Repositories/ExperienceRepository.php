<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ExperienceRepository
 * @package namespace App\Repositories;
 */
interface ExperienceRepository extends RepositoryInterface
{

}
