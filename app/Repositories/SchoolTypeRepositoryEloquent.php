<?php

namespace App\Repositories;

use App\Entities\SchoolType;
use App\Repositories\SchoolTypeRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class SchoolTypeRepositoryEloquent
 * @package namespace App\Repositories;
 */
class SchoolTypeRepositoryEloquent extends BaseRepository implements SchoolTypeRepository {
	/**
	 * Specify Model class name
	 *
	 * @return string
	 */
	public function model() {
		return SchoolType::class;
	}

	/**
	 * Boot up the repository, pushing criteria
	 */
	public function boot() {
		$this->pushCriteria(app(RequestCriteria::class));
	}

    public function getAll() {
        $school_types = SchoolType::orderBy('order', 'desc')->orderBy('id', 'desc')->get();
        return $school_types;
    }
}
