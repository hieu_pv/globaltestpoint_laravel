<?php

namespace App\Repositories;

use App\Entities\Nationality;
use App\Repositories\NationalityRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class NationalityRepositoryEloquent
 * @package namespace App\Repositories;
 */
class NationalityRepositoryEloquent extends BaseRepository implements NationalityRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Nationality::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
