<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SchoolTypeRepository
 * @package namespace App\Repositories;
 */
interface SchoolTypeRepository extends RepositoryInterface
{
    public function getAll();
}
