<?php

namespace App\Repositories;

use App\Entities\Country;
use App\Entities\School;
use App\Entities\SchoolDegree;
use App\Entities\SchoolType;
use App\Entities\State;
use App\Repositories\SchoolRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class SchoolRepositoryEloquent
 * @package namespace App\Repositories;
 */
class SchoolRepositoryEloquent extends BaseRepository implements SchoolRepository {
	/**
	 * Specify Model class name
	 *
	 * @return string
	 */
	public function model() {
		return School::class;
	}

	/**
	 * Boot up the repository, pushing criteria
	 */
	public function boot() {
		$this->pushCriteria(app(RequestCriteria::class));
	}

	public function getAll($active = null) {
		if ($active != null) {
			$schools = School::where('active', $active)->orderBy('rank', 'asc')->orderBy('order', 'desc')->orderBy('id', 'desc')->get();
		} else {
			$schools = School::orderBy('rank', 'asc')->orderBy('order', 'desc')->orderBy('id', 'desc')->get();
		}
		return $schools;
	}

	public function getStateOfSchool($school_id) {
		$school       = School::find($school_id);
		return $state = $school->state;
	}

	public function getCountries() {
		$countries = Country::all();
		return $countries;
	}

	public function getStates($country_id = null) {
		if ($country_id != null) {
			$country = Country::find($country_id);
			$states  = $country->states;
		} else {
			$states = State::all();
		}
		return $states;
	}

	public function getSchoolTypes() {
		$school_types = SchoolType::all();
		return $school_types;
	}

	public function getSchoolDegrees() {
		$school_degrees = SchoolDegree::where('active', 1)->get();
		return $school_degrees;
	}

	public function getSchools($request) {
		if ($request->has('country') && is_int((int) $request->get('country'))) {
			$country_id = (int) $request->get('country');
			$country    = Country::find($country_id);
			if ($country != null) {
				$query = $country->schools()->where('schools.active', 1)->orderBy('rank', 'asc')->orderBy('order', 'desc')->orderBy('id', 'desc');
			} else {
				$query = School::where('active', 1)->orderBy('rank', 'asc')->orderBy('order', 'desc')->orderBy('id', 'desc');
			}
		} else {
			$query = School::where('active', 1)->orderBy('rank', 'asc')->orderBy('order', 'desc')->orderBy('id', 'desc');
		}
		if ($request->has('state') && is_int((int) $request->get('state'))) {
			$state_id = (int) $request->get('state');
			$state    = State::find($state_id);
			if ($state != null) {
				$query->where('state_id', $state_id);
			}
		}
		if ($request->has('schooldegree') && is_int((int) $request->get('schooldegree'))) {
			$schooldegree_id = (int) $request->get('schooldegree');
			$schooldegree    = Schooldegree::find($schooldegree_id);
			if ($schooldegree != null) {
				$query->where('school_degree_id', $schooldegree['id']);
			}
		}
		if ($request->has('schooltype') && is_int((int) $request->get('schooltype'))) {
			$schooltype_id = (int) $request->get('schooltype');
			$schooltype    = SchoolType::find($schooltype_id);
			if ($schooltype != null) {
				$query->where('school_type_id', $schooltype['id']);
			}
		}
		if ($request->has('keyword')) {
			$keyword = $request->get('keyword');
			$search  = '%' . $keyword . '%';
			$query->where('schools.name', 'LIKE', $search);
		}
		return $query->paginate(8);
	}
}
