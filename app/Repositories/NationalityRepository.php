<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface NationalityRepository
 * @package namespace App\Repositories;
 */
interface NationalityRepository extends RepositoryInterface
{

}
