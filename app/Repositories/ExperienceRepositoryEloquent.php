<?php

namespace App\Repositories;

use App\Entities\Experience;
use App\Repositories\ExperienceRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class ExperienceRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ExperienceRepositoryEloquent extends BaseRepository implements ExperienceRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Experience::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
