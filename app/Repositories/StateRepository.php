<?php

namespace App\Repositories;

use App\Entities\Country;
use App\Entities\State;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface StateRepository
 * @package namespace App\Repositories;
 */
interface StateRepository extends RepositoryInterface
{
	public function getAll();
    public function getCountryofState($state_id);
    public function getActiveCountry();
}
