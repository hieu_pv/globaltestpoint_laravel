<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ExamResultRepository
 * @package namespace App\Repositories;
 */
interface ExamResultRepository extends RepositoryInterface
{
    //
}
