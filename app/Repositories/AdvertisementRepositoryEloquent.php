<?php

namespace App\Repositories;

use App\Entities\Advertisement;
use App\Repositories\AdvertisementRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class AdvertisementRepositoryEloquent
 * @package namespace App\Repositories;
 */
class AdvertisementRepositoryEloquent extends BaseRepository implements AdvertisementRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Advertisement::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
