<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AdvertisementRepository
 * @package namespace App\Repositories;
 */
interface AdvertisementRepository extends RepositoryInterface
{

}
