<?php

namespace App\Repositories;

use App\Entities\Application;
use App\Entities\Job;
use App\Entities\Shift;
use App\Entities\User;
use App\Entities\UserAvailability;
use App\Events\NewShiftAddedEvent;
use App\Repositories\CanFlushCache;
use App\Repositories\JobRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Helpers\CacheKeys;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class JobRepositoryEloquent
 * @package namespace App\Repositories;
 */
class JobRepositoryEloquent extends BaseRepository implements JobRepository, CacheableInterface
{
    use CacheableRepository;
    use CanFlushCache;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Job::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function flushCache()
    {
        $keys = CacheKeys::getKeys(get_called_class());
        foreach ($keys as $key) {
            Cache::forget($key);
        }
    }

    public function getAll($condition = [])
    {
        if (count($condition)) {
            $jobs = Job::where($condition)->has('available_shifts')->orderBy('id', 'desc')->simplePaginate();
        } else {
            $jobs = Job::orderBy('id', 'desc')->simplePaginate();
        }
        return $jobs;
    }
    public function apply($job_id, $user_id, $request, $shift_ids)
    {
        $job    = $this->find($job_id);
        $user   = User::find($user_id);
        $shifts = [];
        foreach ($shift_ids as $shift_id) {
            $shifts[$shift_id] = ['job_id' => $job_id];
        }
        DB::table('shift_user')->where('user_id', $user_id)->whereIn('shift_id', $shift_ids)->delete();
        $user->shifts()->attach($shifts);
        $applications = new Collection;
        foreach ($job->shifts as $shift) {
            $applications = $applications->merge($shift->users()->wherePivot('user_id', $user_id)->get());
        }
        return $applications;
    }

    public function cancelPendingJob($user_id, $job_id)
    {
        $user                 = User::findOrFail($user_id);
        $pivot                = $user->applied_jobs()->wherePivot('job_id', $job_id)->firstOrFail();
        $pivot->pivot->active = 0;
        $pivot->pivot->save();
        return $user;
    }

    public function updateShifts($job, $shifts)
    {
        $shifts = is_array($shifts) ? new Collection($shifts) : $shifts;
        foreach ($job->shifts as $item) {
            $find = $shifts->search(function ($shift) use ($item) {
                if (is_array($shift) && isset($shift['id'])) {
                    return $shift['id'] == $item->id;
                }
            });
            if ($find === false) {
                $item->delete();
            }
        }
        $shifts = $shifts->map(function ($item) {
            $item_time_str = UserAvailability::generateAvailableString($item['start_time'], $item['end_time']);
            $data          = [
                'salary'                       => $item['salary'],
                'location'                     => $item['location'],
                'latitude'                     => $item['latitude'],
                'longitude'                    => $item['longitude'],
                'zipcode'                      => $item['zipcode'],
                'on_site_contact'              => $item['on_site_contact'],
                'on_site_contact_phone_number' => $item['on_site_contact_phone_number'],
                'description'                  => isset($item['description']) ? $item['description'] : '',
                'no_of_candidates'             => $item['no_of_candidates'],
                'hourly_rate'                  => $item['hourly_rate'],
                'start_time'                   => $item['start_time'],
                'end_time'                     => $item['end_time'],
                'service_fee'                  => $item['service_fee'],
                'service_fee_percent'          => $item['service_fee_percent'],
                'total_salary'                 => $item['total_salary'],
                'service_fee_discount'         => isset($item['service_fee_discount']) ? $item['service_fee_discount'] : 0,
                'time'                         => $item_time_str,
                'time_duration'                => strlen(str_replace("0", "", $item_time_str)),
                'time_index'                   => strpos($item_time_str, str_replace("0", "", $item_time_str)),
            ];
            if (isset($item['id'])) {
                $data['id'] = $item['id'];
            }
            if (isset($item['job_id'])) {
                $data['job_id'] = $item['job_id'];
            }
            if (isset($item['type'])) {
                $data['type'] = $item['type'];
            }
            $data['no_of_weeks'] = isset($item['no_of_weeks']) ? $item['no_of_weeks'] : '';
            return $data;
        });

        $has_new_shift    = false;
        $new_shifts_added = [];
        foreach ($shifts as $item) {
            if (isset($item['id'])) {
                $s = Shift::updateOrCreate(['id' => $item['id']], [
                    'location'                     => $item['location'],
                    'latitude'                     => $item['latitude'],
                    'longitude'                    => $item['longitude'],
                    'zipcode'                      => $item['zipcode'],
                    'on_site_contact'              => $item['on_site_contact'],
                    'on_site_contact_phone_number' => $item['on_site_contact_phone_number'],
                    'description'                  => $item['description'],
                    'salary'                       => $item['salary'],
                    'hourly_rate'                  => $item['hourly_rate'],
                    'no_of_candidates'             => $item['no_of_candidates'],
                    'start_time'                   => $item['start_time'],
                    'end_time'                     => $item['end_time'],
                    'time'                         => $item['time'],
                    'time_duration'                => $item['time_duration'],
                    'time_index'                   => $item['time_index'],
                    'no_of_weeks'                  => $item['no_of_weeks'],
                    'service_fee'                  => $item['service_fee'],
                    'service_fee_percent'          => $item['service_fee_percent'],
                    'service_fee_discount'         => $item['service_fee_discount'],
                    'total_salary'                 => $item['total_salary'],
                ]);
            } else {
                $has_new_shift = true;

                $s = Shift::create([
                    'location'                     => $item['location'],
                    'latitude'                     => $item['latitude'],
                    'longitude'                    => $item['longitude'],
                    'zipcode'                      => $item['zipcode'],
                    'on_site_contact'              => $item['on_site_contact'],
                    'on_site_contact_phone_number' => $item['on_site_contact_phone_number'],
                    'description'                  => $item['description'],
                    'salary'                       => $item['salary'],
                    'hourly_rate'                  => $item['hourly_rate'],
                    'no_of_candidates'             => $item['no_of_candidates'],
                    'start_time'                   => $item['start_time'],
                    'end_time'                     => $item['end_time'],
                    'no_of_weeks'                  => $item['no_of_weeks'],
                    'time'                         => $item['time'],
                    'time_duration'                => $item['time_duration'],
                    'time_index'                   => $item['time_index'],
                    'service_fee'                  => $item['service_fee'],
                    'service_fee_percent'          => $item['service_fee_percent'],
                    'service_fee_discount'         => $item['service_fee_discount'],
                    'total_salary'                 => $item['total_salary'],
                    'job_id'                       => $job->id,
                    'is_new'                       => 1,
                ]);
                $new_shifts_added[] = $s;
                $ids[]              = $s->id;

                $user_ids = Application::where('job_id', $job->id)->groupBy('user_id')->get()->pluck('user_id');
                if (!empty($user_ids)) {
                    foreach ($user_ids as $user_id) {
                        Application::updateOrCreate(['user_id' => $user_id, 'job_id' => $job->id, 'shift_id' => $s->id]);
                    }
                }
            }
        }
        if ($has_new_shift) {
            Event::fire(new NewShiftAddedEvent($job, $new_shifts_added));
        }
        return $job;
    }

    public function saveShifts($job, $shifts)
    {
        $job->shifts()->saveMany($shifts);
    }

    public function acceptApplication($shift, $employee_id)
    {
        $result = $shift->users()->updateExistingPivot($employee_id, ['accept' => 1]);
        return $result;
    }

    public function completeApplication($shift, $employee_id)
    {
        $result = $shift->users()->updateExistingPivot($employee_id, ['complete' => 1]);
        return $result;
    }

    public function cancelApplication($shift, $employee_id)
    {
        $result = $shift->users()->updateExistingPivot($employee_id, ['accept' => 0]);
        return $result;
    }

}
