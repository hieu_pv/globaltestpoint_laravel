<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PaidRepository;
use App\Entities\Paid;
use App\Validators\PaidValidator;

/**
 * Class PaidRepositoryEloquent
 * @package namespace App\Repositories;
 */
class PaidRepositoryEloquent extends BaseRepository implements PaidRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Paid::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
