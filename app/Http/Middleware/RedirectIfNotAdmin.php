<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfNotAdmin {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @param  string|null  $guard
	 * @return mixed
	 */
	public function handle($request, Closure $next, $guard = null) {
		$user = Auth::user();
		if ($user == null) {
			return redirect('/admin/login');
		} else {
			if (!$user->isAdmin()) {
				return redirect('/admin/login');
			}
		}

		return $next($request);
	}
}