<?php

namespace App\Http\Controllers;

use App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class OptionController extends Controller {

	/**
	 * [feedback description]
	 * @return [type] [description]
	 */
	public function feedback() {
		return view('feedback/index');
	}

	/**
	 * [getfeedback description]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function getfeedback(Request $request) {

		$feedback['tomail'] = "info@globaltestpoint.com";

		$feedback['email']    = $request->get('email');
		$feedback['fullname'] = $request->get('fullname');
		$feedback['subject']  = $request->get('subject');
		$feedback['content']  = $request->get('message');
		Mail::queue('mailletter.feedback', $feedback, function ($message) use ($feedback) {
			$message->from($feedback['email'], $feedback['fullname']);
			$message->to($feedback['tomail'], 'Mr. David')
				->subject($feedback['subject']);
		});
		return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Success !! Send feedback successfully']);
	}
	/**
     * [termpolicy description]
     * @return [NULL] [description]
     */
    public function termpolicy()
    {
        return view('termpolicy.index');
    }
}
