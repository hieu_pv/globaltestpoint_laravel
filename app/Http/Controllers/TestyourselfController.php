<?php

namespace App\Http\Controllers;

use App\Entities\Ads;
use App\Entities\Link;
use App\Entities\Option;
use App\Entities\Slider;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class TestyourselfController extends FrontendController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Slider::where('page', 2)->where('active', 1)->orderBy('sort', 'desc')->orderBy('id', 'desc')->get();

        $video  = Ads::where('type', 0)->where('page', 0)->where('active', 1)->first();
        $flashs = Ads::where('type', 1)->where('page', 0)->where('active', 1)->get();

        $option_site = Option::where('page', 0)->first();
        Session::put('optionsite', $option_site);

        $blankhomepage = Link::with('post')->where('active', 1)->where('page', 2)->where('position', 'homepage')->orderBy('order', 'desc')->get();
        $blankhomepage = $blankhomepage->sortByDesc('post.order');

        $data = [
            'video'         => $video,
            'flashs'        => $flashs,
            'sliders'       => $sliders,
            'blankhomepage' => $blankhomepage,
        ];
        return view('testyourself.home.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
