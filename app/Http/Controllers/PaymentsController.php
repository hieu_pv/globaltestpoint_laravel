<?php

namespace App\Http\Controllers;

use App\Entities\Document;
use App\Entities\Exam;
use App\Entities\Option;
use App\Entities\Paid;
use App\Entities\Subscription;
use App\Entities\User;
use App\Repositories\PaymentRepository;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;

class PaymentsController extends Controller
{
    protected $paystackrepository;
    protected $paystack_sk;
    public function __construct(PaymentRepository $repository)
    {
        \Stripe\Stripe::setApiKey(getenv('STRIPE_SECRET'));
        // \Stripe\Stripe::setApiKey('sk_test_kgSzgjee6mATIMTwBpJKmmkJ');
        $this->paystackrepository = $repository;
    }

    public function test()
    {
        $response = $this->paystackrepository->verify('1503544945');
        dd($response);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type, $id)
    {
        $user = Auth::user();
        if ($type == 'document') {
            $product = Document::find($id);
        } else {
            $product = Exam::find($id);
        }
        if (!empty($user)) {
            $totalprice = $product['price'];
            if (Session::has('tmp_choose')) {
                Session::forget('tmp_choose');
            }
            if (Session::has('backlink')) {
                Session::forget('backlink');
            }
            $backlink         = URL::previous();
            $infor            = '';
            $qty_choose       = 1;
            $paystackresponse = [
                'status' => true,
                'data'   => (object) [
                    'first_name' => (!empty($user->first_name) ? $user->first_name : "NoName"),
                    'last_name'  => $user->last_name,
                    'email'      => $user->email,
                    'phone'      => $user->mobile,
                ],
            ];
            $paystackresponse = (object) $paystackresponse;
            if (!empty($user->paystack_id)) {
                $paystackresponse = $this->paystackrepository->retrieveCustomer($user->paystack_id);
            }
            if (!empty($user->stripe_id)) {
                $infor = $this->getCustomer($user->stripe_id);
            }
            if ($user['active'] != 0) {
                return view('testyourself.payment.index', ['customer_info' => $infor, 'totalprice' => $totalprice, 'backlink' => $backlink, 'paystackresponse' => $paystackresponse, 'product_id' => $product['id'], 'type' => $type, 'qty_choose' => $qty_choose]);
            } else {
                return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => "You must active your account to execute payment !"]);
            }
        } else {
            return redirect()->route('getLoginUser')->with(['flash_level' => 'danger', 'flash_message' => "You need Login/Register to begin payment !"]);
        }
    }

    /**
     * [payment return status after payment]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function payment($type, $id, Request $request)
    {
        $user = Auth::user();
        if (!empty($user)) {
            $data = [
                'email'       => $user->email,
                'description' => 'Billing for ' . $user->last_name . ' from email: ' . $user->email,
            ];
            $token = $request->get('stripeToken');
            //==================================

            if (empty($user->stripe_id)) {
                //=================================================
                $reponseCustomer = $this->createCustomer($data, $token);
                $customer_id     = $reponseCustomer->id;
                $saveCustomer    = $this->saveCustomerToDatabase($reponseCustomer);
                if (!$saveCustomer) {
                    return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => "Don't Save Information Customer"]);
                }
            } else {
                $customer_id = $user->stripe_id;
            }
            // dd($token);
            //================================================
            //==================================
            if ($type == 'document') {
                $product = Document::find($id);
            } elseif ($type == 'exam') {
                $product = Exam::find($id);
            } else {
                return redirect()->route('/testyourself')->with(['flash_level' => 'danger', 'flash_message' => "URL failed !"]);
            }
            $backlink = $request->get('backlink');
            Session::put('backlink', $backlink);
            if (Session::has('fullUrl')) {
                $redirectDocument = Session::get('fullUrl');
            } else {
                $redirectDocument = $backlink;
            }
            $responseCharge = $this->createCharge(($product['price'] * 100), getenv('STRIPE_CURRENCY'), $customer_id, $data['description']);
            if (isset($responseCharge->status) && $responseCharge->status == "succeeded") {
                $price      = ((float) $product['price']);
                $saveCharge = $this->saveTransactionToDatabase($responseCharge);
                if ($saveCharge) {
                    if ($this->savePaidToDatabase($type, $id, $responseCharge->id)) {
                        if ($type == 'document') {
                            $this->sendMailPaymentDocument($user, $price);
                            return view('testyourself.payment.response', ['page' => 'document', 'backlink' => $redirectDocument, 'status' => 1, 'message' => "Payment of " . (getenv('STRIPE_CURRENCY_SYMBOL')) . $product['price'] . " for '" . $product['name'] . "' was successful"]);
                        } else {
                            $this->sendMailPaymentExam($user, $product);
                            $doExam = Session::get('doExam');
                            if ($doExam != null) {
                                return view('testyourself.payment.response', ['page' => 'exam', 'backlink' => url('testyourself/exam/' . $product['slug']), 'status' => 1, 'message' => "Payment of " . (getenv('STRIPE_CURRENCY_SYMBOL')) . $product['price'] . " for '" . $product['name'] . "' was successful"]);
                                // return redirect('testyourself/exam/' . $product['slug'])->with(['flash_level' => 'success', 'flash_message' => "Successfully ! Payment complete with " . $product['price'] . "$ ! You can continue this exam"]);
                            } else {
                                return view('testyourself.payment.response', ['page' => 'exam', 'backlink' => url('testyourself/exams'), 'status' => 1, 'message' => "Payment of " . (getenv('STRIPE_CURRENCY_SYMBOL')) . $product['price'] . " for '" . $product['name'] . "' was successful"]);
                                // return redirect('testyourself/exams')->with(['flash_level' => 'success', 'flash_message' => "Successfully ! Payment complete with " . $product['price'] . "$ ! Now you can test " . $product['name'] . " with full permission."]);
                            }
                        }
                    } else {
                        return view('testyourself.payment.response', ['page' => 'document', 'backlink' => $redirectDocument, 'status' => 0, 'message' => "Don't save information transaction"]);
                    }
                } else {
                    return view('testyourself.payment.response', ['page' => 'document', 'backlink' => $redirectDocument, 'status' => 0, 'message' => "Don't save information transaction"]);
                }
            } else {
                $this->deleteCustomer($customer_id);
                return view('testyourself.payment.response', ['page' => 'document', 'backlink' => $redirectDocument, 'status' => 0, 'message' => "An error occurred while making a payment"]);
            }
        } else {
            return redirect()->route('getLoginUser')->with(['flash_level' => 'danger', 'flash_message' => "You need Login/Register to begin payment !"]);
        }
    }

    /**
     * [multiPayment description]
     * @param  [int] $price       [description]
     * @param  [string] $customer_id [description]
     * @return [boolean]              [description]
     */
    public function multiPayment(Request $request)
    {
        $user = Auth::user();
        if (!empty($user)) {
            $data = [
                'email'       => $user->email,
                'description' => 'Billing for ' . $user->last_name . ' from email: ' . $user->email,
            ];
            //==================================
            if (!empty($request->get('stripeToken'))) {
                $token = $request->get('stripeToken');
            } else {
                $token = '';
            }
            //==================================
            if ($request->has('paystack_submit')) {
                if (empty($user->paystack_id)) {
                    // if ($user->type_payment == 'Paystack') {
                    $params_cus = [
                        'first_name' => (!empty($user->first_name) ? $user->first_name : "NoName"),
                        'last_name'  => $user->last_name,
                        'email'      => $user->email,
                        'phone'      => $user->mobile,
                    ];
                    $paystackresponse = $this->paystackrepository->createCustomer($params_cus);
                    $customer         = (object) [
                        'id' => $paystackresponse->data->id,
                    ];
                    $customer_id  = $paystackresponse->data->id;
                    $saveCustomer = $this->saveCustomerToDatabase($customer, 'Paystack');
                    if (!$saveCustomer) {
                        return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => "Don't Save Information Customer"]);
                    }
                    // }
                } else {
                    $customer_id = $user->paystack_id;
                }
            } else {
                //==================================
                if (empty($user->stripe_id)) {
                    //=================================================

                    // if ($user->type_payment == 'Stripe') {
                    $reponseCustomer = $this->createCustomer($data, $token);
                    $customer_id     = $reponseCustomer->id;
                    $saveCustomer    = $this->saveCustomerToDatabase($reponseCustomer, 'Stripe');
                    if (!$saveCustomer) {
                        return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => "Don't Save Information Customer"]);
                    }
                    // }
                } else {
                    $customer_id = $user->stripe_id;
                }
            }
            if (Session::has('backlink')) {
                $backlink = Session::get('backlink');
                Session::forget('backlink');
            } else {
                $backlink = URL::previous();
            }
            //================================================
            if (Session::has('tmp_choose')) {
                $totalprice = 0;
                $tmp_choose = Session::get('tmp_choose');
                $info_doc   = [];
                // dd($tmp_choose);
                foreach ($tmp_choose as $keydoc => $doc) {
                    $getdoc = Document::find($doc);
                    $totalprice += $getdoc['price'];
                    $info_doc['doc_ids'][] = $doc;
                }
                $qty_choose = count($tmp_choose);
                if ($qty_choose <= 1) {
                    $qty_choose = 1;
                }
                $info_doc['totalprice'] = $totalprice;
                Session::forget('tmp_choose');
                if (Session::has('backlink')) {
                    $backlink = Session::get('backlink');
                    Session::forget('backlink');
                } else {
                    $backlink = URL::previous();
                }

                if ($request->has('paystack_submit')) {
                    $fee_ps        = getenv('PROC_FEE_PAYSTACK');
                    $params_charge = [
                        'callback_url' => $backlink,
                        'email'        => $request->get('email'),
                        'amount'       => (($totalprice * 100) + ((float) $fee_ps * 100 * $qty_choose)),
                        'metadata'     => json_encode($info_doc),
                    ];
                    $responseCharge = $this->paystackrepository->createCharge($params_charge);
                } else {
                    $fee_stripe     = getenv('PERCENT_FEE_STRIPE');
                    $totalprice     = ($totalprice + ceil(($totalprice * ((float) $fee_stripe / 100)))) * 100;
                    $responseCharge = $this->createCharge($totalprice, getenv('STRIPE_CURRENCY'), $customer_id, $data['description']);
                }
                if (isset($responseCharge->status) && ($responseCharge->status == "succeeded" || $responseCharge->status === true)) {
                    $price = $totalprice;
                    if ($request->has('paystack_submit')) {
                        $saveCharge = true;
                    } else {
                        $saveCharge = $this->saveTransactionToDatabase($responseCharge);
                    }
                    if ($saveCharge) {
                        foreach ($tmp_choose as $keydoc => $doc) {
                            if (!$request->has('paystack_submit')) {
                                $getdoc = Document::find($doc);
                                if ($this->savePaidToDatabase('document', $doc, $responseCharge->id)) {
                                    $this->sendMailPaymentDocument($user, $getdoc['price']);
                                    $check = true;
                                } else {
                                    $check = false;
                                    break;
                                }
                            }
                        }
                        if ($request->has('paystack_submit')) {
                            return redirect($responseCharge->data->authorization_url);
                        }
                        if ($check) {
                            return view('testyourself.payment.response', ['page' => 'document', 'backlink' => $backlink, 'status' => 1, 'message' => "Payment of " . (getenv('STRIPE_CURRENCY_SYMBOL')) . ($price / 100) . " was successful"]);
                        } else {
                            return view('testyourself.payment.response', ['page' => 'document', 'backlink' => $backlink, 'status' => 0, 'message' => "Don't save information transaction"]);
                        }
                    } else {
                        return view('testyourself.payment.response', ['page' => 'document', 'backlink' => $backlink, 'status' => 0, 'message' => "Don't save information transaction"]);
                    }
                } else {
                    return view('testyourself.payment.response', ['page' => 'document', 'backlink' => $backlink, 'status' => 0, 'message' => "Payment failed ! Please, try again !"]);
                }
            } else {
                return view('testyourself.payment.response', ['page' => 'document', 'backlink' => $backlink, 'status' => 0, 'message' => "Payment failed ! Please, try again !"]);
            }
        } else {
            return redirect()->route('getLoginUser')->with(['flash_level' => 'danger', 'flash_message' => "You need Login/Register to begin payment !"]);
        }
    }

    public function choiceExams(Request $request)
    {
        // dd($request->get('choicemultiexam'));
        $user = Auth::user();
        if (!empty($user)) {
            if (!empty($request->get('backlink'))) {
                $backlink = $request->get('backlink');
            } else {
                $backlink = URL::previous();
            }
            if (empty($request->get('choicemultiexam'))) {
                return redirect($backlink);
            }

            $tmp_choose = $request->get('choicemultiexam');
            $qty_choose = count($tmp_choose);
            if ($qty_choose <= 1) {
                $qty_choose = 1;
            }
            $totalprice = 0;
            foreach ($tmp_choose as $keydoc => $exam) {
                $getexam = Exam::find($exam);
                $totalprice += $getexam['price'];
                $info_exam['exam_ids'][] = $exam;
            }

            $infor            = '';
            $type             = 'exam';
            $checkmulti       = 1;
            $paystackresponse = [
                'status' => true,
                'data'   => (object) [
                    'first_name' => (!empty($user->first_name) ? $user->first_name : "NoName"),
                    'last_name'  => $user->last_name,
                    'email'      => $user->email,
                    'phone'      => $user->mobile,
                ],
            ];
            $paystackresponse = (object) $paystackresponse;
            if (!empty($user->paystack_id)) {
                $paystackresponse = $this->paystackrepository->retrieveCustomer($user->paystack_id);
            }
            if (!empty($user->stripe_id)) {
                $infor = $this->getCustomer($user->stripe_id);
            }

            return view('testyourself.payment.index-exam', ['customer_info' => $infor, 'totalprice' => $totalprice, 'backlink' => $backlink, 'paystackresponse' => $paystackresponse, 'type' => $type, 'checkmulti' => $checkmulti, 'qty_choose' => $qty_choose, 'info_exam' => json_encode($info_exam)]);
        } else {
            return redirect()->route('getLoginUser')->with(['flash_level' => 'danger', 'flash_message' => "You need Login/Register to begin payment !"]);
        }
    }

    public function handleMultiPaymentExam(Request $request)
    {
        $user = Auth::user();
        if (!empty($user)) {
            $data = [
                'email'       => $user->email,
                'description' => 'Billing for ' . $user->last_name . ' from email: ' . $user->email,
            ];
            //==================================
            if (!empty($request->get('stripeToken'))) {
                $token = $request->get('stripeToken');
            } else {
                $token = '';
            }
            //==================================
            if ($request->has('paystack_submit')) {
                if (empty($user->paystack_id)) {
                    // if ($user->type_payment == 'Paystack') {
                    $params_cus = [
                        'first_name' => (!empty($user->first_name) ? $user->first_name : "NoName"),
                        'last_name'  => $user->last_name,
                        'email'      => $user->email,
                        'phone'      => $user->mobile,
                    ];
                    $paystackresponse = $this->paystackrepository->createCustomer($params_cus);
                    $customer         = (object) [
                        'id' => $paystackresponse->data->id,
                    ];
                    $customer_id  = $paystackresponse->data->id;
                    $saveCustomer = $this->saveCustomerToDatabase($customer, 'Paystack');
                    if (!$saveCustomer) {
                        return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => "Don't Save Information Customer"]);
                    }
                    // }
                } else {
                    $customer_id = $user->paystack_id;
                }
            } else {
                //==================================
                if (empty($user->stripe_id)) {
                    //=================================================

                    // if ($user->type_payment == 'Stripe') {
                    $reponseCustomer = $this->createCustomer($data, $token);
                    $customer_id     = $reponseCustomer->id;
                    $saveCustomer    = $this->saveCustomerToDatabase($reponseCustomer, 'Stripe');
                    if (!$saveCustomer) {
                        return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => "Don't Save Information Customer"]);
                    }
                    // }
                } else {
                    $customer_id = $user->stripe_id;
                }
            }
            if (empty($request->get('info_exam'))) {
                return redirect()->back();
            }
            if (!empty($request->get('backlink'))) {
                $backlink = $request->get('backlink');
            } else {
                $backlink = URL::previous();
            }
            $info_exam  = json_decode($request->get('info_exam'), true);
            $totalprice = 0;
            $exams      = [];
            if (empty($info_exam['exam_ids'])) {
                return redirect($backlink);
            }
            $qty_choose = count($info_exam['exam_ids']);
            foreach ($info_exam['exam_ids'] as $keydoc => $exam) {
                $getexam = Exam::find($exam);
                $totalprice += $getexam['price'];
                $exams[$getexam->id] = $getexam;
            }
            $metadata = [
                'infor_exam' => $info_exam['exam_ids'],
                'backlink'   => $backlink,
                'totalprice' => $totalprice,
                'type'       => 'exam',
            ];
            if ($request->has('paystack_submit')) {
                $fee_ps        = getenv('PROC_FEE_PAYSTACK');
                $params_charge = [
                    'callback_url' => url('testyourself/multi-payment-exam-with-paystack-callback'),
                    'email'        => $request->get('email'),
                    'amount'       => (($totalprice * 100) + ((float) $fee_ps * 100 * $qty_choose)),
                    'metadata'     => json_encode($metadata),
                ];
                $responseCharge = $this->paystackrepository->createCharge($params_charge);
            } else {
                $fee_stripe     = getenv('PERCENT_FEE_STRIPE');
                $totalprice     = ($totalprice + ceil(($totalprice * ((float) $fee_stripe / 100)))) * 100;
                $responseCharge = $this->createCharge($totalprice, getenv('STRIPE_CURRENCY'), $customer_id, $data['description']);
            }
            if (isset($responseCharge->status) && ($responseCharge->status == "succeeded" || $responseCharge->status === true)) {
                if ($request->has('paystack_submit')) {
                    $saveCharge = true;
                } else {
                    $saveCharge = $this->saveTransactionToDatabase($responseCharge);
                }
                if ($saveCharge) {
                    foreach ($info_exam['exam_ids'] as $keydoc => $doc) {
                        if (!$request->has('paystack_submit')) {
                            $exam = Paid::where('product_id', $doc)->where('type', 'exam')->where('user_id', $user->id)->first();
                            if (empty($exam)) {
                                $this->savePaidToDatabase('exam', $doc, $responseCharge->id);
                                $this->sendMailPaymentExam($user, $exams[$doc]);
                            }
                        }
                    }
                    if ($request->has('paystack_submit')) {
                        return redirect($responseCharge->data->authorization_url);
                    }
                    return view('testyourself.payment.response', ['page' => 'document', 'backlink' => $backlink, 'status' => 1, 'message' => "Payment of " . (getenv('STRIPE_CURRENCY_SYMBOL')) . ($totalprice / 100) . " was successful"]);
                } else {
                    return view('testyourself.payment.response', ['page' => 'document', 'backlink' => $backlink, 'status' => 0, 'message' => "Don't save information transaction"]);
                }
            } else {
                return view('testyourself.payment.response', ['page' => 'document', 'backlink' => $backlink, 'status' => 0, 'message' => "Payment failed ! Please, try again !"]);
            }
        }
    }

    public function handleMultiPaymentExamCallback(Request $request)
    {
        if (empty($request->all())) {
            return redirect(url());
        }
        $user = Auth::user();
        if (!empty($user)) {
            $verify = $this->paystackrepository->verify($request->get('reference'));
            if ($verify->status) {
                $retrieveTransaction = $this->paystackrepository->retrieveCharge($verify->data->id);
                if (!empty($verify->data->metadata->infor_exam)) {
                    $this->saveTransactionToDatabase($retrieveTransaction, 'Paystack');
                    foreach ($verify->data->metadata->infor_exam as $key => $val_id) {
                        $exam = Paid::where('product_id', $val_id)->where('type', 'exam')->where('user_id', $user->id)->first();
                        if (empty($exam)) {
                            $this->savePaidToDatabase('exam', $val_id, $retrieveTransaction->data->id);
                        }
                    }
                    $fee_ps     = getenv('PROC_FEE_PAYSTACK');
                    $qty_choose = count($verify->data->metadata->infor_exam);
                    $totalprice = ($verify->data->metadata->totalprice) + ((float) $fee_ps * $qty_choose);
                    return view('testyourself.payment.response', ['page' => 'exam', 'backlink' => $verify->data->metadata->backlink, 'status' => 1, 'message' => "Payment of " . (getenv('STRIPE_CURRENCY_SYMBOL')) . ($totalprice) . " was successful"]);
                } else {
                    return view('testyourself.payment.response', ['page' => 'document', 'backlink' => $verify->data->metadata->referrer, 'status' => 0, 'message' => "Can't save information transaction"]);
                }
            } else {
                return view('testyourself.payment.response', ['page' => 'document', 'backlink' => $verify->data->metadata->referrer, 'status' => 0, 'message' => "Can't save information transaction"]);
            }
        } else {
            return redirect()->route('getLoginUser')->with(['flash_level' => 'danger', 'flash_message' => "You need Login/Register to begin payment !"]);
        }
    }

    public function handlePaystack(Request $request, $type)
    {
        $id = $request->get('product_id');
        if (empty($id)) {
            return redirect()->route('/testyourself')->with(['flash_level' => 'danger', 'flash_message' => "URL failed !"]);
        }

        if ($request->get('exist') === 'no') {
            $params_cus = [
                'first_name' => $request->get('first_name'),
                'last_name'  => $request->get('last_name'),
                'email'      => $request->get('email'),
                'phone'      => $request->get('phone'),
            ];
            $response = $this->paystackrepository->createCustomer($params_cus);
            if ($response->status) {
                $customer = (object) [
                    'id' => $response->data->id,
                ];
                $this->saveCustomerToDatabase($customer, 'Paystack');
            }
        }

        if ($type == 'document') {
            $product = Document::find($id);
        } elseif ($type == 'exam') {
            $product = Exam::find($id);
        } else {
            return redirect()->route('/testyourself')->with(['flash_level' => 'danger', 'flash_message' => "URL failed !"]);
        }

        if (!empty($request->get('backlink'))) {
            $meta['backlink'] = $request->get('backlink');
        } else {
            $meta['backlink'] = URL::previous();
        }
        $fee_ps = getenv('PROC_FEE_PAYSTACK');
        $params = [
            'callback_url' => url('testyourself/payment-with-paystack-callback/' . $type . '/' . $id),
            'email'        => $request->get('email'),
            'amount'       => (($product['price'] * 100) + ((float) $fee_ps * 100)),
            'metadata'     => json_encode($meta),
        ];

        $response = $this->paystackrepository->createCharge($params);
        if ($response->status) {
            return redirect($response->data->authorization_url);
        } else {
            return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => "Can't create charge. Please, try again !"]);
        }
    }

    public function handlePaystackCallback(Request $request, $type, $id)
    {
        if (empty($request->all())) {
            return redirect(url());
        }
        $user = Auth::user();
        if (!empty($user)) {
            $verify = $this->paystackrepository->verify($request->get('reference'));
            if ($verify->status) {
                $retrieveTransaction = $this->paystackrepository->retrieveCharge($verify->data->id);
                $this->saveTransactionToDatabase($retrieveTransaction, 'Paystack');
                if ($type == 'document') {
                    $product = Document::find($id);
                } elseif ($type == 'exam') {
                    $product = Exam::find($id);
                } else {
                    return redirect()->route('/testyourself')->with(['flash_level' => 'danger', 'flash_message' => "URL failed !"]);
                }
                $this->savePaidToDatabase($type, $id, $retrieveTransaction->data->id);

                if ($type == 'exam') {
                    $this->sendMailPaymentExam($user, $product);
                    $doExam = Session::get('doExam');
                    if ($doExam != null) {
                        return view('testyourself.payment.response', ['page' => 'exam', 'backlink' => url('testyourself/exam/' . $product['slug']), 'status' => 1, 'message' => "Payment of " . (getenv('STRIPE_CURRENCY_SYMBOL')) . $product['price'] . " for '" . $product['name'] . "' was successful"]);
                    } else {
                        return view('testyourself.payment.response', ['page' => 'exam', 'backlink' => url('testyourself/exams'), 'status' => 1, 'message' => "Payment of " . (getenv('STRIPE_CURRENCY_SYMBOL')) . $product['price'] . " for '" . $product['name'] . "' was successful"]);
                    }
                } else {
                    $this->sendMailPaymentDocument($user, ((float) ($retrieveTransaction->data->amount) / 100));
                    return view('testyourself.payment.response', ['page' => 'document', 'backlink' => $verify->data->metadata->backlink, 'status' => 1, 'message' => "Payment of " . (getenv('STRIPE_CURRENCY_SYMBOL')) . $product['price'] . " for '" . $product['name'] . "' was successful"]);
                }
            } else {
                return view('testyourself.payment.response', ['page' => 'document', 'backlink' => $verify->data->metadata->referrer, 'status' => 0, 'message' => "Don't save information transaction"]);
            }
        } else {
            return redirect()->route('getLoginUser')->with(['flash_level' => 'danger', 'flash_message' => "You need Login/Register to begin payment !"]);
        }
    }

    public function sendMailPaymentDocument($user, $price)
    {
        $option_tys = Option::where('page', 0)->first();
        Mail::queue('mails.paymentdocument', ['user' => $user, 'price' => $price, 'option_tys' => $option_tys], function ($message) use ($user, $price) {
            $message->from(getenv('MAIL_USERNAME'), 'Confirmation');
            $message->to($user->email)->subject('Dear ' . $user->first_name);
        });
    }

    public function sendMailPaymentExam($user, $exam)
    {
        $option_tys      = Option::where('page', 0)->first();
        $currency_symbol = getenv('STRIPE_CURRENCY_SYMBOL');
        Mail::queue('mails.paymentExam', ['user' => $user, 'exam' => $exam, 'currency_symbol' => $currency_symbol, 'option_tys' => $option_tys], function ($message) use ($user, $exam) {
            $message->from(getenv('MAIL_USERNAME'), 'Confirmation');
            $message->to($user->email)->subject('Dear ' . $user->first_name);
        });
    }

    public function updateCard(Request $request)
    {
        if (Auth::check()) {
            $params = [];
            $user   = Auth::user();
            if (!empty($request->get('first_name'))) {
                $params['first_name'] = $request->get('first_name');
            }
            if (!empty($request->get('last_name'))) {
                $params['last_name'] = $request->get('last_name');
            }
            if (!empty($request->get('phone'))) {
                $params['phone'] = $request->get('phone');
            }
            if (!empty($request->get('email'))) {
                $params['email'] = $request->get('email');
                $response        = $this->paystackrepository->createCustomer($params);
                if ($response->status) {
                    $customer = (object) [
                        'id' => $response->data->id,
                    ];
                    $save = $this->saveCustomerToDatabase($customer, 'Paystack');
                    if (!$save) {
                        return redirect(URL::previous())->with(['flash_level' => 'danger', 'flash_message' => "Can't save your information !"]);
                    }
                }
            } else {
                $response = $this->paystackrepository->updateCustomer($user->paystack_id, $params);
            }
            if ($response->status) {
                return redirect(URL::previous())->with(['flash_level' => 'success', 'flash_message' => "Update information success !", 'backlink_url' => $request->get('backlink_url')]);
            } else {
                return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => $response->message, 'backlink_url' => $request->get('backlink_url')]);
            }
        } else {
            return redirect()->route('getLoginUser')->with(['flash_level' => 'danger', 'flash_message' => "You need Login/Register to begin payment !"]);
        }
    }

    public function updateCardMulti(Request $request)
    {
        if (Auth::check()) {
            $params = [];
            $user   = Auth::user();
            if (!empty($request->get('first_name'))) {
                $params['first_name'] = $request->get('first_name');
            }
            if (!empty($request->get('last_name'))) {
                $params['last_name'] = $request->get('last_name');
            }
            if (!empty($request->get('phone'))) {
                $params['phone'] = $request->get('phone');
            }
            if (!empty($request->get('email'))) {
                $params['email'] = $request->get('email');
                $response        = $this->paystackrepository->createCustomer($params);
                if ($response->status) {
                    $customer = (object) [
                        'id' => $response->data->id,
                    ];
                    $save = $this->saveCustomerToDatabase($customer, 'Paystack');
                    if (!$save) {
                        $data_res = [
                            'code'    => 0,
                            'message' => "Can't save your information !",
                        ];
                        return json_encode($data_res);
                    }
                }
            } else {
                $response = $this->paystackrepository->updateCustomer($user->paystack_id, $params);
            }
            if ($response->status) {
                $data_res = [
                    'code'    => 1,
                    'message' => 'Update your information successful !',
                    'data'    => $params,
                ];
                return json_encode($data_res);
            } else {
                $data_res = [
                    'code'    => 0,
                    'message' => $response->message,
                ];
                return json_encode($data_res);
            }
        } else {
            $data_res = [
                'code'    => 0,
                'message' => "You need Login/Register to begin payment !",
            ];
            return json_encode($data_res);
        }
    }

    /**
     * [transactionpaid save data to database]
     * @param  [string] $type           [description]
     * @param  [int] $product_id     [description]
     * @param  [string] $transaction_id [id transaction when paid]
     * @return [boolean]                 [description]
     */
    public function savePaidToDatabase($type, $product_id, $transaction_id)
    {
        $current_user = Auth::user();
        $user         = User::find($current_user->id);
        if (!empty($user)) {
            $paid                 = new Paid;
            $paid->user_id        = $user['id'];
            $paid->product_id     = $product_id;
            $paid->transaction_id = $transaction_id;
            $paid->type           = $type;
            if ($paid->save()) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * [createCustomer description]
     * @param  [array] $data  [consist email and description]
     * @param  [type] $token [description]
     * @return [type]        [description]
     */
    public function createCustomer($data, $token)
    {
        try {
            $customer = \Stripe\Customer::create([
                "source"      => $token,
                'email'       => $data['email'],
                "description" => $data['description'],
            ]);
            return $customer;
        } catch (\Stripe\Error\Card $e) {
            // return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => $e . error . message]);
            return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => "Don't have create Stripe account !"]);
        }
    }

    /**
     * [getCustomer get inforation of custome execute billing]
     * @param  [type] $customer_id [description]
     * @return [type]              [description]
     */
    public function getCustomer($customer_id)
    {
        try {
            $response = \Stripe\Customer::retrieve($customer_id);
            return $response;
        } catch (\Stripe\Error\Card $e) {
            return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => $e . error . message]);
        }
    }

    public function updateCustomer($customer_id, $params)
    {
        try {
            $response         = \Stripe\Customer::retrieve($customer_id);
            $response->source = [
                'object'    => 'card',
                'number'    => $params['number'],
                'exp_month' => $params['exp_month'],
                'exp_year'  => $params['exp_year'],
                'cvc'       => $params['cvc'],
            ];
            $response = $response->save();
            return $response;
        } catch (\Stripe\Error\Card $e) {
            return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => $e . error . message]);
        }
    }

    /**
     * [saveToDatabase description]
     * @param  [type] $customer [Response return when billing complete on Stripe]
     * @return [boolean]              [save complete so return true]
     */
    public function saveCustomerToDatabase($customer, $type = 'Stripe')
    {
        $current_user = Auth::user();
        $user         = User::find($current_user->id);

        if ($type === 'Stripe') {
            $user->stripe_id = $customer->id;
        } else {
            $user->paystack_id = $customer->id;
        }
        $user->type_payment = $type;
        if ($user->save()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * [charge billing for product by customer id]
     * @param  integer $price       price of product
     * @param  string  $currency    current, example : usd, vnd, ...
     * @param  [type]  $customer_id Customer_id is stripe_id
     * @param  string  $description can exist or not
     * @return [type]               return infor transaction when billing complete
     */
    public function createCharge($price, $currency = 'usd', $customer_id, $description = '')
    {
        try {
            $charge = \Stripe\Charge::create([
                "amount"      => $price, // amount in cents
                "currency"    => $currency,
                "description" => $description,
                "customer"    => $customer_id,
            ]);
            return $charge;
        } catch (\Stripe\Error\Card $e) {
            return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Your card is cannot billing']);
        }
    }

    /**
     * [getIdCharge description]
     * @param  [type] $charge_id [description]
     * @return [type]            [description]
     */
    public function getCharge($charge_id)
    {
        try {
            $response = \Stripe\Charge::retrieve($charge_id);
        } catch (\Stripe\Error\Card $e) {
            return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => $e . error . message]);
        }
        return $response;
    }

    /**
     * [saveTransactionToDatabase save information of customer transaction to database]
     * @param  [type] $charge [response result when billing complete]
     * @return [boolean]         [return true if save successful]
     */
    public function saveTransactionToDatabase($charge, $type = 'Stripe')
    {
        $current_user = Auth::user();
        $user         = User::find($current_user->id);

        $subscription                 = new Subscription;
        $subscription->user_id        = $user['id'];
        $subscription->name           = ((!empty($user['last_name'])) ? $user['last_name'] : $user['first_name']);
        $subscription->stripe_id      = ($type === 'Stripe') ? $charge->customer : $user['paystack_id'];
        $subscription->transaction_id = ($type === 'Stripe') ? $charge->id : $charge->data->id;
        if ($subscription->save()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param  [type] $customer_id [description]
     * @return [boolean]              [return true so billing complete]
     */
    public function deleteCustomer($customer_id)
    {
        $user            = User::where('customer_id', $customer_id)->get();
        $user->stripe_id = '';

        $subscription = Subscription::where('user_id', $user->id);
        $subscription->delete();

        $cu       = \Stripe\Customer::retrieve($customer_id);
        $response = $cu->delete();
        if ($response->delete) {
            return true;
        } else {
            return false;
        }
    }

    public function switcherCurrencyAjax()
    {
        $amount           = 100;
        $from             = $_GET['from'];
        $to               = $_GET['to'];
        $get              = file_get_contents("https://www.google.com/finance/converter?a=$amount&from=" . $from . "&to=" . $to);
        $get              = explode("<span class=bld>", $get);
        $get              = explode("</span>", $get[1]);
        $converted_amount = $get[0];
        // dd($converted_amount);
        return round($converted_amount, 2);
    }
}
