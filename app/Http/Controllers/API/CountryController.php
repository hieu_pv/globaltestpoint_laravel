<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\ApiController;
use App\Repositories\CountryRepository;
use App\Transformers\CountryTransformer;
use App\Validators\CountryValidator;

class CountryController extends ApiController
{
    private $repository;
    private $validator;

    public function __construct(CountryRepository $repository, CountryValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$countries = $this->repository->scopeQuery(function ($query) {
			$query = $query->orderBy('order', 'asc');
			return $query;
		})->all();
		return $this->response->collection($countries, new CountryTransformer);
	}
}
