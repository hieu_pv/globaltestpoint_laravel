<?php

namespace App\Http\Controllers\API;

use App\Entities\City;
use App\Entities\Job;
use App\Entities\State;
use App\Http\Controllers\API\ApiController;
use App\Repositories\JobRepository;
use App\Transformers\JobTransformer;
use App\Validators\JobValidator;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class JobController extends ApiController
{
    const STATUS_ACTIVE  = 1;
    const STATUS_PENDING = 0;

    const NORMAL_JOB_TYPE        = 1;
    const INTERNATIONAL_JOB_TYPE = 2;

    private $repository;
    private $validator;

    public function __construct(JobRepository $repository, JobValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    public function index(Request $request)
    {
        $query = new Job;

        $constraints = (array) json_decode($request->get('constraints'));
        if (count($constraints)) {
            $query = $query->where($constraints);
        }

        $query = $query->where(Job::STATE['JOB_ACTIVE']);

        if ($request->has('order_by')) {
            $orderBy = (array) json_decode($request->get('order_by'));
            if (count($orderBy)) {
                foreach ($orderBy as $key => $value) {
                    $query = $query->orderBy($key, $value);
                }
            }
        } else {
            $query = $query->orderBy('order', 'asc');
            $query = $query->orderBy('id', 'asc');
        }

        if ($request->has('keyword')) {
            $keywords = explode(',', $request->get('keyword'));
            if (!empty($keywords)) {
                foreach ($keywords as $key => $keyword) {
                    $keyword = trim($keyword);
                    if ($key == 0) {
                        $query = $query->where('title', 'like', "%{$keyword}%");
                    } else {
                        $query = $query->orWhere('title', 'like', "%{$keyword}%");
                    }
                }
            }
        }

        $city_ids = [];
        if ($request->has('location')) {
            $locations = explode(',', $request->get('location'));
            if (!empty($locations)) {
                $location_query = new City;
                foreach ($locations as $key => $location) {
                    $location = trim($location);

                    if ($key == 0) {
                        $location_query = $location_query->where('name', $location);
                    } else {
                        $location_query = $location_query->orWhere('name', $location);
                    }
                }

                $locations_db = $location_query->get();
                if (!$locations_db->isEmpty()) {
                    $city_ids = $locations_db->pluck('id')->all();
                } else {
                    $query = $query->where('city_id', 10000);
                }
            }
        }

        if ($request->has('filter_state_ids')) {
            $filter_state_ids = explode(',', $request->get('filter_state_ids'));
            $states           = State::whereIn('id', $filter_state_ids)->get();
            if (!$states->isEmpty()) {
                foreach ($states as $state) {
                    $city_ids = array_merge($city_ids, $state->cities->pluck('id')->toArray());
                }
            }
        }
        if (!empty($city_ids)) {
            $query = $query->whereIn('city_id', $city_ids);
        }

        $experience_ids = [];
        if ($request->has('experience_id')) {
            $experience_ids[] = $request->get('experience_id');
        }
        if ($request->has('filter_experience_ids')) {
            $experience_ids = array_merge($experience_ids, explode(',', $request->get('filter_experience_ids')));
        }
        if (!empty($experience_ids)) {
            $query = $query->whereIn('experience_id', $experience_ids);
        }

        if ($request->has('filter_industry_ids')) {
            $filter_industry_ids = explode(',', $request->get('filter_industry_ids'));
            $query->whereHas('industries', function ($q) use ($filter_industry_ids) {
                $q->whereIn('id', $filter_industry_ids);
            });
        }

        if ($request->has('filter_job_title_ids')) {
            $filter_job_title_ids = explode(',', $request->get('filter_job_title_ids'));
            $query                = $query->whereIn('job_title_id', $filter_job_title_ids);
        }

        if ($request->has('filter_qualification_ids')) {
            $filter_qualification_ids = explode(',', $request->get('filter_qualification_ids'));
            $query                    = $query->whereIn('qualification_id', $filter_qualification_ids);
        }

        $page     = $request->has('page') ? (int) $request->get('page') : 1;
        $per_page = $request->has('per_page') ? (int) $request->get('per_page') : 15;

        $total_jobs = $query->count();

        $jobs = $query->skip(($page - 1) * $per_page)->take($per_page)->get();

        $result = new LengthAwarePaginator($jobs, $total_jobs, $per_page, $page);

        if ($request->has('includes')) {
            $transformer = new JobTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new JobTransformer;
        }

        return $this->response->paginator($result, $transformer);
    }

    public function getAll(Request $request)
    {
        $query = new Job;

        $constraints = (array) json_decode($request->get('constraints'));
        if (count($constraints)) {
            $query = $query->where($constraints);
        }

        $query = $query->where(Job::STATE['JOB_ACTIVE']);

        if ($request->has('order_by')) {
            $orderBy = (array) json_decode($request->get('order_by'));
            if (count($orderBy)) {
                foreach ($orderBy as $key => $value) {
                    $query = $query->orderBy($key, $value);
                }
            }
        } else {
            $query = $query->orderBy('order', 'asc');
            $query = $query->orderBy('id', 'asc');
        }

        if ($request->has('keyword')) {
            $keywords = explode(',', $request->get('keyword'));
            if (!empty($keywords)) {
                foreach ($keywords as $key => $keyword) {
                    $keyword = trim($keyword);
                    if ($key == 0) {
                        $query = $query->where('title', 'like', "%{$keyword}%");
                    } else {
                        $query = $query->orWhere('title', 'like', "%{$keyword}%");
                    }
                }
            }
        }

        $city_ids = [];
        if ($request->has('location')) {
            $locations = explode(',', $request->get('location'));
            if (!empty($locations)) {
                $location_query = new City;
                foreach ($locations as $key => $location) {
                    $location = trim($location);

                    if ($key == 0) {
                        $location_query = $location_query->where('name', $location);
                    } else {
                        $location_query = $location_query->orWhere('name', $location);
                    }
                }

                $locations_db = $location_query->get();
                if (!$locations_db->isEmpty()) {
                    $city_ids = $locations_db->pluck('id')->all();
                } else {
                    $query = $query->where('city_id', 10000);
                }
            }
        }
        if ($request->has('filter_city_ids')) {
            $city_ids = array_merge($city_ids, explode(',', $request->get('filter_city_ids')));
        }
        if (!empty($city_ids)) {
            $query = $query->whereIn('city_id', $city_ids);
        }

        $experience_ids = [];
        if ($request->has('experience_id')) {
            $experience_ids[] = $request->get('experience_id');
        }
        if ($request->has('filter_experience_ids')) {
            $experience_ids = array_merge($experience_ids, explode(',', $request->get('filter_experience_ids')));
        }
        if (!empty($experience_ids)) {
            $query = $query->whereIn('experience_id', $experience_ids);
        }

        if ($request->has('filter_industry_ids')) {
            $filter_industry_ids = explode(',', $request->get('filter_industry_ids'));
            $query->whereHas('industries', function ($q) use ($filter_industry_ids) {
                $q->whereIn('id', $filter_industry_ids);
            });
        }

        if ($request->has('filter_job_title_ids')) {
            $filter_job_title_ids = explode(',', $request->get('filter_job_title_ids'));
            $query                = $query->whereIn('job_title_id', $filter_job_title_ids);
        }

        if ($request->has('filter_qualification_ids')) {
            $filter_qualification_ids = explode(',', $request->get('filter_qualification_ids'));
            $query                    = $query->whereIn('qualification_id', $filter_qualification_ids);
        }

        $jobs = $query->get();

        if ($request->has('includes')) {
            $transformer = new JobTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new JobTransformer;
        }

        return $this->response->collection($jobs, $transformer);
    }

    public function getJobBySlug($slug, Request $request)
    {
        $job = Job::where('slug', $slug)->first();
        if (!$job) {
            throw new NotFoundException("Job");
        }

        if ($request->has('includes')) {
            $transformer = new JobTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new JobTransformer;
        }

        return $this->response->item($job, $transformer);
    }

    public function getSimilarJobs($id, Request $request)
    {
        $job = Job::find($id);
        if (!$job) {
            throw new NotFoundException("Job");
        }

        $query = new Job;
        $query = $query->where('id', '!=', $job->id);
        $query = $query->whereHas('industries', function ($q) use ($job) {
            $q->whereIn('id', $job->industries->pluck('id')->all());
        });

        if ($request->has('order_by')) {
            $orderBy = (array) json_decode($request->get('order_by'));
            if (count($orderBy)) {
                foreach ($orderBy as $key => $value) {
                    $query = $query->orderBy($key, $value);
                }
            }
        } else {
            $query = $query->orderBy('order', 'asc');
            $query = $query->orderBy('id', 'asc');
        }

        $page     = $request->has('page') ? (int) $request->get('page') : 1;
        $per_page = $request->has('per_page') ? (int) $request->get('per_page') : 15;

        $total_jobs = $query->count();

        $jobs = $query->skip(($page - 1) * $per_page)->take($per_page)->get();

        $result = new LengthAwarePaginator($jobs, $total_jobs, $per_page, $page);

        if ($request->has('includes')) {
            $transformer = new JobTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new JobTransformer;
        }

        return $this->response->paginator($result, $transformer);
    }
}
