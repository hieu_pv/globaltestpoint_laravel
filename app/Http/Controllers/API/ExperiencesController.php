<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\ApiController;
use App\Repositories\ExperienceRepository;
use App\Transformers\ExperienceTransformer;
use App\Validators\ExperienceValidator;
use Illuminate\Http\Request;

class ExperiencesController extends ApiController
{
    const STATUS_ACTIVE  = 1;
    const STATUS_PENDING = 0;

    private $repository;
    private $validator;

    public function __construct(ExperienceRepository $repository, ExperienceValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $experiences = $this->repository->scopeQuery(function ($query) {
            $query = $query->where('status', self::STATUS_ACTIVE);
            $query = $query->orderBy('order', 'asc');
            $query = $query->orderBy('id', 'asc');
            return $query;
        })->all();
        return $this->response->collection($experiences, new ExperienceTransformer);
    }
}
