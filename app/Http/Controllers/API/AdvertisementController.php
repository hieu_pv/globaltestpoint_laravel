<?php

namespace App\Http\Controllers\API;

use App\Entities\Advertisement;
use App\Http\Controllers\API\ApiController;
use App\Repositories\AdvertisementRepository;
use App\Transformers\AdvertisementTransformer;
use App\Validators\AdvertisementValidator;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class AdvertisementController extends ApiController
{
    const STATUS_ACTIVE  = 1;
    const STATUS_PENDING = 0;

    private $repository;
    private $validator;

    public function __construct(AdvertisementRepository $repository, AdvertisementValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new Advertisement;
        $query = $query->where('status', self::STATUS_ACTIVE);
        $query = $query->orderBy('order', 'asc');
        $query = $query->orderBy('id', 'asc');

        $constraints = (array) json_decode($request->get('constraints'));
        if (count($constraints)) {
            $query = $query->where($constraints);
        }

        if ($request->has('search')) {
            $search = $request->get('search');
            $query  = $query->where(function ($q) use ($request, $search) {
                $q->where('name', 'like', "%{$search}%");
            });
        }

        if ($request->has('order_by')) {
            $orderBy = (array) json_decode($request->get('order_by'));
            if (count($orderBy) > 0) {
                foreach ($orderBy as $key => $value) {
                    $query = $query->orderBy($key, $value);
                }
            }
        }

        $total_records  = $query->count();
        $page           = $request->has('page') ? (int) $request->get('page') : 1;
        $per_page       = $request->has('per_page') ? (int) $request->get('per_page') : 15;
        $advertisements = $query->skip(($page - 1) * $per_page)->take($per_page)->get();

        $transformer = new AdvertisementTransformer;

        $result = new LengthAwarePaginator($advertisements, $total_records, $per_page, $page);
        return $this->paginator($result, $transformer);
    }

    public function getAll(Request $request)
    {
        $constraints = (array) json_decode($request->get('constraints'));

        $advertisements = $this->repository->scopeQuery(function ($query) use ($constraints) {
            $query = $query->where('status', self::STATUS_ACTIVE);
            $query = $query->orderBy('order', 'asc');
            $query = $query->orderBy('id', 'asc');

            if (count($constraints)) {
                $query = $query->where($constraints);
            }

            return $query;
        })->all();
        return $this->response->collection($advertisements, new AdvertisementTransformer);
    }
}
