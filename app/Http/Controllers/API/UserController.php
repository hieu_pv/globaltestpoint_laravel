<?php

namespace App\Http\Controllers\API;

use App\Entities\Option;
use App\Entities\User;
use App\Http\Controllers\API\ApiController;
use App\Transformers\UserTransformer;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class UserController extends ApiController
{
    public function me()
    {
        if (Auth::check()) {
            return $this->response->item(Auth::user(), new UserTransformer);
        } else {
            throw new \Exception("No user logged in at this time", 1);
        }
    }

    public function store(Request $request)
    {
        $data = $request->all();
        if (User::where('email', $request->get('email'))->count()) {
            throw new \Exception("Email already exist", 1);
        }
        $user = User::create($data);
        if ($request->has('password')) {
            $user->image          = 'uploads/default_avatar.png';
            $user->password       = Hash::make($request->get('password'));
            $user->email_verified = md5($user->email) . uniqid();
            $user->save();
            $user->attachRole($data['role']['id']);
            $option_tys = Option::where('page', 0)->first();
            Mail::queue('mails.admin_create_user', ['user' => $user, 'password' => $request->get('password'), 'option_tys' => $option_tys], function ($message) use ($user) {
                $message->from(getenv('MAIL_USERNAME'), 'GlobalTestPoint');
                $message->to($user->email)->subject('Registration successful ! globaltestpoint');
                $user['image'] = $message->embed(asset($user->image));
            });
        }
        return $this->response->item($user, new UserTransformer);
    }

    public function hasLoggedIn(Request $request)
    {
        $user = Auth::user();
        if ($user) {
            return $this->response->item($user, new UserTransformer);
        } else {
            return $this->success();
        }
    }
}
