<?php

namespace App\Http\Controllers\API;

use App\Events\FileUpload;
use App\Exceptions\NotFoundException;
use App\Http\Controllers\API\ApiController;
use App\Validators\FileValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class FilesController extends ApiController
{

    private $validator;

    public function __construct(FileValidator $validator)
    {
        $this->validator = $validator;
    }

    public function upload(Request $request)
    {
        $data = $request->all();
        $this->validator->isValid($data, 'RULE_CREATE');

        if (!$request->hasFile('file')) {
            throw new NotFoundException("File");
        } else {

            $id             = $request->get('id');
            $file           = $request->file('file');
            $origin_name    = $file->getClientOriginalName();
            $file_extension = $file->getClientOriginalExtension();

            $type       = $request->has('type') ? $request->get('type') : 'images';
            $uploadType = $request->has('uploadType') ? $request->get('uploadType') : 'default';

            if (!is_null($file_extension)) {
                $file_name = snake_case(str_replace('.' . $file_extension, '', $origin_name));
                $path      = $this->upload_directory . "/" . $uploadType . "/" . $id . "/" . $type . "/" . time() . "_{$file_name}.{$file_extension}";
            } else {
                $path = $this->upload_directory . "/" . $uploadType . "/" . $id . "/" . $type . "/" . time() . "_{$file_name}";
            }
            $path = strtolower($path);

            $success = Storage::disk('local')->put($path, File::get($file));

            if ($success) {
                Event::fire(new FileUpload($request, $path));
                return $this->response->array(['success' => $success, 'path' => url($path)]);
            } else {
                return $this->response->error('can\'t upload file', 1009);
            }
        }
    }
}
