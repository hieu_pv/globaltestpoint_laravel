<?php

namespace App\Http\Controllers\API;

use App\Entities\Exam;
use App\Http\Controllers\API\ApiController;
use App\Transformers\ExamTransformer;

class ExamController extends ApiController {

	public function index() {
		$exams = Exam::where('active', 'yes')->orderBy('id', 'desc')->get();
		return $this->response->collection($exams, new ExamTransformer);
	}
	public function show($id) {
		$exam = Exam::with('questions.answers')->findOrFail($id);
		return $this->response->item($exam, new ExamTransformer);
	}
}
