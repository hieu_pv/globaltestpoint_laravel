<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\ApiController;
use App\Repositories\JobTitleRepository;
use App\Transformers\JobTitleTransformer;
use App\Validators\JobTitleValidator;
use Illuminate\Http\Request;

class JobTitleController extends ApiController
{
    const STATUS_ACTIVE  = 1;
    const STATUS_PENDING = 0;

    private $repository;
    private $validator;

    public function __construct(JobTitleRepository $repository, JobTitleValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    public function index(Request $request)
    {
        $job_titles = $this->repository->scopeQuery(function ($query) {
            $query = $query->where('status', self::STATUS_ACTIVE);
            $query = $query->orderBy('order', 'asc');
            $query = $query->orderBy('id', 'asc');
            return $query;
        })->all();
        return $this->response->collection($job_titles, new JobTitleTransformer);
    }
}
