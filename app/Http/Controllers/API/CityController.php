<?php

namespace App\Http\Controllers\API;

use App\Entities\City;
use App\Http\Controllers\API\ApiController;
use App\Repositories\CityRepository;
use App\Transformers\CityTransformer;
use App\Validators\CityValidator;
use Illuminate\Http\Request;

class CityController extends ApiController
{
    const STATUS_ACTIVE  = 1;
    const STATUS_PENDING = 0;

    private $repository;
    private $validator;

    public function __construct(CityRepository $repository, CityValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cities = $this->repository->scopeQuery(function ($query) {
            $query = $query->where('status', self::STATUS_ACTIVE);
            $query = $query->orderBy('order', 'asc');
            $query = $query->orderBy('id', 'asc');
            return $query;
        })->all();
        return $this->response->collection($cities, new CityTransformer);
    }
}
