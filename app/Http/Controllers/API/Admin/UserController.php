<?php

namespace App\Http\Controllers\API\Admin;

use App\Entities\User;
use App\Entities\UserStatus;
use App\Events\Admin\AdminChangeStatusEmployer;
use App\Events\Admin\AdminCreateUser;
use App\Events\Admin\AdminDeleteUser;
use App\Events\Admin\AdminUpdateUser;
use App\Exceptions\PermissionDeniedException;
use App\Http\Controllers\API\ApiController;
use App\Repositories\UserRepository;
use App\Transformers\UserTransformer;
use App\Validators\UserValidator;
use App\Validators\ValidatorInterface;
use Carbon\Carbon;
use Event;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends ApiController
{
    private $repository;
    private $validator;

    public function __construct(UserRepository $repository, UserValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        // $this->middleware('jwt.auth', ['except' => []]);
    }

    public function index(Request $request)
    {
        $constraints = (array) json_decode($request->get('constraints'));

        $query = new User;

        if ($request->has('search')) {
            $query = $query->where(function ($q) use ($request) {
                $q->where('email', 'like', '%' . $request->get('search') . '%')
                    ->orWhere(DB::raw('CONCAT_WS(" ", phone_area_code, mobile)'), 'like', '%' . $request->get('search') . '%')->orWhere('city', 'like', '%' . $request->get('search') . '%')
                    ->orWhere(DB::raw('CONCAT_WS(" ", first_name, last_name)'), 'like', '%' . $request->get('search') . '%')
                    ->orWhereHas('employer', function ($sub_query) use ($request) {
                        $sub_query->where('company_name', 'like', '%' . $request->get('search') . '%')
                            ->orWhere('company_about', 'like', '%' . $request->get('search') . '%')
                            ->orWhere('registration_number', 'like', '%' . $request->get('search') . '%')
                            ->orWhere('company_address', 'like', '%' . $request->get('search') . '%')
                            ->orWhere('contact_name', 'like', '%' . $request->get('search') . '%');
                    });
            });
        }

        if ($request->has('ids')) {
            $query = $query->whereIn('id', explode(',', $request->get('ids')));
        }
        if ($request->has('role')) {
            $query = $query->whereHas('roles', function ($q) use ($request) {
                $q->where('slug', $request->get('role'));
            });
        }
        if ($request->has('roles')) {
            $query = $query->whereHas('roles', function ($q) use ($request) {
                $q->whereIn('slug', explode(',', $request->get('roles')));
            });
        }
        if ($request->has('roles_excerpt')) {
            $query = $query->where('id', '<>', Auth::id())->whereHas('roles', function ($q) use ($request) {
                $q->whereNotIn('slug', explode(',', $request->get('roles_excerpt')));
            });
        }

        if ($request->has('list') && $request->get('list') == 'today') {
            $query = $query->whereDate('created_at', '>=', Carbon::today()->format('Y-m-d'));
        }

        if ($request->has('status')) {
            if ($request->has('status')) {
                $query = $query->where('status', $request->get('status'));
            }
        }

        if (count($constraints)) {
            $query = $query->where($constraints);
        }

        $total_users = $query->count();

        if ($request->has('order_by')) {
            $orderBy = (array) json_decode($request->get('order_by'));

            if (isset($orderBy['phone'])) {
                $query = $query->orderBy('phone_area_code', $orderBy['phone'])->orderBy('mobile', $orderBy['phone']);
                unset($orderBy['phone']);
            }
            if (count($orderBy)) {
                foreach ($orderBy as $key => $value) {
                    $query = $query->orderBy($key, $value);
                }
            }
        }

        $page     = $request->has('page') ? (int) $request->get('page') : 1;
        $per_page = $request->has('per_page') ? (int) $request->get('per_page') : 15;
        $users    = $query->skip(($page - 1) * $per_page)->take($per_page)->with('employer')->get();

        $transformer = new UserTransformer(['employer']);

        if ($request->has('includes')) {
            $transformer = new UserTransformer(explode(',', $request->get('includes')));
        }

        $transformer->setRoleTransformer([]);

        $result = new LengthAwarePaginator($users, $total_users, $per_page, $page);
        return $this->paginator($result, $transformer);
    }

    /**
     * Get user profile
     *
     * @param int
     * @return \Entities\User
     */
    public function show($id)
    {
        $user = $this->repository->find($id);
        if ($user->isRole('employer')) {
            $transformer = new UserTransformer(['roles', 'images', 'employer']);
        } else if ($user->isRole('employee')) {
            $transformer = new UserTransformer(['roles', 'images']);
        } else {
            $transformer = new UserTransformer(['roles', 'images']);
        }
        return $this->response->item($user, $transformer);
    }

    /**
     * update employer profile
     *
     * @param int $id
     * @return \Entities\User
     */
    public function updateEmployerProfile(Request $request, $id)
    {
        if (!Auth::user()->can('update.companies')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, ValidatorInterface::RULE_UPDATE);
        $user        = $this->repository->find($id);
        $existsEmail = $this->repository->existsEmail($id, $request->get('email'));
        if ($existsEmail) {
            throw new ConflictHttpException("Email has exists", null, 1001);
        }
        $attributes = $request->all();

        $user = $this->repository->update($attributes, $id);
        if ($user->employer->isEmpty()) {
            $this->repository->createEmployerProfile($request->all(), $id);
        } else {
            $this->repository->updateEmployerProfile($request->all(), $id);
        }

        $user = $this->repository->find($id);

        Event::fire(new AdminUpdateUser(Auth::user(), $user));
        $transformer = new UserTransformer;
        return $this->response->item($user, $transformer);
    }

    public function employerChangeStatus(Request $request, $id)
    {
        if (!Auth::user()->can('update.users')) {
            throw new PermissionDeniedException;
        }
        $attributes = $request->all();
        $user       = $this->repository->update($attributes, $id);
        if ($user->activated == '0000-00-00 00:00:00') {
            $active_status = UserStatus::where('status', 'Active')->first();
            if ($request->get('status') == $active_status->id) {
                $user->activated = new Carbon();
                $user->save();
            }
        }
        Event::fire(new AdminChangeStatusEmployer(Auth::user(), $user, $attributes['status']));
        $transformer = new UserTransformer;
        return $this->response->item($user, $transformer);
    }

    /**
     * Update user image
     */
    public function images(Request $request, $id)
    {
        if (!Auth::user()->can('update.users')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'images');

        $attributes         = $request->all();
        $attributes['type'] = $attributes == 'license' ? 'driving_license' : $attributes['type'];

        $user  = $this->repository->find($id);
        $image = $user->images()->updateOrCreate(['type' => $attributes['type']], $attributes);
        if ($request->get('type') == 'id') {
            $user->identity_check_status = 'pending';
            $user->save();
        }
        return $this->success();
    }

    /**
     * Create new Employer Profile
     */
    public function employer(Request $request)
    {
        if (!Auth::user()->can('create.companies')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'employer');
        if (!$this->repository->skipCache()->findByField('email', $request->get('email'))->isEmpty()) {
            throw new ConflictHttpException('Email already exist', null, 1001);
        }
        $data = [
            'email'           => $request->get('email'),
            'mobile'          => $request->get('mobile'),
            'phone_area_code' => $request->has('phone_area_code') ? $request->get('phone_area_code') : '',
        ];
        $user = $this->repository->create($data);

        $user_status_active = UserStatus::where('status', 'active')->first();
        if ($user_status_active) {
            $user->status = $user_status_active->id;
        }
        $user->password              = Hash::make($request->get('password'));
        $user->phone_number_verified = 1;
        $user->email_verified        = 1;
        $user->save();

        if ($request->has('tag_ids')) {
            $user->tags()->sync($request->get('tag_ids'));
        }
        $user = $this->repository->attachRole('employer', $user->id);

        $this->repository->createEmployerProfile($request->all(), $user->id);

        if ($request->has('industries')) {
            $user->industries()->sync($request->get('industries'));
        }
        $user->images()->create(['url' => '/uploads/default_avatar.png', 'type' => 'avatar']);
        Event::fire(new AdminCreateUser(Auth::user(), $user));
        $token = JWTAuth::fromUser($user);
        return $this->response->array(compact('token'));
    }

    public function destroyCompany($id)
    {
        if (!Auth::user()->can('delete.companies')) {
            throw new PermissionDeniedException;
        }
        $user = $this->repository->find($id);
        if (!$user->isRole('employer')) {
            throw new AccessDeniedHttpException("This user is not Employer", null, 1);
        }
        Event::fire(new AdminDeleteUser(Auth::user(), $user));
        $this->repository->delete($id);
        return $this->success();
    }

    public function employers(Request $request)
    {
        $constraints = (array) json_decode($request->get('constraints'));
        $users       = $this->repository->scopeQuery(function ($query) use ($constraints, $request) {
            $query = $query->whereHas('roles', function ($q) {
                $q->where('slug', 'employer');
            });

            if ($request->has('status')) {
                switch ($request->get('status')) {
                    case 'banned':
                        $query = $query->where('status', 6);
                        break;
                    case 'active':
                        $query = $query->where('status', 5);
                        break;
                    case 'pending':
                        $query = $query->where('status', 1);
                        break;
                }
            }

            // filter by constraints
            if (count($constraints)) {
                $query = $query->where($constraints);
            }
            return $query;
        })->orderBy('id', 'desc')->all();
        $this->repository->flushCache();
        $transformer = new UserTransformer(['employer']);
        return $this->response->collection($users, $transformer);
    }
}
