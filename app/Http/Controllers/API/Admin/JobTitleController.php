<?php

namespace App\Http\Controllers\API\Admin;

use App\Entities\JobTitle;
use App\Exceptions\PermissionDeniedException;
use App\Http\Controllers\API\ApiController;
use App\Repositories\JobTitleRepository;
use App\Transformers\JobTitleTransformer;
use App\Validators\JobTitleValidator;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;

class JobTitleController extends ApiController
{
    const STATUS_ACTIVE  = 1;
    const STATUS_PENDING = 0;

    private $repository;
    private $validator;

    public function __construct(JobTitleRepository $repository, JobTitleValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new JobTitle;

        $constraints = (array) json_decode($request->get('constraints'));
        if (count($constraints)) {
            $query = $query->where($constraints);
        }

        if ($request->has('search')) {
            $search = $request->get('search');
            $query  = $query->where(function ($q) use ($request, $search) {
                $q->where('name', 'like', "%{$search}%");
            });
        }

        if ($request->has('order_by')) {
            $orderBy = (array) json_decode($request->get('order_by'));
            if (count($orderBy) > 0) {
                foreach ($orderBy as $key => $value) {
                    $query = $query->orderBy($key, $value);
                }
            }
        }

        $total_records = $query->count();
        $page          = $request->has('page') ? (int) $request->get('page') : 1;
        $per_page      = $request->has('per_page') ? (int) $request->get('per_page') : 15;
        $job_titles    = $query->skip(($page - 1) * $per_page)->take($per_page)->get();

        $transformer = new JobTitleTransformer;

        $result = new LengthAwarePaginator($job_titles, $total_records, $per_page, $page);
        return $this->paginator($result, $transformer);
    }

    public function getAll(Request $request)
    {
        $job_titles = $this->repository->all();

        return $this->response->collection($job_titles, new JobTitleTransformer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validator->isValid($request, 'RULE_CREATE');

        $data               = $request->all();
        $data['attributes'] = json_encode($data['attributes']);

        $job_title = $this->repository->create($data);

        if ($request->has('status')) {
            $job_title->status = $request->get('status');
            $job_title->save();
        }

        return $this->response->item($job_title, new JobTitleTransformer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $job_title = $this->repository->find($id);
        return $this->response->item($job_title, new JobTitleTransformer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validator->isValid($request, 'RULE_UPDATE');
        $job_title = $this->repository->find($id);

        $data               = $request->all();
        $data['attributes'] = json_encode($data['attributes']);
        $job_title          = $this->repository->update($data, $id);

        if ($request->has('status')) {
            $job_title->status = $request->get('status');
            $job_title->save();
        }

        return $this->response->item($job_title, new JobTitleTransformer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Auth::user()->can('delete.job.titles')) {
            throw new PermissionDeniedException;
        }
        $this->repository->delete($id);
        return $this->success();
    }

    public function changeStatusAllItems(Request $request)
    {
        if (!Auth::user()->can('update.job.titles')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'CHANGE_STATUS_ALL_ITEMS');

        $data = $request->all();

        switch ($data['status']) {
            case 'active':
                $job_titles = JobTitle::whereIn('id', $data['item_ids'])->update(['status' => self::STATUS_ACTIVE]);
                break;
            case 'pending':
                $job_titles = JobTitle::whereIn('id', $data['item_ids'])->update(['status' => self::STATUS_PENDING]);
                break;
        }
        return $this->success();
    }
}
