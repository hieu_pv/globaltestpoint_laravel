<?php

namespace App\Http\Controllers\API\Admin;

use App\Entities\Job;
use App\Entities\User;
use App\Events\JobCreated;
use App\Events\JobDeleted;
use App\Events\JobUpdated;
use App\Events\UpdateTotalJobsFieldEvent;
use App\Exceptions\PermissionDeniedException;
use App\Http\Controllers\API\ApiController;
use App\Repositories\JobRepository;
use App\Repositories\UserRepository;
use App\Transformers\JobTransformer;
use App\Validators\Admin\AdminJobValidator;
use App\Validators\ValidatorInterface;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;

class JobController extends ApiController
{
    use DispatchesJobs;

    const STATUS_ACTIVE  = 1;
    const STATUS_PENDING = 0;

    private $repository;
    private $validator;
    public function __construct(JobRepository $repository, AdminJobValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        // $this->middleware('jwt.auth', ['except' => []]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $constraints = (array) json_decode($request->get('constraints'));

        $query = new Job();

        if ($request->has('search')) {
            $query = $query->where(function ($q) use ($request) {
                $q->where('title', 'like', '%' . $request->get('search') . '%')
                    ->orWhere('id', $request->get('search'))
                    ->orWhere('address', 'like', '%' . $request->get('search') . '%')
                    ->orWhere('zipcode', 'like', '%' . $request->get('search') . '%')
                    ->orWhereHas('employer', function ($sub_query) use ($request) {
                        $sub_query->where('first_name', 'like', '%' . $request->get('search') . '%')
                            ->orWhere('email', 'like', '%' . $request->get('search') . '%')
                            ->orWhere(DB::raw('CONCAT_WS(" ", phone_area_code, mobile)'), 'like', '%' . $request->get('search') . '%');
                    });
            });
        }
        if ($request->has('employer')) {
            $employer = explode(',', $request->get('employer'));
            $query    = $query->whereIn('employer_id', $employer);
        }

        if ($request->has('status')) {
            switch ($request->get('status')) {
                case 'archive':
                    $query = $query->where('archive', 1);
                    break;
                case 'active':
                    $query = $query->where('archive', 0)->where('active', 1);
                    break;
                case 'pending':
                    $query = $query->where('archive', 0)->where('active', 0);
                    break;
                default:
                    $query = $query->where('archive', 0);
                    break;
            }
        }
        if ($request->has('tag')) {
            $tags = explode(',', $request->get('tag'));
            foreach ($tags as $tag) {
                $query = $query->whereHas('tags', function ($q) use ($tag) {
                    $q->where('id', $tag);
                });
            }
        }
        // filter by time
        if ($request->has('from')) {
            $query = $query->whereDate('created_at', '>=', $request->get('from'));
        }
        if ($request->has('to')) {
            $query = $query->whereDate('created_at', '<=', $request->get('to'));
        }
        if ($request->has('order_by')) {
            $orderBy = (array) json_decode($request->get('order_by'));
            if (count($orderBy)) {
                foreach ($orderBy as $key => $value) {
                    $query = $query->orderBy($key, $value);
                }
            }
        } else {
            $query = $query->orderBy('id', 'asc');
        }

        $page     = $request->has('page') ? (int) $request->get('page') : 1;
        $per_page = $request->has('per_page') ? (int) $request->get('per_page') : 15;

        $total_jobs = $query->count();

        $jobs = $query->skip(($page - 1) * $per_page)->take($per_page)->get();

        $result = new LengthAwarePaginator($jobs, $total_jobs, $per_page, $page);

        if ($request->has('includes')) {
            $transformer = new JobTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new JobTransformer;
        }

        return $this->response->paginator($result, $transformer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validator->isValid($request, ValidatorInterface::RULE_CREATE);
        $user = App::make(UserRepository::class)->find($request->get('employer_id'));
        $data = $request->all();
        $slug = str_slug($data['title']);
        $i    = 1;
        while (!$this->repository->findByField('slug', $slug)->isEmpty()) {
            $slug = str_slug($request->get('title')) . '-' . $i;
            $i++;
        }
        $data['slug'] = $slug;

        $job = $this->repository->create($data);

        if ($request->has('draft') && $request->get('draft')) {
            $job->update(Job::STATE['DRAFT']);
        }

        if (!empty($data['industry_ids'])) {
            $job->industries()->sync($data['industry_ids']);
        }

        if (!empty($data['tag_ids'])) {
            $job->tags()->sync($data['tag_ids']);
        }

        Event::fire(new JobCreated(Auth::user(), $job));
        Event::fire(new UpdateTotalJobsFieldEvent);
        $transformer = new JobTransformer();
        return $this->response->item($job, $transformer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $job         = $this->repository->find($id);
        $transformer = new JobTransformer(['shifts', 'employer', 'industries']);
        $transformer->setShiftTransfomerDefaultIncludes(['users']);
        return $this->response->item($job, $transformer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Auth::user()->can('update.jobs')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, ValidatorInterface::RULE_UPDATE);
        $job = $this->repository->find($id);
        if ($job->deleted == 1) {
            throw new BadRequestHttpException("This job has been canceled", null, 1047);
        }
        $user = App::make(UserRepository::class)->find($job->employer_id);

        $data = $request->all();

        $job = $this->repository->update($data, $id);

        if (!empty($data['industry_ids'])) {
            $job->industries()->sync($data['industry_ids']);
        }

        if (!empty($data['tag_ids'])) {
            $job->tags()->sync($data['tag_ids']);
        }

        Event::fire(new JobUpdated(Auth::user(), $job));
        Event::fire(new UpdateTotalJobsFieldEvent);
        return $this->response->item($job, new JobTransformer);
    }

    private function _validatePaymentMethod($payment_method)
    {
        $methods = [Job::PAYMENT_METHOD_CASH, Job::PAYMENT_METHOD_ONLINE, Job::PAYMENT_METHOD_BANK_TRANSFER];
        switch ($payment_method) {
            case Job::PAY_CASH:
                $method = Job::PAYMENT_METHOD_CASH;
                break;
            case Job::PAY_ONLINE:
                $method = Job::PAYMENT_METHOD_ONLINE;
                break;
            case Job::PAY_BANK_TRANSFER:
                $method = Job::PAYMENT_METHOD_BANK_TRANSFER;
                break;
            default:
                throw new NotAcceptableHttpException("Method is not supported", null, 1028);

        }
        if (!in_array($method, $methods)) {
            throw new NotAcceptableHttpException("Method is not supported", null, 1028);
        }
        return $method;
    }

    private function _validateJobType($type)
    {
        $types = ['normal job', 'nationality job'];
        switch ($type) {
            case '1':
                $job_type = 'normal job';
                break;

            case '2':
                $job_type = 'nationality job';
                break;
        }
        if (!in_array($job_type, $types)) {
            throw new BadRequestHttpException("Job type is not supported", null, 1029);
        }
        return $job_type;
    }

    private function _validateCurrency($currency)
    {
        if (isset($currency) && $currency != '') {
            $currencies = ['usd', 'sgd'];
            $currency   = strtolower($currency);
            if (!in_array($currency, $currencies)) {
                throw new BadRequestHttpException("currency is not supported", null, 1030);
            }
            return $currency;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Auth::user()->can('delete.jobs')) {
            throw new PermissionDeniedException;
        }
        $job = $this->repository->find($id);
        $this->repository->delete($id);
        Event::fire(new JobDeleted($job, Auth::user()));
        return $this->success();
    }

    public function changeStatusAllItems(Request $request)
    {
        if (!Auth::user()->can('update.jobs')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'CHANGE_STATUS_ALL_ITEMS');

        $data = $request->all();
        switch ($data['status']) {
            case 'active':
                $jobs = Job::whereIn('id', $data['item_ids'])->update(['active' => self::STATUS_ACTIVE]);
                break;
            case 'pending':
                $jobs = Job::whereIn('id', $data['item_ids'])->update(['active' => self::STATUS_PENDING]);
                break;
        }
        Event::fire(new UpdateTotalJobsFieldEvent);
        return $this->success();
    }

    public function getJobBySlug($slug)
    {
        $job         = $this->repository->skipCache()->findByField('slug', $slug)->first();
        $transformer = new JobTransformer(['employer', 'industries', 'tags']);
        return $this->response->item($job, $transformer);
    }
}
