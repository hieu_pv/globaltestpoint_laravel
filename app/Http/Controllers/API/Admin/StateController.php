<?php

namespace App\Http\Controllers\API\Admin;

use App\Entities\State;
use App\Exceptions\PermissionDeniedException;
use App\Http\Controllers\API\ApiController;
use App\Repositories\StateRepository;
use App\Transformers\CityTransformer;
use App\Transformers\StateTransformer;
use App\Validators\StateValidator;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;

class StateController extends ApiController
{
    const STATUS_ACTIVE  = 1;
    const STATUS_PENDING = 0;

    private $repository;
    private $validator;

    public function __construct(StateRepository $repository, StateValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new State;

        $constraints = (array) json_decode($request->get('constraints'));
        if (count($constraints)) {
            $query = $query->where($constraints);
        }

        if ($request->has('search')) {
            $search = $request->get('search');
            $query  = $query->where(function ($q) use ($request, $search) {
                $q->where('name', 'like', "%{$search}%")
                    ->orWhereHas('country', function ($sub_query) use ($request, $search) {
                        $sub_query->where('name', 'like', "%{$search}%");
                    });
            });
        }

        if ($request->has('order_by')) {
            $orderBy = (array) json_decode($request->get('order_by'));
            if (count($orderBy) > 0) {
                foreach ($orderBy as $key => $value) {
                    $query = $query->orderBy($key, $value);
                }
            }
        }

        $total_records = $query->count();
        $page          = $request->has('page') ? (int) $request->get('page') : 1;
        $per_page      = $request->has('per_page') ? (int) $request->get('per_page') : 15;
        $states        = $query->skip(($page - 1) * $per_page)->take($per_page)->get();

        if ($request->has('includes')) {
            $transformer = new StateTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new StateTransformer;
        }

        $result = new LengthAwarePaginator($states, $total_records, $per_page, $page);
        return $this->paginator($result, $transformer);
    }

    public function getAll(Request $request)
    {
        $states = State::all();

        return $this->response->collection($states, new StateTransformer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::user()->can('create.states')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'RULE_CREATE');

        $data         = $request->all();
        $data['slug'] = $this->_getSlug($this->repository, $data['name']);

        $state = $this->repository->create($data);

        if ($request->has('active')) {
            $state->active = $request->get('active');
            $state->save();
        }

        return $this->response->item($state, new StateTransformer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $state = $this->repository->find($id);
        return $this->response->item($state, new StateTransformer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Auth::user()->can('update.states')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'RULE_UPDATE');
        $state = $this->repository->find($id);

        $state = $this->repository->update($request->all(), $id);

        if ($request->has('active')) {
            $state->active = $request->get('active');
            $state->save();
        }

        return $this->response->item($state, new StateTransformer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Auth::user()->can('delete.states')) {
            throw new PermissionDeniedException;
        }
        $this->repository->delete($id);
        return $this->success();
    }

    public function changeStatusAllItems(Request $request)
    {
        if (!Auth::user()->can('update.states')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'CHANGE_STATUS_ALL_ITEMS');

        $data = $request->all();

        foreach ($data['item_ids'] as $id) {
            $item = $this->repository->find($id);
            if ($item) {
                switch ($data['status']) {
                    case 'active':
                        $item->status = self::STATUS_ACTIVE;
                        break;
                    case 'pending':
                        $item->status = self::STATUS_PENDING;
                        break;
                }
                $item->save();
            }
        }
        return $this->success();
    }

    public function getCities($id, Request $request)
    {
        $state  = $this->repository->find($id);
        $cities = $state->cities;

        return $this->response->collection($cities, new CityTransformer);
    }
}
