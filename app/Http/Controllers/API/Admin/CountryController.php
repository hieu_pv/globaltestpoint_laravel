<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\API\ApiController;
use App\Repositories\CountryRepository;
use App\Transformers\CityTransformer;
use App\Transformers\CountryTransformer;
use App\Transformers\StateTransformer;
use App\Validators\CountryValidator;
use Illuminate\Http\Request;

class CountryController extends ApiController
{
    private $repository;
    private $validator;

    public function __construct(CountryRepository $repository, CountryValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    public function getAll(Request $request)
    {
        $countries = $this->repository->all();

        return $this->response->collection($countries, new CountryTransformer);
    }

    public function getStates($id, Request $request) {
        $country = $this->repository->find($id);
        $states = $country->states;

        return $this->response->collection($states, new StateTransformer);
    }
}
