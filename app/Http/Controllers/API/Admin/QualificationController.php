<?php

namespace App\Http\Controllers\API\Admin;

use App\Entities\Qualification;
use App\Exceptions\PermissionDeniedException;
use App\Http\Controllers\API\ApiController;
use App\Repositories\QualificationRepository;
use App\Transformers\QualificationTransformer;
use App\Validators\QualificationValidator;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;

class QualificationController extends ApiController
{
    const STATUS_ACTIVE  = 1;
    const STATUS_PENDING = 0;

    private $repository;
    private $validator;

    public function __construct(QualificationRepository $repository, QualificationValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new Qualification;

        $constraints = (array) json_decode($request->get('constraints'));
        if (count($constraints)) {
            $query = $query->where($constraints);
        }

        if ($request->has('search')) {
            $search = $request->get('search');
            $query  = $query->where(function ($q) use ($request, $search) {
                $q->where('name', 'like', "%{$search}%");
            });
        }

        if ($request->has('order_by')) {
            $orderBy = (array) json_decode($request->get('order_by'));
            if (count($orderBy) > 0) {
                foreach ($orderBy as $key => $value) {
                    $query = $query->orderBy($key, $value);
                }
            }
        }

        $total_records  = $query->count();
        $page           = $request->has('page') ? (int) $request->get('page') : 1;
        $per_page       = $request->has('per_page') ? (int) $request->get('per_page') : 15;
        $qualifications = $query->skip(($page - 1) * $per_page)->take($per_page)->get();

        $transformer = new QualificationTransformer;

        $result = new LengthAwarePaginator($qualifications, $total_records, $per_page, $page);
        return $this->paginator($result, $transformer);
    }

    public function getAll(Request $request)
    {
        $qualifications = $this->repository->all();

        return $this->response->collection($qualifications, new QualificationTransformer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::user()->can('create.qualifications')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'RULE_CREATE');

        $data = $request->all();

        $qualification = $this->repository->create($data);

        if ($request->has('status')) {
            $qualification->status = $request->get('status');
            $qualification->save();
        }

        return $this->response->item($qualification, new QualificationTransformer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $qualification = $this->repository->find($id);
        return $this->response->item($qualification, new QualificationTransformer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Auth::user()->can('update.qualifications')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'RULE_UPDATE');
        $qualification = $this->repository->find($id);

        $qualification = $this->repository->update($request->all(), $id);

        if ($request->has('status')) {
            $qualification->status = $request->get('status');
            $qualification->save();
        }

        return $this->response->item($qualification, new QualificationTransformer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Auth::user()->can('delete.qualifications')) {
            throw new PermissionDeniedException;
        }
        $this->repository->delete($id);
        return $this->success();
    }

    public function changeStatusAllItems(Request $request)
    {
        if (!Auth::user()->can('update.qualifications')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'CHANGE_STATUS_ALL_ITEMS');

        $data = $request->all();

        switch ($data['status']) {
            case 'active':
                $qualifications = Qualification::whereIn('id', $data['item_ids'])->update(['status' => self::STATUS_ACTIVE]);
                break;
            case 'pending':
                $qualifications = Qualification::whereIn('id', $data['item_ids'])->update(['status' => self::STATUS_PENDING]);
                break;
        }
        return $this->success();
    }
}
