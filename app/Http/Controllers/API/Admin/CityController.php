<?php

namespace App\Http\Controllers\API\Admin;

use App\Entities\City;
use App\Exceptions\PermissionDeniedException;
use App\Http\Controllers\API\ApiController;
use App\Repositories\CityRepository;
use App\Transformers\CityTransformer;
use App\Validators\CityValidator;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;

class CityController extends ApiController
{
    const STATUS_ACTIVE  = 1;
    const STATUS_PENDING = 0;

    private $repository;
    private $validator;

    public function __construct(CityRepository $repository, CityValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new City;

        $constraints = (array) json_decode($request->get('constraints'));
        if (count($constraints)) {
            $query = $query->where($constraints);
        }

        if ($request->has('search')) {
            $search = $request->get('search');
            $query  = $query->where(function ($q) use ($request, $search) {
                $q->where('name', 'like', "%{$search}%")
                    ->orWhereHas('state', function ($sub_query) use ($request, $search) {
                        $sub_query->where('name', 'like', "%{$search}%");
                    });
            });
        }

        if ($request->has('order_by')) {
            $orderBy = (array) json_decode($request->get('order_by'));
            if (count($orderBy) > 0) {
                foreach ($orderBy as $key => $value) {
                    $query = $query->orderBy($key, $value);
                }
            }
        }

        $total_records = $query->count();
        $page          = $request->has('page') ? (int) $request->get('page') : 1;
        $per_page      = $request->has('per_page') ? (int) $request->get('per_page') : 15;
        $cities        = $query->skip(($page - 1) * $per_page)->take($per_page)->get();

        if ($request->has('includes')) {
            $transformer = new CityTransformer(explode(',', $request->get('includes')));
        } else {
            $transformer = new CityTransformer;
        }

        $result = new LengthAwarePaginator($cities, $total_records, $per_page, $page);
        return $this->paginator($result, $transformer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::user()->can('create.cities')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'RULE_CREATE');

        $data = $request->all();

        $city = $this->repository->create($data);

        if ($request->has('status')) {
            $city->status = $request->get('status');
            $city->save();
        }

        return $this->response->item($city, new CityTransformer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $city = $this->repository->find($id);
        return $this->response->item($city, new CityTransformer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Auth::user()->can('update.cities')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'RULE_UPDATE');
        $city = $this->repository->find($id);

        $city = $this->repository->update($request->all(), $id);

        if ($request->has('status')) {
            $city->status = $request->get('status');
            $city->save();
        }

        return $this->response->item($city, new CityTransformer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Auth::user()->can('delete.cities')) {
            throw new PermissionDeniedException;
        }
        $this->repository->delete($id);
        return $this->success();
    }

    public function changeStatusAllItems(Request $request)
    {
        if (!Auth::user()->can('update.cities')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'CHANGE_STATUS_ALL_ITEMS');

        $data = $request->all();

        switch ($data['status']) {
            case 'active':
                $cities = City::whereIn('id', $data['item_ids'])->update(['status' => self::STATUS_ACTIVE]);
                break;
            case 'pending':
                $cities = City::whereIn('id', $data['item_ids'])->update(['status' => self::STATUS_PENDING]);
                break;
        }
        return $this->success();
    }
}
