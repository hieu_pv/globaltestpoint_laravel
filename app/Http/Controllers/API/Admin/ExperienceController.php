<?php

namespace App\Http\Controllers\API\Admin;

use App\Entities\Experience;
use App\Exceptions\PermissionDeniedException;
use App\Http\Controllers\API\ApiController;
use App\Repositories\ExperienceRepository;
use App\Transformers\ExperienceTransformer;
use App\Validators\ExperienceValidator;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;

class ExperienceController extends ApiController
{
    const STATUS_ACTIVE  = 1;
    const STATUS_PENDING = 0;

    private $repository;
    private $validator;

    public function __construct(ExperienceRepository $repository, ExperienceValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new Experience;

        $constraints = (array) json_decode($request->get('constraints'));
        if (count($constraints)) {
            $query = $query->where($constraints);
        }

        if ($request->has('search')) {
            $search = $request->get('search');
            $query  = $query->where(function ($q) use ($request, $search) {
                $q->where('label', 'like', "%{$search}%");
            });
        }

        if ($request->has('order_by')) {
            $orderBy = (array) json_decode($request->get('order_by'));
            if (count($orderBy) > 0) {
                foreach ($orderBy as $key => $value) {
                    $query = $query->orderBy($key, $value);
                }
            }
        }

        $total_records = $query->count();
        $page          = $request->has('page') ? (int) $request->get('page') : 1;
        $per_page      = $request->has('per_page') ? (int) $request->get('per_page') : 15;
        $experiences   = $query->skip(($page - 1) * $per_page)->take($per_page)->get();

        $transformer = new ExperienceTransformer;

        $result = new LengthAwarePaginator($experiences, $total_records, $per_page, $page);
        return $this->paginator($result, $transformer);
    }

    public function getAll(Request $request)
    {
        $experiences = $this->repository->all();

        return $this->response->collection($experiences, new ExperienceTransformer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::user()->can('create.experiences')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'RULE_CREATE');

        $data       = $request->all();
        $experience = $this->repository->create($data);

        if ($request->has('status')) {
            $experience->status = $request->get('status');
            $experience->save();
        }

        return $this->response->item($experience, new ExperienceTransformer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $experience = $this->repository->find($id);
        return $this->response->item($experience, new ExperienceTransformer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Auth::user()->can('update.experiences')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'RULE_UPDATE');
        $experience = $this->repository->find($id);

        $experience = $this->repository->update($request->all(), $id);

        if ($request->has('status')) {
            $experience->status = $request->get('status');
            $experience->save();
        }

        return $this->response->item($experience, new ExperienceTransformer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Auth::user()->can('update.industries')) {
            throw new PermissionDeniedException;
        }
        $this->repository->delete($id);
        return $this->success();
    }

    public function changeStatusAllItems(Request $request)
    {
        if (!Auth::user()->can('update.experiences')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'CHANGE_STATUS_ALL_ITEMS');

        $data = $request->all();

        switch ($data['status']) {
            case 'active':
                $experiences = Experience::whereIn('id', $data['item_ids'])->update(['status' => self::STATUS_ACTIVE]);
                break;
            case 'pending':
                $experiences = Experience::whereIn('id', $data['item_ids'])->update(['status' => self::STATUS_PENDING]);
                break;
        }
        return $this->success();
    }
}
