<?php

namespace App\Http\Controllers\API\Admin;

use App\Entities\Industry;
use App\Exceptions\PermissionDeniedException;
use App\Http\Controllers\API\ApiController;
use App\Repositories\IndustryRepository;
use App\Transformers\IndustryTransformer;
use App\Validators\IndustryValidator;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;

class IndustryController extends ApiController
{
    const STATUS_ACTIVE  = 1;
    const STATUS_PENDING = 0;

    private $repository;
    private $validator;

    public function __construct(IndustryRepository $repository, IndustryValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new Industry;

        $constraints = (array) json_decode($request->get('constraints'));
        if (count($constraints)) {
            $query = $query->where($constraints);
        }

        if ($request->has('search')) {
            $search = $request->get('search');
            $query  = $query->where(function ($q) use ($request, $search) {
                $q->where('name', 'like', "%{$search}%");
            });
        }

        if ($request->has('order_by')) {
            $orderBy = (array) json_decode($request->get('order_by'));
            if (count($orderBy) > 0) {
                foreach ($orderBy as $key => $value) {
                    $query = $query->orderBy($key, $value);
                }
            }
        }

        $total_records = $query->count();
        $page          = $request->has('page') ? (int) $request->get('page') : 1;
        $per_page      = $request->has('per_page') ? (int) $request->get('per_page') : 15;
        $industries    = $query->skip(($page - 1) * $per_page)->take($per_page)->get();

        $transformer = new IndustryTransformer;

        $result = new LengthAwarePaginator($industries, $total_records, $per_page, $page);
        return $this->paginator($result, $transformer);
    }

    public function getAll(Request $request)
    {
        $industries = $this->repository->all();

        return $this->response->collection($industries, new IndustryTransformer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::user()->can('create.industries')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'RULE_CREATE');

        $data         = $request->all();
        $data['slug'] = $this->_getSlug($this->repository, $data['name']);

        $industry = $this->repository->create($data);

        if ($request->has('status')) {
            $industry->status = $request->get('status');
            $industry->save();
        }

        return $this->response->item($industry, new IndustryTransformer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $industry = $this->repository->find($id);
        return $this->response->item($industry, new IndustryTransformer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Auth::user()->can('update.industries')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'RULE_UPDATE');
        $industry = $this->repository->find($id);

        $industry = $this->repository->update($request->all(), $id);

        if ($request->has('status')) {
            $industry->status = $request->get('status');
            $industry->save();
        }

        return $this->response->item($industry, new IndustryTransformer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Auth::user()->can('delete.industries')) {
            throw new PermissionDeniedException;
        }
        $this->repository->delete($id);
        return $this->success();
    }

    public function changeStatusAllItems(Request $request)
    {
        if (!Auth::user()->can('update.industries')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'CHANGE_STATUS_ALL_ITEMS');

        $data = $request->all();

        switch ($data['status']) {
            case 'active':
                $industries = Industry::whereIn('id', $data['item_ids'])->update(['status' => self::STATUS_ACTIVE]);
                break;
            case 'pending':
                $industries = Industry::whereIn('id', $data['item_ids'])->update(['status' => self::STATUS_PENDING]);
                break;
        }
        return $this->success();
    }
}
