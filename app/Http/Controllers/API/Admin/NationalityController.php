<?php

namespace App\Http\Controllers\API\Admin;

use App\Entities\Nationality;
use App\Exceptions\PermissionDeniedException;
use App\Http\Controllers\API\ApiController;
use App\Repositories\NationalityRepository;
use App\Transformers\NationalityTransformer;
use App\Validators\NationalityValidator;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;

class NationalityController extends ApiController
{
    const STATUS_ACTIVE  = 1;
    const STATUS_PENDING = 0;

    private $repository;
    private $validator;

    public function __construct(NationalityRepository $repository, NationalityValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new Nationality;

        $constraints = (array) json_decode($request->get('constraints'));
        if (count($constraints)) {
            $query = $query->where($constraints);
        }

        if ($request->has('search')) {
            $search = $request->get('search');
            $query  = $query->where(function ($q) use ($request, $search) {
                $q->where('name', 'like', "%{$search}%");
            });
        }

        if ($request->has('order_by')) {
            $orderBy = (array) json_decode($request->get('order_by'));
            if (count($orderBy) > 0) {
                foreach ($orderBy as $key => $value) {
                    $query = $query->orderBy($key, $value);
                }
            }
        }

        $total_records = $query->count();
        $page          = $request->has('page') ? (int) $request->get('page') : 1;
        $per_page      = $request->has('per_page') ? (int) $request->get('per_page') : 15;
        $nationalities = $query->skip(($page - 1) * $per_page)->take($per_page)->get();

        $transformer = new NationalityTransformer;

        $result = new LengthAwarePaginator($nationalities, $total_records, $per_page, $page);
        return $this->paginator($result, $transformer);
    }

    public function getAll(Request $request)
    {
        $nationalities = $this->repository->all();

        return $this->response->collection($nationalities, new NationalityTransformer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::user()->can('create.nationalities')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'RULE_CREATE');

        $data        = $request->all();
        $nationality = $this->repository->create($data);

        if ($request->has('status')) {
            $nationality->status = $request->get('status');
            $nationality->save();
        }

        return $this->response->item($nationality, new NationalityTransformer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $nationality = $this->repository->find($id);
        return $this->response->item($nationality, new NationalityTransformer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Auth::user()->can('update.nationalities')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'RULE_UPDATE');
        $nationality = $this->repository->find($id);

        $nationality = $this->repository->update($request->all(), $id);

        if ($request->has('status')) {
            $nationality->status = $request->get('status');
            $nationality->save();
        }

        return $this->response->item($nationality, new NationalityTransformer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Auth::user()->can('delete.nationalities')) {
            throw new PermissionDeniedException;
        }
        $this->repository->delete($id);
        return $this->success();
    }

    public function changeStatusAllItems(Request $request)
    {
        if (!Auth::user()->can('update.nationalities')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'CHANGE_STATUS_ALL_ITEMS');

        $data = $request->all();

        switch ($data['status']) {
            case 'active':
                $nationalities = Nationality::whereIn('id', $data['item_ids'])->update(['status' => self::STATUS_ACTIVE]);
                break;
            case 'pending':
                $nationalities = Nationality::whereIn('id', $data['item_ids'])->update(['status' => self::STATUS_PENDING]);
                break;
        }
        return $this->success();
    }
}
