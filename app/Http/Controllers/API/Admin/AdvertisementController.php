<?php

namespace App\Http\Controllers\API\Admin;

use App\Entities\Advertisement;
use App\Exceptions\PermissionDeniedException;
use App\Http\Controllers\API\ApiController;
use App\Repositories\AdvertisementRepository;
use App\Transformers\AdvertisementTransformer;
use App\Validators\AdvertisementValidator;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;

class AdvertisementController extends ApiController
{
    const STATUS_ACTIVE  = 1;
    const STATUS_PENDING = 0;

    private $repository;
    private $validator;

    public function __construct(AdvertisementRepository $repository, AdvertisementValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = new Advertisement;

        $constraints = (array) json_decode($request->get('constraints'));
        if (count($constraints)) {
            $query = $query->where($constraints);
        }

        if ($request->has('search')) {
            $search = $request->get('search');
            $query  = $query->where(function ($q) use ($request, $search) {
                $q->where('name', 'like', "%{$search}%");
            });
        }

        if ($request->has('order_by')) {
            $orderBy = (array) json_decode($request->get('order_by'));
            if (count($orderBy) > 0) {
                foreach ($orderBy as $key => $value) {
                    $query = $query->orderBy($key, $value);
                }
            }
        }

        $total_records  = $query->count();
        $page           = $request->has('page') ? (int) $request->get('page') : 1;
        $per_page       = $request->has('per_page') ? (int) $request->get('per_page') : 15;
        $advertisements = $query->skip(($page - 1) * $per_page)->take($per_page)->get();

        $transformer = new AdvertisementTransformer;

        $result = new LengthAwarePaginator($advertisements, $total_records, $per_page, $page);
        return $this->paginator($result, $transformer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (!Auth::user()->can('create.advertisements')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'RULE_CREATE');

        $data = $request->all();

        $data['slug'] = $this->_getSlug($this->repository, $data['name']);

        $advertisement = $this->repository->create($data);

        if ($request->has('status')) {
            $advertisement->status = $request->get('status');
            $advertisement->save();
        }

        return $this->response->item($advertisement, new AdvertisementTransformer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $advertisement = $this->repository->find($id);
        return $this->response->item($advertisement, new AdvertisementTransformer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!Auth::user()->can('update.advertisements')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'RULE_UPDATE');
        $advertisement = $this->repository->find($id);

        $advertisement = $this->repository->update($request->all(), $id);

        if ($request->has('status')) {
            $advertisement->status = $request->get('status');
            $advertisement->save();
        }

        return $this->response->item($advertisement, new AdvertisementTransformer);
    }

    public function updateImage(Request $request, $id)
    {
        if (!Auth::user()->can('update.advertisements')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'UPDATE_IMAGE');

        $attributes = $request->all();

        $advertisement        = $this->repository->find($id);
        $advertisement->image = $request->get('url');
        $advertisement->save();

        return $this->response->item($advertisement, new AdvertisementTransformer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Auth::user()->can('delete.advertisements')) {
            throw new PermissionDeniedException;
        }
        $this->repository->delete($id);
        return $this->success();
    }

    public function changeStatusAllItems(Request $request)
    {
        if (!Auth::user()->can('update.advertisements')) {
            throw new PermissionDeniedException;
        }
        $this->validator->isValid($request, 'CHANGE_STATUS_ALL_ITEMS');

        $data = $request->all();

        switch ($data['status']) {
            case 'active':
                $advertisements = Advertisement::whereIn('id', $data['item_ids'])->update(['status' => self::STATUS_ACTIVE]);
                break;
            case 'pending':
                $advertisements = Advertisement::whereIn('id', $data['item_ids'])->update(['status' => self::STATUS_PENDING]);
                break;
        }
        return $this->success();
    }
}
