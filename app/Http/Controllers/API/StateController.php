<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\ApiController;
use App\Repositories\StateRepository;
use App\Transformers\StateTransformer;
use App\Validators\StateValidator;
use Illuminate\Http\Request;

class StateController extends ApiController
{
    const STATUS_ACTIVE  = 1;
    const STATUS_PENDING = 0;

    private $repository;
    private $validator;

    public function __construct(StateRepository $repository, StateValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $states = $this->repository->scopeQuery(function ($query) {
            $query = $query->where('active', self::STATUS_ACTIVE);
            $query = $query->orderBy('order', 'asc');
            $query = $query->orderBy('id', 'asc');
            return $query;
        })->all();
        return $this->response->collection($states, new StateTransformer);
    }
}
