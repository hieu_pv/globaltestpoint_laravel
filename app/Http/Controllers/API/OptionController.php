<?php

namespace App\Http\Controllers\API;

use App\Entities\Option;
use App\Http\Controllers\API\ApiController;
use App\Http\Controllers\Controller;
use App\Transformers\OptionTransformer;
use Illuminate\Http\Request;

class OptionController extends ApiController
{
	public function getLinkFanpages(Request $request) {
		$option  = Option::where('page', 0)->first();

		return $this->response->item($option, new OptionTransformer);
	}
}
