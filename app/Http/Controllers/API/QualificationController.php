<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\ApiController;
use App\Repositories\QualificationRepository;
use App\Transformers\QualificationTransformer;
use App\Validators\QualificationValidator;
use Illuminate\Http\Request;

class QualificationController extends ApiController
{
    const STATUS_ACTIVE  = 1;
    const STATUS_PENDING = 0;

    private $repository;
    private $validator;

    public function __construct(QualificationRepository $repository, QualificationValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $qualifications = $this->repository->scopeQuery(function ($query) {
            $query = $query->where('status', self::STATUS_ACTIVE);
            $query = $query->orderBy('order', 'asc');
            $query = $query->orderBy('id', 'asc');
            return $query;
        })->all();
        return $this->response->collection($qualifications, new QualificationTransformer);
    }
}
