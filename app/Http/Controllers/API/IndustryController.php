<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\ApiController;
use App\Repositories\IndustryRepository;
use App\Transformers\IndustryTransformer;
use App\Validators\IndustryValidator;
use Illuminate\Http\Request;

class IndustryController extends ApiController
{
    const STATUS_ACTIVE  = 1;
    const STATUS_PENDING = 0;

    private $repository;
    private $validator;

    public function __construct(IndustryRepository $repository, IndustryValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $industries = $this->repository->scopeQuery(function ($query) {
            $query = $query->where('status', self::STATUS_ACTIVE);
            $query = $query->orderBy('order', 'asc');
            $query = $query->orderBy('id', 'asc');
            return $query;
        })->all();
        return $this->response->collection($industries, new IndustryTransformer);
    }
}
