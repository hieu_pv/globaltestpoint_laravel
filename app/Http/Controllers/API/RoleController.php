<?php

namespace App\Http\Controllers\API;

use App\Entities\Role;
use App\Http\Controllers\API\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Transformers\RoleTransformer;
use Illuminate\Http\Request;

class RoleController extends ApiController
{
    public function index() {
    	$roles = Role::where('slug', '!=', 'admin')->get();
    	return $this->response->collection($roles, new RoleTransformer);
    }
}
