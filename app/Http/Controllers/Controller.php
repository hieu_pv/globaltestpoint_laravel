<?php

namespace App\Http\Controllers;

use App\Repositories\SchoolRepository;
use DateTime;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class Controller extends BaseController {
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	private $school_repository;

	public function __construct(SchoolRepository $school_repository) {
		$this->school_repository = $school_repository;
	}

	public function responseData($data) {
		$response = new Response(['data' => $data], '200');
		$response->header('Content-Type', 'application/json');
		return $response;
	}

	public function error($error) {
		$response = new Response(['errorMsg' => $error], '500');
		$response->header('Content-Type', 'application/json');
		return $response;
	}

	public function saveFullURL(Request $request) {
		$data = $request->only('fullUrl', 'redirect');
		Session::put('fullUrl', $data['fullUrl']);
		return redirect($data['redirect']);
	}

	public function getStatesByCountry($country_id) {
		$states = $this->school_repository->getStates($country_id);
		if (!$states->isEmpty()) {
			return $this->responseData($states);
		} else {
			return $this->error("Doesn't exist state");
		}
	}

	/**
	 * @param  Collection $categories
	 * @return Array $categoryHasChild
	 */
	public function getChildCategory($categories) {
		$categoryHasChild = array();
		$total            = count($categories);
		if ($total > 0) {
			foreach ($categories as $key => $category) {
				if ($category['parent_id'] != 0) {
					if (!in_array($category['parent_id'], $categoryHasChild)) {
						$categoryHasChild[] = $category['parent_id'];
					}
				}
			}
			return $categoryHasChild;
		} else {
			return null;
		}
	}

	public function _uploadFile($file, $folder_name) {
		$time           = new DateTime;
		$year           = $time->format('Y');
		$month          = $time->format('m');
		$origin_name    = $file->getClientOriginalName();
		$file_extension = $file->getClientOriginalExtension();
		if (!is_null($file_extension)) {
			$file_name = snake_case(str_replace('.' . $file_extension, '', $origin_name));
			$path      = "uploads/{$folder_name}/{$year}/{$month}/" . time() . "_{$file_name}.{$file_extension}";
		} else {
			$path = "uploads/{$folder_name}/{$year}/{$month}/" . time() . "_{$file_name}";
		}
		$success = Storage::put($path, File::get($file));
		if ($success) {
			return $path;
		} else {
			throw new UploadFileException("can\'t upload file", 1);
		}
	}

	public function logout() {
		Auth::guard('admin')->logout();
		// Auth::logout();
		return redirect('admin/login');
	}

	public function admin_dashboard() {
		die('dashboard');
		return view('admin.dashboard');
	}

	/**
	 * [sendemail description]
	 * @param  [type] $fromemail [mail to send ]
	 * @param  [type] $toemail   [mail customer ]
	 * @param  [type] $subject   [title of mail]
	 * @param  [type] $content   [content of mail]
	 * @param  [type] $fullname  [name of k]
	 * @return [boolean]            [send success]
	 */
	public function sendemail($fromemail, $toemail, $subject = 'Mail to Globaltestpoint', $content) {
		$feedback['from_email'] = $fromemail;
		$feedback['to_email']   = $toemail;
		$feedback['subject']    = $subject;
		$feedback['content']    = $content;
		Mail::queue('mailletter.feedback', $feedback, function ($message) use ($feedback) {
			$message->from($feedback['from_email'], 'Globaltestpoint');
			$message->to($feedback['to_email'], 'customer')
				->subject($feedback['subject']);
		});
		return true;
	}

	// /**
	//  * [getIpAdress description]
	//  * @return [type] [description]
	//  */
	// public function getIpAdress() {
 //        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
 //            $ip = $_SERVER['HTTP_CLIENT_IP'];
 //        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
 //            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
 //        } else {
 //            $ip = $_SERVER['REMOTE_ADDR'];
 //        }
 //        return $ip;
 //    }

 //    public function allowIpLoginSession() {
 //        $user = Auth::user();
 //        $online_hash   = Session::get('online_hash');

 //        // var_dump(empty($user->online_hash));
        
 //        if(empty($user->online_hash)) {
 //        	$user->saveIpLogin();
 //        }

 //        if((!empty($user->online_hash)) && ($user->online_hash == $online_hash)) {
 //        	return true;
 //        } else {
 //        	var_dump(Session::get('test')); 
 //        	die;
 //        	// $user->removeIpLogin();
 //        	// Auth::logout();
 //        	return false;
 //        }
 //    }
}
