<?php

namespace App\Http\Controllers\Frontend;

use App\Entities\Category;
use App\Entities\Option;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class FrontendController extends Controller
{
    public function __construct()
    {}

    public function hitCounter(Request $request)
    {
        //========= user access =======
        if (!$request->is('api/*') && !$request->is('testyourself/blank-page/job-vacancies/*') && !$request->is('assets/*')) {
            $getviewsite = Option::select(['id', 'viewsite'])->first();
            $view        = Option::find($getviewsite['id']);
            if (!SESSION::has('online')) {
                $online         = $view['viewsite'] + 1;
                $view->viewsite = $online;
                $view->save();
                SESSION::put('online', $online);
            } else {
                SESSION::put('online', $view['viewsite']);
            }
        }
    }

    /**
     * [getNameCate return name of a category]
     * @return [string] [name of a category]
     */
    public function getNameCate($id)
    {
        $cate = Category::find($id);
        if (empty($cate)) {
            return false;
        } else {
            return $cate['name'];
        }
    }

    public function checkLogin()
    {
        $login = Auth::user();
        return $this->responseData($login);
    }
}
