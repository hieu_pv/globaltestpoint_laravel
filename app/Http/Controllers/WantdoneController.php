<?php

namespace App\Http\Controllers;

use App\Entities\Ads;
use App\Entities\Link;
use App\Entities\Option;
use App\Entities\Slider;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class WantdoneController extends FrontendController {
	public function __construct() {
		$this->hitCounter(new Request);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$video  = Ads::where('type', 0)->where('page', 1)->where('active', 1)->first();
		$flashs = Ads::where('type', 1)->where('page', 1)->where('active', 1)->get();
		//======= slider =============
		$sliders = Slider::where('page', 3)->where('active', 1)->orderBy('sort', 'desc')->orderBy('id', 'desc')->get();
		//======== blank homepage ======
		$option_site = Option::where('page', 1)->first();
		Session::put('ywd_optionsite', $option_site);
		$blankhomepage = Link::with('post')->where('active', 1)->where('page', 0)->where('position', 'homepage')->get();
		return view('wantdone.home.index', ['video' => $video, 'flashs' => $flashs, 'sliders' => $sliders, 'blankhomepage' => $blankhomepage]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		//
	}

	public function forum() {
		return view('forum.index');
	}
	public function installForum() {
		return view('forum.install.index');
	}
}
