<?php

namespace App\Http\Controllers\Backend;

use App\Entities\Option;
use App\Http\Controllers\Backend\AdminController;
use App\Http\Requests\OptionRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class OptionController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $option  = Option::where('page', 0)->first();
        $option2 = Option::where('page', 1)->first();
        return view('admin.option.index', ['option' => $option, 'option2' => $option2]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OptionRequest $request)
    {
        $option_first = Option::where('page', 0)->first();
        if (!empty($option_first)) {
            $option                           = Option::find($option_first['id']);
            $option->title                    = $request->get('title');
            $option->email                    = $request->get('email');
            $option->phone                    = $request->get('phone');
            $option->address                  = $request->get('address');
            $option->fax                      = $request->get('fax');
            $option->facebook                 = $request->get('facebook');
            $option->twitter                  = $request->get('twitter');
            $option->youtube                  = $request->get('youtube');
            $option->instagram                = $request->get('instagram');
            $option->linkedin                 = $request->get('linkedin');
            $option->termpolicy               = $request->get('termpolicy');
            $option->introduction_variousexam = $request->get('introduction_variousexam');

            if ($request->hasFile('option_logo')) {
                $file      = $request->file('option_logo');
                $filename  = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $namephoto = 'logo_' . time() . '.' . $extension;
                $file->move(public_path() . '/assets/frontend/images/logo', $namephoto);
                $option->logo = '/assets/frontend/images/logo/' . $namephoto;
                if (!empty($option_first['logo'])) {
                    \File::delete(public_path($option_first['logo']));
                }
            } else {
                $option->logo = $request->get('old_logo');
            }
            $option['page'] = 0;

            //=================================================
            if (!$option->save()) {
                return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Error !! Update Faild']);
            }
        } else {
            $option = $request->all();
            $option = $request->except(['_token', 'option_logo', 'btn_option', 'old_logo_ywd', 'ywd_title', 'ywd_email', 'ywd_phone', 'ywd_address', 'ywd_fax', 'ywd_facebook', 'ywd_twitter']);

            if ($request->hasFile('option_logo')) {
                if (in_array($request->file('option_logo')->getClientOriginalExtension(), ['jpg', 'png', 'jpeg', 'gif'])) {
                    $file1     = $request->file('option_logo');
                    $filename  = $file1->getClientOriginalName();
                    $extension = $file1->getClientOriginalExtension();
                    $namephoto = 'logo_' . time() . '.' . $extension;
                    $file1->move(public_path() . '/assets/frontend/images/logo', $namephoto);
                    $option['logo'] = '/assets/frontend/images/logo/' . $namephoto;
                } else {
                    return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Error !! Format file is incorrect']);
                }
            } else {
                $option['logo'] = $request->get('old_logo');
            }
            //===========================================
            if ($request->hasFile('image_login')) {
                if (in_array($request->file('image_login')->getClientOriginalExtension(), ['jpg', 'png', 'jpeg', 'gif'])) {
                    $file2       = $request->file('image_login');
                    $filename    = $file2->getClientOriginalName();
                    $extension   = $file2->getClientOriginalExtension();
                    $image_login = 'logo_' . time() . '.' . $extension;
                    $file2->move(public_path() . '/assets/frontend/images/logo', $image_login);
                    $option['image_login'] = '/assets/frontend/images/logo/' . $image_login;
                } else {
                    return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Error !! Format file is incorrect']);
                }
            }
            //=================================================
            if ($request->hasFile('image_register')) {
                if (in_array($request->file('image_register')->getClientOriginalExtension(), ['jpg', 'png', 'jpeg', 'gif'])) {
                    $file3          = $request->file('image_register');
                    $filename       = $file3->getClientOriginalName();
                    $extension      = $file3->getClientOriginalExtension();
                    $image_register = 'logo_' . time() . '.' . $extension;
                    $file3->move(public_path() . '/assets/frontend/images/logo', $image_register);
                    $option['image_register'] = '/assets/frontend/images/logo/' . $image_register;
                } else {
                    return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Error !! Format file is incorrect']);
                }
            }
            //=================================================
            if ($request->hasFile('image_login_register')) {
                if (in_array($request->file('image_login_register')->getClientOriginalExtension(), ['jpg', 'png', 'jpeg', 'gif'])) {
                    $file                 = $request->file('image_login_register');
                    $filename             = $file->getClientOriginalName();
                    $extension            = $file->getClientOriginalExtension();
                    $image_login_register = 'logo_' . time() . '.' . $extension;
                    $file->move(public_path() . '/assets/frontend/images/logo', $image_login_register);
                    $option['image_login_register'] = '/assets/frontend/images/logo/' . $image_login_register;
                } else {
                    return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Error !! Format file is incorrect']);
                }
            }
            $option['page'] = 0;
            //=================================================
            if (!Option::create($option)) {
                return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Error !! Update Faild']);
            }
        }
        $option_ywd = Option::where('page', 1)->first();
        if (!empty($option_ywd)) {
            $option2           = Option::find($option_ywd['id']);
            $option2->title    = $request->get('ywd_title');
            $option2->page     = 1;
            $option2->email    = $request->get('ywd_email');
            $option2->phone    = $request->get('ywd_phone');
            $option2->address  = $request->get('ywd_address');
            $option2->fax      = $request->get('ywd_fax');
            $option2->facebook = $request->get('ywd_facebook');
            $option2->twitter  = $request->get('ywd_twitter');

            if ($request->hasFile('option_logo_wandont')) {
                if (in_array($request->file('option_logo_wandont')->getClientOriginalExtension(), ['jpg', 'png', 'jpeg', 'gif'])) {
                    $file      = $request->file('option_logo_wandont');
                    $file_name = $file->getClientOriginalName();
                    $ext       = $file->getClientOriginalExtension();
                    $newName   = 'logoWandont_' . time() . '.' . $ext;
                    $file->move(public_path() . '/assets/frontend/images/logo/', $newName);
                    $option2->logo = '/assets/frontend/images/logo/' . $newName;
                    if (!empty($option_ywd['logo'])) {
                        \File::delete(public_path($option_ywd['logo']));
                    }
                } else {
                    return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Error !! Format file is incorrect']);
                }
            } else {
                $option2->logo = $request->get('old_logo_ywd');
            }
            // dd($option2);
            if (!$option2->update()) {
                return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Error !! Update Faild']);
            }
            return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Success ! Updated information website complete !']);
        } else {
            $option2           = new Option;
            $option2->title    = $request->get('ywd_title');
            $option2->page     = 1;
            $option2->email    = $request->get('ywd_email');
            $option2->phone    = $request->get('ywd_phone');
            $option2->address  = $request->get('ywd_address');
            $option2->fax      = $request->get('ywd_fax');
            $option2->facebook = $request->get('ywd_facebook');
            $option2->twitter  = $request->get('ywd_twitter');

            if ($request->hasFile('option_logo_wandont')) {
                if (in_array($request->file('option_logo_wandont')->getClientOriginalExtension(), ['jpg', 'png', 'jpeg', 'gif'])) {
                    $file      = $request->file('option_logo_wandont');
                    $filename  = $file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $namephoto = 'logo_' . time() . '.' . $extension;
                    $file->move(public_path() . '/assets/frontend/images/logo', $namephoto);
                    $option2->logo = '/assets/frontend/images/logo/' . $namephoto;
                } else {
                    return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Error !! Format file is incorrect']);
                }
            } else {
                $option2->logo = $request->get('old_logo_ywd');
            }
            // ========================
            if (!$option2->save()) {
                return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Error !! Update Faild']);
            }
            return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Success ! Updated information website complete !']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * [feedback description]
     * @return [type] [description]
     */
    public function feedback()
    {
        return view('feedback/index');
    }

    /**
     * [getfeedback description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getfeedback(Request $request)
    {
        if (Session::has('optionsite')) {
            $tmp = Session::get('optionsite');
            if (!empty($tmp)) {
                $feedback['tomail'] = $tmp['email'];
            }
        } else {
            $feedback['tomail'] = "canmotcaiten1993@gmail.com";
        }

        $feedback['email']    = $request->get('email');
        $feedback['fullname'] = $request->get('fullname');
        $feedback['subject']  = $request->get('subject');
        $feedback['content']  = $request->get('message');
        Mail::queue('email.feedback', $feedback, function ($message) use ($feedback) {
            $message->from($feedback['email'], $feedback['fullname']);
            $message->to($feedback['tomail'], 'Mr. David')
                ->subject($feedback['subject']);
        });
        return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Success !! Send feedback successful']);
    }
}
