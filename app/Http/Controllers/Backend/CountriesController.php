<?php

namespace App\Http\Controllers\Backend;

use App\Entities\Country;
use App\Http\Controllers\Backend\AdminController;
use App\Http\Requests\CountryRequest;
use App\Repositories\CountryRepository;
use Illuminate\Http\Request;

class CountriesController extends AdminController
{
    protected $repository;

    public function __construct(CountryRepository $repository)
    {
        $this->repository = $repository;
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = $this->repository->getAll();
        $data      = [
            'title'      => 'Globaltestpoint - List Country',
            'tableTitle' => 'List Country',
            'countries'  => $countries,
        ];
        return view('admin.country.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'title'       => 'Globaltestpoint - Add new Country',
            'tableTitle'  => 'Add new Country',
            'buttonTitle' => 'Add',
        ];
        return view('admin.country.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\CountryRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CountryRequest $request)
    {
        $data = $request->all();

        $existsCode = $this->repository->findByField('code', $data['code'])->first();
        if ($existsCode) {
            return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Code has already exists']);
        }

        $data['slug'] = str_slug($data['name']);
        $i            = 1;
        while (!Country::where('slug', $data['slug'])->get()->isEmpty()) {
            $data['slug'] = str_slug($data['name']) . '-' . $i++;
        }

        if (!isset($data['active'])) {
            $data['active'] = 0;
        }
        $country = $this->repository->create($data);
        return redirect('admin/country')->with(['flash_level' => 'success', 'flash_message' => 'Add country success']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $country = Country::findOrFail($id);
        $data    = [
            'title'       => 'Globaltestpoint - Edit Country',
            'tableTitle'  => 'Edit Country',
            'buttonTitle' => 'Update',
            'country'     => $country,
        ];
        return view('admin.country.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\CountryRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CountryRequest $request, $id)
    {
        $data = $request->all();

        $existsCode = Country::where('id', '!=', $id)->where('code', $data['code'])->first();
        if ($existsCode) {
            return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Code has already exists']);
        }

        if (!isset($data['active'])) {
            $data['active'] = 0;
        }
        $country = $this->repository->update($data, $id);
        return redirect('admin/country')->with(['flash_level' => 'success', 'flash_message' => 'Update country success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $country = Country::findOrFail($id);
        $this->repository->delete($id);
        return redirect('admin/country')->with(['flash_level' => 'success', 'flash_message' => 'Delete country success']);
    }
}
