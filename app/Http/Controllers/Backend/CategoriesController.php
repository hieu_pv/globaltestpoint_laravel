<?php

namespace App\Http\Controllers\Backend;

use App\Entities\Category;
use App\Entities\Type;
use App\Http\Controllers\Backend\AdminController;
use App\Http\Requests\CategoryRequest;
use App\Repositories\CategoriesRepository;
use Illuminate\Http\Request;
use Validator;

class CategoriesController extends AdminController {
	private $repository;

	public function __construct(CategoriesRepository $repository) {
		parent::__construct();
		$this->repository = $repository;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$categories = $this->repository->getAll();
		if (count($categories) > 0) {
			$categories = $categories->map(function ($item) {
				$item->parent_name = $this->getParentName($item['parent_id']);
				return $item;
			});
		}
		return view('admin.category.index', compact('categories'));
	}

	/**
	 * [getParentName return name of a category]
	 * @return [string] [name of a category]
	 */
	public function getParentName($cate_id) {
		$category = Category::find($cate_id);
		if (empty($category)) {
			return false;
		} else {
			return $category['name'];
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$categories = $this->repository->getAll();
		$types      = Type::all();
		return view('admin.category.add', ['categories' => $categories, 'types' => $types]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(CategoryRequest $request) {
		$data         = $request->except('_token');
		if ($data['parent_id'] == '') {
			$data['parent_id'] = 0;
		}
		if (!isset($data['active'])) {
			$data['active'] = 0;
		}
		$data['position'] = 0;
		if ($request->has('position')) {
			$data['position'] = $request->get('position');
		}
		$category = $this->repository->create($data);
		return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'success !! Complete Add Categories']);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {

	}
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$category   = Category::find($id);
		$categories = $this->repository->getAll();
		$types      = Type::all();
		return view('admin.category.edit', compact('category', 'categories', 'types'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\CategoryRequest  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(CategoryRequest $request, $id) {
		$data      = $request->all();
		$category  = Category::find($id);
		if ($data['parent_id'] == '') {
			$data['parent_id'] = 0;
		}
		if (!isset($data['active'])) {
			$data['active'] = 0;
		}
		$data['position'] = $request->get('position')[0];
		$updateCategory = $this->repository->update($data, $id);
		return redirect()->route('admin.category.getList')->with(['flash_level' => 'success', 'flash_message' => 'success !! Complete Update Categories']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$category = Category::find($id);
		if ($category == null) {
			return redirect()->route('admin.category.getList')->with(['flash_level' => 'danger', 'flash_message' => "Delete error, don't exist this Category"]);
		} else {

			// Update child category to grand category
			$childCategories = $this->repository->getChildCategory($id);
			if (count($childCategories) > 0) {
				foreach ($childCategories as $childCategory) {
					$childCategory['parent_id'] = $category['parent_id'];
					$childCategory              = $childCategory->toArray();
					$addChildCategory           = $this->repository->update($childCategory, $childCategory['id']);
				}
			}
			$this->repository->delete($id);
			return redirect()->route('admin.category.getList')->with(['flash_level' => 'success', 'flash_message' => 'Delete category success']);
		}
	}
}
