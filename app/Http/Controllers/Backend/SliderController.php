<?php

namespace App\Http\Controllers\Backend;

use App\Entities\Slider;
use App\Http\Controllers\Backend\AdminController;
use Illuminate\Http\Request;

class SliderController extends AdminController {

	public function __construct() {
		parent::__construct();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$sliders = Slider::orderBy('sort', 'desc')->orderBy('id', 'desc')->get();
		return view('admin.slider.index', ['sliders' => $sliders]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		return view('admin.slider.add');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		if ($request->hasFile('image')) {
			foreach ($request->file('image') as $keyimage => $image) {
				$slider    = new Slider;
				$filename  = $image->getClientOriginalName();
				$extension = $image->getClientOriginalExtension();
				$namephoto = 'slider_' . time() . '.' . $extension;
				$image->move(public_path() . '/assets/frontend/images/slider', $namephoto);
				$slider->image = '/assets/frontend/images/slider/' . $namephoto;
				$slider->page  = $request->get('page');
				$slider->sort  = $request->get('sort');
				if ($request->get('active_blank') == 'on') {
					$slider->active = 1;
				} else {
					$slider->active = 0;
				}
				if ($slider->save()) {
					return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Success ! Added Slider complete']);
				} else {
					return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Error ! Added slider Uncomplete']);
				}
			}
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$slider = Slider::find($id);
		return view('admin.slider.edit', ['slider' => $slider]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		$slider = Slider::find($id);
		if (!empty($slider)) {
			if ($request->hasFile('image')) {
				$image     = $request->file('image');
				$filename  = $image->getClientOriginalName();
				$extension = $image->getClientOriginalExtension();
				$namephoto = 'slider_' . time() . '.' . $extension;
				$image->move(public_path() . '/assets/frontend/images/slider', $namephoto);
				$slider->image = '/assets/frontend/images/slider/' . $namephoto;
			} else {
				$slider->image = $request->get('old_image');
			}
			$slider->page = $request->get('page');
			$slider->sort = $request->get('sort');
			if ($request->get('active_slider') == 'on') {
				$slider->active = 1;
			} else {
				$slider->active = 0;
			}
			if ($slider->save()) {
				return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Success ! Added Slider complete']);
			} else {
				return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Error ! Added slider Uncomplete']);
			}
		} else {
			return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Error ! Slider not found !']);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Request $request) {
		if ($request->has('id_delete_slider-1')) {
			$id     = $request->get('id_delete_slider-1');
			$slider = Slider::find($id);
			if (!empty($slider)) {
				$slider->delete();
				return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Success !! Delete Successful !']);
			} else {
				return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Error !! Delete Faild']);
			}
		} else {
			$id     = $request->get('id_delete_slider-2');
			$slider = Slider::find($id);
			if (!empty($slider)) {
				$slider->delete();
				return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Success !! Delete Successful !']);
			} else {
				return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Error !! Delete Faild']);
			}
		}
	}
}
