<?php

namespace App\Http\Controllers\Backend;

use App\Entities\Document;
use App\Http\Controllers\Backend\AdminController;
use App\Http\Requests\DocumentRequest;
use App\Repositories\DocumentRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;

class DocumentsController extends AdminController {
	protected $repository;

	public function __construct(DocumentRepository $repository) {
		$this->repository = $repository;
		parent::__construct();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$documents = Document::orderBy('order', 'desc')->orderBy('id', 'desc')->get();
		if (!empty($documents)) {
			$documents = $documents->map(function ($document) {
				$category                  = $this->repository->getCategoryOfDocument($document['id']);
				$document['category_name'] = $category['name'];
				return $document;
			});
		}
		$data = [
			'title'      => 'Globaltestpoint - List Document',
			'tableTitle' => 'List Document',
			'documents'  => $documents,
		];
		return view('admin.document.list', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$categories = $this->repository->getCategory();
		$data       = [
			'title'       => 'Globaltestpoint - Add new Document',
			'tableTitle'  => 'Add new Document',
			'buttonTitle' => 'Add',
			'categories'  => $categories,
		];
		return view('admin.document.add', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\DocumentRequest  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(DocumentRequest $request) {
		$data      = $request->all();
		$validator = Validator::make($data, [
			'image'    => 'required|mimes:jpg,jpeg,png,gif',
			'document' => 'required|mimes:doc,docx,pdf,csv,xl,xls,ppt,pptx',
		]);
		if ($validator->fails()) {
			return redirect('admin/document/create')
				->withErrors($validator)
				->withInput();
		}
		if (!isset($data['active'])) {
			$data['active'] = 0;
		}
		if ($request->hasFile('image')) {
			$image         = $request->file('image');
			$uploadImage   = $this->_uploadFile($image, 'documents');
			$data['image'] = $uploadImage;
		}
		if ($request->hasFile('document')) {
			$document         = $request->file('document');
			$uploadDocument   = $this->_uploadFile($document, 'documents');
			$data['document'] = $uploadDocument;
		}
		$document = $this->repository->create($data);
		return redirect('admin/document')->with(['flash_level' => 'success', 'flash_message' => 'Add document success']);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$categories = $this->repository->getCategory();
		$document   = Document::findOrFail($id);
		$data       = [
			'title'       => 'Globaltestpoint - Edit document',
			'tableTitle'  => 'Edit document',
			'buttonTitle' => 'Update',
			'document'    => $document,
			'categories'  => $categories,
		];
		return view('admin.document.edit', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\DocumentRequest  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(DocumentRequest $request, $id) {
		$document = Document::find($id);
		if (!$document) {
			throw new Exception("id doesn't exists", 1);
		}
		$data = $request->all();
		if (!isset($data['active'])) {
			$data['active'] = 0;
		}
		if ($request->hasFile('image')) {
			Storage::delete($document['image']);
			$image         = $request->file('image');
			$uploadImage   = $this->_uploadFile($image, 'documents');
			$data['image'] = $uploadImage;
		}
		if ($request->hasFile('document')) {
			Storage::delete($document['document']);
			$document         = $request->file('document');
			$uploadDocument   = $this->_uploadFile($document, 'documents');
			$data['document'] = $uploadDocument;
		}
		$document = $this->repository->update($data, $id);
		return redirect('admin/document')->with(['flash_level' => 'success', 'flash_message' => 'Update document success']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$document = Document::find($id);
		if (!$document) {
			throw new Exception("id doesn't exists", 1);
		}
		if (isset($document['image']) && $document['image'] !== '') {
			Storage::delete($document['image']);
		}
		if (isset($document['document']) && $document['document'] !== '') {
			Storage::delete($document['document']);
		}
		$this->repository->delete($id);
		return redirect('admin/document')->with(['flash_level' => 'success', 'flash_message' => 'Delete document success']);
	}
}
