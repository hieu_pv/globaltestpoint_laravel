<?php

namespace App\Http\Controllers\Backend;

use App\Entities\Answer;
use App\Entities\Category;
use App\Entities\Exam;
use App\Entities\Question;
use App\Http\Controllers\Backend\AdminController;
use App\Http\Requests\CSVRequest;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class QuestionController extends AdminController {

	public function __construct() {
		parent::__construct();
	}

	public function getAdd($id) {
		$exam      = Exam::find($id);
		$category  = Category::find($exam['category_id']);
		$questions = $exam->questions;
		$answers   = $questions->map(function ($item) {
			$question = Question::find($item->id);
			$answer   = $question->answers;
			return $answer;
		});
		return view('admin.question.add', compact('exam', 'category', 'questions', 'answers'));
	}

	public function postAdd($id, Request $request) {
		$exam     = Exam::findOrFail($id);
		$question = new Question($request->all());
		$exam->questions()->save($question);
		$data    = $request->all();
		$answers = new Collection($data['answers']);
		$answers = $answers->map(function ($answer) {
			if ($answer['is_correct'] == true) {
				$answer['is_correct'] = 1;
			} else {
				$answer['is_correct'] = 0;
			}
			return new Answer($answer);
		});
		$question->answers()->saveMany(array_values($answers->all()));
		return redirect()->route('admin.question.getAdd', $id);
	}

	public function addQuestionByCSVFile($id, CSVRequest $request) {
		$file      = $request->file('csv_file');
		$fileName  = $file->getClientOriginalName();
		$extension = $file->getClientOriginalExtension();
		if ($extension != 'csv') {
			return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Please select an csv file']);
		}
		$pathCSV = $file->getPathName();
		$this->ImportCsvToQuestionAnswer($pathCSV, $id);
		return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Question has been added to the database.']);
	}

	public function getUpdate($id) {
		$question = Question::find($id);
		if ($question != null) {
			$next = Question::where('id', '>', $question->id)->min('id');
		} else {
			$next = null;
		}
		return view('admin.question.edit', compact('question', 'next'));
	}

	public function postUpdate($id, Request $request) {
		$data = $request->all();
		if ($data != null) {
			$data['exam_id'] = $data['exam']['id'];
			$question        = Question::find($data['id']);
			$question->update($data);

			// Add or Update Answer
			if (isset($data['answers']) && count($data['answers']['data']) > 0) {
				$answers = $data['answers']['data'];
				foreach ($answers as $answer) {
					if (isset($answer['id']) && $answer['id'] != null) {
						$answerInstance = Answer::find($answer['id']);
						if ($answerInstance == null) {
							throw new Exception("Answer id: {$answer['id']} does'nt exists", 1);
						}
						$answerInstance->update($answer);
					} else {
						$newAnswer              = new Answer;
						$newAnswer->question_id = $question->id;
						$newAnswer->answer      = $answer['answer'];
						$newAnswer->is_correct  = $answer['is_correct'];
						$newAnswer->save();
					}
				}
			}
			return redirect()->route('admin.question.getUpdate', $id);
		} else {
			throw new Exception("Not data", 1);
		}
	}

	public function getDelete($id) {
		$question = Question::find($id);
		$question->delete();
		return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Success !! Delete question']);
	}
}
