<?php

namespace App\Http\Controllers\Backend;

use App\Entities\Country;
use App\Entities\State;
use App\Http\Controllers\Backend\AdminController;
use App\Http\Requests\StateRequest;
use App\Repositories\StateRepository;
use Illuminate\Http\Request;

class StatesController extends AdminController {

	protected $repository;

	public function __construct(StateRepository $repository) {
		$this->repository = $repository;
		parent::__construct();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$states = $this->repository->getAll();
		if (!empty($states)) {
			foreach ($states as $state) {
				$country               = $this->repository->getCountryofState($state['id']);
				$state['country_name'] = $country['name'];
			}
		}
		$data = [
			'title'      => 'Globaltestpoint - List State',
			'tableTitle' => 'List State',
			'states'     => $states,
		];
		return view('admin.state.list', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$countries = Country::all();
		$data      = [
			'title'       => 'Globaltestpoint - Add new State',
			'tableTitle'  => 'Add new State',
			'buttonTitle' => 'Add',
			'countries'   => $countries,
		];
		return view('admin.state.add', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\StateRequest  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(StateRequest $request) {
		$data = $request->all();
		if (!isset($data['active'])) {
			$data['active'] = 0;
		}
		$state = $this->repository->create($data);
		return redirect('admin/state')->with(['flash_level' => 'success', 'flash_message' => 'Add state success']);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$countries = Country::all();
		$state     = State::findOrFail($id);
		$data      = [
			'title'       => 'Globaltestpoint - Edit State',
			'tableTitle'  => 'Edit State',
			'buttonTitle' => 'Update',
			'state'       => $state,
			'countries'   => $countries,
		];
		return view('admin.state.edit', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\StateRequest  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(StateRequest $request, $id) {
		$id;
		$data = $request->all();
		if (!isset($data['active'])) {
			$data['active'] = 0;
		}
		$state = $this->repository->update($data, $id);
		return redirect('admin/state')->with(['flash_level' => 'success', 'flash_message' => 'Update state success']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$state = State::findOrFail($id);
		$this->repository->delete($id);
		return redirect('admin/state')->with(['flash_level' => 'success', 'flash_message' => 'Delete state success']);
	}
}
