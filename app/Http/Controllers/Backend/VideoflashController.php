<?php

namespace App\Http\Controllers\Backend;

use App\Entities\Ads;
use App\Entities\User;
use App\Http\Controllers\Backend\AdminController;
use App\Http\Requests;
use App\Http\Requests\VideoflashRequest;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class VideoflashController extends AdminController
{
    public function __construct() {
        parent::__construct();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videos = Ads::where('type',0)->get();
        $videos = $videos->map(function($item){
            $user = User::find($item->author);
            $item->nameuser = $user->last_name;
            return $item;
        });       
        $flashs = Ads::where('type',1)->get();
        $flashs = $flashs->map(function($item){
            $user = User::find($item->author);
            $item->nameuser = $user->last_name;
            return $item;
        }); 
        return view('admin.videoflash.index',['videos'=>$videos, 'flashs'=>$flashs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.videoflash.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\VideoflashRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->has('addvideo'))
        {
            $ads = new Ads;
            $ads->link = $request->get('inp_video');
            if(!str_is('https://www.youtube.com/*', $ads->link))
            {
                return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Error ! Format link youtube is incorrect!']);
            }
            $ads->type = 0;
            if($request->get('activevideo') == 'on')
            {
                $ads->active = 1;
            }
            else
            {
                $ads->active = 0;
            }
            $ads->author = 1;
            $count_ads = Ads::where('type', 0)->where('page', $request->get('page_video'))->count();
            if($count_ads >= 1)
            {
                $result = 0;
                $message = "Only add 1 video in a Page";
            }
            else
            {
                $ads->page = $request->get('page_video');
                $ads->author = Auth::user()->id;
                if($ads->save())
                {
                    $result = true;
                    $message = "Success ! Add a video";
                }
                else
                {
                    $result = false;
                    $message = "Don't video";
                }
            }    
            return view('admin.videoflash.add', ['result' => $result, 'message' => $message]);
        }
        else
        {
            if($request->has('addflash'))
            {
                
                $ads = new Ads;
                $ads->type = 1;
                if($request->get('activeflash') == 'on')
                {
                    $ads->active = 1;
                }
                else
                {
                    $ads->active = 0;
                }
                $ads->author = 1;
                $count_ads = Ads::where('type', 1)->where('page', $request->get('page_flash'))->count();
                if($count_ads >= 2)
                {
                    $result2 = false;
                    $message2 = "Only add 2 flash in a Page";
                }
                else
                {
                    $ads->page = $request->get('page_flash');
                    if($request->hasFile('inp_flash'))
                    {
                        $filename = $request->file('inp_flash');
                        $nameoriginal = $filename->getClientOriginalName();
                        $extension = $filename->getClientOriginalExtension();
                        $nameflash = 'flash_'.(time()).'.'.$extension;
                        $filename->move(public_path().'/assets/frontend/images/ads', $nameflash);
                        $ads->link = 'assets/frontend/images/ads/'.$nameflash;
                    }
                    else
                    {
                        return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Error ! Input Flash is require']);
                    }
                    $ads->author = Auth::user()->id;
                    if($ads->save())
                    {
                        $result2 = true;
                        $message2 = " Added flash";
                    }
                    else
                    {
                        $result2 = false;
                        $message2 = "Don't add flash";
                    }
                }
                return view('admin.videoflash.add', ['result2' => $result2, 'message2' => $message2]);
            }
            else
            {
                return view('admin.videoflash.add');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $tys_videos = Post::where('type',0)->where('page',0);
        $tys_flashs  = Post::where('type', 1)->where('page',1);
        $ywd_videos = Post::where('type',0)->where('page',0);
        $ywd_flashs  = Post::where('type', 1)->where('page',1);
        return view('admin.videoflash.index',['tsy_videos'=>$tys_videos, 'tys_flashs'=>$tys_flashs, 'ywd_videos' => $ywd_videos, 'ywd_flashs' => $ywd_flashs]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\VideoflashRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if($request->has('editvideo'))
        {
            $id = $request->get('id_video');
            $video = Ads::find($id);
            // chua lay ra so luong tren 1 trang
            if(empty($video))
            {
                $result = false;
                $message = "Error ! Not found link video";
            }
            else
            {
                if($video['page'] == $request->get('page'))
                {
                    if($request->get('activevideo') == 'on')
                    {
                        $video->active = 1;
                    }
                    else
                    {
                        $video->active = 0;
                    }
                    $video->link = $request->get('inp_video');
                    if($video->save())
                    {
                        $result = true;
                        $message = "Success ! Edited";
                    }
                    else
                    {
                        $result = false;
                        $message = "Error ! Don't Edit";
                    }
                }
                else
                {
                    $video->page = $request->get('page');
                    $count_page = Ads::where('type',0)->where('page',$video->page)->count();
                    if($count_page >= 1)
                    {
                        $result = false;
                        $message = "Error ! Only add a video on a page";
                    }
                    else
                    {
                        $video->author = Auth::user()->id;
                        if($request->get('activevideo') == 'on')
                        {
                            $video->active = 1;
                        }
                        else
                        {
                            $video->active = 0;
                        }
                        $video->link = $request->get('inp_video');
                        if($video->save())
                        {
                            $result = true;
                            $message = "Success ! Edited";
                        }
                        else
                        {
                            $result = false;
                            $message = "Error ! Don't Edit";
                        }
                    }
                }                
            }
            //========================================
            $videos = Ads::where('type',0)->get();
            $videos = $videos->map(function($item){
                $user = User::find($item->author);
                $item->nameuser = $user->last_name;
                return $item;
            });       
            $flashs = Ads::where('type',1)->get();
            $flashs = $flashs->map(function($item){
                $user = User::find($item->author);
                $item->nameuser = $user->last_name;
                return $item;
            }); 
            return view('admin.videoflash.index',['videos'=>$videos, 'flashs'=>$flashs, 'result'=>$result, 'message'=>$message]);
        }
        if($request->has('editflash'))
        {
            $id = $request->get('id_flash');
            $flash = Ads::find($id);
            if(empty($flash))
            {
                $result2 = false;
                $message2 = "Error ! Not found image flash";
            }
            else
            {
                if($flash['page'] == $request->get('page'))
                {
                    if($request->get('activeflash') == 'on')
                    {
                        $flash->active = 1;
                    }
                    else
                    {
                        $flash->active = 0;
                    }
                    if($request->hasFile('inp_flash_'.$id))
                    {
                        $filename = $request->file('inp_flash_'.$id);
                        $nameoriginal = $filename->getClientOriginalName();
                        $extension = $filename->getClientOriginalExtension();
                        $nameflash = 'flash_'.(time()).'.'.$extension;
                        $filename->move(public_path().'/assets/frontend/images/ads', $nameflash);
                        $flash->link = 'assets/frontend/images/ads/'.$nameflash;
                    }
                    else
                    {
                        $flash->link = $request->get('readonlyflash_'.$id);
                    }
                    $flash->author = Auth::user()->id;
                    if($flash->save())
                    {
                        $result2 = true;
                        $message2 = "Success ! Edited 1";
                    }
                    else
                    {
                        $result2 = false;
                        $message2 = "Error ! Don't Edit";
                    }
                }
                else
                {
                    $flash->page = $request->get('page');
                    $count_page = Ads::where('type',1)->where('page',$flash->page)->count();
                    if($count_page >= 2)
                    {
                        $result2 = false;
                        $message2 = "Error ! Only add 2 flashs on a page";
                    }
                    else
                    {
                        if($request->get('activeflash') == 'on')
                        {
                            $flash->active = 1;
                        }
                        else
                        {
                            $flash->active = 0;
                        }
                        if($request->hasFile('inp_flash_'.$id))
                        {
                            $filename = $request->file('inp_flash_'.$id);
                            $nameoriginal = $filename->getClientOriginalName();
                            $extension = $filename->getClientOriginalExtension();
                            $nameflash = 'flash_'.(time()).'.'.$extension;
                            $filename->move(public_path().'/assets/frontend/images/ads', $nameflash);
                            $flash->link = 'assets/frontend/images/ads/'.$nameflash;
                        }
                        else
                        {
                            $flash->link = $request->get('readonlyflash_'.$id);
                        }
                        if($flash->save())
                        {
                            $result2 = true;
                            $message2 = "Success ! Edited 2";
                        }
                        else
                        {
                            $result2 = false;
                            $message2 = "Error ! Don't Edit";
                        }
                    }
                }
            }
            //===================================
            $videos = Ads::where('type',0)->get();
            $videos = $videos->map(function($item){
                $user = User::find($item->author);
                $item->nameuser = $user->last_name;
                return $item;
            });       
            $flashs = Ads::where('type',1)->get();
            $flashs = $flashs->map(function($item){
                $user = User::find($item->author);
                $item->nameuser = $user->last_name;
                return $item;
            }); 
            return view('admin.videoflash.index',['videos'=>$videos, 'flashs'=>$flashs, 'result2'=>$result2, 'message2'=>$message2]);
        }
        if($request->has('btn_delete'))
        {
            $ads = Ads::find($request->get('id_delete_video'));
            if(empty($ads))
            {
                $result = false;
                $message = "Error ! Don't Delete";
            }
            else
            {
                if($ads->delete())
                {
                    $result = true;
                    $message = "Success ! Deleted";
                }
                else
                {
                    $result = false;
                    $message = "Error ! Don't Delete";
                }
            }
            $videos = Ads::where('type',0)->get();
            $videos = $videos->map(function($item){
                $item->nameuser = $item->user['name'];
                return $item;
            });       
            $flashs = Ads::where('type',1)->get();
            $flashs = $flashs->map(function($item){
                $item->nameuser = $item->user['name'];
                return $item;
            }); 
            return view('admin.videoflash.index',['videos'=>$videos, 'flashs'=>$flashs, 'result'=>$result, 'message'=>$message]);
        }
        if($request->has('btn_delete_flash'))
        {
            $ads = Ads::find($request->get('id_delete_flash'));
            if(empty($ads))
            {
                $result2 = false;
                $message2 = "Error ! Don't Delete";
            }
            else
            {
                if($ads->delete())
                {
                    $result2 = true;
                    $message2 = "Success ! Deleted";
                }
                else
                {
                    $result2 = false;
                    $message2 = "Error ! Don't Delete";
                }
            }
            $videos = Ads::where('type',0)->get();
            $videos = $videos->map(function($item){
                $item->nameuser = $item->user['name'];
                return $item;
            });       
            $flashs = Ads::where('type',1)->get();
            $flashs = $flashs->map(function($item){
                $item->nameuser = $item->user['name'];
                return $item;
            }); 
            return view('admin.videoflash.index',['videos'=>$videos, 'flashs'=>$flashs, 'result2'=>$result2, 'message2'=>$message2]);
        }
        $videos = Ads::where('type',0)->get();
        $videos = $videos->map(function($item){
            $item->nameuser = $item->user['name'];
            return $item;
        });       
        $flashs = Ads::where('type',1)->get();
        $flashs = $flashs->map(function($item){
            $item->nameuser = $item->user['name'];
            return $item;
        }); 
        return view('admin.videoflash.index',['videos'=>$videos, 'flashs'=>$flashs]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        
    }
}
