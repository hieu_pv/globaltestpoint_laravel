<?php

namespace App\Http\Controllers\Backend;

use App\Entities\School;
use App\Http\Controllers\Backend\AdminController;
use App\Http\Requests\SchoolRequest;
use App\Repositories\SchoolRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;

class SchoolsController extends AdminController {
	protected $repository;

	public function __construct(SchoolRepository $repository) {
		$this->repository = $repository;
		parent::__construct();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$schools = $this->repository->getAll();
		$data    = [
			'title'      => 'Globaltestpoint - List School',
			'tableTitle' => 'List School',
			'schools'    => $schools,
		];
		return view('admin.school.list', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$countries      = $this->repository->getCountries();
		$states         = $this->repository->getStates(1);
		$school_types   = $this->repository->getSchoolTypes();
		$school_degrees = $this->repository->getSchoolDegrees();
		$data           = [
			'title'          => 'Globaltestpoint - Add new School',
			'tableTitle'     => 'Add new School',
			'buttonTitle'    => 'Add',
			'countries'      => $countries,
			'states'         => $states,
			'school_types'   => $school_types,
			'school_degrees' => $school_degrees,
		];
		return view('admin.school.add', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\SchoolRequest  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(SchoolRequest $request) {
		$data      = $request->all();
		$validator = Validator::make($data, [
			'image' => 'required|mimes:jpg,jpeg,png,gif',
		]);
		if ($validator->fails()) {
			return redirect('admin/school/create')
				->withErrors($validator)
				->withInput();
		}
		if (!isset($data['active'])) {
			$data['active'] = 0;
		}
		if ($request->hasFile('image')) {
			$image         = $request->file('image');
			$uploadImage   = $this->_uploadFile($image, 'schools');
			$data['image'] = $uploadImage;
		}
		$school = $this->repository->create($data);
		return redirect('admin/school')->with(['flash_level' => 'success', 'flash_message' => 'Add school success']);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$school         = School::findOrFail($id);
		$countries      = $this->repository->getCountries();
		$states         = $this->repository->getStates();
		$school_types   = $this->repository->getSchoolTypes();
		$school_degrees = $this->repository->getSchoolDegrees();
		$data           = [
			'title'          => 'Globaltestpoint - Edit School',
			'tableTitle'     => 'Edit School',
			'buttonTitle'    => 'Update',
			'school'         => $school,
			'countries'      => $countries,
			'states'         => $states,
			'school_types'   => $school_types,
			'school_degrees' => $school_degrees,
		];
		return view('admin.school.edit', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\SchoolRequest  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(SchoolRequest $request, $id) {
		$data   = $request->all();
		$school = School::find($id);
		if (!$school) {
			throw new Exception("id doesn't exists", 1);
		}
		if (!isset($data['active'])) {
			$data['active'] = 0;
		}
		if ($request->hasFile('image')) {
			Storage::delete($school['image']);
			$image         = $request->file('image');
			$uploadImage   = $this->_uploadFile($image, 'schools');
			$data['image'] = $uploadImage;
		}
		$school = $this->repository->update($data, $id);
		return redirect('admin/school')->with(['flash_level' => 'success', 'flash_message' => 'Update school success']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$school = School::find($id);
		if (!$school) {
			throw new Exception("id doesn't exists", 1);
		}
		if (isset($school['image']) && $school['image'] !== '') {
			Storage::delete($school['image']);
		}
		$this->repository->delete($id);
		return redirect('admin/school')->with(['flash_level' => 'success', 'flash_message' => 'Delete school success']);
	}
}
