<?php

namespace App\Http\Controllers\Backend;

use App\Entities\Answer;
use App\Http\Controllers\Backend\AdminController;

class AnswerController extends AdminController {

	public function __construct() {
		parent::__construct();
	}
	
	public function getDelete($id) {
		$answer = Answer::find($id);
		$answer->delete();
		return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Success !! Delete Answer']);
	}
}
