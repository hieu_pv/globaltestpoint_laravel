<?php

namespace App\Http\Controllers\Backend;

use App\Entities\SchoolDegree;
use App\Http\Controllers\Backend\AdminController;
use App\Http\Requests\SchoolDegreeRequest;
use App\Repositories\SchoolDegreeRepository;
use Illuminate\Http\Request;
use Validator;

class SchoolDegreesController extends AdminController {
	protected $repository;

	public function __construct(SchoolDegreeRepository $repository) {
		$this->repository = $repository;
		parent::__construct();
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$school_degrees = $this->repository->getAll();
		$data           = [
			'title'          => 'Globaltestpoint - List School Degree',
			'tableTitle'     => 'List School Degree',
			'school_degrees' => $school_degrees,
		];
		return view('admin.school_degree.list', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$data = [
			'title'       => 'Globaltestpoint - Add School Degree',
			'tableTitle'  => 'Add School Degree',
			'buttonTitle' => 'Add',
		];
		return view('admin.school_degree.add', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\SchoolDegreeRequest  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(SchoolDegreeRequest $request) {
		$data = $request->all();
		if (!isset($data['active'])) {
			$data['active'] = 0;
		}
		$school_degree = $this->repository->create($data);
		return redirect('admin/school_degree')->with(['flash_level' => 'success', 'flash_message' => 'Add school degree success']);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$school_degree = SchoolDegree::findOrFail($id);
		$data          = [
			'title'         => 'Globaltestpoint - Edit School Degree',
			'tableTitle'    => 'Edit School Degree',
			'buttonTitle'   => 'Update',
			'school_degree' => $school_degree,
		];
		return view('admin.school_degree.edit', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\SchoolDegreeRequest  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(SchoolDegreeRequest $request, $id) {
		$data         = $request->all();
		$schoolDegree = SchoolDegree::find($id);
		if (!isset($data['active'])) {
			$data['active'] = 0;
		}
		$school_degree = $this->repository->update($data, $id);
		return redirect('admin/school_degree')->with(['flash_level' => 'success', 'flash_message' => 'Update school degree success']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$school_degree = SchoolDegree::findOrFail($id);
		$this->repository->delete($id);
		return redirect('admin/school_degree')->with(['flash_level' => 'success', 'flash_message' => 'Delete school degree success']);
	}
}
