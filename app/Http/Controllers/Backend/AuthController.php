<?php

namespace App\Http\Controllers\Backend;

use App\Entities\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    use ThrottlesLogins;

    protected $redirectTo          = '/admin';
    protected $redirectAfterLogout = '/admin/login';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function getLogin()
    {
        return view('admin.login');
    }

    public function postLogin(LoginRequest $request)
    {
        $data = $request->all();
        if (Auth::attempt(['email' => $data['email'], 'password' => $data['password']])) {
            $user = Auth::user();
            if (!$user->isAdmin()) {
                Auth::logout();
                return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Permission denied']);
            } else {
                return redirect()->intended('admin');
            }
        } else {
            return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Incorrect password']);
        }
    }
}
