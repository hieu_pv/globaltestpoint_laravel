<?php

namespace App\Http\Controllers\Backend;

use App\Entities\ExamResult;
use App\Entities\User;
use App\Http\Controllers\Backend\AdminController;
use App\Repositories\ExamResultRepository;
use Illuminate\Http\Request;

class ExamResultController extends AdminController
{
    private $repository;
    public function __construct(ExamResultRepository $repository)
    {
        $this->repository = $repository;
        parent::__construct();
    }

    public function index()
    {
        $examResults = ExamResult::all();
        if (!$examResults->isEmpty()) {
            foreach ($examResults as $examResult) {
                $examResult['data'] = json_decode($examResult['data'], true);
                $userInstance       = User::find($examResult['user_id']);
                $examResult['user'] = $userInstance;

                $examResult['examDate'] = $examResult['data']['examDate'];

                $examResult['timeTaken'] = $examResult['data']['timeTaken'] . ":000";
                $examResult['timeTaken'] = gmdate("H:i:s", (int) $examResult['timeTaken']);
            }
        }

        $data = [
            'title'       => 'Globaltestpoint - Exam Results',
            'tableTitle'  => 'Exam Results',
            'examResults' => $examResults,
        ];
        return view('admin.exam_result.list', $data);
    }

    public function destroy($id, Request $request)
    {
        $exam_result = ExamResult::find($id);
        if ($exam_result) {
            $exam_result->delete();
        }
        return redirect()->route('admin.exam-result.index')->with(['flash_level' => 'success', 'flash_message' => 'Complete Delete Exam Result']);
    }
}
