<?php

namespace App\Http\Controllers\Backend;

use App\Entities\SchoolType;
use App\Http\Controllers\Backend\AdminController;
use App\Http\Requests\SchoolTypeRequest;
use App\Repositories\SchoolTypeRepository;
use Illuminate\Http\Request;

class SchoolTypesController extends AdminController {
	protected $repository;

	public function __construct(SchoolTypeRepository $repository) {
		$this->repository = $repository;
		parent::__construct();
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
		$schooltypes = $this->repository->getAll();
		$data        = [
			'title'       => 'Globaltestpoint - List School Type',
			'tableTitle'  => 'List School Type',
			'schooltypes' => $schooltypes,
		];
		return view('admin.school_type.list', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$data = [
			'title'       => 'Globaltestpoint - Add School Type',
			'tableTitle'  => 'Add School Type',
			'buttonTitle' => 'Add',
		];
		return view('admin.school_type.add', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\SchoolTypeRequest  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(SchoolTypeRequest $request) {
		$data = $request->all();
		if (!isset($data['active'])) {
			$data['active'] = 0;
		}
		$school_type  = $this->repository->create($data);
		return redirect('admin/school_type')->with(['flash_level' => 'success', 'flash_message' => 'Add school type success']);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$school_type = SchoolType::findOrFail($id);
		$data        = [
			'title'       => 'Globaltestpoint - Edit School Type',
			'tableTitle'  => 'Edit School Type',
			'buttonTitle' => 'Update',
			'school_type' => $school_type,
		];
		return view('admin.school_type.edit', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\SchoolTypeRequest  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(SchoolTypeRequest $request, $id) {
		$data        = $request->all();
		$school_type = SchoolType::find($id);
		if (!isset($data['active'])) {
			$data['active'] = 0;
		}
		$school_type  = $this->repository->update($data, $id);
		return redirect('admin/school_type')->with(['flash_level' => 'success', 'flash_message' => 'Update school type success']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$school_type = SchoolType::findOrFail($id);
		$this->repository->delete($id);
		return redirect('admin/school_type')->with(['flash_level' => 'success', 'flash_message' => 'Delete school type success']);
	}
}
