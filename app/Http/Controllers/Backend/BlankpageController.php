<?php

namespace App\Http\Controllers\Backend;

use App\Entities\Category;
use App\Entities\Link;
use App\Entities\Post;
use App\Http\Controllers\Backend\AdminController;
use App\Http\Requests\BlankpageRequest;
use File;
use Illuminate\Http\Request;

class BlankpageController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $link = Link::all();
        $link = $link->map(function ($item) {
            $item->post_title = $item->post->title;
            if (!$this->getNameCate($item->parent_cate_id)) {
                $item->parent_name = "---";
            } else {
                $item->parent_name = $this->getNameCate($item->parent_cate_id);
            }
            if (empty($item->position)) {
                $item->position = "---";
            } elseif ($item->position == 'post') {
                $item->position = 'Post';
            } elseif ($item->position == 'homepage') {
                $item->position = "Home Page";
            }
            if ($item->post->active == 1) {
                $item->active = 1;
            } else {
                $item->active = 0;
            }
            return $item;
        });
        return view('admin.blankpage.index', ['listblankpage' => $link]);
    }

    /**
     * [getNameCate return name of a category]
     * @return [string] [name of a category]
     */
    public function getNameCate($id)
    {
        $cate = Category::find($id);
        if (empty($cate)) {
            return false;
        } else {
            return $cate['name'];
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorys = Category::where('type', 'blank-page')->get();
        return view('admin.blankpage.add', ['categorys' => $categorys]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlankpageRequest $request)
    {
        $link = new Link;
        if ($request->hasFile('inp-photograp')) {
            $file      = $request->file('inp-photograp');
            $filename  = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $namephoto = 'blankpage_' . (md5($filename)) . uniqid() . '.' . $extension;
            $file->move(public_path() . '/assets/frontend/images/link', $namephoto);
            $link->image = '/assets/frontend/images/link/' . $namephoto;
        }
        if ($request->hasFile('inp-banner')) {
            try {
                $filebanner        = $request->file('inp-banner');
                $filename          = $filebanner->getClientOriginalName();
                $extension         = $filebanner->getClientOriginalExtension();
                $namephoto         = 'blankpage_banner_' . (md5($filename)) . uniqid() . '.' . $extension;
                $destination       = public_path() . '/assets/frontend/images/link';
                $uploadSuccess     = $filebanner->move($destination, $namephoto);
                $link->image_cover = '/assets/frontend/images/link/' . $namephoto;
            } catch (Exception $e) {
                throw new Exception($e, 1);
            }
        }
        $post['title'] = $request->get('name-blankpage');
        $post_slug     = str_slug($post['title']);
        $count_slug    = Post::where('slug', $post_slug)->count();
        if ($count_slug > 0) {
            $post['slug'] = str_slug($post['title'] . '-' . ((int) $count_slug + 1));
        } else {
            $post['slug'] = str_slug($post['title']);
        }
        $post['content'] = $request->get('inp-introduction');
        $post['author']  = 1;
        $post['type']    = 'blankpage';

        if ($request->get('active_blank') == 'on') {
            $link->active   = 1;
            $post['active'] = 1;
        } else {
            $link->active   = 0;
            $post['active'] = 0;
        }
        $post['order'] = $request->get('order');
        if ($post_id = Post::create($post)) {
            if (!empty($request->get('parent_id'))) {
                $post_id->categories()->attach($request->get('parent_id'));
            }
            $link->post_id = $post_id->id;
        } else {
            return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Error ! Add UnComplete !']);
        }
        $link->page     = $request->get('page');
        $link->position = $request->get('position');
        if (empty($request->get('parent_id'))) {
            $position = 0;
        } else {
            $position = $request->get('parent_id');
        }
        $link->parent_cate_id = $position;
        // $arr_left             = "";
        // $arr_right            = "";
        // $arr_sub              = "";
        // if ($request->get('inp-left') != null && count($request->get('inp-left')) > 0) {
        //     foreach ($request->get('inp-left') as $keyleft => $left) {
        //         $arr_left .= ';' . $left;
        //     }
        // }
        // if ($request->get('inp-right') != null && count($request->get('inp-right')) > 0) {
        //     foreach ($request->get('inp-right') as $keyright => $right) {
        //         $arr_right .= ';' . $right;
        //     }
        // }
        // if ($request->get('inp-sub') != null && count($request->get('inp-sub')) > 0) {
        //     foreach ($request->get('inp-sub') as $keysub => $sub) {
        //         $arr_sub .= ';' . $sub;
        //     }
        // }

        // $link->leftlink  = $arr_left;
        // $link->rightlink = $arr_right;
        // $link->sublink   = $arr_sub;
        $link->leftlink  = $request->get('inp-leftlink');
        $link->rightlink = $request->get('inp-rightlink');
        $link->sublink   = $request->get('inp-sublink');
        if ($link->save()) {
            return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Success ! Added Complete']);
        } else {
            return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Error ! Added UnComplete']);
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BlankpageRequest $request, $id)
    {
        $link = Link::find($id);
        if ($request->hasFile('inp-photograp')) {
            if (!empty($link['image'])) {
                File::delete(url($link['image']));
            }
            $file      = $request->file('inp-photograp');
            $filename  = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $namephoto = 'blankpage_' . (md5($filename)) . uniqid() . '.' . $extension;
            $file->move(public_path() . '/assets/frontend/images/link', $namephoto);
            $link->image = '/assets/frontend/images/link/' . $namephoto;
        }
        if ($request->hasFile('inp-banner')) {
            if (!empty($link['image_cover'])) {
                File::delete(url($link['image_cover']));
            }
            $filebanner = $request->file('inp-banner');
            $filename   = $filebanner->getClientOriginalName();
            $extension  = $filebanner->getClientOriginalExtension();
            $namephoto  = 'blankpage_banner_' . (md5($filename)) . uniqid() . '.' . $extension;
            $filebanner->move(public_path() . '/assets/frontend/images/link', $namephoto);
            $link->image_cover = '/assets/frontend/images/link/' . $namephoto;
        }
        $post          = Post::find($link->post_id);
        $post->title   = $request->get('name-blankpage');
        $post->content = $request->get('inp-introduction');
        $post->author  = 1;
        if ($request->get('active_blank') == 'on') {
            $link->active = 1;
            $post->active = 1;
        } else {
            $link->active = 0;
            $post->active = 0;
        }
        if ($request->get('position') != 'homepage') {
            if (empty($request->get('parent_id'))) {
                return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Error ! Parent Category is invalid !']);
            }
            $post->categories()->sync([$request->get('parent_id')]);
        }
        $post->order = $request->get('order');
        $post->save();
        if ($request->get('remove-cover') == 'on') {
            $link->image_cover = '';
        }
        if ($request->get('remove-photograp') == 'on') {
            $link->image = '';
        }
        $link->page           = $request->get('page');
        $link->position       = $request->get('position');
        $link->parent_cate_id = $request->get('parent_id');
        $arr_left             = "";
        $arr_right            = "";
        $arr_sub              = "";
        $link->leftlink       = $request->get('inp-leftlink');
        $link->rightlink      = $request->get('inp-rightlink');
        $link->sublink        = $request->get('inp-sublink');
        // if(!empty($request->get('inp-left')))
        // {
        //     foreach ($request->get('inp-left') as $keyleft => $left) {
        //         $arr_left .= ';'.$left;
        //     }
        //     $link->leftlink = $arr_left;
        // }
        // if(!empty($request->get('inp-right')))
        // {
        //     foreach ($request->get('inp-right') as $keyright => $right) {
        //         $arr_right .= ';'.$right;
        //     }
        //     $link->rightlink = $arr_right;
        // }
        // if(!empty($request->get('inp-sub')))
        // {
        //     foreach ($request->get('inp-sub') as $keysub => $sub) {
        //         $arr_sub .= ';'.$sub;
        //     }
        //     $link->sublink = $arr_sub;
        // }
        if ($request->get('active_blank') == 'on') {
            $link->active = 1;
        } else {
            $link->active = 0;
        }
        if ($link->save()) {
            return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Success ! update Complete']);
        } else {
            return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Error ! Update Uncomplete']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showYouwantdone($slug)
    {
        $post = Post::where('slug', $slug)->where('type', 'blankpage')->get();
        $post = $post->map(function ($item) {
            foreach ($item->link as $key => $value) {
                $arr_leftlink      = explode(';', $value->leftlink);
                $item->leftlink    = $arr_leftlink;
                $arr_rightlink     = explode(';', $value->rightlink);
                $item->rightlink   = $arr_rightlink;
                $arr_sublink       = explode(';', $value->sublink);
                $item->sublink     = $arr_sublink;
                $item->image       = $value->image;
                $item->image_cover = $value->image_cover;
            }
            return $item;
        });
        return view('wantdone.blankpage.index', ['post' => $post]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blank            = Link::find($id);
        $blank->nameblank = $blank->post->title;
        $blank->content   = $blank->post->content;
        if (empty($blank)) {
            return view('admin.blankpage.index', ['result' => false]);
        } else {
            $blank->arr_leftlink  = explode(';', $blank->leftlink);
            $blank->arr_rightlink = explode(';', $blank->rightlink);
            $blank->arr_sublink   = explode(';', $blank->sublink);
            $categories           = Category::where('type', 'blank-page')->get();
            // echo '<pre>';
            // print_r($blank->toArray());
            // echo '</pre>';
            // die;
            return view('admin.blankpage.edit', ['blank' => $blank, 'categories' => $categories]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $link = Link::find($request->get('id_delete_blank'));
        if (empty($link)) {
            return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Error ! Delete UnComplete']);
        } else {
            $id = $link['post_id'];
            if ($link->delete()) {
                $post = Post::find($id);
                if ($post->delete()) {
                    return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Success ! Complete Deleted']);
                } else {
                    return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Error ! Delete UnComplete']);
                }
            } else {
                return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Error ! Delete UnComplete']);
            }
        }
    }
}
