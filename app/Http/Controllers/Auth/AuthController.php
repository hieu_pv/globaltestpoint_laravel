<?php

namespace App\Http\Controllers\Auth;

use App\Entities\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Contracts\Factory as Socialite;
use Validator;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
     */

    use ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo          = '/testyourself';
    protected $redirectAfterLogout = '/login';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(Socialite $socialite)
    {
        $this->socialite = $socialite;
        $this->middleware('guest', ['except' => ['logout', 'getLogout']]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'     => 'required|max:255',
            'email'    => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name'     => $data['name'],
            'email'    => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function getLoginUser()
    {
        return view('user.login');
    }

    public function postLoginUser(LoginRequest $request)
    {

        // $userChoose use for Exam
        if (Session::has('backlink')) {
            $backlink = Session::get('backlink');
            Session::forget('backlink');
        } else {
            $backlink = '';
        }
        $data       = $request->all();
        $userChoose = Session::get('userChoose');
        $doExam     = Session::get('doExam');
        $examResult = Session::get('examResult');
        $fullUrl    = Session::get('fullUrl');
        $data       = $request->all();

        $before_get_user = User::where('email', $data['email'])->first();
        if (empty($before_get_user['online_hash'])) {
            if (Auth::attempt(['email' => $data['email'], 'password' => $data['password']])) {
                $user = Auth::user();
                $user->saveIpLogin();
                if (!$user->isAdmin()) {
                    if ($userChoose == 'review') {
                        return redirect('testyourself/exam-result')->with(['flash_level' => 'success', 'flash_message' => 'Logged in successfully']);
                    } else if ($userChoose == 'payment') {
                        return redirect('testyourself/payment/exam-' . $doExam['id'])->with(['flash_level' => 'success', 'flash_message' => 'Logged in successfully']);
                    } else if (!empty($backlink)) {
                        return redirect()->route('testyourself.document.indexshow')->with(['flash_level' => 'success', 'flash_message' => 'Logged in successfully']);
                    } else {
                        if ($fullUrl) {
                            Session::forget('fullUrl');
                            return redirect($fullUrl)->with(['flash_level' => 'success', 'flash_message' => 'Logged in successfully']);
                        } else {
                            return redirect()->route('testyourself.index')->with(['flash_level' => 'success', 'flash_message' => 'Logged in successfully']);
                        }
                    }
                } else {
                    return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Please check your details']);
                }
            } else {
                return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Email does not exist or incorrect password ! Please register or check your details again']);
            }
        } else {
            $this->logout();
            return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'This account is logged in elsewhere !']);
        }

    }

    public function logout()
    {
        Session::forget('userChoose');
        Session::forget('doExam');
        Session::forget('examResult');
        $user = Auth::user();
        if (!empty($user)) {
            $user->removeIpLogin();
        }
        Auth::logout();
        return redirect('login');
    }

    public function forgot_password()
    {
        return view('auth.passwords.forgot');
    }

    public function getSocialAuth($provider = null)
    {
        if (!config("services.$provider")) {
            abort('404');
        }
        return $this->socialite->with($provider)->redirect();
    }

    private function saveOrUpdateUser($infoUser, $provider, $user_id = null)
    {

        // get Firstname, Lastname & Middle name
        $nameArr       = explode(" ", $infoUser->name);
        $nameLength    = count($nameArr);
        $firstName     = $nameArr[0];
        $lastName      = $nameArr[$nameLength - 1];
        $middleNameArr = array();
        $middleName    = "";
        if ($nameLength > 2) {
            for ($i = 0; $i < $nameLength; $i++) {
                if ($i > 0 && $i < $nameLength - 1) {
                    $middleNameArr[] = $nameArr[$i];
                }
            }
            if (!empty($middleNameArr)) {
                $middleName = implode(" ", $middleNameArr);
            }
        }

        $gender = isset($infoUser->user['gender']) && $infoUser->user['gender'] == 'male' ? 1 : 0;
        // $verify = isset($infoUser->user['verified']) && $infoUser->user['verified'] == true ? 1 : 0;

        if ($user_id != null) {
            $user = User::find($user_id);
        } else {
            $user = new User;
        }
        $user->account_type = $provider;
        $user->social_id    = $infoUser->getId() ? $infoUser->getId() : '';
        $user->first_name   = $firstName;
        if ($middleName != '') {
            $user->middle_name = $middleName;
        }
        $user->last_name      = $lastName;
        $user->email          = $infoUser->getEmail() ? $infoUser->getEmail() : '';
        $user->image          = $infoUser->getAvatar() ? $infoUser->getAvatar() : '';
        $user->gender         = $gender;
        $user->active         = 1;
        $user->email_verified = 1;
        $user->save();

        Auth::login($user);
    }

    public function getSocialAuthCallback($provider = null, Request $request)
    {
        if (!$request->input('code')) {
            return redirect('login');
        }
        if ($infoUser = $this->socialite->with($provider)->user()) {
            // echo '<pre>';
            // print_r($infoUser);
            // echo '</pre>';
            // die;
            $checkEmailUser = User::where('email', $infoUser->getEmail())->first();
            if ($checkEmailUser == null) {
                $checkUser = User::where('account_type', $provider)->where('social_id', $infoUser->getId())->first();
                if ($checkUser == null) {
                    $this->saveOrUpdateUser($infoUser, $provider);
                    return redirect()->route('testyourself.index');
                } else {
                    $this->saveOrUpdateUser($infoUser, $provider, $checkUser['id']);
                    return redirect()->route('testyourself.index');
                }
            } else {
                $this->saveOrUpdateUser($infoUser, $provider, $checkEmailUser['id']);
                return redirect()->route('testyourself.index');
            }
        } else {
            return 'something went wrong';
        }
    }
}
