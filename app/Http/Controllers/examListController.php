<?php

namespace App\Http\Controllers;

use App\Entities\Category;
use App\Entities\Exam;
use App\Entities\Option;
use App\Entities\Paid;
use App\Repositories\ExamRepository;
use App\Transformers\ExamTransformer;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class examListController extends Controller
{
    protected $repository;

    public function __construct(ExamRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Show all resource of exam
     * @return Collection
     */
    public function index(Request $request)
    {
        $data    = $request->only('keyword', 'page');
        $keyword = $data['keyword'];
        $page    = $data['page'];
        if ($page == null) {
            $page = 1;
        } else {
            $page = (int) $page;
        }
        if ($keyword == null) {
            $exams = Exam::where('active', 'yes')->paginate(8);
        } else {
            $search = '%' . $keyword . '%';
            $exams  = Exam::where('active', 'yes')->where('name', 'LIKE', $search)->paginate(8);
        }
        if (!$exams->isEmpty()) {
            $exams['item'] = $exams->map(function ($item) {
                if (!empty(Auth::user())) {
                    $user_id = Auth::user()->id;
                    $paid    = Paid::where('product_id', $item->id)->where('type', 'exam')->where('user_id', $user_id)->first();
                } else {
                    $paid = null;
                }
                if ($paid != null) {
                    $item->paid = 1;
                } else {
                    $item->paid = 0;
                }
                return $item;
            });
        }
        return view('testyourself.exam.list_download', compact('exams'), [
            'exams' => $exams->appends($request->except('page')),
        ]);
    }

    public function examList($slug)
    {
        $category = Category::where('slug', $slug)->first();
        if ($category != null) {
            $exams = $this->repository->getExamByCategory($category['id']);
            $exams = $exams->map(function ($item) {
                $user = Auth::user();
                if (!empty($user)) {
                    $paid = Paid::where('product_id', $item->id)->where('user_id', $user->id)->first();
                    if (!empty($paid)) {
                        $item->paided = 1;
                    } else {
                        $item->paided = 0;
                    }
                } else {
                    $item->paided = 0;
                }
                return $item;
            });
        } else {
            $exams = null;
        }
        // ============ garung - 20161214 ===================
        $option = Option::select('introduction_variousexam')->where('page', 0)->first();
        if (!empty($option)) {
            $introduction_variousexam = $option->introduction_variousexam;
        } else {
            $introduction_variousexam = '';
        }
        //===================================================
        return view('testyourself.exam.exam_list', compact('category', 'exams', 'introduction_variousexam'));
    }

    public function examStart($slug)
    {
        $exam               = Exam::where('slug', $slug)->first();
        $introduction_exams = Category::select()->where('id', $exam->category_id)->first();
        return view('testyourself.exam.exam_start', compact('exam', 'introduction_exams'));
    }

    public function getExam($slug, Request $request)
    {
        $exam    = $this->repository->getExam($slug);
        $buyExam = 0;
        $user    = Auth::user();
        if ($user == null) {
            $userLogin = 0;
        } else {
            $buyExam   = $this->repository->checkBuyExam($user['id'], $exam['id']);
            $userLogin = $user;
            if ($user->active == 0) {
                $buyExam = 0;
            }
            if ($user->isTester()) {
                $buyExam = 1;
            }
        }
        if (!$exam) {
            throw new Exception("Don't exists this exam", 1);
        } else {
            $questions = $this->repository->getQuestions($exam['id']);
            $data      = [
                'exam'      => $exam,
                'questions' => $questions,
                'user'      => $user,
                'userLogin' => $userLogin,
                'buyExam'   => $buyExam,
            ];
            return view('testyourself.exam.exam', $data);
        }
    }

    public function postExam(Request $request)
    {
        $data       = $request->all();
        $userChoose = $data['userChoose'];
        $exam       = $data['exam'];
        $result     = $this->repository->getResultExam($exam);
        if ($result != null) {
            Session::put('examResult', $result);
            $user    = Auth::user();
            $buyExam = $this->repository->checkBuyExam($user['id'], $exam['id']);
            if ($user != null) {
                $this->repository->saveExamToDB($user, $result);
                if ($buyExam != 0 || $exam['price'] == 0) {
                    $this->repository->sendMail($user, $result);
                }
            }
        }
        if ($userChoose != null) {
            Session::put('userChoose', $userChoose);
        }
        $returnView = url('testyourself/exam-result');
        $response   = new Response(['data' => $returnView], '200');
        $response->header('Content-Type', 'application/json');
        return $response;
    }

    public function examResult(Request $request)
    {
        $examResult = Session::get('examResult');
        // echo '<pre>';
        // print_r($examResult);
        // echo '</pre>';
        // die;
        if (!$examResult) {
            $buyExam = null;
            $alphas  = [];
            $data    = [
                'examResult' => $examResult,
                'buyExam'    => $buyExam,
                'alphas'     => $alphas,
            ];
            return view('testyourself.exam.exam_result', $data);
        } else {
            $user    = Auth::user();
            $buyExam = null;
            if ($user != null) {
                $buyExam = $this->repository->checkBuyExam($user['id'], $examResult['exam']['id']);
            }
            $alphas                  = range('A', 'Z');
            $examResult['timeTaken'] = $examResult['timeTaken'] . ":000";
            $examResult['timeTaken'] = gmdate("H:i:s", (int) $examResult['timeTaken']);

            $data = [
                'examResult' => $examResult,
                'buyExam'    => $buyExam,
                'alphas'     => $alphas,
            ];

            // get Answer Correct
            $questions = $this->repository->getQuestions($examResult['exam']['id']);
            if (!empty($questions)) {
                $data['questions'] = $this->repository->getAnswerCorrect($questions);
            }

            // get Your choice
            if (isset($examResult['exam']['questions']) && count($examResult['exam']['questions']['data']) > 0) {
                $questions         = $examResult['exam']['questions']['data'];
                $data['questions'] = $this->repository->getYourChoice($questions, $data['questions'], $alphas);
            }

            // Pagination
            $perPage = 5;
            if (!$request->has('page')) {
                $page = 1;
            } else {
                $page = (int) $request['page'];
            }
            if (!$buyExam && $examResult['exam']['price'] != 0 && $user != null && $user['active'] == 0) {
                // if (!$buyExam && $examResult['exam']['price'] != 0 && $user != null && $user['active'] == 0) {
                $maxPage = 1;
            } else {
                $maxPage = (int) ceil(count($data['questions']) / $perPage);
            }
            if ($page > $maxPage) {
                $page = $maxPage;
            }
            $data['page']      = $page;
            $data['maxPage']   = $maxPage;
            $data['questions'] = $data['questions']->chunk($perPage);
            $data['questions'] = $data['questions'][$page - 1];
            return view('testyourself.exam.exam_result', $data);
        }
    }

    public function saveExam(Request $request)
    {
        $data = $request->all();
        if ($data == null) {
            throw new Exception("save Exam fail", 1);
        }

        $exam       = $data['exam'];
        $userChoose = $data['userChoose'];
        if ($exam != null) {
            Session::put('doExam', $data['exam']);
        }
        if ($userChoose != null) {
            Session::put('userChoose', $data['userChoose']);
        }
        return $this->responseData($data);
    }

    public function checkExam(Request $request)
    {
        $doExam = Session::get('doExam');
        if ($doExam != null) {
            return $this->responseData($doExam, new ExamTransformer);
        } else {
            return $this->error("Do not exam");
        }
    }
}
