<?php

namespace App\Http\Controllers;

use App\Entities\Category;
use App\Entities\Link;
use App\Entities\Post;
use App\Http\Controllers\Frontend\FrontendController;
use App\Http\Requests;
use App\Http\Requests\BlankpageRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class BlankpageController extends FrontendController
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug) {
        $post = Post::with('link')->where('slug', $slug)->where('type', 'blankpage')->get();
        $post = $post->map(function ($item) {
            $item->link = $item->link->filter(function($subitem){
                $subitem->page = 2;
                $subitem->active = 1;
                return $subitem;
            });
            foreach ($item->link as $key => $value) {
                if(!empty($value->leftlink)){
                    // if($value->leftlink != ';'){
                    //     $arr_leftlink      = explode(';', $value->leftlink);
                    //     $item->leftlink    = $arr_leftlink;
                    // }
                    $item->leftlink    = $value->leftlink;
                }
                if(!empty($value->rightlink)){
                    // if($value->rightlink != ';'){
                    //     $arr_rightlink     = explode(';', $value->rightlink);
                    //     $item->rightlink   = $arr_rightlink;
                    // }
                    $item->rightlink    = $value->rightlink;
                }
                if(!empty($value->sublink)){
                    // if($value->sublink != ';'){
                    //     $arr_sublink       = explode(';', $value->sublink);
                    //     $item->sublink     = $arr_sublink;
                    // }
                    $item->sublink    = $value->sublink;
                }
                if(!empty($value->image)){
                    $item->image       = $value->image;
                }
                if(!empty($value->image_cover)){
                    $item->image_cover = $value->image_cover;
                }
            }
            return $item;
        });
        return view('testyourself.blankpage.index', ['post' => $post]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showYouwantdone($slug)
    {
        $post = Post::with('link')->where('slug', $slug)->where('type', 'blankpage')->get();
        $post = $post->map(function ($item) {
            $item->link = $item->link->filter(function($subitem){
                $subitem->page = 3;
                $subitem->active = 1;
                return $subitem;
            });
            foreach ($item->link as $key => $value) {
                if(!empty($value->leftlink)){
                    if($value->leftlink != ';'){
                        $arr_leftlink      = explode(';', $value->leftlink);
                        $item->leftlink    = $arr_leftlink;
                    }
                }
                if(!empty($value->rightlink)){
                    if($value->rightlink != ';'){
                        $arr_rightlink     = explode(';', $value->rightlink);
                        $item->rightlink   = $arr_rightlink;
                    }
                }
                if(!empty($value->sublink)){
                    if($value->sublink != ';'){
                        $arr_sublink       = explode(';', $value->sublink);
                        $item->sublink     = $arr_sublink;
                    }
                }
                if(!empty($value->image)){
                    $item->image       = $value->image;
                }
                if(!empty($value->image_cover)){
                    $item->image_cover = $value->image_cover;
                }
            }
            return $item;
        });
        return view('wantdone.blankpage.index', ['post'=>$post]);
    }
}
