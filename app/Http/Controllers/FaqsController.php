<?php

namespace App\Http\Controllers;

use App\Entities\Faq;
use App\Http\Requests;
use App\Repositories\FaqRepository;
use Illuminate\Http\Request;

class FaqsController extends Controller
{
    protected $repository;
    public function __construct(FaqRepository $repository){
        $this->repository = $repository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faqs = FAQ::all();
        $faqs = $faqs->map(function($item){
            $item->answer = str_limit($item->answer, 100);
            return $item;
        });
        $title = 'FAQ - Globaltestpoint';
        $tableTitle = 'List All - FAQ';
        // dd($faqs);
        return view('admin.faq.list', compact('faqs', 'title', 'tableTitle'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'FAQ - Globaltestpoint';
        $tableTitle = 'Create a new Faq - FAQ';
        return view('admin.faq.add', compact('title', 'tableTitle'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $title = 'FAQ - Globaltestpoint';
        $tableTitle = 'List All - FAQ';
        //===============================
        $faq = $request->except('_token');
        $create_faq = $this->repository->create($faq);
        return redirect()->route('admin.faq.index')->with(['flash_level' => 'success', 'flash_message' => 'success !! Create a FAQ successful.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $faqs = FAQ::all();
        return view('testyourself.faq.index', compact('faqs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = 'update FAQ - Globaltestpoint';
        $tableTitle = 'Update - FAQ';
        $faq = FAQ::where('id', $id)->first();
        return view('admin.faq.edit', compact('faq', 'title', 'tableTitle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $title = 'update FAQ - Globaltestpoint';
        $tableTitle = 'Update - FAQ';
        //===============================
        // $faq = FAQ::where('id', $id)->first();
        $faq['question'] = $request->__get('question');
        $faq['answer'] = $request->__get('answer');

        $update_faq = $this->repository->update($faq, $id);
        return redirect()->route('admin.faq.index')->with(['flash_level' => 'success', 'flash_message' => 'Success !! update a FAQ successful.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete_faq = $this->repository->delete($id);
        return redirect()->route('admin.faq.index')->with(['flash_level' => 'success', 'flash_message' => 'Success !! delete a FAQ successful.']);
    }
}
