<?php

namespace App\Http\Controllers;

use App\Entities\Category;
use App\Entities\Document;
use App\Entities\Option;
use App\Entities\Paid;
use App\Http\Controllers\PaymentsController as PaymentController;
use App\Repositories\PaymentRepository;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class DocumentsController extends Controller
{
    private $totalprice = 0;
    protected $payment;
    protected $paystackrepository;
    public function __construct(PaymentController $payment, PaymentRepository $PayStackRepository)
    {
        $this->payment            = $payment;
        $this->paystackrepository = $PayStackRepository;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $slug = null)
    {
        $category = Category::where('slug', $slug)->get()->first();
        $data     = $request->only('keyword', 'page', 'choosedocument');
        $keyword  = $data['keyword'];
        $page     = $data['page'];
        if (!empty($data['choosedocument'])) {
            $listchoose = [];
            $tmp_choose = $data['choosedocument'];
            $total      = 0;
            $qty_choose = count($tmp_choose);
            if ($qty_choose <= 1) {
                $qty_choose = 1;
            }
            foreach ($tmp_choose as $keydoc => $doc) {
                $getdoc       = Document::find($doc);
                $listchoose[] = $getdoc;
                $total += $getdoc['price'];
            }
            Session::put('tmp_choose', $tmp_choose);
            Session::put('totalprice', $total);
            Session::put('qty_choose', $qty_choose);
            return view('testyourself.document.choose_list_document', ['listchoose' => $listchoose]);
        }
        if ($request->get('multipayment')) {
            $user = Auth::user();
            if (!empty($user)) {
                $totalprice = 0;
                if (Session::has('totalprice')) {
                    $totalprice = Session::get('totalprice');
                    Session::forget('totalprice');
                }
                if (Session::has('qty_choose')) {
                    $qty_choose = Session::get('qty_choose');
                    Session::forget('qty_choose');
                } else {
                    $qty_choose = 1;
                }
                $backlink = $request->get('backlink');
                Session::put('backlink', $backlink);
                $checkmulti = 1;
                Session::put('checkmulti', $checkmulti);
                $infor            = '';
                $paystackresponse = [
                    'status' => true,
                    'data'   => (object) [
                        'first_name' => (!empty($user->first_name) ? $user->first_name : "NoName"),
                        'last_name'  => $user->last_name,
                        'email'      => $user->email,
                        'phone'      => $user->mobile,
                    ],
                ];
                $paystackresponse = (object) $paystackresponse;
                if (!empty($user->paystack_id)) {
                    $paystackresponse = $this->paystackrepository->retrieveCustomer($user->paystack_id);
                } else {
                    $params_cus = [
                        'first_name' => (!empty($user->first_name) ? $user->first_name : "NoName"),
                        'last_name'  => $user->last_name,
                        'email'      => $user->email,
                        'phone'      => $user->mobile,
                    ];
                    $this->paystackrepository->createCustomer($params_cus);
                }

                if (!empty($user->stripe_id)) {
                    $infor = $this->payment->getCustomer($user->stripe_id);
                }
                return view('testyourself.payment.index', ['page' => 'document', 'customer_info' => $infor, 'backlink' => $backlink, 'checkmulti' => $checkmulti, 'totalprice' => $totalprice, 'paystackresponse' => $paystackresponse, 'type' => 'document', 'qty_choose' => $qty_choose]);
            } else {
                return redirect()->route('getLoginUser')->with(['flash_level' => 'danger', 'flash_message' => "You need Login/Register to begin payment !"]);
            }
        }
        if ($page == null) {
            $page = 1;
        } else {
            $page = (int) $page;
        }
        if (Session::has('fullUrl')) {
            Session::forget('fullUrl');
        }
        $query = Document::where('active', 1)->orderBy('order', 'desc');
        if ($category != null) {
            $query->where('cate_id', $category['id']);
        }
        if ($keyword != null) {
            $search = '%' . $keyword . '%';
            $query->where('name', 'LIKE', $search);
        }
        $alldocument = $query->paginate(4);
        if (!$alldocument->isEmpty()) {
            $alldocument['item'] = $alldocument->map(function ($item) {
                if (!empty(Auth::user())) {
                    $user_id = Auth::user()->id;
                    $paid    = Paid::where('product_id', $item->id)->where('type', 'document')->where('user_id', $user_id)->first();
                } else {
                    $paid = '';
                }
                if (!empty($paid)) {
                    $item->paid = 1;
                } else {
                    $item->paid = 0;
                }
                return $item;
            });
        }
        // ============ garung - 20161214 ===================
        $option = Option::select('introduction_book')->where('page', 0)->first();
        if (!empty($option)) {
            $introduction_book = $option->introduction_book;
        } else {
            $introduction_book = '';
        }
        //===================================================
        //================== Garung - 20170824 ==============
        if ((isset($_REQUEST['trxref'])) && (isset($_REQUEST['reference'])) && ($_REQUEST['trxref'] === $_REQUEST['reference'])) {
            $user   = Auth::user();
            $verify = $this->paystackrepository->verify($_REQUEST['reference']);
            // dd($_SERVER);
            if ($verify->status) {
                if (!empty($verify->data->metadata->doc_ids)) {
                    $doc_ids = $verify->data->metadata->doc_ids;
                    // dd($verify->data->metadata->doc_ids);
                    $totalprice          = $verify->data->metadata->totalprice;
                    $retrieveTransaction = $this->paystackrepository->retrieveCharge($verify->data->id);
                    foreach ($doc_ids as $keydoc => $doc) {
                        $getdoc = Document::find($doc);
                        $this->payment->savePaidToDatabase('document', $doc, $verify->data->id);
                    }
                    $this->payment->saveTransactionToDatabase($retrieveTransaction, 'Paystack');
                    $this->payment->sendMailPaymentDocument($user, $totalprice);
                    $url_redirect = $_SERVER['REDIRECT_URL'];
                    if (isset($_REQUEST['page'])) {
                        $url_redirect = $url_redirect . '?page=' . $_REQUEST['page'];
                    }
                    return redirect($url_redirect);
                }
            }
        }
        //===================================================
        return view('testyourself.document.index', [
            'request'           => $request->all(),
            'alldocument'       => $alldocument->appends($request->except('page')),
            'introduction_book' => $introduction_book,
        ]);
    }

    /**
     * the function use to redirect to current page when user paypal success Document
     */
    public function urlBeforePayment(Request $request)
    {
        if ($request->has('fullUrl')) {
            Session::put('fullUrl', $request->get('fullUrl'));
            return $this->responseData('save Session fullUrl');
        } else {
            return $this->error("fullUrl doesn't exist");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showDetail()
    {
        return view('testyourself.document.index', ['post' => $post]);
    }

    /**
     * [download access download file]
     * @param  [type] $url [string path to file download]
     * @return [type]      [description]
     */
    public function download($slug, $id)
    {
        $doc      = Document::find($id);
        $namefile = last(explode('/', $doc['document']));
        $file     = public_path($doc['document']);
        $headers  = [
            'Content-Type: application/pdf',
        ];

        return (Response()->download($file, $namefile, $headers));
    }
}
