<?php

namespace App\Http\Controllers;

use App\Entities\Country;
use App\Entities\Option;
use App\Entities\User;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\ProfileRequest;
use App\Http\Requests\RegisterRequest;
use Carbon\Carbon;
use File;
use Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{

    public function getSignUp()
    {
        $user = Auth::user();
        if ($user == null) {
            $listcountry = Country::where('active', '1')->orderBy('order', 'desc')->get();
            return view('user.sign_up')->with(['listcountry' => $listcountry]);
        } else {
            return redirect('/');
        }
    }

    public function postSignUp(RegisterRequest $request)
    {
        if ($request->get('udob')) {
            $input    = $request->udob;
            $date     = Carbon::parse($input);
            $date     = $date->format('Y-m-d');
            $arr_date = explode('-', $date);
            if (Carbon::createFromDate(intval($arr_date[0]), intval($arr_date[1]), intval($arr_date[2]))->age < 15) {
                return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Please enter at least 15 years old']);
            }
        }

        $register              = new User();
        $register->first_name  = $request->fname;
        $register->middle_name = '';
        $register->last_name   = '';
        $register->email       = $request->uemail;
        $register->password    = Hash::make($request->pass1);
        $register->mobile      = $request->utel;
        $register->user_type   = 'student';
        if ($request->get('udob')) {
            $register->birth = $date;
        }
        if ($request->has('ulocation')) {
            $register->country = $request->ulocation;
        }
        $register->active         = 0;
        $register->email_verified = md5($register->email) . uniqid();
        if ($request->hasFile('ufile')) {
            $file_name       = $request->file('ufile')->getClientOriginalName();
            $register->image = 'uploads/profile/' . $file_name;
            $request->file('ufile')->move('uploads/profile/', $file_name);
        } else {
            $register->image = 'uploads/default_avatar.png';
        }
        $register->save();
        $option_tys = Option::where('page', 0)->first();
        Mail::queue('mails.userRegister', ['user' => $register, 'option_tys' => $option_tys], function ($message) use ($register) {
            $message->from(getenv('MAIL_USERNAME'), 'Globaltestpoint');
            $message->to($register->email)->subject('Registration successful ! globaltestpoint');
            // $register['image'] = $message->embed(asset($register->image));
        });
        $examResult = $request->session()->get('examResult');
        if ($examResult == null) {
            return redirect('login')->with(['flash_level' => 'success', 'flash_message' => 'Account created successfully. Please check your email to activate your registration']);
        } else {
            if (Auth::attempt(['email' => $request['uemail'], 'password' => $request['pass1']])) {
                $user = Auth::user();
                return redirect('testyourself/exam-result');
            }
        }
    }
    public function emailVerified($email, $cc_token)
    {
        if (isset($email) && isset($cc_token)) {
            $user = User::where('email', $email)->where('email_verified', $cc_token)->first();
            if (isset($user)) {
                $user->active         = 1;
                $user->email_verified = 1;
                $user->save();
                return redirect('/login')->with(['flash_level' => 'success', 'flash_message' => 'Account activated ! Successfully !']);
            } else {
                return redirect('testyourself')->with(['flash_level' => 'danger', 'flash_message' => 'Link is expire !']);
            }
        }
    }

    public function UserGetProfile()
    {
        $genders    = [1 => 'Male', 0 => 'Female'];
        $locations  = Country::where('active', '1')->orderBy('order', 'desc')->get();
        $user_types = ['student', 'Tutor'];
        $user       = Auth::user();
        $input      = $user->birth;
        $date       = Carbon::parse($input);
        $date       = $date->format('d/m/Y');
        return view('user.profile', compact('user', 'date', 'genders', 'locations', 'user_types'));
    }

    public function UserEditGetProfile()
    {
        $genders    = [1 => 'Male', 0 => 'Female'];
        $locations  = Country::where('active', '1')->orderBy('order', 'desc')->get();
        $user_types = ['student', 'Tutor'];
        $user       = Auth::user();
        if ($user->birth == '0000-00-00') {
            $date = null;
        } else {
            $input = $user->birth;
            $date  = Carbon::parse($input);
            $date  = $date->format('d/m/Y');
        }
        return view('user.edit_profile', compact('user', 'date', 'genders', 'locations', 'user_types'));
    }

    public function UserEditPostProfile(ProfileRequest $request)
    {
        if ($request->get('udob')) {
            $regexDDMMYYYY = '/^(0[1-9]|1\d|2\d|3[01])\/(0[1-9]|1[0-2])\/(19|20)\d{2}$/';
            $input         = $request->udob;
            if (preg_match($regexDDMMYYYY, $input)) {
                $date = Carbon::createFromFormat('d/m/Y', $input);
            } else {
                $date = Carbon::parse($input);
            }
            $date = $date->format('Y-m-d');
        }
        $user             = Auth::user();
        $user->first_name = $request->fname;
        $user->mobile     = $request->utel;
        if ($request->get('udob')) {
            $user->birth = $date;
        }
        $user->gender  = $request->gender;
        $user->country = $request->ulocation;

        if ($request->hasFile('NewImages')) {
            $file_name = $request->file('NewImages')->getClientOriginalName();
            File::delete('uploads/profile/' . $user->image);
            $user->image = "uploads/profile/" . $file_name;
            $request->file('NewImages')->move('uploads/profile/', $file_name);
        }
        $user->save();
        return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'success !!  Update Profile']);
    }

    public function getChangePassword()
    {
        return view('user.change_password');
    }

    public function postChangePassword(ChangePasswordRequest $request)
    {
        $data    = $request->all();
        $user    = Auth::user();
        $user_id = $user->id;
        $users   = User::find($user_id);
        if (Hash::check($data['current_password'], $user->password)) {
            $users->password = Hash::make($request->password);
            // $sendemail       = Mailletter::first();
            // $contentmail     = "Comfirm change password. New password is: " . $request->password;
            // $this->sendemail($sendemail['email'], $user->email, "Reset Password ! globaltestpoint", $contentmail);
            $users->save();
            $option_tys = Option::where('page', 0)->first();
            Mail::queue('mails.changePassword', ['data' => $data, 'option_tys' => $option_tys], function ($message) use ($users) {
                $message->from(getenv('MAIL_USERNAME'), 'Globaltestpoint');
                $message->to($users->email)->subject('Reset password ! globaltestpoint');
            });
            return redirect()->back()->with(['flash_level' => 'success', 'flash_message' => 'Successful password Update']);
        } else {
            return redirect()->back()->with(['flash_level' => 'danger', 'flash_message' => 'Sorry !!  old password incorrectly']);
        }
    }

    public function getForgotPassword()
    {
        return view('user.forgot_password');
    }
}
