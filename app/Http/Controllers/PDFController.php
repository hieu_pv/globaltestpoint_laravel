<?php

namespace App\Http\Controllers;

use App\Entities\ExamResult;
use App\Entities\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class PDFController extends Controller
{

    private function transformData($examResult, $isJson = true)
    {
        if ($isJson == true) {
            $examResult['data']     = json_decode($examResult['data'], true);
            $userInstance           = User::find($examResult['user_id']);
            $examResult['user']     = $userInstance;
            $examResult['examDate'] = $examResult['data']['examDate'];

            $examResult['timeTaken'] = $examResult['data']['timeTaken'] . ":000";
            $examResult['timeTaken'] = gmdate("H:i:s", (int) $examResult['timeTaken']);
        } else {
            $examResult['timeTaken'] = $examResult['timeTaken'] . ":000";
            $examResult['timeTaken'] = gmdate("H:i:s", (int) $examResult['timeTaken']);
        }
        return $examResult;
    }

    public function printResult($id)
    {
        $examResult = ExamResult::find($id);
        if ($examResult != null) {
            $examResult = $this->transformData($examResult);
        }

        $typePDF = 'S';
        $view    = 'pdf.print_one_exam';
        $data    = [
            'title'      => 'Examination Result',
            'examResult' => $examResult,
        ];
        $filename  = 'print_one_exam.pdf';
        $watermark = 'Globaltestpoint Result Sheet';

        $header     = 'pdf.partials._header';
        $dataHeader = [
            'primary_content'   => 'Online Testing System',
            'secondary_content' => 'Website: www.globaltestpoint.com',
        ];

        $footer = 'pdf.partials._footer';
        return $this->printPDF($typePDF, $view, $data, $filename, $watermark, $header, $dataHeader, $footer);
    }

    public function printAllResult()
    {
        $examResults = ExamResult::all();
        if (!$examResults->isEmpty()) {
            $examResults = $examResults->map(function ($examResult) {
                $examResult = $this->transformData($examResult);
                return $examResult;
            });
        }

        $typePDF = 'S';
        $view    = 'pdf.print_all_exam';
        $data    = [
            'title'       => 'Examination Result',
            'examResults' => $examResults,
        ];
        $filename  = 'print_one_exam.pdf';
        $watermark = 'Globaltestpoint Result Sheet';

        $header     = 'pdf.partials._header';
        $dataHeader = [
            'primary_content'   => 'Online Testing System',
            'secondary_content' => 'Website: www.globaltestpoint.com',
        ];

        $footer = 'pdf.partials._footer';
        return $this->printPDF($typePDF, $view, $data, $filename, $watermark, $header, $dataHeader, $footer);
    }

    public function printExam()
    {
        $user       = Auth::user();
        $examResult = Session::get('examResult');
        if ($examResult != null) {
            $examResult = $this->transformData($examResult, false);
        }

        $typePDF = 'S';
        $view    = 'pdf.print_exam';
        $data    = [
            'title'      => 'Examination Result',
            'user'       => $user,
            'examResult' => $examResult,
        ];
        $filename  = 'exam_result.pdf';
        $watermark = 'Globaltestpoint Result Sheet';

        $header     = 'pdf.partials._header';
        $dataHeader = [
            'primary_content'   => 'Online Testing System',
            'secondary_content' => 'Website: www.globaltestpoint.com',
        ];

        $footer = 'pdf.partials._footer';
        // return view('pdf.print_exam', $data);
        return $this->printPDF($typePDF, $view, $data, $filename, $watermark, $header, $dataHeader, $footer);
    }

    private function printPDF($type, $view, $data = null, $filename = null, $watermark = null, $header = null, $dataHeader = null, $footer = null, $dataFooter = null)
    {
        if ($filename == null) {
            $filename = 'document.pdf';
        }

        if ($watermark == null) {
            $watermark = 'Globaltestpoint Result Sheet';
        }

        $mpdf = new \mPDF();
        $mpdf->SetWatermarkText($watermark);

        if ($header != null) {
            if (!empty($dataHeader)) {
                $mpdf->SetHTMLHeader(view($header, $dataHeader));
            } else {
                $mpdf->SetHTMLFooter(view($header));
            }
        }

        if ($footer != null) {
            if (!empty($dataFooter)) {
                $mpdf->SetHTMLFooter(view($footer, $dataFooter));
            } else {
                $mpdf->SetHTMLFooter(view($footer));
            }
        }

        if (!empty($data)) {
            $mpdf->WriteHTML(view($view, $data));
        } else {
            $mpdf->WriteHTML(view($view));
        }
        $mpdf->showWatermarkText = true;

        $type = strtoupper($type);
        switch ($type) {
            case 'D':
                return $data = $mpdf->Output($filename, 'D');
                break;
            default:
                $data = $mpdf->Output($filename, 'S');
                return response($data)
                    ->withHeaders([
                        "Content-Type"              => "application/pdf",
                        "Content-Transfer-Encoding" => "binary",
                        "Content-Length"            => strlen($data),
                    ]);
                break;
        }
    }

    public function resultPDF()
    {
        $user       = Auth::user();
        $examResult = Session::get('examResult');

        $typePDF = 'D';
        $view    = 'pdf.test-result';
        $data    = [
            'title'      => 'Examination Result',
            'user'       => $user,
            'examResult' => $examResult,
        ];
        $filename  = 'exam_result.pdf';
        $watermark = 'Globaltestpoint Result Sheet';

        $header     = 'pdf.partials._header';
        $dataHeader = [
            'primary_content'   => 'Online Testing System',
            'secondary_content' => 'Website: www.globaltestpoint.com',
        ];

        $footer = 'pdf.partials._footer';
        $this->printPDF($typePDF, $view, $data, $filename, $watermark, $header, $dataHeader, $footer);
    }
}
