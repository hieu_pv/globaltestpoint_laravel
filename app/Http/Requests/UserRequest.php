<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'role'       => 'required',
			'first_name' => 'required',
			'last_name'  => 'required',
			'txtsex'     => 'required',
			'txtaddress' => '',
		];
	}

	public function messages() {
		return [
			'first_name.required' => 'The first name field is required.',
			'last_name.required'  => 'The last name field is required.',
			'txtsex.required'     => 'The sex field is required.',
		];
	}
}
