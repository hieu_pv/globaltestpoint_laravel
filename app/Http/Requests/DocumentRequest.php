<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DocumentRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		if (Request::isMethod('post')) {
			return [
				'cate_id'  => 'required',
				'name'     => 'required|min:4|max:100',
				'price'    => 'required|numeric',
				'order'    => 'max:10|regex: /^[0-9][0-9]*$/',
			];
		} else {
			return [
				'cate_id' => 'required',
				'name'    => 'required|min:4|max:100',
				'price'   => 'required|numeric',
				'order'   => 'max:10|regex: /^[0-9][0-9]*$/',
			];
		}
	}

	public function messages() {
		return [
			'cate_id.required' => 'The category field is required',
			'order.regex'      => 'Order is unsigned',
		];
	}
}
