<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class QuestionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'question' => 'required',
            'answer_question' => 'required',
            'answer_question_description' => 'required',
        ];
    }

    public function messages() {
        return [
            'question.required' => 'The question field is required.',
            'answer_question' => 'School results are mandatory',
            'answer_question_description.required' => 'School test steps have',
        ];
    }
}
