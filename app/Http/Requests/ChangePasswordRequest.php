<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ChangePasswordRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'current_password' => 'required',
            'password'         => 'required',
            'retype_password'  => 'required|same:password',
        ];
    }

    public function messages()
    {
        return [
            'current_password.required' => 'Please enter your current password',
            'password.required'         => 'Please enter your new password',
            'retype_password.required'  => 'Please retype your new password',
        ];
    }
}
