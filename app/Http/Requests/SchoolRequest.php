<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SchoolRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'country_id'       => 'required',
			'state_id'         => 'required',
			'school_type_id'   => 'required',
			'school_degree_id' => 'required',
			'name'             => 'required|min:4|max:100',
			'rank'             => 'required|numeric|max:1000',
		];
	}

	public function messages() {
		return [
			'country_id.required'       => 'The country field is required',
			'state_id.required'         => 'The state field is required',
			'school_type_id.required'   => 'The school type field is required',
			'school_degree_id.required' => 'The school degree is required',
		];
	}
}
