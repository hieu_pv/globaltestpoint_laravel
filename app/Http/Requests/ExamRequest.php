<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ExamRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	public function rules() {
		return [
			'name'         => 'required',
			'category_id'  => 'required',
			'duration'     => 'required|numeric',
			'price'        => 'required|numeric',
			'pass_percent' => 'required|integer|max:100',
		];
	}

	public function messages() {
		return [
			'name.required'        => 'The name field is required.',
			'category_id.required' => 'The category field is required.',
			'duration.required'    => 'The duration field is required.',
		];
	}
}
