<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class IndustryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required',
            'order' => 'max:10|regex: /^[0-9][0-9]*$/',
        ];
    }

    public function messages()
    {
        return [
            'order.regex' => 'Order is unsigned',
        ];
    }
}
