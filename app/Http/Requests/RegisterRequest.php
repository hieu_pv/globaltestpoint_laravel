<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RegisterRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'fname'                => 'required|max:40',
			'uemail'               => 'required|unique:users,email,' . $this->users . '|email',
			'utel'                 => 'numeric',
			'pass1'                => 'required|min:6',
			'pass2'                => 'required|min:6|same:pass1',
			'g-recaptcha-response' => 'required',
		];
	}

	public function messages() {
		return [
			'fname.required'                => 'The first Name field is required.',
			'fname.max'                     => 'The first Name field must be at most 40 character.',
			'uemail.required'               => 'The email field is required.',
			'uemail.in'                     => 'The selected Email field is invalid.',
			'uemail.email'                  => 'The Email field format is invalid.',
			'utel.numeric'                  => 'Please enter your correct phone number.',
			'pass1.required'                => 'The password field is required.',
			'pass1.min'                     => 'The password field must be at least 6 character.',
			'pass2.min'                     => 'The retype password field must be at least 6 character.',
			'pass2.required'                => 'The retype password field is required.',
			'pass2.same'                    => 'Two Password Don\'t Match',
			'uemail.unique'                 => 'The email has already been registered.',
			'alpha_dash'                    => 'The :attribute may only contain letters, numbers, and dashes.',
			'g-recaptcha-response.required' => 'The Recaptcha is required.',
		];
	}
}
