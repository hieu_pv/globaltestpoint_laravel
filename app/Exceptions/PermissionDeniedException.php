<?php

namespace App\Exceptions;

class PermissionDeniedException extends \Exception
{
    public function __construct($entity = null)
    {
        $this->message = 'Permission Denied';
        $this->error_code = 1013;
    }
}
