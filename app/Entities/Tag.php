<?php

namespace App\Entities;

use App\Entities\Job;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'type',
        'order',
    ];

    public function jobs()
    {
        return $this->morphedByMany(Job::class, 'taggable');
    }
}
