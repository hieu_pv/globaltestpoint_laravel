<?php

namespace App\Entities;

use App\Entities\Job;
use Illuminate\Database\Eloquent\Model;

class Industry extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'type',
        'total_jobs',
        'order',
    ];

    public function jobs()
    {
        return $this->morphedByMany(Job::class, 'industriable');
    }
}
