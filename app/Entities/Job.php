<?php

namespace App\Entities;

use App\Entities\City;
use App\Entities\Country;
use App\Entities\Employer;
use App\Entities\Experience;
use App\Entities\Industry;
use App\Entities\JobTitle;
use App\Entities\Nationality;
use App\Entities\Qualification;
use App\Entities\State;
use App\Entities\Tag;
use App\Entities\User;
use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    public $fillable = [
        'title',
        'slug',
        'image',
        'address',
        'min_salary',
        'max_salary',
        'zipcode',
        'currency',
        'certificate',
        'gender',
        'experience_id',
        'employer_id',
        'city_id',
        'state_id',
        'country_id',
        'nationality_id',
        'qualification_id',
        'job_title_id',
        'category_id',
        'short_desc',
        'description',
        'desired_candidate_profile',
        'type',
        'location',
        'order',
    ];

    const STATE = [
        'JOB_ACTIVE'  => [
            'active'   => 1,
            'archive'  => 0,
            'draft'    => 0,
            'complete' => 0,
        ],
        'JOB_PENDING' => [
            'active' => 0,
        ],
    ];

    public function industries()
    {
        return $this->morphToMany(Industry::class, 'industriable');
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    public function employer()
    {
        return $this->belongsTo(User::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function state() {
        return $this->belongsTo(State::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function nationality()
    {
        return $this->belongsTo(Nationality::class);
    }

    public function jobTitle()
    {
        return $this->belongsTo(JobTitle::class);
    }

    public function experience()
    {
        return $this->belongsTo(Experience::class);
    }

    public function qualification()
    {
        return $this->belongsTo(Qualification::class);
    }
}
