<?php

namespace App\Entities;

use App\Entities\City;
use App\Entities\Partner;
use App\Entities\School;
use App\Entities\State;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Country extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'id',
        'name',
        'slug',
        'code',
        'phone_area_code',
        'is_default',
        'is_supported_phone_area',
        'order',
        'active',
    ];

    public function states()
    {
        return $this->hasMany(State::class);
    }

    public function schools()
    {
        return $this->hasManyThrough(School::class, State::class);
    }

    public function partners()
    {
        return $this->hasMany(Partner::class, 'country_id');
    }

    public function cities()
    {
        return $this->hasMany(City::class);
    }
}
