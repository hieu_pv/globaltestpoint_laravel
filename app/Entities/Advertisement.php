<?php

namespace App\Entities;

use App\Entities\Image;
use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'image',
        'url',
        'position',
        'order',
    ];

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }
}
