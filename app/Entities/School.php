<?php

namespace App\Entities;

use App\Entities\Country;
use App\Entities\State;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class School extends Model implements Transformable {
	use TransformableTrait;

	protected $fillable = [
		'id',
		'name',
		'slug',
		'image',
		'rank',
		'state_id',
		'school_type_id',
		'school_degree_id',
		'summary',
		'description',
		'order',
		'active',
	];

	public function belongsToManyThrough($related, $through, $firstKey, $secondKey, $pivotKey) {
		$model = new $related;
        $table = $model->getTable();
        $throughModel = new $through;
        $pivot = $throughModel->getTable();

        return $model
            ->join($pivot, $pivot . '.' . $pivotKey, '=', $table . '.' . $secondKey)
            ->select($table . '.*')
            ->where($pivot . '.' . $firstKey, '=', $this->id);
	}

	public function country() {
		return $this->belongsToManyThrough(Country::class, State::class, 'states.id', 'id', 'countries.id');
	}

	public function state() {
		return $this->belongsTo(State::class);
	}

	public function setNameAttribute($value) {
        $this->attributes['name'] = $value;
        $slug                     = str_slug($value);
        $i                        = 1;
        while (!$this->where('slug', $slug)->get()->isEmpty()) {
            $slug = str_slug($value) . '-' . $i++;
        }
        $this->attributes['slug'] = $slug;
    }

}
