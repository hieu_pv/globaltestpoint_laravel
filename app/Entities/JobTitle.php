<?php

namespace App\Entities;

use App\Entities\Job;
use Illuminate\Database\Eloquent\Model;

class JobTitle extends Model
{
    protected $fillable = [
        'name',
        'job_title',
        'job_description',
        'total_jobs',
        'order',
        'attributes',
    ];

    public function jobs()
    {
        return $this->hasMany(Job::class);
    }
}
