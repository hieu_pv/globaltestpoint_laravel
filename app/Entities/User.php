<?php

namespace App\Entities;

use App\Entities\Employer;
use App\Entities\Exam;
use App\Entities\Image;
use App\Entities\Paid;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Session;
use Laravel\Cashier\Billable;
use NF\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;
use NF\Roles\Models\Role;
use NF\Roles\Traits\HasRoleAndPermission;
use Prettus\Repository\Traits\TransformableTrait;

class User extends Authenticatable implements HasRoleAndPermissionContract, CanResetPassword
{
    use TransformableTrait;
    use HasRoleAndPermission;
    use Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'image',
        'phone_area_code',
        'mobile',
        'phone_number_verified',
        'optional_phone_area_code',
        'optional_phone_number',
        'birth',
        'sumary',
        'address',
        'gender',
        'city',
        'country',
        'active',
        'email_verified',
        'status',
        'remember_token',
        'created_at',
        'updated_at',
    ];

    protected $dates = ['trial_ends_at', 'subscription_ends_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function exan_user()
    {
        return $this->hasMany('App\Entities\User');
    }

    /**
     * [ads return infor User reference to Ads]
     * @return [type] [description]
     */
    public function ads()
    {
        return $this->hasMany(Ads::class);
    }

    public function exams()
    {
        return $this->belongsToMany(Exam::class);
    }

    public function paid_exams()
    {
        return $this->hasMany(Paid::class);
    }

    public function employers()
    {
        return $this->belongsToMany(Employer::class);
    }

    public function employer()
    {
        return $this->employers();
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    /**
     * [getIpAdress description]
     * @return [type] [description]
     */
    public function getIpAdress()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public function saveIpLogin()
    {
        $hash = bcrypt(auth()->user()->getKey() . microtime());

        // Session::put('online_hash', $hash);

        $this->online_hash = $hash;
        // $this->online_hash = 1;
        $this->save();
    }

    public function removeIpLogin()
    {
        if (Session::has('online_hash')) {
            Session::forget('online_hash');
        }
        $this->online_hash = '';
        $this->save();
    }
}
