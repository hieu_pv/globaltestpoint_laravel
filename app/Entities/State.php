<?php

namespace App\Entities;

use App\Entities\City;
use App\Entities\Country;
use App\Entities\Job;
use App\Entities\Partner;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class State extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'id',
        'name',
        'country_id',
        'total_jobs',
        'order',
        'active',
    ];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function partners()
    {
        return $this->hasMany(Partner::class);
    }

    public function jobs()
    {
        return $this->hasMany(Job::class);
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
        $slug                     = str_slug($value);
        $i                        = 1;
        while (!$this->where('slug', $slug)->get()->isEmpty()) {
            $slug = str_slug($value) . '-' . $i++;
        }
        $this->attributes['slug'] = $slug;
    }

    public function cities()
    {
        return $this->hasMany(City::class);
    }
}
