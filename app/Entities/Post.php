<?php

namespace App\Entities;

use App\Entities\Category;
use App\Entities\Link;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Post extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'posts';

    protected $fillable = [
        'id',
        'title',
        'content',
        'image',
        'excerpt',
        'author',
        'order',
        'slug',
        'type',
        'created_at',
        'updated_at',
        'video',
        'flash',
        'page',
        'leftlink',
        'rightlink',
        'sublink',
        'parent_id'];

    public function ads()
    {
        return $this->hasMany(Ads::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function link()
    {
        return $this->hasMany(Link::class);
    }

    public function post()
    {
        return $this->title;
    }
}
