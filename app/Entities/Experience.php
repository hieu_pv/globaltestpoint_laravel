<?php

namespace App\Entities;

use App\Entities\Job;
use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    protected $fillable = [
        'label',
        'total_jobs',
        'order',
    ];

    public function jobs() {
    	return $this->hasMany(Job::class);
    }
}
