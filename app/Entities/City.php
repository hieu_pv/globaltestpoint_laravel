<?php

namespace App\Entities;

use App\Entities\Job;
use App\Entities\State;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'state_id',
        'total_jobs',
        'order',
    ];

    public function jobs()
    {
        return $this->hasMany(Job::class);
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }
}
