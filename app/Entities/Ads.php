<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Ads extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'ads';

    protected $fillable = [
        'id',
        'link',
        'position',
        'page',
        'type',
        'active',
        'author',
    ];
    
    /**
     * [user return infor reference to user]
     * @return [type] [description]
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
