<?php

namespace App\Entities;

use App\Entities\Category;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Document extends Model implements Transformable {
	use TransformableTrait;

	protected $fillable = ['id', 'name', 'slug', 'image', 'price', 'download', 'document', 'cate_id', 'order', 'active','introduction_book'];

	public function category() {
		return $this->belongsTo(Category::class, 'cate_id');
	}

	public function setNameAttribute($value) {
        $this->attributes['name'] = $value;
        $slug                     = str_slug($value);
        $i                        = 1;
        while (!$this->where('slug', $slug)->get()->isEmpty()) {
            $slug = str_slug($value) . '-' . $i++;
        }
        $this->attributes['slug'] = $slug;
    }
	
}
