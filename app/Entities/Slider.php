<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Slider extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'sliders';

    protected $fillable = ['id', 'image', 'page', 'sort', 'active', 'created_at', 'updated_at'];
}
