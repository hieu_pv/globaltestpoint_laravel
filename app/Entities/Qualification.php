<?php

namespace App\Entities;

use App\Entities\Job;
use Illuminate\Database\Eloquent\Model;

class Qualification extends Model
{
    protected $fillable = [
        'name',
        'order',
    ];

    public function jobs()
    {
        return $this->hasMany(Job::class);
    }
}
