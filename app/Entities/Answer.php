<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $table = 'answers';

    protected $fillable = ['answer', 'is_correct', 'order', 'question_id', 'description'];

    public function question()
    {
        return $this->belongsTo(Question::class);
    }
}
