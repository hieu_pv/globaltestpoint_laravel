<?php

namespace App\Entities;

use App\Entities\User;
use Illuminate\Database\Eloquent\Model;

class Employer extends Model
{

    protected $fillable = [
        'company_name',
        'slug',
        'fax',
        'website',
        'company_address',
        'company_about',
        'number_of_outlets',
        'registration_number',
        'contact_name',
        'contact_position',
        'note',
    ];

    public function jobs()
    {
        return $this->hasMany(Job::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
