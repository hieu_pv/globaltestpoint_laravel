<?php

namespace App\Entities;

use App\Entities\Category;
use App\Entities\Question;
use App\Entities\User;
use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    protected $table = 'exams';

    protected $fillable = [
        'name',
        'duration',
        'price',
        'pass_percent',
        'grade',
        'active',
        'category_id',
        'created_at'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = $value;
        $slug                     = str_slug($value);
        $i                        = 1;
        while (!$this->where('slug', $slug)->get()->isEmpty()) {
            $slug = str_slug($value) . '-' . $i++;
        }
        $this->attributes['slug'] = $slug;
    }

    public function users()
    {
        return $this->belongsToMany(User::class)->withPivot('id', 'data');
    }
}
