<?php

namespace App\Entities;

use App\Entities\Document;
use App\Entities\Exam;
use App\Entities\Post;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Category extends Model implements Transformable {
	use TransformableTrait;

	protected $table = 'categories';

	protected $fillable = ['id', 'name', 'parent_id', 'page_id', 'active', 'order', 'slug', 'type', 'created_at', 'updated_at', 'introduction_exam', 'position', 'link'];

	public function exams() {
		return $this->hasMany(Exam::class);
	}

	public function posts() {
		return $this->belongsToMany(Post::class);
	}

	public function category_post() {
		return $this->hasMany(Category_post::class);
	}

	public function docs() {
		return $this->hasMany(Document::class);
	}

	public function link() {
		return $this->hasMany(Link::class);
	}

	public function getName() {
		return $this->name;
	}
	
	public function setNameAttribute($value) {
		$this->attributes['name'] = $value;
		$slug                     = str_slug($value);
		$i                        = 1;
		while (!$this->where('slug', $slug)->get()->isEmpty()) {
			$slug = str_slug($value) . '-' . $i++;
		}
		$this->attributes['slug'] = $slug;
	}
}
