<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class UserStatus extends Model
{
    protected $table    = 'users_status';
    protected $fillable = ['status', 'description', 'active'];
}
