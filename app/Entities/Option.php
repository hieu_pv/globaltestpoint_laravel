<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    public $table = 'options';

    protected $fillable = [
        'logo',
        'page',
        'title',
        'email',
        'phone',
        'address',
        'fax',
        'facebook',
        'twitter',
        'image_login',
        'image_register',
        'image_login_register',
        'termpolicy',
        'introduction_book',
        'introduction_variousexam',
        'introduction_exams',
        'youtube',
        'instagram',
        'linkedin',
    ];
}
