<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $table = 'subscriptions';

    protected $fillable = ['id', 'name', 'stripe_id', 'user_id', 'stripe_plan', 'quantity', 'trial_ends_at', 'ends_at', 'updated_at', 'created_at'];
    
}
