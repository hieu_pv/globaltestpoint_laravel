<?php

namespace App\Entities;

use App\Entities\Answer;
use App\Entities\Exam;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'questions';

    protected $fillable = ['id', 'question', 'type', 'exam_id', 'order', 'solution'];

    protected $hidden = ['is_correct'];

    protected $with = ['answers'];

    public function exam()
    {
        return $this->belongsTo(Exam::class);
    }
    public function answers()
    {
        return $this->hasMany(Answer::class);
    }
}
