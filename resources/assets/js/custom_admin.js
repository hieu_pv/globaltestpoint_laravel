$(document).ready(function() {
    if ($('#list_user').length) {
        $("#list_user").DataTable();
    }
    if ($('#list_blankpage').length) {
        $("#list_blankpage").DataTable();
    }
    if ($('#list_slider_tys').length) {
        $("#list_slider_tys").DataTable();
    }
    if ($('#list_slider_ywd').length) {
        $("#list_slider_ywd").DataTable();
    }

    if ($('div.alert').length) {
        $('div.alert').delay(20000).slideUp();
    }

    if ($('.data-table').length) {
        $(".data-table").DataTable();
    }
    //===================== video flash ===========
    if ($("#list_video").length) {
        $("#list_video").DataTable();
    }

    if ($("#list_flash").length) {
        $("#list_flash").DataTable();
    }

    if ($(".inp_flash").length) {
        $(".inp_flash").fileinput();
    }


    //============================ for Blankpage ======================================================
    //=============for blankpage==================
    if ($('#inp-photograp').length) {
        $("#inp-photograp").fileinput();
    }
    if ($('#inp-banner').length) {
        $("#inp-banner").fileinput();
    }
    //============ for slider ==============
    if ($('#image_slider').length) {
        $("#image_slider").fileinput();
    }

    if ($('.add_new_questions').length && $('.list_option_add').length) {
        $(".add_new_questions").click(function() {
            $('.list_option_add').stop().fadeToggle();
        });
    }
    CKEDITOR.replaceClass = 'rich_editor';

    if ($('.name_questions').length) {
        $('.name_questions').mouseover();
    }

    // $('.hidden_Conllap_class').css({
    //     'display':'none',
    // });

    // $('.add_questions_list_table .panel-group .panel-heading').click(function(){
    //     $(this).parent().find('.hidden_Conllap_class').show();
    // });
    if ($('.button_list_question_manage').length && $('.content_question_list_add').length) {
        $('.button_list_question_manage').click(function() {
            $(this).parent().find('.content_question_list_add').slideToggle();
        });
    }

    // $('.add_question_button').click(function(){
    //     var cout_option = $('.list_leng_option_in_ques').length;
    //     alert(cout_option);
    // });
    if ($('#list_category').length) {
        $("#list_category").DataTable();
    }
    //================== Blank page ==================
    if ($('#add_leftlink_blankpage').length) {
        $('#add_leftlink_blankpage').click(function() {
            $html = '<input class="form-control" name="inp-left[]" placeholder="Link" type="url" value=""><br />';
            $("#wraplinkleft").append($html);
        });
    }
    if ($('#add_rightlink_blankpage').length) {
        $('#add_rightlink_blankpage').click(function() {
            $html2 = '<input class="form-control" name="inp-right[]" placeholder="Link" type="url" value=""><br />';
            $("#wraplinkright").append($html2);
        });
    }
    if ($('#add_sublink_blankpage').length) {
        $('#add_sublink_blankpage').click(function() {
            $html3 = '<input class="form-control" name="inp-sub[]" placeholder="Link" type="url" value=""><br />';
            $("#wraplinksub").append($html3);
        });
    }
    //======= edit ====
    if ($('#edit_leftlink_blankpage').length) {
        $('#edit_leftlink_blankpage').click(function() {
            $html4 = '<input class="form-control" name="inp-left[]" placeholder="Link" type="url" value=""><br />';
            $("#wraplinkleft_edit").append($html4);
        });
    }
    if ($('#edit_rightlink_blankpage').length) {
        $('#edit_rightlink_blankpage').click(function() {
            $html5 = '<input class="form-control" name="inp-right[]" placeholder="Link" type="url" value=""><br />';
            $("#wraplinkright_edit").append($html5);
        });
    }
    if ($('#edit_sublink_blankpage').length) {
        $('#edit_sublink_blankpage').click(function() {
            $html6 = '<input class="form-control" name="inp-sub[]" placeholder="Link" type="url" value=""><br />';
            $("#wraplinksub_edit").append($html6);
        });
    }
    //=====================================================
    //===================== option ========================
    if ($('.option_logo').length) {
        $(".option_logo").fileinput();
    }
    //=====================================================

    /**
     * Country
     */
    if ($('#list_country').length) {
        $("#list_country").DataTable();
    }

    /**
     * File Input
     */
    if ($('.file_input').length) {
        $(".file_input").fileinput({
            'showUpload': false
        });
    }
    if ($('.filestyle').length) {
        $(".filestyle").filestyle({ buttonText: "Attach File", buttonName: "btn-success" });
    }

    if ($('#toggleBulkQuestion').length) {
        $('#toggleBulkQuestion').click(function() {
            $('#bulkQuestion').slideToggle();
        });
    }
});

function xacnhanxoa(msg) {
    if (window.confirm(msg)) {
        return true;
    } else {
        return false;
    }
}

function load_csv(id, ext) {
    ext = ext.toLowerCase();
    sample = ext.lastIndexOf("csv");
    if (sample == -1) {
        alert("File type allowed: .csv");
        $("#" + id).filestyle('clear');
        return false;
    }
    return true;
}
