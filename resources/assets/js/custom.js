jQuery(document).ready(function() {
    jQuery("#home .slider_home ul li:gt(0)").hide();
    setInterval(function() {
        jQuery("#home .slider_home ul li:first").fadeOut().next('li').fadeIn().end().appendTo('.slider_home ul');
    }, 5000);

    $('.li_menu_test').hover(function() {
        $(this).has('ul').children('ul').stop().fadeIn();
    }, function() {
        $(this).has('ul').children('ul').stop().fadeOut();
    });
    $('.sub_li_menu_test').hover(function() {
        if ($(this).has('ul li')) {
            $(this).has('ul li').children('ul').stop().fadeIn();
        }
    }, function() {
        $(this).has('ul').children('ul').stop().fadeOut();
    });
    //============================
    if ($('.tlt').length > 0) {
        $('.tlt').textillate({ in: { effect: 'flipInY' },
            out: { effect: 'foldUnfold', sync: true },
            loop: true
        });

        $('.jumbotron1 h5')
            .fitText(0.5)
            .textillate({ loop: true });
    }

    if ($('.active-toggle-review').length > 0) {
        $('.active-toggle-review').click(function() {
            $('.toggle-review').toggleClass('hide');
        });
    }

    if ($('.text-popup').length > 0) {
        $('.text-popup').hide();
        $('#pass1').click(function() {
            $('.password-popup').slideDown('slow');
        });
        $('#telephone').click(function() {
            $('.telephone-popup').slideDown('slow');
        });
    }

    // add caret to main menu
    var navbar = $('.header_test_yourself .container_site .navbar');
    navbar.find('.navbar-nav > .dropdown').each(function() {
        $(this).find('.dropdown-menu li').each(function() {
            if ($(this).find('.dropdown-menu li').length) {
                $(this).find('> a').append('<span class="caret"></span>');
            }
        });
    });
    var navbar_mobile = $('.header_test_yourself .container_site .navbar .navbar-nav > .dropdown');
    if (navbar_mobile.find('.dropdown-menu li').length) {
        navbar_mobile.find('> a').click(function() {
            // navbar_mobile.removeClass('open-parent');
            $(this).parent().toggleClass('open-parent');
        });

        navbar_mobile.find('.dropdown-menu .sub_li_menu_test > a').click(function() {
            // navbar_mobile.removeClass('open-parent');
            $(this).parent().toggleClass('open-parent');
        });



        // $(this).find('.navbar-nav > .dropdown .dropdown-menu li > a').click(function(){
        //     navbar.find('.navbar-nav > .dropdown .dropdown-menu li').removeClass('open');
        //     navbar.find('.navbar-nav > .dropdown .dropdown-menu li').toggleClass('open');
        // });
    }

    // Slider
    if ($('.owl-carousel').length > 0) {
        $('.owl-carousel').owlCarousel({
            singleItem: true,
            transitionStyle: 'fadeUp',
            slideSpeed: 200,
            paginationSpeed: 500,
            autoPlay: 5000,
            stopOnHover: true,
            rewindNav: true,
            rewindSpeed: 600,
            autoHeight: true,
            navigation: true,
            navigationText: false
        });
    }

    // add select2 function to country dropdown in signup page
    // $('#testyourself select#ulocation').select2();
    if ($('select.select2').length > 0) {
        $('select.select2').select2();
    }

    // Save current page when payment Document
    if ($('.saveCurrentPage').length > 0) {
        $('.saveCurrentPage').click(function() {
            var url = BASE_URL + 'testyourself/document/urlBeforePayment';
            var redirect = $(this).attr('data-href');
            var fullUrl = $(this).attr('data-url');
            var data = {
                fullUrl: fullUrl
            };
            $.ajax({
                method: 'POST',
                url: url,
                data: data,
                success: function(response) {
                    window.location.href = redirect;
                },
                error: function(err) {
                    console.log(err);
                }
            });
        });
    }

    //Disable cut copy paste
    $('.exam-result-container').bind('cut copy paste', function(e) {
        e.preventDefault();
    });

    //Disable mouse right click
    $(".exam-result-container").on("contextmenu", function(e) {
        return false;
    });

    /**
     * Truncate text for exam start page
     */
    var introduction_exam_element = $('.exam-start-page .introduction-exam');
    var show_more_wrapper = $('.exam-start-page .show-more-button-wrapper');
    var show_more_button = $('.exam-start-page #show-more-button');
    if (introduction_exam_element.height() > 180) {
        show_more_wrapper.removeClass('hide');
        introduction_exam_element.css({
            'height': '180px',
            'overflow': 'hidden'
        });
    }

    show_more_button.on('click', function() {
        show_more_wrapper.addClass('hide');
        introduction_exam_element.css({
            'height': '100%'
        });
    });

    // $(window).resize(function() {
    //     var width = $(window).width();
    //     console.log('width ', width);
    // });
    // var width = $(window).width();
    // console.log('width ', width);
});


$(function() {
    var timeout = 0;

    if ($('html').hasClass('csstransitions'))
        timeout = 750;

    // On button hover or touch reveal the login form
    $('.login-btn').mouseenter(function() {
        $('.left-door, .right-door, .login-btn').addClass('login-animate');

        setTimeout(function() {
            $('.login-btn .name').fadeOut(650, function() {
                $('.login-btn, .login-btn .square1, .login-btn .square2').fadeIn(750);
                $('#home').fadeIn(1500).removeClass('hide');
                $('#layer').addClass('hide');
            });
        }, 750);
    });

    // After 3 second will auto open form
    setTimeout(function() {
        $('.left-door, .right-door, .login-btn').addClass('login-animate');

        setTimeout(function() {
            $('.login-btn .name').fadeOut(650, function() {
                $('.login-btn, .login-btn .square1, .login-btn .square2').fadeIn(750);
                $('#home').fadeIn(1500).removeClass('hide');
                $('#layer').addClass('hide');
            });
        }, 750);
    }, 3000);
});

// ============== file input for Signup page ==========
if ($('#ufile').length) {
    jQuery("#ufile").fileinput();
}
//========================================\
var yt_id = "";

function youtube_parser(url) {
    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
    var match = url.match(regExp);
    if (match && match[7].length == 11) {
        var b = match[7];
        console.log(b);
        yt_id = b;
    } else {
        yt_id = "";
    }
}

var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player;

function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
        videoId: yt_id,
        events: {
            // 'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    });
}

// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
    event.target.playVideo();
}

// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player should play for six seconds and then stop.
var done = false;

function onPlayerStateChange(event) {
    if (event.data == YT.PlayerState.PLAYING && !done) {
        $('.play-video').hide();
    }
}

function stopVideo() {
    player.stopVideo();
}