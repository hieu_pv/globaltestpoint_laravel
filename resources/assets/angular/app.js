var $ = window.$ = require('jquery');
var jQuery = window.jQuery = require('jquery');
var moment = window.moment = require('moment');
require('angular');
require('lodash');
require('angular-ckeditor');
require('angular-cookies');
require('angular-sanitize');
require('angular-resource');
require('ngStorage');
require('angular-bootstrap');
require('angular-dialog-service');
require('ng-notify');
require('./components/');
require('./api/');
require('./common/services/');
require('./common/filters/');
require('./common/directives/');
require('./common/exception');
(function(app) {

    app.config(['dialogsProvider', function(dialogsProvider) {
        dialogsProvider.useBackdrop('static');
        dialogsProvider.useEscClose(false);
        dialogsProvider.useCopy(false);
        dialogsProvider.setSize('sm');
    }]);

    app.run(['$rootScope', '$http', '$window', 'API',
        function($rootScope, $http, $window, API) {
            API.user.profile()
                .then(function(user) {
                    console.log(user);
                    $rootScope.user = user;
                    $rootScope.isLoggedIn = true;
                    $rootScope.$broadcast('userLoaded', user);
                })
                .catch(function(error) {
                    console.log(error);
                });
            $rootScope.asset = function(path) {
                if (path === '' || path === undefined || path === null) {
                    return false;
                }
                if (path.charAt(0) == '/') {
                    return BASE_URL + path.substr(1);
                } else {
                    return BASE_URL + path;
                }
            };
        }
    ]);

})(angular.module('app', [
    'app.api',
    'ngCookies',
    'ngSanitize',
    'ngStorage',
    'ngResource',
    'ui.bootstrap',
    'dialogs.main',
    'ngNotify',
    // 'app.template',
    'app.components',
    'app.common.directives',
    'app.api',
    'app.common.services',
    'app.common.exceptionHandler',
    'app.common.filters'
]));
