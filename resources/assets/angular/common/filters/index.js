require('./hasItem');
require('./range');
require('./timeFormat');
require('./trustAsHtml');
require('./toString');

(function(app) {

})(angular.module('app.common.filters', [
    'app.common.filters.hasItem',
    'app.common.filters.range',
    'app.common.filters.timeFormat',
    'app.common.filters.trustAsHtml',
    'app.common.filters.toString',
]));
