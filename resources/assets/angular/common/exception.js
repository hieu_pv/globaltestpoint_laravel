(function(app) {
    app.config(['$logProvider', '$provide', function($logProvider, $provide) {
        $logProvider.debugEnabled(true);

        $provide.decorator('$exceptionHandler', [
            '$log',
            '$delegate',
            '$injector',
            function($log, $delegate, $injector) {
                return function(exception) {
                    $log.debug('Exception handler:', exception);
                    var $rootScope = $injector.get("$rootScope");
                    $rootScope.$broadcast('onError', exception);
                };
            }
        ]);
    }]);
    /*
     * Show Error message
     */
    app.run(errorHandler);

    errorHandler.$inject = ['$rootScope'];

    function errorHandler($rootScope) {
        $rootScope.$on('onError', function(e, err) {
            console.log(err);
            if (!$rootScope.hasError) {
                $rootScope.hasError = true;
            }
            if (err.message !== undefined) {
                if (err.code === 1000) {
                    var errors = $.parseJSON(err.message);
                    var message = '';
                    for (var key in errors) {
                        message += errors[key].join(' ') + ' ';
                    }
                } else {}
            }
        });
    }
})(angular.module('app.common.exceptionHandler', []));
