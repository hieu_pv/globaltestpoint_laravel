(function(app) {

    app.service('preloader', ['$rootScope', function($rootScope) {
        this.show = function(element_container) {
            if ($('#andy-preloader').length === 0) {
                var preloader = $('<div id="andy-loader"><div class="text">Please wait while the Questions are loaded</div></div>');
                if (!element_container || element_container === undefined || element_container === null) {
                    $('body').append(preloader);
                } else {
                    $(element_container).append(preloader);
                }
            }
            $rootScope.inProgress = true;
        };
        this.hide = function() {
            if ($('#andy-loader').length > 0) {
                $('#andy-loader').remove();
            }
            $rootScope.inProgress = false;
        };
    }]);

})(angular.module('app.common.services.preloader', []));