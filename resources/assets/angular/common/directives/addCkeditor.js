(function(app) {

    app.directive('addCkeditor', function() {
        return {
            restrict: 'AE',
            scope: {
                addCkeditor: '@'
            },
            link: function(scope, element, attrs) {
                scope.$watch('addCkeditor', function(addCkeditor) {
                    if (addCkeditor !== undefined) {
                        $(element).click(function() {
                            var hEd = CKEDITOR.instances[scope.addCkeditor];
                            if (hEd) {
                                CKEDITOR.remove(hEd);
                            }
                            hEd = CKEDITOR.replace(scope.addCkeditor);
                        });
                    }
                });
            }
        };
    });

})(angular.module('app.common.directives.addCkeditor', []));
