var Dropzone = require('dropzone');
require('ngFileUpload');

(function(app) {
    app.directive('dropzone', function() {
        return {
            strict: 'AE',
            scope: {
                onSuccess: '=',
                onError: '=',
                modelName: '=',
                errorMessage: '=',
                folderUpload: '@'
            },
            link: function(scope, element, attrs) {
                var config = {
                    url: '/file/upload',
                    paramName: "file",
                };

                var eventHandlers = {

                    'sending': function(file, xhr, formData) {
                        if (scope.folderUpload !== undefined) {
                            formData.append('folderUpload', scope.folderUpload);
                        }
                    },

                    'success': function(file, response) {
                        scope.modelName = response.data;
                        scope.errorMessage = undefined;
                        scope.$apply();
                        // scope.onSuccess(response, file);
                    },

                    'queuecomplete': function() {
                        this.removeAllFiles();
                    },

                    'error': function(file, err) {
                        scope.errorMessage = err.file[0];
                        scope.modelName = '';
                        this.removeAllFiles();
                        scope.$apply();
                    }

                };

                dropzone = new Dropzone(element[0], config);

                angular.forEach(eventHandlers, function(handler, event) {
                    dropzone.on(event, handler);
                });
            },
        };
    });
})(angular.module('app.common.directives.dropzone', ['ngFileUpload']));
