(function(app) {
	
	app.directive('scrollToElement', function() {
		return {
			restrict: 'AE',
			link: function(scope, element, attrs) {
				$('html,body').animate({scrollTop: $(element).offset().top},'fast');
			}
		};
	});

})(angular.module('app.common.directives.scrollToElement', []));