require('./scrollToElement');
require('./dropzone');
require('./dataToggle');
require('./addCkeditor');
require('./deleteCkeditor');
require('./select2');
(function(app) {

})(angular.module('app.common.directives', [
	'app.common.directives.scrollToElement',
	'app.common.directives.dropzone',
	'app.common.directives.dataToggle',
	'app.common.directives.addCkeditor',
	'app.common.directives.deleteCkeditor',
	'app.common.directives.select2',
]));
