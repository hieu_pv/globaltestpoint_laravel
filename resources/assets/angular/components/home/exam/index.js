var Answer = function(options) {
    options = options || {};
    this.answer_id = options.answer_id || '';
    this.is_correct = options.is_correct || 0;
};
var Question = function(options) {
    options = options || {};
    this.question_id = options.question_id || '';
    this.answers = options.answers || [];
};
(function(app) {

    app.controller('ExamCtrl', ExamCtrl);
    ExamCtrl.$inject = ['$rootScope', '$scope', '$window', '$http', '$timeout', 'dialogs', 'ngNotify', 'preloader'];

    function ExamCtrl($rootScope, $scope, $window, $http, $timeout, dialogs, ngNotify, preloader) {
        $scope.page = 1;
        $scope.perPage = 5;
        preloader.show('#areaCoundown');
        $scope.$watch('exam_id', function(exam_id) {
            if (exam_id !== undefined) {
                checkExam();
            }
        });

        function checkExam() {
            var url = 'testyourself/checkExam';
            url = $rootScope.asset(url);
            $http.get(url)
                .then(function(result) {
                    var exam = result.data.data;
                    if (exam.id === $scope.exam_id) {
                        if ($scope.buyExam === 0) {
                            $scope.exam = exam;
                            $scope.exam.selectButton = 'both';
                            if ($scope.userLogin === 0) {
                                $scope.exam.typeExam = 'noLogin';
                                $scope.exam.messageDialog = 'Well-done you! You have taken the right decision and you are doing great on the questions. Please register or login to continue.';
                            } else {
                                $scope.exam.typeExam = 'Login';
                                $scope.exam.messageDialog = 'Well-done ' + $scope.user.first_name + '! You have taken the right decision and you are doing great on the questions.';
                            }
                            $scope.exam.CURRENTCY_SYMBOL = CURRENTCY_SYMBOL;
                            showDialog('/dialogs/paypal-review.html', 'customDialogCtrl', $scope.exam);
                        } else {
                            $scope.exam = exam;
                            $scope.doExam = true;
                            configExam($scope.exam, $scope.doExam);
                            startExam($scope.exam.timeTaken);
                        }
                    } else {
                        getExam($scope.exam_id);
                    }
                })
                .catch(function(error) {
                    getExam($scope.exam_id);
                });
        }

        function checkShowModal() {
            $scope.exam.timeTaken = $scope.timeTaken;
            $scope.exam.selectButton = 'both';
            if ($scope.userLogin === 0) {
                $scope.exam.typeExam = 'noLogin';
                $scope.exam.messageDialog = 'Well-done you! You have taken the right decision and you are doing great on the questions. Please register or login to continue.';
            } else {
                if (parseInt($scope.user.active) === 0) {
                    $scope.exam.typeExam = 'noActive';
                } else {
                    $scope.exam.typeExam = 'Login';
                }
                $scope.exam.messageDialog = 'Well-done ' + $scope.user.first_name + '! You have taken the right decision and you are doing great on the questions.';
            }
            $scope.exam.CURRENTCY_SYMBOL = CURRENTCY_SYMBOL;
            showDialog('/dialogs/paypal-review.html', 'customDialogCtrl', $scope.exam);
            if ($scope.user !== undefined && $scope.user !== null) {
                if (parseInt($scope.user.email_verified) === 0) {
                    $window.alert('Please activate your account from your email to be able to continue');
                }
            }
        }

        function checkPaymentReview() {
            if ($scope.user !== undefined && $scope.user !== null) {
                if (parseInt($scope.user.email_verified) === 0) {
                    checkShowModal();
                } else {
                    if ($scope.buyExam === 0 && $scope.exam.price !== 0) {
                        checkShowModal();
                    }
                }
            }
            if ($scope.buyExam === 0 && $scope.exam.price !== 0) {
                checkShowModal();
            }
        }

        function getExam(exam_id) {
            $scope.doExam = false;
            var url = 'api/exam/' + exam_id;
            url = $rootScope.asset(url);
            $http.get(url)
                .then(function(result) {
                    $scope.exam = result.data.data;
                    console.log('andy exam ', $scope.exam);
                    if ($scope.exam.questions.data.length === 0) {
                        $window.alert('Oops! Sorry this exam does not any question.');
                        $window.location.href = BASE_URL + 'testyourself';
                    } else {
                        configExam($scope.exam, $scope.doExam);

                        var timeTaken = 0;
                        startExam(timeTaken);
                    }
                }, function(error) {
                    throw error;
                });
        }

        /**
         * reset all answers = false if doExam = false
         */

        function configExam(exam, doExam) {
            if (exam !== undefined && exam.questions !== undefined) {
                var sttQuestion = 1;
                exam.questions.data = _.map(exam.questions.data, function(question, key) {
                    question.sttQuestion = sttQuestion;
                    sttQuestion++;
                    if (doExam !== true) {
                        if (question.answers !== undefined && question.answers.data.length > 0) {
                            question.answers.data = _.map(question.answers.data, function(answer) {
                                answer.is_correct = 0;
                                return answer;
                            });
                        }
                    }
                    return question;
                });
                $scope.questionsDisplay = _.chunk(exam.questions.data, $scope.perPage);
                preloader.hide();
            }
        }

        function startExam(timeTaken) {
            $scope.$watch('exam.duration', function(duration) {
                if (duration !== undefined) {
                    $scope.timeTaken = timeTaken;
                    var timeOut = $timeout($scope.onTimeout, 1000);
                    duration = duration * 60;
                    duration = duration - parseInt(timeTaken);
                    var myCountdown2 = new Countdown({
                        time: duration,
                        width: 200,
                        height: 60,
                        rangeHi: "hour",
                        hideLine: true,
                        target: "areaCoundown",
                        onComplete: examTimeout,
                        numbers: {
                            font: "Trebuchet MS",
                            color: "#FFFFFF",
                            bkgd: "#365D8B",
                            fontSize: 200,
                            shadow: {
                                x: 0,
                                y: 3,
                                s: 4,
                                c: "#000000",
                                a: 0.4
                            }
                        },
                        labels: {
                            textScale: 0.8,
                            offset: 5
                        }
                    });
                }
            });
        }

        $scope.getTotalAttempted = function(question, answer) {
            $scope.$watch('questionsDisplay', function(questionsDisplay) {
                if (!_.isNil(questionsDisplay)) {
                    $scope.totalAnswerd = 0;
                    $scope.totalQuestion = 0;
                    if (!_.isNil(question) && !_.isNil(answer)) {
                        question.answers.data = _.map(question.answers.data, function(item) {
                            item.is_correct = 0;
                            return item;
                        });
                        answer.is_correct = 1;
                    }

                    if (questionsDisplay !== undefined && questionsDisplay.length > 0) {
                        _.forEach(questionsDisplay, function(page) {
                            _.forEach(page, function(question) {
                                $scope.totalQuestion++;
                                if (question.answers !== undefined && question.answers.data.length > 0) {
                                    var answer = _.find(question.answers.data, function(item) {
                                        return parseInt(item.is_correct) === 1;
                                    });
                                    if (!_.isNil(answer)) {
                                        $scope.totalAnswerd++;
                                    }
                                }
                            });
                        });
                        $scope.tot_atm = $scope.totalAnswerd + " / " + $scope.totalQuestion;
                    }
                }
            });
        };
        $scope.getTotalAttempted();

        function remainingTest(minutes) {
            ngNotify.set('The exam remaining ' + minutes + ' minutes', {
                position: 'top',
                type: 'warn',
                duration: 5000
            });
        }

        $scope.onTimeout = function() {
            $scope.timeTaken++;
            timeOut = $timeout($scope.onTimeout, 1000);
            var remaining = ($scope.exam.duration * 60) - $scope.timeTaken;
            if ($scope.exam.duration > 5) {
                if (remaining === 300 && $scope.remaining5Minutes !== true) {
                    remainingTest(5);
                    $scope.remaining5Minutes = true;
                }
                if (remaining === 120 && $scope.remaining5Minutes === true && $scope.remaining2Minutes !== true) {
                    remainingTest(2);
                    $scope.remaining2Minutes = true;
                }
            } else if ($scope.exam.duration < 5) {
                if (remaining === 120 && $scope.remaining2Minutes !== true) {
                    remainingTest(2);
                    $scope.remaining2Minutes = true;
                }
            }
        };

        $scope.stop = function() {
            $timeout.cancel(timeOut);
        };

        function examTimeout(result) {
            $scope.exam.timeTaken = $scope.exam.duration * 60;
            $scope.exam.selectButton = 'review';
            $scope.exam.typeExam = 'timeOut';
            $scope.exam.messageDialog = 'Your time is up and the Test has timed out, please submit and see your scores';
            $scope.exam.CURRENTCY_SYMBOL = CURRENTCY_SYMBOL;
            showDialog('/dialogs/paypal-review.html', 'customDialogCtrl', $scope.exam);
        }

        $scope.doneExam = function() {
            if ($window.confirm('Are you sure you want to submit your answers? Have you attempted all the questions and have reviewed all your responses?')) {
                $scope.exam.timeTaken = $scope.timeTaken;
                $rootScope.saveExamResult($scope.exam, 'review');
                $scope.$watch('examResultSaved', function(examResultSaved) {
                    if (examResultSaved !== undefined) {
                        $window.location.href = examResultSaved;
                    }
                });
            }
        };

        $scope.prevPage = function() {
            if ($scope.page > 1) {
                $scope.page--;
                $('html,body').animate({ scrollTop: $('.main-container').offset().top }, 'fast');
            }
        };

        $scope.nextPage = function() {
            if ($scope.user !== undefined && $scope.user !== null) {
                if (parseInt($scope.user.email_verified) === 0) {
                    checkPaymentReview();
                }
            }
            if ($scope.buyExam === 0 && $scope.exam.price !== 0) {
                checkPaymentReview();
            } else {
                if ($scope.page < $scope.questionsDisplay.length) {
                    $scope.page++;
                    $('html,body').animate({ scrollTop: $('.main-container').offset().top }, 'fast');
                }
            }
        };

        $rootScope.saveExamResult = function(exam, userChoose) {
            var data = {
                exam: exam,
                userChoose: userChoose
            };
            $http.post($window.location.href, data)
                .then(function(result) {
                    $rootScope.examResultSaved = result.data.data;
                }, function(error) {
                    throw error;
                });
        };

        function showDialog(template, controller, data) {
            if (data === undefined) {
                $scope.$watch('exam', function(exam) {
                    if (exam !== undefined) {
                        var dlg = dialogs.create(template, controller, exam, { size: 'lg' });
                    }
                });
            } else {
                var dlg = dialogs.create(template, controller, data, { size: 'lg' });
            }
        }
    }

    app.controller('customDialogCtrl', customDialogCtrl);
    customDialogCtrl.$inject = ['$scope', '$uibModalInstance', 'data', '$window', '$http', '$rootScope'];

    function customDialogCtrl($scope, $uibModalInstance, data, $window, $http, $rootScope) {
        $scope.exam = data;

        function saveExam(userChoose) {
            var data = {
                userChoose: userChoose,
                exam: $scope.exam
            };
            var url = 'testyourself/saveExam';
            url = $rootScope.asset(url);
            $http.post(url, data)
                .then(function(result) {
                    $scope.examSaved = true;
                })
                .catch(function(error) {
                    throw error;
                });
        }

        $scope.cancel = function() {
            $uibModalInstance.dismiss('Canceled');
        };

        $scope.redirectPayment = function() {
            $window.alert('You will be redirected to payment page');
            saveExam('payment');
            $scope.$watch('examSaved', function(examSaved) {
                if (examSaved !== undefined) {
                    var redirect = '/testyourself/payment/exam-' + $scope.exam.id;
                    redirect = $rootScope.asset(redirect);
                    $window.location.href = redirect;
                }
            });
        };

        $scope.redirectReview = function() {
            $rootScope.saveExamResult($scope.exam, 'review');
            $scope.$watch('examResultSaved', function(examResultSaved) {
                if (examResultSaved !== undefined) {
                    var url = 'checkLogin';
                    url = $rootScope.asset(url);
                    $http.post(url)
                        .then(function(result) {
                            $scope.userLogin = result.data.data;
                            if ($scope.userLogin !== null) {
                                $window.location.href = examResultSaved;
                            } else {
                                $window.alert('You must login or register to continue');
                                $window.location.href = $rootScope.asset('/login');
                            }
                        })
                        .catch(function(error) {
                            throw error;
                        });
                }
            });
        };
    }

})(angular.module('app.components.home.exam', []));