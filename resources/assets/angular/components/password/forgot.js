(function(app) {

    app.controller('ForgotPasswordAdminCtrl', ForgotPasswordAdminCtrl);
    ForgotPasswordAdminCtrl.$inject = ['$rootScope', '$scope', '$http'];

    function ForgotPasswordAdminCtrl($rootScope, $scope, $http) {
        var vm = this;

        vm.checkEmail = false;

        vm.forgotPassword = function() {
            vm.inProgress = true;
            var url = 'api/checkEmailAdmin';
            url = $rootScope.asset(url);
            data = {
                email: vm.email
            };
            $http.post(url, data)
                .then(function(response) {
                    vm.checkEmail = true;
                })
                .catch(function(error) {
                    vm.inProgress = false;
                    vm.checkEmail = false;
                    vm.errorMessage = error.data.message;
                    vm.errorClass = 'alert-danger';
                });
        };

        $scope.$watch('vm.checkEmail', function(checkEmail) {
            if (checkEmail === true) {
                var urlForgotPassword = 'password/email';
                urlForgotPassword = $rootScope.asset(urlForgotPassword);
                $http.post(urlForgotPassword, data)
                    .then(function(response) {
                        vm.checkEmail = false;
                        vm.inProgress = false;
                        vm.errorMessage = 'An email was sent to ' + data.email;
                        vm.errorClass = 'alert-success';
                        vm.email = '';
                        vm.forgotPasswordForm.$setPristine();
                    })
                    .catch(function(error) {
                        console.log(error);
                        throw error;
                    });

            }
        });
    }

})(angular.module('app.components.password.forgot', []));
