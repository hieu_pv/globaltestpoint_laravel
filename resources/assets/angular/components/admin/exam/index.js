var Answer = function(options) {
    options = options || {};
    this.answer = options.answer || '';
    this.is_correct = options.is_correct || false;
    this.order = options.order || '';
    this.description = options.description || '';
};
var Question = function(options) {
    options = options || {};
    this.question = options.question || '';
    this.image = options.image || '';
    this.description = options.description || '';
    this.answers = options.answers || [];

    // 1 is text, 2 is image
    this.type = options.type || 1;
};
(function(app) {
    app.controller('AdminExamCtrl', AdminExamCtrl);
    AdminExamCtrl.$inject = ['$rootScope', '$scope', '$window', '$http', '$q', '$cookieStore'];

    function AdminExamCtrl($rootScope, $scope, $window, $http, $q, $cookieStore) {
        var vm = this;
        vm.question = new Question();

        var flashClass = $cookieStore.get('flashClass');
        var flashMessage = $cookieStore.get('flashMessage');
        if (flashClass !== undefined) {
            vm.flashClass = flashClass;
            $cookieStore.remove('flashClass');
        }
        if (flashMessage !== undefined) {
            vm.flashMessage = flashMessage;
            $cookieStore.remove('flashMessage');
        }

        function getExam(exam_id) {
            var url = 'api/exam/' + exam_id;
            url = $rootScope.asset(url);
            $http.get(url)
                .then(function(response) {
                    vm.exam = response.data.data;
                    if (vm.exam !== undefined && vm.exam.questions !== undefined && vm.exam.questions.data.length > 0) {
                        vm.exam.questions.data = _.map(vm.exam.questions.data, function(question) {
                            question.type = parseInt(question.type);
                            return question;
                        });
                    }
                }, function(error) {
                    throw error;
                });
        }

        $scope.$watch('exam_id', function(exam_id) {
            if (exam_id !== undefined) {
                getExam(exam_id);
            }
        });

        vm.addMoreAnswer = function(question) {
            question.answers.push(new Answer());
        };

        vm.addQuestion = function(question) {
            vm.inProgress = true;
            if (question.type === 2 && question.image === '') {
                return false;
            }
            if (question.type === 2 && question.image !== '') {
                question.question = question.image;
            }
            $http.post($window.location.href, question)
                .then(function(response) {
                    vm.inProgress = false;
                    vm.addQuestionForm.$setPristine();
                    $cookieStore.put('flashClass', 'alert-success');
                    $cookieStore.put('flashMessage', 'Success !! Update Question');
                    $window.location.reload();
                }, function(error) {
                    vm.inProgress = false;
                    throw error;
                });
        };

        vm.updateQuestion = function(exam) {
            vm.inProgress = true;
            var url = 'admin/question/updateQuestionAndAnswer';
            url = $rootScope.asset(url);
            $http.post(url, exam)
                .then(function(response) {
                    vm.inProgress = false;
                    vm.updateQuestionForm.$setPristine();
                    $cookieStore.put('flashClass', 'alert-success');
                    $cookieStore.put('flashMessage', 'Question/options updated successfully!!!');
                    $window.location.reload();
                }, function(error) {
                    vm.inProgress = false;
                    throw error;
                });
        };

        vm.deleteQuestion = function(href) {
            if (href === undefined || href === '') {
                throw ('href not defined');
            }
            if ($window.confirm('Are you sure to delete this question ?')) {
                $window.location.href = href;
            }
        };
    }
})(angular.module('app.components.admin.exam', [
    'ckeditor',
]));
