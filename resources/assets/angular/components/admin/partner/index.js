(function(app) {
    app.controller('AdminPartnerCtrl', AdminPartnerCtrl);
    AdminPartnerCtrl.$inject = ['$rootScope', '$scope', 'API'];

    function AdminPartnerCtrl($rootScope, $scope, API) {
        var vm = this;

        vm.page = 1;

        var params = {
            page: vm.page
        };
        API.partner.get(params)
            .then(function(partners) {
                vm.partners = partners;
                console.log(vm.partners);
            })
            .catch(function(error) {
                throw error;
            });

        vm.active = function(partner, params) {
            var cf = confirm('Are you sure you want to active this partner?');
            if (cf) {
                API.partner.active(partner.id, params)
                    .then(function(result) {
                        vm.partners = _.map(vm.partners, function(item) {
                            if (item.id === result.id) {
                                item = result;
                                return item;
                            } else {
                                return item;
                            }
                        });
                    })
                    .catch(function(error) {
                        alert(error.message);
                    });
            }
        };

        vm.delete = function(partner) {
            var cf = confirm('Are you sure you want to delete this partner?');
            if (cf) {
                API.partner.delete(partner.id)
                    .then(function() {
                        vm.partners.splice(vm.partners.indexOf(partner), 1);
                    })
                    .catch(function(error) {

                    });
            }
        };


    }
})(angular.module('app.components.admin.partner', []));
