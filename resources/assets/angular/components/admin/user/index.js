(function(app) {
    app.controller('AdminUserCtrl', AdminUserCtrl);
    AdminUserCtrl.$inject = ['$rootScope', '$timeout', 'API'];

    function AdminUserCtrl($rootScope, $timeout, API) {
        var vm = this;
        vm.user = {};
        vm.inprocess = false;

        API.role.get().then(function(result) {
            vm.roles = result;
        })
        .catch(function(error) {
            throw error;
        });

        vm.create = function(user) {
            vm.inprocess = true;
            API.user.create(user)
                .then(function(user) {
                    vm.inprocess = false;
                    vm.user = {};
                    vm.create_user_form.$setPristine();
                    vm.success = true;
                    $timeout(function() {
                        vm.success = false;
                    }, 5000);
                })
                .catch(function(error) {
                    vm.inprocess = false;
                    alert(error.message);
                });
        };

        vm.generate = function() {
            var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
            var pass = "";
            for (var x = 0; x < 8; x++) {
                var i = Math.floor(Math.random() * chars.length);
                pass += chars.charAt(i);
            }
            vm.user.password = pass;
        };
    }
})(angular.module('app.components.admin.user', []));
