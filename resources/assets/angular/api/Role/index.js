(function(app) {
	app.service('RoleService', RoleService);
    RoleService.$inject = ['$http', '$resource', '$q'];

    function RoleService($http, $resource, $q) {
    	var url = '/api/roles/:id';
        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
            }
        });

        this.get = function(params) {
            params = params || {};
            var deferred = $q.defer();
            resource.get(params, function(response) {
                deferred.resolve(response.data);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };
    }
})(angular.module('app.api.role', []));