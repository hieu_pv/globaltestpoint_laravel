(function(app) {
    app.service('UserService', UserService);
    UserService.$inject = ['$resource', '$http', '$q'];

    function UserService($resource, $http, $q) {
        var url = '/api/users/:id';
        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
            },
            create: {
                method: 'POST',
            },
            update: {
                method: 'PUT',
            }
        });

        this.create = function(user, params) {
            var deferred = $q.defer();
            user = user || {};
            params = params || {};
            resource.create(params, user, function(response) {
                deferred.resolve(response.data);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };
        this.profile = function() {
            var deferred = $q.defer();
            var url = '/api/me';
            $http.get(url)
                .then(function(response) {
                    deferred.resolve(response.data.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };
    }
})(angular.module('app.api.user', []));
