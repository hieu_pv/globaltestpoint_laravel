@extends('master') @section('title', 'Forgot your password') @section('content') @include('_include_angular')
<div ng-app="app" ng-controller="ForgotPasswordAdminCtrl as vm">
    <div class="container login_site admin-forgot-password-container">
        <div class="row">
            <div class="col-md-6 col-md-offset-4">
                @if (session('flash_message'))
                <div class="alert alert-{{ session('flash_level') }}">
                    {{ session('flash_message') }}
                </div>
                @endif
                <div ng-show="vm.errorMessage" ng-class="vm.errorClass" class="error-angular alert">
                    <ul>
                        <li ng-bind="vm.errorMessage"></li>
                    </ul>
                </div>
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please enter your email address here</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" method="POST" name="vm.forgotPasswordForm" ng-submit="vm.forgotPasswordForm.$valid && vm.forgotPassword()" novalidate>
                            <fieldset>
                                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                <div class="form-group">
                                    <input class="form-control" placeholder="Email" name="email" type="email" ng-change="vm.errorMessage = undefined" ng-model="vm.email" autofocus required ng-maxlength="40" ng-pattern='/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/'>
                                    <div class="error">
                                        <p ng-show="vm.forgotPasswordForm.$submitted && vm.forgotPasswordForm.email.$error.required">Email is required</p>
                                        <p ng-show="vm.forgotPasswordForm.$submitted && vm.forgotPasswordForm.email.$error.pattern">Please enter a valid email address</p>
                                        <p ng-show="vm.forgotPasswordForm.$submitted && vm.forgotPasswordForm.email.$error.maxlength">The value is too long</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button ng-disabled="vm.inProgress" type="submit" class="btn btn-success pull-left">Submit</button>  
                                    <a href="{{ url('testyourself') }}" class="btn btn-info pull-right">Back to Homepage</a>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
