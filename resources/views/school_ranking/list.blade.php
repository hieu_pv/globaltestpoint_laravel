@extends ('testyourself.testyourself') 
@section('title') Test Your Self - School Ranking @stop 
@section('content_testyourself')
<div class="container-fluid main_content_inner_page">
    <div class="page-content school-ranking-container container_site">
        <div class="school-ranking-filter col-xs-12">
            <div class="filter">
                <form class="form-inline" action="" method="GET">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <select name="country" id="country" class="form-control full-width country-school">
                                <option value="">Filter by country</option>
                                @if (!$countries->isEmpty()) 
                                @foreach ($countries as $country)
                                <option value="{{ $country['id'] }}" {{ isset($request['country']) && $request['country'] == $country['id'] ? 'selected' : '' }}>{{ $country['name'] }}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group col-sm-6">
                            <select name="state" id="state" class="form-control full-width state-school">
                            </select>
                        </div>
                        <div class="form-group col-sm-6">
                            <select name="schooldegree" id="schooldegree" class="form-control full-width">
                                <option value="">Filter by school degree</option>
                                @if (!$school_degrees->isEmpty()) 
                                @foreach ($school_degrees as $school_degree)
                                <option value="{{ $school_degree['id'] }}" {{ isset($request['schooldegree']) && $request['schooldegree'] == $school_degree['id'] ? 'selected' : '' }}>{{ $school_degree['name'] }}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group col-sm-6">
                            <select name="schooltype" id="schooltype" class="form-control full-width">
                                <option value="">Filter by school type</option>
                                @if (!$school_types->isEmpty()) 
                                @foreach ($school_types as $school_type)
                                <option value="{{ $school_type['id'] }}" {{ isset($request['schooltype']) && $request['schooltype'] == $school_type['id'] ? 'selected' : '' }}>{{ $school_type['name'] }}</option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                        <div class="form-group col-sm-12">
                            <input type="text" name="keyword" class="form-control full-width" placeholder="Search by school name..." value="{{ isset($request['keyword']) && $request['keyword'] != null ? $request['keyword'] : '' }}">
                        </div>
                        <div class="col-sm-12 search-button">
                            <button type="submit" class="btn btn-default">Search</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="school-ranking-content col-xs-12">
            <table class="responsive-table bordered school">
                <thead>
                    <tr>
                        @if (!$schools->isEmpty())
                        <th>Rank</th>
                        <th class="th-image">Image</th>
                        <th>Name</th>
                        <th>Country</th>
                        @else
                            @if (isset($request) && count($request) > 0)
                            <th colspan="4" class="no-record">No matching results</th>
                            @else
                            <th colspan="4" class="no-record">The data is being updated</th>
                            @endif
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @if(!empty($schools)) 
                    @foreach ($schools as $school)
                    <tr id="{{ $school['id'] }}">
                        <td><a href="{{ url('testyourself/school', $school['slug']) }}">{{ $school['rank'] }}</a></td>
                        <td>
                            <a href="{{ url('testyourself/school', $school['slug']) }}">
                                <img class="img-thumbnail img-responsive margin-auto" src="{{ asset($school['image']) }}" alt="{{ $school['name'] }}">
                            </a>
                        </td>
                        <td><a href="{{ url('testyourself/school', $school['slug']) }}">{{ $school['name'] }}</a></td>
                        <td><a href="{{ url('testyourself/school', $school['slug']) }}">{{ $school->state->country['name'] }}</a></td>
                    </tr>
                    @endforeach 
                    @endif
                </tbody>
            </table>
        </div>
        <div class="school-ranking-pagination text-right col-xs-12">{{ $schools->links() }}</div>
    </div>
</div>
</div>
<script>
    function generateStates(country_id, state_id) {
        var url = BASE_URL + 'school/getStatesByCountry/' + country_id;
        $.ajax({
            method: 'GET',
            url: url,
            success: function(response) {
                var states = response.data;
                var domElement = "<option value=''>Filter by state</option>";
                states.forEach(function(state, key) {
                    if (parseInt(state.id) === parseInt(state_id)) {
                        domElement += "<option selected value='" + state.id + "'>" + state.name + "</option>";
                    } else {
                        domElement += "<option value='" + state.id + "'>" + state.name + "</option>";
                    }
                });
                $('.state-school').html(domElement);
            },
            error: function(err) {
                deleteStates();
            }
        });
    }

    function deleteStates() {
        $('.state-school').html('');
    }

    if ($('.country-school').length > 0) {
        $('.country-school').change(function() {
            var value = $(this).val();
            if (value !== '') {
                generateStates(value);
            } else {
                deleteStates();
            }
        });
    }

    @if (isset($request['state']) && $request['state'] != null) 
        generateStates(1, {{ $request['state'] }});
    @else
        generateStates(1);
    @endif
</script>
@stop