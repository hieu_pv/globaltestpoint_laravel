@extends ('testyourself.testyourself') 
@section('title') Test Your Self - {{ $school != null ? $school['name'] : 'School' }} @stop 
@section('content_testyourself')
<div class="container-fluid main_content_inner_page">
    <div class="page-content school-detail-container container_site">
        <div class="school-detail-content col-xs-12">
        	@if ($school != null)
            <div class="school row">
                <div class="school-image col-sm-4 col-xs-5 full-xs">
                    <a href="{{ url('testyourself/school', $school['slug']) }}"><img src="{{ asset($school['image']) }}" alt="{{ $school['name'] }}" class="img-responsive"></a>
                </div>
                <div class="school-content col-sm-8 col-xs-7 full-xs">
                    <a href="{{ url('testyourself/school', $school['slug']) }}">
                        <h3 class="school-name">{{ $school['name'] }}</h3>
                    </a>
                    <a href="{{ url('testyourself/school', $school['slug']) }}">
                        <h4 class="school-rank">Rank {{ $school['rank'] }}</h4>
                    </a>
                    <div class="school-description">{!! $school['description'] !!}</div>
                </div>
            </div>
            @else
            <div class="custom-alert alert-danger">School not found!</div>
            @endif
        </div>
    </div>
</div>
</div>
@stop
