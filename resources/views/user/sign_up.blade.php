@extends ('testyourself.testyourself') 
@section('title') 
    Testyourself 
@stop 
@section('content_testyourself')
<div class="container_site">
    <div class="col-md-12">
        @include('blocks.error')
        <div id="testyourself" class="sign_up_page">
            <div id="signup">
                <div class="container-fluid main_content_inner_page container_fluider_sigup">
                    <div class="container container_sign_up">
                        <form action="" class="reg_box" enctype="multipart/form-data" id="form_signup" method="post" name="form_signup" novalidate="novalidate">
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                            <input name="mode" type="hidden" value="ca">
                            <div class="row">
                                <h2 class="reg_title">
                                    <img alt="icon" src="{!! asset('assets/frontend/images/testyourself/reg_ico.jpg') !!}">
                                    <div>
                                        Registration
                                    </div>
                                </h2>
                                <div class="row">
                                    <div class="col-sm-6 form-group">
                                        <div class="col-sm-4">
                                            <label for="">
                                                <span class="required">
                                                    *
                                                </span> Full Name:
                                            </label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input class="form-control" autocomplete="off" class="texbox_rounded" id="fname" maxlength="100" name="fname" type="text" tabindex="1" value="{!! (isset($_POST['fname'])) ? $_POST['fname'] : old('fname') !!}" />
                                        </div>
                                    </div>

                                    <div class="col-sm-6 form-group">
                                        <div class="col-sm-4">
                                            <label for="telephone">Telephone: </label>
                                        </div>
                                        <div class="col-sm-8 sign-up-popup">
                                            <p class="text-popup telephone-popup">If you desire to be notified on job vacancies and new items</p>
                                            <input autocomplete="off" class="form-control" class="texbox_rounded number" id="telephone" maxlength="20" name="utel" type="text" tabindex="10" value="{!! (isset($_POST['utel'])) ? $_POST['utel'] : old('utel') !!}" />
                                        </div>
                                    </div>
                                    {{-- <div class="col-sm-6 form-group">
                                        <div class="col-sm-4">
                                            <label for="">
                                                Middle Name:
                                            </label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input autocomplete="off" class="texbox_rounded form-control" id="mname" maxlength="100" name="mname" type="text" tabindex="7" value="{!! old('mname') !!}" />
                                        </div>
                                    </div> --}}
                                </div>
                                <div class="row">
                                    {{-- <div class="col-sm-6 form-group">
                                        <div class="col-sm-4">
                                            <label for="">
                                                <span class="required">
                                                    *
                                                </span> Last Name:
                                            </label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input autocomplete="off" class="texbox_rounded form-control" id="lname" maxlength="100" name="lname" type="text" tabindex="2" value="{!! old('lname') !!}" />
                                        </div>
                                    </div> --}}
                                    {{-- <div class="col-sm-6 form-group">
                                        <div class="col-sm-4">
                                            <label for="">
                                                <span class="required">
                                                    *
                                                </span> Gender:
                                            </label>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12">
                                                    <label class="lb_radio label_gender_register">Male</label> 
                                                    <input checked="" class="" name="gender" type="radio" value="1" tabindex="9"> 
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12">
                                                    <label class="lb_radio label_gender_register">Female</label>
                                                    <input checked="" class="" name="gender" type="radio" value="0"> 
                                                </div>
                                            </div>
                                        </div>
                                    </div> --}}
                                    <div class="col-sm-6 form-group">
                                        <div class="col-sm-4">
                                            <label for="udob">Date Of birth: </label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input autocomplete="off" class="form-control texbox_rounded required" id="udob" maxlength="10" name="udob" type="text" data-provide="datepicker" tabindex="11" value="{!! (isset($_POST['udob'])) ? $_POST['udob'] : old('udob') !!}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 form-group">
                                        <div class="col-sm-4">
                                            <label for="">
                                                <span class="required">
                                                    *
                                                </span> Email Address:
                                            </label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input autocomplete="off" class="texbox_rounded form-control" id="uemail" maxlength="100" name="uemail" type="text" tabindex="3" value="{!! (isset($_POST['uemail'])) ? $_POST['uemail'] : old('uemail') !!}" />
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-6 form-group">
                                        <div class="col-sm-4">
                                            <label for="">
                                                Upload Photo:
                                            </label>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="customfile-wrap">
                                                <input class="file" id="ufile" name="ufile" type="file" tabindex="13">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 form-group">
                                        <div class="col-sm-4">
                                            <label for="">
                                                <span class="required">
                                                    *
                                                </span> Create Password:
                                            </label>
                                        </div>
                                        <div class="col-sm-8 sign-up-popup">
                                            <p class="text-popup password-popup">This is not your email password but set a password that you will use for this site</p>
                                            <input autocomplete="off" class="texbox_rounded form-control" id="pass1" maxlength="20" name="pass1" type="password" tabindex="4" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 form-group">
                                        <div class="col-sm-4">
                                            <label for="">
                                                <span class="required">
                                                    *
                                                </span> Confirm Password:
                                            </label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input autocomplete="off" class="texbox_rounded form-control" id="pass2" maxlength="20" name="pass2" type="password" tabindex="5">
                                        </div>
                                    </div>
                                    {{-- <div class="col-sm-6 form-group">
                                        <div class="col-sm-4">
                                            <label for="">
                                                <span class="required">
                                                    *
                                                </span> User Type:
                                            </label>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="row">
                                                <div class="col-sm-6 col-md-4">
                                                    <label class="lb_radio label_gender_register" >Student</label>
                                                    <input checked="" name="utype" type="radio" value="student" tabindex="12"> 
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6 col-md-4">
                                                    <label class="lb_radio label_gender_register"> Tutor</label>
                                                    <input name="utype" type="radio" value="tutor">
                                                </div>
                                            </div>
                                        </div>
                                    </div> --}}
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 form-group">
                                        <div class="col-sm-4">
                                            <label for="#ulocation">
                                                Location:
                                            </label>
                                        </div>
                                        <div class="col-sm-8">
                                            <select class="form-control" id="ulocation" name="ulocation" tabindex="6">
                                                <option value="">-- select location --</option>
                                                @if(isset($listcountry) && !empty($listcountry))
                                                    @foreach($listcountry as $keylist => $country)
                                                        <option value="{{ $country->id }}">{{ $country->name }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 form-group">
                                        <div class="col-sm-4">
                                            <label for="">
                                                Prove You're Not A Robot :  
                                            </label>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="form-group container_fluider_sigup_recapcha">
                                                <div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_CAPTCHA') }}"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-2 col-sm-offset-2 register">
                                        <input class="blue_btn1 form-control btn btn-primary" name="" type="submit" value="Register">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#udob').datepicker();
    })
</script>
@stop