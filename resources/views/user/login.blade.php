@extends ('testyourself.testyourself') @section('title') Testyourself @stop @section('content_testyourself')
<div class="login">
    <div class="container">
        <div class="col-md-6 col-md-offset-2">
            @include('blocks/error')
            <h3><img src="{!! asset('assets/frontend/images/login.jpg') !!}" alt=""> <span>Registered user Log-in</span></h3>
            <form class="form-horizontal" role="form" method="post">
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <div class="form-group">
                    <label class="control-label col-sm-4" for="email">Email Address:</label>
                    <div class="col-sm-8">
                        <input type="email" name="email" class="form-control" id="email" placeholder="Enter email">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-4" for="pwd">Password:</label>
                    <div class="col-sm-8">
                        <input type="password" name="password" class="form-control" id="pwd" placeholder="Enter password">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="checkbox">
                            <a href="{!! url('sign_up') !!}" class="pull-left">Register an account</a>
                            <a href="{!! url('forgot-password') !!}" class="pull-right">Forgot Password?</a>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <button type="submit" class="btn btn-primary">LOGIN</button>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="login-options login-facebook">
                            <a class="social-icon-color facebook" title="Facebook" href="{{ url('login/facebook') }}">
                                <i class="fa fa-facebook-official"></i>
                                <div class="social-title">
                                    <strong>Login</strong> with <strong>Facebook</strong>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="login-options login-google">
                            <a class="social-icon-color google" title="Google" href="{{ url('login/google') }}">
                                <i class="fa fa-google"></i>
                                <div class="social-title">
                                    <strong>Login</strong> with <strong>Google</strong>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-offset-4 col-sm-8">
                        <div class="login-options login-linkedin">
                            <a class="social-icon-color linkedin" title="Linkedin" href="{{ url('login/linkedin') }}">
                                <i class="fa fa-linkedin"></i>
                                <div class="social-title">
                                    <strong>Login</strong> with <strong>Linked</strong><i class="icon-linkedin fa fa-linkedin-square"></i>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop
