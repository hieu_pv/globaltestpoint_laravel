@extends('master')

@section('content')
    @include('testyourself.partials.header')

    @yield ('content_testyourself')

    @yield('script_stripe')

    @include ('testyourself.partials.footer')
@stop
