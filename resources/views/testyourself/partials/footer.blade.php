<div class="footer_test_yourself">
    <div class="container_site">
        <div class="col-md-12 col-xs-12">
             <div class="container-fluid">
                @if (!Route::is('testyourself.index'))
                <h4 class="hits"><a href="javascript:;">{{ (Session::has('online')) ? Session::get('online') : '0' }} hits</a></h4>
                @endif
                <div class="footer_icon">
                    <div class="row bottom_contact_us_section">
                        <div class="container">
                        <?php $optionsite = Session::get('optionsite'); ?>

                            <div class="row">
                                <div class="col-xs-12 col-sm-6 col-md-5 col-md-push-1 col-number-1 center-block">
                                    {{-- <figure>
                                        <a href="{{ url('contact-us') }}"><img src="{!! asset('assets/frontend/images/contact_ico.png') !!}" alt="contact"></a>
                                    </figure>
                                    <a href="#">{{ (!empty($optionsite->email) ? $optionsite->email : 'contact@demo.org') }}</a> --}}
                                    @if(!empty($cate_footer1))
                                        @foreach($cate_footer1 as $key => $val_foo)
                                            @php
                                                $url = url('testyourself/' . str_slug($val_foo->type) . '/' . ($val_foo->slug));
                                                if(!empty($val_foo['link'])) {
                                                    $url = $val_foo['link'];
                                                }   
                                            @endphp
                                            <p><a href="{{ $url }}">{{ $val_foo->name }}</a></p>
                                        @endforeach
                                    @endif
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-4 col-number-2 text-left">
                                    {{-- <figure>
                                        <img src="{!! asset('assets/frontend/images/mob_ico.png') !!}" alt="mobile">
                                    </figure>
                                    Telephone - <a href="#">{{ (!empty($optionsite->phone) ? $optionsite->phone : '+00 000 000 000') }}</a> --}}
                                    @if(!empty($cate_footer2))
                                        @foreach($cate_footer2 as $key => $val_foo)
                                            @php
                                                $url = url('testyourself/' . str_slug($val_foo->type) . '/' . ($val_foo->slug));
                                                if(!empty($val_foo['link'])) {
                                                    $url = $val_foo['link'];
                                                }   
                                            @endphp
                                            <p><a href="{{ $url }}">{{ $val_foo->name }}</a></p>
                                        @endforeach
                                    @endif
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-3 col-number-3 text-left">
                                    {{-- <figure>
                                        <img src="{!! asset('assets/frontend/images/addr_ico.png') !!}" alt="address">
                                    </figure>
                                    {{ (!empty($optionsite->address) ? $optionsite->address : 'Address company') }} --}}
                                    @if(!empty($cate_footer3))
                                        @foreach($cate_footer3 as $key => $val_foo)
                                            @php
                                                $url = url('testyourself/' . str_slug($val_foo->type) . '/' . ($val_foo->slug));
                                                if(!empty($val_foo['link'])) {
                                                    $url = $val_foo['link'];
                                                }   
                                            @endphp
                                            <p><a href="{{ $url }}">{{ $val_foo->name }}</a></p>
                                        @endforeach
                                    @endif
                                </div>
                                {{-- <div class="col-xs-12 col-sm-6 col-md-3">
                                    <figure>
                                        <img src="{!! asset('assets/frontend/images/fax_ico.png') !!}" alt="fax">
                                    </figure>
                                    FAX: {{ (!empty($optionsite->fax) ? $optionsite->fax : '+00 000 000 000') }}
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer_test">
                <div class="col-md-4 col-sm-6 col-xs-12 footer_left">
                    <p>
                        <a href="{!! url('term-and-policy') !!}">Terms & Conditions and Privacy Policy</a>
                    </p>
                </div>
                <div class="col-md-2 col-sm-6 col-xs-12 footer_left">
                    <p><a href="{!! url('contact-us') !!}">Contact Us</a></p>
                </div>
                <div class="col-md-2 col-sm-6 col-xs-12 footer_left">
                    <p><a href="{!! url('testyourself/show-faq') !!}">FAQs</a></p>
                </div>
                <div class="col-md-4 col-sm-6 col-xs-12 footer_right">
                    <p>Stay connect with 
                        @if (!empty($optionsite->facebook))
                            <a href="{{ $optionsite->facebook }}">
                                <i class="fa fa-facebook-official icon_fb" aria-hidden="true"></i>
                            </a>
                        @endif

                        @if (!empty($optionsite->youtube))
                            <a href="{{ $optionsite->youtube }}">  
                                <i class="fa fa-youtube icon_twitter" aria-hidden="true"></i>
                            </a>
                        @endif

                        @if (!empty($optionsite->instagram))
                            <a href="{{ $optionsite->instagram }}">  
                                <i class="fa fa-instagram icon_twitter" aria-hidden="true"></i>
                            </a>
                        @endif

                        @if (!empty($optionsite->twitter))
                            <a href="{{ $optionsite->twitter }}"> 
                                <i class="fa fa-twitter icon_twitter" aria-hidden="true"></i>
                            </a>
                        @endif
                    </p>
                </div>
            </div>
            <div class="footer-address">
                <div class="body">
                    <div class="icon">
                        <i class="fa fa-map-marker"></i>
                    </div>
                    <div class="content">
                        <p>Roxy Drive, Windsor Mill, Baltimore MD. USA</p>
                        <p>Suite 104 Medel Center, Gudu District, Abuja Nigeria.</p>
                    </div>
                </div>
            </div>
            <div class="footer_below">
                <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                    2017 © <a href="{{ url('') }}">www.globaltestpoint.com </a>All Rights Reserved.
                </div>
            </div>
        </div>
    </div>
</div>