@foreach($categories as $key => $category)
    @php
        // var_dump(in_array($category['position'], [1, 2, 3])
    @endphp
    @if ($category['parent_id'] == $category_parent['id'] && (!in_array($category['position'], [1, 2, 3])))
    <li class="sub_li_menu_test">
        @if ($category['type'] == 'document')
        <a href="{!! URL::route('testyourself.document.indexshow', $category['slug']) !!}">{!! $category['name'] !!}</a>
        @elseif ($category['hasChild'])
        <a href="javascript:;">{!! $category['name'] !!}</a>
        @else
        <a href="{!! URL::route('testyourself.exam.examList', $category['slug']) !!}">{!! $category['name'] !!}</a>
        @endif
        <ul class="dropdown-menu">
            @include('testyourself.partials.menu_top', ['category_parent' => $category])
        </ul>
    </li>
    @endif
@endforeach
@if ($category_parent['type'] == 'blank-page' && (!in_array($category_parent['position'], [1, 2, 3])))
    @if (!$category_parent->posts->isEmpty())
        @foreach ($category_parent->posts()->where('active', 1)->get() as $blank)
        @if ($blank->link()->where('page', 2)->first() != null)
        <li class="sub_li_menu_test">
            <a href="{!! URL::route('testyourself.blankpage.show', $blank['slug']) !!}">{!! $blank['title'] !!}</a>
        </li>
        @endif
        @endforeach
    @endif
@endif
