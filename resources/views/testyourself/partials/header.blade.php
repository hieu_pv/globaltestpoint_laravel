<div class="header_test_yourself">
    <div class="container_site">
        <div class="col-md-12">
            <?php $optionsite = Session::get('optionsite');?>
            <div class="logo_register">
                <div class="col-md-6 col-sm-6 col-xs-6 full-xs logo_test">
                    <a href="{!! url('testyourself') !!}">
                        <div class="logo" style="background: url('{!! (!empty($optionsite->logo) ? asset($optionsite->logo) : asset('assets/frontend/images/testyourself/logo.png')) !!}') no-repeat;">
                        </div>
                    </a>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 full-xs register_col">
                    <div class="register">
                        <p>
                            @if(Auth::user())
                            <img src="{!! (!empty(Auth::user()->image) ? url(Auth::user()->image) : asset('uploads/default_avatar.png')) !!}" alt="" id="user_avatar" style="width: 50px; height: 50px; border-radius:50%;">
                            <a class="member_name" href="#">
                                {!! Auth::user()->first_name !!}
                            </a>
                            |
                            <a href="{!! url('user/profile') !!}">
                                Profile
                            </a>
                            |
                            <a href="{!! url('logout') !!}">
                                Logout
                            </a>
                            @else
                            <a class="login" href="{!! url('login') !!}">
                                Login
                            </a>
                            |
                            <a class="sign_up" href="{!! url('sign_up') !!}">
                                Sign Up
                            </a>
                            @endif
                        </p>
                    </div>
                </div>
            </div>
            <nav class="navbar navbar-default navbar-static-top">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button class="navbar-toggle" data-target="#myNavbar" data-toggle="collapse" type="button">
                            <span class="icon-bar">
                            </span>
                            <span class="icon-bar">
                            </span>
                            <span class="icon-bar">
                            </span>
                        </button>
                        <a class="navbar-brand dropdown header-home" href="{!! url('/testyourself') !!}">
                            Home
                        </a>
                    </div>
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav">
                            @if(isset($category_parents))
                            @foreach ($category_parents as $category_parent)
                                @if(!in_array($category_parent['position'], [1, 2, 3]))
                                    <li class="dropdown li_menu_test">
                                    @if ($category_parent['type'] == 'document')
                                    <a class="dropdown-toggle" href="{!! URL::route('testyourself.document.indexshow') !!}">
                                    @elseif ($category_parent['hasChild'] || $category_parent['type'] == 'blank-page')
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    @else
                                    <a class="dropdown-toggle" href="{!! URL::route('testyourself.exam.examList', $category_parent['id']) !!}">
                                    @endif
                                        {!! $category_parent['name'] !!}
                                        @if ($category_parent['hasChild'])
                                        <span class="caret"></span>
                                        @endif
                                    </a>
                                        @if (!$categories->isEmpty())
                                        <ul class="dropdown-menu">
                                            @include('testyourself.partials.menu_top', ['category_parent' => $category_parent])
                                        </ul>
                                        @endif
                                    </li>
                                @endif
                            @endforeach
                            @endif
                            <li class="dropdown li_menu_test">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    More
                                    <span class="caret">
                                    </span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="dropdown sub_li_menu_test">
                                        <a href="{!! URL('/contact-us') !!}">
                                            Contact Us
                                        </a>
                                    </li>
                                    <li class="dropdown li_menu_test">
                                        <a href="{!! url('/testyourself/show-faq') !!}">
                                            FAQ
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
        @if (Session::has('flash_message'))
        <div class="message container">
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-{!! Session::get('flash_level') !!}">
                        {!! Session::get('flash_message') !!}
                    </div>
                </div>
            </div>
        </div>
        @elseif(!empty(Auth::user()))
            @if((Auth::user()->email_verified != 1) || (Auth::user()->email_verified != '1'))
                <div class="message container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="custom-alert alert-danger">
                                Your account status is still pending: Check your mail to activate your account or contact the <a href="{!! URL('/contact-us') !!}">Admin</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        @endif
<?php
// use App\Entities\Option;
// use Illuminate\Support\Facades\Session;

// $getviewsite = Option::select(['id', 'viewsite'])->first();
// $view = Option::find($getviewsite['id']);
// if (!SESSION::has('online')) {
//     if(empty($view)) {
//         $view['viewsite'] = 0;
//     }
//     $online = $view['viewsite'] + 1;
//     if(empty($view)) {
//         $view = new Option;
//     }
//     $view->viewsite = $online;
//     $view->save();
//     SESSION::set('online', $online);
// } else {
//     SESSION::put('online', $view['viewsite']);
// }
?>
    </div>
</div>
