@extends ('testyourself.testyourself') 
@section('title') 
    Testyourself 
@stop 
@section('content_testyourself')
<?php $optionsite = Session::get('optionsite'); ?>
<div class="feeture_test_yourself">
    <div class="container_site">
        <div class="col-md-9 col-sm-9 col-xs-12">
            <div class="slider_test_yourself slider_site">
                <div id="myCarousel" class="owl-carousel">
                    @if (!$sliders->isEmpty())
                        @foreach($sliders as $slider)
                            <div class="item" style="background:url('{!! asset($slider['image']) !!}')"><img src="{!! url('assets/frontend/images/blank-image.png') !!}"></div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="menu_test_yourself_right">
                <ul>
                    @if(isset($blankhomepage) && !empty($blankhomepage))
                        @foreach($blankhomepage as $valueblank)
                            @if ($valueblank->post['title'] == 'School Ranking')
                            <li><a href="{{ url('/testyourself/'.$valueblank->post['slug']) }}">{{ $valueblank->post['title'] }}</a></li>
                            @else
                            <li><a href="{{ url('/testyourself/blank-page/'.$valueblank->post['slug']) }}">{{ $valueblank->post['title'] }}</a></li>
                            @endif
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="content_testyourself">
    <div class="container_site">
        <div class="free_share">
            <p>We have <a class="number" href="{{ url('testyourself/exams') }}">35,000+</a> Free Practice Questions in Over <a class="subject" href="{{ url('testyourself/exams') }}">20 Subjects</a> that will Make you Pass your Exam With Flying Colours ...</p>
        </div>
        <div class="flash_video">
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="list_step item1 not-border">
                    <!-- <div class="img" style="background-image: url('{!! asset($optionsite['image_login']) !!}');">
                        <img src="{!! asset('assets/frontend/images/blank-image.png') !!}" alt="">
                    </div> -->
                    <div class="title"><a class="text-capitalize" href="{{ url('sign_up') }}">register</a></div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="list_step item2 not-border">
                    <!-- <div class="img" style="background-image: url('{!! asset($optionsite['image_register']) !!}');">
                        <img src="{!! asset('assets/frontend/images/blank-image.png') !!}" alt="">
                    </div> -->
                    <div class="title"><a class="text-capitalize" href="{{ url('login') }}">login</a></div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="list_step item3 not-border">
                    <!-- <div class="img" style="background-image: url('{!! asset($optionsite['image_login_register']) !!}');">
                        <img src="{!! asset('assets/frontend/images/blank-image.png') !!}" alt="">
                    </div> -->
                    <div class="title"><a href="{{asset('become-partner')}}">Become a Partner</a></div>
                </div>
            </div>
            @if(isset($video['link']))
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="list_step item1">
                    <div class="img3" style="">
                        <div id="player"></div>
                    </div>
                </div>
            </div>
            <script>
                youtube_parser("{!! $video['link'] !!}");
            </script>
            @endif
            @if(isset($flashs))
            <?php $i = 2;  ?> 
            @foreach($flashs as $key => $flash)
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="list_step item{{ $i }}">
                    <div class="img2 box-min-height" style="background-image: url('{!! asset($flash["link"]) !!}');">
                        <img src="" alt="">
                    </div>
                </div>
            </div>
            <?php if($i >= 3) { $i = 2; } $i++;?> 
            @endforeach 
            @endif
        </div>
        <div class="col-xs-12 col-md-10 col-md-offset-1">
            <div class="big_black_title">
                <h2>A great place <span class="for_learning">for practice and learning</span></h2>
                <p>Academic Test, Professional Test, School Ranking, Job Listing, Job Aptitude Test, Job Interview Tips, eBooks, Manuals, Software, consulting, Schools, Scholarships, Career Advise etc.</p>
            </div>
        </div>
    </div>
</div>
@stop
