@extends ('testyourself.testyourself') 
@section('title')
    FAQ
@stop
@section('content_testyourself')
@if ($faqs->isEmpty())
<div class="row">
	<div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 pull-left left">
		<h3 class="error no-record">To be updated soon</h3>
	</div>
</div>
@else
<div class="row">
	<div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 pull-left left">
		<div class="panel-group" id="accordion">
			@foreach ($faqs as $faq)
			    <div class="panel panel-default">
			      	<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $faq['id'] }}">
			         	<h4 class="panel-title">
			               <i class="fa fa-plus-circle" aria-hidden="true"></i>
			               {{ $faq['question'] }}
			         	</h4>
			      	</div>
			        <div id="collapse{{ $faq['id'] }}" class="panel-collapse collapse">
			            <div class="panel-body">
				            {!! $faq['answer'] !!}
			            </div>
			        </div>
			    </div>
		    @endforeach
		</div>
	</div>
</div>
@endif
@stop