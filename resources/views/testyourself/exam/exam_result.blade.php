@extends ('testyourself.testyourself') @section('title') Test Your Self - Exam @stop @include('_include_angular') @section('content_testyourself')
<div class="container-fluid main_content_inner_page">
    <div class="container page-content exam-result-container">
        <div class="page-header">
            <h1>Hello @if(Auth::user()) {{ Auth::user()->first_name }} @else guest @endif, here is your score</h1>
        </div>
        <div class="row page-body">
            <div class="col-sm-8 center table-result-content">
                <h4>Thank you for taking the test.</h4>
                <div class="padding10 pdf-print">
                    @if(!empty(Auth::user()) && ((Auth::user()->email_verified != 1) || (Auth::user()->email_verified != '1')))
                        <div class="custom-alert alert-danger center">
                            Active your account to Save and Print your test !
                        </div>  
                    @else
                        @if(!empty(Auth::user()))
                            <a href="{{ url('print-pdf/result-pdf') }}" class="btn btn-primary pdf-button"></i> Save result as PDF</a>
                            <a href="{{ url('print-pdf/print-exam') }}" target="_blank" class="btn btn-primary print-button" target="_blank"></i> Print</a>
                        @else
                        <a href="{{ url('login') }}" style="text-decoration: none;">
                            <div class="custom-alert alert-danger center">
                                Log in to review the questions
                            </div> 
                        </a>
                        @endif
                    @endif
                </div>
                @if ($examResult == null)
                <table class="table table-result table-bordered table-condensed table-datatable table-hover">
                    <tr>
                        <td class="error">Your exam result dont exist</td>
                    </tr>
                </table>
                @else
                <table class="table table-result table-bordered table-condensed table-datatable table-hover">
                    <tr>
                        <td width="40%">Name of candidate:</td>
                        <td width="50%">{!! Auth::user() ? Auth::user()->first_name : 'You' !!}</td>
                    </tr>
                    <tr>
                        <td width="40%">Subject:</td>
                        <td width="50%">{!! $examResult['exam']['name'] !!}</td>
                    </tr>
                    <tr>
                        <td>Date/Time:</td>
                        <td>{!! $examResult['examDate'] !!}</td>
                    </tr>
                    <tr>
                        <td>Scores:</td>
                        <td>{!! $examResult['score'] !!}%</td>
                    </tr>
                    <tr>
                        <td>Grade:</td>
                        <td>{!! $examResult['grade'] !!}</td>
                    </tr>
                    <tr>
                        <td>Remark:</td>
                        <td>{!! $examResult['remark'] !!}</td>
                    </tr>
                </table>
                @endif
            </div>
            @if ($examResult != null)
            <div class="col-sm-12 center review-content">
                @if( !empty(Auth::user()) && ((Auth::user()->email_verified != 1) || (Auth::user()->email_verified != '1')))
                    <div class="col-sm-6 custom-alert alert-danger center">
                        Active your account to Review test and Take more test !
                    </div>  
                @elseif(!empty(Auth::user()))
                    <h4 class="review_test_result btn btn-primary active-toggle-review"><a class="review-animation title a_no_hover">Review your test</a>
                    </h4> @if(Auth::user())
                    <h4 class="review_test_result btn btn-primary">
                       <a class="title a_no_hover" href="{!! url('testyourself/exams') !!}">Take more Test</a>
                    </h4>
                @else
                    <div class="col-sm-6 custom-alert alert-danger center">
                        <a href="{{ url('login') }}">Login to Review test and Take more test !</a>
                    </div>
                @endif
                <div class="toggle-review {{ $page == 1 ? 'hide' : '' }}">
                    <table class="table table-review">
                        <tr>
                            <td>
                                @if (!empty($questions)) @foreach ($questions as $key => $question) @if (!$buyExam && $examResult['exam']['price'] != 0) @if ($key
                                <=4 ) @include( 'testyourself.exam.exam_result._table_review') @endif @else @include( 'testyourself.exam.exam_result._table_review') @endif @endforeach @else <table class="table">
                                    <tr>
                                        <td class="required">Don't exists any question!</td>
                                    </tr>
                    </table>
                    @endif
                    </td>
                    </tr>
                    </table>
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="pagination-button text-left">
                                @if ($page > 1) 
                                <a href="{{ url('testyourself/exam-result?page='.($page-1)) }}" class="btn btn-warning">
                                    <i class="glyphicon glyphicon-backward"></i> Back
                                </a>
                                @endif
                                @if ($page < $maxPage)
                                <a href="{{ url('testyourself/exam-result?page='.($page+1)) }}" class="btn btn-success">
                                Next <i class="glyphicon glyphicon-forward"></i>
                                @endif
                                </a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">                    
                            <div class="text-center">
                                <a class="btn btn-warning" href="{!! url('testyourself/exams') !!}">Take more Test</a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                            <div class="text-right">
                                <a class="btn btn-warning" href="{!! url('testyourself') !!}">Home</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 center toggle-review {{ $page == 1 ? 'hide' : '' }}">
                    @if(empty(Auth::user()))
                        <div class="no_login_review">
                            <div class="col-md-12 text-center">
                                <div class="alert alert-danger">
                                    <strong>Opssss!!</strong> You must register/sign-up to be able to see your performance.
                                </div>
                            </div>
                            <div class="login_register">
                                <p>Registered User: <a href="{{ url('login') }}">Login here</a></p>
                                <p>New User: <a href="{{ url('sign_up') }}">Register/Sign-up-here</a></p>
                            </div>
                        </div>
                    @endif
                </div>
                @endif
            </div>
            @endif
        </div>
    </div>
</div>
</div>
@stop
