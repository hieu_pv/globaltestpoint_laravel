@extends ('testyourself.testyourself') @section('title') Download Exam - Testyourself @stop @section('content_testyourself')
<div id="frontend_document" class="exam-list">
    <div class="container_site">
        <div class="search-wrapper search-document text-right">
            <form class="search-form form-inline col-sm-12" action="" method="GET">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-6">
                        <label class="sr-only" for="search">Search by Exam Key Words e.g. Maths</label>
                        <div class="input-group">
                            <input type="text" name="keyword" class="form-control" id="search" placeholder="Search by Exam Key Words e.g. Maths">
                            <div class="input-group-addon">
                                <button type="submit" class="submit-search">
                                    <i class="glyphicon glyphicon-search"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="list-document">
            @if(isset($exams) && (!empty($exams))) 
                @if(Auth::check())
                <form action="{{ url('testyourself/payment-multi-exam') }}" method="post" id="choicemultiform_listpage">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="backlink" value="{{ Request::fullUrl() }}">
                @endif
                @foreach($exams['item'] as $key => $exam)
                    <div class="col-xs-12 col-sm-4 col-md-3 frame-container">
                        <div class=" col-md-12 child-frame">
                            <div class="col-md-12 info-doc info-exam center">
                                <div class="col-md-12 name-doc name-exam">
                                    <p>
                                        {{ $exam['name'] }}
                                    </p>
                                </div>
                                @if(Auth::check())
                                    <div class="wrap-inp-multi">
                                        @if(($exam['paid'] == 1 || $exam['price'] == 0) && Auth::check())
                                            <input type="checkbox" name="choicemultiexam[]" value="{{ $exam['id'] }}" disabled="disabled">
                                        @else
                                            <input type="checkbox" name="choicemultiexam[]" value="{{ $exam['id'] }}">
                                        @endif
                                    </div>
                                    <div class="col-md-12 exam-button">
                                        @if($exam['paid'] == 1 || $exam['price'] == 0)
                                        <a href="{!! URL::route('testyourself.exam.examStart', $exam['slug']) !!}">
                                            <button class="btn btn-primary" type="button">
                                                <i aria-hidden="true" class="glyphicon glyphicon-ok">
                                                </i> Taken Exam
                                            </button>
                                        </a>
                                        @else
                                        <a href="{{ url('testyourself/payment/exam-'.$exam['id']) }}">
                                            <button class="btn btn-danger" type="button">
                                                <i aria-hidden="true" class="fa fa-download">
                                                </i> Take exam
                                                <i aria-hidden="true" class="fa fa-lock">
                                                </i> {{ getenv('STRIPE_CURRENCY_SYMBOL') }}{{ $exam['price'] }}
                                            </button>
                                        </a>
                                        @endif
                                    </div>
                                @else
                                <div class="col-md-12">
                                    <a href="{{ url('login') }}">
                                        <button class="btn btn-default" type="button">
                                            <i aria-hidden="true" class="fa fa-cogs">
                                            </i> Login/ Register
                                        </button>
                                    </a>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach 
                <div class="row">
                    <div class="col-xs-12 center-block text-center">
                        <button class="btn btn-success" type="submit" form="choicemultiform_listpage" @if(!Auth::check()) {{ 'disabled="disabled"' }} @endif>Payment for Exams selected</button>
                    </div>
                </div>
                @if(Auth::check())
                    </form>
                @endif
            @endif
        </div>
        <div class="pagination pull-right">{{ $exams->links() }}</div>
    </div>
</div>
@stop
