@extends ('testyourself.testyourself') @section('title') Test Your Self - Exam @stop @section('content') @include('_include_angular')
<div class="exam-container">
    <div class="header_test_yourself">
        <div class="container_site">
            <div class="col-md-12">
                <?php $optionsite = Session::get('optionsite'); ?>
                <div class="logo_register">
                    <div class="col-md-6 col-sm-6 col-xs-6 logo_test">
                        <a href="{!! url('/') !!}">
                          <div class="logo" style="background: url('{!! (!empty($optionsite->logo) ? asset($optionsite->logo) : asset('assets/frontend/images/testyourself/logo.png')) !!}') no-repeat;">
                          </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div ng-app="app" ng-controller="ExamCtrl" class="container-fluid main_content_inner_page exam_font_en_action" ng-init="duration = {{$exam['duration']}}; buyExam = {{$buyExam}}; userLogin = {{$userLogin}}; exam_id = {{$exam['id']}}; user = {{$user != null ? $user : 'null'}}">
        <div class="container page-content">
            <div class="page-header fix-h1">
                <h3 class="back_home_test">
                  <a class="bk fix-h1" href="{{ url('/testyourself') }}">
                    <span class="back-home"> Back to home page </span>
                  </a>  
                </h3>
            </div>
            <div class="col-sm-6 total-attempted" style="overflow: hidden;height: 70px;">
                <div class="blue_box1 tol-attmpt">
                    Total Attempted: <span id="tot_atm" ng-bind="tot_atm">-</span>
                </div>
            </div>
            <div class="col-sm-6 pull-right" id="areaCoundown"></div>
            <div class="clearfix"></div>
            <div class="row" ng-init="">
                <div class="col-lg-12">
                    @include('blocks/error')
                    <div class="well well-lg main-container" scroll-to-element>
                        <div id="loginForm">
                            <form class="form-horizontal" method="post" id="form1" name="form1" ng-submit="doneExam()">
                                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                                <input type="hidden" value="end" name="mode">
                                <input type="hidden" value="" name="eid">
                                <input type="hidden" value="" name="st_exams_time">
                                <input type="hidden" value="" name="etime">
                                <div class="list_question_exam_font_en">
                                    <div class="question">
                                        <div ng-if="questionsDisplay !== undefined && questionsDisplay.length > 0" ng-repeat="(key, question) in questionsDisplay[page - 1]">
                                            <h4><strong>Question @{{ question.sttQuestion }}</strong></h4>
                                            <p ng-show="question.type == 1" ng-bind-html="question.question"></p>
                                            <img ng-show="question.type == 2" ng-src="@{{asset(question.question)}}" class="img-responsive img-thumbnail margin-bottom-10">
                                            <ul class="list_options" ng-if="question.answers !== undefined && question.answers.data.length > 0">
                                                <li ng-repeat="answer in question.answers.data">
                                                    <input type="radio" ng-model="answer.is_correct" ng-value="1" ng-change="getTotalAttempted(question, answer)" id="question@{{key+1}}answer@{{$index}}" name="question@{{key+1}}">
                                                    <label ng-bind-html="answer.answer" for="question@{{key+1}}answer@{{$index}}"></label>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="list_question_exam_font_en_submit">
                                    <button type="button" ng-show="page > 1" class="btn btn-info" ng-click="prevPage()">PREVIOUS</button>
                                    <button type="button" ng-show="page <questionsDisplay.length" class="btn btn-info" ng-click="nextPage()">NEXT</button>
                                    <button ng-disabled="vm.inProgress" type="submit" class="btn btn-success">Submit Test</button>
                                    <p>Do not go to any other page while taking test. Your data may be lost.</p>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('testyourself.partials.footer') @stop
