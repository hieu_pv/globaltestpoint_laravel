@extends ('testyourself.testyourself') @section('title') Exam list @stop @section('content_testyourself')
<div class="container-fluid main_content_inner_page exam-list-container">
    @if ($category == null)
    <div class="container page-content">
        <div class="page-header">
            <h3 class="error">To be updated soon</h3>
        </div>
    </div>
    @else
    <div class="container page-content">
        <div class="page-header">
            <h3>
                Category: {!! $category['name'] !!}
            </h3>
            <div class="row introduction_book">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    @if(!empty($introduction_variousexam))
                        {!! $introduction_variousexam !!}
                    @endif
                </div>
            </div>
        </div>
        <table class="exam-table table table-bordered table-condensed table-responsive table-responsive">
            <thead>
                <tr>
                    <th class="col-xs-2 text-center">Bulk Payment</th>
                    <th class="col-xs-3">Exam subject</th>
                    <th class="text-center col-xs-2">Duration</th>
                    <th class="text-center col-xs-2">Price</th>
                    <th class="text-center col-xs-3">Action</th>
                </tr>
            </thead>
            <tbody>
                @if(isset($exams)) 
                    @if(Auth::check())
                    <form action="{{ url('testyourself/payment-multi-exam') }}" method="post" id="choicemultiform">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="backlink" value="{{ Request::fullUrl() }}">
                    @endif
                        @foreach($exams as $exam)
                        <tr>
                            <td class="text-center">
                                <span class="title-on-mobile">Bulk Payment</span>
                                <span class="content">
                                @if($exam->paided === 0 && Auth::check())
                                    <input type="checkbox" name="choicemultiexam[]" value="{{ $exam->id }}">
                                @elseif($exam->paided === 1 && Auth::check())
                                    Paid !
                                @else 
                                    <input type="checkbox" name="choicemultiexam[]" value="{{ $exam->id }}" disabled="disabled">
                                @endif
                                </span>
                            </td>
                            <td>
                                <span class="title-on-mobile">Exam subject</span>
                                <span class="content">{!! $exam->name !!}</span>
                            </td>
                            <td class="text-center">
                                <span class="title-on-mobile">Duration</span>
                                <span class="content">{!! $exam->duration !!} mins</span>
                            </td>
                            @if(!empty(Auth::user()))
                                @if((Auth::user()->email_verified != 1) || (Auth::user()->email_verified != '1'))
                                    <td class="text-center">
                                        <span class="title-on-mobile">Price</span>
                                        <span class="content">Your account has not been activated</span>
                                    </td>
                                @else
                                    <td class="text-center">
                                        <span class="title-on-mobile">Price</span>
                                        <span class="content">{{ getenv('STRIPE_CURRENCY_SYMBOL') }}{!! $exam->price !!}</span>
                                    </td>
                                @endif
                            @else
                                <td class="text-center">
                                    <span class="title-on-mobile">Price</span>
                                    <span class="content">
                                        <form action="{{ url('testyourself/saveFullURL') }}" method="POST" id="formLoginExam">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="_method" value="POST">
                                            <input type="hidden" name="fullUrl" value="{{ Request::fullUrl() }}">
                                            <input type="hidden" name="redirect" value="{{ url('login') }}">
                                            <button class="btn btn-default" type="submit" form="formLoginExam"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Login/Register</button>
                                        </form>
                                    </span>
                                </td>
                            @endif
                            <td class="take-test text-center">
                                <span class="title-on-mobile">Action</span>
                                <span class="content">
                                    <p class="tooltip-mobile">Accept the terms and policy to continue. Check the bottom page!</p>
                                    <a class="a-tag-exam btn btn-sm btn-primary btn-exam-test" data-toggle="tooltip" data-placement="top" title="Accept the terms and policy to continue. Check the bottom page!"  id="take-test-{{ $exam->id }}" stt="{{ $exam->id }}" href="{!! URL::route('testyourself.exam.examStart', $exam->slug) !!}">
                                        Take test
                                    </a>
                                </span>
                            </td>
                        </tr>
                        @endforeach 
                        <tr>
                            <td class="text-center">
                                <button class="btn btn-success" type="submit" form="choicemultiform" @if(!Auth::check()) {{ 'disabled="disabled"' }} @endif>Make payment</button>
                            </td>
                            <td colspan="4" class="td-make-payment-unnecessary"></td>
                        </tr>
                    @if(Auth::check())                        
                    </form>
                    @endif
                @endif
            </tbody>
        </table>
        <div class="back">
            <a href="{!! url('testyourself') !!}" class="btn btn-primary">
                <i class="glyphicon glyphicon-backward"></i> Back
            </a>
        </div>
        {{-- <div class="term-use">
            <input type="checkbox" name="examterm" value="" required="" id="examterm"> 
            I accept Globaltespoint’s
            <a href="{!! url('term-and-policy') !!}" class="">
            Terms of Use and Privacy Policy     
            </a>
        </div> --}}
    </div>
    @endif
</div>
{{-- <script type="text/javascript">
    $(document).ready(function(){
        $('.btn-exam-test').attr('disabled', true);
        $('.btn-exam-test').hover(function(){
            $('[data-toggle="tooltip"]').tooltip(); 
        });
        $("#examterm").change(function(){
            if(!$(this).prop('checked'))
            {
                $('.btn-exam-test').attr('disabled', true);
                $('.btn-exam-test').hover(function(){
                    $('[data-toggle="tooltip"]').tooltip();
                });
                $('.tooltip-mobile').show();
            }
            else
            {
                $('.btn-exam-test').attr('disabled', false);
                $('.btn-exam-test').hover(function(){
                    $('[data-toggle="tooltip"]').tooltip("destroy")
                });
                $('.tooltip-mobile').hide();
            }
        })
    });
</script> --}}
@stop