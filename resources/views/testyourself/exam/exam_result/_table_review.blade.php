<table class="table">
    <tr class="question-stt">
        <td><strong>Question #{!! $key + 1!!}</strong></td>
    </tr>
    <tr class="question">
        @if ($question['type'] == 1)
        <td>
            {!! $question['question']!!}
        </td>
        @else
        <td>
            <img src="{!! asset($question['question']) !!}" class="img-responsive img-thumbnail">
        </td>
        @endif
    </tr>
    <tr class="list-answer">
        <td>
            <table class="table">
                @foreach($question['answers'] as $key2 => $answer)
                <tr>
                    <th class="col-sm-1 no-padding">{!! $alphas[$key2] !!} :</th>
                    <td class="col-sm-11 no-padding">{!! $answer['answer'] !!}</td>
                </tr>
                @endforeach
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <strong>Your choice: </strong>{!! $question['yourChoice'] !!}
        </td>
    </tr>
    <tr>
        <td>
            <strong>Correct answer is: </strong>{!! $question['answerCorrect'] !!}
        </td>
    </tr>
    <tr>
        @if (isset($question['solution']) && $question['solution'] != '')
        <td>
            <p><strong>Solution</strong></p>
            <div>{!! $question['solution'] !!}</div>
        </td>
        @endif
    </tr>
    <tr>
        <td>
            <div class="fb-comments" data-order-by="reverse_time" data-href="{{ url("testyourself/exam-result#question" . $question['id']) }}" data-numposts="5"></div>
        </td>
    </tr>
</table>
