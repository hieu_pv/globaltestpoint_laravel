@extends ('testyourself.testyourself') 
@section('title') Documents selected - Testyourself @stop 
@section('content_testyourself')
<div class="container_site">
    @if(isset($listchoose) && !empty($listchoose))
	    <div class="container">
	    	<div class="col-md-8 col-sm-12 center">
		        <table class="table table-hover table-bordered">
		            <thead class="center">
		                <tr>
		                    <th>
		                        Image
		                    </th>
		                    <th>
		                        Name
		                    </th>
		                    <th>
		                        Price
		                    </th>
		                </tr>
		            </thead>
		            <tbody>
		            	<?php $total = 0; ?>
		                @foreach($listchoose as $keylist => $vallist)
		                <tr>
		                    <td>
		                        <img alt="{{ $vallist['name'] }}" height="100px" src="{{ asset($vallist['image']) }}" width="100px">
		                        </img>
		                    </td>
		                    <td>
		                        {{ $vallist['name'] }}
		                    </td>
		                    <td>
		                        {{ $vallist['price'] }} {!! getenv('STRIPE_CURRENCY_SYMBOL') !!}
		                        <?php $total += $vallist['price']; ?>
		                    </td>
		                </tr>
		                @endforeach
		                <tr>
		                	<td colspan="2">
		                		<h4 class="pull-right">Total: </h4>
		                	</td>
		                	<td>
		                		{{ $total }}{!! getenv('STRIPE_CURRENCY_SYMBOL') !!}
		                	</td>
		                </tr>
		                <tr>
		                	<td colspan="2"></td>
		                	<td >
		                		<form method="post" accept-charset="utf-8">
		                			<input type="hidden" name="backlink" value="{{ "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" }}">
		                			<input type="hidden" name="_token" value="{{ csrf_token() }}">
		                			<input class="btn btn-primary" name="multipayment" type="submit" value="Go to Payment">
		                		</form>
		                	</td>
		                </tr>
		            </tbody>
		        </table>
		    </div>
	    </div>
    @else
    <h3>
        No documents selected
    </h3>
    @endif
</div>
@stop
