@extends ('testyourself.testyourself') @section('title') Document - Testyourself @stop @section('content_testyourself')
<div id="frontend_document">
    <div class="container_site search-document search-wrapper">
        <form action="" class="search-form form-inline col-sm-12" method="post" id="formmultidocument">
            <div class="search-document text-right">
                <input name="_token" type="hidden" value="{{ csrf_token() }}">
                @if(Auth::user())
                <div class="form-group form-group-payment">
                    <button class="btn btn-primary" name="multipayment" type="submit">
                        Payment for documents selected
                    </button>
                </div>
                @endif
                <div class="form-group form-group-search">
                    <label class="sr-only" for="search">
                        Search by Exam Key Words e.g. Agric
                    </label>
                    <div class="input-group">
                        <input class="form-control" id="search" name="keyword" placeholder="Search by Exam Key Words e.g. Agric" type="text">
                            <div class="input-group-addon">
                                <button class="submit-search" type="submit">
                                    <i class="glyphicon glyphicon-search">
                                    </i>
                                </button>
                            </div>
                        </input>
                    </div>
                </div>
            </div>
            <div class="row introduction_book">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class=" col-md-12 ">
                    @if(!empty($introduction_book))
                    {!! $introduction_book !!}
                    @endif
                    </div>
                </div>
            </div>
            <div class="list-document">
                @if(isset($alldocument) && (!empty($alldocument['item']))) 
                    @foreach($alldocument['item'] as $keydoc => $document)
                        <div class="col-xs-12 col-sm-12 col-md-12 frame-container">
                            <div class=" col-md-12 child-frame">
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="frame-img center">
                                        <img alt="{{ $document['name'] }}" src="{{ url($document['image']) }}">
                                            @if(Auth::user())
                                            <input name="choosedocument[]" type="checkbox" value="{{ $document['id'] }}" @if($document['paid'] == true || $document['price'] == 0) {{ 'disabled="disabled"' }} @endif>
                                                @endif
                                            </input>
                                        </img>
                                    </div>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-12 info-doc left">
                                    <div class="col-md-12 name-doc">
                                        <p>
                                            {{ $document['name'] }}
                                        </p>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 intro_doc left">
                                        {!! $document['introduction_book'] !!}
                                    </div>
                                    @if(Auth::user())
                                    <div class="col-md-12">
                                        @if($document['paid'] == true || $document['price'] == 0)
                                        <a href="{{ asset($document['document']) }}" target="_blank">
                                            <button class="btn btn-primary" type="button">
                                                <i aria-hidden="true" class="fa fa-download">
                                                </i> Download
                                            </button>
                                        </a>
                                        @else
                                        <a class="saveCurrentPage" href="javascript:;" data-url="{{ Request::fullUrl() }}" data-href="{{ URL('testyourself/payment/document-'.$document['id']) }}">
                                            <button class="btn btn-danger" type="button">
                                                <i aria-hidden="true" class="fa fa-download">
                                                </i> Download with
                                                <i aria-hidden="true" class="fa fa-lock">
                                                </i> {!! getenv('STRIPE_CURRENCY_SYMBOL') !!}{{ $document['price'] }}
                                            </button>
                                        </a>
                                        <div class="form-group">
                                            <button class="btn btn-primary" name="multipayment" type="submit" form="formmultidocument">
                                                Payment for documents selected
                                            </button>
                                        </div>
                                        @endif
                                    </div>
                                    @else
                                    <div class="col-md-12">
                                        <a class="saveCurrentPage" href="javascript:;" data-url="{{ Request::fullUrl() }}" data-href="{{ url('login') }}">
                                            <button class="btn btn-default" type="button">
                                                <i aria-hidden="true" class="fa fa-cogs">
                                                </i> Login/ Register
                                            </button>
                                        </a>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="col-xs-12 col-sm-12">
                        <div class="pagination pull-right">
                            {{ $alldocument->links() }}
                        </div>
                    </div>
                @else
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="custom-alert alert-danger">
                            @if (isset($request['keyword']) && $request['keyword'] != null)
                                <strong>
                                    Keyword not found !
                                </strong>
                                    Please try again with other keywords ... !
                            @else
                                
                            @endif
                        </div>
                    </div>
                @endif
            </div>
            </div>
        </form>
    </div>
</div>
@stop