@extends ('testyourself.testyourself') @section('title') Blank - Testyourself @stop @section('content_testyourself')
<div class="container_site payment-container">
	<div class="centered-form">
        <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        @if(!empty($status) && ($status == 1))
                        	Transaction successful !
                        @else
                        	Transaction failure !
                        @endif
                    </h3>
                </div>
                <div class="panel-body">       
                    @if(Auth::user())
                    <div id="formupdate" class="center">
                    	@if(!empty($status) && ($status == 1))
	                        <div class="custom-alert alert-success">
	                        	<strong>{{ (!empty($message)) ? $message : 'Have not any message !' }}</strong>
	                        </div>
	                        <a href="{{ (!empty($backlink)) ? $backlink : '' }}" title=""><button class="btn btn-primary ">Click here to go to {{ (!empty($page) && $page == 'document') ? 'Download' : 'Test' }} page</button></a>
	                    @else
	                    	<div class="custom-alert alert-danger">
	                    		<strong>{{ (!empty($message)) ? $message : 'Have not any message !' }}</strong>
	                        </div>
	                        <p>(Please, check your account information and try again !)</p>
	                        <a href="{{ (!empty($backlink)) ? $backlink : '' }}" title=""><button class="btn btn-primary btn-lg">Click here to go previous page</button></a>
                        @endif
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@stop