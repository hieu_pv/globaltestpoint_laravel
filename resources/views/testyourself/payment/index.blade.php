@extends ('testyourself.testyourself') @section('title') Payment online - Testyourself @stop @section('content_testyourself')
<div class="message-paystack container hide">
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-paystack">
            </div>
        </div>
    </div>
</div>
<div class="container_site payment-container">
    {{-- <div class="container">
        <div class="col-md-12 col-sm-12">
            <div class="form-group">
                <h3>
                    <u>
                        <i aria-hidden="true" class="fa fa-credit-card">
                        </i>
                        Payment Page for Globaltestpoint.com!
                    </u>
                </h3>
            </div>
        </div>
    </div> --}}
    <div class="centered-form">
        <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
            <div class="tabbable-panel">
                <div class="tabbable-line">
                    <ul class="nav nav-tabs ">
                        <li class="active tab_common">
                            <a href="#tab_default_1" data-toggle="tab">
                                <img src="{!! asset('assets/images/paystack_logo_4.png') !!}" alt="Paystack" width="145px" height="29px">
                                <hr>
                                <img src="{!! asset('assets/images/visa_master_serve.png') !!}" alt="Paystack">
                            </a>
                        </li>
                        <!-- <li class="tab_common">
                            <a href="#tab_default_2" data-toggle="tab">
                                <img src="{!! asset('assets/images/stripe_logo.png') !!}" alt="Stripe" height="29px">
                            </a>
                            <img src="{!! asset('assets/images/visa_master.png') !!}" alt="Paystack" width="145px" height="40px">
                        </li> -->
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_default_1">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="stripe-errors hide">
                                        <div class="alert-payment">
                                            <span class="payment-errors">
                                            </span>
                                        </div>
                                    </div>
                                    @if(Auth::user())
                                        <?php $user = Auth::user();?>
                                        @if(!empty($paystackresponse) && ($paystackresponse->status === true))
                                            <table class="table table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            Name
                                                        </td>
                                                        <td>
                                                            <span class="l_name_p">
                                                                {{ $paystackresponse->data->first_name }}
                                                            </span>
                                                            <span class="f_name_p"> 
                                                                {{ $paystackresponse->data->last_name }} 
                                                            </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Email
                                                        </td>
                                                        <td>
                                                            <div class="email_p"> {{ $paystackresponse->data->email }} </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Phone
                                                        </td>
                                                        <td>
                                                            <div class="phone_p" >{{ $paystackresponse->data->phone }} </div>
                                                        </td>
                                                    </tr>
                                                    @if(!empty($totalprice))
                                                        <tr>
                                                            <td>
                                                                Amount:
                                                            </td>
                                                            <td>
                                                                {!! getenv('STRIPE_CURRENCY_SYMBOL') !!}{{ $totalprice }}
                                                            </td>
                                                        </tr>
                                                    @endif
                                                    @php
                                                        if (Session::has('backlink_url')) {
                                                            $subbacklink = Session::get('backlink_url');
                                                        }
                                                    @endphp
                                                    <tr>
                                                        <td colspan="2">
                                                            <form action="{{ (isset($checkmulti) && $checkmulti == 1) ? url('testyourself/payment-list-documents') : url('testyourself/payment-with-paystack/' . $type) }}" id="multipayment" method="POST" role="form">
                                                                <input name="_token" type="hidden" value="{{ csrf_token() }}">
                                                                <input type="hidden" name="backlink" value="{{ (isset($subbacklink) ? $subbacklink : $backlink ) }}">
                                                                <input type="hidden" name="email" value="{{ $paystackresponse->data->email }}">
                                                                <input type="hidden" name="first_name" value="{{ $paystackresponse->data->first_name }}">
                                                                <input type="hidden" name="last_name" value="{{ $paystackresponse->data->last_name }}">
                                                                <input type="hidden" name="phone" value="{{ $paystackresponse->data->phone }}">
                                                                <input type="hidden" name="exist" value="{{ (!empty($paystackresponse->data->id) ? 'yes' : 'no') }}">
                                                                <input type="hidden" name="product_id" value="{{ (!empty($product_id) ? $product_id : '') }}">
                                                                <button type="button" class="center-block btn btn-success" data-toggle="modal" data-target="#processpayment_paystack">Proceed to Payment page</button>
                                                                <div class="modal fade" id="processpayment_paystack" role="dialog">
                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content center">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                <h4 class="modal-title">Confirm Payment !</h4>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <div class="custom-alert alert-warning">
                                                                                    <div><h4>Message: {!! getenv('STRIPE_CURRENCY_SYMBOL') !!}{{ (!empty($totalprice)) ? $totalprice : 0 }} will be deducted from your account</h4></div>
                                                                                    <div class="text-center"><h5>A processing fee of {!! getenv('STRIPE_CURRENCY_SYMBOL') !!}{{ $qty_choose * (int)getenv('PROC_FEE_PAYSTACK') }} for {{ $qty_choose }} product(s) will be added to this transaction</h5></div>   
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <input class="btn btn-primary" name="paystack_submit" type="submit" value="Continue">
                                                                                <a href="{{ (!empty($backlink)) ? $backlink : URL::previous() }}">
                                                                                    <button class="btn btn-default" type="button">
                                                                                        Back
                                                                                    </button>
                                                                                </a>
                                                                                <button class="btn btn-default" data-dismiss="modal" type="button">
                                                                                    Cancel
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                            <br>
                                                            @if(!empty($user->paystack_id))
                                                            @php
                                                                if (Session::has('backlink_url')) {
                                                                    $subbacklink = Session::get('backlink_url');
                                                                } else {
                                                                    $subbacklink = URL::previous();
                                                                }
                                                            @endphp
                                                            <a href="javascript:void(0)" class="clicktoupdate">Click here to update your infomation on Paystack</a>
                                                            <div id="formUpdatePaystack">
                                                                <hr>
                                                                <form id="formpaystack" action="{!! url('testyourself/update-card') !!}" method="post">
                                                                    <input name="_token" class="_token" type="hidden" value="{{ csrf_token() }}">
                                                                    <input type="hidden" class="backlink_url" name="backlink_url" value="{{ (isset($subbacklink) ? $subbacklink : $backlink ) }}">
                                                                    <input type="hidden" class="multipayment" name="multipayment" value="{{ ((isset($checkmulti) && $checkmulti == 1) ? $checkmulti : '' ) }}">
                                                                    <div class="row">
                                                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                                                            <div class="form-group">
                                                                                <input class="form-control input-sm"  id="first_name_p" name="first_name" placeholder="First name" type="text">
                                                                                </input>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                                                            <div class="form-group">
                                                                                <input class="form-control input-sm"  id="last_name_p" name="last_name" placeholder="Last name" type="text">
                                                                                </input>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                                                            <div class="form-group">
                                                                                <input class="form-control input-sm"  id="email_p" name="email" placeholder="Email" type="email">
                                                                                </input>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-12 col-sm-12 col-md-12">
                                                                            <div class="form-group">
                                                                                <input class="form-control input-sm"  id="phone_p" name="phone" placeholder="Phone" type="text">
                                                                                </input>
                                                                            </div>
                                                                        </div>
                                                                        @if(isset($checkmulti) && $checkmulti == 1)
                                                                            <div class="col-xs-12 col-sm-12 col-md-12 center-block">
                                                                                <div class="form-group text-center">
                                                                                    <button class="btn btn-success" id="updateCustomerAjax" name="updateCustomer" type="button" value="Update"><span class="spinload" style="float: left;"><i class="fa fa-spinner fa-pulse fa-fw spinner-loading" aria-hidden="true"></i>&nbsp </span>Update</button>
                                                                                    <button class="btn btn-danger" id="cancelUpdate" type="button">Cancel</button>
                                                                                </div>
                                                                            </div>
                                                                        @else
                                                                        <div class="col-xs-12 col-sm-12 col-md-12 center-block">
                                                                            <div class="form-group text-center">
                                                                                <input class="btn btn-success" id="updateCustomer" name="updateCustomer" type="submit" value="Update">
                                                                                <button class="btn btn-danger" id="cancelUpdate" type="button">Cancel</button>
                                                                            </div>
                                                                        </div>
                                                                        @endif
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_default_2">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">
                                        Payment Card
                                        <small>
                                            It's easy!
                                        </small>
                                    </h3>
                                </div>
                                <div class="panel-body">
                                    <div class="stripe-errors hide">
                                        <div class="alert-payment">
                                            <span class="payment-errors">
                                            </span>
                                        </div>
                                    </div>
                                    @if(Auth::user())
                                    <?php $user = Auth::user();?>
                                    @if(!empty($customer_info))
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    Name
                                                </td>
                                                <td>
                                                    {{ (!empty($user->last_name)) ? $user->last_name : $user->first_name }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Email
                                                </td>
                                                <td>
                                                    {{ $customer_info->email }}
                                                </td>
                                            </tr>
                                            <tr>
                                                @if((empty($customer_info->sources['data'][0]->last4)))
                                                    <td colspan="2">
                                                        <p class="text-danger">An error occurred while making a payment</p>
                                                    </td>
                                                @else
                                                    <td>
                                                        Last Four
                                                    </td>
                                                    <td>
                                                        {{ $customer_info->sources['data'][0]->last4 }}
                                                    </td>
                                                @endif
                                            </tr>
                                            @if(!empty($totalprice))
                                                <tr>
                                                    <td>
                                                        Amount:
                                                    </td>
                                                    <td>
                                                        {!! getenv('STRIPE_CURRENCY_SYMBOL') !!}{{ $totalprice }}
                                                    </td>
                                                </tr>
                                            @endif
                                            <tr>
                                                <td colspan="2">
                                                    <form action="{{ (isset($checkmulti) && $checkmulti == 1) ? url('testyourself/payment-list-documents') : '' }}" id="multipayment" method="POST" role="form">
                                                        <input name="_token" type="hidden" value="{{ csrf_token() }}">
                                                        <input type="hidden" name="backlink" value="{{ URL::previous() }}">
                                                        <button type="button" class="btn btn-primary center-block" data-toggle="modal" data-target="#processpayment_1">Proceed to Payment page</button>
                                                        {{-- <input id="paystacksubmit" class="btn btn-info btn-block" type="submit" name="paystacksubmit" value="Payment via PayStack"> --}}
                                                        <div class="modal fade" id="processpayment_1" role="dialog">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content center">
                                                                    <div class="modal-header">
                                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                        <h4 class="modal-title">Confirm Payment !</h4>
                                                                    </div>
                                                                    <div class="modal-body">    
                                                                        <div class="custom-alert alert-warning">
                                                                            <div><h4>Message: {!! getenv('STRIPE_CURRENCY_SYMBOL') !!}{{ (!empty($totalprice)) ? $totalprice : 0 }} will be deducted from your account</h4></div>
                                                                            <div class="text-center"><h5>Let use {!! getenv('PERCENT_FEE_STRIPE') !!}% of total purchase for stripe as processing fees </h5></div>  
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button class="btn btn-primary" type="submit">
                                                                            Continue
                                                                        </button>
                                                                        <a href="{{ (!empty($backlink)) ? $backlink : '' }}">
                                                                            <button class="btn btn-default" type="button">
                                                                                Back
                                                                            </button>
                                                                        </a>
                                                                        <button class="btn btn-default" data-dismiss="modal" type="button">
                                                                            Cancel
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <hr>
                                    <a href="javascript:void(0)" class="clickhereupdate"><u>Click here to Update your card</u></a>
                                    <script type="text/javascript">
                                        $(document).ready(function(){
                                            $('#formupdate').css('display','none');
                                            $('.clickhereupdate').click(function(){
                                                $('#formupdate').css('display','block');
                                            })
                                        });
                                    </script>
                                    <hr>
                                    @endif
                                    <div id="formupdate">
                                        <form action="{{ (isset($checkmulti) && $checkmulti == 1) ? url('testyourself/payment-list-documents') : '' }}" id="payment-form" method="POST" role="form">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-12 col-md-12">
                                                    <div class="form-group">
                                                        <input class="form-control input-sm" data-stripe="number" data-paystack="number" id="first_name" name="" placeholder="Card number" required="" size="20" type="text">
                                                        </input>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <input class="form-control input-sm" data-stripe="cvc" data-paystack="cvv" maxlength="4" placeholder="CVC" required="" size="4" type="text">
                                                </input>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-6 col-sm-6 col-md-6">
                                                    <div class="form-group">
                                                        <select class="form-control input-sm" data-stripe="exp-month" data-paystack="expiryMonth" required="">
                                                            @for($i = 1; $i <= 12; $i++)
                                                            @if ($i < 10)
                                                            <option value="{{ $i }}">
                                                                0{{ $i }}
                                                            </option>
                                                            @else
                                                            <option value="{{ $i }}">
                                                                {{ $i }}
                                                            </option>
                                                            @endif
                                                            @endfor
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6 col-sm-6 col-md-6">
                                                    <div class="form-group">
                                                        <select class="form-control input-sm" data-stripe="exp-year" data-paystack="expiryYear" required="">
                                                            @for($i = 2017; $i <= 2100; $i++)
                                                            <option value="{{ $i }}">
                                                                {{ $i }}
                                                            </option>
                                                            @endfor
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                                <input type="hidden" name="backlink" value="{{ URL::previous() }}">
                                                <input name="_token" type="hidden" value="{{ csrf_token() }}">
                                                <button type="button" class="btn btn-primary submit center-block" data-toggle="modal" data-target="#processpayment_2">{{ (!empty($customer_info)) ? "Update Card" : "Proceed to Payment page" }}</button>
                                            <div class="modal fade" id="processpayment_2" role="dialog">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">Confirm Payment !</h4>
                                                        </div>
                                                        <div class="modal-body center">
                                                            <div class="custom-alert alert-warning">
                                                                <div><h4>Message: {!! getenv('STRIPE_CURRENCY_SYMBOL') !!}{{ (!empty($totalprice)) ? $totalprice : 0 }} will be deducted from your account</h4></div>
                                                                <div class="text-center">
                                                                    <h5>Let use <?php echo env('PERCENT_FEE_STRIPE', 5) ?>% of total purchase for stripe as processing fees</h5>
                                                                </div>  
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button class="btn btn-primary" type="submit" data-paystack="submit">
                                                                Continue
                                                            </button>
                                                            <a href="{{ (!empty($backlink)) ? $backlink : '' }}">
                                                                <button class="btn btn-default" type="button">
                                                                    Back
                                                                </button>
                                                            </a>
                                                            <button class="btn btn-default" data-dismiss="modal" type="button">
                                                                Cancel
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop 

@section('script_stripe')
<script src="https://js.stripe.com/v2/" type="text/javascript">
</script>
<script src="{{ asset('assets/js/stripe.js') }}" type="text/javascript">
</script>
<script src="https://js.paystack.co/v1/paystack.js"></script>
@stop
