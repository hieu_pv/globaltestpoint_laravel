<div class="form-group">
    <label for="name"><span class="required">*</span> Name</label>
    <input type="text" name="name" class="form-control" id="name" placeholder="Name" value="{{ isset($industry['name']) && $industry['name'] != null ? $industry['name'] : old('name') }}">
</div>
<div class="form-group">
    <label for="order">Order</label>
    <input type="text" name="order" class="form-control" id="order" placeholder="Order" value="{{ isset($industry['order']) && $industry['order'] != null ? $industry['order'] : 0 }}">
</div>
<div class="form-group">
    <label for="active">Active</label>
    <input name="active" id="active" type="checkbox" value="1" {{ isset($industry['active']) && $industry['active'] == 1 ? 'checked' : 'checked' }}></input>
</div>
<button type="submit" class="btn btn-primary">{{ isset($buttonTitle) && $buttonTitle != null ? $buttonTitle : 'Submit' }}</button>
