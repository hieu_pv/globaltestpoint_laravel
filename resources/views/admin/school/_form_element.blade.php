<div class="form-group">
    <label for="image"><span class="required">*</span> Image</label>
    <input type="file" class="form-control school-image" id="image" name="image">
</div>
<div class="form-group">
    <label for="country_id"><span class="required">*</span> Country</label>
    <select name="country_id" id="country_id" class="form-control country-school">
        <option value=""></option>
        @if (!empty($countries))
        @foreach($countries as $country)
        <option value="{{$country['id']}}" {{ isset($school->state->country['id']) && $school->state->country['id'] == $country['id'] ? 'selected' : '' }}>{{$country['name']}}</option>
        @endforeach
        @endif
    </select>
</div>
<div class="form-group">
    <label for="state_id"><span class="required">*</span> State</label>
    <select name="state_id" id="state_id" class="form-control state-school">
    </select>
</div>
<div class="form-group">
    <label for="school_type_id"><span class="required">*</span> School type</label>
    <select name="school_type_id" id="school_type_id" class="form-control">
        <option value=""></option>
        @if (!empty($school_types))
        @foreach($school_types as $school_type)
        <option value="{{$school_type['id']}}" {{ isset($school['school_type_id']) && $school['school_type_id'] == $school_type['id'] ? 'selected' : '' }} >{{$school_type['name']}}</option>
        @endforeach
        @endif
    </select>
</div>
<div class="form-group">
    <label for="school_degree_id"><span class="required">*</span> School degree</label>
    <select name="school_degree_id" id="school_degree_id" class="form-control">
        <option value=""></option>
        @if (!empty($school_degrees))
        @foreach($school_degrees as $school_degree)
        <option value="{{$school_degree['id']}}" {{ isset($school['school_degree_id']) && $school['school_degree_id'] == $school_degree['id'] ? 'selected' : '' }} >{{$school_degree['name']}}</option>
        @endforeach
        @endif
    </select>
</div>
<div class="form-group">
    <label for="name"><span class="required">*</span> Name</label>
    <input type="text" name="name" class="form-control" id="name" placeholder="Name" value="{{ isset($school['name']) && $school['name'] != null ? $school['name'] : old('name') }}">
</div>
<div class="form-group">
    <label for="rank"><span class="required">*</span> Rank</label>
    <input type="text" name="rank" class="form-control" id="rank" placeholder="Rank" value="{{ isset($school['rank']) && $school['rank'] != null ? $school['rank'] : old('rank') }}">
</div>
<div class="form-group">
    <label for="summary">Summary</label>
    <textarea class="form-control" name="summary" id="summary" cols="30" rows="5">{{ isset($school['summary']) && $school['summary'] != null ? $school['summary'] : old('summary') }}</textarea>
</div>
<div class="form-group">
    <label for="order">Order</label>
    <input type="text" name="order" class="form-control" id="order" placeholder="Order" value="{{ isset($school['order']) && $school['order'] != null ? $school['order'] : 0 }}">
</div>
<div class="form-group">
    <label for="active">Active</label>
    <input name="active" id="active" type="checkbox" value="1" {{ isset($school['active']) && $school['active'] == 1 ? 'checked' : 'checked' }}></input>
</div>
<button type="submit" class="btn btn-primary">{{ isset($buttonTitle) && $buttonTitle != null ? $buttonTitle : 'Submit' }}</button>

<script type="text/javascript">
    $(document).ready(function() {
        function generateStates(country_id, state_id) {
            var url = BASE_URL + 'school/getStatesByCountry/' + country_id;
            $.ajax({
                method: 'GET',
                url: url,
                success: function(response) {
                    var states = response.data;
                    var domElement = "<option value=''>Select state</option>";
                    states.forEach(function(state, key) {
                        if (parseInt(state.id) === parseInt(state_id)) {
                            domElement += "<option selected value='" + state.id + "'>" + state.name + "</option>";
                        } else {
                            domElement += "<option value='" + state.id + "'>" + state.name + "</option>";
                        }
                    });
                    $('.state-school').html(domElement);
                },
                error: function(err) {
                    deleteStates();
                }
            });
        }

        function deleteStates() {
            $('.state-school').html('');
        }

        $('.country-school').change(function() {
            var value = $(this).val();
            if (value !== '') {
                generateStates(value);
            } else {
                deleteStates();
            }
        });

        @if (isset($school))
            $(".school-image").fileinput({
                'showUpload': false,
                'showRemove': false,
                initialPreview: [
                    "<img src='{{ asset($school['image']) }}' class='file-preview-image img-responsive img-thumbnail' alt='{{ $school['name'] }}' title='{{ $school['name'] }}'>",
                ],
            });

            generateStates({{ $school->state->country['id'] }}, {{ $school['state_id'] }});
        @else
            $(".school-image").fileinput({
                'showUpload': false,
                'showRemove': false
            });

            generateStates(1);
        @endif
    });
</script>