@extends('admin') @section('title') Add Exams @stop @section('content')
@include('_include_angular')
<div id="add_exams">
    <form class="form-horizontal" role="form" action="" method="post">
        <div class="container edit_users-admin" ng-app="app" ng-controller="AddExamCtrl as vm">
            @include('blocks/error')
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading"><span class="glyphicon glyphicon-cog"></span> Add Exams</div>
                    <div class="container_test_input panel-footer container">
                        <div class="col-md-12">
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="name">Name <span class="required">*</span></label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="name" placeholder="Enter name" name="name" value="{!! old('name') !!}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="category">Category <span class="required">*</span></label>
                                <div class="col-sm-10">
                                    <select class="form-control" id="category" name="category_id">
                                        <option value="">Select category</option>
                                        @foreach($categories as $category)
                                        <option value="{!! $category['id'] !!}">{!! $category['name'] !!}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="duration">Duration <span class="required">*</span></label>
                                <div class="col-md-10 col-sm-10 col-xs-12 input-group duration_add_exam">
                                    <input type="text" class="form-control" id="duration" placeholder="Enter Duration" name="duration" value="{!! old('duration') !!}" aria-describedby="basic-addon2">
                                    <span class="input-group-addon" id="basic-addon2">MINUTES</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="price">Price (USD) <span class="required">*</span></label>
                                <div class="col-md-10 col-sm-10 col-xs-12">
                                    <input type="text" class="form-control" id="price" placeholder="Enter Price" name="price" value="0">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="pass_percent">Pass Percent (%) <span class="required">*</span></label>
                                <div class="col-md-10 col-sm-10 col-xs-12">
                                    <input type="text" class="form-control" id="pass_percent" placeholder="Enter Pass Percent" name="pass_percent" value="{!! old('pass_percent') !!}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="grade">Grade <span class="required">*</span></label>
                                <div class="col-md-10 col-sm-10 col-xs-12">
                                    <div class="grade row" ng-repeat="grade in vm.grades">
                                        <label for="title" class="control-label col-sm-1">Title <span class="required">*</span></label>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control" id="title" name="grade_title[]" ng-model="grade.title" required maxlength="2">
                                        </div>
                                        <label for="min" class="control-label col-sm-1">Min <span class="required">*</span></label>
                                        <div class="col-sm-2">
                                            <input type="number" class="form-control" id="min" name="grade_min[]" ng-model="grade.min" min="0" required>
                                        </div>
                                        <label for="max" class="control-label col-sm-1">Max <span class="required">*</span></label>
                                        <div class="col-sm-2">
                                            <input type="number" class="form-control" id="max" name="grade_max[]" ng-model="grade.max" max="100" required>
                                        </div>
                                        <div class="col-sm-2">
                                            <button class="btn btn-danger" type="button" ng-click="vm.removeGrade(grade)">Remove</button>
                                        </div>
                                        <div class="error row">
                                            <div class="col-sm-8 col-sm-offset-4">
                                                <p class="margin-top-10" ng-show="grade.min > grade.max">Min field must be less than or equal Max field</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="add-grade row">
                                        <div class="col-sm-12">
                                            <button class="btn btn-success" type="button" ng-click="vm.addGrade()">Add grade</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="status">Status:</label>
                                <div class="col-sm-10">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" id="status" checked="checked" name="active" value="yes"> Active
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="address">Add Exams: </label>
                                <div class="col-md-10">
                                    <button type="submit" class="btn btn-success" name="exams">Add New Exams</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
