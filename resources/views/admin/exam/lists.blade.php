@extends('admin') @section('title') Manage Exams @stop @section('content')
<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading"><span class="glyphicon glyphicon-cog"></span> Manage Exams
            </div>
            <div class="panel-body">
                <table class="table table-bordered" id="list_user">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Duration</th>
                            <th>Pass percent</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $stt = 1; ?> @foreach ($exams as $exam)
                        <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Delete</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>Are you sure you want to delete it</p>
                                    </div>
                                    <div class="modal-footer">
                                        <a href="{!! URL::route('admin.exam.getDelete', $exam['id']) !!}">
                                            <button type="button" class="btn btn-danger">Yes</button>
                                        </a>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <tr>
                            <td>{!! $stt++ !!}</td>
                            <td>{!! $exam['name'] !!}</td>
                            <td>{!! $exam['category']->name !!}</td>
                            <td>{!! $exam['duration'] !!}</td>
                            <td>{!! $exam['pass_percent'] !!}%</td>
                            <td>
                                @if($exam['active'] == "yes")
                                    <span class="label label-success">{!! "Active" !!}</span> 
                                @else
                                    <span class="label label-danger">{!! "No active" !!}</span> 
                                @endif
                            </td>
                            <td>
                                <a class="a_no_hover" href="{!! URL::route('admin.question.getAdd', $exam['id']) !!}">
                                    <button type="button" class="btn btn-info"><span class="glyphicon glyphicon-cog"></span> Manage Questions</button>
                                </a>
                                <a class="a_no_hover" href="{!! URL::route('admin.exam.getEdit', $exam['id']) !!}">
                                    <button type="button" class="btn btn-sm btn-warning"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                                </a>
                                <a class="a_no_hover" href="javascript:void(0);" data-target="#confirm-delete_{{ $exam['id'] }}" data-toggle="modal">
                                    <button type="button" class="btn btn-sm btn-danger">
                                        <span class="glyphicon glyphicon-trash"></span> Delete
                                    </button>
                                </a>
                                <div id="confirm-delete_{{ $exam['id'] }}" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Are you sure you want to delete this question ?</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form action="{{ route('admin.exam.getDelete', $exam['id']) }}" method="post" accept-charset="utf-8">
                                                    <div class="form-group center">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <input type="hidden" name="_method" value="GET">
                                                        <input type="submit" class="btn btn-danger" value="OK">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
