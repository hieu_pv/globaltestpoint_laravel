@extends ('admin')

@section('title')
    Blankpage - Globaltestpoint
@stop

@section ('content')
<div class="container" id="blankpageadmin">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <span class="glyphicon glyphicon-cog">
                </span>
                Add Blank Page
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3">
                        <a href="{{{ url('/admin/blankpage') }}}">
                        <button type="button" class="btn btn-primary" >
                            <i class="fa fa-backward" aria-hidden="true"></i>
                        Back
                        </button>
                        </a>
                    </div>
                </div>
            	<div class="form-group col-sm-12 col-md-12 center">
			        @if (count($errors) > 0)
					    <div class="alert alert-danger" style="text-align: left">
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					@endif
		        </div>
                <brr />
                <form accept-charset="utf-8" action="" enctype="multipart/form-data" method="post" role="form">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group">
                                <div class="col-sm-12 col-md-12 mgr-bottom15">
                                    <label class="control-label">
                                        <span class="required">
                                            *
                                        </span>
                                        Name Blankpage
                                    </label>
                                    <input class="form-control" name="name-blankpage" placeholder="Name of blankpage" type="text" value="{{ old('name-blankpage') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="for-parentcate col-sm-12 col-md-12 mgr-bottom15">
                                    <label class="control-label" for="parent">
                                        <span class="required">
                                            *
                                        </span>
                                        Belong to Category:
                                    </label>
                                    <select class="form-control" id="parent_id" name="parent_id">
                                        <option value="">
                                            -- Select --
                                        </option>
			                            @if(!empty($categorys))
				                        	@foreach( $categorys as $keycate => $cate)
                                        <option value="{{{ $cate['id'] }}}">
                                            {{{ $cate['name'] }}}
                                        </option>
                                        @endforeach
				                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 col-md-12 mgr-bottom15">
                                    <label class="control-label" for="parent">
                                        <span class="required">
                                            *
                                        </span>
                                        Page:
                                    </label>
                                    <select name="page" class="form-control">
                                        <option value="2" selected="">Test Yourself</option>
                                        <option value="3">You Want Done</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 col-md-12 mgr-bottom15">
                                    <label class="control-label" for="parent">
                                        <span class="required">
                                            *
                                        </span>
                                        Position on Page:
                                    </label>
                                    <select name="position" class="form-control">
                                        <option value="post" selected="">Belong to Post</option>
                                        <option value="homepage">Homepage</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 col-md-12 mgr-bottom15">
                                    <label class="control-label">
                                        Order
                                    </label>
                                    <input class="form-control" name="order" placeholder="0" type="text" value="{{ old('order') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 col-md-12 wraplinkleft mgr-bottom15" id="wraplinkleft">
                                    <label class="control-label">
                                        Left link
                                    </label>
                                    <textarea id="inp-leftlink" class="form-control" name="inp-leftlink" class="rich_editor"></textarea>
                                    <script>
                                        ckeditor('inp-leftlink');
                                    </script>
                                    {{-- <input class="form-control" name="inp-left[]" placeholder="Link" type="url" value="{{ old('inp-left[]') }}"> --}}
                                    <br />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class=" col-sm-12 col-md-12 wraplinkright mgr-bottom15" id="wraplinkright">
                                    <label class="control-label">
                                        Right link
                                    </label>
                                    <textarea id="inp-rightlink" class="form-control" name="inp-rightlink" class="rich_editor"></textarea>
                                    <script>
                                        ckeditor('inp-rightlink');
                                    </script>
                                    {{-- <input class="form-control" name="inp-right[]" placeholder="Link" type="url" value="{{ old('inp-right[]') }}"> --}}
                                    <br />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class=" col-sm-12 col-md-12 sublink mgr-bottom15" id="wraplinksub">
                                    <label class="control-label">
                                        Sub link
                                    </label>
                                    <textarea id="inp-sublink" class="form-control" name="inp-sublink" class="rich_editor"></textarea>
                                    <script>
                                        ckeditor('inp-sublink');
                                    </script>
                                    {{-- <input class="form-control" name="inp-sub[]" placeholder="Link" type="url" value="{{ old('inp-sub[]') }}"> --}}
                                    <br />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 col-md-12 for-photograp  mgr-bottom15">
                                    <label>Upload Banner: </label>
                                    <input type="file" id="inp-banner" name="inp-banner" multiple data-show-upload="false" data-show-caption="true" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 col-md-12 for-photograp mgr-bottom15">
                                    <label>
                                        Upload Photograp:
                                    </label>
                                    <input data-show-caption="true" data-show-upload="false" id="inp-photograp" name="inp-photograp" type="file">
                                    </input>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 col-md-12 mgr-bottom15">
                                    <label style="margin-top: 5px;">
                                        <input checked="" id="active_blank" name="active_blank" type="checkbox">
                                            Active
                                        </input>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 col-md-12 mgr-bottom15">
                                    <input name="_token" type="hidden" value="{{ csrf_token() }}">
                                        <input class="btn btn-primary" id="inp-photograp" name="inp-photograp" placeholder="" type="submit" value="Create">
                                        </input>
                                    </input>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="form-group for-link">
                                <div class="col-sm-12 col-md-12 for-introduction mgr-bottom15">
                                    <label>
                                        Introduction
                                    </label>
                                    <textarea id="inp-introduction" class="form-control" name="inp-introduction" class="rich_editor"></textarea>
                                    <script>
                                        ckeditor('inp-introduction');
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@stop
