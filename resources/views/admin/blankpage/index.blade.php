@extends ('admin')

@section('title')
    Globaltestpoint - Dashboard Category
@stop

@section ('content')
<div class="container">
	<div class="row">
		<div class="panel panel-default">
            <div class="panel-heading"><span class="glyphicon glyphicon-cog"></span> List Blank Page</div>
            <div class="panel-body">
                <div class="margin10">
                	<div class="form-group col-sm-6 center">
			        	@if(isset($result))
			        		@if($result)
			        		<div class="alert alert-success">
			                    <strong>Successful !!</strong> Deleted a Blank Page.
			                </div>
				            @else
				            	<div class="alert alert-danger">
				                    <strong>Error !!</strong> Not delete this Blank Page.
				                </div>
				            @endif
				        @endif
	        		</div>
	        		<br/>
                    <div class="padding5 clearfix"></div>
                    <div class="col-sm-12">
                    	<table id="list_blankpage" class="table table-striped table-bordered" cellspacing="0" width="100%">
							<thead>
								<tr>
					                <th>STT</th>
					                <th>Name Page</th>
					                <th>Active</th>
					                <th>Position</th>
					                <th>Parent Category</th>
					                <th>Page</th>
					                <th>Action</th>
					            </tr>
							</thead>
							<tbody>
							<?php $i = 0; ?>
							@if(isset($listblankpage))
								@foreach ($listblankpage as $keyblank => $blank)
								<tr id="{{{ $blank['id'] }}}">
					                <td>{{{ ++$i }}}</td>
					                <td>{{{ $blank['post_title'] }}}</td>
					                @if($blank['active'] == 1 || $blank['active'] == '1')
					                <td><span class="label label-success">Active</span></td>
					                @else
					                <td><span class="label label-danger">No Active</span></td>
					                @endif
					                <td>{{ $blank['position'] }}</td>
					                <td>{{ $blank['parent_name'] }}</td>
					                @if($blank['page'] == 2)
					                <td><span class="label label-success">TestYourself</span></td>
					                @elseif($blank['page'] == 3)
					                <td><span class="label label-danger">YouWantDone</span></td>
					                @endif
					                <td>
					                	<a href="{{{ url('admin/blankpage/edit') }}}/{{{ $blank['id'] }}}">
                                            <button type="button" class="btn btn-sm btn-warning"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                                        </a>
                                        <a href="javascript:void(0);" data-target="#confirm-delete_{{{ $blank['id'] }}}" data-toggle="modal" data-href="{{{ url('admin/blankpage/delete/') }}}/{{{ $blank['id'] }}}">
                                            <button stt="{{{ $blank['id'] }}}" type="button" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                                        </a>
                                        <div id="confirm-delete_{{{ $blank['id'] }}}" class="modal fade" role="dialog">
										    <div class="modal-dialog">
											    <!-- Modal content-->
											    <div class="modal-content">
											        <div class="modal-header">
											            <button type="button" class="close" data-dismiss="modal">&times;</button>
											            <h4 class="modal-title">Are you sure to delete this blankpage ?</h4>
											        </div>
											        <div class="modal-body">
											        <form action="" method="post" accept-charset="utf-8">
											        	<div class="form-group center">
											        		<input type="hidden" name="id_delete_blank" value="{{{ $blank['id'] }}}">
											        		<input type="hidden" name="_token" value="{{ csrf_token() }}">
											        		<input type="submit" class="btn btn-danger" value="OK">
											            	<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
											            </div>
											        </form>
											        </div>
											    </div>
										    </div>
										</div>
					                </td>
					            </tr>
					            @endforeach
					        @endif
							</tbody>
						</table>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>
@stop
