@extends ('admin')

@section('title')
    Blankpage - Globaltestpoint
@stop

@section ('content')
<div class="container" id="blankpageadmin">
	<div class="row">
		<div class="panel panel-default">
            <div class="panel-heading"><span class="glyphicon glyphicon-cog"></span> Edit Blank Page</div>
            <div class="panel-body">
            	<div class="row">
                    <div class="col-sm-3">
                        <a href="{{{ url('/admin/blankpage') }}}">
                        <button type="button" class="btn btn-primary" >
                            <i class="fa fa-backward" aria-hidden="true"></i>
                        Back
                        </button>
                        </a>
                    </div>
                </div>
            	@include('blocks.error')
            	@if(isset($blank))
            	<form action="" method="post" accept-charset="utf-8" role="form" enctype="multipart/form-data">
            		<div class="row">
            			<div class="col-sm-12 col-md-6">
            				<div class="form-group ">
	            				<div class="nameblank col-sm-12 col-md-12  mgr-bottom15">
	            					<label for="name_blankpage"><span class="required">* </span>Name Blank Page: </label>
            					    <input type="text" name="name-blankpage" class="form-control" id="name_blankpage" value="{{{ $blank['nameblank'] }}}" placeholder="Name Blank Page">
	            				</div>
	            			</div>
	            			<div class="form-group">
	            				<div class="for-parentcate col-sm-12 col-md-12 mgr-bottom15">
	            					<label for="parent" class="control-label"><span class="required">* </span>Parent Post: </label>
			                        <select id="parent_id" name="parent_id" class="form-control">
			                        	<option value=""> -- Select --</option>}
			                        	option
			                            @if(!empty($categories))
				                        	@foreach( $categories as $keycate => $cate)
					                        	@if($blank['post_id'] != $cate['id'])
					                            <option value="{{{ $cate['id'] }}}" {{{ ($blank['parent_cate_id'] == $cate['id']) ? 'selected' : '' }}}>{{{ $cate['name'] }}}</option>
					                            @endif
				                            @endforeach
				                        @endif
			                        </select>
	            				</div>
			                </div>
			                <div class="form-group">
                                <div class="col-sm-12 col-md-12 mgr-bottom15">
                                    <label class="control-label" for="parent">
                                        <span class="required">
                                            *
                                        </span>
                                        Follow Page:
                                    </label>
                                    <select name="page" class="form-control">
                                        <option value="2" {{ ($blank['page'] == 2) ? "selected" : '' }}>Test Yourself</option>
                                        <option value="3" {{ ($blank['page'] == 3) ? "selected" : '' }}>You Want Done</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 col-md-12 mgr-bottom15">
                                    <label class="control-label" for="parent">
                                        <span class="required">
                                            *
                                        </span>
                                        Position on Page:
                                    </label>
                                    <select name="position" class="form-control">
                                        <option value="post" {{ ($blank['position'] == "post") ? "selected" : '' }}>Belong to Post</option>
                                        <option value="homepage" {{ ($blank['position'] == "homepage") ? "selected" : '' }}>Homepage</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12 col-md-12 mgr-bottom15">
                                    <label class="control-label">
                                        Order
                                    </label>
                                    <input class="form-control" name="order" placeholder="0" type="text" value="{{{ $blank->post->order }}}">
                                </div>
                            </div>
	            			<div class="form-group">
	            				<div class="col-sm-12 col-md-12 wraplinkleft  mgr-bottom15" id="wraplinkleft_edit">
	        						<label >Left link</label>
	        						{{-- @foreach($blank->arr_leftlink as $keyleft => $left)
	        						@if(!empty($left))
	        						<input type="text" class="form-control" name="inp-left[]" value="{{{ $left }}}" placeholder="Link">
	        						<br />
	        						@endif
	        						@endforeach --}}
	        						<textarea id="inp-leftlink" class="form-control" name="inp-leftlink" class="rich_editor">{{ $blank->leftlink }}</textarea>
                                    <script>
                                        ckeditor('inp-leftlink');
                                    </script>
	            				</div>
	            			</div>
	            			<div class="form-group">
	            				<div class=" col-sm-12 col-md-12 wraplinkright  mgr-bottom15" id="wraplinkright_edit">
	        						<label >Right link</label>
	        						{{-- @foreach($blank->arr_rightlink as $keyright => $right)
	        						@if(!empty($right))
	        						<input type="text" class="form-control" name="inp-right[]" value="{{{ $right }}}" placeholder="Link">
	        						<br />
	        						@endif
	        						@endforeach --}}
	        						<textarea id="inp-rightlink" class="form-control" name="inp-rightlink" class="rich_editor">{{ $blank->rightlink }}</textarea>
                                    <script>
                                        ckeditor('inp-rightlink');
                                    </script>
	            				</div>
	            			</div>
	            			<div class="form-group">
	            				<div class=" col-sm-12 col-md-12 sublink  mgr-bottom15" id="wraplinksub_edit">
	            					<label >Sub link</label>
	            					{{-- @foreach($blank->arr_sublink as $keysub => $sub)
	            					@if(!empty($sub))
	        						<input type="text" class="form-control" name="inp-sub[]" value="{{{ $sub }}}" placeholder="Link">
	        						<br />
	        						@endif
	        						@endforeach --}}
	        						<textarea id="inp-sublink" class="form-control" name="inp-sublink" class="rich_editor">{{ $blank->sublink }}</textarea>
                                    <script>
                                        ckeditor('inp-sublink');
                                    </script>
	            				</div>
	            			</div>
	            			@if(!empty($blank['image_cover']))
	            			{{-- <div class="form-group">
            					<div class="col-sm-12 col-md-12 for-photograp  mgr-bottom15">
		            				<label>Banner: </label>
				            		<input type="hidden" class="form-control" name="tmp-banner" value="{{{ $blank['image_cover'] }}}" placeholder="" readonly="">
				            		<img src="{!! asset($blank['image_cover']) !!}" alt="" width="100%" height="200px">
				            	</div>
	            			</div> --}}
	            			{{-- <input type="hidden" class="form-control" name="tmp-banner" value="{{{ $blank['image_cover'] }}}" placeholder="" readonly=""> --}}
	            			@endif
	            			<div class="form-group">
            					<div class="col-sm-12 col-md-12 for-photograp  mgr-bottom15">
		            				<label>Upload New Banner: </label>
				            		<input type="file" id="inp-banner" name="inp-banner"  data-show-upload="false" data-show-caption="true">
				            	</div>
	            			</div>
	            			<div class="form-group">
	            				<div class="col-sm-12 col-md-12 mgr-bottom15">
	            					<label for="">
	            						<input type="checkbox" name="remove-cover" value="on">
	            						Remove Cover
	            					</label>
	            				</div>
	            			</div>
	            			{{-- @if(!empty($blank['image']))
	            			<div class="form-group">
            					<div class="col-sm-12 col-md-12 for-photograp  mgr-bottom15">
		            				<label>Photograp: </label>
				            		<input type="hidden" class="form-control" name="tmp-photograp" value="{{{ $blank['image'] }}}" placeholder="" readonly="">
				            		<img src="{!! asset($blank['image']) !!}" alt="" width="200px" height="200px">
				            	</div>
	            			</div>
	            			<input type="hidden" class="form-control" name="tmp-photograp" value="{{{ $blank['image'] }}}" placeholder="" readonly="">
	            			@endif --}}
	            			<div class="form-group">
            					<div class="col-sm-12 col-md-12 for-photograp  mgr-bottom15">
		            				<label>Upload New Photograp: </label>
				            		<input type="file" id="inp-photograp" name="inp-photograp" data-show-upload="false" data-show-caption="true">
				            	</div>
	            			</div>
	            			<div class="form-group">
	            				<div class="col-sm-12 col-md-12 mgr-bottom15">
	            					<label for="">
	            						<input type="checkbox" name="remove-photograp" value="on">
	            						Remove Photograp
	            					</label>
	            				</div>
	            			</div>
	            			<div class="form-group">
	            				<div class="col-sm-12 col-md-12 mgr-bottom15">
		            				<label style="margin-top: 5px;">
		                            	<input type="checkbox" {{{ ($blank['active'] == 1) ? 'checked' : '' }}} name="active_blank" id="active_blank"> Active
		                        	</label>
		                        </div>
	            			</div>
	            			<div class="form-group">
            					<div class="col-sm-12 col-md-12 mgr-bottom15">
            						<input type="hidden" name="_token" value="{{ csrf_token() }}">
				            		<input type="submit" class="btn btn-primary" value="Update" placeholder="">
				            	</div>
	            			</div>
		            	</div>
		            	<div class="col-sm-12 col-md-6">
		            		<div class="form-group for-link">
	            				<div class="col-sm-12 col-md-12 for-introduction  mgr-bottom15">
				            		<label>Introduction</label>
				            		<textarea name="inp-introduction" class="rich_editor form-control" id="inp-introduction">{{{ $blank['content'] }}}</textarea>
				            		<script>
                                        ckeditor('inp-introduction');
                                    </script>
		            			</div>
			            	</div>
		            	</div>
            		</div>
            	</form>
            	@endif
            </div>
        </div>
    </div>
</div>
@if(!empty($blank['image']))
<script type="text/javascript">
	$("#inp-photograp").fileinput({
        'showUpload': false,
        'showRemove': false,
        initialPreview: [
            "<img src='{{ asset($blank['image']) }}' class='file-preview-image img-responsive img-thumbnail' alt='{{ $blank['nameblank'] }}' title='{{ $blank['nameblank'] }}'>",
        ],
    });
</script>
@endif
@if(!empty($blank['image_cover']))
<script type="text/javascript">    
    $("#inp-banner").fileinput({
        'showUpload': false,
        'showRemove': false,
        initialPreview: [
            "<img src='{{ asset($blank['image_cover']) }}' class='file-preview-image img-responsive img-thumbnail' alt='{{ $blank['nameblank'] }}' title='{{ $blank['nameblank'] }}'>",
        ],
    });
</script>
@endif
@stop
