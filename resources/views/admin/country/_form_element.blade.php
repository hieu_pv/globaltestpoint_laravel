<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label for="name"><span class="required">*</span> Name</label>
            <input type="text" name="name" class="form-control" id="name" placeholder="Name" value="{{ isset($country['name']) && $country['name'] != null ? $country['name'] : old('name') }}">
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label for="order">Order</label>
            <input type="text" name="order" class="form-control" id="order" placeholder="Order" value="{{ isset($country['order']) && $country['order'] != null ? $country['order'] : 0 }}">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label for="code"><span class="required">*</span> Code</label>
            <input type="text" name="code" class="form-control" id="code" placeholder="Code" value="{{ isset($country['code']) && $country['code'] != null ? $country['code'] : old('code') }}">
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label for="phone_area_code"> Phone Area Code</label>
            <input type="text" name="phone_area_code" class="form-control" id="phone_area_code" placeholder="Phone Area Code" value="{{ isset($country['phone_area_code']) && $country['phone_area_code'] != null ? $country['phone_area_code'] : old('phone_area_code') }}">
        </div>
    </div>
</div>
<div class="form-group">
    <label for="active">Active</label>
    <input name="active" id="active" type="checkbox" value="1" {{ isset($country[ 'active']) && $country[ 'active']==1 ? 'checked' : 'checked' }}></input>
</div>
<button type="submit" class="btn btn-primary">{{ isset($buttonTitle) && $buttonTitle != null ? $buttonTitle : 'Submit' }}</button>