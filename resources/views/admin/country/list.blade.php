@extends ('admin') 

@section('title') 
    {{$title}} 
@stop 

@section ('content')
<div class="container country-container">
    <div class="row">
        <a href="{{ url('admin/country/create') }}" class="add-new pull-right btn btn-primary margin-bottom-15"><i class="glyphicon glyphicon-plus"></i> Add new</a>
    </div>
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
            	<span><i class="glyphicon glyphicon-cog"></i> {{$tableTitle}}</span> 
            </div>
            <div class="panel-body">
                <div class="margin10">
                    <div class="col-sm-12">
                        <table id="list_country" class="table table-striped table-bordered data-table" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    @if(!empty($countries) && count($countries) > 0)
                                    <th class="text-center">STT</th>
                                    <th>Name</th>
                                    <th class="text-center">Code</th>
                                    <th class="text-center">Phone Area Code</th>
                                    <th class="text-center">Order</th>
                                    <th class="text-center">Active</th>
                                    <th>Action</th>
                                    @else
                                    <th colspan="5" class="no-record">You don't have any record</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 0; ?> @if(!empty($countries)) @foreach ($countries as $country)
                                <tr id="{{ $country['id'] }}">
                                    <td class="text-center">{{ ++$i }}</td>
                                    <td>{{ $country['name'] }}</td>
                                    <td class="text-center">{{ $country['code'] }}</td>
                                    <td class="text-center">{{ $country['phone_area_code'] }}</td>
                                    <td class="text-center">{{ $country['order'] }}</td>
                                    @if($country['active'] == 1)
                                    <td class="text-center"><span class="label label-success">Active</span></td>
                                    @else
                                    <td class="text-center"><span class="label label-danger">No Active</span></td>
                                    @endif
                                    <td>
                                        <a class="a_no_hover" href="{{ url('admin/country/'. $country->id . '/edit') }}">
                                            <button type="button" class="btn btn-sm btn-warning">
                                            	<span class="glyphicon glyphicon-edit"></span> Edit
                                            </button>
                                        </a>
                                        <a class="a_no_hover" href="javascript:void(0);" data-target="#confirm-delete_{{ $country['id'] }}" data-toggle="modal">
                                            <button stt="{{ $country['id'] }}" type="button" class="btn btn-sm btn-danger">
                                            	<span class="glyphicon glyphicon-trash"></span> Delete
                                            </button>
                                        </a>
                                        <div id="confirm-delete_{{ $country['id'] }}" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 class="modal-title">Are you sure to delete this country ?</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form action="{{ url('admin/country', $country['id']) }}" method="post" accept-charset="utf-8">
                                                            <div class="form-group center">
                                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                <input type="hidden" name="_method" value="DELETE">
                                                                <input type="submit" class="btn btn-danger" value="OK">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
