@extends('admin') @section('title') Update Question @stop @section('content') @include('_include_angular')
<div id="add_exams" ng-app="app" ng-controller="UpdateQuestionCtrl as vm">
    <div class="container edit_users-admin update-question-container" ng-init="question_id = {{$question['id']}}">
        <div ng-show="vm.flashMessage" ng-class="vm.flashClass" class="error-angular alert">
            <ul>
                <li ng-bind="vm.flashMessage"></li>
            </ul>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading"><span class="glyphicon glyphicon-edit"></span> Update Question</div>
            <div class="container_test_input panel-footer container">
                <div class="col-md-12">
                    <div class="list_Exam_in_question">
                        @if ($question != null)
                        <form class="form-horizontal form-questions" name="vm.updateQuestionForm" method="post" ng-submit="vm.updateQuestionForm.$valid && vm.updateQuestion(vm.question)">
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-sm-2">Exam</label>
                                <div class="col-xs-12 col-sm-10">
                                    <select ng-model="vm.question.exam" ng-options="option as option.name for option in vm.exams" class="form-control"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-sm-2">Question</label>
                                <div class="col-xs-12 col-sm-10">
                                    <div ng-show="vm.question.type === 1">
                                        <textarea ckeditor="" ng-model="vm.question.question"></textarea>
                                    </div>
                                    <div ng-show="vm.question.image && vm.question.type === 2">
                                        <img ng-src="@{{asset(vm.question.image)}}" class="img-responsive img-thumbnail">
                                    </div>
                                </div>
                            </div>
                            <div class="question" ng-show="vm.errorUploadMsg && vm.question.type === 2">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-10 col-sm-offset-2">
                                        <div class="error" ng-bind="vm.errorUploadMsg"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="question">
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-10 col-sm-offset-2">
                                        <div class="pull-left" ng-show="vm.question.type === 1">
                                            <a class="btn btn-success question-type question-tpe-image" ng-click="vm.question.type = 2">Question Image</a>
                                        </div>
                                        <div class="margin-bottom-10" ng-show="vm.question.type === 2">
                                            <button class="btn-width upload-image btn btn-success" type="button" dropzone model-name="vm.question.image" error-message="vm.errorUploadMsg" folder-upload="questions">@{{ vm.question.image ? 'Change Image' : 'Upload Image' }}</button>
                                        </div>
                                        <div ng-show="vm.question.type === 2">
                                            <a class="btn-width btn btn-success question-type question-tpe-text" ng-click="vm.question.type = 1">Question Text</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-xs-12 col-sm-2">Solution</label>
                                <div class="col-xs-12 col-sm-10">
                                    <textarea ckeditor="" ng-model="vm.question.solution"></textarea>
                                </div>
                            </div>
                            <div class="form-group" ng-if="vm.question.answers && vm.question.answers.data.length > 0" ng-repeat="answer in vm.question.answers.data">
                                <label class="control-label col-xs-12 col-sm-2">Option @{{$index + 1}}</label>
                                <div class="col-xs-12 col-sm-10">
                                    <textarea ckeditor="" ng-model="answer.answer"></textarea>
                                </div>
                                <div class="col-xs-12 col-sm-10 col-sm-offset-2">
                                    <div class="choose-remove">
                                        <div class="pull-left">
                                            <input type="checkbox" id="is-correct-@{{$index}}" ng-model="answer.is_correct">
                                            <label for="is-correct-@{{$index}}">Choose if this answer is correct</label>
                                        </div>
                                        <div class="pull-right">
                                            <a class="btn btn-danger" ng-click="vm.removeAnswer(answer)">Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <div class="pull-right">
                                        <a class="btn btn-default" ng-click="vm.addMoreAnswer(vm.question)">Add Answer</a>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group add_question_new">
                                <div class="col-xs-10 col-xs-offset-2">
                                    <a href="{!! URL::route('admin.exam.lists') !!}" class="btn btn-warning">
                                        <i class="glyphicon glyphicon-backward"></i> Back
                                    </a>
                                    @if ($next != null)
                                    <a href="{!! url('admin/question/update', $next) !!}" class="btn btn-success col-xs-offset-6">
                                        Next <i class="glyphicon glyphicon-forward"></i>
                                    </a>
                                    @endif
                                    <button ng-disabled="vm.inProgress" type="submit" class="btn btn-primary pull-right">Update</button>
                                </div>
                            </div>
                        </form>
                        @else
                        <div class="custom-alert alert-danger" role="alert">Question doen't exist</div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
