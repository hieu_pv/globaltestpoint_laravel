@extends ('admin') @section('title') {{$title}} @stop @section ('content')
<div class="container profile-container">
    <div class="row">
        <h1 class="profile-title">Manage Profile</h1>
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="glyphicon glyphicon-edit">
                </i> {{ $tableTitle }}
            </div>
            <div class="panel-body">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form class="form-horizontal profile-form" accept-charset="utf-8" action="{{ route('admin.profile') }}" method="post" role="form">
                    <input type="hidden" name="_method" value="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-6">
                            <span>{!! $admin['email'] !!}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="first_name" class="col-sm-2 control-label"><span class="required">*</span> First Name</label>
                        <div class="col-sm-6">
                            <input type="text" name="first_name" class="form-control" id="first_name" placeholder="First name" value="{{ isset($admin['first_name']) && $admin['first_name'] != null ? $admin['first_name'] : old('first_name') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="last_name" class="col-sm-2 control-label"><span class="required">*</span> Last Name</label>
                        <div class="col-sm-6">
                            <input type="text" name="last_name" class="form-control" id="last_name" placeholder="First name" value="{{ isset($admin['last_name']) && $admin['last_name'] != null ? $admin['last_name'] : old('last_name') }}">
                        </div>
                    </div>
                    <div class="well well-sm">Leave password blank, if you dont want to update password.</div>
                    <div class="form-group">
                        <label for="password" class="col-sm-2 control-label"> New Password</label>
                        <div class="col-sm-6">
                            <input type="password" name="password" class="form-control" id="password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password_confirmation" class="col-sm-2 control-label"> Confirm Password</label>
                        <div class="col-sm-6">
                            <input type="password" name="password_confirmation" class="form-control" id="password_confirmation">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-6">
                            <button type="submit" class="btn btn-profile">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@stop
