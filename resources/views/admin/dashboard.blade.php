@extends ('admin')

@section('title')
    Globaltestpoint - Dashboard Admin
@stop

@section ('content')
<div class="content_admin admin_dashboard">
    <div class="content_admin_table">
        <div class="container">
            <h3 class="title_h3">Admin Dashboard</h3>
            <div class="dashboard_admin">
                <img src="{!! asset('assets/frontend/images/dashboard_pic.jpg') !!}" alt="">
            </div>
        </div>
    </div>
</div>
@stop