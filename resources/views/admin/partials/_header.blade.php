<div class="admin_home">
    <div class="admin_home_container">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{!! url('admin') !!}"><span class="fa fa-home"></span> Home</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Modules  <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="{!! url('admin/category') !!}"><span class="glyphicon glyphicon-cog"></span> Manage Category</a></li>
                                <li><a href="{!! url('admin/add-category') !!}"><span class="glyphicon glyphicon-plus-sign"></span> Add Category</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{!! url('admin/exam/lists') !!}"><span class="glyphicon glyphicon-cog"></span> Manage Exams</a></li>
                                <li><a href="{!! url('admin/exam/add') !!}"><span class="glyphicon glyphicon-plus-sign"></span> Add Exams</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{{{ url('admin/blankpage') }}}"><span class="glyphicon glyphicon-cog"></span> Manage Blank Pages</a></li>
                                <li><a href="{{{ url('admin/blankpage/add') }}}"><span class="glyphicon glyphicon-plus-sign"></span> Add Pages</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{{{ url('admin/videoflash') }}}"><span class="glyphicon glyphicon-cog"></span> Manager Video Flash Pages</a></li>
                                <li><a href="{{{ url('admin/videoflash/add') }}}"><span class="glyphicon glyphicon-plus-sign"></span> Add Video Falsh Pages</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{!! url('admin/country') !!}"><span class="glyphicon glyphicon-cog"></span> Manage Country</a></li>
                                <li><a href="{!! url('admin/country/create') !!}"><span class="glyphicon glyphicon-plus-sign"></span> Add Country</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{!! url('admin/state') !!}"><span class="glyphicon glyphicon-cog"></span> Manage State</a></li>
                                <li><a href="{!! url('admin/state/create') !!}"><span class="glyphicon glyphicon-plus-sign"></span> Add State</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{!! url('admin/document') !!}"><span class="glyphicon glyphicon-cog"></span> Manage Document</a></li>
                                <li><a href="{!! url('admin/document/create') !!}"><span class="glyphicon glyphicon-plus-sign"></span> Add Document</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Users  <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="{!! url('admin/users/lists') !!}"><span class="glyphicon glyphicon-cog"></span> List</a></li>
                                <li><a href="{!! url('admin/users/create') !!}"><span class="glyphicon glyphicon-cog"></span> Create</a></li>
                            </ul>
                        </li>
                        <li><a class="dropdown-toggle" href="{!! url('admin/partners') !!}">Partners</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">School  <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="{!! url('admin/school_type') !!}"><span class="glyphicon glyphicon-cog"></span> Manage School Type</a></li>
                                <li><a href="{!! url('admin/school_degree') !!}"><span class="glyphicon glyphicon-cog"></span> Manage School Degree</a></li>
                                <li><a href="{!! url('admin/school') !!}"><span class="glyphicon glyphicon-cog"></span> Manage School</a></li>
                            </ul>
                        </li>
                        <li><a class="dropdown-toggle" href="{!! url('admin/exam-result') !!}">Exams Results</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Option Page<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="{!! url('admin/slider') !!}"><span class="glyphicon glyphicon-cog"></span> Manage Slider</a></li>
                                <li><a href="{!! url('admin/slider/add') !!}"><span class="glyphicon glyphicon-cog"></span> Add Slider</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{!! url('admin/faq') !!}"><span class="glyphicon glyphicon-cog"></span> FAQ</a></li>
                                <li><a href="{!! url('admin/option') !!}"><span class="glyphicon glyphicon-cog"></span> Config Website</a></li>
                                <li><a href="{!! url('admin/maintenance') !!}"><span class="glyphicon glyphicon-cog"></span> Maintenance Website</a></li>
                            </ul>
                        </li>
                        <li><a class="dropdown-toggle" href="{!! url('admin/job-listing/dashboard') !!}">Job Listing</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span> Admin <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="{!! url('admin/profile') !!}">Manage Profile</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="{!! url('admin/logout') !!}"><span class="glyphicon glyphicon-off"></span> Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>
        <div class="message container">
            <div class="row">
                @if (Session::has('flash_message'))
                <div class="alert alert-{!! Session::get('flash_level') !!}">
                    {!! Session::get('flash_message') !!}
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
