<div class="form-group">
    <label for="country"><span class="required">*</span> Country name</label>
    <select name="country_id" id="country" class="form-control">
    	<option value=""></option>
    	@if (!empty($countries))
    	@foreach($countries as $country)
    	<option value="{{$country['id']}}" {{ isset($state['country_id']) && $state['country_id'] == $country['id'] ? 'selected' : '' }} >{{$country['name']}}</option>
    	@endforeach
    	@endif
    </select>
</div>
<div class="form-group">
    <label for="name"><span class="required">*</span> Name</label>
    <input type="text" name="name" class="form-control" id="name" placeholder="Name" value="{{ isset($state['name']) && $state['name'] != null ? $state['name'] : old('name') }}">
</div>
<div class="form-group">
    <label for="order">Order</label>
    <input type="text" name="order" class="form-control" id="order" placeholder="Order" value="{{ isset($state['order']) && $state['order'] != null ? $state['order'] : 0 }}">
</div>
<div class="form-group">
    <label for="active">Active</label>
    <input name="active" id="active" type="checkbox" value="1" {{ isset($state['active']) && $state['active'] == 1 ? 'checked' : 'checked' }}></input>
</div>
<button type="submit" class="btn btn-primary">{{ isset($buttonTitle) && $buttonTitle != null ? $buttonTitle : 'Submit' }}</button>
