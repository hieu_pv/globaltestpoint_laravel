@extends ('admin') @section('title') Add Category @stop @section ('content')
<div class="container index_category">
    @include('blocks/error')
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading"><span class="glyphicon glyphicon-cog"></span>Add New Category</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3">
                        <a href="{{{ url('/admin/category') }}}">
                            <button type="button" class="btn btn-primary">
                                <i class="fa fa-backward" aria-hidden="true"></i> Back
                            </button>
                        </a>
                    </div>
                </div>
                <br /> @if(isset($result))
                <div class="form-group col-sm-12 col-md-12 center">
                    @if($result)
                    <div class="alert alert-success">
                        <strong>Successful !!</strong> Added a new Category.
                    </div>
                    @else
                    <div class="alert alert-danger">
                        <strong>Error !!</strong> Not add a Category.
                    </div>
                    @endif @if (count($errors) > 0)
                    <div class="alert alert-danger" style="text-align: left">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>
                @endif
                <form action="" method="post" role="form" class="form-horizontal" accept-charset="utf-8">
                    <fieldset>
                        <div class="form-group">
                            <label for="parent" class="col-sm-12 col-md-3 control-label">Category Parent: </label>
                            <div class="col-sm-12 col-md-6">
                                <select id="parent" name="parent_id" class="form-control">
                                    <option value="">Please choose Category</option>
                                    <?php category_parent($categories, 0, '--', 0); ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-sm-12 col-md-3 control-label"><span class="required">*</span> Category Name: </label>
                            <div class="col-sm-12 col-md-6">
                                <input type="text" value="{!! old('name') !!}" class="form-control" name="name" autocomplete="off" id="name" placeholder="Category Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="type" class="col-sm-12 col-md-3 control-label"><span class="required">*</span> Type: </label>
                            <div class="col-sm-12 col-md-6">
                                <select id="type" name="type" class="form-control">
                                    <option data-name="" value="">Please select a type</option>
                                    @foreach($types as $type)
                                    <option data-name="{!! $type->name !!}" value="{!! $type->name !!}">{!! $type->name !!}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="page" class="col-sm-12 col-md-3 control-label"><span class="required">*</span> Belong to Page: </label>
                            <div class="col-sm-12 col-md-6">
                                <select id="page" name="page_id" class="form-control">
                                    <option value="2">Test Your Self</option>
                                    <option value="3">What Do You Want done</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="order" class="col-sm-12 col-md-3 control-label"> Order: </label>
                            <div class="col-sm-12 col-md-6">
                                <input type="text" value="0" class="form-control" name="order" id="order">
                            </div>
                        </div>
                        <div class="form-group intro_exams_container">
                            <label for="order" class="col-sm-12 col-md-3 control-label"> Introduction and instruction on the exams: </label>
                            <div class="col-sm-12 col-md-6">
                                <textarea class="rich_editor" name="introduction_exam" id="introduction_exam"></textarea>
                                <script>
                                    ckeditor('introduction_exam');
                                </script>
                            </div>
                        </div>
                        <div class="form-group position-cate">
                            <label for="order" class="col-sm-12 col-md-3 control-label"> Choice position : </label>
                            <div class="col-sm-12 col-md-6">
                                <lable><input type="radio" name="position" value="0" checked> None</lable> &nbsp &nbsp
                                <lable><input type="radio" name="position" value="1"> Footer column 1</lable>
                                &nbsp &nbsp
                                <lable><input type="radio" name="position" value="2"> Footer column 2</lable>
                                &nbsp &nbsp
                                <lable><input type="radio" name="position" value="3"> Footer column 3</lable>
                            </div>
                        </div>
                        <div class="form-group link-cate">
                            <label for="inp_link" class="col-sm-12 col-md-3 control-label"> Link url: </label>
                            <div class="col-sm-12 col-md-6">
                                <input type="text" name="link" class="inp_link form-control" id="inp_link">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="is_active" class="col-sm-12 col-md-3 control-label">Status: </label>
                            <div class="col-sm-12 col-md-9">
                                <label style="margin-top: 5px;">
                                    <input type="checkbox" value="1" checked name="active" id="active"> Active
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="col-sm-4 col-md-4 col-sm-offset-3 col-md-offset-3">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.intro_exams_container').addClass('hide'); 
        $('#type').change(function(){
            var val_name = $('#type option:selected').val();
            console.log(val_name);
            if(val_name == 'Exam') {
                $('.intro_exams_container').removeClass('hide');
            } else {
                $('.intro_exams_container').addClass('hide');
            }
        });
    });
</script>
@stop
