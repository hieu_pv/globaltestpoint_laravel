@extends ('admin') @section('title') Globaltestpoint - Dashboard Category @stop @section ('content')
<div class="container">
    <div class="row">
        @include('blocks/error')
        <div class="panel panel-default">
            <div class="panel-heading"><span class="glyphicon glyphicon-cog"></span> Category Listing</div>
            <div class="panel-body">
                <div class="margin10">
                    <div class="col-sm-12">
                        <table id="list_category" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Category name</th>
                                    <th>Status</th>
                                    <th>Parent</th>
                                    <th>Page</th>
                                    <th>Type</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $stt = 0; ?> @if(isset($categories)) @foreach ($categories as $category)
                                <tr>
                                    <td>{!! ++$stt !!}</td>
                                    <td>{{{ $category['name'] }}}</td>
                                    @if($category['active'] == 1)
                                    <td><span class="label label-success">Active</span></td>
                                    @else
                                    <td><span class="label label-danger">No Active</span></td>
                                    @endif
                                    <td>
                                        @if($category['parent_id'] == 0) {!! "PARENT" !!} @else {!! $category->parent_name !!} @endif
                                    </td>
                                    @if($category['page_id'] == 2)
                                    <td><span class="label label-success">Testyourself</span></td>
                                    @else
                                    <td><span class="label label-danger">You Want done</span></td>
                                    @endif
                                    <td>{{{ $category['type'] }}}</td>
                                    <td>
                                        <a href="{{{ url('admin/edit-category/') }}}/{{{ $category['id'] }}}">
                                            <button type="button" class="btn btn-sm btn-warning"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                                        </a>
                                        <a onclick="return xacnhanxoa('Are you sure you want to delete it')" href="{!! URL::route('admin.category.destroy', $category['id']) !!}">
                                            <button data-toggle="modal" data-target="#myModal" type="button" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                                        </a>
                                    </td>
                                </tr>
                                @endforeach @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
