@extends ('admin') @section('title') Add Video or Falsh - Globaltestpoint @stop @section ('content')
<div class="container">
    <form action="" method="post" enctype="multipart/form-data" role="form">
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="glyphicon glyphicon-cog">
                        </span> Add Video
                    </div>
                    <div class="panel-body">
                        <div class="margin10">
                            <div class="form-group col-sm-12 center">
                                @if(isset($result)) @if($result)
                                <div class="alert alert-success">
                                    <strong>
                                        Successful !!
                                    </strong> {{{ $message }}}
                                </div>
                                @else
                                <div class="alert alert-danger">
                                    <strong>
                                        Error !!
                                    </strong> {{{ $message }}}
                                </div>
                                @endif @endif @include('blocks.error')
                            </div>
                            <br/>
                            <div class="padding5 clearfix">
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>
                                        Link youtube:
                                    </label>
                                    <input class="form-control" id="inp_video" name="inp_video" placeholder="" type="text" value="">
                                </div>
                                <div class="form-group">
                                    <label>
                                        Choose page:
                                    </label>
                                    <select name="page_video" id="page" class="form-control">
                                        <option value="0">Test Yourself</option>
                                        <option value="1">What you do want done?</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>
                                        Active:
                                    </label>
                                    <input type="checkbox" name="activeflash" id="activeflash" checked="">
                                </div>
                                <div class="form-group left">
                                    <input class="btn btn-sm btn-primary" type="submit" name="addvideo" value="Add Video">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="glyphicon glyphicon-cog">
                        </span> Add Flash
                    </div>
                    <div class="panel-body">
                        <div class="margin10">
                            <div class="form-group col-sm-12 center">
                                @if(isset($result2)) @if($result2)
                                <div class="alert alert-success">
                                    <strong>
                                        Successful !!
                                    </strong> {{{ $message2 }}}
                                </div>
                                @else
                                <div class="alert alert-danger">
                                    <strong>
                                        Error !!
                                    </strong> {{{ $message2 }}}
                                </div>
                                @endif @endif
                            </div>
                            <br/>
                            <div class="padding5 clearfix">
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>
                                        Link:
                                    </label>
                                    <input class="form-control" id="inp_flash" name="inp_flash" placeholder="" type="file" value="">
                                </div>
                                <div class="form-group">
                                    <label>
                                        Choose page:
                                    </label>
                                    <select name="page_flash" class="form-control">
                                        <option value="0">Test Yourself</option>
                                        <option value="1">What you do want done?</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>
                                        Active:
                                    </label>
                                    <input type="checkbox" name="activevideo" id="activevideo" checked="">
                                </div>
                                <div class="form-group left">
                                    <input class="btn btn-sm btn-primary" type="submit" name="addflash" value="Add Flash">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input name="_token" type="hidden" value="{{ csrf_token() }}">
    </form>
</div>
<script>
$('#inp_flash').fileinput();
</script>
@stop
