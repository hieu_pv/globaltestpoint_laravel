@extends ('admin')

@section('title')
    Globaltestpoint - Dashboard Video Flash
@stop

@section ('content')
<div class="container">
    <form accept-charset="utf-8" action="" method="post">
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="glyphicon glyphicon-cog">
                        </span>
                        Edit for TEST YOURSELF
                    </div>
                    <div class="panel-body">
                        <div class="margin10">
                            <div class="form-group col-sm-12 center">
                                @if(isset($result))
			        		@if($result)
                                <div class="alert alert-success">
                                    <strong>
                                        Successful !!
                                    </strong>
                                    Deleted .
                                </div>
                                @else
                                <div class="alert alert-danger">
                                    <strong>
                                        Error !!
                                    </strong>
                                    Don't delete.
                                </div>
                                @endif
				        @endif
                            </div>
                            <br/>
                            <div class="padding5 clearfix">
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label>
                                        Url image flash 1:
                                    </label>
                                    <input class="form-control" id="tys_flash_1" name="tys_flash_1" placeholder="" type="text" value="">
                                    </input>
                                </div>
                                <div class="form-group">
                                    <label>
                                        Url image flash 2:
                                    </label>
                                    <input class="form-control" id="tys_flash_2" name="tys_flash_2" placeholder="" type="text" value="">
                                    </input>
                                </div>
                                <div class="form-group">
                                    <label>
                                        Iframe Video:
                                    </label>
                                    <textarea class="form-control" id="tys_video" name="tys_video">
                                    </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span class="glyphicon glyphicon-cog">
                        </span>
                        Edit for YOU WANT DONE
                    </div>
                    <div class="panel-body">
                        <div class="margin10">
                            <div class="form-group col-sm-6 center">
                                @if(isset($result))
			        		@if($result)
                                <div class="alert alert-success">
                                    <strong>
                                        Successful !!
                                    </strong>
                                    Deleted .
                                </div>
                                @else
                                <div class="alert alert-danger">
                                    <strong>
                                        Error !!
                                    </strong>
                                    Don't delete.
                                </div>
                                @endif
				        @endif
                            </div>
                            <br/>
                            <div class="padding5 clearfix">
                            </div>
                            <div class="col-sm-12">
                            	<div class="form-group">
                                    <label>
                                        Url image flash 1:
                                    </label>
                                    <input class="form-control" id="ywd_flash_1" name="tys_flash_1" placeholder="" type="text" value="">
                                    </input>
                                </div>
                                <div class="form-group">
                                    <label>
                                        Url image flash 2:
                                    </label>
                                    <input class="form-control" id="ywd_flash_2" name="tys_flash_2" placeholder="" type="text" value="">
                                    </input>
                                </div>
                                <div class="form-group">
                                    <label>
                                        Iframe Video:
                                    </label>
                                    <textarea class="form-control" id="ywd_video" name="tys_video">
                                    </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3 col-md-3">
                <a href="{{{ url('admin/videoflash/edit') }}}">
                    <button class="btn btn-sm btn-warning" type="button">
                        <span class="glyphicon glyphicon-edit">
                        </span>
                        Update video and flash
                    </button>
                </a>
            </div>
        </div>
        <br/>
    </form>
</div>
<script>
    $(document).ready(function(){
        $("#list_video").DataTable();
        $("#list_flash").DataTable();
	});
</script>
@stop
