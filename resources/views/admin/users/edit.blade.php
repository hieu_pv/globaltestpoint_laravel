@extends('admin') 

@section('title') 
    Edit User 
@stop 

@section('content')
<div class="container edit_users-admin">
    @include('blocks/error')
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading"><span class="glyphicon glyphicon-cog"></span> Edit users
            </div>
            <form class="form-horizontal" role="form" action="" method="post">
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                <div class="form-group">
                    <label class="control-label col-sm-2" for="role">Role:</label>
                    <div class="col-sm-10">
                        <select name="role" id="role" class="form-control">
                            <option value="">Please select a role</option>
                            @if (!$roles->isEmpty())
                                @foreach ($roles as $role)
                                <option {{ !$data->roles->isEmpty() && $data->roles[0]['id'] == $role['id'] ? 'selected' : '' }} value="{{ $role['id'] }}">{{ $role['name'] }}</option>
                                @endforeach
                            @endif 
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="name">First Name:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="name" placeholder="Enter name" name="first_name" value="{!! old('txtname', isset($data) ? $data['first_name'] : null) !!}">
                    </div>
                </div> 
                <div class="form-group">
                    <label class="control-label col-sm-2" for="name">Last Name:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="name" placeholder="Enter name" name="last_name" value="{!! old('txtname', isset($data) ? $data['last_name'] : null) !!}">
                    </div>
                </div> 
                <div class="form-group">
                    <label class="control-label col-sm-2" for="sex">Sex:</label>
                    <div class="col-sm-10">
                        <select class="form-control" id="sel1" name="txtsex">
                            <option value="1" @if($data['gender'] == 1) {!! "selected='selected'" !!} @endif >Male</option>
                             <option value="0" @if($data['gender'] == 0) {!! "selected='selected'" !!} @endif >Female</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="address">Address:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="address" placeholder="Enter address" name="txtaddress" value="{!! old('txtaddresst', isset($data) ? $data['address'] : null) !!}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="phone">Phone:</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="phone" placeholder="Enter Phone" name="txtphone" value="{!! old('txtphone', isset($data) ? $data['mobile'] : null) !!}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" for="active">Active:</label>
                    <div class="col-sm-10">
                        <input type="checkbox" id="active" value="1" name="active" {{$data['active'] == 1 ? 'checked' : ''}}>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop
