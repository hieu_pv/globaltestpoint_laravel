@extends ('admin') @section('title') {{$title}} @stop @section ('content')
<div class="container" id="blankpageadmin">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <span class="glyphicon glyphicon-cog">
                </span> {{ $tableTitle }}
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-3 margin-bottom-15">
                        <a href="{{ route('admin.faq.index') }}">
                            <button type="button" class="btn btn-primary">
                                <i class="fa fa-backward" aria-hidden="true"></i> Back
                            </button>
                        </a>
                    </div>
                </div>
                @if (count($errors) > 0)
                <div class="alert alert-danger text-left">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <form accept-charset="utf-8" method="post" role="form">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                        @include('admin.faq._form_element')
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
