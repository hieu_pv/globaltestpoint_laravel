@extends ('admin') 

@section('title') 
    {{$title}} 
@stop 

@section ('content')
<div class="container document-container">
    <div class="row">
        <a href="{{ route('admin.faq.create') }}" class="add-new pull-right btn btn-primary margin-bottom-15"><i class="glyphicon glyphicon-upload"></i> Add New</a>
    </div>
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
            	<span><i class="glyphicon glyphicon-cog"></i> {{$tableTitle}}</span>
            </div>
            <div class="panel-body">
                <div class="margin10">
                    <div class="col-sm-12">
                        <table id="list_document" class="table table-striped table-bordered data-table" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th class="text-center">STT</th>
                                    <th>Question</th>
                                    <th>Answer</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 0; ?> 
                                @if(!empty($faqs)) 
                                    @foreach ($faqs as $faq)
                                        <tr id="{{ $faq['id'] }}">
                                            <td class="text-center">{{ ++$i }}</td>
                                            <td>{{ $faq['question'] }}</td>
                                            <td>{!! $faq['answer'] !!}</td>
                                            
                                            <td>
                                                <a class="a_no_hover" href="{{ route('admin.faq.edit', $faq->id) }}">
                                                    <button type="button" class="btn btn-sm btn-warning">
                                                    	<span class="glyphicon glyphicon-edit"></span> Edit
                                                    </button>
                                                </a>
                                                <a class="a_no_hover" href="javascript:void(0);" data-target="#confirm-delete_{{ $faq['id'] }}" data-toggle="modal">
                                                    <button stt="{{ $faq['id'] }}" type="button" class="btn btn-sm btn-danger">
                                                    	<span class="glyphicon glyphicon-trash"></span> Delete
                                                    </button>
                                                </a>
                                                <div id="confirm-delete_{{ $faq['id'] }}" class="modal fade" role="dialog">
                                                    <div class="modal-dialog">
                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                <h4 class="modal-title">Are you sure to delete this question ?</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="form-group center">
                                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                    <input type="hidden" name="_method" value="DELETE">
                                                                    <a href="{{ route('admin.faq.destroy', $faq['id']) }}"><input type="button" class="btn btn-danger" value="OK"></a>
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach 
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
