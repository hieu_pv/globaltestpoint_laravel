<!DOCTYPE html>
<html ng-app="app">

<head>
    <title ng-bind="title">Global Test Point</title>
    <META NAME="ROBOTS" CONTENT="NOINDEX, FOLLOW">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=0.5, maximum-scale=1.0" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="icon" type="image/ico" href="{!! url('assets/frontend/images/button_home.png') !!}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script type="text/javascript">
        var BASE_URL = "{!! asset('/') !!}";
        var GOOGLE_MAP_API_KEY = "{{getenv('GOOGLE_API_KEY')}}";
        var JWT_TOKEN_COOKIE_KEY = "ushift_jwt_token_admin";
        var USER_JWT_TOKEN_COOKIE_KEY = "ushift_jwt_token_v2";
        var STRIPE_PUBLISHABLE_KEY = "{{getenv('STRIPE_PUBLISHABLE_KEY')}}";
        var FACEBOOK_CLIENT_ID = "{{getenv('FACEBOOK_CLIENT_ID')}}";
        var GOOGLE_CLIENT_ID = "{{getenv('GOOGLE_CLIENT_ID')}}";
        var PUSHER_APP_KEY = "{{getenv('PUSHER_APP_KEY')}}";
        var PUSHER_APP_CLUSTER = "{{getenv('PUSHER_APP_CLUSTER')}}";
        var AllowMaxFileSize = 16;
        var AppTimeZone = "{{env('APP_TIME_ZONE', 'Asia/Singapore')}}";
        var ADMIN_SESSION = 'admin_session';
        var ADMIN_PROFILE = 'admin_profile';
        var ADMIN_USER = {!!Auth::user()!!};
    </script>
    @if(getenv('APP_ENV') == 'production')
        <link rel="stylesheet" href="{!! asset('assets/css/admin.all.min.css?v=' . $buster['assets/css/admin.all.min.css']) !!}">
        <script src="{!! asset('assets/js/admin.min.js?v=' . $buster['assets/js/admin.min.js']) !!}" async></script>
    @else
        <link rel="stylesheet" href="{!! asset('assets/css/admin.all.css') !!}">
        <script src="{!! asset('assets/js/admin.js') !!}" async></script>
    @endif
</head>

<body ui-view="main"></body>

</html>
