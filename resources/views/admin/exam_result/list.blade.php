@extends ('admin') @section('title') {{$title}} @stop @section ('content')
<div class="container school-degree-container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                <span><i class="glyphicon glyphicon-cog"></i> {{$tableTitle}}</span>
            </div>
            <div class="panel-body">
                <div class="margin10">
                    <div class="col-sm-12 margin-bottom-10">
                        <a href="{{ url('print-pdf/print-all-result') }}" class="btn btn-success">
                            <i class="glyphicon glyphicon-print"></i> Print all
                        </a>
                    </div>
                    <div class="col-sm-12">
                        <table id="list_exam_result" class="table table-striped table-bordered data-table" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    @if(!empty($examResults) && count($examResults) > 0)
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>Test Name</th>
                                    <th class="text-center">Total Marks (%)</th>
                                    <th>Time Taken</th>
                                    <th>Exam Date</th>
                                    <th>Action</th>
                                    @else
                                    <th colspan="7" class="no-record">You don't have any record</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @if(!empty($examResults)) @foreach ($examResults as $examResult)
                                <tr>
                                    <td>{{ $examResult['user']['first_name'] }} {{ $examResult['user']['last_name'] }}</td>
                                    <td>{{ $examResult['user']['email'] }}</td>
                                    <td>{{ $examResult['data']['exam']['name'] }}</td>
                                    <td class="text-center">{{ number_format($examResult['data']['score'], 2) }}%</td>
                                    <td>{{ $examResult['timeTaken'] }}</td>
                                    <td>{{ $examResult['examDate'] }}</td>
                                    <td>
                                        <a href="{{ url('print-pdf/print-result', $examResult['id']) }}" class="btn btn-success">
                                            <i class="glyphicon glyphicon-print"></i> Print
                                        </a>
                                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete_exam_result_{{ $examResult['id'] }}">Delete</button>
                                    </td>
                                </tr>
                                <div class="modal fade" id="delete_exam_result_{{ $examResult['id'] }}" role="dialog">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Delete</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>Are you sure you want to delete this exam result?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <a class="btn btn-danger" href="{{ route('admin.exam-result.destroy', $examResult['id']) }}" onclick="event.preventDefault();
                                                     document.getElementById('delete-exam-result-form-{{ $examResult['id'] }}').submit();">
                                                    Yes
                                                </a>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <form id="delete-exam-result-form-{{ $examResult['id'] }}" action="{{ route('admin.exam-result.destroy', $examResult['id']) }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE">
                                </form>
                                @endforeach @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
