@extends('master') 
@section('title', 'Login')
@section('content')
<div class="container login_site">
    <div class="row">
        <div class="col-md-5 col-md-offset-4">
            @include('blocks/error') 
            @if (session('flash_message'))
            <div class="alert alert-{{ session('flash_level') }}">
                {{ session('flash_message') }}
            </div>
            @endif
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Please Sign In</h3>
                </div>
                <div class="panel-body">
                    <form role="form" action="" method="POST">
                        <fieldset>
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                            <div class="form-group">
                                <input class="form-control" placeholder="Email" name="email" type="email" autofocus>
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Password" name="password" type="password" value="">
                            </div>
                            <div class="form-group">
                                <div class="text-right forgot-password">
                                <a href="{!! url('forgot-password') !!}" class="a_no_hover">Can't log in?</a>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-lg btn-success btn-block">Login</button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
