@extends ('admin')

@section('title')
    Globaltestpoint - Dashboard Category
@stop

@section ('content')
<div class="container option_site">
	<form action="" method="post" accept-charset="utf-8" enctype="multipart/form-data">
		<div class="row">
			<div class="panel panel-default">
	            <div class="panel-heading"><span class="glyphicon glyphicon-cog"></span> OPTION FOR WEBSITE</div>
	            <div class="panel-body">
	                <div class="margin10">
	                	@if(Session::has('result'))
	                	<div class="form-group col-sm-12 col-md-6 center">
			        		@if(Session::get('result'))
			        		<div class="alert alert-success">
			                    {{ Session::get('message') }}
			                </div>
				            @else
				            	<div class="alert alert-danger">
				                    {{ Session::get('message') }}
				                </div>
				            @endif
		        		</div>
		        		@endif
		        		<br/>
	                    <div class="padding5 clearfix"></div>
	                    <div class="main_option">
	                		<div class="row form-group">
	                    		<div class="col-sm-12 col-md-6">
	                    			<div class="form-group">
	                    				@if(isset($option['logo']))
	                    				<label>Logo Testyourself:</label>
	                    				<img src="{{ url($option['logo']) }}" alt="Logo" id="name_logo" class="logo_testyourself">
	                    				<input type="hidden" name="old_logo" value="{{ url($option['logo']) }}">
	                    				<br />
	                    				@endif
	                    				<label for="logo">Upload Logo</label>
	                    				<input type="file" name="option_logo" class="option_logo">
	                    			</div>
	                    			<div class="form-group">
	                    				<label for="title">Title website: </label>
	                    				<input type="text" class="form-control" name="title" id="title" value="{{ (isset($option['title']) ? $option['title'] : old('title')) }}">
	                    			</div>
	                    			<div class="form-group">
	                    				<label for="email">Email contact: </label>
	                    				<input type="email" class="form-control" name="email" id="email" value="{{ (isset($option['email']) ? $option['email'] : old('email')) }}">
	                    			</div>
	                    			<div class="form-group">
	                    				<label for="phone">Phone contact: </label>
	                    				<input type="text" class="form-control" name="phone" id="phone" value="{{ (isset($option['phone']) ? $option['phone'] : old('phone')) }}">
	                    			</div>
	                    			<div class="form-group">
	                    				<label for="address">Address company: </label>
	                    				<textarea class="form-control" name="address" id="address">{{ (isset($option['address']) ? $option['address'] : old('address')) }}</textarea>
	                    			</div>
	                    			<div class="form-group">
	                    				<label for="fax">Fax: </label>
	                    				<input type="text" class="form-control" name="fax" id="fax" value="{{ (isset($option['fax']) ? $option['fax'] : old('fax')) }}">
	                    			</div>
	                    			{{-- <div class="form-group">
	                					<label for="">Image for "Login" on Testyourself page: </label>
	                    				@if(isset($option['image_login']))
	                    					<br >
		                    				<img src="{{ url($option['image_login']) }}" alt="Logo" class="logo_testyourself">
		                    				<input type="hidden" name="old_image_login" value="{{ url($option['image_login']) }}">
		                    				<br /><br />
		                    			@endif
	                					<input type="file" name="image_login" class="file option_logo image_login" >	
	                				</div>
	                				<div class="form-group">
	                					<label for="">Image for "Register" on Testyourself page: </label>
	                					@if(isset($option['image_register']))
	                						<br />
		                    				<img src="{{ url($option['image_register']) }}" alt="Logo" class="logo_testyourself">
		                    				<input type="hidden" name="old_image_register" value="{{ url($option['image_register']) }}">
		                    				<br /><br />
		                    			@endif
	                					<input type="file" data-show-caption="true" data-show-upload="false" name="image_register" class="option_logo image_register" >	
	                				</div>
	                				<div class="form-group">
	                					<label for="">Image for "Login/Resgister" on Testyourself page: </label>
	                					@if(isset($option['image_login_register']))
	                						<br />
		                    				<img src="{{ url($option['image_login_register']) }}" alt="Logo" class="logo_testyourself">
		                    				<input type="hidden" name="old_image_login_register" value="{{ url($option['image_login_register']) }}">
		                    				<br /><br />
		                    			@endif
	                					<input type="file" data-show-caption="true" data-show-upload="false" name="image_login_register" class="option_logo image_login_register" >
	                				</div> --}}
	                    		</div>	
	                    		<div class="col-sm-12 col-md-6">
	                    		    <div class="form-group">
	                    				<label>Logo YouWantdone:</label>
	                    				@if(!empty($option2['logo']))
	                    					<img src="{!! url( $option2['logo']) !!}" alt="logowandont" class="logo_wandont">
	                    				@else
	                    					<img src="{!! url('uploads/default_avatar.png') !!}" alt="logowandont" class="logo_wandont">
	                    				@endif
	                    				<br />
	                    				<label for="logowandont">Upload Logo</label>
	                    				<input type="hidden" name="old_logo_ywd" value="{{ $option2['logo'] }}">
	                    				<input type="file" name="option_logo_wandont" class="option_logo">
	                    			</div>
	                    			<div class="form-group">
	                    				<label for="ywd_title">Title website: </label>
	                    				<input type="text" class="form-control" name="ywd_title" id="ywd_title" value="{{ (isset($option2['title']) ? $option2['title'] : old('title')) }}">
	                    			</div>
	                    			<div class="form-group">
	                    				<label for="ywd_email">Email contact: </label>
	                    				<input type="email" class="form-control" name="ywd_email" id="ywd_email" value="{{ (isset($option2['email']) ? $option2['email'] : old('email')) }}">
	                    			</div>
	                    			<div class="form-group">
	                    				<label for="ywd_phone">Phone contact: </label>
	                    				<input type="text" class="form-control" name="ywd_phone" id="ywd_phone" value="{{ (isset($option2['phone']) ? $option2['phone'] : old('phone')) }}">
	                    			</div>
	                    			<div class="form-group">
	                    				<label for="ywd_address">Address company: </label>
	                    				<textarea class="form-control" name="ywd_address" id="ywd_address">{{ (isset($option2['address']) ? $option2['address'] : old('address')) }}</textarea>
	                    			</div>
	                    			<div class="form-group">
	                    				<label for="ywd_fax">Fax: </label>
	                    				<input type="text" class="form-control" name="ywd_fax" id="ywd_fax" value="{{ (isset($option2['fax']) ? $option2['fax'] : old('fax')) }}">
	                    			</div>
	                    		</div>
	                    	</div>
	                    </div>
	                </div>
	            </div>
	        </div>
		</div>
		<div class="row">
			<div class="panel panel-default">
	            <div class="panel-heading"><span class="glyphicon glyphicon-cog"></span> Link fanpage</div>
	            <div class="panel-body">
		            <div class="form-group">
        				<label for="facebook">Link Facebook page: </label>
        				<input type="url" class="form-control" name="facebook" id="facebook" value="{{ (isset($option['facebook']) ? $option['facebook'] : old('facebook')) }}">
        			</div>
        			<div class="form-group">
        				<label for="twitter">Link Twitter page: </label>
        				<input type="url" class="form-control" name="twitter" id="twitter" value="{{ (isset($option['twitter']) ? $option['twitter'] : old('twitter')) }}">
        			</div>
        			<div class="form-group">
        				<label for="youtube">Link Youtube page: </label>
        				<input type="url" class="form-control" name="youtube" id="youtube" value="{{ (isset($option['youtube']) ? $option['youtube'] : old('youtube')) }}">
        			</div>
        			<div class="form-group">
        				<label for="instagram">Link Instagram page: </label>
        				<input type="url" class="form-control" name="instagram" id="instagram" value="{{ (isset($option['instagram']) ? $option['instagram'] : old('instagram')) }}">
        			</div>
        			<div class="form-group">
        				<label for="">Link Linkedin page: </label>
        				<input type="url" class="form-control" name="linkedin" id="linkedin" value="{{ (isset($option['linkedin']) ? $option['linkedin'] : old('linkedin')) }}">
        			</div>
	            </div>
	        </div>
	    </div>
		<div class="row">
			<div class="panel panel-default">
	            <div class="panel-heading"><span class="glyphicon glyphicon-cog"></span> Term and Policy</div>
	            <div class="panel-body">
		            <textarea class="rich_editor" name="termpolicy" id="termpolicy">{!! (isset($option['termpolicy']) && !empty($option['termpolicy']) ? $option['termpolicy'] : '') !!}</textarea>
		            <script>
	                    ckeditor('termpolicy');
	                </script>
	            </div>
	        </div>
	    </div>
	    {{-- <div class="row">
			<div class="panel panel-default">
	            <div class="panel-heading"><span class="glyphicon glyphicon-cog"></span> Introduction and instruction on the books</div>
	            <div class="panel-body">
		            <textarea class="rich_editor" name="introduction_book" id="introduction_book">{!! (isset($option['introduction_book']) && !empty($option['introduction_book']) ? $option['introduction_book'] : '') !!}</textarea>
		            <script>
	                    ckeditor('introduction_book');
	                </script>
	            </div>
	        </div>
	    </div> --}}
	    <div class="row">
			<div class="panel panel-default">
	            <div class="panel-heading"><span class="glyphicon glyphicon-cog"></span> Introduction and instruction on the various exams</div>
	            <div class="panel-body">
		            <textarea class="rich_editor" name="introduction_variousexam" id="introduction_variousexam">{!! (isset($option['introduction_variousexam']) && !empty($option['introduction_variousexam']) ? $option['introduction_variousexam'] : '') !!}</textarea>
		            <script>
	                    ckeditor('introduction_variousexam');
	                </script>
	                <br>
	                <div class="row">
	            		<div class="col-md-3 form-group">
	            			<input name="_token" type="hidden" value="{{ csrf_token() }}">
	            			<input type="submit" class="btn btn-primary" name="btn_option" value="Update Option">
	            		</div>
	            	</div>
	            </div>
	        </div>
	    </div>
	    {{-- <div class="row">
			<div class="panel panel-default">
	            <div class="panel-heading"><span class="glyphicon glyphicon-cog"></span> Introduction and instruction for the exams</div>
	            <div class="panel-body">
		            <textarea class="rich_editor" name="introduction_exams" id="introduction_exams">{!! (isset($option['introduction_exams']) && !empty($option['introduction_exams']) ? $option['introduction_exams'] : '') !!}</textarea>
		            <script>
	                    ckeditor('introduction_exams');
	                </script>
	                <br>
	                <div class="row">
	            		<div class="col-md-3 form-group">
	            			<input name="_token" type="hidden" value="{{ csrf_token() }}">
	            			<input type="submit" class="btn btn-primary" name="btn_option" value="Update Option">
	            		</div>
	            	</div>
	            </div>
	        </div>
	    </div> --}}
	</form>
</div>
@stop

