<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="google-site-verification" content='<meta name="google-site-verification" content="wcFCJEcKG9Jw-4xUKOZ8VE6cMbvzKqP4M0Fwut9ZMdY" />' />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/ico" href="{!! url('assets/frontend/images/button_home.png') !!}">
    <link href="https://fonts.googleapis.com/css?family=Cormorant+Garamond:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
    <title> @section('title') @show </title>
    @section('style') @include('partials._style') @show
    <script src="{!! url('assets/js/website.js') !!}"></script>
    <script src="{{ url('admin_plugin/js/plugin-soanthao/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ url('admin_plugin/js/plugin-soanthao/ckfinder/ckfinder.js') }}"></script>
    <script>
    var baseURL = "{!! url('/') !!}";
    var BASE_URL = "{!! asset('') !!}";
    var STRIPE_KEY = "{{ getenv('STRIPE_KEY') }}";
    var CURRENTCY_SYMBOL = "{{ getenv('STRIPE_CURRENCY_SYMBOL') }}";
    </script>
    <script src="{{ url('admin_plugin/js/plugin-soanthao/func_ckfinder.js') }}"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    @section('textanimationjs') @show
    <!--Start of Zopim Live Chat Script-->
    <script type="text/javascript">
    window.$zopim || (function(d, s) {
        var z = $zopim = function(c) {
                z._.push(c)
            },
            $ = z.s =
            d.createElement(s),
            e = d.getElementsByTagName(s)[0];
        z.set = function(o) {
            z.set.
            _.push(o)
        };
        z._ = [];
        z.set._ = [];
        $.async = !0;
        $.setAttribute("charset", "utf-8");
        $.src = "//v2.zopim.com/?4AwLWL3ZRqPuijXa8OP56nGQMDDW5XxI";
        z.t = +new Date;
        $.
        type = "text/javascript";
        e.parentNode.insertBefore($, e)
    })(document, "script");
    </script>
    <!--End of Zopim Live Chat Script-->
</head>

<body>
    <div id="fb-root"></div>
    <script>
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    </script>
    <!-- GA -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-83879747-1', 'auto');
      ga('send', 'pageview');

    </script>
    <!-- END GA -->
    @yield('content')
</body>

</html>
