<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ $title ? $title : 'Examination Result' }}</title>
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/pdf/pdf.css') }}">
</head>

<body>
    <div class="main-container">
        <div class="content">
            @if ($examResult != null)
            <div class="name-area">
                <span class="name-title col-xs-4">Name: </span>
                <span class="name col-xs-8">{{ $user != null ? $user['first_name'] : 'Guest' }}</span>
            </div>
            <h3 class="text-center">{!! $title !!}</h3>
            <table class="table table-bordered table-condensed table-datatable table-hover">
                <tr>
                    <td class="col-xs-4">Name of candidate:</td>
                    <td class="col-xs-8">{!! Auth::user() ? Auth::user()->first_name : 'Guest' !!}</td>
                </tr>
                <tr>
                    <td class="col-xs-4">Subject:</td>
                    <td class="col-xs-8">{{ $examResult['exam']['name'] }}</td>
                </tr>
                <tr>
                    <td class="col-xs-4">Date/Time:</td>
                    <td class="col-xs-8">{!! $examResult['examDate'] !!}</td>
                </tr>
                <tr>
                    <td class="col-xs-4">Scores:</td>
                    <td class="col-xs-8">{!! $examResult['score'] !!}%</td>
                </tr>
                <tr>
                    <td class="col-xs-4">Grade:</td>
                    <td class="col-xs-8">{!! $examResult['grade'] !!}</td>
                </tr>
                <tr>
                    <td class="col-xs-4">Remark:</td>
                    <td class="col-xs-8">{!! $examResult['remark'] !!}</td>
                </tr>
            </table>
            @else
            <p class="alert alert-danger">Exam not found</p>
            @endif
        </div>
        <div class="footer-content">
            <div class="head-footer">
                {!! Auth::user() ? Auth::user()->first_name : 'Guest' !!}, The Globaltespoint Team congratulates you for attempting ‘{{ $examResult['exam']['name'] }}’. Remember you can always improve on your scores by reviewing the question{s) you missed out and by carefully noting why you got such question(s) wrong.
            </div>
            <div class="body-footer">
                <p>{!! Auth::user() ? Auth::user()->first_name : 'Guest' !!}, here is the catch: </p> 
                <ul>
                    <li>Review the whole topic(s) and make sure you fully understand the topic content and context.</li>
                    <li>When you are done, go back to globaltespoint and take similar test – preferably, advance version(s).</li>
                    <li>Attempt to finish the Test before the last 5-10 Mins.</li>
                    <li>Try and be deliberately conscious of the test timing.</li>
                    <li>Review your responses.</li>
                </ul>
                <p>{!! Auth::user() ? Auth::user()->first_name : 'Guest' !!}, by doing all these, we bet your score will improve greatly.</p>
            </div>
            <div class="foot-footer">
                We share your dreams – <a href="http://globaltestpoint.com/">www.globaltestpoint.com. info@globaltestpoint.com</a> +1 703 332 9157
            </div>
        </div>
    </div>
</body>

</html>
