@extends ('master') @section('title') Home @stop @section('style') @parent
<style type="text/css">body {overflow: hidden;}</style>
<link rel="stylesheet" href="{{ url('assets/css/textanimate.css') }}">
<link rel="stylesheet" href="{{ url('assets/css/style_textanimation.css') }}"> @stop @section('textanimationjs')
<script src="{{ url('assets/js/jquery.fittext.js') }}"></script>
<script src="{{ url('assets/js/jquery.lettering.js') }}"></script>
<script src="{{ url('assets/js/jquery.textillate.js') }}"></script>
@stop @section ('content')
<div id="layer" style="background: url({!! asset('assets/frontend/images/slider_home/imgpsh_fullsize_2.jpg') !!}); 
    height: 100%;
    position: fixed;
    width: 100%;">
    <a class="login-btn themed-background-default" href="javascript:void(0)">
        <span class="login-logo">
            <span class="square1 themed-border-default"></span>
        <span class="square2"><img style="width:150px;margin-top: -15px;" src="{!! asset('assets/frontend/images/button_home.png') !!}"></span>
        <span class="name"><img style="width:150px;margin-top: -15px;" src="{!! asset('assets/frontend/images/button_home.png') !!}"></span>
        </span>
    </a>
    <div class="left-door"></div>
    <div class="right-door"></div>
</div>
<div id="home" class="hide">
    <div class="container_home">
        <div class="home_page">
            <div class="slider_home">
                @if (!$homeSliders->isEmpty())
                    <ul>
                    @foreach($homeSliders as $homeSlider)
                        <li>
                            <a href="javascript:void(0)"><img style="background-image: url({!! asset($homeSlider['image']) !!});" src="{!! asset('assets/frontend/images/blank-image.png') !!}" alt=""></a>
                        </li>
                    @endforeach
                    </ul>
                @endif
            </div>
            <div class="title">
                <div class="col-md-12" id="tys_textanimation">
                    <div class="jumbotron1">
                        <div class="container">
                            <h5 class="tlt">Global Test Point</h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 testyourself-test">
                    <div class="global_test_poin academic-tests general-tests">
                        <a href="{!! url('testyourself'); !!}">
                            <div class="cube">
                                <div class="flippety">
                                    <p class="test_bg algin mattien">Academic Tests, Professional Test & CV Services</p>
                                </div>
                                <div class="flop">
                                    <p class="test_bg algin">AACE, ICAN, CIMA, CCNA, IELTS, GRE, JAMB, WAEC etc.</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="global_test_poin professional-tests general-tests">
                        <a href="{!! url('testyourself'); !!}">
                            <div class="cube">
                                <div class="flippety">
                                    <p class="test_bg algin mattien">Job Aptitude Tests, Interview Tips & Job Listing</p>
                                </div>
                                <div class="flop">
                                    <p class="test_bg algin">Oil & Gas, Banking, Construction, Int'I Jobs etc.</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="global_test_poin job-aptitude-tests general-tests">
                        <a href="{!! url('testyourself'); !!}">
                            <div class="cube">
                                <div class="flippety">
                                    <p class="test_bg algin mattien">E-Books, Biz Proposals & Consultancy Services</p>
                                </div>
                                <div class="flop">
                                    <p class="test_bg algin">How to start a business, Change Life Style, Parenting, Agriculture, Relationships, Career Advice etc.</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <!-- <div class="col-xs-12 col-sm-5 col-md-4  global_test_poin">
                        <a href="{!! url('do-you-want-done') !!}">
                            <div class="cube">
                                <div class="flippety">
                                    <p class="agriculture_bg algin mattien">What do you want done?</p>
                                </div>
                                <div class="flop">
                                    <p class="agriculture_bg algin">Learn Fish, Grass Cutter farming etc.</p>
                                </div>
                            </div>
                        </a>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</div>
@stop
