@extends('master')
@include('_include_angular')
@section('title')
    Apply to Become a Partner
@stop
@section('content')
    @include('testyourself.partials.header')

    <script type="text/javascript">
        var countries = {!! json_encode($countries) !!};
        console.log(countries);
    </script>

    <div class="container" ng-app="app" ng-controller="PartnerCtrl as vm">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2">
            <h3 class="text-center">Become a partner of www.globaltestpoint.com</h3>
            <div ng-show="!vm.success">
                <form method="post" name="partner_form" ng-submit="partner_form.$valid && vm.addPartner(vm.partner)" novalidate>
                    <div class="form-group">
                        <label>*Business Name</label>
                        <input type="text" class="form-control" name="business_name" ng-model="vm.partner.business_name" required>
                        <div class="errors">
                            <span ng-show="partner_form.$submitted && partner_form.business_name.$error.required">Required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>*First Name</label>
                        <input type="text" class="form-control" name="first_name" ng-model="vm.partner.first_name" required>
                        <div class="errors">
                            <span ng-show="partner_form.$submitted && partner_form.first_name.$error.required">Required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>*Last Name</label>
                        <input type="text" class="form-control" name="last_name" ng-model="vm.partner.last_name" required>
                        <div class="errors">
                            <span ng-show="partner_form.$submitted && partner_form.last_name.$error.required">Required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>*Email Address</label>
                        <input type="email" class="form-control" name="email" ng-model="vm.partner.email" required>
                        <div class="errors">
                            <span ng-show="partner_form.$submitted && partner_form.email.$error.required">Required</span>
                            <span ng-show="partner_form.$submitted && partner_form.email.$error.email">Please enter a valid email</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>*Location</label>
                        <select class="form-control" select2 name="country_id" id="partner-location" required>
                            <option ng-repeat="country in vm.countries" ng-value="country.id" ng-bind="country.name"></option>
                        </select>
                        <div class="errors">
                            <span ng-show="partner_form.$submitted && partner_form.country_id.$error.required">Required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>*State</label>
                        <select class="form-control" select2 name="state_id" id="partner-state" required>
                            <option ng-repeat="state in vm.selectedCountry.states" ng-value="state.id" ng-bind="state.name"></option>
                        </select>
                        <div class="errors">
                            <span ng-show="partner_form.$submitted && partner_form.state_id.$error.required">Required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>*City</label>
                        <input type="text" class="form-control" ng-model="vm.partner.city" name="city" required>
                        <div class="errors">
                            <span ng-show="partner_form.$submitted && partner_form.city.$error.required">Required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>*Address</label>
                        <input type="text" class="form-control" ng-model="vm.partner.address" name="address" required>
                        <div class="errors">
                            <span ng-show="partner_form.$submitted && partner_form.address.$error.required">Required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>*Phone Number</label>
                        <input type="text" class="form-control" ng-model="vm.partner.phone" name="phone" required>
                        <div class="errors">
                            <span ng-show="partner_form.$submitted && partner_form.phone.$error.required">Required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Website</label>
                        <input type="text" class="form-control" ng-model="vm.partner.website" name="website">
                    </div>
                    <div class="form-group">
                        <label>What are your main interest areas</label>
                        <select class="form-control" name="interest" ng-model="vm.partner.interest">
                            <option value="Academic Test" selected>Academic Test</option>
                            <option value="Profestional Test">Profestional Test</option>
                            <option value="Job Aptitude">Job Aptitude</option>
                            <option value="E-books">E-books</option>
                            <option value="School etc.">School etc.</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Who are your target(niche)</label>
                        <input type="text" class="form-control" name="target" ng-model="vm.partner.target">
                    </div>  
                    <div class="form-group">
                        <label>How many users are you expecting to reach?</label>
                        <select class="form-control" name="number_of_users" ng-model="vm.partner.number_of_users">
                            <option value="10-100">10-100</option>
                            <option value="100-1000">100-1000</option>
                            <option value="1000-10000">1000-10000</option>
                            <option value="10000-1000000">10000-1000000</option>
                            <option value="above">above</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>How do you intend to reach them?</label>
                        <input type="text" class="form-control" name="how_do_intend_to_reach" ng-model="vm.partner.how_do_intend_to_reach">
                    </div>
                    <div class="form-group">
                        <label>How did you here about us?</label>
                        <input type="text" class="form-control" name="how_did_here" ng-model="vm.partner.how_did_here">
                    </div>
                    <div class="form-group">
                        <label>Review Link(s)</label>
                        <textarea class="form-control" ng-model="vm.partner.review_links"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Partnership Preference</label>
                        <select class="form-control" name="perference" ng-model="vm.partner.perference" ng-options="item as item for item in vm.parter_preferences"></select>
                    </div>
                    <div class="form-group">
                        <label>Tell us a little bit about your business</label>
                        <textarea class="form-control" ng-model="vm.partner.about"></textarea>
                    </div>
                    <div class="form-group container_fluider_sigup_recapcha">
                        <div class="g-recaptcha" data-sitekey="{{ getenv('GOOGLE_CAPTCHA') }}"></div>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" name="accept" ng-model="vm.partner.accept" id="accept-term-of-services" required> <label for="accept-term-of-services">We/I accept the <a href="{{ asset('/term-and-policy') }}">term and policy</a> of Globaltestpoint.com</label>
                        <div class="errors">
                            <span ng-show="partner_form.$submitted && partner_form.accept.$error.required">Required</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" ng-class="{'disabled': vm.inprocess}" ng-disabled="!vm.partner || vm.inprocess">Become a Partner</button>
                    </div>
                </form>
            </div>
            <div class="alert alert-success" ng-show="vm.success">
                <p><span ng-if="isLoggedIn">Mr/Mrs @{{user.first_name}}, </span>your application has been submitted successfully. We are going to review your submission and revert on your suitability or otherwise for this offer</p> 
            </div>
        </div>
    </div>

    @include ('testyourself.partials.footer')
@stop
