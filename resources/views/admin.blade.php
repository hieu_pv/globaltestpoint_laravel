<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/ico" href="{!! url('assets/images/favicon/favicon.png') !!}">
    <title>@section('title') @show</title>
    
    <link rel="stylesheet" href="{!! url('assets/css/libraries.min.css') !!}">
    <script src="{!! url('assets/js/website.js') !!}"></script>
    <link rel="stylesheet" href="{!! url('assets/css/backend.css') !!}">
    <script src="{{ url('admin_plugin/js/plugin-soanthao/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ url('admin_plugin/js/plugin-soanthao/ckfinder/ckfinder.js') }}"></script>
    <script>
    var BASE_URL = "{!! asset('') !!}";
    </script>
    <script src="{{ url('admin_plugin/js/plugin-soanthao/func_ckfinder.js') }}"></script>
</head>

<body>
    @include('admin/partials/_header') @yield ('content') @include('admin/partials/_footer')
</body>
    <script src="{{ url('admin_plugin/js/bootstrap-filestyle.js') }}"></script>
</html>
