<?php $optionsite = Session::get('ywd_optionsite')?>
<div class="header_test_yourself do_you_want_done">
    <div class="container_site">
        <div class="col-md-12">
            <div class="logo_register">
                <div class="col-md-6 col-sm-6 col-xs-6 logo_test">
                    <a href="{!! url('do-you-want-done') !!}">
                        <div class="logo" style="background: url('{!! (!empty($optionsite['logo']) ?  $optionsite['logo'] : asset('assets/frontend/images/testyourself/no-image.svg')) !!}') no-repeat;">
                        </div>
                    </a>
                   
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 register_col">
                    <div class="register">
                        <p>
                            @if(Auth::user())
                            <a href="#" class="member_name">{!! Auth::user()->last_name !!}</a> | 
                            <a href="{!! url('user/profile') !!}" >Profile</a> | 
                            <a href="{!! url('auth/logout') !!}">Logout</a>
                            @else
                            <a href="{!! url('/login') !!}" class="login">Login</a> | 
                            <a href="{!! url('/sign_up') !!}" class="sign_up">Sign Up</a>
                            @endif
                        </p>
                    </div>
                </div>
            </div>
            <nav class="navbar  navbar-static-top navbar_agriculture_menu">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="bg_li_menu navbar-brand dropdown" href="{!! url('/') !!}">Home</a>
                    </div>
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav">
                            @if(isset($ywd_blank_parents))
                                @if(isset($ywd_countblankpage))
                                    @if($ywd_countblankpage <= 8)
                                        @foreach ($ywd_blank_parents as $cate_parent)
                                            <li class="dropdown li_menu_test">
                                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">{!! $cate_parent['name'] !!} <span class="caret"></span></a>
                                                @if (!empty($cate_parent['listblank']))
                                                <ul class="dropdown-menu">
                                                    @include('wantdone.partials.menu_blank')
                                                </ul>
                                                @endif
                                            </li>
                                        @endforeach
                                    @else
                                        @for($i = 0; $i < 8; $i++)
                                            <li class="dropdown li_menu_test">
                                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">{!! $ywd_blank_parents[$i]['name'] !!} <span class="caret"></span></a>
                                                @if (!empty($ywd_blank_parents[$i]['listblank']))
                                                <ul class="dropdown-menu">
                                                    @foreach($ywd_blank_parents[$i]['listblank'] as $blank)
                                                        <li class="sub_li_menu_test">
                                                            <a href="{!! URL::route('youwantdone.blankpage.show', $blank['slug']) !!}">{!! $blank['title'] !!}</a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                                @endif
                                            </li>
                                        @endfor                                    
                                        <li class="dropdown li_menu_test">
                                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Other <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                            @for($i = 8; $i < $ywd_countblankpage; $i++)
                                                
                                                    <li class="dropdown sub_li_menu_test">
                                                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">{!! $ywd_blank_parents[$i]['name'] !!} <span class="caret"></span></a>
                                                        @if (!empty($ywd_blank_parents[$i]['listblank']))
                                                        <ul class="dropdown-menu submenu-other" role="menu" style="" >
                                                            @foreach($ywd_blank_parents[$i]['listblank'] as $blank)
                                                                <li class="sub_li_menu_test">
                                                                    <a href="{!! URL::route('youwantdone.blankpage.show', $blank['slug']) !!}">{!! $blank['title'] !!}</a>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                        @endif
                                                    </li>
                                                
                                            @endfor
                                            </ul>
                                        </li>
                                    @endif
                                @endif
                            @endif
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>

