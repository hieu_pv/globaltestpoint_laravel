@extends ('wantdone.wantdone') 
@section('title') 
  wantdone 
@stop 
@section('content_wantdone')

<div class="feeture_test_yourself">
    <div class="container_site">
        <div class="col-md-9 col-sm-9 col-xs-12">
            @if(isset($sliders))
            <div class="slider_test_yourself slider_site">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
                    </ol>
                    <!-- Wrapper for slides -->
                    <?php $i = 0; ?>
                    <div class="carousel-inner" role="listbox">
                        @foreach($sliders as $keyslider => $slider)
                        <div class="item {{ ($i == 0) ? "active" : '' }}" style="background:url('{!! url($slider['image']) !!}');">
                            <img src="{!! url('assets/frontend/images/blank-image.png') !!}" alt="Chania">
                        </div>
                        <?php ++$i; ?>
                        @endforeach
                    </div>
                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <!--end-->
            </div>
            @endif
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="menu_test_yourself_right">
                @if($blankhomepage)
                <ul>
                    @foreach($blankhomepage as $valueblank)
                    <li><a href="{{ url('/testyourself/blank-page/'.$valueblank->post['slug']) }}">{{ $valueblank->post['title'] }}</a></li>
                    @endforeach
                </ul>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="content_testyourself">
    <div class="container_site">
        <div class="free_share">
            
        </div>
        <div class="flash_video">
            @if(!empty($video['link']))
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="list_step item1">
                    <div class="img3" style="">
                        <div id="player"></div>
                    </div>
                </div>
            </div>
            <script>
                youtube_parser("{!! $video['link'] !!}");
            </script>
            @endif
            @if(isset($flashs))
                <?php $i = 2;  ?> 
                @foreach($flashs as $key => $flash)
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="list_step item{{ $i }}">
                        <div class="img2" style="background-image: url('{!! asset($flash["link"]) !!}');">
                            <img src="" alt="">
                        </div>
                    </div>
                </div>
                <?php if($i >= 3) { $i = 2; } $i++;?> 
                @endforeach 
            @endif
        </div>
        <div class="document_frames">
            <div class="content_document">
                <button type="submit" class="btn btn-danger pull-right">Download the complete document</button>
                <div class="content_document_vie">
                    
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="big_black_title">
                <h2>A great place <span class="for_learning">for learning</span></h2>
                <p>Weac, Jamb, NECO, Post UME, ICAN, NABTEB, Job Listing, Job Aptitude Test,</p>
                <p>Schools, Scholarships, Career Advise etc.</p>
            </div>
        </div>
    </div>
</div>
@stop
