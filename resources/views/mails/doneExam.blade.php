<p>You have successfully taken {{ $exam['exam']['name'] }} test with Globaltestpoint.com and you {{ $exam['remark'] }}ed.</p>
<p>In order to keep you being at the top of your chosen field, we advise that you explore and take further tests.</p>
<p>Kindly let us know if you would require a customized feedback on your performance where our system will show you your strength(s) and weakness (if any).</p>
<p>Yours,</p>
<p>Globaltestpoint Support Team.</p>
<p><a href="mailto:{{ (!empty($option_tys['email'])) ? $option_tys['email'] : 'info@globaltestpoint.com' }}">@if(!empty($option_tys['email'])) {{ $option_tys['email'] }} @else {{ 'info@globaltestpoint.com' }} @endif</a></p>
<p>{{ (!empty($option_tys['address'])) ? $option_tys['address'] : 'Baltimore MD, USA ' }}</p>