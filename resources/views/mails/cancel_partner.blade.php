<div class="wrapper" style="background: #eee">
	<div class="container" style="width: 500px; margin: 0px auto; padding: 20px;">
		<div class="logo">
			<img src="{{asset('assets/frontend/images/testyourself/logo.png') }}" max-width="100px" max-height="100px">
		</div>
		<div style="background: #fff; overflow: auto; padding: 20px 10px;">
			<div style="text-align: center">
				<div style="text-align: left; padding-bottom: 20px;">
					<p>Your partner account has been cancel</p>
				</div>
			</div>
			<div style="margin-top: 20px; padding-left: 20px">
				<p>Yours,</p>
				<p>Globaltestpoint Support Team.</p>
				<p><a href="mailto:{{ (!empty($option_tys['email'])) ? $option_tys['email'] : 'info@globaltestpoint.com' }}">@if(!empty($option_tys['email'])) {{ $option_tys['email'] }} @else {{ 'info@globaltestpoint.com' }} @endif</a></p>
				<p>{{ (!empty($option_tys['address'])) ? $option_tys['address'] : 'Baltimore MD, USA' }}.</p>
			</div>
		</div>
	</div>
</div>