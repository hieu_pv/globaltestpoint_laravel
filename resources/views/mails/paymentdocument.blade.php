<p>Thank you {{ $user->first_name }} for subscribing to our material. Your payment was successful and you can go ahead with your transaction.</p>
<p>Yours,</p>
<p>Globaltestpoint Support Team.</p>
<p><a href="mailto:{{ (!empty($option_tys['email'])) ? $option_tys['email'] : 'info@globaltestpoint.com' }}">@if(!empty($option_tys['email'])) {{ $option_tys['email'] }} @else {{ 'info@globaltestpoint.com' }} @endif</a></p>
<p>{{ (!empty($option_tys['address'])) ? $option_tys['address'] : 'Baltimore MD, USA ' }}</p>