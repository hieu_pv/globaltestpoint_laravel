<p>Your payment of {{ $currency_symbol }}{{$exam['price']}} for Exam {{$exam['name']}} on globaltestpoint was successful.</p>
<p>
	<a href="{{ url('testyourself/exam-start', $exam->slug) }}">Click here to proceed</a>
</p>
<p>Good luck in your test.</p>
<p>Yours,</p>
<p>Globaltestpoint Support Team.</p>
<p><a href="mailto:{{ (!empty($option_tys['email'])) ? $option_tys['email'] : 'info@globaltestpoint.com' }}">@if(!empty($option_tys['email'])) {{ $option_tys['email'] }} @else {{ 'info@globaltestpoint.com' }} @endif</a></p>
<p>{{ (!empty($option_tys['address'])) ? $option_tys['address'] : 'Baltimore MD, USA ' }}</p>
