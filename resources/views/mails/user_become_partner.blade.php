<div class="wrapper" style="background: #eee">
	<div class="container" style="width: 500px; margin: 0px auto; padding: 20px;">
		<div class="logo">
			<img src="{{asset('assets/frontend/images/testyourself/logo.png') }}" max-width="100px" max-height="100px">
		</div>
		<div style="background: #fff; overflow: auto; padding: 20px 10px;">
			<div style="text-align: center">
				<h3 style="margin-top: 50px; padding-bottom: 40px;">
					Hi {{$user->first_name}}, Welcome to www.globaltestpoint.com 
				</h3>
				<div style="text-align: left; padding: 10px 20px;">
					<p>Your request become a partner of www.globaltestpoint.com was accepted</p>
					<p>Now you can login with your account</strong></p>
				</div>
				<p>Click the tab below to login.</p>
				<a href="{{ url('/login') }}" style="cursor: pointer">
					<button style="padding: 10px 20px; background: #27ae60; color: #fff; font-size: 18px; border: none; box-shadow: none; border-radius: 0px">Login to your account</button>
				</a>
			</div>
			<div style="margin-top: 20px;">
				<p>Yours,</p>
				<p>Globaltestpoint Support Team.</p>
				<p><a href="mailto:{{ (!empty($option_tys['email'])) ? $option_tys['email'] : 'info@globaltestpoint.com' }}">@if(!empty($option_tys['email'])) {{ $option_tys['email'] }} @else {{ 'info@globaltestpoint.com' }} @endif</a></p>
				<p>{{ (!empty($option_tys['address'])) ? $option_tys['address'] : 'Baltimore MD, USA ' }}</p>
			</div>
		</div>
	</div>
</div>