<div class="wrapper" style="background: #eee">
    <div class="container" style="width: 500px; margin: 0px auto; padding: 20px;">
        <div class="logo">
            <img style="max-width: 100px; max-height: 100px" src="{{ $message->embed($user['image']) }}">
        </div>
        <div style="background: #fff; overflow: auto; padding: 20px 10px;">
            <div style="text-align: center">
                <h3 style="margin-top: 50px; padding-bottom: 40px;">
					Hi {{$user->first_name}}, Welcome to www.globaltestpoint.com
				</h3>
                <p>Your account has been created by admin.</p>
                <p>Your password is <strong>{{$password}}</strong></p>
                <p>Click the tab below to confirm your email address and get started.</p>
                <a href="{{ url('/verified/'.$user->email.'/'. $user->email_verified) }}" style="cursor: pointer">
                    <button style="padding: 20px 40px; background: #27ae60; color: #fff; font-size: 18px; border: none; box-shadow: none; border-radius: 0px">Activate your account</button>
                </a>
            </div>
            <hr>
            <p style="color: #999">If the above button does not work please follow the url:</p>
            <a href="{{ url('/verified/'.$user->email.'/'. $user->email_verified) }}">{{ url('/verified/'.$user->email.'/'. $user->email_verified) }}</a>
            <div style="margin-top: 20px; text-align: left; padding: 10px 20px;">
                <p>At <a href="http://globaltestpoint.com/">globaltestpoint.com</a>, you can:</p>
                <ul>
                    <li>Practice for your Exam by taking some carefully selected questions. You will be timed as true Exam situation is simulated.</li>
                    <li>You can also be able to review your questions and see solutions plus explanations.</li>
                    <li>Practice job aptitude test for various fields.</li>
                    <li>Have access to sample job interview questions and interview tips</li>
                    <li>Be informed of latest job opening(s) and vacancies</li>
                    <li>Download a lot of e-Books, manuals and articles</li>
                    <li>Learn a lot of new things that will make you a better person</li>
                    <li>You can also be our partner or our affiliate</li>
                </ul>
                <p>Yours,</p>
                <p>Globaltestpoint Support Team.</p>
                <p><a href="mailto:{{ (!empty($option_tys['email'])) ? $option_tys['email'] : 'info@globaltestpoint.com' }}">@if(!empty($option_tys['email'])) {{ $option_tys['email'] }} @else {{ 'info@globaltestpoint.com' }} @endif</a></p>
                <p>{{ (!empty($option_tys['address'])) ? $option_tys['address'] : 'Baltimore MD, USA' }}.</p>
            </div>
        </div>
    </div>
</div>
