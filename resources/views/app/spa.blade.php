<!DOCTYPE html>
<html ng-app="app">

<head>
    <title ng-bind="title"></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=0.5, maximum-scale=1.0" />
    <meta ng-if="description" name="description" content="@{{description}}" />
    <meta NAME="ROBOTS" CONTENT="NOINDEX, FOLLOW" />
    <link rel="icon" type="image/ico" href="{!! url('assets/frontend/images/button_home.png') !!}">
    <script type="text/javascript">
    var BASE_URL = "{!! asset('') !!}";
    var GOOGLE_MAP_API_KEY = "{{getenv('GOOGLE_API_KEY')}}";
    var JWT_TOKEN_COOKIE_KEY = "ushift_jwt_token_admin";
    var USER_JWT_TOKEN_COOKIE_KEY = "ushift_jwt_token_v2";
    var STRIPE_PUBLISHABLE_KEY = "{{getenv('STRIPE_PUBLISHABLE_KEY')}}";
    var FACEBOOK_CLIENT_ID = "{{getenv('FACEBOOK_CLIENT_ID')}}";
    var GOOGLE_CLIENT_ID = "{{getenv('GOOGLE_CLIENT_ID')}}";
    var LINKEDIN_CLIENT_ID = "{{getenv('LINKEDIN_CLIENT_ID')}}";
    var PUSHER_APP_KEY = "{{getenv('PUSHER_APP_KEY')}}";
    var PUSHER_APP_CLUSTER = "{{getenv('PUSHER_APP_CLUSTER')}}";
    var AllowMaxFileSize = 16;
    var AppTimeZone = "{{env('APP_TIME_ZONE', 'Asia/Singapore')}}";
    var ADMIN_SESSION = 'admin_session';
    var ADMIN_PROFILE = 'admin_profile';
    var STRIPE_CURRENCY_SYMBOL = "{{getenv('STRIPE_CURRENCY_SYMBOL')}}";
    </script>
    <!-- <script type="text/javascript" src="/assets/js/outdated.js" async></script> -->
    @if(getenv('APP_ENV') == 'production')
    <link rel="stylesheet" href="{!! asset('assets/css/app.all.min.css?v=' . $buster['assets/css/app.all.min.css']) !!}">
    <script src="{!! asset('assets/js/app.min.js?v=' . $buster['assets/js/app.min.js']) !!}" async></script>
    @else
    <link rel="stylesheet" href="{!! asset('assets/css/app.all.css') !!}">
    <script src="{!! asset('assets/js/app.js') !!}" async></script>
    @endif
</head>

<body ui-view="main" ng-cloak></body>
<!-- <body ui-view="main" ng-class="{'no-padding-top': isStateOrChildOf('user'), 'body-admin-login-as-user': admin_session}"></body> -->

</html>