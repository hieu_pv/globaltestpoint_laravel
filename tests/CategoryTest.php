<?php

use App\Entities\Category;

class CategoryTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->categories = Category::get();
        $this->category   = Category::create([
            'name'      => 'Test' . mt_rand(0, 1000),
            'slug'      => str_slug('Academic Test' . mt_rand(0, 1000)),
            'type'      => 'Exam',
            'parent_id' => 0,
            'order'     => 99,
            'page_id'   => 2,
        ]);
    }
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testHasCategory()
    {
        $this->assertNotEmpty($this->categories);
    }

    public function testCreateNewCategory()
    {
        $this->assertInstanceOf(Category::class, $this->category);
    }
    public function testDeleteCategory()
    {
        $this->assertTrue($this->category->delete());
    }

}
