<?php

use App\Entities\Role;

class RoleTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();
    }
    /**
     * Applicaion need 3 roles: admin, user and tester to run correctly
     *
     * @return void
     */
    public function testHasAdminRole()
    {
        $this->assertNotEmpty(Role::where('slug', 'admin')->first());
    }
    public function testHasUserRole()
    {
        $this->assertNotEmpty(Role::where('slug', 'user')->first());
    }
    public function testHasTesterRole()
    {
        $this->assertNotEmpty(Role::where('slug', 'tester')->first());
    }
}
