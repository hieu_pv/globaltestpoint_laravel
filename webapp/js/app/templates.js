(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/app/components/job-vacancies/_job-vacancies.tpl.html',
    '<div id="job-vacancies-page" ui-view="job-vacancies"></div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/app/components/login/_login.tpl.html',
    '<div id="login">\n' +
    '    <div class="container">\n' +
    '        <div class="col-xs-12">\n' +
    '            <h4 class="page-title text-center animated fadeInUp">Welcome back, friend</h4>\n' +
    '            <div class="login-form animated fadeInUp">\n' +
    '                <form name="vm.login_form" ng-submit="vm.login_form.$valid && vm.login(vm.employee)" novalidate>\n' +
    '                    <div class="form-group float-input">\n' +
    '                        <label>Email *</label>\n' +
    '                        <input type="email" ng-focus="vm.login_form.email.$focused = true" class="form-control" name="email" ng-model="vm.employee.email" required>\n' +
    '                        <div class="icons" ng-show="vm.login_form.email.$focused">\n' +
    '                            <i ng-if="!vm.login_form.email.$error.required && !vm.login_form.email.$error.email" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                            <i ng-if="vm.login_form.email.$error.required || vm.login_form.email.$error.email" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                        </div>\n' +
    '                        <div class="errors">\n' +
    '                            <span ng-show="vm.login_form.$submitted && vm.login_form.email.$error.required">Required</span>\n' +
    '                            <span ng-show="vm.login_form.$submitted && vm.login_form.email.$error.email">Please enter a valid email address</span>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="form-group float-input">\n' +
    '                        <label>Password *</label>\n' +
    '                        <input type="password" ng-focus="vm.login_form.password.$focused = true" name="password" class="form-control" ng-model="vm.employee.password" ng-maxlength="20" ng-minlength="6" required>\n' +
    '                        <div class="icons" ng-show="vm.login_form.password.$focused">\n' +
    '                            <i ng-if="!vm.login_form.password.$error.required && !vm.login_form.password.$error.maxlength && !vm.login_form.password.$error.minlength" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                            <i ng-if="vm.login_form.password.$error.required || vm.login_form.password.$error.maxlength || vm.login_form.password.$error.minlength" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                        </div>\n' +
    '                        <div class="errors">\n' +
    '                            <span ng-show="vm.login_form.$submitted && vm.login_form.password.$error.required">Required</span>\n' +
    '                            <span ng-show="vm.login_form.$submitted && vm.login_form.password.$error.maxlength">The value is too long</span>\n' +
    '                            <span ng-show="vm.login_form.$submitted && vm.login_form.password.$error.minlength">The value is too short</span>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="form-group">\n' +
    '                        <a class="forgot-password pull-right" ui-sref="user.password.forgot">Forgot Password?</a>\n' +
    '                    </div>\n' +
    '                    <div class="form-group">\n' +
    '                        <input type="submit" class="text-uppercase btn ui-btn btn-white btn-full-width" value="Login">\n' +
    '                    </div>\n' +
    '                    <div class="social-login">\n' +
    '                        <ul>\n' +
    '                            <li ng-click="vm.authenticate(\'facebook\')">\n' +
    '                                <div class="icon text-center">\n' +
    '                                    <i class="fa fa-facebook"></i>\n' +
    '                                </div>\n' +
    '                                <div class="text text-center">\n' +
    '                                    <span>Log In via Facebook</span>\n' +
    '                                </div>\n' +
    '                            </li>\n' +
    '                            <!-- <li ng-click="vm.authenticate(\'linkedin\')">\n' +
    '                                <div class="icon text-center">\n' +
    '                                    <i class="fa fa-linkedin"></i>\n' +
    '                                </div>\n' +
    '                                <div class="text text-center">\n' +
    '                                    <span>Log In via LinkedIn</span>\n' +
    '                                </div>\n' +
    '                            </li> -->\n' +
    '                            <li ng-click="vm.authenticate(\'google\')">\n' +
    '                                <div class="icon text-center">\n' +
    '                                    <i class="fa fa-google-plus"></i>\n' +
    '                                </div>\n' +
    '                                <div class="text text-center">\n' +
    '                                    <span>Log In via Google</span>\n' +
    '                                </div>\n' +
    '                            </li>\n' +
    '                        </ul>\n' +
    '                    </div>\n' +
    '                </form>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/app/common/partials/_application_paginator.tpl.html',
    '<div class="application-paginator">\n' +
    '    <ul>\n' +
    '        <li>\n' +
    '            <a ng-click="page = page - 1" ng-show="page > 1"><i class="material-icons">keyboard_arrow_left</i></a>\n' +
    '        </li>\n' +
    '        <li>Page <span ng-bind="page"></span> of <span ng-bind="data.getTotalPages()"></span></li>\n' +
    '        <li>\n' +
    '            <a ng-click="page = page + 1" ng-show="page < data.getTotalPages()"><i class="material-icons">keyboard_arrow_right</i></a>\n' +
    '        </li>\n' +
    '    </ul>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/app/common/partials/_beta_teseter.tpl.html',
    '<section id="get-a-call" class="hidden">\n' +
    '    <div class="popup-headline">\n' +
    '        <!--   <div class="pp-logo">\n' +
    '            <img src="/assets/images/logo-red.svg">\n' +
    '        </div> -->\n' +
    '        <div class="pp-close" ng-click="hideRightConnerPopup()">\n' +
    '            <i class="fa fa-minus-square-o"></i>\n' +
    '        </div>\n' +
    '        <div class="popup-headline">\n' +
    '            <h3>We launch the App soon!</h3>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="popup-content">\n' +
    '        <p class="text-center">\n' +
    '            Tell us if you want to Test the App before the launch, give us feedback about your experience and\n' +
    '            <br>Get Rewarded!\n' +
    '        </p>\n' +
    '        <div class="pp-form">\n' +
    '            <form method="post" name="get_the_app_form" ng-submit="get_the_app_form.$valid && becomeBetaTester(get_the_app)" novalidate>\n' +
    '                <div class="pull-right">\n' +
    '                    <input type="submit" class="ui-btn btn-orange btn-small" value="Become Beta Tester!">\n' +
    '                </div>\n' +
    '                <div class="form-group">\n' +
    '                    <label>+65</label>\n' +
    '                    <input type="text" class="form-control" name="phone_number" ng-model="get_the_app.phone_number" placeholder="..." ng-pattern="/^\\d+$/" required ng-maxlength="20">\n' +
    '                    <div class="errors-wrapper">\n' +
    '                        <div class="errors">\n' +
    '                            <span ng-show="get_the_app_form.$submitted && get_the_app_form.phone_number.$error.required">Required</span>\n' +
    '                            <span ng-show="get_the_app_form.$submitted && get_the_app_form.phone_number.$error.maxlength">The value is too long</span>\n' +
    '                            <span ng-show="get_the_app_form.$submitted && get_the_app_form.phone_number.$error.pattern">Please enter a valid phone number</span>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </form>\n' +
    '        </div>\n' +
    '        <p class="text-center">\n' +
    '            Win 2 cinema tickets ;)\n' +
    '        </p>\n' +
    '    </div>\n' +
    '</section>\n' +
    '\n' +
    '<section id="get-a-call-small" class="hidden">\n' +
    '    <div class="popup-content">\n' +
    '        <a ng-click="showBetaTestAppPopup()">\n' +
    '            <img src="/assets/images/logo-white.png">\n' +
    '        </a>\n' +
    '    </div>\n' +
    '</section>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/app/common/partials/_preloader.tpl.html',
    '<div class="preloader-wrapper active small">\n' +
    '    <div class="spinner-layer spinner-red-only">\n' +
    '        <div class="circle-clipper left">\n' +
    '            <div class="circle"></div>\n' +
    '        </div>\n' +
    '        <div class="gap-patch">\n' +
    '            <div class="circle"></div>\n' +
    '        </div>\n' +
    '        <div class="circle-clipper right">\n' +
    '            <div class="circle"></div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/app/common/partials/_simple_pagination.tpl.html',
    '<ul class="pagination right pagination-box">\n' +
    '    <li ng-if="page != 1" ng-click="prev(); callbackPrev(page)">\n' +
    '    	<a href="javascript:;"><i class="glyphicon glyphicon-chevron-left"></i></a>\n' +
    '    </li>\n' +
    '    <li class="active">\n' +
    '        Page <span ng-bind="page"></span>\n' +
    '    </li>\n' +
    '    <li class="waves-effect" ng-click="next(); callbackNext(page)">\n' +
    '    	<a><i class="glyphicon glyphicon-chevron-right"></i></a>\n' +
    '    </li>\n' +
    '</ul>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/app/common/partials/breadcrumb.tpl.html',
    '<ol class="breadcrumb" ng-if="data.length > 0">\n' +
    '    <li ng-repeat="(key, item) in data" ng-class="{\'active\': !item.state}">\n' +
    '    	<a ng-if="item.state" ui-sref="{{item.state}}({{item.options}})" ui-sref-opts="({inherit: false})" ng-bind="item.title"></a>\n' +
    '    	<span ng-if="!item.state" ng-bind="item.title"></span>\n' +
    '    </li>\n' +
    '</ol>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/app/common/partials/pagination.tpl.html',
    '<ul class="pagination pull-right" ng-show="data.getTotalPages() > 1">\n' +
    '    <li ng-if="currentPage > 1" ng-click="goto(currentPage - 1)">\n' +
    '        <a aria-label="Previous"><span aria-hidden="true">Prev</span></a>\n' +
    '    </li>\n' +
    '    <li ng-repeat="page in pages" ng-click="goto(page)" ng-class="{active: page === currentPage}">\n' +
    '        <a ng-bind="page"></a>\n' +
    '    </li>\n' +
    '    <li ng-if="currentPage < data.getTotalPages()" ng-click="goto(currentPage + 1)">\n' +
    '        <a aria-label="Next"><span aria-hidden="true">Next</span></a>\n' +
    '    </li>\n' +
    '</ul>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/app/components/job-vacancies/detail/_detail.tpl.html',
    '<div class="full-wrap detail-job-page">\n' +
    '    <div class="bg-wrap"></div>\n' +
    '    <div class="container custom-container">\n' +
    '        <breadcrumb data="vm.breadcrumbs"></breadcrumb>\n' +
    '        <div class="row">\n' +
    '            <div class="left-block col-sm-9">\n' +
    '                <div class="job-information-action">\n' +
    '                    <div class="information">\n' +
    '                        <h1 class="job-title" ng-bind="vm.job.getTitle()"></h1>\n' +
    '                        <h2 class="company-name" ng-bind="vm.job.employer.employer.getCompanyName()"></h2>\n' +
    '                        <div class="experience-location">\n' +
    '                            <div class="experience">\n' +
    '                                <i class="icon fa fa-suitcase"></i>\n' +
    '                                <span class="text" ng-bind="vm.job.experience.getLabel()"></span>\n' +
    '                            </div>\n' +
    '                            <div class="salary">\n' +
    '                                <i class="icon fa fa-money"></i>\n' +
    '                                <span class="text">{{ vm.STRIPE_CURRENCY_SYMBOL }}{{ vm.job.getMinSalary() }} - {{ vm.STRIPE_CURRENCY_SYMBOL }}{{ vm.job.getMaxSalary() }}</span>\n' +
    '                            </div>\n' +
    '                            <div class="location">\n' +
    '                                <i class="icon fa fa-map-marker"></i>\n' +
    '                                <span class="text" ng-if="vm.job.city">{{ vm.job.city.getName() }} - {{ vm.job.country.getName() }}</span>\n' +
    '                                <span class="text" ng-if="!vm.job.city">{{ vm.job.getLocation() }}</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="education-nationality-gender">\n' +
    '                            <div class="education">\n' +
    '                                <i class="icon fa fa-graduation-cap"></i>\n' +
    '                                <span class="text" ng-bind="vm.job.getCertificate()"></span>\n' +
    '                            </div>\n' +
    '                            <div class="nationality">\n' +
    '                                <i class="icon fa fa-globe"></i>\n' +
    '                                <span class="text" ng-bind="vm.job.nationality.getName()"></span>\n' +
    '                            </div>\n' +
    '                            <div class="gender" ng-if="vm.job.gender">\n' +
    '                                <i class="icon fa fa-user"></i>\n' +
    '                                <span class="text" ng-bind="vm.job.getGender()"></span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="action-wrapper">\n' +
    '                        <div class="login-wrapper" ng-if="!user">\n' +
    '                            <a href="{{ asset(\'/login\') }}" force-reload class="ui-btn btn-blue-sky login-button">Login To Apply</a>\n' +
    '                        </div>\n' +
    '                        <div class="register-wrapper" ng-if="!user">\n' +
    '                            <a href="{{ asset(\'/sign_up\') }}" force-reload class="ui-btn btn-red register-button">Register & Apply</a>\n' +
    '                        </div>\n' +
    '                        <div class="apply-wrapper" ng-if="user">\n' +
    '                            <a href="#" class="ui-btn btn-red apply-button" ng-click="featureComingSoon()">Apply</a>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="job-description-wrapper">\n' +
    '                    <div class="information">\n' +
    '                        <h3 class="title pull-left">Job Description</h3>\n' +
    '                        <div class="email-share pull-right">\n' +
    '                            <a href="#" class="share" ng-click="featureComingSoon()">Share</a>\n' +
    '                        </div>\n' +
    '                        <div class="clearfix"></div>\n' +
    '                        <div ng-bind="vm.job.getDescription()"></div>\n' +
    '                    </div>\n' +
    '                    <div class="industry-functional" ng-if="vm.job.industries | hasItem">\n' +
    '                        <div class="industry">\n' +
    '                            <span class="title">Industry Type :</span>\n' +
    '                            <a class="link" ui-sref="app.job-vacancies.list({page: 1, keyword: industry.getName()})" ng-repeat="industry in vm.job.industries" ng-bind="industry.getName()"></a>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="desired-candidate-profile-wrapper">\n' +
    '                    <h3 class="title">Desired Candidate Profile</h3>\n' +
    '                    <div ng-bind="vm.job.getDesiredCandidateProfile()"></div>\n' +
    '                </div>\n' +
    '                <div class="keywords-wrapper" ng-if="vm.job.tags | hasItem">\n' +
    '                    <h3 class="title">Keywords</h3>\n' +
    '                    <div class="keywords">\n' +
    '                        <span ng-repeat="tag in vm.job.tags" ng-bind="tag.getName()"></span>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="all-jobs">\n' +
    '                    <a href="javascript:;" class="ui-btn btn-red register-button pull-right" ng-click="featureComingSoon()">All Jobs</a>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="right-block col-sm-3">\n' +
    '                <div class="company-wrapper">\n' +
    '                    <h3 class="company-name" ng-bind="vm.job.employer.employer.getCompanyName()"></h3>\n' +
    '                    <div class="company-info company-contact">\n' +
    '                        <div class="heading">Contact</div>\n' +
    '                        <div class="sub-heading">Name/ Designation:</div>\n' +
    '                        <div class="text" ng-bind="vm.job.employer.employer.getContactName()"></div>\n' +
    '                    </div>\n' +
    '                    <div class="company-info company-address">\n' +
    '                        <div class="heading">Address: </div>\n' +
    '                        <div class="text" ng-bind="vm.job.employer.employer.getCompanyAddress()"></div>\n' +
    '                    </div>\n' +
    '                    <div class="company-info company-mobile">\n' +
    '                        <div class="heading">Mob.: </div>\n' +
    '                        <div class="text" ng-bind="vm.job.employer.getPhone()"></div>\n' +
    '                    </div>\n' +
    '                    <div class="company-info company-website">\n' +
    '                        <div class="heading">Website</div>\n' +
    '                        <div class="text">\n' +
    '                            <a ng-href="{{ vm.job.employer.employer.getWebsite() }}" target="_blank">{{ vm.job.employer.employer.getWebsite() }}</a>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="advertisements-wrapper" ng-if="vm.advertisements | hasItem">\n' +
    '                    <div class="advertisements row">\n' +
    '                        <div class="advertisement-item col-sm-12" ng-repeat="advertisement in vm.advertisements">\n' +
    '                            <a ng-href="{{ advertisement.getUrl() }}" target="_blank">\n' +
    '                                <img ng-src="{{ asset(\'/uploads/3x2.png\')}}" alt="{{ advertisement.getName() }}" style="background-image: url({{ advertisement.getImage() }}); background-size: cover;">\n' +
    '                            </a>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="similar-jobs-wrapper" ng-if="vm.similar_jobs | hasItem">\n' +
    '                    <div class="title-view-all">\n' +
    '                        <h3 class="title pull-left">Similar Jobs</h3>\n' +
    '                        <a href="#" class="view-all pull-right" ng-click="featureComingSoon()">View All</a>\n' +
    '                    </div>\n' +
    '                    <div class="list-jobs">\n' +
    '                        <a ui-sref="app.job-vacancies.detail({slug: job.getSlug()})" class="job-wrapper" ng-repeat="job in vm.similar_jobs">\n' +
    '                            <div class="job">\n' +
    '                                <h5 class="job-title" ng-bind="job.getTitle()"></h5>\n' +
    '                                <h6 class="company-name" ng-bind="job.employer.employer.getCompanyName()"></h6>\n' +
    '                                <div class="experience">\n' +
    '                                    <i class="icon fa fa-suitcase"></i>\n' +
    '                                    <span class="text" ng-bind="job.experience.getLabel()"></span>\n' +
    '                                </div>\n' +
    '                                <div class="location">\n' +
    '                                    <i class="icon fa fa-map-marker"></i>\n' +
    '                                    <span class="text" ng-if="job.city">{{ job.city.getName() }} - {{ job.country.getName() }}</span>\n' +
    '                                    <span class="text" ng-if="!job.city">{{ job.getLocation() }}</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </a>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/app/components/job-vacancies/list/_list.tpl.html',
    '<div class="list-jobs-page">\n' +
    '    <div class="full-wrap search-job">\n' +
    '        <div class="bg-wrap"></div>\n' +
    '        <div class="container custom-container">\n' +
    '            <breadcrumb data="vm.breadcrumbs"></breadcrumb>\n' +
    '            <div class="adv-search">\n' +
    '                <form name="vm.search_form" ng-submit="vm.searchJobs(vm.search)" novalidate>\n' +
    '                    <div class="keyword-section suggest">\n' +
    '                        <span class="heading">\n' +
    '                        <label for="keyword">Enter Keywords</label>\n' +
    '                    </span>\n' +
    '                        <div class="keyword-wrap">\n' +
    '                            <div class="input-wrap">\n' +
    '                                <input type="text" id="keyword" name="keyword" placeholder="Enter job titles" ng-model="vm.search.keyword">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="location-section suggest">\n' +
    '                        <span class="heading">\n' +
    '                        <label for="location">Location</label>\n' +
    '                    </span>\n' +
    '                        <div class="location-wrap">\n' +
    '                            <div class="input-wrap">\n' +
    '                                <ui-select ng-model="vm.search.location" title="Choose a location">\n' +
    '                                    <ui-select-match placeholder="City">{{ $select.selected.name }}</ui-select-match>\n' +
    '                                    <ui-select-choices repeat="city in vm.cities | filter: $select.search">\n' +
    '                                        <span ng-bind="city.name"></span>\n' +
    '                                    </ui-select-choices>\n' +
    '                                </ui-select>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="experience-section">\n' +
    '                        <span class="heading">\n' +
    '                        <label for="experience">Experience</label>\n' +
    '                    </span>\n' +
    '                        <select name="experience" id="experience" class="form-group" ng-model="vm.search.experience" ng-options="option as option.label for option in vm.search_experiences">\n' +
    '                        </select>\n' +
    '                    </div>\n' +
    '                    <div class="submit-section">\n' +
    '                        <span class="heading">&nbsp;</span>\n' +
    '                        <input type="submit" value="Search" class="btn red">\n' +
    '                    </div>\n' +
    '                </form>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="content">\n' +
    '        <div class="container custom-container">\n' +
    '            <div class="row">\n' +
    '                <div class="refine-search-wrapper col-sm-3 col-xs-3">\n' +
    '                    <h4 class="search-heading">Refine Search</h4>\n' +
    '                    <a href="#" class="reset-all" ng-if="vm.show_reset_all_filter_button" ng-click="vm.resetAllFilter()">Reset All</a>\n' +
    '                    <div class="filters" id="job-filter-wrapper">\n' +
    '                        <div class="filter-wrapper filter-industry" ng-if="vm.raw_industries | hasItem">\n' +
    '                            <h5 class="filter-heading">Industry</h5>\n' +
    '                            <a href="javascript:;" id="reset-filter-location" class="reset" ng-if="vm.params.filter_industry_ids" ng-click="vm.resetFilter(\'filter_industry_ids\')">Reset</a>\n' +
    '                            <div class="filter-options">\n' +
    '                                <div class="option" ng-repeat="industry in vm.industries">\n' +
    '                                    <input type="checkbox" name="industry_{{ industry.getId() }}" id="industry_{{ industry.getId() }}" ng-model="industry.selected" ng-true-value="true" ng-false-value="false" ng-change="vm.checkDisplayRefineSearch()">\n' +
    '                                    <label class="text">\n' +
    '                                        <label for="industry_{{ industry.getId() }}" title="{{ industry.getName() }}" ng-bind="industry.getName()"></label>\n' +
    '                                    </label>\n' +
    '                                    <span class="total-jobs" ng-bind="industry.getTotalJobs()"></span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="filter-wrapper filter-location" ng-if="vm.raw_states | hasItem">\n' +
    '                            <h5 class="filter-heading clickable" ng-class="{\'arrow-right\': vm.toggle.states}" ng-click="vm.toggleFilter(\'states\')">Location</h5>\n' +
    '                            <a href="javascript:;" id="reset-filter-location" class="reset" ng-if="vm.params.filter_state_ids" ng-click="vm.resetFilter(\'filter_state_ids\')">Reset</a>\n' +
    '                            <div class="filter-options">\n' +
    '                                <div class="option" ng-repeat="state in vm.states">\n' +
    '                                    <input type="checkbox" name="location_{{ state.getId() }}" id="location_{{ state.getId() }}" ng-model="state.selected" ng-true-value="true" ng-false-value="false" ng-change="vm.checkDisplayRefineSearch()">\n' +
    '                                    <label class="text">\n' +
    '                                        <label for="location_{{ state.getId() }}" title="{{ state.getName() }}" ng-bind="state.getName()"></label>\n' +
    '                                    </label>\n' +
    '                                    <span class="total-jobs" ng-bind="state.getTotalJobs()"></span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="filter-wrapper filter-job-titles" ng-if="vm.raw_job_titles | hasItem">\n' +
    '                            <h5 class="filter-heading">Job Titles</h5>\n' +
    '                            <a href="javascript:;" id="reset-filter-location" class="reset" ng-if="vm.params.filter_job_title_ids" ng-click="vm.resetFilter(\'filter_job_title_ids\')">Reset</a>\n' +
    '                            <div class="filter-options">\n' +
    '                                <div class="option" ng-repeat="job_title in vm.job_titles">\n' +
    '                                    <input type="checkbox" name="job_title_{{ job_title.getId() }}" id="job_title_{{ job_title.getId() }}" ng-model="job_title.selected" ng-true-value="true" ng-false-value="false" ng-change="vm.checkDisplayRefineSearch()">\n' +
    '                                    <label class="text">\n' +
    '                                        <label for="job_title_{{ job_title.getId() }}" title="{{ job_title.getName() }}" ng-bind="job_title.getName()"></label>\n' +
    '                                    </label>\n' +
    '                                    <span class="total-jobs" ng-bind="job_title.getTotalJobs()"></span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="filter-wrapper filter-qualification" ng-if="vm.raw_qualifications | hasItem">\n' +
    '                            <h5 class="filter-heading">Qualification</h5>\n' +
    '                            <a href="javascript:;" id="reset-filter-location" class="reset" ng-if="vm.params.filter_qualification_ids" ng-click="vm.resetFilter(\'filter_qualification_ids\')">Reset</a>\n' +
    '                            <div class="filter-options">\n' +
    '                                <div class="option" ng-repeat="qualification in vm.qualifications">\n' +
    '                                    <input type="checkbox" name="qualification_{{ qualification.getId() }}" id="qualification_{{ qualification.getId() }}" ng-model="qualification.selected" ng-true-value="true" ng-false-value="false" ng-change="vm.checkDisplayRefineSearch()">\n' +
    '                                    <label class="text">\n' +
    '                                        <label for="qualification_{{ qualification.getId() }}" title="{{ qualification.getName() }}" ng-bind="qualification.getName()"></label>\n' +
    '                                    </label>\n' +
    '                                    <span class="total-jobs" ng-bind="qualification.getTotalJobs()"></span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="filter-wrapper filter-experience" ng-if="vm.raw_experiences | hasItem">\n' +
    '                            <h5 class="filter-heading">Experience</h5>\n' +
    '                            <a href="javascript:;" id="reset-filter-location" class="reset" ng-if="vm.params.filter_experience_ids" ng-click="vm.resetFilter(\'filter_experience_ids\')">Reset</a>\n' +
    '                            <div class="filter-options">\n' +
    '                                <div class="option" ng-repeat="experience in vm.experiences">\n' +
    '                                    <input type="checkbox" name="experience_{{ experience.getId() }}" id="experience_{{ experience.getId() }}" ng-model="experience.selected" ng-true-value="true" ng-false-value="false" ng-change="vm.checkDisplayRefineSearch()">\n' +
    '                                    <label class="text">\n' +
    '                                        <label for="experience_{{ experience.getId() }}" title="{{ experience.getLabel() }}" ng-bind="experience.getLabel()"></label>\n' +
    '                                    </label>\n' +
    '                                    <span class="total-jobs" ng-bind="experience.getTotalJobs()"></span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="refine-button fixed" fixed-refine-button>\n' +
    '                            <button type="button" ng-click="vm.filterJobs()" ng-show="vm.show_refine_button">Refine</button>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="normal-international-jobs-wrapper col-sm-6 col-xs-9">\n' +
    '                    <div class="headings">\n' +
    '                        <h4 class="search-result" ng-bind="vm.search_result_message"></h4>\n' +
    '                        <div class="jobs-found-sortby">\n' +
    '                            <span class="jobs-found" ng-if="vm.jobs.length === 0">Oops! No jobs found with your search criteria</span>\n' +
    '                            <span class="jobs-found col-sm-6" ng-if="vm.jobs.length === 1">{{ vm.jobs.length }} Job Found</span>\n' +
    '                            <span class="jobs-found" ng-if="vm.jobs.length > 1">{{ vm.jobs.length }} Jobs Found</span>\n' +
    '                            <div class="sortby">\n' +
    '                                <a href="#">Sort By</a>\n' +
    '                                <div class="data">\n' +
    '                                    <a href="javascript:;" ng-class="{\'checked\': vm.order_by_key !== \'created_at\'}" ng-click="vm.sortBy(\'relevance\')">Relevance</a>\n' +
    '                                    <a href="javascript:;" ng-class="{\'checked\': vm.order_by_key === \'created_at\'}" ng-click="vm.sortBy(\'created_at\')">Date</a>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="list-jobs" ng-if="vm.jobs | hasItem">\n' +
    '                        <div class="job-wrapper">\n' +
    '                            <div class="job" ng-repeat="job in vm.jobs">\n' +
    '                                <div class="information" ng-class="{\'has-company-logo\': job.employer.getAvatar()}">\n' +
    '                                    <div class="job-title">\n' +
    '                                        <a ui-sref="app.job-vacancies.detail({slug: job.getSlug()})" target="_blank">\n' +
    '                                            <h2 title="{{ job.getTitle() }}" ng-bind="job.getTitle()"></h2>\n' +
    '                                        </a>\n' +
    '                                    </div>\n' +
    '                                    <div class="company-name" ng-bind="job.employer.employer.getCompanyName()"></div>\n' +
    '                                    <div class="experience-location">\n' +
    '                                        <div class="experience">\n' +
    '                                            <i class="icon fa fa-suitcase"></i>\n' +
    '                                            <span class="text" ng-bind="job.experience.getLabel()"></span>\n' +
    '                                        </div>\n' +
    '                                        <div class="location">\n' +
    '                                            <i class="icon fa fa-map-marker"></i>\n' +
    '                                            <span class="text">{{ job.city.getName() }} - {{ job.country.getName() }}</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="company-logo" ng-if="job.employer.getAvatar()">\n' +
    '                                    <img ng-src="{{ job.employer.getAvatar() }}" title="{{ job.getTitle() }}">\n' +
    '                                </div>\n' +
    '                                <div class="keyword-postedOn">\n' +
    '                                    <div class="keyword">\n' +
    '                                        <span ng-bind="job.display_tag"></span>\n' +
    '                                    </div>\n' +
    '                                    <div class="postedOn">Posted {{ job.getCreatedAt() | timeFormat: \'DD MMM YYYY\' }}</div>\n' +
    '                                </div>\n' +
    '                                <div class="options">\n' +
    '                                    <i class="icon fa fa-angle-down"></i>\n' +
    '                                    <div class="menu">\n' +
    '                                        <a href="#" class="view-similar-jobs">View similar jobs</a>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <pagination data="vm.pagination"></pagination>\n' +
    '                    </div>\n' +
    '                    <div class="international-jobs" ng-if="vm.international_jobs | hasItem">\n' +
    '                        <h4 class="heading">Some international job maybe you want to see</h4>\n' +
    '                        <div class="job-wrapper">\n' +
    '                            <div class="job" ng-repeat="job in vm.international_jobs">\n' +
    '                                <div class="information" ng-class="{\'has-company-logo\': job.employer.getAvatar()}">\n' +
    '                                    <div class="job-title">\n' +
    '                                        <a ui-sref="app.job-vacancies.detail({slug: job.getSlug()})" target="_blank">\n' +
    '                                            <h2 title="{{ job.getTitle() }}" ng-bind="job.getTitle()"></h2>\n' +
    '                                        </a>\n' +
    '                                    </div>\n' +
    '                                    <div class="company-name" ng-bind="job.employer.employer.getCompanyName()"></div>\n' +
    '                                    <div class="experience-location">\n' +
    '                                        <div class="experience">\n' +
    '                                            <i class="icon fa fa-suitcase"></i>\n' +
    '                                            <span class="text" ng-bind="job.experience.getLabel()"></span>\n' +
    '                                        </div>\n' +
    '                                        <div class="location">\n' +
    '                                            <i class="icon fa fa-map-marker"></i>\n' +
    '                                            <span class="text">{{ job.getLocation() }}</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="company-logo" ng-if="job.employer.getAvatar()">\n' +
    '                                    <img ng-src="{{ job.employer.getAvatar() }}" title="{{ job.getTitle() }}">\n' +
    '                                </div>\n' +
    '                                <div class="keyword-postedOn">\n' +
    '                                    <div class="keyword">\n' +
    '                                        <span ng-bind="job.display_tag"></span>\n' +
    '                                    </div>\n' +
    '                                    <div class="postedOn">Posted {{ job.getCreatedAt() | timeFormat: \'DD MMM YYYY\' }}</div>\n' +
    '                                </div>\n' +
    '                                <div class="options">\n' +
    '                                    <i class="icon fa fa-angle-down"></i>\n' +
    '                                    <div class="menu">\n' +
    '                                        <a href="#" class="view-similar-jobs" ng-click="featureComingSoon()">View similar jobs</a>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="includes-jobs-from-wrapper col-sm-3 col-xs-12">\n' +
    '                    <div class="above-advertisements" ng-if="vm.above_advertisements | hasItem">\n' +
    '                        <div class="advertisements row">\n' +
    '                            <div class="advertisement-item col-sm-12 col-xs-6" ng-repeat="advertisement in vm.above_advertisements">\n' +
    '                                <a ng-href="{{ advertisement.getUrl() }}" target="_blank">\n' +
    '                                <img ng-src="{{ asset(\'/uploads/3x2.png\')}}" alt="{{ advertisement.getName() }}" style="background-image: url({{ advertisement.getImage() }}); background-size: cover;">\n' +
    '                            </a>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="related-searches-wrapper" ng-if="vm.params.location && vm.job_titles | hasItem">\n' +
    '                        <div class="heading">Related Searches</div>\n' +
    '                        <div class="related-searches">\n' +
    '                            <div class="related-job" ng-repeat="job_title in vm.job_titles">\n' +
    '                                <a ui-sref="app.job-vacancies.list({page: 1, location: vm.params.location, keyword: job_title.getName()})">{{ job_title.getName() }} Jobs in {{ vm.params.location }}</a>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="under-advertisements" ng-if="vm.under_advertisements | hasItem">\n' +
    '                        <div class="advertisements row">\n' +
    '                            <div class="advertisement-item col-sm-12 col-xs-6" ng-repeat="advertisement in vm.under_advertisements">\n' +
    '                                <a ng-href="{{ advertisement.getUrl() }}" target="_blank">\n' +
    '                                <img ng-src="{{ asset(\'/uploads/3x2.png\')}}" alt="{{ advertisement.getName() }}" style="background-image: url({{ advertisement.getImage() }}); background-size: cover;">\n' +
    '                            </a>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="social">\n' +
    '                        <p class="social-heading">Share</p>\n' +
    '                        <a ng-if="web_info.getFacebook() !== \'\'" class="social-item social-facebook" ng-href="{{ web_info.getFacebook() }}" target="_blank">\n' +
    '                            <i class="icon fa fa-facebook"></i>\n' +
    '                        </a>\n' +
    '                        <a ng-if="web_info.getTwitter() !== \'\'" class="social-item social-twitter" ng-href="{{ web_info.getTwitter() }}" target="_blank">\n' +
    '                            <i class="icon fa fa-twitter"></i>\n' +
    '                        </a>\n' +
    '                        <a ng-if="web_info.getLinkedin() !== \'\'" class="social-item social-linkedin" ng-href="{{ web_info.getLinkedin() }}" target="_blank">\n' +
    '                            <i class="icon fa fa-linkedin"></i>\n' +
    '                        </a>\n' +
    '                        <!-- <a class="social-item social-google-plus" href="#">\n' +
    '                            <i class="icon fa fa-google-plus"></i>\n' +
    '                        </a> -->\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/app/common/partials/layouts/_footer.tpl.html',
    '<footer class="footer">\n' +
    '    <div class="scroll-top hide" scroll-top>\n' +
    '        <i class="fa fa-arrow-up icon"></i>\n' +
    '    </div>\n' +
    '    <div class="container custom-container">\n' +
    '        <div class="row">\n' +
    '            <section class="area-1 col-sm-3 col-xs-6 full-width-xs">\n' +
    '                <div class="link-item">\n' +
    '                    <a href="/testyourself/blank-page/your-cv" force-reload title="CV - Free Review">CV - Free Review</a>\n' +
    '                </div>\n' +
    '                <div class="link-item">\n' +
    '                    <a ui-sref="app.job-vacancies.list({page: 1})" title="Job Vacancies">Job Vacancies</a>\n' +
    '                </div>\n' +
    '                <div class="link-item">\n' +
    '                    <a href="/testyourself/exam-list/immigration-service" force-reload title="Job Aptitude Tests">Job Aptitude Tests</a>\n' +
    '                </div>\n' +
    '                <div class="link-item">\n' +
    '                    <a href="#" title="Job Interview Tips" ng-click="featureComingSoon()">Job Interview Tips</a>\n' +
    '                </div>\n' +
    '                <div class="link-item">\n' +
    '                    <a href="#" title="Scholarships" ng-click="featureComingSoon()">Scholarships</a>\n' +
    '                </div>\n' +
    '                <div class="link-item">\n' +
    '                    <a href="#" title="Cover Letter & Statement of Purpose" ng-click="featureComingSoon()">Cover Letter & Statement of Purpose</a>\n' +
    '                </div>\n' +
    '            </section>\n' +
    '            <section class="area-2 col-sm-3 col-xs-6 full-width-xs">\n' +
    '                <div class="link-item">\n' +
    '                    <a href="#" title="Business Plans" ng-click="featureComingSoon()">Business Plans</a>\n' +
    '                </div>\n' +
    '                <div class="link-item">\n' +
    '                    <a href="#" title="Bulk SMS Services" ng-click="featureComingSoon()">Bulk SMS Services</a>\n' +
    '                </div>\n' +
    '                <div class="link-item">\n' +
    '                    <a href="#" title="Data Services" ng-click="featureComingSoon()">Data Services</a>\n' +
    '                </div>\n' +
    '                <div class="link-item">\n' +
    '                    <a href="#" title="Marketing and Strategy" ng-click="featureComingSoon()">Marketing and Strategy</a>\n' +
    '                </div>\n' +
    '                <div class="link-item">\n' +
    '                    <a href="#" title="Consultancy Services" ng-click="featureComingSoon()">Consultancy Services</a>\n' +
    '                </div>\n' +
    '            </section>\n' +
    '            <section class="area-3 col-sm-3 col-xs-6 full-width-xs">\n' +
    '                <div class="link-item">\n' +
    '                    <a href="/testyourself/document/popular-downloads-1" title="eBooks" force-reload>eBooks</a>\n' +
    '                </div>\n' +
    '                <div class="link-item">\n' +
    '                    <a href="#" title="E-Books" ng-click="featureComingSoon()">E-Books</a>\n' +
    '                </div>\n' +
    '                <div class="link-item">\n' +
    '                    <a href="#" title="Softwares" ng-click="featureComingSoon()">Softwares</a>\n' +
    '                </div>\n' +
    '                <div class="link-item">\n' +
    '                    <a href="#" title="Business Proposals" ng-click="featureComingSoon()">Business Proposals</a>\n' +
    '                </div>\n' +
    '                <div class="link-item">\n' +
    '                    <a href="#" title="News and Information" ng-click="featureComingSoon()">News and Information</a>\n' +
    '                </div>\n' +
    '                <div class="link-item">\n' +
    '                    <a href="#" title="Career Advice" ng-click="featureComingSoon()">Career Advice</a>\n' +
    '                </div>\n' +
    '            </section>\n' +
    '            <section class="area-4 col-sm-3 col-xs-6 full-width-xs">\n' +
    '                <div class="follow-us-wrapper">\n' +
    '                    <div class="heading">Follow Us</div>\n' +
    '                    <div class="social social-wrapper">\n' +
    '                        <a ng-if="web_info.getFacebook() !== \'\'" class="social-item social-facebook" ng-href="{{ web_info.getFacebook() }}" target="_blank">\n' +
    '                            <i class="icon fa fa-facebook"></i>\n' +
    '                        </a>\n' +
    '                        <a ng-if="web_info.getYoutube() !== \'\'" class="social-item social-youtube" ng-href="{{ web_info.getYoutube() }}" target="_blank">\n' +
    '                            <i class="icon fa fa-youtube"></i>\n' +
    '                        </a>\n' +
    '                        <a ng-if="web_info.getInstagram() !== \'\'" class="social-item social-instagram" ng-href="{{ web_info.getInstagram() }}" target="_blank">\n' +
    '                            <i class="icon fa fa-instagram"></i>\n' +
    '                        </a>\n' +
    '                        <a ng-if="web_info.getTwitter() !== \'\'" class="social-item social-twitter" ng-href="{{ web_info.getTwitter() }}" target="_blank">\n' +
    '                            <i class="icon fa fa-twitter"></i>\n' +
    '                        </a>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </section>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="extra-links">\n' +
    '        <a href="#" ng-click="featureComingSoon()" title="Terms & Conditions and Privacy Policy">Terms & Conditions and Privacy Policy</a>\n' +
    '        <a href="/contact-us" force-reload title="Contact Us">Contact Us</a>\n' +
    '        <a href="/testyourself/show-faq" force-reload title="FAQs">FAQs</a>\n' +
    '    </div>\n' +
    '    <div class="copy-right">2017 © <a href="https://globaltestpoint.com/">www.globaltestpoint.com</a> All Rights Reserved.</div>\n' +
    '</footer>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/app/common/partials/layouts/_header.tpl.html',
    '<header class="header">\n' +
    '    <div class="container custom-container">\n' +
    '        <div class="logo">\n' +
    '            <a ui-sref="app.job-vacancies.list({page: 1})" ui-sref-opts="({inherit: false})">\n' +
    '				<img ng-src="{{ asset(\'/assets/frontend/images/testyourself/logo.png\')}}" alt="Globaltestpoint logo" />\n' +
    '			</a>\n' +
    '        </div>\n' +
    '        <div class="menu-toggle" toggle-menu selector=".menu-header">\n' +
    '            <span class="icon-bar"></span>\n' +
    '            <span class="icon-bar"></span>\n' +
    '            <span class="icon-bar"></span>\n' +
    '        </div>\n' +
    '        <nav class="menu-header">\n' +
    '            <ul>\n' +
    '                <li class="tab tab-jobs">\n' +
    '                    <a ui-sref="app.job-vacancies.list({page: 1})">\n' +
    '                        <i class="icon fa fa-suitcase"></i>\n' +
    '                        <span class="text">Jobs</span>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '                <li class="tab tab-interview-tips">\n' +
    '                    <a href="/testyourself/exam-list/promotion-exams-1" force-reload>\n' +
    '                        <i class="icon fa fa-cog"></i>\n' +
    '                        <span class="text">Interview Tips</span>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '                <li class="tab tab-aptitude-tests">\n' +
    '                    <a href="/testyourself/exam-list/immigration-service" force-reload>\n' +
    '                        <i class="icon fa fa-bolt"></i>\n' +
    '                        <span class="text">Aptitude Tests</span>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '                <li class="tab tab-cv-review">\n' +
    '                    <a href="/testyourself/blank-page/your-cv-free-review" force-reload>\n' +
    '						<i class="icon fa fa-hand-o-right"></i>\n' +
    '						<span class="text">CV Review</span>\n' +
    '					</a>\n' +
    '                </li>\n' +
    '                <li class="tab tab-ebooks">\n' +
    '                    <a href="/testyourself/blank-page/e-books" force-reload>\n' +
    '                        <i class="icon fa fa-book"></i>\n' +
    '                        <span class="text">Ebooks</span>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '                <li class="tab tab-login" ng-if="!user">\n' +
    '                    <a href="{{ asset(\'/login\') }}" force-reload>\n' +
    '						<i class="icon fa fa-lock"></i>\n' +
    '						<span class="text">Login</span>\n' +
    '					</a>\n' +
    '                </li>\n' +
    '                <li class="tab tab-register" ng-if="!user">\n' +
    '                    <a href="{{ asset(\'/sign_up\') }}" force-reload>\n' +
    '						<i class="icon fa fa-user-plus"></i>\n' +
    '						<span class="text">Register</span>\n' +
    '					</a>\n' +
    '                </li>\n' +
    '                <li class="divider"></li>\n' +
    '                <li class="tab tab-notification">\n' +
    '                    <a href="javascript:;" ng-click="featureComingSoon()">\n' +
    '						<i class="icon fa fa-bell-o"></i>\n' +
    '						<span class="text">Notification</span>\n' +
    '					</a>\n' +
    '                </li>\n' +
    '                <li class="divider"></li>\n' +
    '                <li class="tab tab-employers">\n' +
    '                    <a href="javascript:;" ng-click="featureComingSoon()">\n' +
    '						<i class="icon fa fa-users"></i>\n' +
    '						<span class="text">For Employers</span>\n' +
    '					</a>\n' +
    '                </li>\n' +
    '            </ul>\n' +
    '        </nav>\n' +
    '    </div>\n' +
    '</header>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/app/common/partials/layouts/_master.tpl.html',
    '<div ng-include="\'/webapp/js/app/common/partials/layouts/_header.tpl.html\'"></div>\n' +
    '<div ui-view="content"></div>\n' +
    '<div ng-include="\'/webapp/js/app/common/partials/layouts/_footer.tpl.html\'"></div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/app/common/partials/layouts/_master_employee.tpl.html',
    '<div class="admin-login-as-user" ng-if="admin_session">\n' +
    '    <div class="container">\n' +
    '        <div class="row">\n' +
    '            <!-- <div class="role col-md-2 col-xs-4">\n' +
    '                <span>role: </span>\n' +
    '                <strong ng-bind="admin_profile.roles[0].name"></strong>\n' +
    '            </div> -->\n' +
    '            <div class="return-to-your-account col-md-3 col-xs-5 pull-right">\n' +
    '                <a class="ui-btn btn-white btn-full-width" ng-click="vm.logoutAsUser()">Return to your account...</a>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '<header class="fixed-header" ng-class="{\'fixed-header-admin-login-as-user\': admin_session}" ng-if="!user.isEmployer() || (user.isEmployer() && !isStateOrChildOf(\'app.dashboard\') && !isStateOrChildOf(\'app.my_account\'))">\n' +
    '    <div class="menu-top">\n' +
    '        <div class="main-nav">\n' +
    '            <nav class="navbar navbar-default">\n' +
    '                <div class="container">\n' +
    '                    <div class="navbar-header">\n' +
    '                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">\n' +
    '                            <span class="sr-only">Toggle navigation</span>\n' +
    '                            <span class="icon-bar"></span>\n' +
    '                            <span class="icon-bar"></span>\n' +
    '                            <span class="icon-bar"></span>\n' +
    '                        </button>\n' +
    '                        <div class="sms-inbox-mobile visible-mobile">\n' +
    '                            <a ui-sref="app.inbox">\n' +
    '                                <i class="glyphicon glyphicon-envelope icon"></i>\n' +
    '                                <span class="unread-count" ng-if="vm.store.getState().Inbox.has_unread">({{vm.store.getState().Inbox.unread}})</span>\n' +
    '                            </a>\n' +
    '                        </div>\n' +
    '                        <a ng-if="$state.current.name != \'app.dashboard.jobs.all\'" class="navbar-brand" ui-sref="app.dashboard.jobs.all({\'page\': 1})">\n' +
    '                            <img ng-src="{{ asset(\'/assets/images/logo-new.png\')}}" alt="Ushift logo" />\n' +
    '                        </a>\n' +
    '                        <a ng-if="$state.current.name == \'app.dashboard.jobs.all\'" class="navbar-brand" ui-sref="app.dashboard.jobs.all({\'page\': 1})" force-reload>\n' +
    '                            <img ng-src="{{ asset(\'/assets/images/logo-new.png\')}}" alt="Ushift logo" />\n' +
    '                        </a>\n' +
    '                    </div>\n' +
    '                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">\n' +
    '                        <ul class="nav navbar-nav left-list">\n' +
    '                            <li>\n' +
    '                                <a ng-if="user.isEmployer() && $state.current.name != \'app.post-job\'" ui-sref="app.post-job">POST A JOB</a>\n' +
    '                            </li>\n' +
    '                        </ul>\n' +
    '                        <ul class="nav navbar-nav navbar-right" ng-if="user.needUpdateRole()">\n' +
    '                            <li class="dropdown">\n' +
    '                                <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My Account <span class="caret"></span></a>\n' +
    '                                <ul class="dropdown-menu">\n' +
    '                                    <li>\n' +
    '                                        <a ng-click="logout()">Logout</a>\n' +
    '                                    </li>\n' +
    '                                </ul>\n' +
    '                            </li>\n' +
    '                        </ul>\n' +
    '                        <ul class="nav navbar-nav navbar-right" ng-if="user.isEmployer()">\n' +
    '                            <li class="dropdown hidden-mobile">\n' +
    '                                <a ui-sref="app.inbox">Inbox <span ng-show="vm.store.getState().Inbox.has_unread">({{vm.store.getState().Inbox.unread}})</span></a>\n' +
    '                            </li>\n' +
    '                            <li class="dropdown">\n' +
    '                                <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My Account <span class="caret"></span></a>\n' +
    '                                <ul class="dropdown-menu">\n' +
    '                                    <li>\n' +
    '                                        <a ui-sref="app.dashboard.jobs.all({\'page\': 1})">Dashboard</a>\n' +
    '                                    </li>\n' +
    '                                    <li>\n' +
    '                                        <a ui-sref="app.profile.update">Profile</a>\n' +
    '                                    </li>\n' +
    '                                    <li>\n' +
    '                                        <a ui-sref="app.payment.account">Payment</a>\n' +
    '                                    </li>\n' +
    '                                    <li>\n' +
    '                                        <a ui-sref="app.account-settings">Account Settings</a>\n' +
    '                                    </li>\n' +
    '                                    <li>\n' +
    '                                        <a ng-click="logout()">Logout</a>\n' +
    '                                    </li>\n' +
    '                                </ul>\n' +
    '                            </li>\n' +
    '                        </ul>\n' +
    '                        <ul class="nav navbar-nav navbar-right" ng-if="user.isEmployee()">\n' +
    '                            <li class="dropdown hidden-mobile" ng-if="user.isSocialAccount() || user.isVerifiedPhoneNumber()">\n' +
    '                                <a ui-sref="app.inbox">Inbox <span ng-show="vm.store.getState().Inbox.has_unread">({{vm.store.getState().Inbox.unread}})</span></a>\n' +
    '                            </li>\n' +
    '                            <li class="dropdown">\n' +
    '                                <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My Account <span class="caret"></span></a>\n' +
    '                                <ul class="dropdown-menu">\n' +
    '                                    <li ng-if="user.isVerifiedPhoneNumber()">\n' +
    '                                        <a ui-sref="app.dashboard.jobs.all({\'page\': 1})">Dashboard</a>\n' +
    '                                    </li>\n' +
    '                                    <li ng-if="user.isVerifiedPhoneNumber()">\n' +
    '                                        <a ui-sref="app.profile.update({\'user_id\': me})">Profile</a>\n' +
    '                                    </li>\n' +
    '                                    <li ng-if="user.isVerifiedPhoneNumber()">\n' +
    '                                        <a ui-sref="app.payment.account">Payment</a>\n' +
    '                                    </li>\n' +
    '                                    <li ng-if="user.isVerifiedPhoneNumber()">\n' +
    '                                        <a ui-sref="app.account-settings">Account Settings</a>\n' +
    '                                    </li>\n' +
    '                                    <li>\n' +
    '                                        <a ng-click="logout()">Logout</a>\n' +
    '                                    </li>\n' +
    '                                </ul>\n' +
    '                            </li>\n' +
    '                        </ul>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </nav>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</header>\n' +
    '<div ui-view="content" class="employee-master"></div>\n' +
    '<div ng-include="\'/webapp/js/app/common/partials/layouts/_footer.tpl.html\'"></div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/app/common/partials/layouts/_master_employer.tpl.html',
    '<div class="admin-login-as-user" ng-if="admin_session">\n' +
    '    <div class="container">\n' +
    '        <div class="row">\n' +
    '            <!-- <div class="role col-md-2 col-xs-4">\n' +
    '                <span>role: </span>\n' +
    '                <strong ng-bind="admin_profile.roles[0].name"></strong>\n' +
    '            </div> -->\n' +
    '            <div class="return-to-your-account col-md-3 col-xs-5 pull-right">\n' +
    '                <a class="ui-btn btn-white btn-full-width" ng-click="vm.logoutAsUser()">Return to your account...</a>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '<div class="wrapper employer-master">\n' +
    '    <div class="div-left">\n' +
    '        <div class="logo">\n' +
    '            <a ui-sref="app.dashboard.jobs.draft" ui-sref-opts="{reload: true}"><img ng-src="/assets/images/logo-ushift.png" alt="" /></a>\n' +
    '            <a ng-click="showMobileNabar = !showMobileNabar" id="nav-toggle"><i class="fa fa-bars"></i></a>\n' +
    '        </div>\n' +
    '        <nav class="side-nav" ng-class="{\'show\': showMobileNabar}">\n' +
    '            <!-- <ul class="navigation sub-navigation animated fadeIn" ng-if="subMenus | hasItem">\n' +
    '                <li ui-sref-active="active" ng-repeat="item in subMenus">\n' +
    '                    <a ng-if="item.state" ui-sref="{{item.state}}" ui-sref-opts="{reload: item.state}" title="{{item.menu}}">\n' +
    '                        <span>{{item.menu}} <span class="badge" ng-if="item.badge && item.badge > 0" ng-bind="item.badge"></span></span>\n' +
    '                    </a>\n' +
    '                    <a ng-if="!item.state" ng-click="item.action()" title="{{item.menu}}">\n' +
    '                        <span>{{item.menu}} <span class="badge" ng-if="item.badge && item.badge > 0" ng-bind="item.badge"></span></span>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '            </ul> -->\n' +
    '            <ul class="navigation animated fadeIn">\n' +
    '                <li ui-sref-active="active" ng-repeat="item in mainMenus">\n' +
    '                    <a ng-if="item.state" ui-sref="{{item.state}}" ui-sref-opts="{reload: item.state}" title="{{item.menu}}">\n' +
    '                        <span>{{item.menu}} <span class="badge" ng-if="item.badge && item.badge > 0" ng-bind="item.badge"></span></span>\n' +
    '                    </a>\n' +
    '                    <a ng-if="!item.state" ng-click="item.action()" title="{{item.menu}}">\n' +
    '                        <span>{{item.menu}} <span class="badge" ng-if="item.badge && item.badge > 0" ng-bind="item.badge"></span></span>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '            </ul>\n' +
    '            <!-- <div class="text-center">\n' +
    '                <a href="#" class="btn btn-primary btn--red"><img ng-src="/assets/images/calendar.png" alt="" /> Jobs Calendar</a>\n' +
    '            </div> -->\n' +
    '        </nav>\n' +
    '    </div>\n' +
    '    <div class="div-right">\n' +
    '        <div class="top-nav">\n' +
    '            <div class="top-nav-left">\n' +
    '                <ul>\n' +
    '                    <li>\n' +
    '                        <a ui-sref="app.inbox" ui-sref-opts="{reload: true}">\n' +
    '                            <i class="fa fa-envelope"></i> Inbox <span class="unread-count" ng-if="vm.store.getState().Inbox.has_unread">({{vm.store.getState().Inbox.unread}})</span>\n' +
    '                        </a>\n' +
    '                    </li>\n' +
    '                    <li class="dropdown">\n' +
    '                        <a class="dropdown-toggle" type="button" data-toggle="dropdown" id="main-menu-dropdown">\n' +
    '                            <i class="fa fa-user"></i> My Account\n' +
    '                        </a>\n' +
    '                        <ul class="dropdown-menu" aria-labelledby="main-menu-dropdown">\n' +
    '                            <li><a ui-sref="app.profile.update" ui-sref-opts="{reload: true}"><i class="fa fa-user"></i> Profile</a></li>\n' +
    '                            <li><a ui-sref="app.payment.account" ui-sref-opts="{reload: true}"><i class="fa fa-credit-card"></i> Payment</a></li>\n' +
    '                            <li><a ui-sref="app.account-settings" ui-sref-opts="{reload: true}"><i class="fa fa-gear"></i> Account Settings</a></li>\n' +
    '                            <li><a ng-click="logout()"><i class="fa fa-sign-out"></i> Logout</a></li>\n' +
    '                        </ul>\n' +
    '                    </li>\n' +
    '                </ul>\n' +
    '            </div>\n' +
    '            <div class="top-nav-right">\n' +
    '                <ul>\n' +
    '                    <li class="xs-hide dropdown">\n' +
    '                        <a type="button" data-toggle="dropdown" id="call-support-dropdown">\n' +
    '                            <i class="fa fa-phone"></i> Call Support\n' +
    '                        </a>\n' +
    '                        <ul class="dropdown-menu" aria-labelledby="call-support-dropdown">\n' +
    '                            <li><a href="tel:6531389711"><i class="fa fa-phone-square"></i> +65 3138 9711</a></li>\n' +
    '                        </ul>\n' +
    '                    </li>\n' +
    '                    <li class="top-nav-right-action">\n' +
    '                        <a ng-if="!$state.is(\'app.post-job\')" ui-sref="app.post-job" class="btn--white">Post/Renew a Job</a>\n' +
    '                        <a ng-if="$state.is(\'app.post-job\')" ui-sref="app.dashboard.jobs.draft" class="btn--white">Dashboard</a>\n' +
    '                    </li>\n' +
    '                </ul>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <section id="main-content" ng-class="{\'employer-dashboard\': $state.current.name == \'app.job\'}">\n' +
    '            <div class="container-fluid">\n' +
    '                <div ui-view="content"></div>\n' +
    '            </div>\n' +
    '        </section>\n' +
    '    </div>\n' +
    '</div>\n' +
    '<!-- <div ng-include="\'/webapp/js/app/common/partials/layouts/_footer.tpl.html\'"></div> -->\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/app/common/partials/layouts/_no_login.tpl.html',
    '<header class="no-login-header">\n' +
    '    <div class="menu-top">\n' +
    '        <div class="main-nav">\n' +
    '            <nav class="navbar navbar-default">\n' +
    '                <div class="container">\n' +
    '                    <div class="navbar-header">\n' +
    '                        <a class="navbar-brand" href="/" force-reload>\n' +
    '                            <img src="/assets/images/logo-new.png" alt="Ushift logo" />\n' +
    '                        </a>\n' +
    '                    </div>\n' +
    '                    <div class="homepage-btn pull-left">\n' +
    '                        <a href="/" force-reload class="btn btn-primary orange-btn-sm job-seekers">Homepage</a>\n' +
    '                    </div>\n' +
    '                    <ul class="nav navbar-nav navbar-right">\n' +
    '                        <li ng-if="$state.is(\'user.login\')">\n' +
    '                            <a ui-sref="user.register.employer">REGISTER</a>\n' +
    '                        </li>\n' +
    '                        <li ng-if="$state.is(\'user.register\') || $state.is(\'user.password.forgot\') || $state.is(\'user.password.reset\')">\n' +
    '                            <a ui-sref="user.login">LOGIN</a>\n' +
    '                        </li>\n' +
    '                    </ul>\n' +
    '                </div>\n' +
    '            </nav>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</header>\n' +
    '<div ui-view="content" class="no-login"></div>\n' +
    '<div ng-include="\'/webapp/js/app/common/partials/layouts/_footer.tpl.html\'"></div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/app/common/partials/modal/_alert.tpl.html',
    '<div class="modal-system">\n' +
    '    <div class="modal-header">\n' +
    '        <h2 class="modal-title" id="modal-title" ng-bind="vm.data.title"></h2>\n' +
    '    </div>\n' +
    '    <div class="modal-footer">\n' +
    '        <button class="btn ui-btn btn-orange" type="button" ng-click="vm.ok()" ng-bind="vm.data.ok"></button>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/app/common/partials/modal/_confirm.tpl.html',
    '<div class="modal-system">\n' +
    '    <div class="modal-header">\n' +
    '        <h2 class="modal-title" id="modal-title" ng-bind="vm.data.title"></h2>\n' +
    '    </div>\n' +
    '    <div class="modal-body" id="modal-body">\n' +
    '        <p ng-if="vm.data.content" ng-bind="vm.data.content"></p>\n' +
    '        <div ng-if="vm.data.htmlContent" ng-bind-html="vm.data.htmlContent | trustAsHtml" compile-template></div>\n' +
    '    </div>\n' +
    '    <div class="modal-footer">\n' +
    '        <button class="btn ui-btn btn-white" type="button" ng-click="vm.cancel()" ng-bind="vm.data.cancel"></button>\n' +
    '        <button class="btn ui-btn btn-orange" type="button" ng-click="vm.ok()" ng-bind="vm.data.ok"></button>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();
