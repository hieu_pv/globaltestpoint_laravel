let {
    call, put, take, takeEvery, takeLatest, select, fork
} = require('redux-saga/effects');

let {
    FETCH_LOGGED_IN_USER_REQUESTED,
    FETCH_LOGGED_IN_USER_SUCCESSED,
} = require('./action');

(function(app) {
    app.factory('LoginSagas', ['$rootScope', '$state', '$stateParams', 'UshiftApp', 'API', 'Notification', 'Modal', 'preloader', function($rootScope, $state, $stateParams, UshiftApp, API, Notification, Modal, preloader) {


        function* proccessFetchUser(action) {
            try {
                let request = yield API.user.profile();
                yield put({
                    type: FETCH_LOGGED_IN_USER_SUCCESSED,
                    data: request,
                });
            } catch (error) {
                yield put({
                    type: 'API_CALL_ERROR',
                    error: error,
                });
            }
        }

        function* watchFetchUser() {
            yield takeLatest(FETCH_LOGGED_IN_USER_REQUESTED, proccessFetchUser);
        }

        function proccessFetchUserSuccessed(action) {
            $rootScope.user = action.data;
            $rootScope.isLoggedIn = true;
        }

        function* watchFetchUserSuccessed() {
            yield takeLatest(FETCH_LOGGED_IN_USER_SUCCESSED, proccessFetchUserSuccessed);
        }


        return _.map([
            watchFetchUser,
            watchFetchUserSuccessed,
        ], item => fork(item));
    }]);
})(angular.module('app.components.login.sagas', []));