(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('user.login', {
                url: '/login?redirect&block',
                views: {
                    'content@user': {
                        templateUrl: "/webapp/js/app/components/login/_login.tpl.html",
                        controller: 'LoginCtrl',
                        controllerAs: 'vm',
                    }
                }
            });
    }]);
})(angular.module('app.components.login.router', []));