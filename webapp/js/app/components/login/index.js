require('./router');

let {
    USER_LOGGED_IN,
    FETCH_LOGGED_IN_USER_SUCCESSED,
} = require('./action');

(function(app) {

    app.controller('LoginCtrl', LoginCtrl);
    LoginCtrl.$inject = ['$rootScope', '$state', '$stateParams', '$auth', 'store', '$cookies', 'API', 'Notification', 'Modal', 'preloader'];

    function LoginCtrl($rootScope, $state, $stateParams, $auth, store, $cookies, API, Notification, Modal, preloader) {
        var vm = this;
        vm.store = store;

        $rootScope.title = 'Login | Ushift SG';
        $rootScope.description = '';

        if (!_.isNil($rootScope.user)) {
            if ($rootScope.user.isEmployer() || $rootScope.user.isEmployee()) {
                $state.go('app.dashboard.jobs.draft');
            } else if ($rootScope.user.needUpdateRole()) {
                $state.go('app.profile.update.role');
            } else {
                $cookies.remove(JWT_TOKEN_COOKIE_KEY, {
                    path: '/'
                });
            }
            return false;
        }

        vm.login = function(user) {
            preloader.show();
            API.auth.login(user.email, user.password)
                .then(function(response) {
                    preloader.hide();
                    processLogin(response.token);
                })
                .catch(function(error) {
                    preloader.hide();
                    vm.inProgress = false;
                    throw (error);
                });

        };

        vm.authenticate = function(provider) {
            preloader.show();
            $auth.authenticate(provider)
                .then(function(response) {
                    if (_.isUndefined(response.access_token) && !_.isUndefined(response['!#access_token'])) {
                        response.access_token = response['!#access_token'];
                    }
                    if (provider === 'linkedin') {
                        let params = {
                            redirect_uri: response.data.redirectUri,
                            code: response.data.code,
                            client_id: response.data.clientId,
                            state: response.data.state,
                        };
                        API.auth.getLinkedInAccessToken(params)
                            .then(function(response) {
                                processSocialLogin(provider, response.access_token);
                            })
                            .catch(function(error) {
                                preloader.hide();
                                throw error;
                            });
                        return false;
                    } else {
                        processSocialLogin(provider, response.access_token);
                    }

                })
                .catch(function(error) {
                    preloader.hide();
                    throw error;
                });
        };

        function processSocialLogin(provider, access_token) {
            let params = {
                provider: provider,
                access_token: access_token
            };
            API.auth.social_login(params)
                .then(function(response) {
                    processLogin(response.token);
                })
                .catch(function(error) {
                    preloader.hide();
                    throw error;
                });
        }

        function processLogin(token) {
            $cookies.put(JWT_TOKEN_COOKIE_KEY, `"${token}"`, {
                path: '/',
            });
            vm.inProgress = false;
            API.user.profile()
                .then(function(user) {
                    if (user.isBanned()) {
                        $rootScope.logout(function() {
                            Notification.show('warning', 'Your account has been banned', 5000);
                            preloader.hide();
                        });
                    } else if (user.isEmployee() && $stateParams.block !== 'false') {
                        Notification.show('warning', 'To continue using UShift, download the App. Website for job seekers will stop on the 10th of August.', 5000);
                        $cookies.remove(JWT_TOKEN_COOKIE_KEY, {
                            path: '/'
                        });
                        return false;
                    } else {
                        vm.store.dispatch({
                            type: FETCH_LOGGED_IN_USER_SUCCESSED,
                            data: user
                        });
                        if (!_.isNil($stateParams.redirect)) {
                            document.location.href = decodeURIComponent($stateParams.redirect);
                        } else {
                            if (user.isEmployer() || user.isEmployee()) {
                                $state.go('app.dashboard.jobs.draft');
                            } else if ($rootScope.user.needUpdateRole()) {
                                $state.go('app.profile.update.role');
                            } else {
                                $cookies.remove(JWT_TOKEN_COOKIE_KEY, {
                                    path: '/'
                                });
                                document.location.reload();
                            }
                        }
                        preloader.hide();
                    }

                })
                .catch(function(error) {
                    preloader.hide();
                    throw error;
                });
        }
    }

})(angular.module('app.components.login', ['app.components.login.router']));