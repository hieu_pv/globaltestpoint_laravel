(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.job-vacancies', {
                url: '/job-vacancies',
                views: {
                    'content@app': {
                        templateUrl: "/webapp/js/app/components/job-vacancies/_job-vacancies.tpl.html",
                        controller: 'JobVacanciesCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.job-vacancies.list', {
                url: `/list?page&keyword&location&experience_id&order_by&constraints
                    &filter_state_ids&filter_industry_ids&filter_job_title_ids&filter_qualification_ids
                    &filter_experience_ids&refine_search`,
                views: {
                    'job-vacancies@app.job-vacancies': {
                        templateUrl: "/webapp/js/app/components/job-vacancies/list/_list.tpl.html",
                        controller: 'ListJobVacanciesCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.job-vacancies.detail', {
                url: '/detail?slug',
                views: {
                    'job-vacancies@app.job-vacancies': {
                        templateUrl: "/webapp/js/app/components/job-vacancies/detail/_detail.tpl.html",
                        controller: 'DetailJobVacancyCtrl',
                        controllerAs: 'vm',
                    }
                },
                resolve: {
                    job: ['API', '$stateParams', function(API, $stateParams) {
                        return API.job.getJobBySlug($stateParams.slug, { includes: 'employer,experience,city,country,tags,nationality,industries' });
                    }]
                }
            });
    }]);
})(angular.module('app.components.job-vacancies.router', []));