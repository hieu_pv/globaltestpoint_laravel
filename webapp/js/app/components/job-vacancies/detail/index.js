(function(app) {

    app.controller('DetailJobVacancyCtrl', DetailJobVacancyCtrl);

    DetailJobVacancyCtrl.$inject = ['$rootScope', '$scope', 'job', '$state', '$stateParams', 'API', 'Modal', 'preloader', 'Notification'];

    function DetailJobVacancyCtrl($rootScope, $scope, job, $state, $stateParams, API, Modal, preloader, Notification) {
        var vm = this;
        vm.STRIPE_CURRENCY_SYMBOL = STRIPE_CURRENCY_SYMBOL;

        vm.breadcrumbs = [{
            title: 'Home',
            state: 'app.job-vacancies.list',
            options: `{
                page: 1
            }`
        }];
        
        if (!_.isNil(job.city)) {
            let title = _.capitalize(job.city.getName());

            let breadcrumb = {
                title: `Jobs In ${title}`,
                state: 'app.job-vacancies.list',
                options: `{page: 1, location: '${job.city.getName()}'}`
            };
            vm.breadcrumbs.push(breadcrumb);
        }
        let breadcrumb = {
            title: job.getTitle()
        };
        vm.breadcrumbs.push(breadcrumb);

        let params = {
            per_page: 5,
            includes: 'employer,experience,city,country'
        };

        let advertisement_params = {
            page: 1,
            per_page: 2
        };

        vm.job = job;

        vm.getSimilarJobs = getSimilarJobs;
        vm.getAdvertisements = getAdvertisements;

        vm.getSimilarJobs(params);
        vm.getAdvertisements(advertisement_params);

        function getSimilarJobs(params) {
            API.job.getSimilarJobs(vm.job.getId(), params)
                .then(function(response) {
                    vm.similar_jobs = response.jobs;
                    vm.pagination = response.pagination;
                })
                .catch(function(error) {
                    throw error;
                });
        }

        function getAdvertisements(params) {
            API.advertisements.get(params)
                .then(function(response) {
                    vm.advertisements = response.advertisements;
                })
                .catch(function(error) {
                    throw error;
                });
        }

    }

})(angular.module('app.components.job-vacancies.detail', []));