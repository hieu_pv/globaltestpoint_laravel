require('./router');
require('./list/');
require('./detail/');

(function(app) {

    app.controller('JobVacanciesCtrl', JobVacanciesCtrl);

    JobVacanciesCtrl.$inject = ['$rootScope', '$scope', '$state'];

    function JobVacanciesCtrl($rootScope, $scope, $state) {
        if ($state.current.name === 'app.job-vacancies') {
            $state.go('app.job-vacancies.list', { page: 1 });
        }
    }

})(angular.module('app.components.job-vacancies', [
    'app.components.job-vacancies.router',
    'app.components.job-vacancies.list',
    'app.components.job-vacancies.detail'
]));