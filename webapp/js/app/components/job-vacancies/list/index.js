(function(app) {

    app.controller('ListJobVacanciesCtrl', ListJobVacanciesCtrl);

    ListJobVacanciesCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Modal', 'Notification', 'preloader'];

    function ListJobVacanciesCtrl($rootScope, $scope, $state, $stateParams, API, Modal, Notification, preloader) {
        var vm = this;

        vm.breadcrumbs = [{
            title: 'Home',
            state: 'app.job-vacancies.list',
            options: `{
                page: 1
            }`
        }];
        if (!_.isNil($stateParams.location)) {
            let title = _.capitalize(_.join(_.map(_.split($stateParams.location, ','), _.trim), ' AND '));

            let breadcrumb = {
                title: `Jobs In ${title}`
            };
            vm.breadcrumbs.push(breadcrumb);
        }

        const ADVERTISEMENT_ABOVE_RELATED_SEARCH = 1;
        const ADVERTISEMENT_UNDER_RELATED_SEARCH = 2;

        const NORMAL_JOB_TYPE = 1;
        const INTERNATIONAL_JOB_TYPE = 2;

        const LIMIT_FILTER_ITEMS = 3;

        if (!_.isNil($stateParams.filter_state_ids) ||
            !_.isNil($stateParams.filter_industry_ids) ||
            !_.isNil($stateParams.filter_job_title_ids) ||
            !_.isNil($stateParams.filter_qualification_ids) ||
            !_.isNil($stateParams.filter_experience_ids)) {
            vm.show_refine_button = true;

            if (!_.isNil($stateParams.refine_search)) {
                vm.show_reset_all_filter_button = true;
            }
        } else {
            vm.show_refine_button = false;
            vm.show_reset_all_filter_button = false;
        }

        vm.params = $stateParams;
        vm.params.per_page = 15;
        vm.params.constraints = {
            type: NORMAL_JOB_TYPE
        };
        vm.params.includes = 'employer,experience,city,country,tags';

        vm.search_result_message = 'All Jobs';
        if (!_.isNil(vm.params.keyword) && !_.isNil(vm.params.location)) {
            vm.search_result_message = `Search result for ${vm.params.keyword} Jobs In ${vm.params.location}`;
        } else {
            if (!_.isNil(vm.params.keyword)) {
                vm.search_result_message = `Search result for ${vm.params.keyword} Jobs`;
            }
            if (!_.isNil(vm.params.location)) {
                vm.search_result_message = `Search result Jobs In ${vm.params.location}`;
            }
        }

        vm.toggle = {
            states: false,
            industries: false,
            job_titles: false,
            qualifications: false,
            experiences: false
        };

        vm.getStates = getStates;
        vm.getCities = getCities;
        vm.getIndustries = getIndustries;
        vm.getJobTitles = getJobTitles;
        vm.getQualifications = getQualifications;
        vm.getExperiences = getExperiences;
        vm.getAboveRelatedSearchAdvertisements = getAboveRelatedSearchAdvertisements;
        vm.getUnderRelatedSearchAdvertisements = getUnderRelatedSearchAdvertisements;
        vm.getJobs = getJobs;
        vm.getIntenationalJobs = getIntenationalJobs;
        vm.searchJobs = searchJobs;
        vm.filterJobs = filterJobs;
        vm.checkDisplayRefineSearch = checkDisplayRefineSearch;
        vm.resetFilter = resetFilter;
        vm.resetAllFilter = resetAllFilter;
        vm.sortBy = sortBy;
        vm.toggleFilter = toggleFilter;

        vm.getStates();
        vm.getCities();
        vm.getIndustries();
        vm.getJobTitles();
        vm.getQualifications();
        vm.getExperiences();
        vm.getAboveRelatedSearchAdvertisements();
        vm.getUnderRelatedSearchAdvertisements();
        vm.getJobs(vm.params);
        vm.getIntenationalJobs();

        function getStates() {
            API.state.get()
                .then(function(states) {
                    if (!_.isNil($stateParams.filter_state_ids)) {
                        let filter_state_ids = _.split($stateParams.filter_state_ids, ',');
                        filter_state_ids = _.map(filter_state_ids, (item) => {
                            item = parseInt(item);
                            return item;
                        });

                        states = _.map(states, (item) => {
                            if (_.indexOf(filter_state_ids, item.getId()) !== -1) {
                                item.selected = true;
                            }
                            return item;
                        });
                        vm.states = _.filter(states, { selected: true });
                        vm.raw_states = angular.copy(vm.states);
                    } else {
                        if (states.length > LIMIT_FILTER_ITEMS) {
                            vm.limit_states = [];

                            for (let i = 0; i < LIMIT_FILTER_ITEMS; i++) {
                                vm.limit_states.push(states[i]);
                            }
                        }
                        vm.states = angular.copy(vm.limit_states);
                        vm.raw_states = angular.copy(states);
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        }

        function getCities() {
            API.city.get()
                .then(function(cities) {
                    vm.cities = cities;
                    let default_city = API.city.createNewCityInstance({
                        id: null,
                        name: 'City'
                    });
                    vm.cities.unshift(default_city);

                    if (!_.isNil(vm.params) && !_.isNil(vm.params.location)) {
                        vm.search.location = _.find(vm.cities, { name: vm.params.location });
                    } else {
                        vm.search.location = _.head(vm.cities);
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        }

        function getIndustries() {
            API.industries.get()
                .then(function(industries) {
                    if (!_.isNil($stateParams.filter_industry_ids)) {
                        let filter_industry_ids = _.split($stateParams.filter_industry_ids, ',');
                        filter_industry_ids = _.map(filter_industry_ids, (item) => {
                            item = parseInt(item);
                            return item;
                        });

                        industries = _.map(industries, (item) => {
                            if (_.indexOf(filter_industry_ids, item.getId()) !== -1) {
                                item.selected = true;
                            }
                            return item;
                        });
                        vm.industries = _.filter(industries, { selected: true });
                        vm.raw_industries = angular.copy(vm.industries);
                    } else {
                        vm.industries = industries;
                        vm.raw_industries = angular.copy(vm.industries);
                    }

                })
                .catch(function(error) {
                    throw error;
                });
        }

        function getJobTitles() {
            API.job_titles.get()
                .then(function(job_titles) {
                    if (!_.isNil($stateParams.filter_job_title_ids)) {
                        let filter_job_title_ids = _.split($stateParams.filter_job_title_ids, ',');
                        filter_job_title_ids = _.map(filter_job_title_ids, (item) => {
                            item = parseInt(item);
                            return item;
                        });

                        job_titles = _.map(job_titles, (item) => {
                            if (_.indexOf(filter_job_title_ids, item.getId()) !== -1) {
                                item.selected = true;
                            }
                            return item;
                        });
                        vm.job_titles = _.filter(job_titles, { selected: true });
                        vm.raw_job_titles = angular.copy(vm.job_titles);
                    } else {
                        vm.job_titles = job_titles;
                        vm.raw_job_titles = angular.copy(vm.job_titles);
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        }

        function getQualifications() {
            API.qualifications.get()
                .then(function(qualifications) {
                    if (!_.isNil($stateParams.filter_qualification_ids)) {
                        let filter_qualification_ids = _.split($stateParams.filter_qualification_ids, ',');
                        filter_qualification_ids = _.map(filter_qualification_ids, (item) => {
                            item = parseInt(item);
                            return item;
                        });

                        qualifications = _.map(qualifications, (item) => {
                            if (_.indexOf(filter_qualification_ids, item.getId()) !== -1) {
                                item.selected = true;
                            }
                            return item;
                        });
                        vm.qualifications = _.filter(qualifications, { selected: true });
                        vm.raw_qualifications = angular.copy(vm.qualifications);
                    } else {
                        vm.qualifications = qualifications;
                        vm.raw_qualifications = angular.copy(vm.qualifications);
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        }

        function getExperiences() {
            API.experiences.get()
                .then(function(experiences) {
                    vm.search_experiences = angular.copy(experiences);

                    let default_experience = API.experiences.createNewExperienceInstance({
                        id: 0,
                        label: 'Years'
                    });
                    vm.search_experiences.unshift(default_experience);

                    if (_.isNil(vm.params.experience_id)) {
                        vm.search.experience = _.head(vm.search_experiences);
                    } else {
                        vm.search.experience = _.find(vm.search_experiences, { id: parseInt(vm.params.experience_id) });
                    }

                    if (!_.isNil($stateParams.filter_experience_ids)) {
                        let filter_experience_ids = _.split($stateParams.filter_experience_ids, ',');
                        filter_experience_ids = _.map(filter_experience_ids, (item) => {
                            item = parseInt(item);
                            return item;
                        });

                        experiences = _.map(experiences, (item) => {
                            if (_.indexOf(filter_experience_ids, item.getId()) !== -1) {
                                item.selected = true;
                            }
                            return item;
                        });
                        vm.experiences = _.filter(experiences, { selected: true });
                        vm.raw_experiences = angular.copy(vm.experiences);
                    } else {
                        vm.experiences = experiences;
                        vm.raw_experiences = angular.copy(vm.experiences);
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        }

        function getAboveRelatedSearchAdvertisements() {
            var advertisement_constraints = {
                position: ADVERTISEMENT_ABOVE_RELATED_SEARCH
            };

            var params = {
                constraints: advertisement_constraints
            };

            API.advertisements.getAll(params)
                .then(function(above_advertisements) {
                    vm.above_advertisements = above_advertisements;
                })
                .catch(function(error) {
                    throw error;
                });
        }

        function getUnderRelatedSearchAdvertisements() {
            var advertisement_constraints = {
                position: ADVERTISEMENT_UNDER_RELATED_SEARCH
            };

            var params = {
                constraints: advertisement_constraints
            };

            API.advertisements.getAll(params)
                .then(function(under_advertisements) {
                    vm.under_advertisements = under_advertisements;
                })
                .catch(function(error) {
                    throw error;
                });
        }

        function getJobs(params) {
            API.job.get(params)
                .then(function(response) {
                    vm.jobs = _.map(response.jobs, (job) => {
                        if (!_.isNil(job.tags)) {
                            job.display_tag = _.join(_.map(job.tags, 'name'), ', ');
                        }
                        return job;
                    });
                    vm.pagination = response.pagination;
                })
                .catch(function(error) {
                    throw error;
                });
        }

        function getIntenationalJobs() {
            let params = {
                constraints: {
                    type: INTERNATIONAL_JOB_TYPE
                },
                includes: 'employer,experience,tags'
            };

            API.job.getAll(params)
                .then(function(jobs) {
                    vm.international_jobs = _.map(jobs, (job) => {
                        if (!_.isNil(job.tags)) {
                            job.display_tag = _.join(_.map(job.tags, 'name'), ', ');
                        }
                        return job;
                    });
                })
                .catch(function(error) {
                    throw error;
                });
        }

        vm.search = {};
        if (!_.isNil(vm.params) && !_.isNil(vm.params.keyword)) {
            vm.search.keyword = vm.params.keyword;
        }

        function searchJobs(search) {
            $stateParams.page = 1;

            if (!_.isNil(search.keyword)) {
                $stateParams.keyword = search.keyword;
            }
            if (!_.isNil(search.location) && !_.isNil(search.location.getId())) {
                $stateParams.location = search.location.getName();
            } else {
                $stateParams.location = null;
            }
            if (!_.isNil(search.experience)) {
                if (search.experience.getId() !== 0) {
                    $stateParams.experience_id = search.experience.getId();
                } else {
                    $stateParams.experience_id = null;
                }
            }

            $state.go($state.current, $stateParams, {
                reload: true
            });
        }

        function filterJobs() {
            $stateParams.page = 1;
            $stateParams.refine_search = true;

            if (vm.selected_states.length) {
                $stateParams.filter_state_ids = _.join(_.map(vm.selected_states, 'id'), ',');
            }
            if (vm.selected_industries.length) {
                $stateParams.filter_industry_ids = _.join(_.map(vm.selected_industries, 'id'), ',');
            }
            if (vm.selected_job_titles.length) {
                $stateParams.filter_job_title_ids = _.join(_.map(vm.selected_job_titles, 'id'), ',');
            }
            if (vm.selected_qualifications.length) {
                $stateParams.filter_qualification_ids = _.join(_.map(vm.selected_qualifications, 'id'), ',');
            }
            if (vm.selected_experiences.length) {
                $stateParams.filter_experience_ids = _.join(_.map(vm.selected_experiences, 'id'), ',');
            }

            $state.go($state.current, $stateParams, {
                reload: true
            });
        }

        function checkDisplayRefineSearch() {
            vm.selected_states = _.filter(vm.states, { selected: true });
            vm.selected_industries = _.filter(vm.industries, { selected: true });
            vm.selected_job_titles = _.filter(vm.job_titles, { selected: true });
            vm.selected_qualifications = _.filter(vm.qualifications, { selected: true });
            vm.selected_experiences = _.filter(vm.experiences, { selected: true });

            if (vm.selected_states.length ||
                vm.selected_industries.length ||
                vm.selected_job_titles.length ||
                vm.selected_qualifications.length ||
                vm.selected_experiences.length) {
                vm.show_refine_button = true;

                if (!_.isNil($stateParams.refine_search)) {
                    vm.show_reset_all_filter_button = true;
                }
            } else {
                vm.show_refine_button = false;
                vm.show_reset_all_filter_button = false;
            }
        }

        function resetFilter(property) {
            if ($stateParams.hasOwnProperty(property)) {
                $stateParams[property] = null;

                $state.go($state.current, $stateParams, {
                    reload: true
                });
            }
        }

        function resetAllFilter() {
            $stateParams.refine_search = null;

            for (var key in $stateParams) {
                let filter_key_match = /^filter_.+$/;
                if (filter_key_match.test(key)) {
                    $stateParams[key] = null;
                }
            }

            $state.go($state.current, $stateParams, {
                reload: true
            });
        }

        if (!_.isNil($stateParams.order_by)) {
            let order_by = JSON.parse($stateParams.order_by);
            vm.order_by_key = Object.keys(order_by)[0];
        }

        function sortBy(field) {
            let order_by = {};
            if (field !== 'relevance') {
                order_by[field] = 'desc';
                $stateParams.order_by = JSON.stringify(order_by);
            } else {
                order_by = null;
                $stateParams.order_by = null;
            }
            $state.go($state.current, $stateParams, {
                reload: true
            });
        }

        function toggleFilter(key) {
            vm.toggle[key] = !vm.toggle[key];
            if (vm.toggle[key]) {
                let raw_items = `raw_${key}`;
                vm[key] = angular.copy(vm[raw_items]);
            } else {
                let limit_items = `limit_${key}`;
                vm[key] = angular.copy(vm[limit_items]);
            }
        }
    }

})(angular.module('app.components.job-vacancies.list', []));