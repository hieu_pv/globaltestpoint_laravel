(function(app) {
    app.config([
        '$httpProvider',
        '$authProvider',
        '$animateProvider',
        'UshiftAppProvider',
        function($httpProvider, $authProvider, $animateProvider, UshiftAppProvider) {

            $animateProvider.classNameFilter(/animated/);

            $httpProvider.interceptors.push(['$cookieStore', function($cookieStore) {
                return {
                    request: function(config) {
                        if (!_.isNil($cookieStore.get(JWT_TOKEN_COOKIE_KEY))) {
                            config.headers.Authorization = 'Bearer ' + $cookieStore.get(JWT_TOKEN_COOKIE_KEY);
                        }
                        return config;
                    },
                    response: function(response) {
                        if (!_.isNil(response.headers().authorization)) {
                            $cookieStore.put(JWT_TOKEN_COOKIE_KEY, response.headers().authorization.substr(7));
                        }
                        return response;
                    }
                };
            }]);

            $authProvider.facebook({
                clientId: FACEBOOK_CLIENT_ID,
                responseType: 'token',
            });

            $authProvider.google({
                clientId: GOOGLE_CLIENT_ID,
                responseType: 'token'
            });

            $authProvider.github({
                clientId: 'GitHub Client ID'
            });

            $authProvider.linkedin({
                clientId: LINKEDIN_CLIENT_ID,
                redirectUri: window.location.origin + '/auth/linkedin',
            });


            $authProvider.instagram({
                clientId: 'Instagram Client ID'
            });

            $authProvider.yahoo({
                clientId: 'Yahoo Client ID / Consumer Key'
            });

            $authProvider.live({
                clientId: 'Microsoft Client ID'
            });

            $authProvider.twitch({
                clientId: 'Twitch Client ID'
            });

            $authProvider.bitbucket({
                clientId: 'Bitbucket Client ID'
            });

            $authProvider.spotify({
                clientId: 'Spotify Client ID'
            });
            // No additional setup required for Twitter

            $authProvider.oauth2({
                name: 'foursquare',
                url: '/auth/foursquare',
                clientId: 'Foursquare Client ID',
                redirectUri: window.location.origin,
                authorizationEndpoint: 'https://foursquare.com/oauth2/authenticate',
            });

            // disable satellizer token
            $authProvider.httpInterceptor = function() {
                return false;
            };

            UshiftAppProvider.currencies([{
                id: 1,
                country: 'Singapore',
                name: 'Singapore Dolar',
                code: 'SGD',
                symbols: '$',

            }]);

            // https://github.com/moment/moment-timezone/blob/develop/data/packed/latest.json
            UshiftAppProvider.timezones([
                "Asia/Singapore|SMT MALT MALST MALT MALT JST SGT SGT|-6T.p -70 -7k -7k -7u -90 -7u -80|012345467|-2Bg6T.p 17anT.p 7hXE dM00 17bO 8Fyu Mspu DTA0|56e5",
                "Etc/UTC|UTC|0|0|",
                "Asia/Ho_Chi_Minh|LMT PLMT ICT IDT JST|-76.E -76.u -70 -80 -90|0123423232|-2yC76.E bK00.a 1h7b6.u 5lz0 18o0 3Oq0 k5b0 aW00 BAM0|90e5",
            ]);
        }
    ]);

})(angular.module('app.config', [
    'satellizer',
    'ngAnimate',
]));
