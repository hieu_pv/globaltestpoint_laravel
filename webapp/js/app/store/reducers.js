let {
    FETCH_JOB_COUNT,
    FETCH_JOB_COUNT_SUCCESSED,
    UPDATE_JOB_ANALYTIC,
    FETCHING_MORE_DATA,
    PUT_TO_WAITING_LIST,
    ADMIN_LOGGED_AS_USER,
    ADMIN_LOG_OUT_AS_USER_SUCCESSED,
    SET_HAS_NO_JOB,
} = require('./action');

let {
    combineReducers,
} = require('redux');


const analytic = (state, action) => {
    if (_.isUndefined(state)) {
        state = {};
    }
    switch (action.type) {
        case FETCH_JOB_COUNT_SUCCESSED:
            return _.assign(state, {
                job: action.data
            });
        case UPDATE_JOB_ANALYTIC:
            return _.assign(state, action.data);
        default:
            return state;
    }
};

const app = (state, action) => {
    if (_.isUndefined(state)) {
        state = {
            user_logged_in: false,
            draft_jobs_fetched: false,
            under_review_jobs_fetched: false,
            hiring_jobs_fetched: false,
            pay_shifts_fetched: false,
            rate_shifts_fetched: false,
            closed_shifts_fetched: false,
            no_more_pay_shifts: false,
            no_more_rate_shifts: false,
            no_more_draft_jobs: false,
            no_more_under_review_jobs: false,
            no_more_hiring_jobs: false,
            has_no_job: false,
            fetched_favorite_candidates: false,
        };
    }
    switch (action.type) {
        default:
            return state;
    }
};

const admin = (state, action) => {
    if (_.isUndefined(state)) {
        state = {
            loggedin: false,
            profile: undefined,
        };
    }
    switch (action.type) {
        case ADMIN_LOGGED_AS_USER:
            return _.assign(state, {
                loggedin: true,
                profile: action.data,
            });
        case ADMIN_LOG_OUT_AS_USER_SUCCESSED:
            return _.assign(state, {
                loggedin: false,
                profile: undefined,
            });
        default:
            return state;
    }
};

module.exports = combineReducers({
    analytic,
    app,
    admin,
});
