require('../components/login/sagas');
var {
    call,
    put,
    takeEvery,
    takeLatest,
    fork
} = require('redux-saga/effects');

let {
    FETCH_JOB_COUNT,
    FETCH_JOB_COUNT_SUCCESSED,
    UPDATE_JOB_ANALYTIC,
    ADMIN_LOGGED_AS_USER,
    ADMIN_LOG_OUT_AS_USER,
    ADMIN_LOG_OUT_AS_USER_SUCCESSED,
    SET_HAS_NO_JOB,
    EMPLOYER_DOES_NOT_HAVE_JOB,
} = require('./action');


(function(app) {
    app.factory('sagas', sagas);
    sagas.$inject = [
        '$rootScope',
        'API',
        'Notification',
        'preloader',
        '$cookies',
        'LoginSagas'
    ];

    function sagas(
        $rootScope,
        API,
        Notification,
        preloader,
        $cookies,
        LoginSagas
    ) {
        function onError(action) {
            preloader.hide();
            if (!$rootScope.hasError) {
                $rootScope.hasError = true;
            }
            let err = action.error;
            if (!_.isNil(err) && err.message !== undefined) {
                if (err.code === 1000) {
                    var errors = $.parseJSON(err.message);
                    var message = '';
                    for (var key in errors) {
                        message += errors[key].join(' ') + ' ';
                    }
                    Notification.show('warning', message, 5000);
                } else {
                    Notification.show('warning', err.message, 5000);
                }
            }
        }

        function* watchOnError() {
            yield takeEvery('API_CALL_ERROR', onError);
        }

        function* updateMainMenu(action) {
            $rootScope.mainMenus = _.map($rootScope.mainMenus, item => _.assign(item, {
                badge: _.isNil(_.find(action.data, i => i.label === item.label)) ? 0 : _.find(action.data, i => i.label === item.label).count,
            }));
            $rootScope.totalJob = _.sumBy(_.filter(action.data, item => ['draft', 'under_review', 'hiring', 'rate', 'closed'].indexOf(item.label) > -1), item => item.count);
            yield put({
                type: SET_HAS_NO_JOB,
                data: !_.isUndefined($rootScope.totalJob) && $rootScope.totalJob === 0
            });
            yield put({
                type: UPDATE_JOB_ANALYTIC,
                data: {
                    has_no_draft: _.find(action.data, item => item.label === 'draft').count === 0,
                    has_no_under_review: _.find(action.data, item => item.label === 'under_review').count === 0,
                    has_no_hiring: _.find(action.data, item => item.label === 'hiring').count === 0,
                    has_no_pay: _.find(action.data, item => item.label === 'pay').count === 0,
                    has_no_rate: _.find(action.data, item => item.label === 'rate').count === 0,
                    has_no_closed: _.find(action.data, item => item.label === 'closed').count === 0,
                },
            });
        }

        function* watchUpdateMainMenu() {
            yield takeEvery(FETCH_JOB_COUNT_SUCCESSED, updateMainMenu);
        }

        function* broadcastNoJobFound(action) {
            if (action.data) {
                yield put({
                    type: EMPLOYER_DOES_NOT_HAVE_JOB,
                });
            }
        }

        function* watchBroadcastNoJobFound() {
            yield takeEvery(SET_HAS_NO_JOB, broadcastNoJobFound);
        }

        function adminLogin(action) {
            $rootScope.admin_session = true;
            $rootScope.admin_profile = action.data;
        }

        function* watchAdminLoggedIn() {
            yield takeEvery(ADMIN_LOGGED_AS_USER, adminLogin);
        }

        function* adminLogout(action) {
            try {
                let request = yield API.user.logoutAsUser();
                yield put({
                    type: ADMIN_LOG_OUT_AS_USER_SUCCESSED,
                    data: request,
                });
            } catch (error) {
                yield put({
                    type: 'API_CALL_ERROR',
                    error: error,
                });
            }
        }

        function* watchAdminLogOut() {
            yield takeEvery(ADMIN_LOG_OUT_AS_USER, adminLogout);
        }

        function adminLogoutSuccessed(action) {
            $cookies.remove(JWT_TOKEN_COOKIE_KEY, {
                path: '/'
            });
            $cookies.remove(ADMIN_SESSION, {
                path: '/'
            });
            $cookies.remove(ADMIN_PROFILE, {
                path: '/'
            });
            $rootScope.isLoggedIn = false;
            document.location.href = '/admin/jobs/all';
        }

        function* watchAdminLogOutSuccessed() {
            yield takeEvery(ADMIN_LOG_OUT_AS_USER_SUCCESSED, adminLogoutSuccessed);
        }

        let StateSagas = _.map([
            watchOnError,
            watchUpdateMainMenu,
            watchBroadcastNoJobFound,
            watchAdminLoggedIn,
            watchAdminLogOut,
            watchAdminLogOutSuccessed,
        ], item => fork(item));

        return function* sagas() {
            yield _.concat(
                StateSagas,
                LoginSagas
            );
        };
    }

})(angular.module('app.store.sagas', [
    'app.components.login.sagas'
]));