require("babel-polyfill");
global.jQuery = global.$ = require('jquery');
require('angular');
require('bootstrap');
global.moment = require('moment');
require('moment-timezone');
require('twix');
global._ = require('lodash');
require('picker');
require('picker.date');
require('picker.time');
require('ngPickadate');
require('angular-resource');
require('satellizer');
require('angular-aria');
require('angular-filter');
require('angular-sanitize');
require('angular-cookies');
require('angular-ui-mask');
require('select2');
require('angular-ui-select');
// require('angular-ui-select2');
require('angulartics');
require('angulartics-gtm');
require('pdfjs-dist');
require('nf-pdf');
require('bootstrap-select');
require('jquery.ns-autogrow');
require('./common/providers/');
require('./config');
require('./routes');
require('./templates');
require('./store/');
require('./components/');
require('../api/');
require('./common/services/');
require('./common/filters/');
require('./common/directives/');
require('./common/exception');

var User = require('../models/User');

let {
    FETCH_JOB_COUNT,
    ADMIN_LOGGED_AS_USER,
    ADMIN_LOG_OUT_AS_USER,
} = require('./store/action');

(function(app) {
    app.constant('APP_CONSTANTS', {
        CHAT_CONVERSATION_TYPE: 1,
        SMS_CONVERSATION_TYPE: 2,
        TAG_TYPE_COMPANY: 2,
        JOB_TYPE_SHIFT: 1,
        JOB_TYPE_PART_TIME: 2,
        JOB_PAYMENT_TYPE_CASH: 1,
        JOB_PAYMENT_TYPE_ONLINE: 2,
        JOB_STATUS_ACTIVATED: "ACTIVATED",
        JOB_STATUS_DRAFT: "DRAFT",
        JOB_STATUS_UNDER_REVIEW: "UNDER_REVIEW",
    });
    app.run(['$rootScope', '$state', '$stateParams', '$http', 'UshiftApp', 'APP_CONSTANTS', 'store', 'API', '$window', '$interval', '$cookieStore', '$cookies', 'Notification', 'Modal', 'preloader',
        function($rootScope, $state, $stateParams, $http, UshiftApp, APP_CONSTANTS, store, API, $window, $interval, $cookieStore, $cookies, Notification, Modal, preloader) {

            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
            $rootScope.APP_CONSTANTS = APP_CONSTANTS;


            if ($cookies.get(ADMIN_SESSION) && $cookies.get(ADMIN_PROFILE)) {
                store.dispatch({
                    type: ADMIN_LOGGED_AS_USER,
                    data: JSON.parse($cookies.get(ADMIN_PROFILE))
                });
            }

            API.auth.has_logged_in()
                .then(function(response) {
                    if (!_.isNil(response.data)) {
                        $rootScope.user = new User(response.data);
                    }
                })
                .catch(function(error) {
                    throw error;
                });

            _.forEach(UshiftApp.timezones, (i) => {
                moment.tz.add(i);
            });

            /*
             * Global asset function
             *
             * @return absolute path of a resource
             */
            $rootScope.asset = function(path) {
                if (path.charAt(0) == '/') {
                    return BASE_URL + path.substr(1);
                } else {
                    return BASE_URL + path;
                }
            };

            /*
             * Global function to logout
             */
            $rootScope.logout = function(callback) {
                if (!_.isNil($rootScope.admin_session) && $rootScope.admin_session === true) {
                    Notification.show('warning', 'Can\'t logout during admin session', 3000);
                    return false;
                }
                API.auth.logout()
                    .then(function() {
                        $cookies.remove(JWT_TOKEN_COOKIE_KEY, {
                            path: '/'
                        });
                        $cookies.remove('LoggedIn', {
                            path: '/'
                        });
                        $rootScope.isLoggedIn = false;

                        if (_.isUndefined(callback)) {
                            document.location.href = BASE_URL;
                        } else {
                            callback();
                        }

                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            /*
             * scroll to top every time $state was changed
             */
            $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
                $rootScope.showMobileNabar = false;
                $rootScope.toState = toState;
                $rootScope.toParams = toParams;
                $rootScope.fromState = fromState;
                $rootScope.fromParams = fromParams;
            });

            $rootScope.scrollTop = function() {
                $("html, body").animate({
                    scrollTop: 0
                }, 500);
            };

            $rootScope.isValidFormOrScrollToError = function(form) {
                var errors = form.$error;
                if (_.isEmpty(errors)) {
                    return true;
                } else {
                    var top;
                    var input_names = [];
                    for (var key in errors) {
                        input_names = _.concat(input_names, _.map(errors[key], (item) => item.$name));
                    }
                    _.forEach(input_names, (item) => {
                        var t = $(`[name=${item}]`).offset().top;
                        if (_.isUndefined(top)) {
                            top = t;
                        } else {
                            if (t < top) {
                                top = t;
                            }
                        }
                    });
                    if (!_.isUndefined(top)) {
                        $("html, body").animate({
                            scrollTop: top - 90
                        }, 500);
                    }
                    return false;
                }
            };

            $rootScope.isStateOrChildOf = function(state) {
                return $state.current.name.indexOf(state) === 0;
            };

            $rootScope.scrollTop();
            $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
                let blockedState = [
                    'app.payment',
                    'app.account-settings',
                    'app.post-job',
                ];
                if ($rootScope.admin_session) {
                    _.forEach(blockedState, function(item) {
                        if (toState.name.indexOf(item) !== -1) {
                            Notification.show('warning', 'It is not accessible', 5000);
                            event.preventDefault();
                        }
                    });
                }
            });

            $rootScope.mainMenus = [{
                menu: 'Draft',
                state: 'app.dashboard.jobs.draft',
                badge: 0,
                label: 'draft',
            }, {
                menu: 'Under Review',
                state: 'app.dashboard.jobs.under_review',
                badge: 0,
                label: 'under_review',
            }, {
                menu: 'Hiring',
                state: 'app.dashboard.jobs.hiring',
                badge: 0,
                label: 'hiring',
            }, {
                menu: 'Pay',
                state: 'app.dashboard.jobs.pay',
                badge: 0,
                label: 'pay',
            }, {
                menu: 'Rate',
                state: 'app.dashboard.jobs.rate',
                badge: 0,
                label: 'rate',
            }, {
                menu: 'Closed',
                state: 'app.dashboard.jobs.closed',
                badge: 0,
                label: 'closed',
            }];

            $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
                $rootScope.scrollTop();
                if ($rootScope.isLoggedIn && $rootScope.user.isEmployer()) {
                    if ($rootScope.isStateOrChildOf('app.dashboard') || $rootScope.isStateOrChildOf('app.post-job')) {
                        store.dispatch({
                            type: FETCH_JOB_COUNT,
                        });
                        $rootScope.subMenus = [];
                    } else {
                        $rootScope.subMenus = [];
                        if (!store.getState().app.is_fetched_job_count) {
                            store.dispatch({
                                type: FETCH_JOB_COUNT,
                            });
                        }
                    }
                }
                preloader.hide();
            });


            $rootScope.middleware = {
                app: function() {
                    // Redirect normal user if the phone number is not verified

                    if (!_.isNil($rootScope.user) && $rootScope.user.isEmployee()) {
                        if ($rootScope.user.getPhone() === '' && $state.current.name !== 'app.profile.update.general-information') {
                            $state.go('app.profile.update.general-information');
                            return false;
                        } else if (!$rootScope.user.isVerifiedPhoneNumber() && $state.current.name !== 'app.profile.update.general-information' && $state.current.name !== 'app.profile.update.verify-phone-number') {
                            $state.go('app.profile.update.verify-phone-number');
                            return false;
                        }
                    }
                },
            };
            $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
                for (var key in $rootScope.middleware) {
                    if (toState.name.indexOf(key) > -1) {
                        $rootScope.middleware[key]();
                        return false;
                    }
                }
            });


            $rootScope.initGoogleMapSdk = function() {
                let event = document.createEvent('Event');
                event.initEvent('googleMapSdkLoaded', true, true);
                $window.dispatchEventGoogleMapSdkLoaded = function() {
                    document.dispatchEvent(event);
                };
                if (document.getElementById('gmap-sdk') !== undefined && document.getElementById('gmap-sdk') !== null) {
                    document.dispatchEvent(event);
                } else {
                    var script = document.createElement("script");
                    script.type = "text/javascript";
                    script.id = 'gmap-sdk';
                    script.src = "//maps.google.com/maps/api/js?key=" + GOOGLE_MAP_API_KEY + "&libraries=places&callback=dispatchEventGoogleMapSdkLoaded";
                    document.body.appendChild(script);
                }
            };
        }

    ]);


    app.controller('MainCtrl', MainCtrl);
    MainCtrl.$inject = ['$rootScope', '$scope', '$state', 'store', 'Notification', '$interval', 'API', '$window', '$analytics', '$cookies'];

    function MainCtrl($rootScope, $scope, $state, store, Notification, $interval, API, $window, $analytics, $cookies) {
        $rootScope.title = 'Globaltestpoint - Job Vacancies';

        $rootScope.featureComingSoon = function() {
            Notification.show('warning', 'Feature coming soon!', 5000);
        };

        if (!_.isNil($rootScope.user) && ($rootScope.user.isEmployee() || $rootScope.user.isEmployer())) {
            if (!$rootScope.user.isVerifiedEmail() && !$rootScope.showEmailVerifyNotice && $state.current.name !== 'app.profile.update.resend_verify_email' && $state.current.name !== 'app.edit-job') {
                $rootScope.showEmailVerifyNotice = true;
                Notification.show('warning', 'Your email is not verified. <a ui-sref="app.profile.update.resend_verify_email">Click here if you have not received the verification email</a>', 10000);
            } else if ($rootScope.user.isEmployee() && !$rootScope.showDownloadAppNotification) {
                $rootScope.showDownloadAppNotification = true;
                Notification.show('warning', 'To continue using UShift, download the App. Website for job seekers will stop on the 10th of August.', 5000);
            }
        }

        var vm = this;
        vm.store = store;
        vm.user = $rootScope.user;
        vm.showPostJobModal = false;

        vm.logoutAsUser = logoutAsUser;
        vm.getLinkFanpages = getLinkFanpages;

        vm.getLinkFanpages();

        vm.subscriber = {};
        vm.subscribe = function(subscriber) {
            API.subscriber.create({
                    email: subscriber.email
                })
                .then((response) => {
                    $analytics.eventTrack('subscribe', {
                        category: 'onboarding',
                        label: 'newsletter',
                        value: '1'
                    });
                    Notification.show('success', 'Success', 5000);
                })
                .catch((error) => {
                    throw error;
                });
        };

        function logoutAsUser() {
            vm.store.dispatch({
                type: ADMIN_LOG_OUT_AS_USER
            });
        }

        function getLinkFanpages() {
            API.option.getLinkFanpages()
                .then(function(web_info) {
                    $rootScope.web_info = web_info;
                })
                .catch(function(error) {
                    throw error;
                });
        }
    }

    app.controller('NoLoginCtrl', ['Notification', 'API', '$analytics', function(Notification, API, $analytics) {
        var vm = this;
        vm.subscriber = {};
        vm.subscribe = function(subscriber) {
            API.subscriber.create({
                    email: subscriber.email
                })
                .then((response) => {
                    $analytics.eventTrack('subscribe', {
                        category: 'onboarding',
                        label: 'newsletter',
                        value: '1'
                    });
                    Notification.show('success', 'Success', 5000);
                })
                .catch((error) => {
                    throw error;
                });
        };
    }]);

})(angular.module('app', [
    'app.common.providers',
    'app.config',
    'app.router',
    'ngResource',
    'ngSanitize',
    'ngCookies',
    'angular.filter',
    'ui.mask',
    'ui.select',
    // 'ui.select2',
    'pickadate',
    'nfpdf',
    'app.template',
    'app.store',
    'app.components',
    'app.common.directives',
    'app.api',
    'app.common.services',
    'app.common.exceptionHandler',
    'app.common.filters',
    'angulartics',
    'angulartics.google.tagmanager',
]));