var moment = require('moment');
(function(app) {

    app.filter('timeFormat', function() {
        return function(time, format) {
            if (_.isObject(time) && time instanceof moment) {
                return time.format(format);
            } else if (_.isString(time)) {
                return moment(time).format(format);
            } else {
                return null;
            }
        };
    });

})(angular.module('app.common.filters.timeFormat', []));
