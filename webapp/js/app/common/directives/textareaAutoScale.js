(function(app) {

    app.directive('textareaAutoScale', function() {
        return {
            restrict: 'AE',
            scope: false,
            link: function(scope, element, attrs) {
                setTimeout(function() {
                    $(element).autogrow({
                        vertical: true,
                        horizontal: false,
                        flickering: false
                    });
                }, 100);
            }
        };
    });

})(angular.module('app.common.directives.textareaAutoScale', []));
