(function(app) {
    app.directive('footer', ['$http', function($http) {
        return {
            restrict: 'E',
            link: function(scope, element, attrs) {
                function setContentMinHeight() {
                    var minHeight = $(window).height() - (95 + $('footer').height());
                    $('div[ui-view="content"]').css({
                        minHeight: minHeight
                    });
                }
                setContentMinHeight();
                $(window).resize(function() {
                    setContentMinHeight();
                });

            },
        };
    }]);
})(angular.module('app.common.directives.footer', []));
