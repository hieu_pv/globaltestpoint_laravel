(function(app) {
    app.directive('map', ['$window', function($window) {
        return {
            restrict: 'E',
            scope: {
                lat: '=',
                lng: '=',
            },
            template: '<div style="height: 150px; width: 100%" id="map_canvas"></div>',
            link: function(scope, element, attrs) {
                if (document.getElementById('gmap-sdk') !== undefined && document.getElementById('gmap-sdk') !== null) {
                    CreateMap(scope.lat, scope.lng);
                } else {
                    var script = document.createElement("script");
                    script.type = "text/javascript";
                    script.id = 'gmap-sdk';
                    script.src = "//maps.google.com/maps/api/js?key=" + GOOGLE_MAP_API_KEY + "&libraries=places&callback=CreateMap";
                    document.body.appendChild(script);
                }
                $window.CreateMap = function() {
                    CreateMap(scope.lat, scope.lng);
                };

                function CreateMap(lat, lng) {
                    var myLatlng = new google.maps.LatLng(lat, lng);
                    var mapOptions = {
                        zoom: 14,
                        center: myLatlng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        scrollwheel: false,
                        navigationControl: false,
                        mapTypeControl: false,
                    };
                    map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
                    var marker = new google.maps.Marker({
                        position: myLatlng,
                        map: map,
                    });
                }
            }
        };
    }]);
})(angular.module('app.common.directives.map', []));
