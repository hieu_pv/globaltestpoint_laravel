(function(app) {

    app.directive('target', target);

    function target() {
        return {
            restrict: 'A',
            scope: {
                target: '@'
            },
            link: function(scope, element, attrs) {
                $(element).click(function() {
                	$(scope.target).slideToggle();
                });
            }
        };
    }

})(angular.module('app.common.directives.target', []));
