(function(app) {
    app.directive('nfswitch', ['$compile', function($compile) {
        return {
            strict: 'E',
            scope: {
                ngModel: '=',
                nfswitch: '=',
                name: '=',
                type: '@',
            },
            link: (scope, element, attrs) => {
                let options = scope.nfswitch || {};

                scope.random_id = randomString(10, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
                scope.type = scope.type || 'checkbox';

                let html = `<div class="material-switch">
                            <input id="{{random_id}}" name="{{scope.name}}" type="checkbox"/>
                            <label for="{{random_id}}" class="label-default"></label>
                        </div>`;

                html = $compile(html)(scope);
                $(html).insertAfter($(element));

                let cb = $(element).next().find('input[type="checkbox"]');
                if (scope.ngModel) {
                    cb.prop('checked', true);
                } else {
                    cb.prop('checked', false);
                }

                cb.on('change', (event) => {
                    console.log(event);
                    scope.ngModel = cb.is(':checked');
                    scope.$apply();
                });

                function randomString(length, chars) {
                    var result = '';
                    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
                    return result;
                }
            }
        };
    }]);
})(angular.module('app.common.directives.nfswitch', []));
