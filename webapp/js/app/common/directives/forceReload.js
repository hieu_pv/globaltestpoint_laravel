(function(app) {
    app.directive('forceReload', function() {
        return {
            restrict: 'A',
            scope: true,
            link: function(scope, element, attrs) {
                $(element).click(function(event) {
                    event.preventDefault();
                    if (attrs.reloadTarget === '_blank') {
                        window.open(attrs.href, '_blank');
                    } else {
                        document.location.href = attrs.href;
                    }
                    return false;
                });
            }
        };
    });
})(angular.module('app.common.directives.forceReload', []));
