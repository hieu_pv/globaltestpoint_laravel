(function(app) {
    app.directive('selectpicker', function() {
        return {
            restrict: 'A',
            scope: {
                selectpicker: '=',
            },
            link: function(scope, element, attrs) {
                scope.selectpicker = element;
            }
        };
    });

})(angular.module('app.common.directives.selectpicker', []));
