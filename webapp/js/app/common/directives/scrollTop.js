(function(app) {
    app.directive('scrollTop', ['$rootScope', function($rootScope) {
        return {
            restrict: 'A',
            scope: {
                selector: '@'
            },
            link: function(scope, element, attrs) {
                $(element).on('click', function(e) {
                    $("html, body").animate({
                        scrollTop: 0
                    }, 500);
                });

                $(window).scroll(function() {
                    var window_scroll_top = $(window).scrollTop();

                    if (window_scroll_top > 200) {
                        $(element).removeClass('hide');
                    } else {
                        $(element).addClass('hide');
                    }
                });
            }
        };
    }]);
})(angular.module('app.common.directives.scrollTop', []));