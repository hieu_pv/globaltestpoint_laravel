(function(app) {
    app.directive('toggleMenu', ['$rootScope', function($rootScope) {
        return {
            restrict: 'A',
            scope: {
                selector: '@'
            },
            link: function(scope, element, attrs) {
                var pull = $(element);
                menu = $(scope.selector);

                $(pull).on('click', function(e) {
                    e.preventDefault();
                    menu.slideToggle();
                });

                $(window).resize(function() {
                    var width = $(window).width();
                    if (width > 992 && menu.is(':hidden')) {
                        menu.removeAttr('style');
                    }
                });
            }
        };
    }]);
})(angular.module('app.common.directives.toggleMenu', []));