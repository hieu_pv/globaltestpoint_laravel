require('./pagination');
require('./forceReload');
require('./map');
require('./geocoding');
require('./toggleDropdown');
require('./simplePagination');
require('./footer');
require('./userProfileImage');
require('./uploadFile');
require('./target');
require('./floatInput');
require('./selectpicker');
require('./preloader');
require('./textareaAutoScale');
require('./autocompleteAddress');
require('./nfswitch');
// require('./betaTester');
require('./timepicker');
require('./fixedRefineButton');
require('./breadcrumb');
require('./toggleMenu');
require('./scrollTop');
(function(app) {

})(angular.module('app.common.directives', [
    'app.common.directives.pagination',
    'app.common.directives.forceReload',
    'app.common.directives.map',
    'app.common.directives.geocoding',
    'app.common.directives.toggleDropdown',
    'app.common.directives.simplePagination',
    'app.common.directives.footer',
    'app.common.directives.userProfileImage',
    'app.common.directives.uploadFile',
    'app.common.directives.target',
    'app.common.directives.floatInput',
    'app.common.directives.selectpicker',
    'app.common.directives.preloader',
    'app.common.directives.textareaAutoScale',
    'app.common.directives.autocompleteAddress',
    'app.common.directives.nfswitch',
    // 'app.common.directives.betaTester',
    'app.common.directives.timepicker',
    'app.common.directives.fixedRefineButton',
    'app.common.directives.breadcrumb',
    'app.common.directives.toggleMenu',
    'app.common.directives.scrollTop'
]));
