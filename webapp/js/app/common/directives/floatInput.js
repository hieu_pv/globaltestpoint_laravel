(function(app) {
    app.directive('floatInput', [function() {
        return {
            restrict: 'AC',
            link: (scope, element, attrs) => {
                element.$focused = false;
                scope.$watch(() => scope.$eval($(element).find('.form-control').attr('ng-model')), (model) => {
                    if (model !== undefined && model !== '') {
                        element.activeLabel();
                    }
                });

                $(element).find('label').click(function() {
                    $(this).parent().find('.form-control').focus();
                });

                $(element).find('[data-toggle="tooltip"]').tooltip();

                element.activeLabel = function() {
                    this.$focused = true;
                    $(this).find('label').addClass('active');
                    setTimeout(() => {
                        let input = $(this).find('.form-control');
                        input.attr('placeholder', input.attr('data-placeholder'));
                    }, 100);
                };
                element.deactiveLabel = function() {
                    this.$focused = false;
                    $(this).find('label').removeClass('active');
                    let input = $(this).find('.form-control');
                    input.attr('placeholder', '');
                };

                $(element).find('.form-control').focusin(function() {
                    element.activeLabel();
                });

                $(element).find('.form-control').change(function() {
                    element.activeLabel();
                });

                $(element).focusout(function() {
                    if (_.isNil($(this).find('.form-control').val()) || $(this).find('.form-control').val() === '') {
                        element.deactiveLabel();
                    }
                });

                var textarea = $(element).find('textarea.form-control');

                if (!_.isUndefined(textarea)) {
                    textarea.on('change', () => {
                        resize(textarea);
                    });
                    textarea.on('cut', () => {
                        delayedResize(textarea);
                    });
                    textarea.on('paste', () => {
                        delayedResize(textarea);
                    });
                    textarea.on('drop', () => {
                        delayedResize(textarea);
                    });
                    textarea.on('keydown', () => {
                        delayedResize(textarea);
                    });
                    textarea.on('keyup', () => {
                        delayedResize(textarea);
                    });
                }

                function resize(textarea) {
                    textarea.css('height', 'auto');
                    textarea.css('height', textarea.prop('scrollHeight') + 'px');
                }

                function delayedResize(textarea) {
                    window.setTimeout(resize(textarea), 0);
                }
            }
        };
    }]);
})(angular.module('app.common.directives.floatInput', []));
