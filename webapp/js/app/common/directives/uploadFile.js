require('ngFileUpload');
(function(app) {
    app.directive('uploadFile', ['Upload', 'Notification', 'Modal', function(Upload, Notification, Modal) {
        return {
            restrict: 'A',
            scope: {
                uploadFile: '=',
                onComplete: '=',
                onError: '=',
                inProgress: '=',
                progressPercentage: '=',
                allowMaxSize: '@',
                accept: '@',
                type: '@',
                crop: '@',
                cropRatioWidth: '@',
                cropRatioHeight: '@',
            },
            link: (scope, element, attrs) => {
                let ratio;

                if (!_.isUndefined(scope.accept)) {
                    var valid_extensions = scope.accept.split('|');
                }
                if (!_.isNil(scope.crop) && scope.crop === 'true') {
                    ratio = !_.isNil(scope.cropRatioWidth) && !_.isNil(scope.cropRatioHeight) ? {
                        with: scope.cropRatioWidth,
                        height: scope.cropRatioHeight,
                    } : {
                        with: 1,
                        height: 1,
                    };

                    let event = document.createEvent('Event');
                    event.initEvent('cropperJsLoadded', true, true);
                    if (document.getElementById('cropper-js') !== undefined && document.getElementById('cropper-js') !== null) {
                        document.dispatchEvent(event);
                    } else {
                        var script = document.createElement("script");
                        script.type = "text/javascript";
                        script.id = 'cropper-js';
                        script.src = "//cdnjs.cloudflare.com/ajax/libs/cropper/2.3.4/cropper.min.js";
                        document.body.appendChild(script);
                        script.onload = function() {
                            document.dispatchEvent(event);
                        };
                    }
                }

                document.addEventListener('cropperJsLoadded', function(e) {

                }, false);

                $(element).click(() => {
                    if (scope.inProgress !== true) {
                        var input = document.createElement('input');
                        input.type = 'file';
                        $(input).trigger('click');
                        $(input).on('change', (event) => {
                            scope.uploadFile = input.files[0];

                            if (input.files[0] !== undefined) {
                                var maxSize = _.isUndefined(scope.allowMaxSize) ? AllowMaxFileSize : scope.allowMaxSize;
                                if (scope.uploadFile.size > maxSize * 1024 * 1024) {
                                    Notification.show('warning', `Please select file less than ${maxSize}MB`, 5000);
                                    return false;
                                }
                                if (!_.isUndefined(scope.accept) && _.isUndefined(_.find(valid_extensions, (item) => item.toLowerCase() === getFileExtension(scope.uploadFile).toLowerCase()))) {
                                    Notification.show('warning', `Please select a file with valid type`, 5000);
                                    return false;
                                }

                                if (!_.isNil(scope.crop) && scope.crop === 'true') {
                                    var reader = new FileReader();
                                    let params = {};

                                    reader.onload = function(e) {
                                        Modal.confirm({
                                            title: 'Crop',
                                            htmlContent: `<div id="cropper"><img id="img-cropper" /></div>`,
                                            size: 'lg',
                                        }, () => {
                                            upload(scope.uploadFile, {
                                                crop: params
                                            });

                                        }, () => {});

                                        setTimeout(() => {
                                            $('#img-cropper').attr('src', e.target.result);

                                            var cropper = $('#img-cropper').cropper({
                                                aspectRatio: ratio.with / ratio.height,
                                                crop: function(e) {
                                                    console.log(e);
                                                    params = e;
                                                }
                                            });
                                        }, 200);
                                    };
                                    reader.readAsDataURL(input.files[0]);
                                } else {
                                    upload(scope.uploadFile);
                                }
                            }
                            scope.$apply();
                        });
                    }
                });

                function getFileExtension(file) {
                    return file.name.split('.').pop();
                }

                function upload(file, params) {
                    params = _.assign(params, {
                        file: file,
                        type: _.isNil(scope.type) ? 'images' : scope.type,
                    }) || {
                        file: file,
                        type: _.isNil(scope.type) ? 'images' : scope.type,
                    };
                    scope.inProgress = true;
                    Upload.upload({
                        url: '/api/file/upload',
                        data: params,
                    }).then(function(response) {
                        scope.inProgress = false;
                        scope.progressPercentage = 0;
                        if (_.isFunction(scope.onComplete)) {
                            scope.onComplete(response);
                        }
                    }, function(response) {
                        scope.inProgress = false;
                        scope.progressPercentage = 0;
                        if (_.isFunction(scope.onError)) {
                            scope.onError(response);
                        }
                    }, function(evt) {
                        scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                        console.log(scope.progressPercentage);
                    });
                }
            },
        };
    }]);
})(angular.module('app.common.directives.uploadFile', [
    'ngFileUpload'
]));
