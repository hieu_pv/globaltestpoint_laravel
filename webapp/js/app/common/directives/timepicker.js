(function(app) {
    app.directive('timepicker', function() {
        return {
            restrict: 'E',
            scope: {
                ngModel: '='
            },
            template: `
                        <ui-select ng-model="time" on-select="onSelect($item, $model)">
                            <ui-select-match placeholder="placeholder">
                                <span ng-bind="time.display()"></span>
                            </ui-select-match>
                            <ui-select-choices repeat="time in times">
                                <div class="item ">
                                    <p ng-bind="time.display()"></p>
                                </div>
                            </ui-select-choices>
                        </ui-select>
                        `,
            link: function(scope, element, attrs) {
                console.log(element);

                function time(options) {
                    this.label = options.label;
                    this.value = options.value;
                    this.display = function() {
                        return this.label;
                    };
                }
                
                let times = [];

                for (i = 0; i < 24; i++) {
                    let m;
                    if (i < 10) {
                        m = moment(`1970-01-01 0${i}:00:00`);
                    } else {
                        m = moment(`1970-01-01 ${i}:00:00`);
                    }
                    times.push({
                        label: m.format('hh:mm A'),
                        value: m.format('HH:mm'),
                    });
                    times.push({
                        label: m.add(30, 'minutes').format('hh:mm A'),
                        value: m.add(30, 'minutes').format('HH:mm'),
                    });
                }

                scope.times = _.map(times, i => new time(i));
                console.log(scope.times);

                scope.onSelect = onSelect;

                function onSelect($item, $model) {
                    console.log($item, $model);
                    scope.ngModel = moment(`1970-01-01 ${$item.value}:00`).toDate();
                }
            }
        };
    });
})(angular.module('app.common.directives.timepicker', []));
