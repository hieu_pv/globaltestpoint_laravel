(function(app) {
    app.directive('userProfileImage', ['$rootScope', 'store', function($rootScope, store) {
        return {
            strict: 'E',
            scope: {
                image: '=',
                isUpdateProfile: '=',
                userRole: '@',
                user: '=',
            },
            template: `<div class="user-profile-image"> 
                    <div ng-if="isUpdateProfile == true">
                        <div class="upload-icon">
                            <div class="icon">
                                <i class="fa fa-camera" aria-hidden="true"></i>
                            </div>
                            <p>Update Profile Picture</p>
                        </div>
                        <div ng-if="isDefaultImage(image) && userRole === 'employer'" class="placeholder-picture">UPLOAD YOUR LOGO</div>
                        <div ng-if="isDefaultImage(image) && userRole === 'employee'" class="placeholder-picture">UPLOAD YOUR PHOTO</div>
                        
                        <img ng-if="!isDefaultImage(image) && userRole === 'employer'" src="/assets/images/transparent-1x1.png" style="background-image: url(\'{{image}}\')" alt="user profile image" alt="user profile image" />
                        <img ng-if="!isDefaultImage(image) && userRole === 'employee'" src="/assets/images/transparent-1x1.png" style="background-image: url(\'{{image}}\')" alt="user profile image" alt="user profile image" />
                    </div>
                    <img ng-if="isUpdateProfile != true" src="/assets/images/transparent-1x1.png" style="background-image: url(\'{{image}}\')" alt="user profile image" alt="user profile image" />
                    <i ng-if="isFavorited === true" class="fa fa-heart"></i>
                </div>`,
            link: function(scope, element, attrs) {
                scope.isDefaultImage = function(image) {
                    var isDefaultImage = false;
                    if (_.isNil(image) || image === '' || image.indexOf('/uploads/default_avatar.png') > -1) {
                        isDefaultImage = true;
                    }
                    return isDefaultImage;
                };
            }
        };
    }]);
})(angular.module('app.common.directives.userProfileImage', []));
