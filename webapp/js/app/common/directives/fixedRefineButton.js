(function(app) {
    app.directive('fixedRefineButton', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                $(window).scroll(function() {
                    var filter_height = $('#job-filter-wrapper').height();
                    var offset_top = $('#job-filter-wrapper').offset().top;
                    var window_height = $(window).height();
                    var window_scroll_top = $(window).scrollTop();

                    if (window_scroll_top + window_height >= filter_height + offset_top) {  
                        $(element).removeClass('fixed');
                    } else {
                        $(element).addClass('fixed');
                    }
                });
            }
        };
    });
})(angular.module('app.common.directives.fixedRefineButton', []));