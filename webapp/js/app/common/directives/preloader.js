(function(app) {
    app.directive('preloader', function() {
        return {
            restrict: 'E',
            templateUrl: '/webapp/js/app/common/partials/_preloader.tpl.html',
            link: function(scope, element, attrs) {

            }
        };
    });
})(angular.module('app.common.directives.preloader', []));
