(function(app) {
    app.directive('betaTester', ['popup', '$cookies', '$cookieStore', 'API', 'Notification', 'preloader', function(popup, $cookies, $cookieStore, API, Notification, preloader) {
        return {
            restrict: 'E',
            templateUrl: '/webapp/js/app/common/partials/_beta_teseter.tpl.html',
            link: function(scope, element, attrs) {
                scope.becomeBetaTester = becomeBetaTester;

                console.log($cookieStore.get('HIDE_BETA_TESTER_POPUP'), $cookieStore.get('HIDE_BETA_TESTER_POPUP') === true, $cookieStore.get('HIDE_BETA_TESTER_POPUP') === 'true');
                if (_.isNil($cookieStore.get('HIDE_BETA_TESTER_POPUP')) || $cookieStore.get('HIDE_BETA_TESTER_POPUP') !== true) {
                    setTimeout(() => {
                        popup.show($('#get-a-call'), 'bounceInRight');
                    }, 1000);
                } else {
                    setTimeout(() => {
                        popup.show($('#get-a-call-small'), 'bounceInRight');
                    }, 1000);
                }


                scope.hideRightConnerPopup = function() {
                    $cookies.put('HIDE_BETA_TESTER_POPUP', true);
                    popup.hide($('#get-a-call'), 'bounceOutRight');
                    setTimeout(() => {
                        popup.show($('#get-a-call-small'), 'bounceInRight');
                    }, 500);
                };

                scope.showBetaTestAppPopup = function() {
                    $cookies.put('HIDE_BETA_TESTER_POPUP', false);
                    popup.hide($('#get-a-call-small'), 'bounceOutRight');
                    setTimeout(() => {
                        popup.show($('#get-a-call'), 'bounceInRight');
                    }, 500);
                };

                function becomeBetaTester(get_the_app) {
                    get_the_app.phone_area_code = '65';
                    preloader.show();
                    API.beta_tester.create(get_the_app)
                        .then(function(response) {
                            scope.hideRightConnerPopup();
                            Notification.show('success', 'Your mobile number is saved successfully', 5000);
                            scope.get_the_app = null;
                            preloader.hide();
                            scope.get_the_app_form.$setPristine();
                        })
                        .catch(function(error) {
                            scope.hideRightConnerPopup();
                            preloader.hide();
                            throw error;
                        });
                }
            }
        };
    }]);
})(angular.module('app.common.directives.betaTester', []));
