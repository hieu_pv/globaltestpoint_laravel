(function(app) {

    app.directive('breadcrumb', ['$rootScope', '$state', function($rootScope, $state) {
        return {
            restrict: 'E',
            templateUrl: '/webapp/js/app/common/partials/breadcrumb.tpl.html',
            scope: {
                data: '='
            },
            link: function(scope, elememt, attrs) {}
        };
    }]);

})(angular.module('app.common.directives.breadcrumb', []));