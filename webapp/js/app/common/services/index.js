require('./notify');
require('./modal');
require('./preloader');
require('./popup');
(function(app) {

})(angular.module('app.common.services', [
    'app.common.services.Notification',
    'app.common.services.Modal',
    'app.common.services.preloader',
    'app.common.services.popup'
]));
