(function(app) {
    app.service('popup', function() {
        this.show = (element, direction) => {
            element.removeClass('hidden')
                .addClass('animated')
                .addClass(direction);

            setTimeout(() => {
                element.removeClass(direction);
            }, 1000);
        };

        this.hide = (element, direction) => {
            element.addClass(direction);
            setTimeout(() => {
                element
                    .addClass('hidden')
                    .removeClass(direction);
            }, 1000);
        };
    });
})(angular.module('app.common.services.popup', []));
