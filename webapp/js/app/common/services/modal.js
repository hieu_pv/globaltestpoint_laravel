require('angular-bootstrap');
(function(app) {
    app.service('Modal', ['$uibModal', function($uibModal) {
        var STR_OK = 'OK';
        var STR_CANCEL = 'Cancel';
        var SIZE = 'sm';
        var sizeAvaiable = ['sm', 'lg', 'md'];

        this.confirm = function() {
            var options, ok_callable, cancel_callable, size;
            if (arguments.length === 0) {
                throw "Modal confirm need at least 1 param";
            }
            options = _.isObject(arguments[0]) ? arguments[0] : {
                textContent: _.toString(arguments[0])
            };

            ok_callable = _.isFunction(arguments[1]) ? arguments[1] : undefined;
            cancel_callable = _.isFunction(arguments[2]) ? arguments[2] : undefined;
            if (!_.isNil(options.size) && sizeAvaiable.indexOf(options.size) !== -1) {
                size = options.size;
            } else {
                size = SIZE;
            }

            if (options.ok === undefined) {
                options.ok = STR_OK;
            }
            if (options.cancel === undefined) {
                options.cancel = STR_CANCEL;
            }
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: '/webapp/js/app/common/partials/modal/_confirm.tpl.html',
                controller: 'ModalInstanceCtrl',
                controllerAs: 'vm',
                size: size,
                resolve: {
                    data: function() {
                        return options;
                    }
                }
            });

            modalInstance.result.then(function() {
                if (ok_callable !== undefined) {
                    ok_callable();
                }
            }, function() {
                if (cancel_callable !== undefined) {
                    cancel_callable();
                }
            });
        };

        this.alert = function() {
            var options, callable, ok_callable, cancel_callable, size;
            if (arguments.length === 0) {
                throw "Modal confirm need at least 1 param";
            }
            options = _.isObject(arguments[0]) ? arguments[0] : {
                textContent: _.toString(arguments[0])
            };
            ok_callable = _.isFunction(arguments[1]) ? arguments[1] : undefined;
            cancel_callable = _.isFunction(arguments[2]) ? arguments[2] : undefined;
            if (!_.isNil(options.size) && sizeAvaiable.indexOf(options.size) !== -1) {
                size = options.size;
            } else {
                size = SIZE;
            }
            if (options.ok === undefined) {
                options.ok = STR_OK;
            }
            if (options.cancel === undefined) {
                options.cancel = STR_CANCEL;
            }
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                templateUrl: '/webapp/js/app/common/partials/modal/_alert.tpl.html',
                controller: 'ModalInstanceCtrl',
                controllerAs: 'vm',
                resolve: {
                    data: function() {
                        return options;
                    }
                }
            });

            modalInstance.result.then(function() {
                if (ok_callable !== undefined) {
                    ok_callable();
                }
            }, function() {
                if (cancel_callable !== undefined) {
                    cancel_callable();
                }
            });
        };

    }]);

    app.controller('ModalInstanceCtrl', ModalInstanceCtrl);
    ModalInstanceCtrl.$inject = ['$rootScope', '$compile', '$uibModalInstance', 'data'];

    function ModalInstanceCtrl($rootScope, $compile, $uibModalInstance, data) {
        var vm = this;
        vm.data = data;
        let scope;
        if (!_.isNil(vm.data.scope)) {
            scope = vm.data.scope;
        } else {
            scope = $rootScope;
        }

        if (!_.isNil(vm.data.htmlContent)) {
            vm.data.htmlContent = $compile(angular.element(`<div>${vm.data.htmlContent}</div>`))($rootScope).html();
        }

        vm.ok = function() {
            $uibModalInstance.close();
        };

        vm.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };
    }

    app.directive('compileTemplate', ['$rootScope', '$compile', '$parse', function($rootScope, $compile, $parse) {
        return {
            link: function(scope, element, attr) {
                var parsed = $parse(attr.ngBindHtml);

                function getStringValue() {
                    return (parsed(scope) || '').toString();
                }

                scope.$watch(getStringValue, function() {
                    $compile(element, null, -9999)($rootScope);
                });
            }
        };
    }]);

})(angular.module('app.common.services.Modal', [
    'ui.bootstrap',
]));
