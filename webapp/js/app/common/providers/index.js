(function(app) {
    app.provider('UshiftApp', function() {
        var currencies = [{
            id: 1,
            country: 'United States',
            name: 'United States Dolar',
            code: 'USD',
            symbols: '$',

        }, {
            id: 2,
            country: 'Singapore',
            name: 'Singapore Dolar',
            code: 'SGD',
            symbols: '$',

        }];

        var driving_licenses = [{
            label: 'Class 3',
            data: 'Class 3',
            selected: false,
        }, {
            label: 'Class 2',
            data: 'Class 2',
            selected: false,
        }, {
            label: 'Class 3 or Class 2',
            data: 'Class 3 or Class 2',
            selected: false,
        }];

        var timezones = [
            "Asia/Singapore|SMT MALT MALST MALT MALT JST SGT SGT|-6T.p -70 -7k -7k -7u -90 -7u -80|012345467|-2Bg6T.p 17anT.p 7hXE dM00 17bO 8Fyu Mspu DTA0|56e5",
            "Etc/UTC|UTC|0|0|",
            "Etc/UTC|UTC",
            "Asia/Ho_Chi_Minh|LMT PLMT ICT IDT JST|-76.E -76.u -70 -80 -90|0123423232|-2yC76.E bK00.a 1h7b6.u 5lz0 18o0 3Oq0 k5b0 aW00 BAM0|90e5",
        ];

        var jobCities = [{
            name: "Singapore"
        }];

        var genders = [{
            name: 'My Ushifter should be a Girl',
            value: 1
        }, {
            name: 'My Ushifter should be a Boy',
            value: 0
        }];

        var userAttributes = {
            profile_url: 'url profile of employee',
            shorten_profile_url: 'shorten url profile of employee',
            email: 'email of employee',
            first_name: 'first name of employee',
            phone_area_code: 'phone area code of employee',
            phone_number: 'phone number of employee',
            gender: 'gender of employee',
            birth: 'birth date of employee',
            address: 'address 1 of employee',
            address2: 'address 2 of employee',
            zipcode: 'zipcode of employee',
            city: 'city of employee',
            country: 'country of employee',
            nationality: 'nationality of employee',
            university: 'university of employee',
            summary: 'description of employee'
        };

        var no_of_weeks_available = [{
            value: 2,
            display: '2 weeks',
        }, {
            value: 3,
            display: '3 weeks',
        }, {
            value: 4,
            display: '4 weeks',
        }, {
            value: 5,
            display: '5 weeks',
        }, {
            value: 6,
            display: '6 weeks',
        }, {
            value: 7,
            display: '7 weeks',
        }, {
            value: 8,
            display: '8 weeks',
        }, {
            value: 9,
            display: '9 weeks',
        }, {
            value: 10,
            display: '10 weeks',
        }, {
            value: 11,
            display: '11 weeks',
        }, {
            value: 12,
            display: '12 weeks',
        }, {
            value: '12+',
            display: 'More than 12 weeks',
        }];

        var no_of_candidates_available = [{
            no_of_candidates: 1,
            title: '1',
        }];
        for (let i = 2; i < 101; i++) {
            no_of_candidates_available.push({
                no_of_candidates: i,
                title: `${i}`,
            });
        }
        no_of_candidates_available.push({
            no_of_candidates: 0,
            title: 'More than 100',
        });

        var payment_methods = [{
            id: 1,
            title: 'Cash'
        }, {
            id: 2,
            title: 'Online payment with UShift'
        }, {
            id: 3,
            title: 'Bank transfer'
        }];

        // Shift Times
        function Time(options) {
            this.label = options.label;
            this.value = options.value;
            this.toDate = function() {
                return this.value.toDate();
            };

            this.format = function(time_format) {
                return moment(this.value).format(time_format);
            };
        }
        var shift_times = [];
        for (let i = 0; i < 24; i++) {
            let m = moment('1970-01-01 00:00:00');
            m.hours(i);
            shift_times.push({
                label: m.format('HH:mm'),
                value: m,
            });

            let mm = moment('1970-01-01 00:00:00');
            mm.hours(i);
            mm.minutes(30);
            shift_times.push({
                label: mm.format('HH:mm'),
                value: mm,
            });
        }
        for (let i = 0; i < 12; i++) {
            let m = moment('1970-01-02 00:00:00');
            m.hours(i);
            shift_times.push({
                label: m.format('HH:mm') + ' (next day)',
                value: m,
            });

            let mm = moment('1970-01-02 00:00:00');
            mm.hours(i);
            mm.minutes(30);
            shift_times.push({
                label: mm.format('HH:mm') + ' (next day)',
                value: mm,
            });
        }
        shift_times = _.map(shift_times, i => new Time(i));

        return {
            currencies: function(value) {
                currencies = value;
            },
            driving_licenses: function(value) {
                driving_licenses = value;
            },
            timezones: function(value) {
                timezones = value;
            },
            jobCities: function(value) {
                jobCities = value;
            },
            genders: function(value) {
                genders = value;
            },
            userAttributes: function(value) {
                userAttributes = value;
            },
            shift_times: function(value) {
                shift_times = value;
            },
            $get: function() {
                return {
                    currencies: currencies,
                    driving_licenses: driving_licenses,
                    timezones: timezones,
                    jobCities: jobCities,
                    genders: genders,
                    userAttributes: userAttributes,
                    no_of_weeks_available: no_of_weeks_available,
                    no_of_candidates_available: no_of_candidates_available,
                    payment_methods: payment_methods,
                    shift_times: shift_times,

                };
            }
        };
    });


})(angular.module('app.common.providers', []));
