require('ui-router');
require('angular-animate');

let {
    FETCH_LOGGED_IN_USER_SUCCESSED,
} = require('./components/login/action');

(function(app) {
    var _getTemplatePath = function() {
        return ['$rootScope', '$templateCache', function($rootScope, $templateCache) {
            var templateUrl;
            if (!_.isNil($rootScope.user) && $rootScope.user.isEmployer()) {
                templateUrl = '/webapp/js/app/common/partials/layouts/_master_employer.tpl.html';
            } else if (!_.isNil($rootScope.user) && $rootScope.user.isEmployee()) {
                templateUrl = '/webapp/js/app/common/partials/layouts/_master_employee.tpl.html';
            } else {
                templateUrl = '/webapp/js/app/common/partials/layouts/_master.tpl.html';
            }
            return $templateCache.get(templateUrl);
        }];
    };

    app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {

        $urlRouterProvider.otherwise('/testyourself/blank-page/andy');

        $stateProvider
            .state('user', {
                views: {
                    'main@': {
                        templateUrl: '/webapp/js/app/common/partials/layouts/_no_login.tpl.html',
                        controller: 'NoLoginCtrl',
                        controllerAs: 'vm',
                    }
                },
                resolve: {
                    user: ['$rootScope', '$state', '$cookies', 'store', 'API', function($rootScope, $state, $cookies, store, API) {
                        if (!_.isNil($cookies.get(JWT_TOKEN_COOKIE_KEY))) {
                            var user = API.user.profile()
                                .then(function(user) {
                                    store.dispatch({
                                        type: FETCH_LOGGED_IN_USER_SUCCESSED,
                                        data: user
                                    });
                                    $rootScope.user = user;
                                    $rootScope.isLoggedIn = true;
                                    return user;
                                })
                                .catch(function(error) {
                                    return undefined;
                                });
                            return user;
                        } else {
                            $cookies.remove(JWT_TOKEN_COOKIE_KEY, {
                                path: '/'
                            });
                            return undefined;
                        }
                    }]
                }
            })
            .state('app', {
                url: '/testyourself/blank-page',
                views: {
                    'main@': {
                        templateProvider: _getTemplatePath(),
                        controller: 'MainCtrl',
                        controllerAs: 'vm',
                    }
                }
                // resolve: {
                //     user: ['$rootScope', '$state', '$cookies', 'store', 'API', function($rootScope, $state, $cookies, store, API) {
                //         if (!_.isUndefined($rootScope.user)) {
                //             return $rootScope.user;
                //         } else {
                //             if (!_.isNil($cookies.get(JWT_TOKEN_COOKIE_KEY))) {
                //                 var user = API.user.profile()
                //                     .then(function(user) {
                //                         if (!user.isEmployer() && !user.isEmployee() && user.getRoles().length > 0) {
                //                             $rootScope.logout();
                //                         } else {
                //                             store.dispatch({
                //                                 type: FETCH_LOGGED_IN_USER_SUCCESSED,
                //                                 data: user
                //                             });
                //                             $rootScope.user = user;
                //                             $rootScope.isLoggedIn = true;
                //                             if (user.needUpdateRole() && $state.current.name !== 'app.profile.update.role') {
                //                                 $state.go('app.profile.update.role');
                //                             }
                //                             return user;
                //                         }
                //                     })
                //                     .catch(function(error) {
                //                         $state.go('user.login');
                //                         return undefined;
                //                     });
                //                 return user;
                //             } else {
                //                 $cookies.remove(JWT_TOKEN_COOKIE_KEY, {
                //                     path: '/'
                //                 });
                //                 document.location.href = BASE_URL + 'login?redirect=' + encodeURIComponent(document.location.href);
                //                 return undefined;
                //             }
                //         }
                //     }]
                // }
            });

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    }]);
})(angular.module('app.router', ['ui.router', 'ngAnimate']));
