var Model = require('./entities/Model');

class Country extends Model {
    constructor(options) {
        super(options);
        this.bind(options);
    }
}

module.exports = Country;
