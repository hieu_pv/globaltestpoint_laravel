var JobModel = require('./entities/JobModel');
var User = require('./User');
var Shift = require('./Shift');
var Industry = require('./Industry');
var Tag = require('./Tag');
var City = require('./City');
var Country = require('./Country');
var Nationality = require('./Nationality');
var JobTitle = require('./JobTitle');
var Experience = require('./Experience');
var Qualification = require('./Qualification');

class Job extends JobModel {
    constructor(options) {
        super(options);
       
        this.employer = d => new User(d.data);
        this.industries = d => d.data.map(i => new Industry(i));
        this.tags = d => d.data.map(i => new Tag(i));
        this.city = d => new City(d.data);
        this.country = d => new Country(d.data);
        this.nationality = d => new Nationality(d.data);
        this.jobTitle = d => new JobTitle(d.data);
        this.experience = d => new Experience(d.data);
        this.qualification = d => new Qualification(d.data);

        this.bind(options);
    }
}

module.exports = Job;
