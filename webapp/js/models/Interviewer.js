var Model = require('./entities/Model');

class Interviewer extends Model {
    constructor(options) {
        super(options);

        this.bind(options);
    }
}


module.exports = Interviewer;
