var Model = require('./entities/Model');

class Option extends Model {
    constructor(options) {
        super(options);

        this.bind(options);
    }
}

module.exports = Option;