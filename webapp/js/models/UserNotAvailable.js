var Model = require('./entities/Model');

class UserNotAvailable extends Model {
    constructor(options) {
        super(options);

        this.bind(options);
    }

    isSelected() {
        return this.selected;
    }
}

module.exports = UserNotAvailable;
