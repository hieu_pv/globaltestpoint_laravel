var Model = require('./entities/Model');
var PreferredPosition = require('./PreferredPosition');
var PreferredLocation = require('./PreferredLocation');
var LastPosition = require('./LastPosition');
var WorkingExperience = require('./WorkingExperience');
var EducationLevel = require('./EducationLevel');

class Employee extends Model {

    constructor(options) {
        super(options);
        this.education_level = d => new EducationLevel(d.data);
        this.preferred_positions = d => d.data.map(i => new PreferredPosition(i));
        this.preferred_locations = d => d.data.map(i => new PreferredLocation(i));
        this.working_experience = d => new WorkingExperience(d.data);
        this.last_positions = d => d.data.map(i => new LastPosition(i));
        this.bind(options);
    }
}

module.exports = Employee;
