var Model = require('./entities/Model');

class ShortenUrl extends Model {
    constructor(options) {
        super(options);

        this.bind(options);
    }
}

module.exports = ShortenUrl;
