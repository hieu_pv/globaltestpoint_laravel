var Model = require('./entities/Model');

class Advertisement extends Model {
    constructor(options) {
        super(options);

        this.bind(options);
    }
}

module.exports = Advertisement;