var Model = require('./entities/Model');

var State = require('./State');
var Job = require('./Job');

class City extends Model {
    constructor(options) {
        super(options);

        this.state = d => new State(d.data);
        this.jobs = d => d.data.map(i => new Job(i));

        this.bind(options);
    }
}

module.exports = City;