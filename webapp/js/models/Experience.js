var Model = require('./entities/Model');

class Experience extends Model {
    constructor(options) {
        super(options);

        this.bind(options);
    }
}

module.exports = Experience;