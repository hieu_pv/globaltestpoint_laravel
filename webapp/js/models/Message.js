var Model = require('./entities/Model');

class Message extends Model {
    constructor(options) {
        super(options);

        this.bind(options);
    }

    isUnread() {
        return this.unread;
    }

    isMine(id) {
        return parseInt(this.sender_id) === parseInt(id);
    }

    getJobSlug() {
        if (this.getType() === 4 || this.getType() === 5) {
            var data = JSON.parse(this.getContent());
            return data.job_slug;
        }
    }

    getEmployeeId() {
        if (this.getType() === 4 || this.getType() === 5) {
            var data = JSON.parse(this.getContent());
            return data.employee_id;
        }
    }

    isAcceptNewApplication() {
        if (this.getType() === 4 || this.getType() === 5) {
            var data = JSON.parse(this.getContent());
            return data.applicationHasBeenAccept;
        }
    }
}

module.exports = Message;
