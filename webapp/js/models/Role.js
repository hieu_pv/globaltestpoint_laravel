var Model = require('./entities/Model');
var Permission = require('./Permission');

class Role extends Model {
    constructor(options) {
        super(options);
        this.permissions = d => d.data.map(i => new Permission(i));
        this.bind(options);
    }

    addPermission(item) {
        this.permissions.push(item);
        return this;
    }

    removePermission(item) {
        var find = this.permissions.find(i => i.getId() === item.getId());
        if (find !== undefined) {
            var index = this.permissions.findIndex(i => i.getId() === item.getId());
            this.permissions.splice(index, 1);
        }
        return this;
    }
}

module.exports = Role;
