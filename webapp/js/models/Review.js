var Model = require('./entities/Model');
var User = require('./User');

class Review extends Model {
    constructor(options) {
        super(options);
        this.reviewer = (d) => {
            return new User(d.data);
        };
        this.reviewee = (d) => {
            return new User(d.data);
        };
        this.job = (d) => {
            return d.data;
        };
        this.bind(options);
    }
}

module.exports = Review;
