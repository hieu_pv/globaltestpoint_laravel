var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function Permission(options) {
    this.id = null;
    this.name = '';
    this.slug = '';
    this.description = '';
    BaseModel.call(this, options);
}

inherits(Permission, BaseModel);

module.exports = Permission;
