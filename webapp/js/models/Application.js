var Model = require('./entities/Model');
var ShiftModel = require('./entities/ShiftModel');
var JobModel = require('./entities/JobModel');
var UserModel = require('./entities/UserModel');
var Image = require('./Image');
var Employer = require('./Employer');

class User extends UserModel {
    constructor(options) {
        super(options);
        this.images = d => d.data.map(i => new Image(i));
        this.bind(options);
    }
}

class Shift extends ShiftModel {
    constructor(options) {
        super(options);

        this.bind(options);
    }
}

class Job extends JobModel {
    constructor(options) {
        super(options);
        this.shifts = d => {
            if (!_.isUndefined(d.data) && _.isArray(d.data)) {
                return d.data.map(i => new Shift(i));
            } else {
                return [];
            }
        };

        this.employer = d => new User(d.data);

        this.bind(options);
    }
}

class Application extends Model {
    constructor(options) {
        super(options);
        this.job = (d) => new Job(d.data);
        this.shift = (d) => new Shift(d.data);
        this.user = (d) => new User(d.data);
        this.bind(options);

        this.getExecuteTimeStart = () => {
            if (!moment(this.execute_time_start).isValid() && !_.isUndefined(this.shift)) {
                return this.shift.start_time;
            } else {
                return this.execute_time_start;
            }
        }

        this.getExecuteTimeEnd = () => {
            if (!moment(this.execute_time_end).isValid() && !_.isUndefined(this.shift)) {
                return this.shift.end_time;
            } else {
                return this.execute_time_end;
            }
        }
    }

    states() {
        const STATE = [{
            state: "JOB_PROPOSAL_SENT",
            params: {
                active: false,
                reject: false,
                accept: false,
                complete: false,
                ee_not_accept: false,
                ee_can_not_go: false,
            }
        }, {
            state: "USER_CAN_NOT_GO",
            params: {
                active: false,
                reject: false,
                accept: false,
                complete: false,
                ee_not_accept: false,
                ee_can_not_go: true,
            }
        }, {
            state: "EMPLOYEE_NOT_INTERESTED",
            params: {
                active: false,
                reject: false,
                accept: false,
                complete: false,
                ee_not_accept: true,
                ee_can_not_go: false,
            }
        }, {
            state: "ADMIN_CANCEL_HIRED",
            params: {
                active: true,
                reject: false,
                accept: false,
                complete: false,
                ee_not_accept: false,
                ee_can_not_go: false,
                admin_cancel_hired: true,
            }
        }, {
            state: "EMPLOYEE_APPLY",
            params: {
                active: true,
                reject: false,
                accept: false,
                complete: false,
                ee_not_accept: false,
                ee_can_not_go: false,
            }
        }, {
            state: "EMPLOYER_REJECTED",
            params: {
                active: true,
                reject: true,
                accept: false,
                complete: false,
                ee_not_accept: false,
            }
        }, {
            state: "EMPLOYER_REJECT_CANDIDATE",
            params: {
                active: true,
                reject: true,
                accept: false,
                complete: false,
                ee_not_accept: false,
            }
        }, {
            state: "EMPLOYER_REJECT_HIRED_CANDIDATE",
            params: {
                active: true,
                reject: true,
                accept: false,
                complete: false,
                ee_not_accept: false,
            }
        }, {
            state: "HIRED",
            params: {
                active: true,
                reject: false,
                accept: true,
                complete: false,
                ee_not_accept: false,
                ee_can_not_go: false,
                admin_cancel_hired: false,
            }
        }, {
            state: "CONFIRMED",
            params: {
                active: true,
                reject: false,
                accept: true,
                complete: false,
                ee_not_accept: false,
                ee_can_not_go: false,
                admin_cancel_hired: false,
                confirmed: true,
            }
        }, {
            state: "NOT_CONFIRMED",
            params: {
                active: true,
                reject: false,
                accept: true,
                complete: false,
                ee_not_accept: false,
                ee_can_not_go: false,
                admin_cancel_hired: false,
                confirmed: false,
            }
        }, {
            state: "COMPLETED",
            params: {
                active: true,
                reject: false,
                accept: true,
                complete: true,
                ee_not_accept: false,
            }
        }, {
            state: "EXPIRED",
            params: {
                active: false,
                reject: false,
                accept: false,
                complete: false,
                ee_not_accept: false,
            }
        }, ];
        return STATE;
    }

    getAdjustedDuration() {
        return this.execute_time_end.diff(this.execute_time_start, 'hours', true);
    }

    getSalary() {
        if (_.isNil(this.shift)) {
            return 0;
        }
        return Math.round(moment(this.shift.end_time).diff(moment(this.shift.start_time), 'hours', true) * this.shift.hourly_rate * 100) / 100;
    }
    
    getAdjustedSalary() {
        if (_.isNil(this.shift)) {
            return 0;
        }
        return Math.round(this.getAdjustedDuration() * this.shift.hourly_rate * 100) / 100;
    }

    getAdjustedServiceFee() {
        if (_.isNil(this.shift)) {
            return 0;
        }
        return Math.round(this.getAdjustedSalary() * this.shift.service_fee_percent) / 100;
    }

    getAdjustedSalaryWithServiceFee() {
        return this.getAdjustedSalary() + this.getAdjustedServiceFee();
    }

    isOverlapping(state) {
        return this.is_overlapping;
    }

    isActived() {
        return this.active;
    }

    isExpired() {
        return this.expired;
    }

    isRejected() {
        return this.reject;
    }

    isAccepted() {
        return this.accept;
    }

    isCompleted() {
        return this.complete;
    }

    isInWaitingList() {
        return this.is_in_waiting_list;
    }

    isEENotAccepted() {
        return this.ee_not_accept;
    }

    getTimeToNow() {
        return this.getCreatedAt().toNow();
    }
}
module.exports = Application;