var Model = require('./entities/Model');

class Availability extends Model {
    constructor(options) {
        super(options);
        this.bind(options);
    }
}

module.exports = Availability;
