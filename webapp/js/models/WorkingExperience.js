var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function WorkingExperience(options) {
    this.id = null;
    this.name = '';
    BaseModel.call(this, options);
}

inherits(WorkingExperience, BaseModel);

module.exports = WorkingExperience;
