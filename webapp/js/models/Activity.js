var Model = require('./entities/Model');

class Activity extends Model {
    constructor(options) {
        super(options);
        this.bind(options);
    } 
}

module.exports = Activity;
