var Model = require('./entities/Model');

class InterestedJob extends Model {
    constructor(options) {
        super(options);

        this.bind(options);
    }
}

module.exports = InterestedJob;
