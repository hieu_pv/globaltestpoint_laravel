var Model = require('./entities/Model');

class SMSSender extends Model {
    constructor(options) {
        super(options);

        this.bind(options);
    }
}

module.exports = SMSSender;
