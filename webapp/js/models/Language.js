var Model = require('./entities/Model');

class Language extends Model {
    constructor(options) {
        super(options);

        this.bind(options);
    }

    isSelected() {
        return this.selected;
    }
}

module.exports = Language;
