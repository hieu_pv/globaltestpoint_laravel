var Model = require('./entities/Model');

class InterestedJobDuration extends Model {
    constructor(options) {
        super(options);
        this.bind(options);
    }
}
module.exports = InterestedJobDuration;
