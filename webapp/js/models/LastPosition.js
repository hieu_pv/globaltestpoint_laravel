var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function LastPosition(options) {
    this.id = null;
    this.company_name = '';
    this.country = '';
    this.role = '';
    this.type = '';
    this.start_date = '';
    this.end_date = '';
    this.reason_of_leaving = '';
    this.name_of_manager = '';
    this.comment = '';
    BaseModel.call(this, options);
}

inherits(LastPosition, BaseModel);

module.exports = LastPosition;
