var Model = require('./entities/Model');
var UserModel = require('./entities/UserModel');
var Image = require('./Image');

class User extends UserModel {
    constructor(options) {
        super(options);
        this.images = d => d.data.map(i => new Image(i));
        this.bind(options);
    }
}

class FavoriteCandidate extends Model {
    constructor(options) {
        super(options);

        this.employee = d => new User(d.data);
        this.bind(options);
    }
}

module.exports = FavoriteCandidate;
