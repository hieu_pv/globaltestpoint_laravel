var Model = require('./entities/Model');

class UpdateField extends Model {
    constructor(options) {
        super(options);

        this.bind(options);
    }
}

module.exports = UpdateField;
