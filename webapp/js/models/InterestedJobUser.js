var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function InterestedJobUser(options) {
    this.interested_job_id = null;
    this.interested_job_duration_id = null;
    this.industry_id = null;
    this.name = '';
    this.slug = '';
    this.order = null;
    BaseModel.call(this, options);
}

inherits(InterestedJobUser, BaseModel);

module.exports = InterestedJobUser;
