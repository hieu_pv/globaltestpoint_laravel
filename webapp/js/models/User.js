var Image = require('./Image');
var Role = require('./Role');
var Employer = require('./Employer');
var Employee = require('./Employee');
var Tag = require('./Tag');
var Industry = require('./Industry');
var Language = require('./Language');
var Application = require('./Application');
var UpdateField = require('./UpdateField');
var Interviewer = require('./Interviewer');
var Card = require('./Card');
var PreferredLocation = require('./PreferredLocation');
var UserNotAvailable = require('./UserNotAvailable');
var FavoriteCandidate = require('./FavoriteCandidate');
var Balance = require('./Balance');

var UserModel = require('./entities/UserModel');

class User extends UserModel {
    constructor(options) {
        super(options);
        this.images = d => d.data.map(i => new Image(i));
        this.payment = d => new Card(d);
        this.interviewers = d => d.data.map(i => new Interviewer(i));
        this.roles = d => d.data.map(i => new Role(i));
        this.tags = d => d.data.map(i => new Tag(i));
        this.employee = d => new Employee(d.data);
        this.employer = d => {
            if (_.isArray(d.data) && d.data.length) {
                return new Employer(_.head(d.data));
            }
        };
        this.service_fees = d => {
            if (_.isArray(d.data) && d.data.length) {
                return _.head(d.data);
            }
        };
        this.industries = d => d.data.map(i => new Industry(i));
        this.languages = d => d.data.map(i => new Language(i));
        this.application = d => new Application(d);
        this.profile_update = d => d.data.map(i => new UpdateField(i));
        this.preferred_locations = d => d.data.map(i => new PreferredLocation(i));
        this.user_not_avaiables = d => d.data.map(i => new UserNotAvailable(i));
        this.favorite_candidates = d => d.data.map(i => new FavoriteCandidate(i));
        this.balance = d => new Balance(d.data);
        this.bind(options);
    }
    addNotAvailable(options) {
        if (_.isArray(this.user_not_avaiables)) {
            this.user_not_avaiables.push(new UserNotAvailable(options));
        } else {
            this.user_not_avaiables = [new UserNotAvailable(options)];
        }
    }
}

module.exports = User;
