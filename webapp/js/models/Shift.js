var ShiftModel = require('./entities/ShiftModel');

var EmployeeApplied = require('./EmployeeApplied');


class Shift extends ShiftModel {
    constructor(options) {
        super(options);
        this.users = (d) => {
            if (d.data !== undefined && Array.isArray(d.data)) {
                return d.data.map(i => new EmployeeApplied(i));
            }
        };
        this.visible_users = (d) => {
            if (d.data !== undefined && Array.isArray(d.data)) {
                return d.data.map(i => new EmployeeApplied(i));
            }
        };
        this.bind(options);
    }
}

module.exports = Shift;
