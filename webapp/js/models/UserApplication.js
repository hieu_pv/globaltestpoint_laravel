var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function UserApplication(options) {
    this.id = null;
    this.title = '';
    this.slug = '';
    this.image = '';
    this.address = '';
    this.latitude = '';
    this.longitude = '';
    this.short_desc = '';
    this.description = '';
    this.start_date = null;
    this.end_date = null;
    this.note = '';
    this.type = '';
    this.payment_type = '';
    this.salary = '';
    this.active = null;
    this.employer = {};
    this.categories = {};
    this.application = {};

    BaseModel.call(this, options);
}

inherits(UserApplication, BaseModel);

UserApplication.prototype.getCategories = function() {
    return this.categories.data;
};

module.exports = UserApplication;
