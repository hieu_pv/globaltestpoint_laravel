function BaseModel(options) {
    this.bind(options);
}

BaseModel.prototype.bind = function(options) {
    options = options || {};
    for (var k in options) {
        var v = options[k];
        if (typeof v === 'function') {
            continue;
        }

        if (this.hasOwnProperty(k)) {
            if (v !== null && v !== undefined) {
                if (typeof this[k] === 'function') {
                    this[k] = this[k](v);
                } else {
                    this[k] = v;
                }
            }
        }
        if (options.timestamps !== undefined) {
            this.timestamps = options.timestamps;
        }
        Object.getOwnPropertyNames(this).forEach((property) => {
            if (options[property] === null || options[property] === undefined) {
                delete this[property];
            } else {
                var proto = Object.getPrototypeOf(this);
                var method = this.camelCase('get_' + property);
                proto[method] = function() {
                    return this[property];
                };
            }
        });
    }
};

BaseModel.prototype.camelCase = function(string) {
    string = string.toLowerCase();
    string = string.replace(/[^a-z0-9]/g, ' ');
    string = string.replace(/\s{2}/g, '');
    string = string.replace(/\w+/g, function(match) {
        return match.replace(/\b./, item => item.toUpperCase());
    });
    string = string.replace(/\s/g, '');
    string = string.replace(/\b./, item => item.toLowerCase());
    return string;
};


BaseModel.prototype.getId = function() {
    if (!this.id) {
        return null;
    }
    return this.id;
};

BaseModel.prototype.getCreatedAt = function() {
    var m = moment.tz(this.timestamps.created_at.date, this.timestamps.created_at.timezone);
    m.tz(AppTimeZone);
    return m;
};

BaseModel.prototype.getUpdatedAt = function() {
    // return this.timestamps.updated_at.date;
    var m = moment.tz(this.timestamps.updated_at.date, this.timestamps.updated_at.timezone);
    m.tz(AppTimeZone);
    return m;
};

BaseModel.prototype.getTimezoneType = function() {
    return this.timestamps.created_at.timezone_type;
};

BaseModel.prototype.getTimezone = function() {
    return this.timestamps.created_at.timezone;
};

function createNewInstanceOfArray(instance, array) {
    return _.map(array, function(item) {
        return new instance(item);
    });
}

module.exports = BaseModel;
