var Model = require('./entities/Model');

class Employer extends Model {
    constructor(options) {
        super(options);

        this.bind(options);
    }
}

module.exports = Employer;
