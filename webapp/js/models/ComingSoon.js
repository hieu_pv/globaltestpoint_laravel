var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function ComingSoon(options) {
    this.id = null;
    this.name = '';
    this.email = '';
    this.interested_finding_job = null;
    this.interested_posting_job = null;
    this.comment = '';
    BaseModel.call(this, options);
}

inherits(ComingSoon, BaseModel);

ComingSoon.prototype.setId = function(value) {
    this.id = value;
};

ComingSoon.prototype.setName = function(value) {
    this.name = value;
};

ComingSoon.prototype.setEmail = function(value) {
    this.email = value;
};

ComingSoon.prototype.setInterestedFindingJob = function(value) {
    this.interested_finding_job = value;
};

ComingSoon.prototype.setComment = function(value) {
    this.comment = value;
};

ComingSoon.prototype.setInterestedPostingJob = function(value) {
    this.interested_posting_job = value;
};

module.exports = ComingSoon;
