var Model = require('./entities/Model');

class University extends Model {
    constructor(options) {
        super(options);

        this.bind(options);
    }
}

module.exports = University;
