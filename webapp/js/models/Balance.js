var Model = require('./entities/Model');

class Balance extends Model {
    constructor(options) {
        super(options);
        this.bind(options);
    }

    getFormattedAmount() {
        return this.amount / 100;
    }
}

module.exports = Balance;
