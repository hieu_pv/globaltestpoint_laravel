var Model = require('./entities/Model');

class JobTitle extends Model {
    constructor(options) {
        super(options);

        this.bind(options);
    }
}

module.exports = JobTitle;