var Model = require('./entities/Model');
var User = require('./User');
var Ingredient = require('./Ingredient');

class RefundTransaction extends Model {
    constructor(options) {
        super(options);
        this.bind(options);
        this.getPayload = () => JSON.parse(this.payload);
    }

    getFormatedAmount() {
        return this.amount / 100;
    }
}

class Transaction extends Model {
    constructor(options) {
        super(options);
        this.customer = (d) => new User(d.data);
        this.ingredients = (d) => _.map(d.data, item => new Ingredient(item));
        this.refunds = (d) => _.map(d.data, item => new RefundTransaction(item));
        this.bind(options);
        this.getPayload = () => JSON.parse(this.payload);
    }

    getFormatedAmount() {
        return this.amount / 100;
    }

    getFormatedServiceFee() {
        return this.service_fee_in_cent / 100;
    }

    getStatus() {
        let status;
        if (this.type == 'charge') {
            if (this.isRefunded()) {
                status = 'refunded';
            } else {
                status = 'paid';
            }
        }
        return status;
    }

    isRefunded() {
        return _.isArray(this.refunds) && this.refunds.length;
    }

    getAvailableRefundAmount() {
        let amount = this.getAmount();
        if (_.isArray(this.refunds) && this.refunds.length) {
            _.forEach(this.refunds, item => {
                amount = amount - item.getAmount();
            });
        }
        return amount;
    }

    getFormatedAvailableRefundAmount() {
        return this.getAvailableRefundAmount() / 100;
    }

    getRefundedAmount() {
        let amount = 0;
        if (_.isArray(this.refunds) && this.refunds.length) {
            _.forEach(this.refunds, item => {
                amount += item.getAmount();
            });
        }
        return amount;
    }

    getFormatedRefundedAmount() {
        return this.getRefundedAmount() / 100;
    }
}

module.exports = Transaction;
