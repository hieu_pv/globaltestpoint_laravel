var Model = require('./entities/Model');
var ShiftModel = require('./entities/ShiftModel');
var User = require('./User');
var ApplicationModel = require('./Application');

class Shift extends ShiftModel {
    constructor(options) {
        super(options);
        this.bind(options);
    }
}

class Application extends ApplicationModel {
    constructor(options) {
        super(options);
        this.shift = d => new Shift(d);
        this.bind(options);
    }
}

class Invoice extends Model {
    constructor(options) {
        super(options);
        this.candidate = d => new User(d.data);
        this.applications = d => _.map(d.data, item => new Application(item));
        this.bind(options);
    }
}


module.exports = Invoice;