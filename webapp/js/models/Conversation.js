var Model = require('./entities/Model');
var Message = require('./Message');
var User = require('./User');

class Conversation extends Model {
    constructor(options) {
        super(options);
        this.users = d => d.data.map(i => new User(i));
        this.messages = d => d.data.map(i => new Message(i));
        this.bind(options);
    }

    isPersonalConversation() {
        return this.is_personal_conversation;
    }

    getReceiver(id) {
        return _.find(this.getUsers(), item => item.getId() !== id);
    }

    getParticipant(id) {
        return _.find(this.getUsers(), item => item.getId() === id);
    }

    hasParticipant(id) {
        return !_.isUndefined(_.find(this.getUsers(), item => item.getId() === id));
    }

    getParticipantsExcerpt(user_id) {
        return _.filter(this.users, item => item.getId() !== parseInt(user_id));
    }

    getParticipantsNameExcerpt(user_id) {
        let participants = _.filter(this.users, item => item.getId() !== parseInt(user_id));
        return _.map(participants, item => item.getName()).join(', ');
    }

    getEmployee() {
        return _.find(this.getUsers(), item => item.isEmployee());
    }

    getMessages() {
        return this.messages;
    }

    isDeleted() {
        return !_.isNil(this.deleted_by) && this.deleted_by !== '';
    }

    addMessage(message) {
        if (_.isUndefined(_.find(this.getMessages(), item => parseInt(item.getId()) === parseInt(message.id)))) {
            this.messages.unshift(new Message(message));
        }
        return this;
    }

    hasNewMessage(current_user_id) {
        if (_.isNil(this.messages)) {
            return false;
        }
        return !_.isUndefined(_.find(this.messages, (item) => item.sender_id !== current_user_id && item.unread));
    }
}

module.exports = Conversation;
