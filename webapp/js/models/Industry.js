var Model = require('./entities/Model');

class Industry extends Model {
    constructor(options) {
        super(options);

        this.bind(options);
    }
}

module.exports = Industry;