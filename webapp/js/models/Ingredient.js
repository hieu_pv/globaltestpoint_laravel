var Model = require('./entities/Model');
var User = require('./User');

class Ingredient extends Model {
    constructor(options) {
        super(options);
        this.user = d => new User(d.data);
        this.bind(options);
        this.getSalaryWithServiceFee = () => {
            if (!_.isNil(this.salary_with_service_fee) && this.salary_with_service_fee !== '') {
                return this.salary_with_service_fee;
            } else {
                return this.salary;
            }
        }
    }
    isPaid() {
        return !_.isNil(this.stripe_transfer_id) && this.stripe_transfer_id !== '';
    }

    isPaidAndPayout() {
        return !_.isNil(this.stripe_transfer_id) && this.stripe_transfer_id !== '' && !_.isNil(this.stripe_payout_id) && this.stripe_payout_id !== '';
    }
}

module.exports = Ingredient;