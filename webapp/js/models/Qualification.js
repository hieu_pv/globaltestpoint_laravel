var Model = require('./entities/Model');

class Qualification extends Model {
    constructor(options) {
        super(options);

        this.bind(options);
    }
}

module.exports = Qualification;