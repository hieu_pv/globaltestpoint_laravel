var Model = require('./entities/Model');

class Currency extends Model {
    constructor(options) {
        super(options);
        this.bind(options);
    }
}

module.exports = Currency;
