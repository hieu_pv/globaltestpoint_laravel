var BaseModel = require('./BaseModel');
var Job = require('./Job');
var User = require('./User');
var inherits = require('inherits');

function Notification(options) {
    this.id = null;
    this.type = '';
    this.notifiable_id = '';
    this.notifiable_type = '';
    this.data = (d => {
        if (!_.isNil(d.job)) {
            d.job = new Job(d.job);
        }
        if (!_.isNil(d.user)) {
            d.user = new User(d.user);
        }
        return d;
    });
    this.read_at = null;
    BaseModel.call(this, options);
}


inherits(Notification, BaseModel);

Notification.prototype.is = function(type) {
    return _.camelCase(this.type) === _.camelCase(type);
};

Notification.prototype.isUnread = function(type) {
    return _.isNil(this.read_at);
};

module.exports = Notification;
