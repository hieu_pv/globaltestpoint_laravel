var Model = require('./entities/Model');

var Country = require('./Country');

class State extends Model {
    constructor(options) {
        super(options);

        this.country = d => new Country(d.data);

        this.bind(options);
    }
}

module.exports = State;