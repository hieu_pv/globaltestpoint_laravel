var Model = require('./entities/Model');

class Nationality extends Model {
    constructor(options) {
        super(options);

        this.bind(options);
    }
}

module.exports = Nationality;