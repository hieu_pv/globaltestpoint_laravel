var BaseModel = require('./BaseModel');
var inherits = require('inherits');
var moment = require('moment');
var Application = require('./Application');
var Image = require('./Image');

function EmployeeApplied(options) {
    this.employee_id = null;
    this.email = '';
    this.first_name = '';
    this.last_name = '';
    this.mobile = '';
    this.gender = '';
    this.birth = '';
    this.address = '';
    this.zipcode = '';
    this.city = '';
    this.country = '';
    this.summary = '';
    this.application = d => new Application(d);
    this.images = d => d.data.map(i => new Image(i));
    BaseModel.call(this, options);
}

inherits(EmployeeApplied, BaseModel);

EmployeeApplied.prototype.getName = function() {
    return this.getFirstName() + ' ' + this.getLastName();
};

EmployeeApplied.prototype.getTime = function() {
    return parseInt(this.application.time);
};

EmployeeApplied.prototype.getImage = function() {
    if (Array.isArray(this.getImages()) && this.getImages().find(i => i.getType() === 'avatar') !== undefined) {
        return this.getImages().find(i => i.getType() === 'avatar').getUrl();
    } else {
        return '';
    }
};

EmployeeApplied.prototype.getVisaImage = function() {
    if (Array.isArray(this.getImages()) && this.getImages().find(i => i.getType() === 'visa') !== undefined) {
        return this.getImages().find(i => i.getType() === 'visa').getUrl()
    } else {
        return '';
    }
};

module.exports = EmployeeApplied;
