var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function Contact(options) {
    this.id = null;
    this.name = '';
    this.email = '';
    this.message = null;
    BaseModel.call(this, options);
}

inherits(Contact, BaseModel);

Contact.prototype.setId = function(value) {
    this.id = value;
};

Contact.prototype.setName = function(value) {
    this.name = value;
};

Contact.prototype.setEmail = function(value) {
    this.email = value;
};

Contact.prototype.setMessage = function(value) {
    this.message = value;
};

module.exports = Contact;
