var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function ReviewDetail(options) {
    this.id = '';
    this.title = '';
    this.active = false;
    BaseModel.call(this, options);

}

inherits(ReviewDetail, BaseModel);

ReviewDetail.prototype.isActivated = function() {
    return this.active
};


module.exports = ReviewDetail;
