var Model = require('./entities/Model');
var ShiftModel = require('./entities/ShiftModel');
var JobModel = require('./entities/JobModel');
var Transaction = require('./Transaction');

class Job extends JobModel {
    constructor(options) {
        super(options);
        this.bind(options);
    }
}

class Shift extends ShiftModel {
    constructor(options) {
        super(options);
        this.job = d => new Job(d.data);
        this.bind(options);
    }
}

class JobSalary extends Model {
    constructor(options) {
        super(options);
        this.shift = d => new Shift(d.data);
        this.Transaction = d => new Transaction(d.data);
        this.bind(options);
    }

    isPaid() {
        return !_.isNil(this.stripe_transfer_id) && this.stripe_transfer_id !== '';
    }
}


module.exports = JobSalary;
