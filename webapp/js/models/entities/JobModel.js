var Model = require('./Model');

class JobModel extends Model {
    states() {
        const STATE = [{
            state: "DRAFT",
            params: {
                active: false,
                archive: false,
                deleted: false,
                complete: false,
                draft: true,
                cancel_by_admin: false,
                cancel_by_employer: false,
            }
        }, {
            state: "UNDER_REVIEW",
            params: {
                active: false,
                archive: false,
                deleted: false,
                complete: false,
                draft: false,
                cancel_by_admin: false,
                cancel_by_employer: false,
            }
        }, {
            state: "ACTIVATED",
            params: {
                active: true,
                archive: false,
                deleted: false,
                complete: false,
                draft: false,
                cancel_by_admin: false,
                cancel_by_employer: false,
            }
        }, {
            state: "PENDING",
            params: {
                archive: false,
                active: false,
                cancel_by_admin: false,
                cancel_by_employer: false,
            }
        }, {
            state: "ARCHIVED",
            params: {
                archive: true,
            }
        }, {
            state: "CANCEL_BY_ADMIN",
            params: {
                cancel_by_admin: true,
                cancel_by_employer: false,
                deleted: true,
            }
        }, {
            state: "CANCEL_BY_EMPLOYER",
            params: {
                cancel_by_admin: false,
                cancel_by_employer: true,
                deleted: true,
            }
        }, {
            state: "DELETED",
            params: {
                deleted: true,
            }
        }, {
            state: "COMPLETED",
            params: {
                complete: true,
            }
        }, {
            state: "REVIEWED",
            params: {
                is_reviewed: true,
            }
        }];
        return STATE;
    }

    setActive(value) {
        this.active = value;
    }

    setArchive(value) {
        this.archive = value;
    }

    isDraft() {
        return this.active == 0 &&
            this.archive == 0 &&
            this.deleted == 0 &&
            this.complete == 0 &&
            this.draft == 1;
    }

    isPast() {
        return _.filter(this.shifts, item => item.isPast()).length === this.shifts.length;
    }

    isActive() {
        return this.deleted == 0 && this.active == 1 && this.cancel_by_admin == 0 && this.cancel_by_employer == 0;
    }

    isArchived() {
        return this.deleted == 0 && this.archive == 1;
    }

    isCompleted() {
        return this.deleted == 0 && this.complete == 1;
    }

    isDeleted() {
        return this.deleted == 1;
    }

    isCanceled() {
        return this.cancel_by_admin == 1 || this.cancel_by_employer == 1;
    }

    isCanceledByAdmin() {
        return this.cancel_by_admin == 1 && this.cancel_by_employer == 0;
    }

    isCanceledByEmployer() {
        return this.cancel_by_admin == 0 && this.cancel_by_employer == 1;
    }

    isHired() {
        return this.countApplications('HIRED') > 0;
    }

    isReviewed() {
        return this.is_reviewed;
    }

    getNeededCandidates() {
        if (_.isArray(this.shifts)) {
            return _.sumBy(this.shifts, item => item.getNoOfCandidates());
        } else {
            return 0;
        }
    }

    countApplications(state) {
        if (_.isArray(this.applications)) {
            if (_.isUndefined(state)) {
                return this.applications.length;
            } else {
                return _.countBy(this.applications, i => i.isState(state))['true'];
            }
        } else {
            return 0;
        }
    }

    getApplicationWithState(state) {
        if (_.isArray(this.applications)) {
            if (_.isUndefined(state)) {
                return this.applications;
            } else {
                return _.filter(this.applications, i => i.isState(state));
            }
        } else {
            return [];
        }
    }

    countShiftApplication(shift_id, state) {
        if (_.isArray(this.applications) && this.applications.length) {
            let applications = _.filter(this.applications, (item) => parseInt(item.shift_id) === parseInt(shift_id));
            if (_.isUndefined(state)) {
                return applications.length;
            } else {
                let count = _.countBy(applications, i => i.isState(state))['true'];
                return _.isUndefined(count) ? 0 : count;
            }
        } else {
            return 0;
        }
    }


    getJobType() {
        let type;
        switch (parseInt(this.type)) {
            case 1:
                type = 'normal job';
                break;
            case 2:
                type = 'nationality job';
                break;
            default:
                break;
        }
        return type;
    }

    isParttimeJob() {
        return parseInt(this.type) === 2;
    }

    isCreditCardPayment() {
        return parseInt(this.payment_method) === 2;
    }

    getPaymentMethodName() {
        let method = '';
        switch (parseInt(this.payment_method)) {
            case 1:
                method = 'Cash';
                break;
            case 2:
                method = 'Online payment with UShift';
                break;
            case 3:
                method = 'Bank transfer';
                break;
            default:
                break;
        }
        return method;
    }

    getSelectedShifts() {
        if (this.shifts !== undefined && Array.isArray(this.shifts)) {
            return _.filter(this.shifts, function(item) {
                return item.selected;
            });
        } else {
            return [];
        }
    }

    getSalary() {
        if (this.shifts.length > 0 && this.getJobType() === 'shifts') {
            let total = _.sumBy(this.shifts, item => Number(item.calcSalary()));
            return Math.round(total * 100) / 100;
        } else {
            return 0;
        }

    }

    getJobServiceFee(service_fee) {
        service_fee = service_fee || {};
        if (this.shifts.length > 0 && this.getJobType() === 'shifts') {
            let total = _.sumBy(this.shifts, function(shift) {
                return shift.calcServiceFee(service_fee);
            });
            return Math.round(total * 100) / 100;
        } else {
            return 0;
        }
    }

    getServiceFeeDiscount() {
        return _.isUndefined(this.service_fee_discount) ? 0 : Math.round(Number(this.service_fee_discount) * 100) / 100;
    }

    getJobServiceFeeAfterDiscount(service_fee) {
        let sf = Math.round((this.getJobServiceFee(service_fee) - this.getServiceFeeDiscount()) * 100) / 100;
        return sf > 0 ? sf : 0;
    }

    getJobSalaryIncludeServiceFee(service_fee) {
        return Math.round((this.getSalary() + this.getJobServiceFeeAfterDiscount(service_fee)) * 100) / 100;
    }

    getAmount() {
        if (this.shifts.length > 0 && this.countApplications('HIRED') > 0 && this.getJobType() === 'shifts') {
            let total = 0;
            let shifts = this.shifts;
            _.forEach(this.getApplicationWithState('HIRED'), function(application) {
                let shift = _.find(shifts, (item) => item.getId() === parseInt(application.shift_id));
                if (!_.isUndefined(shift)) {
                    total += Number(shift.total_salary);
                }
            });
            return Math.round(total * 100) / 100;
        } else {
            return 0;
        }

    }

    getAvgHourlyRate() {
        if (this.shifts.length > 0) {
            return Math.round(_.meanBy(this.shifts, (item) => Number(item.getHourlyRate())) * 100) / 100;
        } else {
            return 0;
        }

    }

    getLastExcutedShift() {
        let lastTime;
        let lastShift;
        if (_.isArray(this.shifts) && this.shifts.length) {
            _.forEach(this.shifts, shift => {
                if (_.isUndefined(lastTime)) {
                    lastTime = moment(shift.getEndTime());
                    lastShift = shift;
                } else {
                    if (moment(shift.getEndTime()).diff(lastTime) > 0) {
                        lastTime = moment(shift.getEndTime());
                        lastShift = shift;
                    }
                }
            });
        }
        return lastShift;
    }

    getFirstExcutedShift() {
        let firstTime;
        let firstShift;
        if (_.isArray(this.shifts) && this.shifts.length) {
            _.forEach(this.shifts, shift => {
                if (_.isUndefined(firstTime)) {
                    firstTime = moment(shift.getEndTime());
                    firstShift = shift;
                } else {
                    if (moment(shift.getEndTime()).diff(firstTime) < 0) {
                        firstTime = moment(shift.getEndTime());
                        firstShift = shift;
                    }
                }
            });
        }
        return firstShift;
    }

    getPaid() {
        if (_.isArray(this.transactions)) {
            let transactions = _.filter(this.transactions, (i) => i.type === 'charge');
            return _.sumBy(transactions, (i) => i.getAmount()) / 100;
        } else {
            return 0;
        }
    }

    getRefunded() {
        if (_.isArray(this.transactions)) {
            let transactions = _.filter(this.transactions, (i) => i.type === 'charge');
            return _.sumBy(transactions, (i) => i.getRefundedAmount()) / 100;
        } else {
            return 0;
        }
    }
}

module.exports = JobModel;
