var moment = require('moment');
var Model = require('./Model');
var Image = require('../Image');
var UpdateField = require('../UpdateField');

class User extends Model {
    getAvatar() {
        if (Array.isArray(this.images) && _.find(this.images, i => i.type === 'avatar') !== undefined) {
            return _.find(this.images, i => i.type === 'avatar').getUrl();
        } else {
            return '';
        }
    }

    getPhone() {
        if (this.phone_area_code !== '' && this.mobile !== '') {
            return '+' + this.phone_area_code + ' ' + this.mobile;
        } else {
            return '';
        }
    }

    getFormattedPhoneNumber() {
        return `+${this.getMobile()}`;
    }

    getImageObject() {
        if (Array.isArray(this.images) && _.find(this.images, i => i.type === 'avatar') !== undefined) {
            return _.find(this.images, i => i.type === 'avatar');
        } else {
            return undefined;
        }
    }

    setImageObject(options) {
        var image;
        if (Array.isArray(this.images) && _.find(this.images, i => i.type === 'avatar') !== undefined) {
            this.images = this.images.map((item) => {
                if (item.getType() === 'avatar') {
                    item.url = options.url;
                    item.meta = options.meta;
                }
                return item;
            });
        } else {
            image = new Image({
                type: 'avatar',
                url: options.url,
                meta: options.meta,
            });
            if (Array.isArray(this.images)) {
                this.images.push(image);
            } else {
                this.images = [image];
            }
        }
    }

    setActive(value) {
        this.active = value;
    }

    setBanned(value) {
        this.banned = value;
    }

    setIdentityCheckStatus(value) {
        this.identity_check_status = value;
    }

    isDefaultImage() {
        let image;
        if (Array.isArray(this.images) && _.find(this.images, i => i.type === 'avatar') !== undefined) {
            image = _.find(this.images, i => i.type === 'avatar').getUrl();
        }
        return !_.isNil(image) && image.indexOf('/uploads/default_avatar.png') > -1;
    }

    getIdImage() {
        if (Array.isArray(this.images) && _.find(this.images, i => i.type === 'id') !== undefined) {
            return _.find(this.images, i => i.type === 'id').getUrl();
        } else {
            return '';
        }
    }

    getIdImageExtension() {
        if (Array.isArray(this.images) && _.find(this.images, i => i.type === 'id') !== undefined) {
            return _.find(this.images, i => i.type === 'id').getExtension();
        } else {
            return '';
        }
    }

    getDrivingLicenseImage() {
        if (Array.isArray(this.images) && _.find(this.images, i => i.type === 'driving_license') !== undefined) {
            return _.find(this.images, i => i.type === 'driving_license').getUrl();
        } else {
            return '';
        }
    }

    getDrivingLicenseImageExtension() {
        if (Array.isArray(this.images) && _.find(this.images, i => i.type === 'driving_license') !== undefined) {
            return _.find(this.images, i => i.type === 'driving_license').getExtension();
        } else {
            return '';
        }
    }

    setAvatar(url) {
        var image;
        if (Array.isArray(this.images) && _.find(this.images, i => i.type === 'avatar') !== undefined) {
            this.images = this.images.map((item) => {
                if (item.getType() === 'avatar') {
                    item.url = url;
                }
                return item;
            });
        } else {
            image = new Image({
                type: 'avatar',
                url: url,
            });
            if (Array.isArray(this.images)) {
                this.images.push(image);
            } else {
                this.images = [image];
            }
        }
    }

    setIdImage(url) {
        var image;
        if (Array.isArray(this.images) && _.find(this.images, i => i.type === 'id') !== undefined) {
            this.images = this.images.map((item) => {
                if (item.getType() === 'id') {
                    item.url = url;
                }
                return item;
            });
        } else {
            image = new Image({
                type: 'id',
                url: url,
            });
            if (Array.isArray(this.images)) {
                this.images.push(image);
            } else {
                this.images = [image];
            }
        }
    }


    setDrivingLicenseImage(url) {
        var image;
        if (Array.isArray(this.images) && _.find(this.images, i => i.type === 'driving_license') !== undefined) {
            this.images = this.images.map((item) => {
                if (item.getType() === 'driving_license') {
                    item.url = url;
                }
                return item;
            });
        } else {
            image = new Image({
                type: 'driving_license',
                url: url,
            });
            if (Array.isArray(this.images)) {
                this.images.push(image);
            } else {
                this.images = [image];
            }
        }
    }

    isVerifiedPhoneNumber() {
        return this.phone_number_verified;
    }

    isVerifiedEmail() {
        return this.email_verified;
    }

    isActivated() {
        return this.active && !this.isBanned();
    }

    isPending() {
        return !this.active && !this.isBanned();
    }

    isBanned() {
        return this.banned;
    }

    needVerifyNewEmail() {
        return !_.isUndefined(_.find(this.profile_update, (item) => item.field === 'email'));
    }

    needVerifyNewPhoneNumber() {
        return !_.isUndefined(_.find(this.profile_update, (item) => item.field === 'phone'));
    }

    getPendingPhoneNumber() {
        let v = _.find(this.profile_update, (item) => item.field === 'phone');
        if (!_.isUndefined(v)) {
            v = JSON.parse(v.value);
            return `+${v.phone_area_code} ${v.phone_number}`;
        } else {
            return `+${this.phone_area_code} ${this.phone_number}`;
        }
    }

    setProfileUpdate(options) {
        this.profile_update.push(new UpdateField(options));
        return this;
    }

    getPendingEmail() {
        let field = _.find(this.profile_update, (item) => item.field === 'email');
        if (!_.isUndefined(field)) {
            return field.value;
        } else {
            return '';
        }
    }


    isUpdatedProfile() {
        return this.is_updated_profile;
    }

    isSocialAccount() {
        return this.is_social_account;
    }

    getUsername() {
        return this.first_name + ' ' + this.last_name;
    }

    getName() {
        if (this.is('employer')) {
            return _.isUndefined(this.employer) ? '' : this.employer.getCompanyName();
        } else {
            if (this.first_name !== '' && this.last_name !== '') {
                return this.first_name + ' ' + this.last_name;
            } else {
                return this.contact_name;
            }
        }
    }
    getNameAdminList() {
        if (this.is('employer')) {
            return this.employer.getCompanyName();
        } else {
            if (this.first_name !== '' && this.last_name !== '') {
                return this.first_name + ' ' + this.last_name;
            } else {
                return `(${this.username})`;
            }
        }
    }

    getAge() {
        if (moment(this.birth).isValid()) {
            return moment().format('YYYY') - moment(this.birth).format('YYYY');
        }
    }


    getLastPositions() {
        return this.employee.last_positions;
    }



    getCompanyAbout() {
        return this.employer.company_about;
    }

    getCompanyAddress() {
        return this.employer.company_address;
    }

    getCompanyMainActivity() {
        return this.employer.company_main_activity;
    }

    getCompanyName() {
        return this.employer.company_name;
    }

    getNumberOfOutlets() {
        return this.employer.number_of_outlets;
    }

    getPreferredContactMethod() {
        return this.employer.preferred_contact_method;
    }

    getPreferredContactTime() {
        return this.employer.preferred_contact_time;
    }

    getRegistedNumber() {
        return this.employer.registration_number;
    }

    hasPaymentAccount() {
        return !_.isUndefined(this.payment.id) && this.payment.id !== '' && !_.isUndefined(this.payment.card_last_four) && this.payment.card_last_four !== '';
    }

    can(permission) {
        var hasPermission = false;
        this.roles.forEach(function(role) {
            if (!_.isUndefined(role.permissions)) {
                if (!_.isUndefined(_.find(role.getPermissions(), function(per) {
                        return per.getSlug() === permission;
                    }))) {
                    hasPermission = true;
                }
            }
        });
        return hasPermission;
    }

    is(role) {
        if (!_.isUndefined(this.roles)) {
            return !_.isUndefined(_.find(this.roles, function(item) {
                return _.toLower(item.name) === _.toLower(role);
            }));
        } else {
            if (!_.isUndefined(this.employee) && role === 'employee') {
                return true;
            } else if (!_.isUndefined(this.employer) && role === 'employer') {
                return true;
            } else {
                return false;
            }
        }
    }

    isEmployer() {
        return this.is('employer');
    }

    isEmployee() {
        return this.is('employee');
    }

    isAdmin() {
        return !this.isEmployer() && !this.isEmployee() && this.roles.length > 0;
    }

    needUpdateRole() {
        return this.roles.length === 0;
    }

    profileCompletedPercent() {
        var completed = 0;
        var total = 0;
        if (this.isEmployee()) {
            let completed_fields = [],
                incompleted_fields = [];
            let fields = ['address', 'birth', 'city', 'country', 'email', 'first_name', 'last_name', 'gender', 'nationality', 'phone_area_code', 'phone_number', 'summary', 'zipcode'];
            for (let k in this) {
                if (fields.indexOf(k) > -1) {
                    total++;
                    if (!_.isNil(this[k]) && this[k] !== '') {
                        completed++;
                        completed_fields.push(k);
                    } else {
                        incompleted_fields.push(k);
                    }
                }
            }

            let employee_fields = ['id_type', 'video'];
            var employee = this.employee;
            for (let k in employee) {
                if (employee_fields.indexOf(k) > -1) {
                    total++;
                    if (!_.isNil(employee[k]) && employee[k] !== '') {
                        completed++;
                        completed_fields.push(k);
                    } else {
                        incompleted_fields.push(k);
                    }
                }
            }

            total++;
            if (!this.isDefaultImage()) {
                completed++;
                completed_fields.push('Image');
            } else {
                incompleted_fields.push('Image');
            }

            total++;
            if (this.getIdImage() !== '') {
                completed++;
                completed_fields.push('ID Image');
            } else {
                incompleted_fields.push('ID Image');
            }

            total++;
            if (_.isArray(this.industries) && this.industries.length > 0) {
                completed++;
                completed_fields.push('Industry');
            } else {
                incompleted_fields.push('Industry');
            }
        } else if (this.isEmployer()) {
            let fields = ['country', 'email', 'gender', 'phone_area_code', 'phone_number'];
            for (let k in this) {
                if (fields.indexOf(k) > -1) {
                    total++;
                    if (!_.isNil(this[k]) && this[k] !== '') {
                        completed++;
                    }
                }
            }
            var employer = this.employer;
            let employer_fields = ['company_about', 'company_address', 'company_name', 'contact_name', 'contact_position', 'registration_number', 'url'];
            for (let k in employer) {
                if (employer_fields.indexOf(k) > -1) {
                    total++;
                    if (!_.isNil(employer[k]) && employer[k] !== '') {
                        completed++;
                    }
                }
            }

            total++;
            if (!this.isDefaultImage()) {
                completed++;
            }

            total++;
            if (_.isArray(this.industries) && this.industries.length > 0) {
                completed++;
            }
        }
        return Math.round(completed / total * 100);
    }

    getRaw() {
        var birth_pattern = /^([0-9]{4})([0-9]{2})([0-9]{2})$/;
        var params = {};
        for (var key in this) {
            if (!_.isFunction(this[key]) && !_.isUndefined(this[key]) && !_.isNull(this[key]) && !_.isObject(this[key])) {
                if (_.isBoolean(this[key])) {
                    params[key] = this[key] ? '1' : '0';
                }
                if (_.isString(this[key])) {
                    params[key] = this[key];
                }
                if (_.isNumber(this[key])) {
                    params[key] = this[key];
                }
            }
        }

        if (this.is('employee') && this.employee !== undefined) {
            // Kind of work
            if (!_.isNil(this.employee.kind_of_work)) {
                params.kind_of_work = this.employee.kind_of_work;
            }
            // Hours per week
            if (!_.isNil(this.employee.hours_per_week)) {
                params.hours_per_week = this.employee.hours_per_week;
            }
            // Allow to work in SG
            if (!_.isNil(this.employee.allow_to_work_in_sg)) {
                params.allow_to_work_in_sg = this.employee.allow_to_work_in_sg;
            }
            // Education level
            if (!_.isNil(this.employee.education_level) && !_.isNil(this.employee.education_level.id)) {
                params.education_level_id = this.employee.education_level.id;
            }
            // working experience
            if (!_.isNil(this.employee.working_experience) && !_.isNil(this.employee.working_experience.id)) {
                params.working_experience_id = this.employee.working_experience.id;
            }
            // Ziggeo video token
            if (!_.isNil(this.employee.video)) {
                params.video = this.employee.video;
            }
            // ID Type
            if (!_.isNil(this.employee.id_type)) {
                params.id_type = this.employee.id_type;
            }
            // Driving License
            if (!_.isNil(this.employee.id_type)) {
                params.driving_license = this.employee.driving_license;
            }
            // Vehicle
            if (!_.isNil(this.employee.id_type)) {
                params.vehicle = this.employee.vehicle;
            }
        }
        if (this.is('employer') && this.employer !== undefined) {
            // Company Name
            if (!_.isNil(this.employer.company_name)) {
                params.company_name = this.employer.company_name;
            }
            // Company URL
            if (!_.isNil(this.employer.website)) {
                params.website = this.employer.website;
            }
            // Working on Singapore
            if (!_.isNil(this.employer.only_citizen_singapore)) {
                params.only_citizen_singapore = this.employer.only_citizen_singapore;
            }
            // Company About
            if (!_.isNil(this.employer.company_about)) {
                params.company_about = this.employer.company_about;
            }
            // Company Registed Number
            if (!_.isNil(this.employer.registration_number)) {
                params.registration_number = this.employer.registration_number;
            }
            // Company Address
            if (!_.isNil(this.employer.company_address)) {
                params.company_address = this.employer.company_address;
            }
            // Company Number of Outlets
            if (!_.isNil(this.employer.number_of_outlets)) {
                params.number_of_outlets = this.employer.number_of_outlets;
            }
            // Company Main Activiry
            if (!_.isNil(this.employer.company_main_activity)) {
                params.company_main_activity = this.employer.company_main_activity;
            }
            // Company Preferred Contact Method
            if (!_.isNil(this.employer.preferred_contact_time)) {
                params.preferred_contact_time = this.employer.preferred_contact_time;
            }
            // Company Preferred Contact Time
            if (!_.isNil(this.employer.contact_name)) {
                params.contact_name = this.employer.contact_name;
            }
            // Company Contact Position
            if (!_.isNil(this.employer.contact_position)) {
                params.contact_position = this.employer.contact_position;
            }
        }
        // Service fee
        if (!_.isNil(this.service_fees) && this.enable_service_fee === true) {
            params.service_fees = this.service_fees;
        }
        if (!_.isNil(this.enable_monthly_subscription) && this.enable_monthly_subscription === true) {
            params.enable_monthly_subscription = 1;
        } else {
            params.enable_monthly_subscription = 0;
        }
        // Country
        if (!_.isNil(this.country) && !_.isString(this.country) && !_.isNil(this.country.name) && !_.isNil(this.country.phone_area_code)) {
            params.country = this.country.name;
            params.phone_area_code = this.country.phone_area_code;
        }
        // Nationality
        if (!_.isNil(this.nationality) && !_.isString(this.nationality) && !_.isNil(this.nationality.name)) {
            params.nationality = this.nationality.name;
        }
        // Optional Phone Area Code
        if (!_.isNil(this.optional_phone_area_code) && !_.isString(this.optional_phone_area_code) && !_.isNil(this.optional_phone_area_code.phone_area_code)) {
            params.optional_phone_area_code = this.optional_phone_area_code.phone_area_code;
        }
        // University
        if (!_.isNil(this.university) && !_.isString(this.university) && !_.isNil(this.university.name)) {
            params.university = this.university.name;
        } else {
            params.university = '';
        }

        // Admin interviewer

        if (!_.isNil(this.interviewers) && _.isArray(this.interviewers) && this.interviewers.length > 0) {
            params.interviewers = this.interviewers;
        }

        // Tag
        if (!_.isUndefined(this.tags) && _.isArray(this.tags) && !_.isEmpty(this.tags)) {
            params.tag_ids = this.tags.map(item => item.id);
        }
        // Industry
        if (!_.isNil(this.industries) && _.isArray(this.industries)) {
            params.industries = this.industries.map(item => item.getId());
        }
        // Interested Job
        if (!_.isNil(this.industries) && _.isArray(this.industries)) {
            params.interested_jobs = [];
            let user_id = this.getId();
            _.forEach(this.industries, function(industry) {
                if (_.isArray(industry.interested_jobs) && industry.interested_jobs.length > 0) {
                    let interested_jobs = _.filter(industry.interested_jobs, item => item.selected);
                    params.interested_jobs = params.interested_jobs || [];
                    params.interested_jobs = _.concat(params.interested_jobs, _.map(interested_jobs, item => _.assign({}, {
                        user_id: user_id,
                        interested_job_id: item.getId(),
                        interested_job_duration_id: item.duration.getId(),
                        tag_ids: _.isArray(item.tags) ? _.map(item.tags, t => t.getId()) : [],
                    })));
                }
            });
        }
        // Language
        if (!_.isNil(this.languages) && _.isArray(this.languages)) {
            params.languages = this.languages.map(item => item.getId());
        }
        // Preferred Location
        if (!_.isNil(this.preferred_locations) && _.isArray(this.preferred_locations)) {
            params.preferred_locations = this.preferred_locations.map(item => item.getId());
        }
        // Birthday
        if (!_.isUndefined(params.birth) && params.birth.match(birth_pattern)) {
            params.birth = params.birth.replace(birth_pattern, "$1-$2-$3");
        }
        // Not available
        if (!_.isNil(this.user_not_avaiables) && _.isArray(this.user_not_avaiables)) {
            params.user_not_avaiables = _.map(this.user_not_avaiables, function(item) {
                item.start = moment(item.start_time).format('YYYY-MM-DD');
                item.end = moment(item.end_time).format('YYYY-MM-DD');
                return item;
            });
        }
        return params;
    }

}

module.exports = User;