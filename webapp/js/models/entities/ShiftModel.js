var Model = require('./Model');

class ShiftModel extends Model {

    states() {
        const STATE = [{
            state: "ACTIVATED",
            params: {
                active: true,
                expired: false,
            }
        }, {
            state: "EXPIRED",
            params: {
                active: false,
                expired: true,
            }
        }, {
            state: "WAITING",
            params: {
                active: false,
                expired: false,
            }
        }, {
            state: "NORMAL",
            params: {
                is_new: false,
            }
        }, {
            state: "IS_NEW",
            params: {
                is_new: true,
            }
        }, {
            state: "COMPLETED",
            params: {
                active: true,
                complete: true,
            }
        }, {
            state: "REVIEWED",
            params: {
                active: true,
                complete: true,
                is_reviewed: true,
            }
        }, ];
        return STATE;
    }

    getSalaryPerHour() {
        var salary_per_hour = Math.floor(this.salary / this.getDuration() * 100) / 100;
        return salary_per_hour;
    }

    isPast() {
        let now = moment();
        return now.diff(moment(this.start_time)) > 0;
    }

    isFeatured() {
        return this.is_featured;
    }

    getMomentObject(attr) {
        switch (attr) {
            case 'start_time':
                return moment(this.start_time);
            case 'end_time':
                return moment(this.end_time);
            default:
                return null;
        }
    }

    calcSalaryPerCandidate() {
        let start, end;
        let salary = 0;
        if (this.start && this.end && !_.isNil(this.start.value) && !_.isNil(this.hourly_rate)) {
            start = moment(this.start.toDate());
            end = moment(this.end.toDate());
            salary = end.diff(start, 'hour', true) * Number(this.hourly_rate);
        } else if (this.start && this.end && !_.isNil(this.hourly_rate)) {
            start = this.start;
            end = this.end;
            salary = end.diff(start, 'hour', true) * Number(this.hourly_rate);
        } else if (this.start_time && this.end_time && !_.isNil(this.hourly_rate)) {
            start = moment(this.start_time);
            end = moment(this.end_time);
            salary = end.diff(start, 'hour', true) * Number(this.hourly_rate);
        }
        return salary;
    }

    calcSalary() {
        return this.calcSalaryPerCandidate() * this.no_of_candidates
    }


    calcServiceFeePercent(service_fees) {
        if (_.isNil(service_fees) || _.isNil(service_fees.less_than_a_day) || _.isNil(service_fees.more_than_a_day)) {
            return 0;
        }
        let start, service_fee_percent;
        if (this.start && this.end && !_.isNil(this.hourly_rate)) {
            // start is not a momment object - checkout /webapp/js/app/common/providers/index.js for more information
            start = moment(this.start.value);
        } else if (_.isNil(this.start) && _.isNil(this.end) && this.start_time && this.end_time && !_.isNil(this.hourly_rate)) {
            start = moment(this.start_time);
        }

        if (_.isNil(start)) {
            return 0;
        }
        let time_diff_to_now_in_minutes = Math.abs(moment().diff(moment(this.date).format('YYYY-MM-DD') + ' ' + start.format('HH:mm:ss'), 'minutes'));
        if (time_diff_to_now_in_minutes <= 24 * 60) {
            service_fee_percent = service_fees.less_than_a_day;
        } else {
            service_fee_percent = service_fees.more_than_a_day;
        }
        return service_fee_percent;
    }

    calcServiceFee(service_fees) {
        return Math.round(this.calcSalary() * this.calcServiceFeePercent(service_fees)) / 100;
    }

    calcServiceFeePerCandidate(service_fees) {
        return Math.round(this.calcSalaryPerCandidate() * this.calcServiceFeePercent(service_fees)) / 100;
    }

    getShiftSalaryWithServiceFee(service_fees) {
        return Math.round((this.calcSalary() + this.calcServiceFee(service_fees)) * 100) / 100;
    }

    getShiftSalaryWithServiceFeePerCandidate(service_fees) {
        return Math.round((this.calcSalaryPerCandidate() + this.calcServiceFeePerCandidate(service_fees)) * 100) / 100;
    }

    isSelected() {
        return this.selected;
    }

    isCompleted() {
        return this.selected;
    }

    isFree() {
        return this.free;
    }

    toggleSelected() {
        this.selected = !this.selected;
        return this;
    }

    isTaken() {
        var user = _.find(this.users, function(user) {
            if (!_.isNil(user) && !_.isNil(user.application)) {
                return user.application.accept;
            }
        });
        return !_.isUndefined(user);
    }

    getDuration() {
        return moment(this.getEndTime()).diff(moment(this.getStartTime()), 'hours', true);
    }

    getHiredCandidate() {
        let users = [];
        if (_.isArray(this.users)) {
            users = this.users;
        } else if (_.isArray(this.visible_users)) {
            users = this.visible_users;
        }

        return _.filter(users, (u) => u.application.accept && u.application.active);
    }

    isExpired() {
        return this.expired;
    }
}


module.exports = ShiftModel;
