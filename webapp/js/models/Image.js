var Model = require('./entities/Model');

class Image extends Model {
    constructor(options) {
        super(options);
        this.bind(options);
    }
    getExtension() {
        return this.url.split('.').pop();
    }
}

module.exports = Image;
