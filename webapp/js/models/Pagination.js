var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function Pagination(options) {
    this.total = null;
    this.count = null;
    this.per_page = null;
    this.current_page = null;
    this.total_pages = null;
    this.links = {
    	prev: null,
    	next: null
    };

    BaseModel.call(this, options);
}

inherits(Pagination, BaseModel);

Pagination.prototype.getPrevPageUrl = function() {
    return this.links.prev;
};

Pagination.prototype.getNextPageUrl = function() {
    return this.links.next;
};

module.exports = Pagination;
