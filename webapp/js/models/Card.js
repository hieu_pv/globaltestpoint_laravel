var Model = require('./entities/Model');

class Card extends Model {
    constructor(options) {
        super(options);
        this.bind(options);
    }
}

module.exports = Card;
