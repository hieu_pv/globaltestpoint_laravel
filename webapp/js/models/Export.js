var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function Export(options) {
    this.id = null;
    this.label = '';

    BaseModel.call(this, options);
}

inherits(Export, BaseModel);

module.exports = Export;
