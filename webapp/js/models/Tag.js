var Model = require('./entities/Model');

class Tag extends Model {
    constructor(options) {
        super(options);

        this.bind(options);
    }

    getActive() {
        return this.active;
    }
}

module.exports = Tag;
