var Model = require('./entities/Model');

class EducationLevel extends Model {
    constructor(options) {
        super(options);
        this.bind(options);
    }
}

module.exports = EducationLevel;
