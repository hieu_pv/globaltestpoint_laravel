var Model = require('./entities/Model');
var Tag = require('./Tag');
var Industry = require('./Industry');
var InterestedJob = require('./InterestedJob');

class JobTemplate extends Model {
    constructor(options) {
        super(options);
        this.tags = d => d.data.map(i => new Tag(i));
        this.industries = d => d.data.map(i => new Industry(i));
        this.interested_jobs = d => d.data.map(i => new InterestedJob(i));
        this.bind(options);
    }
    isOtherTemplate() {
        return this.id === 0;
    }
}

module.exports = JobTemplate;
