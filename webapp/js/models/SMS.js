var Model = require('./entities/Model');

class SMS extends Model {
    constructor(options) {
        super(options);

        this.bind(options);
    }
}

module.exports = SMS;
