var BaseModel = require('./BaseModel');
var inherits = require('inherits');

function PreferredLocation(options) {
    this.id = null;
    this.name = '';
    BaseModel.call(this, options);
}

inherits(PreferredLocation, BaseModel);

module.exports = PreferredLocation;
