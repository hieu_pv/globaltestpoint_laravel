var Job = require('../../models/Job');
var Employer = require('../../models/Employer');
var User = require('../../models/User');
var Pagination = require('../../models/Pagination');
var Application = require('../../models/Application');

var {
    create,
    update,
} = require('./params');

(function(app) {

    app.service('JobService', JobService);
    JobService.$inject = ['$resource', '$http', '$q', '$cookies', 'ApiUrl'];

    function JobService($resource, $http, $q, $cookies, ApiUrl) {
        var url = ApiUrl.get('/api/jobs/:id');
        var resource = $resource(url, {}, {
            get: {
                method: 'GET'
            },
            create: {
                method: 'POST'
            },
            update: {
                method: 'PUT'
            },
            delete: {
                method: 'DELETE'
            }
        });

        this.get = function(params) {
            params = params || {};

            var deferred = $q.defer();
            resource.get(params, function(response) {
                var jobs = _.map(response.data, function(item) {
                    return new Job(item);
                });
                var data = {
                    jobs: jobs,
                    pagination: new Pagination(response.meta.pagination)
                };
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.getAll = function(params) {
            params = params || {};
            var deferred = $q.defer();
            let url = ApiUrl.get('/api/jobs/all');
            $http.get(url, {
                    params: params
                })
                .then(function(response) {
                    var jobs = _.map(response.data.data, function(item) {
                        return new Job(item);
                    });
                    deferred.resolve(jobs);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.createNewJobInstance = function(options) {
            return new Job(options);
        };

        this.getJobBySlug = function(slug, params) {
            params = params || {};
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/jobs/slug/' + slug);

            $http.get(url, {
                params: params
            })
            .then(function(response) {
                deferred.resolve(new Job(response.data.data));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.getSimilarJobs = function(id, params) {
            params = params || {};
            var deferred = $q.defer();
            var url = ApiUrl.get(`/api/jobs/${id}/similar-jobs`);

            $http.get(url, {
                params: params
            })
            .then(function(response) {
                var jobs = _.map(response.data.data, function(item) {
                    return new Job(item);
                });
                var data = {
                    jobs: jobs,
                    pagination: new Pagination(response.data.meta.pagination)
                };
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };
    }

})(angular.module('app.api.job', []));
