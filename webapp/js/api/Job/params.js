class shift {
    constructor(params) {
        if (!_.isUndefined(params.id)) {
            this.id = params.id;
        }
        this.start_time = moment(params.start_time).isValid() ? params.start_time : moment('1970-01-01 00:00:00').format('YYYY-MM-DD HH:mm:ss');
        this.end_time = moment(params.end_time).isValid() ? params.end_time : moment('1970-01-01 00:00:00').add(1, 'hour').format('YYYY-MM-DD HH:mm:ss');
        this.hourly_rate = params.hourly_rate ? params.hourly_rate : 0;
        this.no_of_candidates = params.no_of_candidates;
        this.on_site_contact = !_.isNil(params.on_site_contact) ? params.on_site_contact : '';
        this.on_site_contact_phone_number = !_.isNil(params.on_site_contact_phone_number) ? params.on_site_contact_phone_number : '';
        this.location = params.location;
        this.latitude = params.latitude || 0;
        this.longitude = params.longitude || 0;
        this.zipcode = !_.isUndefined(params.address) && !_.isUndefined(params.address.components.postal_code) ? params.address.components.postal_code : '';
        this.job_title_id = params.job_title.id;
    }
}

class create {
    constructor(params) {
        this.company_name = params.company_name;
        this.city = params.city || 'Singapore';
        this.type = params.type || 1;
        this.payment_method = !_.isUndefined(params.payment) && !_.isUndefined(params.payment.id) ? params.payment.id : 1;
        this.onsite_contact = params.onsite_contact || '';
        this.title = params.title || '';
        this.position = params.position || '';
        this.description = params.description || '';
        this.requirement = params.requirement || '';
        this.immediate_selection = params.immediate_selection || false;
        this.alcoolic_beverage = params.alcoolic_beverage || false;
        this.hallal_food_only = params.hallal_food_only || false;
        this.send_offer_to_favorite_candidate = params.send_offer_to_favorite_candidate || false;
        this.industry_id = params.industry_id || '';
        this.currency = params.currency || 'SGD';
        this.job_template_id = params.job_template_id || 0;
        this.candidate_gender_required = params.gender_required ? params.gender.value : null;
        this.draft = params.draft || false;
        this.driving_license = params.driving_license_required ? params.driving_license.data : '';
        this.language_id = params.language_required ? params.language.id : 0;
        this.send_invite_to_favorite = params.send_invite_to_favorite || false;

        let shifts = _.map(params.shifts, function(shift) {
            if (!_.isUndefined(shift.date)) {
                if (moment(shift.start_time).format('YYYY-MM-DD') !== moment(shift.end_time).format('YYYY-MM-DD')) {
                    shift.end_time = moment(shift.date).add(1, 'day').format('YYYY-MM-DD') + " " + moment(shift.end_time).format('HH:mm:ss');
                } else {
                    shift.end_time = moment(shift.date).format('YYYY-MM-DD') + " " + moment(shift.end_time).format('HH:mm:ss');
                }
                shift.start_time = moment(shift.date).format('YYYY-MM-DD') + " " + moment(shift.start_time).format('HH:mm:ss');
            }
            shift.hourly_rate = shift.hourly_rate;
            shift.no_of_candidates = shift.no_of_candidates;
            shift.description = '';
            return shift;
        });
        this.shifts = _.map(shifts, item => new shift(item));
    }
}

class update {
    constructor(params) {
        this.company_name = params.company_name;
        this.city = params.city || 'Singapore';
        this.type = params.type || 1;
        this.payment_method = params.payment.id || 1;
        this.onsite_contact = params.onsite_contact || '';
        this.title = params.title || '';
        this.position = params.position || '';
        this.description = params.description || '';
        this.requirement = params.requirement ? params.requirement : '';
        this.immediate_selection = params.immediate_selection || false;
        this.alcoolic_beverage = params.alcoolic_beverage || false;
        this.hallal_food_only = params.hallal_food_only || false;
        this.send_offer_to_favorite_candidate = params.send_offer_to_favorite_candidate || false;
        this.industry_id = params.industry_id || '';
        this.currency = params.currency || 'SGD';
        this.job_template_id = params.job_template_id || 0;
        this.candidate_gender_required = params.gender_required ? params.gender.value : null;
        this.draft = params.draft || false;
        this.driving_license = params.driving_license_required ? params.driving_license.data : '';
        this.language_id = params.language_required ? params.language.id : 0;
        this.send_invite_to_favorite = params.send_invite_to_favorite || false;

        let shifts = _.map(params.shifts, function(shift) {
            if (shift.start.format('YYYY-MM-DD') !== shift.end.format('YYYY-MM-DD')) {
                shift.end_time = moment(shift.date).add(1, 'day').format('YYYY-MM-DD') + " " + shift.end.format('HH:mm:ss');
            } else {
                shift.end_time = moment(shift.date).format('YYYY-MM-DD') + " " + shift.end.format('HH:mm:ss');
            }
            shift.start_time = moment(shift.date).format('YYYY-MM-DD') + " " + shift.start.format('HH:mm:ss');
            shift.hourly_rate = shift.hourly_rate;
            shift.no_of_candidates = shift.no_of_candidates;
            shift.description = '';
            return shift;
        });
        this.shifts = _.map(shifts, item => new shift(item));
    }
}
module.exports = {
    create,
    update
};
