require('./ApiUrl');
require('./Admin/');
require('./Auth/');
require('./Role/');
require('./User/');
require('./Job/');
require('./Industries/');
require('./Tag/');
require('./Country/');
require('./City/');
require('./JobTitles/');
require('./Qualifications/');
require('./Experiences/');
require('./Advertisements/');
require('./State/');
require('./Option/');

(function(app) {
    app.factory('API', theFactory);

    theFactory.$inject = [
        'AdminApiService',
        'AuthService',
        'RoleService',
        'UserService',
        'JobService',
        'IndustriesService',
        'TagService',
        'CountryService',
        'CityService',
        'JobTitlesService',
        'QualificationsService',
        'ExperiencesService',
        'AdvertisementsService',
        'StateService',
        'OptionService'
    ];

    function theFactory(
        AdminApiService,
        AuthService,
        RoleService,
        UserService,
        JobService,
        IndustriesService,
        TagService,
        CountryService,
        CityService,
        JobTitlesService,
        QualificationsService,
        ExperiencesService,
        AdvertisementsService,
        StateService,
        OptionService
    ) {
        return {
            admin: AdminApiService,
            auth: AuthService,
            role: RoleService,
            user: UserService,
            job: JobService,
            industries: IndustriesService,
            tag: TagService,
            country: CountryService,
            city: CityService,
            job_titles: JobTitlesService,
            qualifications: QualificationsService,
            experiences: ExperiencesService,
            advertisements: AdvertisementsService,
            state: StateService,
            option: OptionService
        };
    }
})(angular.module('app.api', [
    'app.api.url',
    'app.api.admin',
    'app.api.auth',
    'app.api.role',
    'app.api.user',
    'app.api.job',
    'app.api.industries',
    'app.api.tag',
    'app.api.country',
    'app.api.city',
    'app.api.job_titles',
    'app.api.qualifications',
    'app.api.experiences',
    'app.api.advertisements',
    'app.api.state',
    'app.api.option'
]));