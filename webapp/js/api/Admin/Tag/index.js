var Tag = require('../../../models/Tag');
var Pagination = require('../../../models/Pagination');

(function(app) {

    app.service('AdminTagService', AdminTagService);
    AdminTagService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function AdminTagService($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/admin/tags/:id');

        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
            },
            create: {
                method: 'POST',
            },
            update: {
                method: 'PUT',
            }
        });

        this.get = function(params) {
            params = params || {};
            params.page = params.page || 1;
            var deferred = $q.defer();
            resource.get(params, function(response) {
                deferred.resolve({
                    tags: _.map(response.data, function(item) {
                        return new Tag(item);
                    }),
                    pagination: new Pagination(response.meta.pagination)
                });
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.getAll = function(params) {
            params = params || {};
            var deferred = $q.defer();
            var url = '/api/admin/tags/all';

            $http.get(url, {
                    params
                })
                .then(function(response) {
                    let tags = _.map(response.data.data, (item) => {
                        return new Tag(item);
                    });
                    deferred.resolve(tags);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.show = function(id, include) {
            var deferred = $q.defer();
            var params = {
                id: id
            };
            if (include !== undefined) {
                params.include = include.join();
            }
            resource.get(params, function(response) {
                deferred.resolve(new Tag(response.data));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.create = function(tag) {
            var deferred = $q.defer();
            tag = tag || {};
            resource.create({}, tag, function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.update = function(tag) {
            var deferred = $q.defer();
            tag = tag || {};
            resource.update({
                id: tag.getId()
            }, tag, function(response) {
                deferred.resolve(new Tag(response.data));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.delete = function(id) {
            var deferred = $q.defer();
            resource.delete({
                id: id
            }, function(response) {
                deferred.resolve(response.data);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.changeStatusAllItem = function(item_ids, status) {
            var deferred = $q.defer();
            var url = '/api/admin/tags/change-status-all-items';
            var params = {
                'item_ids': item_ids,
                'status': status
            };
            $http.put(url, params)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.createNewTagInstance = function(options) {
            options = options || {};
            return new Tag(options);
        };
    }

})(angular.module('app.api.admin.tag', []));