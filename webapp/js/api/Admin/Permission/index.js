var Permission = require('../../../models/Permission');
(function(app) {

    app.service('AdminPermissionService', AdminPermissionService);
    AdminPermissionService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function AdminPermissionService($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/admin/permissions/:id');
        var resource = $resource(url, {}, {
            get: {
                method: 'GET'
            },
            create: {
                method: 'POST'
            },
            update: {
                method: 'PUT'
            },
            delete: {
                method: 'DELETE'
            }
        });
        this.get = function(params) {
            params = params || {};
            var deferred = $q.defer();
            resource.get(params, function(response) {
                var permissions = _.map(response.data, function(item) {
                    return new Permission(item);
                });
                deferred.resolve(permissions);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        this.create = function(data) {
            var deferred = $q.defer();
            data = data || {};
            resource.create(data, function(response) {
                deferred.resolve(new Permission(response.data));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.update = function(permission) {
            var deferred = $q.defer();
            permission = permission || {};
            resource.update({
                id: permission.getId()
            }, role, function(response) {
                deferred.resolve(new Permission(response.data));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.delete = function(id) {
            var deferred = $q.defer();
            resource.delete({
                id: id
            }, function(response) {
                deferred.resolve(response.data);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };
    }

})(angular.module('app.api.admin.permission', []));
