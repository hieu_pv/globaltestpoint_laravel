require('./Job/');
require('./User/');
require('./Role/');
require('./Permission/');
require('./Tag/');
require('./Industry/');
require('./Advertisement/');
require('./JobTitle/');
require('./Nationality/');
require('./State/');
require('./City/');
require('./Experience/');
require('./Qualification/');
require('./Country/');

(function(app) {
    app.factory('AdminApiService', AdminApiService);
    AdminApiService.$inject = [
        'AdminJobService',
        'AdminUserService',
        'AdminRoleService',
        'AdminPermissionService',
        'AdminTagService',
        'AdminIndustryService',
        'AdminAdvertisementService',
        'AdminJobTitleService',
        'AdminNationalityService',
        'AdminStateService',
        'AdminCityService',
        'AdminExperienceService',
        'AdminQualificationService',
        'AdminCountryService'
    ];

    function AdminApiService(
        AdminJobService,
        AdminUserService,
        AdminRoleService,
        AdminPermissionService,
        AdminTagService,
        AdminIndustryService,
        AdminAdvertisementService,
        AdminJobTitleService,
        AdminNationalityService,
        AdminStateService,
        AdminCityService,
        AdminExperienceService,
        AdminQualificationService,
        AdminCountryService
    ) {
        return {
            job: AdminJobService,
            user: AdminUserService,
            role: AdminRoleService,
            permission: AdminPermissionService,
            tag: AdminTagService,
            industry: AdminIndustryService,
            advertisement: AdminAdvertisementService,
            job_title: AdminJobTitleService,
            nationality: AdminNationalityService,
            state: AdminStateService,
            city: AdminCityService,
            experience: AdminExperienceService,
            qualification: AdminQualificationService,
            country: AdminCountryService
        };
    }
})(angular.module('app.api.admin', [
    'app.api.admin.job',
    'app.api.admin.user',
    'app.api.admin.role',
    'app.api.admin.permission',
    'app.api.admin.tag',
    'app.api.admin.industry',
    'app.api.admin.advertisement',
    'app.api.admin.job_title',
    'app.api.admin.nationality',
    'app.api.admin.state',
    'app.api.admin.city',
    'app.api.admin.experience',
    'app.api.admin.qualification',
    'app.api.admin.country'
]));