var Role = require('../../../models/Role');
(function(app) {

    app.service('AdminRoleService', AdminRoleService);
    AdminRoleService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function AdminRoleService($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/admin/roles/:id');
        var resource = $resource(url, {}, {
            get: {
                method: 'GET'
            },
            create: {
                method: 'POST'
            },
            update: {
                method: 'PUT'
            },
            delete: {
                method: 'DELETE'
            }
        });
        this.get = function(params) {
            params = params || {};
            var deferred = $q.defer();
            resource.get(params, function(response) {
                var roles = _.map(response.data, function(item) {
                    return new Role(item);
                });
                deferred.resolve(roles);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        this.create = function(data) {
            var deferred = $q.defer();
            data = data || {};
            resource.create(data, function(response) {
                deferred.resolve(new Role(response.data));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.update = function(role) {
            var deferred = $q.defer();
            role = role || {};
            resource.update({
                id: role.getId()
            }, role, function(response) {
                deferred.resolve(new Role(response.data));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.savePermissions = function(params) {
            var deferred = $q.defer();
            params = params || {};
            var url = ApiUrl.get('/api/admin/roles/permissions');
            $http.put(url, params)
                .then(function(response) {
                    deferred.resolve(response.data.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.delete = function(id) {
            var deferred = $q.defer();
            resource.delete({
                id: id
            }, function(response) {
                deferred.resolve(response.data);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };
    }

})(angular.module('app.api.admin.role', []));
