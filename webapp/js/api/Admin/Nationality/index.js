var Nationality = require('../../../models/Nationality');
var Pagination = require('../../../models/Pagination');

(function(app) {

    app.service('AdminNationalityService', AdminNationalityService);
    AdminNationalityService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function AdminNationalityService($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/admin/nationalities/:id');

        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
            },
            create: {
                method: 'POST',
            },
            update: {
                method: 'PUT',
            }
        });

        this.get = function(params) {
            params = params || {};
            params.page = params.page || 1;
            var deferred = $q.defer();
            resource.get(params, function(response) {
                deferred.resolve({
                    nationalities: _.map(response.data, function(item) {
                        return new Nationality(item);
                    }),
                    pagination: new Pagination(response.meta.pagination)
                });
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.getAll = function(params) {
            params = params || {};
            var deferred = $q.defer();
            var url = '/api/admin/nationalities/all';

            $http.get(url, {
                    params
                })
                .then(function(response) {
                    let nationalities = _.map(response.data.data, (item) => {
                        return new Nationality(item);
                    });
                    deferred.resolve(nationalities);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.show = function(id, include) {
            var deferred = $q.defer();
            var params = {
                id: id
            };
            if (include !== undefined) {
                params.include = include.join();
            }
            resource.get(params, function(response) {
                deferred.resolve(new Nationality(response.data));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.create = function(nationality) {
            var deferred = $q.defer();
            nationality = nationality || {};
            resource.create({}, nationality, function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.update = function(nationality) {
            var deferred = $q.defer();
            nationality = nationality || {};
            resource.update({
                id: nationality.getId()
            }, nationality, function(response) {
                deferred.resolve(new Nationality(response.data));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.delete = function(id) {
            var deferred = $q.defer();
            resource.delete({
                id: id
            }, function(response) {
                deferred.resolve(response.data);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.changeStatusAllItem = function(item_ids, status) {
            var deferred = $q.defer();
            var url = '/api/admin/nationalities/change-status-all-items';
            var params = {
                'item_ids': item_ids,
                'status': status
            };
            $http.put(url, params)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };
    }

})(angular.module('app.api.admin.nationality', []));