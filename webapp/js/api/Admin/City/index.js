var City = require('../../../models/City');
var Pagination = require('../../../models/Pagination');

(function(app) {

    app.service('AdminCityService', AdminCityService);
    AdminCityService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function AdminCityService($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/admin/cities/:id');

        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
            },
            create: {
                method: 'POST',
            },
            update: {
                method: 'PUT',
            }
        });

        this.get = function(params) {
            params = params || {};
            params.page = params.page || 1;
            var deferred = $q.defer();
            resource.get(params, function(response) {
                deferred.resolve({
                    cities: _.map(response.data, function(item) {
                        return new City(item);
                    }),
                    pagination: new Pagination(response.meta.pagination)
                });
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.show = function(id, include) {
            var deferred = $q.defer();
            var params = {
                id: id
            };
            if (include !== undefined) {
                params.include = include.join();
            }
            resource.get(params, function(response) {
                deferred.resolve(new City(response.data));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.create = function(city) {
            var deferred = $q.defer();
            city = city || {};
            resource.create({}, city, function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.update = function(city) {
            var deferred = $q.defer();
            city = city || {};
            resource.update({
                id: city.getId()
            }, city, function(response) {
                deferred.resolve(new City(response.data));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.delete = function(id) {
            var deferred = $q.defer();
            resource.delete({
                id: id
            }, function(response) {
                deferred.resolve(response.data);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.changeStatusAllItem = function(item_ids, status) {
            var deferred = $q.defer();
            var url = '/api/admin/cities/change-status-all-items';
            var params = {
                'item_ids': item_ids,
                'status': status
            };
            $http.put(url, params)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };
    }

})(angular.module('app.api.admin.city', []));