var JobTitle = require('../../../models/JobTitle');
var Pagination = require('../../../models/Pagination');

(function(app) {

    app.service('AdminJobTitleService', AdminJobTitleService);
    AdminJobTitleService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function AdminJobTitleService($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/admin/job_titles/:id');

        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
            },
            create: {
                method: 'POST',
            },
            update: {
                method: 'PUT',
            }
        });

        this.get = function(params) {
            params = params || {};
            params.page = params.page || 1;
            var deferred = $q.defer();
            resource.get(params, function(response) {
                deferred.resolve({
                    job_titles: _.map(response.data, function(item) {
                        return new JobTitle(item);
                    }),
                    pagination: new Pagination(response.meta.pagination)
                });
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.getAll = function(params) {
            params = params || {};
            var deferred = $q.defer();
            var url = '/api/admin/job_titles/all';

            $http.get(url, {
                    params
                })
                .then(function(response) {
                    let job_titles = _.map(response.data.data, (item) => {
                        return new JobTitle(item);
                    });
                    deferred.resolve(job_titles);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.show = function(id, include) {
            var deferred = $q.defer();
            var params = {
                id: id
            };
            if (include !== undefined) {
                params.include = include.join();
            }
            resource.get(params, function(response) {
                deferred.resolve(new JobTitle(response.data));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.create = function(job_title) {
            var deferred = $q.defer();
            job_title = job_title || {};
            resource.create({}, job_title, function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.update = function(job_title) {
            var deferred = $q.defer();
            job_title = job_title || {};
            resource.update({
                id: job_title.getId()
            }, job_title, function(response) {
                deferred.resolve(new JobTitle(response.data));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.delete = function(id) {
            var deferred = $q.defer();
            resource.delete({
                id: id
            }, function(response) {
                deferred.resolve(response.data);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.changeStatusAllItem = function(item_ids, status) {
            var deferred = $q.defer();
            var url = '/api/admin/job_titles/change-status-all-items';
            var params = {
                'item_ids': item_ids,
                'status': status
            };
            $http.put(url, params)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };
    }

})(angular.module('app.api.admin.job_title', []));