var Job = require('../../../models/Job');
var Employer = require('../../../models/Employer');
var User = require('../../../models/User');
var Shift = require('../../../models/Shift');
var Pagination = require('../../../models/Pagination');
var EmployeeApplied = require('../../../models/EmployeeApplied');

let {
    create,
    update,
} = require('./params');

(function(app) {

    app.service('AdminJobService', AdminJobService);
    AdminJobService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function AdminJobService($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/admin/jobs/:id');
        var resource = $resource(url, {}, {
            get: {
                method: 'GET'
            },
            create: {
                method: 'POST'
            },
            update: {
                method: 'PUT'
            },
            delete: {
                method: 'DELETE'
            }
        });

        this.get = function(params) {
            params = params || {};
            params.page = params.page || 1;
            var deferred = $q.defer();
            resource.get(params, function(response) {
                var jobs = _.map(response.data, function(item) {
                    return new Job(item);
                });
                var data = {
                    jobs: jobs,
                    pagination: new Pagination(response.meta.pagination)
                };
                deferred.resolve(data);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        this.list = function(params) {
            params = params || {};
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/admin/jobs/list');
            $http.post(url, params)
                .then(function(response) {
                    deferred.resolve(_.map(response.data.data, function(item) {
                        return new Job(item);
                    }));
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.getCandidateCanceled = function(params) {
            params = params || {};
            var url = ApiUrl.get('/api/admin/jobs/candidate-canceled');
            var deferred = $q.defer();
            $http.get(url, {
                    params: params
                })
                .then(function(response) {
                    var jobs = _.map(response.data.data, function(item) {
                        return new Job(item);
                    });
                    var data = {
                        jobs: jobs,
                        pagination: new Pagination(response.meta.data.pagination)
                    };
                    deferred.resolve(data);
                }, function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        this.create = function(job) {
            var deferred = $q.defer();
            let params = new create(job);
            resource.create(params, function(response) {
                deferred.resolve(new Job(response.data));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.update = function(job) {
            var deferred = $q.defer();
            let params = new update(job);
            resource.update({
                id: job.getId()
            }, params, function(response) {
                deferred.resolve(new Job(response.data));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.updateFeatured = function(params) {
            params = params || {};
            var deferred = $q.defer();
            let url = ApiUrl.get(`/api/admin/jobs/featured-shift`)
            $http.put(url, params)
                .then(function(response) {
                    deferred.resolve(response.data.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        }

        this.delete = function(id) {
            var deferred = $q.defer();
            resource.delete({
                id: id
            }, function(response) {
                deferred.resolve(response.data);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.getBySlug = function(slug, include) {
            slug = slug || '';
            if (!_.isUndefined(include) && _.isArray(include)) {
                include = include.join(',');
            }
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/admin/jobs/slug/' + slug);
            $http.get(url, {
                    params: {
                        include: include
                    }
                })
                .then(function(response) {
                    deferred.resolve(new Job(response.data.data));
                }, function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        this.getRelated = function(id) {
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/jobs/' + id + '/related');
            $http.get(url, {
                    params: {
                        include: 'employer'
                    }
                })
                .then(function(response) {
                    var jobs = _.map(response.data.data, function(item) {
                        item.employer = new Employer(item.employer.data);
                        return new Job(item);
                    });
                    var data = {
                        jobs: jobs,
                        pagination: new Pagination(response.data.meta.pagination),
                    };
                    deferred.resolve(data);
                }, function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        this.show = function(id, include) {
            var deferred = $q.defer();
            var params = {
                id: id
            };
            if (include !== undefined && include instanceof Array) {
                params.include = include.join(',');
            }
            resource.get(params, function(response) {
                deferred.resolve(new Job(response.data));
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        function getApplications(items) {
            var applications = [];
            _.forEach(items, function(item) {
                applications.push(item.application);
            });
            return applications;
        }

        this.apply = function(data, job_id) {
            var deferred = $q.defer();
            data = data || {};
            var url = ApiUrl.get('/api/admin/jobs/' + job_id + '/apply');
            $http.post(url, data)
                .then(function(response) {
                    var emloyees = _.groupBy(response.data.data, 'employee_id');
                    emloyees = _.map(emloyees, function(items) {
                        var obj = items[0];
                        obj.applications = getApplications(items);
                        _.unset(obj, 'application');
                        return obj;
                    });
                    emloyees = _.map(emloyees, function(item) {
                        return new EmployeeApplied(item);
                    });
                    deferred.resolve(emloyees);
                }, function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        this.isSentApplication = function(job_id, user_id) {

            var deferred = $q.defer();
            var url = ApiUrl.get('/api/jobs/' + job_id + '/apply/' + user_id + '/is-sent-application');
            $http.get(url)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        this.reject = function(job_id, employee_id) {
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/jobs/' + job_id + '/reject');
            var params = {
                employee_id: employee_id,
            };
            $http.put(url, params)
                .then(function(response) {
                    deferred.resolve(new Application(response.data.data));
                }, function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        this.cancelPendingJob = function(job_id) {
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/jobs/' + job_id + '/cancel-pending-job');
            $http.put(url)
                .then(function(response) {
                    deferred.resolve(new User(response.data.data));
                }, function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        this.changeJobStatus = function(job_id, status) {
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/jobs/' + job_id + '/status');
            $http.put(url, {
                    completed: status
                })
                .then(function(response) {
                    deferred.resolve(new Job(response.data.data));
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.changeStatusAllItem = function(item_ids, status) {
            var deferred = $q.defer();
            var url = '/api/admin/jobs/change-status-all-items';
            var params = {
                'item_ids': item_ids,
                'status': status
            };
            $http.put(url, params)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.sendInvoice = function(params) {
            var deferred = $q.defer();
            var url = '/api/admin/jobs/send-invoice';
            params = params || {};
            $http.put(url, params)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.complete = function(job_id, params) {
            params = params || {};
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/admin/jobs/' + job_id + '/complete');
            $http.put(url, params)
                .then(function(response) {
                    deferred.resolve(new Job(response.data.data));
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.markAsIncomplete = function(job_id, params) {
            params = params || {};
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/admin/jobs/' + job_id + '/incomplete');
            $http.put(url, params)
                .then(function(response) {
                    deferred.resolve(new Job(response.data.data));
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.createNewJobInstance = function(options) {
            return new Job(options);
        };

        this.jobHistory = function(params) {
            params = params || {};
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/admin/jobs/history');
            $http.get(url, {
                    params: params
                })
                .then(function(response) {
                    let jobs = _.map(response.data.data, function(item) {
                        return new Job(item);
                    });
                    deferred.resolve(jobs);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.downloadInvoice = function(params) {
            params = params || {};
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/admin/jobs/download-invoice');
            $http.get(url, {
                    params: params
                })
                .then(function(response) {
                    console.log('chung ', response);
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.hiredApplications = function(params) {
            params = params || {};
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/admin/jobs/hired-applications');
            $http.get(url, {
                    params: params
                })
                .then(function(response) {
                    let applications = _.map(response.data.data, function(application) {
                        application.applications = _.map(application.applications, function(item) {
                            item.shift = new Shift(item.shift);
                            return item;
                        });
                        application.candidate = new User(application.candidate);
                        return application;
                    });
                    deferred.resolve(applications);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.addCandidateToWaitingList = function(params) {
            params = params || {};
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/admin/waiting-list');
            $http.post(url, params)
                .then(function(response) {
                    deferred.resolve(response.data.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };
    }

})(angular.module('app.api.admin.job', []));
