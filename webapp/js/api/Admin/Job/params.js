class create {
    constructor(params) {
        this.employer_id = params.employer.getId();
        this.company_name = params.company_name;
        this.title = params.title;
        this.short_desc = params.short_desc;
        this.description = params.description;
        this.desired_candidate_profile = params.desired_candidate_profile;
        this.industry_ids = _.map(params.industries, 'id');
        this.tag_ids = _.map(params.tags, 'id');
        this.address = params.address;
        this.location = params.location;
        this.certificate = params.certificate;
        this.gender = params.gender_required ? params.gender.value : null;
        this.min_salary = params.min_salary;
        this.max_salary = params.max_salary;
        this.currency = params.currency;
        this.job_title_id = params.job_title.getId();
        this.experience_id = params.experience.getId();
        this.nationality_id = params.nationality.getId();
        this.qualification_id = params.qualification.getId();
        this.country_id = !_.isNil(params.country) ? params.country.getId() : 0;
        this.state_id = !_.isNil(params.state) ? params.state.getId() : 0;
        this.city_id = !_.isNil(params.city) ? params.city.getId() : 0;
        this.type = params.type || 1;
    }
}

class update {
    constructor(params) {
        this.employer_id = params.employer.getId();
        this.company_name = params.company_name;
        this.title = params.title;
        this.short_desc = params.short_desc;
        this.description = params.description;
        this.desired_candidate_profile = params.desired_candidate_profile;
        this.industry_ids = _.map(params.industries, 'id');
        this.tag_ids = _.map(params.tags, 'id');
        this.address = params.address;
        this.location = params.location;
        this.certificate = params.certificate;
        this.gender = params.gender_required ? params.gender.value : null;
        this.min_salary = params.min_salary;
        this.max_salary = params.max_salary;
        this.currency = params.currency;
        this.job_title_id = params.job_title.getId();
        this.experience_id = params.experience.getId();
        this.nationality_id = params.nationality.getId();
        this.qualification_id = params.qualification.getId();
        this.country_id = !_.isNil(params.country) ? params.country.getId() : 0;
        this.state_id = !_.isNil(params.state) ? params.state.getId() : 0;
        this.city_id = !_.isNil(params.city) ? params.city.getId() : 0;
        this.type = params.type || 1;
    }
}
module.exports = {
    create,
    update
};