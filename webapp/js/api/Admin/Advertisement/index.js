var Advertisement = require('../../../models/Advertisement');
var Pagination = require('../../../models/Pagination');

(function(app) {

    app.service('AdminAdvertisementService', AdminAdvertisementService);
    AdminAdvertisementService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function AdminAdvertisementService($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/admin/advertisements/:id');

        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
            },
            create: {
                method: 'POST',
            },
            update: {
                method: 'PUT',
            }
        });

        this.get = function(params) {
            params = params || {};
            params.page = params.page || 1;
            var deferred = $q.defer();
            resource.get(params, function(response) {
                deferred.resolve({
                    advertisements: _.map(response.data, function(item) {
                        return new Advertisement(item);
                    }),
                    pagination: new Pagination(response.meta.pagination)
                });
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.show = function(id, include) {
            var deferred = $q.defer();
            var params = {
                id: id
            };
            if (include !== undefined) {
                params.include = include.join();
            }
            resource.get(params, function(response) {
                deferred.resolve(new Advertisement(response.data));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.create = function(advertisement) {
            var deferred = $q.defer();
            advertisement = advertisement || {};
            resource.create({}, advertisement, function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.update = function(advertisement) {
            var deferred = $q.defer();
            advertisement = advertisement || {};
            resource.update({
                id: advertisement.getId()
            }, advertisement, function(response) {
                deferred.resolve(new Advertisement(response.data));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.delete = function(id) {
            var deferred = $q.defer();
            resource.delete({
                id: id
            }, function(response) {
                deferred.resolve(response.data);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.updateImage = function(id, params) {
            var deferred = $q.defer();
            var url = ApiUrl.get(`/api/admin/advertisements/${id}/image`);
            params = params || {};
            $http.put(url, params)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.changeStatusAllItem = function(item_ids, status) {
            var deferred = $q.defer();
            var url = '/api/admin/advertisements/change-status-all-items';
            var params = {
                'item_ids': item_ids,
                'status': status
            };
            $http.put(url, params)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };
    }

})(angular.module('app.api.admin.advertisement', []));