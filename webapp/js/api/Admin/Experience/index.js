var Experience = require('../../../models/Experience');
var Pagination = require('../../../models/Pagination');

(function(app) {

    app.service('AdminExperienceService', AdminExperienceService);
    AdminExperienceService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function AdminExperienceService($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/admin/experiences/:id');

        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
            },
            create: {
                method: 'POST',
            },
            update: {
                method: 'PUT',
            }
        });

        this.get = function(params) {
            params = params || {};
            params.page = params.page || 1;
            var deferred = $q.defer();
            resource.get(params, function(response) {
                deferred.resolve({
                    experiences: _.map(response.data, function(item) {
                        return new Experience(item);
                    }),
                    pagination: new Pagination(response.meta.pagination)
                });
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.getAll = function(params) {
            params = params || {};
            var deferred = $q.defer();
            var url = '/api/admin/experiences/all';

            $http.get(url, {
                    params
                })
                .then(function(response) {
                    let experiences = _.map(response.data.data, (item) => {
                        return new Experience(item);
                    });
                    deferred.resolve(experiences);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.show = function(id, include) {
            var deferred = $q.defer();
            var params = {
                id: id
            };
            if (include !== undefined) {
                params.include = include.join();
            }
            resource.get(params, function(response) {
                deferred.resolve(new Experience(response.data));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.create = function(experience) {
            var deferred = $q.defer();
            experience = experience || {};
            resource.create({}, experience, function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.update = function(experience) {
            var deferred = $q.defer();
            experience = experience || {};
            resource.update({
                id: experience.getId()
            }, experience, function(response) {
                deferred.resolve(new Experience(response.data));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.delete = function(id) {
            var deferred = $q.defer();
            resource.delete({
                id: id
            }, function(response) {
                deferred.resolve(response.data);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.changeStatusAllItem = function(item_ids, status) {
            var deferred = $q.defer();
            var url = '/api/admin/experiences/change-status-all-items';
            var params = {
                'item_ids': item_ids,
                'status': status
            };
            $http.put(url, params)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };
    }

})(angular.module('app.api.admin.experience', []));