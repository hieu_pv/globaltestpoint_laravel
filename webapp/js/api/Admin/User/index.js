var Role = require('../../../models/Role');
var User = require('../../../models/User');
var Pagination = require('../../../models/Pagination');
var Availability = require('../../../models/Availability');
var Interviewer = require('../../../models/Interviewer');
var ShortenUrl = require('../../../models/ShortenUrl');
(function(app) {
    app.service('AdminUserService', AdminUserService);

    AdminUserService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function AdminUserService($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/admin/users/:id');
        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
            },
            create: {
                method: 'POST',
            },
            update: {
                method: 'PUT',
            },
            delete: {
                method: 'DELETE',
            }
        });

        this.get = function(params) {
            params = params || {};
            params.page = params.page || 1;
            var deferred = $q.defer();
            resource.get(params, function(response) {
                deferred.resolve({
                    users: _.map(response.data, function(item) {
                        return new User(item);
                    }),
                    pagination: new Pagination(response.meta.pagination)
                });
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.profile = function(params = {}) {
            var url = ApiUrl.get('/api/me');
            var deferred = $q.defer();
            $http({
                    url: url,
                    method: "GET",
                    params: params,
                    cache: false,
                })
                .then(function(response) {
                    if (response.status === 204) {
                        deferred.reject({
                            message: 'Token not provided',
                            error_code: 1023,
                        });
                    } else {
                        var user = new User(response.data.data);
                        deferred.resolve(user);
                    }

                }, function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        this.show = function(id, include) {
            var deferred = $q.defer();
            var params = {
                id: id
            };
            if (include !== undefined) {
                params.include = include.join();
            }
            resource.get(params, function(response) {
                deferred.resolve(new User(response.data));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.create = function(user) {
            var deferred = $q.defer();
            user = user || {};
            resource.create({}, user, function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.update = function(user) {
            var deferred = $q.defer();
            user = user || {};
            if (user instanceof User) {
                resource.update({
                    id: user.getId()
                }, user, function(response) {
                    var user = new User(response.data);
                    deferred.resolve(user);
                }, function(error) {
                    deferred.reject(error.data);
                });
                return deferred.promise;
            } else {
                deferred.reject('user must be instance of \models\User');
            }
        };

        this.delete = function(id) {
            var deferred = $q.defer();
            var url = ApiUrl.get("/api/admin/users/" + id);
            $http.delete(url)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.deleteCompany = function(id) {
            var deferred = $q.defer();
            var url = ApiUrl.get(`/api/admin/users/${id}/company`);
            $http.delete(url)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.update_employer = function(id, user) {
            var deferred = $q.defer();
            var url = '/api/admin/users/' + id + '/employer';
            user = user || {};
            $http.put(url, user)
                .then(function(response) {
                    deferred.resolve(new User(response.data.data));
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.createEmployerProfile = function(user) {
            user = user || {};
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/admin/users/employer');
            $http.post(url, user)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.getAllEmployer = function(params) {
            params = params || {};
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/admin/users/employers');
            $http({
                url: url,
                method: "GET",
                params: params
            }).then(function(response) {
                var users = _.map(response.data.data, function(item) {
                    return new User(item);
                });
                deferred.resolve(users);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.profileWithToken = function(token, include) {
            var url = ApiUrl.get('/api/me/token');
            var deferred = $q.defer();
            var params = {
                token: token
            };
            if (include !== undefined && include instanceof Array) {
                params.include = include.join(',');
            }
            $http({
                    url: url,
                    method: "GET",
                    params: params
                })
                .then(function(response) {
                    var user = new User(response.data.data);
                    deferred.resolve(user);
                }, function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        this.updateImage = function(id, params) {
            var deferred = $q.defer();
            var url = ApiUrl.get(`/api/admin/users/${id}/images`);
            params = params || {};
            $http.put(url, params)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.getStatus = function(params) {
            var deferred = $q.defer();
            var url = '/api/admin/user-status';
            url = ApiUrl.get(url);
            $http.get(url, params, {
                    cache: true
                })
                .then(function(response) {
                    deferred.resolve(response.data.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.employerChangeStatus = function(user_id, params) {
            var deferred = $q.defer();
            var url = '/api/admin/users/' + user_id + '/change-status/employer';
            params = params || {};
            $http.put(url, params)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };
    }

})(angular.module('app.api.admin.user', []));
