var Qualification = require('../../../models/Qualification');
var Pagination = require('../../../models/Pagination');

(function(app) {

    app.service('AdminQualificationService', AdminQualificationService);
    AdminQualificationService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function AdminQualificationService($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/admin/qualifications/:id');

        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
            },
            create: {
                method: 'POST',
            },
            update: {
                method: 'PUT',
            }
        });

        this.get = function(params) {
            params = params || {};
            params.page = params.page || 1;
            var deferred = $q.defer();
            resource.get(params, function(response) {
                deferred.resolve({
                    qualifications: _.map(response.data, function(item) {
                        return new Qualification(item);
                    }),
                    pagination: new Pagination(response.meta.pagination)
                });
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.getAll = function(params) {
            params = params || {};
            var deferred = $q.defer();
            var url = '/api/admin/qualifications/all';

            $http.get(url, {
                    params
                })
                .then(function(response) {
                    let qualifications = _.map(response.data.data, (item) => {
                        return new Qualification(item);
                    });
                    deferred.resolve(qualifications);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.show = function(id, include) {
            var deferred = $q.defer();
            var params = {
                id: id
            };
            if (include !== undefined) {
                params.include = include.join();
            }
            resource.get(params, function(response) {
                deferred.resolve(new Qualification(response.data));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.create = function(qualification) {
            var deferred = $q.defer();
            qualification = qualification || {};
            resource.create({}, qualification, function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.update = function(qualification) {
            var deferred = $q.defer();
            qualification = qualification || {};
            resource.update({
                id: qualification.getId()
            }, qualification, function(response) {
                deferred.resolve(new Qualification(response.data));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.delete = function(id) {
            var deferred = $q.defer();
            resource.delete({
                id: id
            }, function(response) {
                deferred.resolve(response.data);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.changeStatusAllItem = function(item_ids, status) {
            var deferred = $q.defer();
            var url = '/api/admin/qualifications/change-status-all-items';
            var params = {
                'item_ids': item_ids,
                'status': status
            };
            $http.put(url, params)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };
    }

})(angular.module('app.api.admin.qualification', []));