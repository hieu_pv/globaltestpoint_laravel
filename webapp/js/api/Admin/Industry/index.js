var Industry = require('../../../models/Industry');
var Pagination = require('../../../models/Pagination');

(function(app) {

    app.service('AdminIndustryService', AdminIndustryService);
    AdminIndustryService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function AdminIndustryService($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/admin/industries/:id');

        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
            },
            create: {
                method: 'POST',
            },
            update: {
                method: 'PUT',
            }
        });

        this.get = function(params) {
            params = params || {};
            params.page = params.page || 1;
            var deferred = $q.defer();
            resource.get(params, function(response) {
                deferred.resolve({
                    industries: _.map(response.data, function(item) {
                        return new Industry(item);
                    }),
                    pagination: new Pagination(response.meta.pagination)
                });
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.getAll = function(params) {
            params = params || {};
            var deferred = $q.defer();
            var url = '/api/admin/industries/all';

            $http.get(url, {
                    params
                })
                .then(function(response) {
                    let industries = _.map(response.data.data, (item) => {
                        return new Industry(item);
                    });
                    deferred.resolve(industries);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.list = function(params) {
            params = params || {};
            var deferred = $q.defer();
            resource.get(params, function(response) {
                deferred.resolve(_.map(response.data, function(item) {
                    return new Industry(item);
                }));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.show = function(id, include) {
            var deferred = $q.defer();
            var params = {
                id: id
            };
            if (include !== undefined) {
                params.include = include.join();
            }
            resource.get(params, function(response) {
                deferred.resolve(new Industry(response.data));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.create = function(industry) {
            var deferred = $q.defer();
            industry = industry || {};
            resource.create({}, industry, function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.update = function(industry) {
            var deferred = $q.defer();
            industry = industry || {};
            resource.update({
                id: industry.getId()
            }, industry, function(response) {
                deferred.resolve(new Industry(response.data));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.delete = function(id) {
            var deferred = $q.defer();
            resource.delete({
                id: id
            }, function(response) {
                deferred.resolve(response.data);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.changeStatusAllItem = function(item_ids, status) {
            var deferred = $q.defer();
            var url = '/api/admin/industries/change-status-all-items';
            var params = {
                'item_ids': item_ids,
                'status': status
            };
            $http.put(url, params)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };
    }

})(angular.module('app.api.admin.industry', []));