var Country = require('../../../models/Country');
var State = require('../../../models/State');
var Pagination = require('../../../models/Pagination');

(function(app) {

    app.service('AdminCountryService', AdminCountryService);
    AdminCountryService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function AdminCountryService($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/admin/countries/:id');

        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
            },
            create: {
                method: 'POST',
            },
            update: {
                method: 'PUT',
            }
        });

        this.getAll = function(params) {
            params = params || {};
            var deferred = $q.defer();
            var url = '/api/admin/countries/all';

            $http.get(url, {
                    params
                })
                .then(function(response) {
                    let countries = _.map(response.data.data, (item) => {
                        return new Country(item);
                    });
                    deferred.resolve(countries);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.getStates = function(id, params) {
            params = params || {};
            var deferred = $q.defer();
            var url = `/api/admin/countries/${id}/states`;

            $http.get(url, {
                    params
                })
                .then(function(response) {
                    let states = _.map(response.data.data, (item) => {
                        return new State(item);
                    });
                    deferred.resolve(states);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };
    }

})(angular.module('app.api.admin.country', []));