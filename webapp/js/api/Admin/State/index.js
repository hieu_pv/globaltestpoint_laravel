var State = require('../../../models/State');
var City = require('../../../models/City');
var Pagination = require('../../../models/Pagination');

(function(app) {

    app.service('AdminStateService', AdminStateService);
    AdminStateService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function AdminStateService($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/admin/states/:id');

        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
            },
            create: {
                method: 'POST',
            },
            update: {
                method: 'PUT',
            }
        });

        this.get = function(params) {
            params = params || {};
            params.page = params.page || 1;
            var deferred = $q.defer();
            resource.get(params, function(response) {
                deferred.resolve({
                    states: _.map(response.data, function(item) {
                        return new State(item);
                    }),
                    pagination: new Pagination(response.meta.pagination)
                });
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.getAll = function(params) {
            params = params || {};
            var deferred = $q.defer();
            let url = ApiUrl.get('/api/admin/states/all');
            $http.get(url, {
                    params: params
                })
                .then(function(response) {
                    var states = _.map(response.data.data, function(item) {
                        return new State(item);
                    });
                    deferred.resolve(states);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.show = function(id, include) {
            var deferred = $q.defer();
            var params = {
                id: id
            };
            if (include !== undefined) {
                params.include = include.join();
            }
            resource.get(params, function(response) {
                deferred.resolve(new State(response.data));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.create = function(state) {
            var deferred = $q.defer();
            state = state || {};
            resource.create({}, state, function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.update = function(state) {
            var deferred = $q.defer();
            state = state || {};
            resource.update({
                id: state.getId()
            }, state, function(response) {
                deferred.resolve(new JobTitle(response.data));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.delete = function(id) {
            var deferred = $q.defer();
            resource.delete({
                id: id
            }, function(response) {
                deferred.resolve(response.data);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.changeStatusAllItem = function(item_ids, status) {
            var deferred = $q.defer();
            var url = '/api/admin/states/change-status-all-items';
            var params = {
                'item_ids': item_ids,
                'status': status
            };
            $http.put(url, params)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.getCities = function(id, params) {
            params = params || {};
            var deferred = $q.defer();
            var url = `/api/admin/states/${id}/cities`;

            $http.get(url, {
                    params
                })
                .then(function(response) {
                    let cities = _.map(response.data.data, (item) => {
                        return new City(item);
                    });
                    deferred.resolve(cities);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };
    }

})(angular.module('app.api.admin.state', []));