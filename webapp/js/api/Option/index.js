var Option = require('../../models/Option');

(function(app) {

    app.service('OptionService', OptionService);
    OptionService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function OptionService($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/options/:id');
        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
            }
        });

        this.getLinkFanpages = function(params) {
            params = params || {};
            var deferred = $q.defer();
            let url = ApiUrl.get('/api/options/link-fanpages');
            $http.get(url, {
                    params: params
                })
                .then(function(response) {
                    deferred.resolve(new Option(response.data.data));
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };
    }

})(angular.module('app.api.option', []));