var Advertisement = require('../../models/Advertisement');
var Pagination = require('../../models/Pagination');

(function(app) {

    app.service('AdvertisementsService', AdvertisementsService);
    AdvertisementsService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function AdvertisementsService($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/advertisements/:id');
        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
            }
        });

        this.get = function(params) {
            params = params || {};
            params.page = params.page || 1;
            var deferred = $q.defer();
            resource.get(params, function(response) {
                deferred.resolve({
                    advertisements: _.map(response.data, function(item) {
                        return new Advertisement(item);
                    }),
                    pagination: new Pagination(response.meta.pagination)
                });
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.getAll = function(params) {
            params = params || {};
            var deferred = $q.defer();
            var url = '/api/advertisements/all';

            $http.get(url, {
                    params
                })
                .then(function(response) {
                    let advertisements = _.map(response.data.data, (item) => {
                        return new Advertisement(item);
                    });
                    deferred.resolve(advertisements);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };
    }

})(angular.module('app.api.advertisements', []));