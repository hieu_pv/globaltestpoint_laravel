var JobTitle = require('../../models/JobTitle');

(function(app) {

    app.service('JobTitlesService', JobTitlesService);
    JobTitlesService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function JobTitlesService($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/job_titles/:id');
        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
            }
        });

        this.get = function(params) {
            params = params || {};
            var deferred = $q.defer();
            resource.get(params, function(response) {
                var job_titles = _.map(response.data, function(item) {
                    return new JobTitle(item);
                });
                deferred.resolve(job_titles);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };
    }

})(angular.module('app.api.job_titles', []));
