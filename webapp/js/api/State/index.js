var State = require('../../models/State');
var Pagination = require('../../models/Pagination');

(function(app) {

    app.service('StateService', StateService);
    StateService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function StateService($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/states/:id');

        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
            },
            create: {
                method: 'POST',
            },
            update: {
                method: 'PUT',
            }
        });

        this.get = function(params) {
            params = params || {};
            var deferred = $q.defer();
            resource.get(params, function(response) {
                deferred.resolve(response.data.map(item => new State(item)));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };
    }

})(angular.module('app.api.state', []));