var Tag = require('../../models/Tag');
(function(app) {
    app.service('TagService', TagService);

    TagService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function TagService($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/tags/:id');
        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
                cache: true,
            },
            create: {
                method: 'POST',
            },
            update: {
                method: 'PUT',
            }
        });

        this.get = function(params) {
            params = params || {};
            var deferred = $q.defer();
            resource.get(params, function(response) {
                deferred.resolve(response.data.map(item => new Tag(item)));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };
    }

})(angular.module('app.api.tag', []));
