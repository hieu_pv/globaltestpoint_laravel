var Industry = require('../../models/Industry');

(function(app) {

    app.service('IndustriesService', IndustriesService);
    IndustriesService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function IndustriesService($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/industries/:id');
        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
            }
        });

        this.get = function(params) {
            params = params || {};
            var deferred = $q.defer();
            resource.get(params, function(response) {
                var industries = _.map(response.data, function(item) {
                    return new Industry(item);
                });
                deferred.resolve(industries);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };
    }

})(angular.module('app.api.industries', []));
