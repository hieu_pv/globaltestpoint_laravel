var User = require('../../models/User');
var UserApplication = require('../../models/UserApplication');
var Job = require('../../models/Job');
var Shift = require('../../models/Shift');
var Availability = require('../../models/Availability');
var Pagination = require('../../models/Pagination');
var Application = require('../../models/Application');
var ShortenUrl = require('../../models/ShortenUrl');
var FavoriteCandidate = require('../../models/FavoriteCandidate');
(function(app) {
    app.service('UserService', UserService);

    UserService.$inject = ['$resource', '$http', '$q', '$cookies', 'ApiUrl'];

    function UserService($resource, $http, $q, $cookies, ApiUrl) {
        var url = ApiUrl.get('/api/users/:id');
        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
            },
            create: {
                method: 'POST',
            },
            update: {
                method: 'PUT',
            }
        });

        this.profile = function(params = {}) {
            var url = ApiUrl.get('/api/me');
            var deferred = $q.defer();
            $http({
                    url: url,
                    method: "GET",
                    params: params,
                    cache: false,
                })
                .then(function(response) {
                    if (response.status === 204) {
                        deferred.reject({
                            message: 'Token not provided',
                            error_code: 1023,
                        });
                    } else {
                        var user = new User(response.data.data);
                        deferred.resolve(user);
                    }

                }, function(error) {
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        this.get = function(params) {
            var deferred = $q.defer();
            params = params || {};
            resource.get(params, function(response) {
                deferred.resolve(_.map(response.data, item => new User(item)));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.getUsers = function(params) {
            var deferred = $q.defer();
            params = params || {};
            let url = ApiUrl.get('/api/list-users');
            $http.post(url, params)
                .then(function(response) {
                    deferred.resolve(_.map(response.data.data, item => new User(item)));
                }, function(error) {
                    deferred.reject(error.data.data);
                });
            return deferred.promise;
        };

        this.show = function(id, params) {
            var deferred = $q.defer();
            params = params || {};
            resource.get({
                id: id
            }, params, function(response) {
                deferred.resolve(new User(response.data));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };
        this.availabilities = function(id, params) {
            var deferred = $q.defer();
            params = params || {};
            var url = ApiUrl.get(`/api/users/${id}/availabilities`);
            $http.get(url, {
                    params: params
                })
                .then(function(response) {
                    deferred.resolve(response.data.data.map(item => new Availability(item)));
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.create = function(user) {
            var deferred = $q.defer();
            user = user || {};
            resource.create({}, user, function(response) {
                deferred.resolve(response);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.createEmployerProfile = function(user) {
            user = user || {};
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/users/employer');
            $http.post(url, user)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.updateImage = function(id, params) {
            var deferred = $q.defer();
            var url = ApiUrl.get(`/api/users/${id}/images`);
            params = params || {};
            $http.put(url, params)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.updateAvailabilities = function(id, params) {
            var deferred = $q.defer();
            var url = ApiUrl.get(`/api/users/${id}/availabilities`);
            params = params || {};
            $http.put(url, params)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.updateRole = function(id, params) {
            var deferred = $q.defer();
            params = params || {};
            var url = ApiUrl.get("/api/users/" + id + "/role");
            $http.put(url, params)
                .then(function(response) {
                    deferred.resolve(new User(response.data.data));
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.forgot_password = function(email) {
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/password/email');
            $http.post(url, {
                    email: email
                })
                .then(function(response) {
                    deferred.resolve();
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.reset_password = function(data) {
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/password/reset');
            $http.post(url, data)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.change_password = function(params) {
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/password/change-password');
            params = params || {};
            $http.put(url, params)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.check_reset_password_token = function(params = {}) {
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/password/check-reset-password-token');
            $http.get(url, {
                    params: params
                })
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.verify_email = function(user_id, token) {
            var deferred = $q.defer();
            if (user_id !== undefined && user_id !== '' && token !== undefined && token !== '') {
                var url = '/api/users/' + user_id + '/verify-email';
                var params = {
                    token: token
                };
                url = ApiUrl.get(url);
                $http.put(url, params)
                    .then(function(response) {
                        deferred.resolve(new User(response.data.data));
                    }, function(error) {
                        deferred.reject(error.data);
                    });
                return deferred.promise;
            } else {
                deferred.reject("id or token doesn't exist");
            }
        };

        this.resendVerifyEmail = function(user_id) {
            var deferred = $q.defer();
            var url = `/api/users/${user_id}/resend-verify-email`;
            url = ApiUrl.get(url);
            $http.put(url)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.verifyPhoneNumber = function(user_id, params) {
            var deferred = $q.defer();
            params = params || {};
            var url = ApiUrl.get('/api/users/' + user_id + '/verify-phone-number');
            $http.put(url, params)
                .then(function(response) {
                    deferred.resolve(new User(response.data.data));
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.last_position = function(id) {
            var deferred = $q.defer();
            var url = '/api/users/' + id + '/last-positions';
            url = ApiUrl.get(url);
            $http.get(url)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.add_last_position = function(user_id, params) {
            var deferred = $q.defer();
            var url = '/api/users/' + user_id + '/last-positions';
            url = ApiUrl.get(url);
            $http.post(url, params)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.update_last_position = function(user_id, id, params) {
            var deferred = $q.defer();
            var url = '/api/users/' + user_id + '/last-positions/' + id;
            url = ApiUrl.get(url);
            $http.put(url, params)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.delete_last_position = function(user_id, id) {
            var deferred = $q.defer();
            var url = '/api/users/' + user_id + '/last-positions/' + id;
            url = ApiUrl.get(url);
            $http.delete(url)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.update_employee = function(id, data) {
            var deferred = $q.defer();
            var url = '/api/users/' + id + '/employee?include=roles,employee';
            data = data || {};
            $http.put(url, data)
                .then(function(response) {
                    deferred.resolve(new User(response.data.data));
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.update_employer = function(id, user) {
            var deferred = $q.defer();
            if ($cookies.get(ADMIN_SESSION)) {
                var url = '/api/users/' + id + '/admin-employer';
            } else {
                var url = '/api/users/' + id + '/employer';
            }
            user = user || {};
            $http.put(url, user)
                .then(function(response) {
                    deferred.resolve(new User(response.data.data));
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.isOwner = function(user_id, job_id) {
            var deferred = $q.defer();
            var url = '/api/users/' + user_id + '/is-owner-of/' + job_id;
            $http.get(url)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.allShiftOfEmployee = function(user_id) {
            var deferred = $q.defer();
            var url = '/api/users/' + user_id + '/all-shift-of-employee';
            $http.get(url)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };


        this.applications = function(params = {}) {
            var deferred = $q.defer();
            var url = '/api/applications';
            $http.get(url, {
                    params: params
                })
                .then(function(response) {
                    deferred.resolve({
                        applications: _.map(response.data.data, (item) => new Application(item)),
                        pagination: new Pagination(response.data.meta.pagination),
                    });
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.updateTags = function(user_id, tags) {
            var deferred = $q.defer();
            var params = {
                tags: tags
            };
            var url = '/api/users/' + user_id + '/tags';
            $http.put(url, params)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.employeeChangeApplicationStatus = function(params = {}) {
            var deferred = $q.defer();
            if ($cookies.get(ADMIN_SESSION)) {
                var url = '/api/admin-update-applications';
            } else {
                var url = '/api/applications';
            }
            $http.put(url, params)
                .then(function(response) {
                    return deferred.resolve(response.data);
                }, function(error) {
                    return deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.sendOTP = function(user_id, params) {
            var deferred = $q.defer();
            params = params || {};
            var url = `/api/users/${user_id}/otp`;
            $http.post(url, params)
                .then(function(response) {
                    return deferred.resolve(response.data);
                }, function(error) {
                    return deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.isVerifiedEmail = function(user_id) {
            var deferred = $q.defer();
            var url = `/api/users/${user_id}/is-verified-email`;
            $http.get(url)
                .then(function(response) {
                    return deferred.resolve(response.data);
                }, function(error) {
                    return deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.getShortenReferralUrl = function(user_id, params = {}) {
            var deferred = $q.defer();
            var url = '/api/urls';
            $http.get(url, {
                    params: params
                })
                .then(function(response) {
                    return deferred.resolve(new ShortenUrl(response.data.data));
                }, function(error) {
                    return deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.employerIsHiredEmployee = function(employee_id, params = {}) {
            var deferred = $q.defer();
            var url = `/api/employer/${employee_id}/is-hired`;
            $http.get(url, {
                    params: params
                })
                .then(function(response) {
                    return deferred.resolve(response.data.data);
                }, function(error) {
                    return deferred.reject(error.data);
                });
            return deferred.promise;
        };


        this.getServiceFee = function(params) {
            var deferred = $q.defer();
            params = params || {};
            var url = '/api/service-fee';
            $http.get(url, {
                    params: params
                })
                .then(function(response) {
                    return deferred.resolve(response.data.data);
                }, function(error) {
                    return deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.updateAccountSettings = function(id, params) {
            var deferred = $q.defer();
            params = params || {};
            var url = `/api/account-settings`;
            $http.put(url, params)
                .then(function(response) {
                    return deferred.resolve(response.data.data);
                }, function(error) {
                    return deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.getFavoritedCandidate = function(params) {
            var deferred = $q.defer();
            params = params || {};
            var url = '/api/favorited-candidates';
            $http.get(url, {
                    params: params
                })
                .then(function(response) {
                    var users = _.map(response.data.data, function(item) {
                        return new FavoriteCandidate(item);
                    });
                    var data = {
                        users: users,
                    };
                    if (!_.isNil(response.data.meta) && !_.isNil(response.data.meta.pagination)) {
                        data.pagination = new Pagination(response.data.meta.pagination);
                    }
                    return deferred.resolve(data);
                }, function(error) {
                    return deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.favoriteCandidate = function(params) {
            var deferred = $q.defer();
            params = params || {};
            var url = `/api/users/favorite-candidate`;
            $http.post(url, params)
                .then(function(response) {
                    return deferred.resolve(new FavoriteCandidate(response.data.data));
                }, function(error) {
                    return deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.removeFavoriteCandidate = function(employee_id) {
            var deferred = $q.defer();
            var url = `/api/users/favorite-candidate/${employee_id}`;
            $http.delete(url)
                .then(function(response) {
                    return deferred.resolve(response.data);
                }, function(error) {
                    return deferred.reject(error);
                });
            return deferred.promise;
        };

        this.logoutAsUser = function() {
            var deferred = $q.defer();
            var url = '/api/admin/logout-as-user';
            $http.delete(url)
                .then(function(response) {
                    return deferred.resolve(response.data.data);
                }, function(error) {
                    return deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.unsubscribe = function(params) {
            var deferred = $q.defer();
            var url = '/api/unsubscribe';
            $http.put(url, params)
                .then(function(response) {
                    return deferred.resolve(response.data.data);
                }, function(error) {
                    return deferred.reject(error.data);
                });
            return deferred.promise;
        };
    }

})(angular.module('app.api.user', []));
