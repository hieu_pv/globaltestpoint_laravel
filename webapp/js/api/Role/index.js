var Role = require('../../models/Role');

(function(app) {

    app.service('RoleService', RoleService);
    RoleService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function RoleService($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/roles/:id');
        var resource = $resource(url, {}, {
            post: {
                method: 'POST',
            }
        });

        this.get = function() {
            var deferred = $q.defer();
            resource.post({}, function(response) {
                var roles = _.map(response.data, function(item) {
                    return new Role(item);
                });
                deferred.resolve(roles);
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };
    }

})(angular.module('app.api.role', []));
