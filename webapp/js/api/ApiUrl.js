(function(app) {
    app.factory('ApiUrl', function() {
        var service = {
            get: get,
        };

        function get(url) {
            if (BASE_URL.substr(BASE_URL.length - 1) == '/') {
                return BASE_URL.substr(0, BASE_URL.length - 1) + url;
            } else {
                return BASE_URL + url;
            }
        }
        return service;
    });
})(angular.module('app.api.url', []));
