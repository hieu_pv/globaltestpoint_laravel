var City = require('../../models/City');
var Pagination = require('../../models/Pagination');

(function(app) {

    app.service('CityService', CityService);
    CityService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function CityService($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/cities/:id');

        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
            },
            create: {
                method: 'POST',
            },
            update: {
                method: 'PUT',
            }
        });

        this.get = function(params) {
            params = params || {};
            var deferred = $q.defer();
            resource.get(params, function(response) {
                deferred.resolve(response.data.map(item => new City(item)));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };

        this.createNewCityInstance = function(options) {
            return new City(options);
        };
    }

})(angular.module('app.api.city', []));