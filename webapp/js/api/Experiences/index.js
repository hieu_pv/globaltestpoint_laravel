var Experience = require('../../models/Experience');

(function(app) {

    app.service('ExperiencesService', ExperiencesService);
    ExperiencesService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function ExperiencesService($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/experiences/:id');
        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
            }
        });

        this.get = function(params) {
            params = params || {};
            var deferred = $q.defer();
            resource.get(params, function(response) {
                var experiences = _.map(response.data, function(item) {
                    return new Experience(item);
                });
                deferred.resolve(experiences);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };

        this.createNewExperienceInstance = function(options) {
            return new Experience(options);
        };
    }

})(angular.module('app.api.experiences', []));
