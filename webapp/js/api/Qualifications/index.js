var Qualification = require('../../models/Qualification');

(function(app) {

    app.service('QualificationsService', QualificationsService);
    QualificationsService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function QualificationsService($resource, $http, $q, ApiUrl) {
        var url = ApiUrl.get('/api/qualifications/:id');
        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
            }
        });

        this.get = function(params) {
            params = params || {};
            var deferred = $q.defer();
            resource.get(params, function(response) {
                var qualifications = _.map(response.data, function(item) {
                    return new Qualification(item);
                });
                deferred.resolve(qualifications);
            }, function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        };
    }

})(angular.module('app.api.qualifications', []));
