(function(app) {
    app.service('AuthService', AuthService);
    AuthService.$inject = ['$http', '$q', 'ApiUrl'];

    function AuthService($http, $q, ApiUrl) {
        this.login = function(email, password, remember) {
            var url = ApiUrl.get('/api/login');
            var data = {
                email: email,
                password: password,
            };
            var deferred = $q.defer();
            $http.post(url, data)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.social_login = function(params) {
            params = params || {};
            var url = ApiUrl.get('/api/social-login');
            var deferred = $q.defer();
            $http.post(url, params)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.getLinkedInAccessToken = function(params) {
            params = params || {};
            var url = ApiUrl.get("/auth/linkedin/access-token?redirect_uri=" + params.redirect_uri + "&state=" + params.state + "&code=" + params.code + "&client_id=" + params.client_id);
            var deferred = $q.defer();
            $http.post(url, params)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.refreshToken = function() {
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/auth/refresh-token');
            $http.put(url)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.logout = function() {
            var deferred = $q.defer();
            var url = ApiUrl.get('/api/auth/logout');
            $http.delete(url)
                .then(function(response) {
                    deferred.resolve();
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };

        this.has_logged_in = function() {
            var url = '/api/has-logged-in';

            var deferred = $q.defer();
            $http.post(url)
                .then(function(response) {
                    deferred.resolve(response.data);
                }, function(error) {
                    deferred.reject(error.data);
                });
            return deferred.promise;
        };
    }
})(angular.module('app.api.auth', []));