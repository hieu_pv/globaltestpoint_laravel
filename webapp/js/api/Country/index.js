var Country = require('../../models/Country');
(function(app) {
    app.service('CountryService', CountryService);

    CountryService.$inject = ['$resource', '$http', '$q', 'ApiUrl'];

    function CountryService($resource, $http, $q, ApiUrl, UshiftApp) {
        var url = ApiUrl.get('/api/countries/:id');
        var resource = $resource(url, {}, {
            get: {
                method: 'GET',
                cache: true,
            },
            create: {
                method: 'POST',
            },
            update: {
                method: 'PUT',
            }
        });

        this.get = function(params = {}) {
            var deferred = $q.defer();
            resource.get(params, function(response) {
                deferred.resolve(response.data.map(item => new Country(item)));
            }, function(error) {
                deferred.reject(error.data);
            });
            return deferred.promise;
        };
    }

})(angular.module('app.api.country', []));
