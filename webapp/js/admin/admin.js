require("babel-polyfill");
global.jQuery = global.$ = require('jquery');
global.moment = require('moment');
require('bootstrap');
require('moment-timezone');
require('twix');
require('angular');
global._ = require('lodash');
require('picker');
require('picker.date');
require('picker.time');
require('angular-resource');
require('pusher');
require('pusher-angular');
require('angular-aria');
require('angular-filter');
require('angular-animate');
require('angular-sanitize');
require('angular-cookies');
require('angular-ui-mask');
require('ngPickadate');
require('select2');
require('pdfjs-dist');
require('nf-pdf');
require('bootstrap-select');
require('angular-ui-select2');
require('angular-ui-select');
require('angular-input-star');
require('angular-bootstrap');
require('./config');
require('./routes');
require('./templates');
require('./components/');
require('./store/');
require('../api/');
require('./common/providers/');
require('./common/services/');
require('./common/filters/');
require('./common/directives/');
require('./common/exception');

let {
    createStore,
    combineReducers
} = require('redux');

(function(app) {
    app.constant('APP_CONSTANTS', {
        TAG_TYPE_COMPANY: 2,
        JOB_TYPE_SHIFT: 1,
        JOB_TYPE_PART_TIME: 2,
        JOB_PAYMENT_TYPE_CASH: 1,
        JOB_PAYMENT_TYPE_ONLINE: 2,
        PAYMENT_STATUS_OTHER: 0,
        PAYMENT_STATUS_PENDING: 1,
        PAYMENT_STATUS_PROCESSING: 2,
        PAYMENT_STATUS_DONE: 3,
        EMPLOYER_DOESNT_PAY: 4,
        PAYMENT_STATUS_ARRIVING: 5,
    });
    app.run([
        '$rootScope',
        '$state',
        '$cookieStore',
        '$cookies',
        '$window',
        'API',
        'Notification',
        'UshiftApp',
        'APP_CONSTANTS',
        'store',
        function($rootScope, $state, $cookieStore, $cookies, $window, API, Notification, UshiftApp, APP_CONSTANTS, store) {
            _.forEach(UshiftApp.timezones, (i) => {
                moment.tz.add(i);
            });

            $rootScope.$state = $state;

            $rootScope.APP_CONSTANTS = APP_CONSTANTS;


            /*
             * Global asset function
             *
             * @return absolute path of a resource
             */
            $rootScope.asset = function(path) {
                if (path.charAt(0) == '/') {
                    return BASE_URL + path.substr(1);
                } else {
                    return BASE_URL + path;
                }
            };

            /*
             * Global function to logout
             */
            $rootScope.logout = function(callback) {
                API.auth.logout()
                    .then(function() {
                        $cookies.remove(JWT_TOKEN_COOKIE_KEY, {
                            path: '/'
                        });
                        $rootScope.isLoggedIn = false;
                        $state.go('user.login');
                    })
                    .catch(function(error) {
                        throw error;
                    });
            };

            /*
             * scroll to top every time $state was changed
             */
            $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
                $rootScope.toState = toState;
                $rootScope.toParams = toParams;
                $rootScope.fromState = fromState;
                $rootScope.fromParams = fromParams;
            });
            $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
                if ($('main').find('#expand-data').length) {
                    $('#expand-data').remove();
                }
                $rootScope.scrollTop();
            });

            $rootScope.scrollTop = function() {
                $("html, body").animate({
                    scrollTop: 0
                }, 500);
            };

            $rootScope.isValidFormOrScrollToError = function(form) {
                var errors = form.$error;
                if (_.isEmpty(errors)) {
                    return true;
                } else {
                    var top;
                    var input_names = [];
                    for (var key in errors) {
                        input_names = _.concat(input_names, _.map(errors[key], (item) => item.$name));
                    }
                    _.forEach(input_names, (item) => {
                        var t = $(`[name=${item}]`).offset().top;
                        if (_.isUndefined(top)) {
                            top = t;
                        } else {
                            if (t < top) {
                                top = t;
                            }
                        }
                        $("html, body").animate({
                            scrollTop: top - 90
                        }, 500);
                    });
                    return false;
                }
            };
        }
    ]);
    app.controller('AdminMainController', AdminMainController);
    AdminMainController.$inject = ['$rootScope', '$scope', '$state', '$pusher', 'API', 'Notification', 'user', '$cookieStore', '$cookies', 'APP_CONSTANTS'];

    function AdminMainController($rootScope, $scope, $state, $pusher, API, Notification, user, $cookieStore, $cookies, APP_CONSTANTS) {
        var vm = this;
        vm.user = user;

        $rootScope.title = 'Globaltestpoint Admin';

        if (!_.isNil($cookieStore.get('EXPAND_SIDEBAR'))) {
            vm.expandSidebar = $cookieStore.get('EXPAND_SIDEBAR');
        } else {
            vm.expandSidebar = true;
        }

        var originatorEv;

        $rootScope.openMenu = openMenu;
        $rootScope.coming = coming;
        vm.hasSubmenu = hasSubmenu;

        $scope.$watch('vm.expandSidebar', function(expandSidebar) {
            if (!_.isUndefined(expandSidebar)) {
                if (!_.isNil($cookieStore.get('EXPAND_SIDEBAR'))) {
                    $cookies.remove('EXPAND_SIDEBAR', {
                        path: '/'
                    });
                }
                $cookieStore.put('EXPAND_SIDEBAR', expandSidebar);
            }
        });

        vm.adminSidebarMenus = [{
            menu: 'Jobs',
            state: 'app.jobs.all',
            permission: '',
            icon: 'fa fa-suitcase',
            submenu: []
        }, {
            menu: 'Companies',
            state: 'app.companies.all',
            permission: '',
            icon: 'fa fa-building',
            submenu: []
        }, {
            menu: 'Industries',
            state: 'app.industries.list',
            permission: '',
            icon: 'fa fa-industry',
            submenu: []
        }, {
            menu: 'Nationalities',
            state: 'app.nationality.list',
            permission: '',
            icon: 'fa fa-globe',
            submenu: []
        }, {
            menu: 'Cities',
            state: 'app.city.list',
            permission: '',
            icon: 'fa fa-user-secret',
            submenu: []
        }, {
            menu: 'Experiences',
            state: 'app.experience.list',
            permission: '',
            icon: 'fa fa-cube',
            submenu: []
        }, {
            menu: 'Qualifications',
            state: 'app.qualification.list',
            permission: '',
            icon: 'fa fa-graduation-cap',
            submenu: []
        }, {
            menu: 'Job Titles',
            state: 'app.job_titles.list',
            permission: '',
            icon: 'fa fa-book',
            submenu: []
        }, {
            menu: 'Tags',
            state: 'app.tag.list',
            permission: '',
            icon: 'fa fa-tags',
            submenu: []
        }, {
            menu: 'Advertisements',
            state: 'app.advertisements.list',
            permission: '',
            icon: 'fa fa-tripadvisor',
            submenu: []
        }, {
            menu: 'Go to Admin page',
            state: null,
            custom_url: '/admin/',
            permission: '',
            icon: 'fa fa-backward',
            submenu: []
        }];

        function hasSubmenu(item) {
            return _.isArray(item.submenu) && item.submenu.length > 0;
        }

        function coming() {
            Notification.show('warning', 'Feature coming soon', 3000);
        }

        function openMenu($mdOpenMenu, ev) {
            originatorEv = ev;
            $mdOpenMenu(ev);
        }

    }
    app.directive('collapsible', function() {
        return {
            restrict: 'C',
            link: function(scope, element) {
                setTimeout(function() {
                    if ($(element).find('.collapsible-body').find('ul').find('.active').length) {
                        $(element).find('.collapsible-header').addClass('active');
                        $(element).parent().addClass('active');
                        $(element).collapsible({
                            accordion: false
                        });
                    }
                }, 500);
                $(element).collapsible();
            }
        };
    });
})(angular.module('app', [
    'app.common.providers',
    'app.config',
    'app.router',
    'ngResource',
    'pusher-angular',
    'ngSanitize',
    'ngCookies',
    'angular.filter',
    'ngAnimate',
    'ui.mask',
    'nfpdf',
    'pickadate',
    'ui.select2',
    'ui.select',
    'angular-input-stars',
    'ui.bootstrap',
    'app.template',
    'app.store',
    'app.components',
    'app.common.directives',
    'app.api',
    'app.common.services',
    'app.common.exceptionHandler',
    'app.common.filters'
]));
