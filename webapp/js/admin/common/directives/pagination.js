(function(app) {

    app.directive('pagination', ['$rootScope', '$state', function($rootScope, $state) {
        return {
            restrict: 'E',
            templateUrl: '/webapp/js/admin/common/partials/_pagination.tpl.html',
            scope: {
                data: '='
            },
            link: function(scope, elememt, attrs) {
                scope.state = $state;

                scope.goto = function(page) {
                    if (parseInt($state.params.page) === parseInt(page)) {
                        return false;
                    }
                    var params = $state.params;
                    params.page = page;
                    $state.go($state.current.name, params, {
                        reload: true
                    });
                };
                scope.$watch('data', function(data) {
                    if (!_.isNil(data)) {
                        var skip_page = 5;
                        scope.pages = [1];
                        var i = 1;
                        for (i; i <= data.total_pages; i++) {
                            if ((Math.abs(i - data.current_page) < skip_page || i % skip_page === 0) && scope.pages.indexOf(i) === -1) {
                                scope.pages.push(i);
                            }
                        }
                        if (scope.pages.indexOf(data.total_pages) === -1) {
                            scope.pages.push(data.total_pages);
                        }
                        scope.currentPage = data.current_page;
                        scope.total = data.total;

                        var minEntity = (data.current_page - 1) * data.per_page + 1;
                        if (minEntity >= data.total) {
                            scope.minEntity = null;
                        } else {
                            scope.minEntity = minEntity;
                        }

                        var maxEntity = data.current_page * data.per_page;
                        if (maxEntity >= data.total) {
                            scope.maxEntity = data.total;
                        } else {
                            scope.maxEntity = maxEntity;
                        }
                    }
                });
            }
        };
    }]);

})(angular.module('app.common.directives.pagination', []));
