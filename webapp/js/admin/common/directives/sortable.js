(function(app) {
    app.directive('sortable', ['$state', '$stateParams', function($state, $stateParams) {
        return {
            restrict: 'A',
            scope: {
                sortable: '@',
            },
            template: `<i ng-if="order_by[sortable] == 'desc'" class="fa fa-sort-desc" aria-hidden="true"></i>
                        <i ng-if="order_by[sortable] == 'asc'" class="fa fa-sort-asc" aria-hidden="true"></i>
                        <i ng-if="!order_by[sortable]" class="downup fa fa-sort" aria-hidden="true"></i>`,
            link: function(scope, element, attrs) {
                scope.order_by = !_.isUndefined($stateParams.order_by) ? JSON.parse($stateParams.order_by) : {};
                $(element).click(() => {
                    reverse();
                });

                function reverse() {
                    let key = scope.sortable;
                    let v = scope.order_by[key];
                    switch (key) {
                        case 'hired':
                            // delete scope.order_by.contacted;
                            // delete scope.order_by.rejected;
                            // delete scope.order_by.accepted;
                            scope.order_by = {};
                            scope.order_by[key] = v;
                            break;
                        case 'rejected':
                            // delete scope.order_by.contacted;
                            // delete scope.order_by.hired;
                            // delete scope.order_by.accepted;
                            scope.order_by = {};
                            scope.order_by[key] = v;
                            break;
                        case 'accepted':
                            // delete scope.order_by.contacted;
                            // delete scope.order_by.hired;
                            // delete scope.order_by.rejected;
                            scope.order_by = {};
                            scope.order_by[key] = v;
                            break;
                        case 'contacted':
                            // delete scope.order_by.accepted;
                            // delete scope.order_by.hired;
                            // delete scope.order_by.rejected;
                            scope.order_by = {};
                            scope.order_by[key] = v;
                            break;
                        default:
                            delete scope.order_by.contacted;
                            delete scope.order_by.accepted;
                            delete scope.order_by.rejected;
                            delete scope.order_by.hired;
                            break;
                    }
                    if (_.isUndefined(scope.order_by[key])) {
                        scope.order_by[key] = 'desc';
                    } else {
                        switch (scope.order_by[key]) {
                            case 'desc':
                                scope.order_by[key] = 'asc';
                                break;
                            case 'asc':
                                delete scope.order_by[key];
                                break;
                        }
                    }
                    $stateParams.page = 1;
                    $stateParams.order_by = JSON.stringify(scope.order_by);
                    $state.go($state.current.name, $stateParams, {
                        reload: $state.current.name,
                    });
                }
            }
        };
    }]);
    app.directive('sortableAttributes', ['$state', '$stateParams', function($state, $stateParams) {
        return {
            restrict: 'E',
            scope: {},
            template: `<div class="sortable-attributes">
                <ul>
                    <li ng-repeat="item in attributes"><span ng-bind="item.key"></span>
                        <i ng-if="item.direction == 'desc'" class="fa fa-sort-desc" aria-hidden="true"></i>
                        <i ng-if="item.direction == 'asc'" class="fa fa-sort-asc" aria-hidden="true"></i>
                        <i class="fa fa-remove" ng-click="remove(item)"></i>
                    </li>
                </ul>
            </div>`,
            link: function(scope, element, attrs) {
                let order_by = !_.isUndefined($stateParams.order_by) ? JSON.parse($stateParams.order_by) : {};
                scope.attributes = [];
                for (let k in order_by) {
                    scope.attributes.push({
                        key: k,
                        direction: order_by[k],
                    });
                }
                scope.remove = function(item) {
                    _.remove(scope.attributes, item);
                    let order_by = {};
                    _.forEach(scope.attributes, (item) => {
                        order_by[item.key] = item.direction;
                    });
                    $stateParams.order_by = JSON.stringify(order_by);
                    $state.go($state.current.name, $stateParams, {
                        reload: $state.current.name,
                    });
                };
            }
        };
    }]);
})(angular.module('app.common.directives.sortable', []));
