(function(app) {
    app.directive('uploadFile', ['Upload', 'Notification', function(Upload, Notification) {
        return {
            restrict: 'A',
            scope: {
                uploadFile: '=',
                onComplete: '=',
                onError: '=',
                inProgress: '=',
                progressPercentage: '=',
                allowMaxSize: '@',
                accept: '@',
                type: '@',
                uploadType: '@',
                id: '@'
            },
            link: (scope, element, attrs) => {
                if (!_.isUndefined(scope.accept)) {
                    var valid_extensions = scope.accept.split('|');
                }
                $(element).click(() => {
                    if (scope.inProgress !== true) {
                        var input = document.createElement('input');
                        input.type = 'file';
                        $(input).trigger('click');
                        $(input).on('change', (event) => {
                            scope.uploadFile = input.files[0];
                            if (input.files[0] !== undefined) {
                                var maxSize = _.isUndefined(scope.allowMaxSize) ? AllowMaxFileSize : scope.allowMaxSize;
                                if (scope.uploadFile.size > maxSize * 1024 * 1024) {
                                    Notification.show('warning', `Please select file less than ${maxSize}MB`, 5000);
                                    return false;
                                }
                                if (!_.isUndefined(scope.accept) && _.isUndefined(_.find(valid_extensions, (item) => item.toLowerCase() === getFileExtension(scope.uploadFile).toLowerCase()))) {
                                    Notification.show('warning', `Please select a file with valid type`, 5000);
                                    return false;
                                }

                                upload(scope.uploadFile);
                            }
                            scope.$apply();
                        });
                    }
                });

                function getFileExtension(file) {
                    return file.name.split('.').pop();
                }

                function upload(file) {
                    scope.inProgress = true;
                    let type = _.isNil(scope.type) ? 'images' : scope.type;
                    let uploadType = _.isNil(scope.uploadType) ? 'default' : scope.uploadType;

                    Upload.upload({
                        url: '/api/file/upload',
                        data: {
                            file: file,
                            type: type,
                            uploadType: uploadType,
                            id: scope.id
                        }
                    }).then(function(response) {
                        scope.inProgress = false;
                        scope.progressPercentage = 0;
                        if (_.isFunction(scope.onComplete)) {
                            scope.onComplete(response);
                        }
                    }, function(response) {
                        scope.inProgress = false;
                        scope.progressPercentage = 0;
                        if (_.isFunction(scope.onError)) {
                            scope.onError(response);
                        }
                    }, function(evt) {
                        scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                        console.log(scope.progressPercentage);
                    });
                }
            },
        };
    }]);
})(angular.module('app.common.directives.uploadFile', [
    'ngFileUpload'
]));