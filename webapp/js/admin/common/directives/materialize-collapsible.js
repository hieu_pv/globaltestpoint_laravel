(function(app) {
    app.directive('materializeCollapsible', function() {
        return {
            strict: 'A',
            link: function(scope, element) {
                $(element).sideNav();
            }
        };
    });
})(angular.module('app.common.directives.materialize-collapsible', []));
