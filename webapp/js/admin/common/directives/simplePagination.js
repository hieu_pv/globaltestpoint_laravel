(function(app) {
    app.directive('simplePagination', function() {
        return {
            restrict: 'E',
            templateUrl: '/webapp/js/admin/common/partials/_simple_pagination.tpl.html',
            scope: {
                page: '=',
            },
            link: function(scope, element, attrs) {
                scope.page = scope.page || 1;
                scope.next = function() {
                    scope.page++;
                };
                scope.prev = function() {
                    scope.page--;
                };
            }
        };
    });
})(angular.module('app.common.directives.simplePagination', []));
