(function(app) {
    app.directive('autocompleteAddress', ['$window', function($window) {
        return {
            restrict: 'AE',
            require: "^ngModel",
            scope: {
                autocompleteAddress: '='
            },
            link: function(scope, element, attrs, ngModelCtrl) {
                var placeSearch, autocomplete;
                if (_.isUndefined($window.google)) {
                    document.addEventListener('googleMapSdkLoaded', function(e) {
                        initAutocomplete();
                    }, false);
                } else {
                    initAutocomplete();
                }

                function initAutocomplete() {
                    autocomplete = new google.maps.places.Autocomplete(
                        element[0], {
                            types: ['geocode'],
                        });

                    google.maps.event.addListener(autocomplete, 'place_changed', function() {
                        var place = autocomplete.getPlace();
                        var componentForm = {
                            street_number: 'short_name',
                            route: 'long_name',
                            locality: 'long_name',
                            administrative_area_level_1: 'short_name',
                            country: 'long_name',
                            postal_code: 'short_name'
                        };

                        let components = {};

                        for (var i = 0; i < place.address_components.length; i++) {
                            var addressType = place.address_components[i].types[0];
                            if (componentForm[addressType]) {
                                components[addressType] = place.address_components[i][componentForm[addressType]];
                            }
                        }

                        scope.autocompleteAddress = _.assign(place, {
                            components: components,
                        });

                        ngModelCtrl.$setViewValue(element[0].value);
                    });

                    google.maps.event.addDomListener(element[0], 'keydown', function(e) {
                        if (e.keyCode == 13) {
                            e.preventDefault();
                        }
                    });
                }
            }
        };
    }]);

})(angular.module('app.common.directives.autocompleteAddress', []));