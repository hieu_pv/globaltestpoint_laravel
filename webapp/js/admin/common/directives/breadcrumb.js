(function(app) {
    app.directive('breadcrumb', ['$rootScope', '$state', '$stateParams', function($rootScope, $state, $stateParams) {
        return {
            restrict: 'E',
            scope: {
                items: '='
            },
            templateUrl: '/webapp/js/admin/common/partials/_breadcrumb.tpl.html',
            link: function(scope, element, attrs) {}
        };
    }]);
})(angular.module('app.common.directives.breadcrumb', []));
