(function(app) {
    app.directive('userProfileImage', ['$rootScope', function($rootScope) {
        return {
            strict: 'E',
            scope: {
                image: '=',
                isUpdateProfile: '='
            },
            template: `
                <div class="user-profile-image"> 
                    <div ng-if="isUpdateProfile == true">
                        <div class="upload-icon">
                            <div class="icon">
                                <i class="fa fa-camera" aria-hidden="true"></i>
                            </div>
                            <p>Update Picture</p>
                        </div>
                        <div ng-if="isDefaultImage(image)" class="placeholder-picture">UPLOAD PICTURE</div>
                        
                        <img ng-if="!isDefaultImage(image)" src="/assets/images/transparent-1x1.png" style="background-image: url(\'{{image}}\')" alt="user profile image" alt="user profile image" />
                    </div>
                    <img ng-if="isUpdateProfile != true" src="/assets/images/transparent-1x1.png" style="background-image: url(\'{{image}}\')" alt="user profile image" alt="user profile image" />
                </div>
            `,
            link: function(scope, element, attrs) {
                scope.isDefaultImage = function(image) {
                    var isDefaultImage = false;
                    if (_.isNil(image) || image === '') {
                        isDefaultImage = true;
                    }
                    return isDefaultImage;
                };
            }
        };
    }]);
})(angular.module('app.common.directives.userProfileImage', []));