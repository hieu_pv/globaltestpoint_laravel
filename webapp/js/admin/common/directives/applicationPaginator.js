(function(app) {
    app.directive('applicationPaginator', function() {
        return {
            restrict: 'E',
            scope: {
                data: '=',
                page: '=',
            },
            templateUrl: '/webapp/js/app/common/partials/_application_paginator.tpl.html',
            link: function(scope, element, attrs) {
                if (scope.page === undefined) {
                    scope.page = 1;
                }
            }
        };
    });
})(angular.module('app.common.directives.applicationPaginator', []));
