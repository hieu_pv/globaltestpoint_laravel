(function(app) {
    app.directive('materialDatetimePicker', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                $(element).pickadate({
                    selectMonths: true, 
                    selectYears: 1
                });
            }
        };
    });
})(angular.module('app.common.directives.materialDatetimePicker', []));
