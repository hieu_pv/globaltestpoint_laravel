(function(app) {
    app.directive('geocoding', ['$window', function($window) {
        return {
            restrict: 'E',
            scope: {
                address: '=',
                zipcode: '=',
                city: '=',
                lat: '=',
                lng: '=',
                defaultLat: '=',
                defaultLng: '=',
            },
            template: '<div id="nf-geocoding-map" class="nf-geocoding"></div>',
            link: function(scope, element, attrs) {
                var map;
                if (_.isUndefined($window.google)) {
                    document.addEventListener('googleMapSdkLoaded', function(e) {
                        var lat = scope.lat || scope.defaultLat;
                        var lng = scope.lng || scope.defaultLng;
                        CreateMap(lat, lng);
                    }, false);
                } else {
                    CreateMap(scope.defaultLat, scope.defaultLng);
                }

                scope.$watch('lat', function(lat) {
                    if (!_.isUndefined($window.google) && !_.isUndefined(lat) && !_.isUndefined(scope.lng)) {
                        CreateMap(lat, scope.lng);
                    }
                });
                scope.$watch('lng', function(lng) {
                    if (!_.isUndefined($window.google) && !_.isUndefined(lng) && !_.isUndefined(scope.lat)) {
                        CreateMap(scope.lat, lng);
                    }
                });

                function CreateMap(lat, lng) {
                    if (!_.isUndefined($window.initAutocomplete)) {
                        $window.initAutocomplete();
                    }
                    var myLatlng = new google.maps.LatLng(lat, lng);
                    var mapOptions = {
                        zoom: 14,
                        center: myLatlng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        scrollwheel: false,
                        navigationControl: false,
                        mapTypeControl: false,
                    };
                    map = new google.maps.Map(document.getElementById('nf-geocoding-map'), mapOptions);
                    var marker = new google.maps.Marker({
                        position: myLatlng,
                        map: map,
                    });
                }

                scope.$watchGroup(['address', 'zipcode', 'city'], function(newValues, oldValues, scope) {
                    if (!_.isUndefined(map)) {
                        let add = '';
                        if (!_.isNil(scope.address) && scope.address !== '') {
                            add = scope.address;
                        }
                        if (!_.isNil(scope.zipcode) && scope.zipcode !== '') {
                            add = `${add} ${scope.zipcode}`;
                        }
                        if (!_.isNil(scope.city) && scope.city !== '') {
                            add = `${add} ${scope.city}`;
                        }
                        codeAddress(add);
                    }
                });

                function codeAddress(data) {
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        'address': data
                    }, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            map.setCenter(results[0].geometry.location);
                            var marker = new google.maps.Marker({
                                map: map,
                                position: results[0].geometry.location
                            });
                            var latlng = results[0].geometry.location.toJSON();
                            scope.lat = latlng.lat;
                            scope.lng = latlng.lng;
                        } else {
                            console.log("Geocode was not successful for the following reason: " + status);
                        }
                    });
                }
            }
        };
    }]);
})(angular.module('app.common.directives.geocoding', []));
