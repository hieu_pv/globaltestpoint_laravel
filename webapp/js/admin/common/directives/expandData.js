(function(app) {

    app.directive('expandData', ['$compile', '$templateCache', 'API', 'preloader', '$rootScope', function($compile, $templateCache, API, preloader, $rootScope) {
        return {
            restrict: 'A',
            scope: {
                id: '=',
                api: '@',
                template: '@'
            },
            link: function(scope, element, attrs) {

                function isDescendant(parent, child) {
                    var node = child.parentNode;
                    while (node !== null) {
                        if (node === parent) {
                            return true;
                        }
                        node = node.parentNode;
                    }
                    return false;
                }

                element.remove = function() {
                    if ($('main').find('#expand-data').length) {
                        $('#expand-data').remove();
                    }
                };

                $(element).click(function(e) {
                    let tr = $(element).closest('tr');
                    let top = tr.offset().top;
                    let height = tr.outerHeight(true);
                    let totalHeight = top + height;
                    let width = tr.outerWidth(true);

                    if ($('main').find('#expand-data').length) {
                        $('#expand-data').remove();
                    } else {
                        preloader.show();
                        switch (scope.api) {
                            case 'user':
                                API.admin.user.show(scope.id)
                                    .then(function(user) {
                                        if (user.industries.length > 0) {
                                            user.showIndustry = _.map(user.industries, 'name').join(', ');
                                        }
                                        scope.user = user;
                                    })
                                    .catch(function(error) {
                                        preloader.hide();
                                        throw error;
                                    });

                                var day_of_week = {
                                    0: 'Mon',
                                    1: 'Tue',
                                    2: 'Wed',
                                    3: 'Thu',
                                    4: 'Fri',
                                    5: 'Sat',
                                    6: 'Sun'
                                };
                                API.admin.availability.get()
                                    .then((availabilities) => {
                                        API.admin.user.availabilities(scope.id)
                                            .then((user_availabilities) => {
                                                availabilities = availabilities.map((item) => {
                                                    if (!_.isUndefined(user_availabilities.find(i => i.getId() === item.getId()))) {
                                                        item.selected = true;
                                                    }
                                                    return item;
                                                });
                                                scope.dayAvailabilities = _.map(_.groupBy(availabilities, 'day_of_week'), (availabilities, key) => {
                                                    return {
                                                        selected: !_.isUndefined(_.find(availabilities, item => item.selected)),
                                                        day_of_week: key,
                                                        availabilities: availabilities,
                                                        name: day_of_week[key]
                                                    };
                                                });
                                            })
                                            .catch((error) => {
                                                preloader.hide();
                                                throw error;
                                            });
                                    })
                                    .catch((error) => {
                                        preloader.hide();
                                        throw error;
                                    });

                                scope.$watchGroup(['user', 'dayAvailabilities'], function(newValues, oldValues, scope) {
                                    if (!_.isNil(scope.user) && !_.isNil(scope.dayAvailabilities)) {
                                        scope.data = {
                                            user: scope.user,
                                            dayAvailabilities: scope.dayAvailabilities
                                        };
                                    }
                                });
                                break;
                            case 'job':
                                API.admin.job.show(scope.id)
                                    .then(function(job) {
                                        scope.data = job;
                                    })
                                    .catch(function(error) {
                                        preloader.hide();
                                        throw error;
                                    });
                                break;
                        }
                        e.stopPropagation();
                    }

                    scope.$watch('data', function(data) {
                        if (!_.isNil(data) && !$('main').find('#expand-data').length) {
                            setTimeout(function() {
                                let html = $compile(`<div id="expand-data" style="top: ${totalHeight}px"><i class="icon-close fa fa-close"></i><div expand-data-wrapper style="width: ${width}px" data="data" template="${scope.template}" class="expand-data-wrapper"></div></div>`)(scope);
                                $("main").append(html);
                            }, 2000);
                        }
                    });
                });

                $('body').click(function(e) {
                    if ($('main').find('#expand-data').length) {
                        if (!isDescendant(document.getElementById('expand-data'), e.target)) {
                            element.remove();
                        } else {
                            if ($(e.target).is('#expand-data .icon-close')) {
                                element.remove();
                            }
                        }
                    }
                });
            }
        };
    }]);

    app.directive('expandDataWrapper', ['$rootScope', 'preloader', function($rootScope, preloader) {
        return {
            restrict: 'A',
            scope: {
                data: '=',
                template: '@'
            },
            link: function(scope, element, attrs) {
                scope.getContentUrl = function() {
                    return attrs.template;
                };
                preloader.hide();
            },
            template: '<div ng-include="getContentUrl()"></div>'
        };
    }]);

})(angular.module('app.common.directives.expand-data', []));
