(function(app) {

    app.directive('pushpin', function() {
        return {
            restrict: 'AC',
            scope: false,
            link: function(scope, element, attrs) {
                let {
                    top, bottom, offset
                } = attrs;
                $(element).pushpin({
                    top: top,
                    bottom: bottom,
                    offset: offset
                });
            }
        };
    });

})(angular.module('app.common.directives.pushpin', []));
