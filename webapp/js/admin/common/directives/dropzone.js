var Dropzone = require('dropzone');
require('ngFileUpload');

(function(app) {
    app.directive('dropzone', function() {
        return {
            strict: 'AE',
            scope: {
                beforeUpload: '=?',
                onSuccess: '=',
                onError: '=?'
            },
            link: function(scope, element, attrs) {
                var config = {
                    url: '/api/file/upload',
                    paramName: "file",
                    clickable: true
                };

                var eventHandlers = {

                    'addedfile': function(file) {
                        var check = scope.beforeUpload(file);
                        if (check === false) {
                            this.removeFile(this.files[0]);
                        }
                    },

                    'sending': function(file, xhr, formData) {
                        if (scope.maxFileName !== undefined) {
                            formData.append('maxFileName', scope.maxFileName);
                        }
                    },

                    'success': function(file, response) {
                        scope.onSuccess(response, file);
                    },

                    'queuecomplete': function() {
                        this.removeAllFiles();
                    },

                    'error': function(file, err) {
                        scope.onError(err, file);
                    }

                };

                var dropzone = new Dropzone(element[0], config);

                angular.forEach(eventHandlers, function(handler, event) {
                    dropzone.on(event, handler);
                });
            },
        };
    });
})(angular.module('app.common.directives.dropzone', ['ngFileUpload']));
