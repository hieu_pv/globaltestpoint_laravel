(function(app) {
    app.directive('updateTextFields', ['$window', function($window) {
        function updateTextFields(element) {
            $(element).find('input').each(function() {
                if ($(this).val().length > 0) {
                    $(this).siblings('label').addClass('active');
                }
            });
        }
        return {
            restrict: 'A',
            scope: true,
            link: function(scope, element, attrs) {
                updateTextFields(element);
                _([100, 500, 1000, 2000]).forEach(function(time) {
                    setTimeout(function() {
                        updateTextFields(element);
                    }, time);
                });
            }
        };
    }]);
})(angular.module('app.common.directives.updateTextFields', []));
