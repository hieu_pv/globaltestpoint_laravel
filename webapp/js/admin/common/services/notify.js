(function(app) {
    app.service('Notification', ['$compile', '$rootScope', function($compile, $rootScope) {
        function scrollTop() {
            $("html, body").animate({
                scrollTop: 0
            }, 500);
        }
        this.show = function(type, content, time) {
            // scrollTop();
            this.remove();
            var notify = document.createElement('div');
            notify.id = 'nf-notify';
            notify.className = 'notify notify-' + type;
            var icon;
            if (type == 'warning') {
                icon = 'warning';
            }
            if (type == 'success') {
                icon = 'check';
            }
            let html = `<div class="notify-container"><i class="fa fa-${icon}"></i><span class="notify-message">${content}</span><i class="fa fa-close"></i></div>`;
            html = $compile(angular.element(html))($rootScope);
            $(notify).append(html);
            $('body').prepend(notify);
            $(notify).animate({
                height: 40
            }, 500);
            if (time && parseInt(time) > 0) {
                time = parseInt(time);
                setTimeout(function() {
                    $(notify).animate({
                        height: 0
                    }, 250);
                }, time);
            }
            $(notify).find('.right-align').click(function() {
                if ($('body').find('#nf-notify').length) {
                    $('#nf-notify').remove();
                }
            });
        };
        this.remove = function() {
            if ($('body').find('#nf-notify').length) {
                $('#nf-notify').remove();
            }
        };
    }]);
})(angular.module('app.common.services.Notification', []));
