(function(app) {
    app.service('preloader', ['$rootScope', function($rootScope) {
        this.show = function() {
            if ($('#nf-preloader').length === 0) {
                var preloader = $('<div id="nf-preloader"><div class="nf-preloader-bg"><div class="progress"><div class="indeterminate"></div></div></div></div>');
                $('body').append(preloader);
            }
            $rootScope.inProgress = true;
        };
        this.hide = function() {
            if ($('#nf-preloader').length > 0) {
                $('#nf-preloader').remove();
            }
            $rootScope.inProgress = false;
        };
    }]);
})(angular.module('app.common.services.preloader', []));
