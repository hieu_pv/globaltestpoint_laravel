(function(app) {
    app.filter('trustAsHtml', ['$sce', function($sce) {
        return function(html) {
            return $sce.trustAsHtml(html);
        };
    }]);
    app.filter('trustAsResourceUrl', ['$sce', function($sce) {
        return function(src) {
            return $sce.trustAsResourceUrl(src);
        };
    }]);
})(angular.module('app.common.filters.trustAsHtml', []));
