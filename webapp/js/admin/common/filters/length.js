(function(app) {
    app.filter('length', function() {
        return function(entity) {
            if (_.isArray(entity)) {
                return entity.length;
            }
            if (_.isString(entity)) {
                return entity.length;
            }
        };
    });
})(angular.module('app.common.filters.length', []));
