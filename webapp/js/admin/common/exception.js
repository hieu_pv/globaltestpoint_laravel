(function(app) {
    app.config(['$logProvider', '$provide', function($logProvider, $provide) {
        $logProvider.debugEnabled(true);

        $provide.decorator('$exceptionHandler', [
            '$log',
            '$delegate',
            '$injector',
            function($log, $delegate, $injector) {
                return function(exception) {
                    $log.debug('Exception:', exception);
                    var $rootScope = $injector.get("$rootScope");
                    if (typeof exception === 'string') {
                        exception = exception.replace(/(.*):\s\{(.*)\}$/, "\{$2\}");
                        exception = $.parseJSON(exception);
                    }
                    $rootScope.$broadcast('onError', exception);
                };
            }
        ]);
    }]);
    /*
     * Show Error message
     */
    app.run(errorHandler);

    errorHandler.$inject = ['$rootScope', 'Notification'];

    function errorHandler($rootScope, Notification) {
        $rootScope.$on('onError', function(e, err) {
            if (!$rootScope.hasError) {
                $rootScope.hasError = true;
            }
            if (err.message !== undefined) {
                if (err.code === 1000) {
                    var errors = $.parseJSON(err.message);
                    var message = '';
                    for (var key in errors) {
                        message += errors[key].join(' ') + ' ';
                    }
                    Notification.show('warning', message, 5000);
                } else {
                    Notification.show('warning', err.message, 5000);
                }
            }
        });
    }
})(angular.module('app.common.exceptionHandler', []));
