require('./sagas');
var {
    createStore,
    combineReducers,
    applyMiddleware
} = require('redux');
var logger = require('redux-logger').default;

var reduxSaga = require('redux-saga');
var createSagaMiddleware = reduxSaga.default;

var reducer = require('./reducers');

(function(app) {
    app.factory('store', store);

    store.$inject = [
        'sagas',
    ];

    function store(sagas) {
        const sagaMiddleware = createSagaMiddleware();
        let store;
        if (BASE_URL.indexOf('dev') > -1) {
            store = createStore(reducer, applyMiddleware(sagaMiddleware, logger));
        } else {
            store = createStore(reducer, applyMiddleware(sagaMiddleware));
        }
        sagaMiddleware.run(sagas);
        return store;
    }

})(angular.module('app.store', [
    'app.store.sagas',
]));
