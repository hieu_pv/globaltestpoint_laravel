var {
    combineReducers,
} = require('redux');

let {
    PostJob,
} = require('../components/jobs/create/reducer');

let {
    UpdateJob,
} = require('../components/jobs/update/reducer');

let {
    EmailManagement,
} = require('../components/email_management/reducer');

let {
    JobTitle,
    Experience,
    Nationality,
    Qualification,
    Country,
    State,
    City,
    Industry,
    Tag
} = require('../components/jobs/reducer');

let {
    CreateIndustry
} = require('../components/industries/create/reducer');

let {
    UpdateIndustry
} = require('../components/industries/edit/reducer');

let {
    FETCH_USER_STATUS_SUCCESSED,
} = require('./action');

let UserStatus = (state, action) => {
    if (_.isUndefined(state)) {
        state = {
            fetched: false,
            status: [],
            forFilterSelection: [],
        };
    }
    switch (action.type) {
        case FETCH_USER_STATUS_SUCCESSED:
            return _.assign(state, {
                fetched: true,
                status: action.data,
                forFilterSelection: _.concat(action.data, {
                    active: true,
                    description: "",
                    id: 0,
                    status: "All",
                }),
            });
        default:
            return state;
    }
};

module.exports = combineReducers({
    UserStatus,
    PostJob,
    UpdateJob,
    EmailManagement,
    JobTitle,
    Experience,
    Nationality,
    Qualification,
    Country,
    State,
    City,
    Industry,
    Tag,
    CreateIndustry,
    UpdateIndustry
});
