require('../components/jobs/sagas');
require('../components/email_management/sagas');
require('../components/industries/create/sagas');
require('../components/industries/edit/sagas');

var {
    call,
    put,
    takeEvery,
    takeLatest,
    fork
} = require('redux-saga/effects');


let {
    FETCH_USER_STATUS_REQUESTED,
    FETCH_USER_STATUS_SUCCESSED,
    INIT_GOOGLE_MAP_SDK,
} = require('./action');

(function(app) {
    app.factory('sagas', sagas);
    sagas.$inject = [
        '$rootScope',
        '$window',
        'API',
        'Notification',
        'preloader',
        'JobSagas',
        'EmailManagementSagas',
        'CreateIndustrySagas',
        'EditIndustrySagas'
    ];

    function sagas(
        $rootScope,
        $window,
        API,
        Notification,
        preloader,
        JobSagas,
        EmailManagementSagas,
        CreateIndustrySagas,
        EditIndustrySagas
    ) {
        function onError(action) {
            if (!$rootScope.hasError) {
                $rootScope.hasError = true;
            }
            let err = action.error;
            if (!_.isNil(err) && err.message !== undefined) {
                preloader.hide();
                console.log(err);
                if (err.code === 1000) {
                    var errors = $.parseJSON(err.message);
                    var message = '';
                    for (var key in errors) {
                        message += errors[key].join(' ') + ' ';
                    }
                    Notification.show('warning', message, 5000);
                } else {
                    Notification.show('warning', err.message, 5000);
                }
            }
        }

        function* watchOnError() {
            yield takeEvery('API_CALL_ERROR', onError);
        }

        function* proccessFetchUserStatus(action) {
            try {
                let request = yield API.admin.user.getStatus();
                yield put({
                    type: FETCH_USER_STATUS_SUCCESSED,
                    data: request,
                });
            } catch (error) {
                yield put({
                    type: 'API_CALL_ERROR',
                    error: error,
                });
            }
        }

        function* watchFetchUserStatus() {
            yield takeLatest(FETCH_USER_STATUS_REQUESTED, proccessFetchUserStatus);
        }

        function initGoogleMapSdk(action) {
            let event = document.createEvent('Event');
            event.initEvent('googleMapSdkLoaded', true, true);
            $window.dispatchEventGoogleMapSdkLoaded = function() {
                document.dispatchEvent(event);
            };
            if (document.getElementById('gmap-sdk') !== undefined && document.getElementById('gmap-sdk') !== null) {
                document.dispatchEvent(event);
            } else {
                var script = document.createElement("script");
                script.type = "text/javascript";
                script.id = 'gmap-sdk';
                script.src = "//maps.google.com/maps/api/js?key=" + GOOGLE_MAP_API_KEY + "&libraries=places&callback=dispatchEventGoogleMapSdkLoaded";
                document.body.appendChild(script);
            }
        }

        function* watchInitGoogleMapSdk() {
            yield takeLatest(INIT_GOOGLE_MAP_SDK, initGoogleMapSdk);
        }

        let StateSagas = _.map([
            watchOnError,
            watchFetchUserStatus,
            watchInitGoogleMapSdk,
        ], item => fork(item));

        return function* sagas() {
            yield _.concat(
                StateSagas,
                JobSagas,
                EmailManagementSagas,
                CreateIndustrySagas,
                EditIndustrySagas
            );
        };
    }

})(angular.module('app.store.sagas', [
    'app.components.jobs.sagas',
    'app.components.email_management.sagas',
    'app.components.industries.create.sagas',
    'app.components.industries.edit.sagas'
]));