(function(app) {

    app.controller('AdminCreateJobTitleCtrl', AdminCreateJobTitleCtrl);

    AdminCreateJobTitleCtrl.$inject = ['$rootScope', '$scope', 'API', 'preloader', 'Notification'];

    function AdminCreateJobTitleCtrl($rootScope, $scope, API, preloader, Notification) {
        var vm = this;

        vm.job_title = {
            attributes: {
                company_name: 'Company Name'
            }
        };

        vm.createJobTitle = createJobTitle;

        function createJobTitle(job_title) {
            preloader.show();
            API.admin.job_title.create(job_title)
                .then(function(response) {
                    preloader.hide();
                    vm.create_job_title_form.$setPristine();
                    vm.job_title = null;
                    Notification.show('success', 'Added success!', 5000);
                })
                .catch(function(error) {
                    preloader.hide();
                    vm.create_job_title_form.$setPristine();
                    vm.job_title = null;
                    throw error;
                });
        }
    }

})(angular.module('app.components.job_titles.create', []));