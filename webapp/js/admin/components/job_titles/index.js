require('./router');
require('./list/');
require('./create/');
require('./edit/');

(function(app) {

    app.controller('AdminJobTitlesCtrl', AdminJobTitlesCtrl);

    AdminJobTitlesCtrl.$inject = ['$rootScope', '$scope', '$state'];

    function AdminJobTitlesCtrl($rootScope, $scope, $state) {

        var current_state = $state.current.name;
        if (!_.isNil(current_state) && current_state === 'app.job_titles') {
            $state.go('app.job_titles.list', {
                page: 1
            });
        }
    }

})(angular.module('app.components.job_titles', [
    'app.components.job_titles.router',
    'app.components.job_titles.list',
    'app.components.job_titles.create',
    'app.components.job_titles.edit'
]));