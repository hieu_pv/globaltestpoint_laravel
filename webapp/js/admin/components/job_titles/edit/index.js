(function(app) {

    app.controller('AdminEditJobTitleCtrl', AdminEditJobTitleCtrl);

    AdminEditJobTitleCtrl.$inject = ['$rootScope', '$scope', 'API', 'job_title', '$state', '$stateParams', 'Notification', 'preloader'];

    function AdminEditJobTitleCtrl($rootScope, $scope, API, job_title, $state, $stateParams, Notification, preloader) {
        var vm = this;

        vm.job_title = job_title;
        vm.job_title.attributes = {
            company_name: 'Company Name'
        };

        vm.editJobTitle = editJobTitle;

        function editJobTitle(job_title) {
            preloader.show();
            API.admin.job_title.update(job_title)
                .then(function(response) {
                    preloader.hide();
                    vm.edit_job_title_form.$setPristine();
                    Notification.show('success', 'Updated success!', 5000);
                })
                .catch(function(error) {
                    preloader.hide();
                    vm.edit_job_title_form.$setPristine();
                    throw error;
                });
        }
    }

})(angular.module('app.components.job_titles.edit', []));