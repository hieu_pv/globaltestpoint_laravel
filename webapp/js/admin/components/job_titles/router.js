(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.job_titles', {
                url: '/job_titles',
                views: {
                    'content@app': {
                        templateUrl: "/webapp/js/admin/components/job_titles/_job_titles.tpl.html",
                        controller: 'AdminJobTitlesCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.job_titles.list', {
                url: '/list?page&search&order_by&constraints',
                views: {
                    'admin-job-titles@app.job_titles': {
                        templateUrl: "/webapp/js/admin/components/job_titles/list/_list.tpl.html",
                        controller: 'AdminListJobTitlesCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.job_titles.create', {
                url: '/create',
                views: {
                    'admin-job-titles@app.job_titles': {
                        templateUrl: "/webapp/js/admin/components/job_titles/create/_create.tpl.html",
                        controller: 'AdminCreateJobTitleCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.job_titles.edit', {
                url: '/:id/edit',
                views: {
                    'admin-job-titles@app.job_titles': {
                        templateUrl: "/webapp/js/admin/components/job_titles/edit/_edit.tpl.html",
                        controller: 'AdminEditJobTitleCtrl',
                        controllerAs: 'vm',
                    }
                },
                resolve: {
                    job_title: ['API', '$stateParams', function(API, $stateParams) {
                        return API.admin.job_title.show($stateParams.id);
                    }]
                }
            });
    }]);
})(angular.module('app.components.job_titles.router', []));