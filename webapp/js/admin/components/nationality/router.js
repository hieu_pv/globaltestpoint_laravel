(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.nationality', {
                url: '/nationality',
                views: {
                    'content@app': {
                        templateUrl: "/webapp/js/admin/components/nationality/_nationality.tpl.html",
                        controller: 'AdminNationalityCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.nationality.list', {
                url: '/list?page&search&order_by&constraints',
                views: {
                    'admin-nationality@app.nationality': {
                        templateUrl: "/webapp/js/admin/components/nationality/list/_list.tpl.html",
                        controller: 'AdminListNationalitiesCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.nationality.create', {
                url: '/create',
                views: {
                    'admin-nationality@app.nationality': {
                        templateUrl: "/webapp/js/admin/components/nationality/create/_create.tpl.html",
                        controller: 'AdminCreateNationalityCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.nationality.edit', {
                url: '/:id/edit',
                views: {
                    'admin-nationality@app.nationality': {
                        templateUrl: "/webapp/js/admin/components/nationality/edit/_edit.tpl.html",
                        controller: 'AdminEditNationalityCtrl',
                        controllerAs: 'vm',
                    }
                },
                resolve: {
                    nationality: ['API', '$stateParams', function(API, $stateParams) {
                        return API.admin.nationality.show($stateParams.id);
                    }]
                }
            });
    }]);
})(angular.module('app.components.nationality.router', []));