(function(app) {

    app.controller('AdminCreateNationalityCtrl', AdminCreateNationalityCtrl);

    AdminCreateNationalityCtrl.$inject = ['$rootScope', '$scope', 'API', 'preloader', 'Notification'];

    function AdminCreateNationalityCtrl($rootScope, $scope, API, preloader, Notification) {
        var vm = this;

        vm.createNationality = createNationality;

        function createNationality(nationality) {
            preloader.show();
            API.admin.nationality.create(nationality)
                .then(function(response) {
                    preloader.hide();
                    vm.create_nationality_form.$setPristine();
                    vm.nationality = null;
                    Notification.show('success', 'Added success!', 5000);
                })
                .catch(function(error) {
                    preloader.hide();
                    vm.create_nationality_form.$setPristine();
                    vm.nationality = null;
                    throw error;
                });
        }
    }

})(angular.module('app.components.nationality.create', []));