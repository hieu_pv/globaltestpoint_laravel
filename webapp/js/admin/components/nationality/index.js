require('./router');
require('./list/');
require('./create/');
require('./edit/');

(function(app) {

    app.controller('AdminNationalityCtrl', AdminNationalityCtrl);
    AdminNationalityCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API'];

    function AdminNationalityCtrl($rootScope, $scope, $state, $stateParams, API) {}
    
})(angular.module('app.components.nationality', [
    'app.components.nationality.router',
    'app.components.nationality.list',
    'app.components.nationality.create',
    'app.components.nationality.edit'
]));
