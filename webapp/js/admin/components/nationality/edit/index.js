(function(app) {

    app.controller('AdminEditNationalityCtrl', AdminEditNationalityCtrl);

    AdminEditNationalityCtrl.$inject = ['$rootScope', '$scope', 'API', 'nationality', '$state', '$stateParams', 'Notification', 'preloader'];

    function AdminEditNationalityCtrl($rootScope, $scope, API, nationality, $state, $stateParams, Notification, preloader) {
        var vm = this;

        vm.nationality = nationality;

        vm.editNationality = editNationality;

        function editNationality(nationality) {
            preloader.show();
            API.admin.nationality.update(nationality)
                .then(function(response) {
                    preloader.hide();
                    vm.edit_nationality_form.$setPristine();
                    Notification.show('success', 'Updated success!', 5000);
                })
                .catch(function(error) {
                    preloader.hide();
                    vm.edit_nationality_form.$setPristine();
                    throw error;
                });
        }
    }

})(angular.module('app.components.nationality.edit', []));