(function(app) {

    app.controller('AdminEditTagCtrl', AdminEditTagCtrl);

    AdminEditTagCtrl.$inject = ['$rootScope', '$scope', 'API', 'tag', '$state', '$stateParams', 'Notification', 'preloader'];

    function AdminEditTagCtrl($rootScope, $scope, API, tag, $state, $stateParams, Notification, preloader) {
        var vm = this;

        vm.tag = tag;

        vm.editTag = editTag;

        function editTag(tag) {
            preloader.show();
            API.admin.tag.update(tag)
                .then(function(response) {
                    preloader.hide();
                    vm.edit_tag_form.$setPristine();
                    Notification.show('success', 'Updated success!', 5000);
                })
                .catch(function(error) {
                    preloader.hide();
                    vm.edit_tag_form.$setPristine();
                    throw error;
                });
        }
    }

})(angular.module('app.components.tag.edit', []));