require('./router');
require('./list/');
require('./create/');
require('./edit/');

(function(app) {

    app.controller('AdminTagCtrl', AdminTagCtrl);
    AdminTagCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API'];

    function AdminTagCtrl($rootScope, $scope, $state, $stateParams, API) {}
    
})(angular.module('app.components.tag', [
    'app.components.tag.router',
    'app.components.tag.list',
    'app.components.tag.create',
    'app.components.tag.edit'
]));
