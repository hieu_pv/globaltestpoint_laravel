(function(app) {

    app.controller('AdminCreateTagCtrl', AdminCreateTagCtrl);

    AdminCreateTagCtrl.$inject = ['$rootScope', '$scope', 'API', 'preloader', 'Notification'];

    function AdminCreateTagCtrl($rootScope, $scope, API, preloader, Notification) {
        var vm = this;

        vm.createTag = createTag;

        function createTag(tag) {
            preloader.show();
            API.admin.tag.create(tag)
                .then(function(response) {
                    preloader.hide();
                    vm.create_tag_form.$setPristine();
                    vm.tag = null;
                    Notification.show('success', 'Added success!', 5000);
                })
                .catch(function(error) {
                    preloader.hide();
                    vm.create_tag_form.$setPristine();
                    vm.tag = null;
                    throw error;
                });
        }
    }

})(angular.module('app.components.tag.create', []));