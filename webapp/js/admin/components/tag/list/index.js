(function(app) {

    app.controller('AdminListTagsCtrl', AdminListTagsCtrl);

    AdminListTagsCtrl.$inject = ['$rootScope', '$scope', 'API', '$state', '$stateParams', 'Modal', 'preloader', 'Notification'];

    function AdminListTagsCtrl($rootScope, $scope, API, $state, $stateParams, Modal, preloader, Notification) {
        var vm = this;

        vm.params = $stateParams;

        vm.params.per_page = 100;

        vm.getTags = getTags;
        vm.searchPage = searchPage;
        vm.searchKeyEvent = searchKeyEvent;
        vm.filterStatus = filterStatus;
        vm.changeStatusAllItem = changeStatusAllItem;
        vm.updateSelectedItem = updateSelectedItem;
        vm.selectAll = selectAll;
        vm.deleteItem = deleteItem;

        vm.statuses = [{
            value: null,
            name: 'All'
        }, {
            value: 1,
            name: 'Active'
        }, {
            value: 0,
            name: 'Pending'
        }];
        vm.status = _.head(vm.statuses);
        if (!_.isNil($stateParams.constraints)) {
            var constraints = JSON.parse($stateParams.constraints);
            if (!_.isNil(constraints.status)) {
                vm.status = _.find(vm.statuses, {
                    value: constraints.active
                });
            } else {
                vm.status = _.head(vm.statuses);
            }
        }

        vm.getTags(vm.params);

        function getTags(params) {
            API.admin.tag.get(params)
                .then(function(response) {
                    vm.tags = response.tags;
                    vm.pagination = response.pagination;
                })
                .catch(function(error) {
                    throw error;
                });
        }

        function searchPage(search) {
            $state.go($state.current, {
                page: 1,
                search: search
            }, {
                reload: true
            });
        }

        function searchKeyEvent(event, search) {
            if (event.which === 13) {
                searchPage(search);
            }
        }

        function filterStatus(status) {
            var constraints = {};

            if (_.isNil(status.value)) {
                delete constraints.active;
            } else {
                constraints.active = status.value;
            }
            $state.go($state.current, {
                page: 1,
                constraints: JSON.stringify(constraints)
            });
        }

        function updateSelectedItem() {
            vm.selectedItem = _.filter(vm.tags, {
                selected: true
            });
        }

        function selectAll() {
            $scope.$watch('vm.tags', function(tags) {
                if (!_.isUndefined(tags)) {
                    _.forEach(tags, function(item) {
                        if (vm.selectedAll) {
                            item.selected = true;
                        } else {
                            item.selected = false;
                        }
                    });
                    vm.selectedItem = _.filter(tags, {
                        selected: true
                    });
                }
            });
        }

        function deleteItem(item) {
            var content = 'Are you sure you want to remove ' + item.getName();
            Modal.confirm({
                title: 'Warning',
                content: content,
                ok: 'YES',
                cancel: 'NO',
            }, function() {
                preloader.show();
                API.admin.tag.delete(item.getId())
                    .then(function(response) {
                        _.remove(vm.tags, item);
                        preloader.hide();
                    })
                    .catch(function(error) {
                        preloader.hide();
                        throw error;
                    });
            }, function() {});
        }

        function changeStatusAllItem(type) {
            vm.selectedItem = _.filter(vm.tags, {
                selected: true
            });
            if (vm.selectedItem.length === 0) {
                Notification.show('warning', 'Please select at least one item', 5000);
                return false;
            } else {
                preloader.show();
                var item_ids = _.map(vm.selectedItem, 'id');
                API.admin.tag.changeStatusAllItem(item_ids, type)
                    .then(function(response) {
                        Notification.show('success', 'Change status success!', 5000);
                        vm.getTags(vm.params);
                        vm.selectedAll = false;
                        vm.selectAll();
                        preloader.hide();
                    })
                    .catch(function(error) {
                        preloader.hide();
                        throw error;
                    });
            }
        }
    }

})(angular.module('app.components.tag.list', []));