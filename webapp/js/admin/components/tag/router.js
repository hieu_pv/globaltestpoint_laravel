(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.tag', {
                url: '/tag',
                views: {
                    'content@app': {
                        templateUrl: "/webapp/js/admin/components/tag/_tag.tpl.html",
                        controller: 'AdminTagCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.tag.list', {
                url: '/list?page&search&order_by&constraints',
                views: {
                    'admin-tag@app.tag': {
                        templateUrl: "/webapp/js/admin/components/tag/list/_list.tpl.html",
                        controller: 'AdminListTagsCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.tag.create', {
                url: '/create',
                views: {
                    'admin-tag@app.tag': {
                        templateUrl: "/webapp/js/admin/components/tag/create/_create.tpl.html",
                        controller: 'AdminCreateTagCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.tag.edit', {
                url: '/:id/edit',
                views: {
                    'admin-tag@app.tag': {
                        templateUrl: "/webapp/js/admin/components/tag/edit/_edit.tpl.html",
                        controller: 'AdminEditTagCtrl',
                        controllerAs: 'vm',
                    }
                },
                resolve: {
                    tag: ['API', '$stateParams', function(API, $stateParams) {
                        return API.admin.tag.show($stateParams.id);
                    }]
                }
            });
    }]);
})(angular.module('app.components.tag.router', []));