(function(app) {
    app.controller('PaymentTransactionListCtrl', PaymentTransactionListCtrl);
    PaymentTransactionListCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Modal', 'preloader'];

    function PaymentTransactionListCtrl($rootScope, $scope, $state, $stateParams, API, Modal, preloader) {
        var vm = this;
        let params = {
            type: 'charge',
            page: !_.isNil($stateParams.page) ? $stateParams.page : 1,
        };
        API.admin.payment.get(params)
            .then(function(result) {
                console.log(result);
                vm.transactions = result.transactions;
                vm.pagination = result.pagination;
            })
            .catch((error) => {
                throw error;
            });

        vm.refund = refund;
        vm.user = $rootScope.user;

        function refund(transaction) {
            Modal.confirm({
                title: 'Warning',
                htmlContent: 'Are you sure you want to refund this transaction?',
                ok: 'YES',
                cancel: 'NO',
            }, () => {
                preloader.show();
                API.admin.payment.refund(transaction.getId())
                    .then(result => {
                        console.log(result);
                        vm.transactions = _.map(vm.transactions, transaction => {
                            if (transaction.getId() === result.getId()) {
                                return result;
                            } else {
                                return transaction;
                            }
                        });
                        preloader.hide();
                    })
                    .catch((error) => {
                        preloader.hide();
                        throw error;
                    });
            }, () => {});
        }
    }
})(angular.module('app.components.payment.transactions.list', []));
