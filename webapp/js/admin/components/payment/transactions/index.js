require('./router');
require('./list');
require('./detail');

(function(app) {
    app.controller('AdminTransactionCtrl', AdminTransactionCtrl);
    AdminTransactionCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'preloader'];

    function AdminTransactionCtrl($rootScope, $scope, $state, $stateParams, API, preloader) {

    }



})(angular.module('app.components.payment.transactions', [
    'app.components.payment.transactions.router',
    'app.components.payment.transactions.list',
    'app.components.payment.transactions.detail',
]));
