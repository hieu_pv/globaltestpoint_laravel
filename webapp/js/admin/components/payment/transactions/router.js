(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.payment.transactions.list', {
                url: '/list?page',
                views: {
                    'payment@app.payment': {
                        templateUrl: "/webapp/js/admin/components/payment/transactions/list/_transaction_list.tpl.html",
                        controller: 'PaymentTransactionListCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.payment.transactions.detail', {
                url: '/:id',
                views: {
                    'content@app': {
                        templateUrl: "/webapp/js/admin/components/payment/transactions/detail/_transaction_detail.tpl.html",
                        controller: 'PaymentTransactionDetailCtrl',
                        controllerAs: 'vm',
                    }
                }
            });
    }]);
})(angular.module('app.components.payment.transactions.router', []));
