(function(app) {
    app.controller('PaymentTransactionDetailCtrl', PaymentTransactionDetailCtrl);
    PaymentTransactionDetailCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Modal', 'preloader'];

    function PaymentTransactionDetailCtrl($rootScope, $scope, $state, $stateParams, API, Modal, preloader) {
        var vm = this;

        API.admin.payment.show($stateParams.id)
            .then(function(transaction) {
                console.log(transaction);
                vm.transaction = transaction;
                vm.amount = vm.transaction.getFormatedAvailableRefundAmount();
            })
            .catch((error) => {
                throw error;
            });

        vm.toggleRefundForm = false;
        vm.refund = refund;

        function refund(transaction, amount) {
            Modal.confirm({
                title: 'Warning',
                htmlContent: 'Are you sure you want to refund this transaction?',
                ok: 'YES',
                cancel: 'NO',
            }, () => {
                preloader.show();
                API.admin.payment.refund(transaction.getId(), {
                        amount: amount
                    })
                    .then(result => {
                        console.log(result);
                        vm.refund_form.$setPristine();
                        vm.transaction = result;
                        preloader.hide();
                    })
                    .catch((error) => {
                        preloader.hide();
                        throw error;
                    });
            }, () => {});
        }
    }
})(angular.module('app.components.payment.transactions.detail', []));
