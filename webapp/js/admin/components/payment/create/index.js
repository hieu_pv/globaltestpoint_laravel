(function(app) {
    app.controller('CreatePaymentCtrl', CreatePaymentCtrl);
    CreatePaymentCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Notification', 'preloader', 'Modal'];

    function CreatePaymentCtrl($rootScope, $scope, $state, $stateParams, API, Notification, preloader, Modal) {
        if (!$rootScope.user.can('create.transactions')) {
            Modal.alert({
                title: "You don't have a required Create Transactions permission.",
                ok: 'Go to Dashboard',
                cancel: 'NO',
            }, function() {
                $state.go('app.admin_home');
            });
            return false;
        }

        var vm = this;

        API.admin.user.get({
                roles: 'employee'
            })
            .then(function(result) {
                vm.users = _.map(result.users, (i) => {
                    i.name = i.getName();
                    return i;
                });
                console.log(vm.users);
            })
            .catch(function(error) {
                throw error;
            });

        vm.pay = pay;

        function pay(transaction) {
            console.log(transaction);

            preloader.show();
            API.admin.payment.sendMoneyToConnectedAccount(transaction)
                .then((response) => {
                    preloader.hide();
                    console.log(response);
                    Notification.show('success', 'Success', 5000);
                    $state.go('app.payment.transfers.list', {}, {
                        reload: true
                    });
                })
                .catch((error) => {
                    preloader.hide();
                    throw error;
                });
        }
    }

    app.filter('propsFilter', function() {
        return function(items, props) {
            var out = [];

            if (angular.isArray(items)) {
                var keys = Object.keys(props);

                items.forEach(function(item) {
                    var itemMatches = false;

                    for (var i = 0; i < keys.length; i++) {
                        var prop = keys[i];
                        var text = props[prop].toLowerCase();
                        if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                            itemMatches = true;
                            break;
                        }
                    }

                    if (itemMatches) {
                        out.push(item);
                    }
                });
            } else {
                // Let the output be the input untouched
                out = items;
            }

            return out;
        };
    });
})(angular.module('app.components.payment.create', []));
