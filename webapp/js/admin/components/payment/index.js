require('./router');
require('./create');
require('./transactions');
require('./transfers');
require('./connected_accounts');

(function(app) {
    app.controller('PaymentCtrl', PaymentCtrl);
    PaymentCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'preloader'];

    function PaymentCtrl($rootScope, $scope, $state, $stateParams, API, preloader) {
    	console.log($state.current.name);
        if ($state.current.name === 'app.payment') {
            $state.go('app.payment.transfers.list');
        }
    }



})(angular.module('app.components.payment', [
    'app.components.payment.router',
    'app.components.payment.create',
    'app.components.payment.transactions',
    'app.components.payment.transfers',
    'app.components.payment.connected_accounts',
]));
