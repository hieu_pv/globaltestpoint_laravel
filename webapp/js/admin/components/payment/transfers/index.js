require('./router');
require('./list');
require('./detail');

(function(app) {
    app.controller('AdminTransferCtrl', AdminTransferCtrl);
    AdminTransferCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'preloader'];

    function AdminTransferCtrl($rootScope, $scope, $state, $stateParams, API, preloader) {

    }



})(angular.module('app.components.payment.transfers', [
    'app.components.payment.transfers.router',
    'app.components.payment.transfers.list',
    'app.components.payment.transfers.detail',
]));
