(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.payment.transfers.list', {
                url: '/list?page',
                views: {
                    'payment@app.payment': {
                        templateUrl: "/webapp/js/admin/components/payment/transfers/list/_transfers_list.tpl.html",
                        controller: 'AdminTransferListCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.payment.transfers.detail', {
                url: '/:id',
                views: {
                    'payment@app.payment': {
                        templateUrl: "/webapp/js/admin/components/payment/transfers/detail/_transaction_detail.tpl.html",
                        controller: 'PaymentTransferDetailCtrl',
                        controllerAs: 'vm',
                    }
                }
            });

    }]);
})(angular.module('app.components.payment.transfers.router', []));
