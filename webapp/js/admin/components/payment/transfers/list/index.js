(function(app) {
    app.controller('AdminTransferListCtrl', AdminTransferListCtrl);
    AdminTransferListCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API'];

    function AdminTransferListCtrl($rootScope, $scope, $state, $stateParams, API) {
        if (!$rootScope.user.can('view.transactions')) {
            Modal.alert({
                title: "You don't have a required View Transactions permission.",
                ok: 'Go to Dashboard',
                cancel: 'NO',
            }, function() {
                $state.go('app.admin_home');
            });
            return false;
        }

        var vm = this;
        vm.user = $rootScope.user;

        let params = {
            type: 'transfer',
            page: !_.isNil($stateParams.page) ? $stateParams.page : 1,
        };
        API.admin.payment.get(params)
            .then(function(result) {
                console.log(result);
                vm.transactions = result.transactions;
                vm.pagination = result.pagination;
            })
            .catch((error) => {
                throw error;
            });
    }
})(angular.module('app.components.payment.transfers.list', []));
