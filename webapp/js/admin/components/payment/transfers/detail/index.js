(function(app) {
    app.controller('PaymentTransferDetailCtrl', PaymentTransferDetailCtrl);
    PaymentTransferDetailCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Modal', 'preloader'];

    function PaymentTransferDetailCtrl($rootScope, $scope, $state, $stateParams, API, Modal, preloader) {
        var vm = this;

        API.admin.payment.show($stateParams.id)
            .then(function(transaction) {
                console.log(transaction);
                vm.transaction = transaction;
            })
            .catch((error) => {
                throw error;
            });
    }
})(angular.module('app.components.payment.transfers.detail', []));
