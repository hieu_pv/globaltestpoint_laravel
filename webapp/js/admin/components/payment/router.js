(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.payment', {
                url: '/payment',
                views: {
                    'content@app': {
                        templateUrl: "/webapp/js/admin/components/payment/_payment.tpl.html",
                        controller: 'PaymentCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.payment.create', {
                url: '/create',
                views: {
                    'payment@app.payment': {
                        templateUrl: "/webapp/js/admin/components/payment/create/_create.tpl.html",
                        controller: 'CreatePaymentCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.payment.transactions', {
                url: '/transactions',
                views: {
                    'payment@app.payment': {
                        controller: 'AdminTransactionCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.payment.transfers', {
                url: '/transfers',
                views: {
                    'payment@app.payment': {
                        controller: 'AdminTransactionCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.payment.connected_accounts', {
                url: '/connected-accounts',
                views: {
                    'payment@app.payment': {
                        controller: 'AdminPaymentConnectedAccountCtrl',
                        templateUrl: "/webapp/js/admin/components/payment/connected_accounts/_connected_account.tpl.html",
                        controllerAs: 'vm',
                    }
                }
            });
    }]);
})(angular.module('app.components.payment.router', []));
