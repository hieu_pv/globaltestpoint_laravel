(function(app) {
    app.controller('AdminPaymentConnectedAccountCtrl', AdminPaymentConnectedAccountCtrl);

    AdminPaymentConnectedAccountCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API'];

    function AdminPaymentConnectedAccountCtrl($rootScope, $scope, $state, $stateParams, API) {
        var vm = this;

        API.admin.user.getUsersWithConnectedAccount({
                positive_balance: true,
            })
            .then(result => {
                vm.users = result.users;
                vm.pagination = result.pagination;
            })
            .catch(error => {
                throw error;
            });
    }
})(angular.module('app.components.payment.connected_accounts', []));
