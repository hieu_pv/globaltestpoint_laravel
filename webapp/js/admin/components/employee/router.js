(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.employees', {
                url: '/employees',
                views: {
                    'content@app': {
                        templateUrl: "/webapp/js/admin/components/employee/_admin_employee.tpl.html",
                        controller: 'AdminEmployeeCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.employees.all', {
                url: '/all?page&list&status&tag&search&order_by',
                views: {
                    'admin-employees@app.employees': {
                        templateUrl: "/webapp/js/admin/components/employee/list/_list.tpl.html",
                        controller: 'ListEmployeeCtrl',
                        controllerAs: 'vm',
                    }
                },
                resolve: {
                    user_status: ['API', function(API) {
                        return API.admin.user.getStatus();
                    }],
                    tags: ['API', function(API) {
                        return API.admin.tag.get({
                                active: 1
                            })
                            .then(function(tags) {
                                return tags;
                            })
                            .catch(function(error) {
                                return [];
                            });

                    }],
                }
            })
            .state('app.employees.create', {
                url: '/create',
                views: {
                    'admin-employees@app.employees': {
                        templateUrl: "/webapp/js/admin/components/employee/create/_employee_register_form.tpl.html",
                        controller: 'CreateEmployeeCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.employees.edit', {
                url: '/:id/edit',
                views: {
                    'admin-employees@app.employees': {
                        templateUrl: "/webapp/js/admin/components/employee/edit/_edit.tpl.html",
                        controller: 'EditEmployeeCtrl',
                        controllerAs: 'vm',
                    }
                },
                resolve: {
                    profile: ['API', '$stateParams', function(API, $stateParams) {
                        return API.admin.user.show($stateParams.id);
                    }]
                }
            });
    }]);

})(angular.module('app.components.employee.router', []));
