require('./router');
require('./list/');
require('./create/');
require('./edit/');

(function(app) {

    app.controller('AdminEmployeeCtrl', AdminEmployeeCtrl);
    AdminEmployeeCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API'];

    function AdminEmployeeCtrl($rootScope, $scope, $state, $stateParams, API) {}

})(angular.module('app.components.employee', [
    'app.components.employee.router',
    'app.components.employee.list',
    'app.components.employee.create',
    'app.components.employee.edit',
]));
