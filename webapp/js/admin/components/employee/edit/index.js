var LastPosition = require('../../../../models/LastPosition');
(function(app) {

    app.controller('EditEmployeeCtrl', EditEmployeeCtrl);
    EditEmployeeCtrl.$inject = ['$rootScope', '$scope', 'profile', '$state', '$stateParams', 'Notification', 'API', 'Modal', 'preloader', 'UshiftApp', 'APP_CONSTANTS'];

    function EditEmployeeCtrl($rootScope, $scope, profile, $state, $stateParams, Notification, API, Modal, preloader, UshiftApp, APP_CONSTANTS) {
        if (profile.is('admin') || profile.is('superadmin')) {
            Modal.alert({
                title: "You do not have access to this user",
                ok: 'Go to Dashboard',
                cancel: 'NO',
            }, function() {
                $state.go('app.admin_home');
            });
            return false;
        }

        if (!$rootScope.user.can('update.users')) {
            Modal.alert({
                title: "You don't have a required Update Users permission.",
                ok: 'Go to Dashboard',
                cancel: 'NO',
            }, function() {
                $state.go('app.admin_home');
            });
            return false;
        }

        if (!profile.isEmployee()) {
            Modal.alert({
                title: 'This user is not employee',
                ok: 'Go to employees page',
                cancel: 'NO',
            }, function() {
                $state.go('app.employees.all', {
                    page: 1
                });
            });
        }


        $rootScope.initGoogleMapSdk();

        var day_of_week = {
            0: 'Mon',
            1: 'Tue',
            2: 'Wed',
            3: 'Thu',
            4: 'Fri',
            5: 'Sat',
            6: 'Sun'
        };

        var vm = this;
        vm.profile = profile;
        console.log('profile ', profile);
        vm.isDifferentPhoneNumber = false;
        vm.searchTextChange = searchTextChange;
        vm.selectedItemChange = selectedItemChange;
        vm.newState = newState;
        vm.addInterestedJobTag = addInterestedJobTag;
        vm.nfPdfOptions = {
            resizeToWidth: 260
        };
        vm.today = new moment();
        vm.minDate = new moment('1970-01-01');
        vm.profile.birth = new moment(vm.profile.birth);
        vm.genders = [{
            value: 'male',
            display: 'Male'
        }, {
            value: 'female',
            display: 'Female'
        }];
        vm.maxBirthYear = moment().endOf("year").subtract(15, 'years').toDate();
        vm.minBirthYear = moment().startOf("year").subtract(75, 'years').toDate();
        vm.verifyIdImage = verifyIdImage;
        vm.rejectIdImage = rejectIdImage;
        vm.setStatusUser = setStatusUser;
        vm.resendVerifyEmail = resendVerifyEmail;
        vm.hasSelectedAvailability = hasSelectedAvailability;
        vm.toggleSelectDay = toggleSelectDay;
        vm.addLastPosition = addLastPosition;
        vm.updateLastPosition = updateLastPosition;
        vm.deleteLastPosition = deleteLastPosition;
        vm.addInterviewer = addInterviewer;
        vm.deleteInterviewer = deleteInterviewer;
        vm.addMoreNotAvailable = addMoreNotAvailable;
        vm.removeMoreNotAvailable = removeMoreNotAvailable;
        vm.idImageUploadCompleted = idImageUploadCompleted;
        vm.drivingLicenseImageUploadCompleted = drivingLicenseImageUploadCompleted;
        vm.searchTag = null;
        vm.selectedTag = null;
        vm.autocompleteRequireMatch = true;
        vm.transformChip = transformChip;
        vm.querySearch = querySearch;
        vm.update = update;
        vm.updateStatus = updateStatus;
        vm.generateShortenUrl = generateShortenUrl;
        vm.getPrivateNoteAndSalary = getPrivateNoteAndSalary;
        vm.getProfile = getProfile;
        vm.avatarUploaded = avatarUploaded;
        vm.changeUniversity = changeUniversity;
        vm.showMessage = showMessage;

        vm.getPrivateNoteAndSalary(profile);

        API.admin.activity.historyStatusUpdate(profile.getId())
            .then(function(activities) {
                vm.historyStatus = _.map(activities, item => _.assign(item, {
                    payload: JSON.parse(item.payload),
                }));
            }, function(error) {
                throw error;
            });

        API.admin.tag.get({
                active: 1,
                type: APP_CONSTANTS.TAG_TYPE_COMPANY,
            })
            .then((tags) => {
                vm.company_tags = tags;
                console.log(tags);
            })
            .catch((error) => {
                throw error;
            });

        if (!_.isNil(vm.profile.optional_phone_area_code) && vm.profile.optional_phone_area_code !== '' && vm.profile.optional_phone_number !== '') {
            vm.isDifferentPhoneNumber = true;
        }

        $scope.$watch(() => {
            return vm.place;
        }, () => {
            if (!_.isUndefined(vm.place) && !_.isUndefined(vm.place.components) && !_.isNil(vm.place.components.postal_code) && vm.place.components.postal_code !== '') {
                vm.profile.zipcode = vm.place.components.postal_code;
            }
        });

        API.admin.industry.get({
                include: 'interested_jobs',
            })
            .then((industries) => {
                API.admin.industry.interestedJobDuration()
                    .then(durations => {
                        API.admin.industry.getInterestedJob({
                                user_id: vm.profile.getId(),
                            })
                            .then(user_interested_job => {
                                vm.durations = durations;
                                vm.industries = _.map(industries, industry => {
                                    industry.selected = false;
                                    industry.interested_jobs = _.map(industry.interested_jobs, i => {
                                        let find = _.find(user_interested_job, d => parseInt(d.interested_job_id) === i.getId());
                                        if (!_.isUndefined(find)) {
                                            industry.selected = true;
                                            i.duration = _.find(durations, d => parseInt(find.interested_job_duration_id) === d.getId());
                                            if (!_.isNil(find.tag_ids) && find.tag_ids !== '') {
                                                let tag_ids = JSON.parse(find.tag_ids);
                                                i.tags = _.filter(vm.company_tags, tag => tag_ids.indexOf(tag.getId()) > -1);
                                            }
                                            i.selected = true;
                                        } else {
                                            i.duration = _.head(durations);
                                            i.selected = false;
                                        }
                                        return i;
                                    });
                                    return industry;
                                });
                                console.log('industries ', vm.industries);
                                console.log('user_interested_job ', user_interested_job);
                            })
                            .catch(error => {
                                throw error;
                            });
                    })
                    .catch(error => {
                        throw error;
                    });
            })
            .catch((error) => {
                throw error;
            });

        API.admin.user.getStatus()
            .then((status) => {
                vm.user_status = status;
            })
            .catch((error) => {
                throw error;
            });

        API.admin.review.analytic(vm.profile.getId())
            .then(function(response) {
                if (response.total_reviews !== 0) {
                    response.total_stars = parseInt(response.total_stars);
                    response.avg_star = Math.round(response.total_stars / response.total_reviews);
                    response.message_total_reviews = response.total_reviews === 1 ? 'review' : 'reviews';
                }
                vm.analyticReview = response;
            }, function(error) {
                throw error;
            });

        API.admin.preferred_location.get()
            .then(function(preferred_locations) {
                vm.preferred_locations = preferred_locations.map((item) => {
                    if (!_.isUndefined(vm.profile.getPreferredLocations().find(i => i.getId() === item.getId()))) {
                        item.selected = true;
                    }
                    return item;
                });
            })
            .catch(function(error) {
                throw error;
            });

        API.country.get()
            .then((countries) => {
                vm.countries = _.map(countries, (item) => {
                    item.is_supported_phone_area = item.is_supported_phone_area ? 1 : 0;
                    item.display = function() {
                        return `${this.name}(+${this.phone_area_code})`;
                    };
                    return item;
                });

                vm.profile.nationality = _.find(vm.countries, function(item) {
                    return item.name === vm.profile.nationality;
                });

                if (_.isNil(vm.profile.nationality) || vm.profile.nationality === '') {
                    vm.profile.nationality = _.find(vm.countries, (item) => item.is_default);
                }

                if (!_.isNil(vm.profile.optional_phone_area_code) && vm.profile.optional_phone_area_code !== '') {
                    vm.profile.optional_phone_area_code = _.find(vm.countries, function(item) {
                        return item.phone_area_code === vm.profile.optional_phone_area_code;
                    });
                } else {
                    vm.profile.optional_phone_area_code = _.head(vm.countries);
                }

                vm.profile.country = _.find(vm.countries, item => parseInt(item.phone_area_code) === parseInt(vm.profile.getPhoneAreaCode()));
                if (_.isUndefined(vm.profile.country)) {
                    vm.profile.country = _.find(vm.countries, (item) => item.is_default);
                }
            })
            .catch((error) => {
                throw error;
            });

        API.admin.language.get()
            .then((languages) => {
                vm.languages = languages.map((item) => {
                    if (!_.isUndefined(vm.profile.getLanguages().find(i => i.getId() === item.getId()))) {
                        item.selected = true;
                    }
                    return item;
                });
            })
            .catch((error) => {
                throw error;
            });

        API.admin.university.get()
            .then(function(universities) {
                vm.universities = universities;

                vm.profile.university = _.find(vm.universities, function(item) {
                    return item.name === vm.profile.university;
                });
                if (!_.isNil(vm.profile.university)) {
                    vm.isStudent = true;
                }
            }, function(error) {
                throw error;
            });

        API.admin.availability.get()
            .then((availabilities) => {
                API.admin.user.availabilities(profile.getId())
                    .then((user_availabilities) => {
                        availabilities = availabilities.map((item) => {
                            if (!_.isUndefined(user_availabilities.find(i => i.getId() === item.getId()))) {
                                item.selected = true;
                            }
                            return item;
                        });
                        vm.dayAvailabilities = _.map(_.groupBy(availabilities, 'day_of_week'), (availabilities, key) => {
                            return {
                                selected: !_.isUndefined(_.find(availabilities, item => item.selected)),
                                day_of_week: key,
                                availabilities: availabilities,
                                name: day_of_week[key]
                            };
                        });
                    })
                    .catch((error) => {
                        throw error;
                    });
            })
            .catch((error) => {
                throw error;
            });

        vm.id_types = [{
            label: 'Citizen / PR',
            data: 'Citizen / PR'
        }, {
            label: 'WHP, TEP, TWP',
            data: 'WHP, TEP, TWP'
        }, {
            label: 'SP, FDW',
            data: 'SP, FDW'
        }, {
            label: 'EP, PEP',
            data: 'EP, PEP'
        }, {
            label: 'DP',
            data: 'DP'
        }, {
            label: 'LTVP, LTVP+',
            data: 'LTVP, LTVP+'
        }, {
            label: 'Other',
            data: 'Other'
        }];

        if (!_.isUndefined(vm.profile.employee) && !_.isNil(vm.profile.employee.id_type) && vm.profile.employee.id_type !== '') {
            let item = _.find(vm.id_types, (i) => i.data === vm.profile.employee.id_type);
            vm.id_type = _.isUndefined(item) ? _.last(vm.id_types) : item;
        } else {
            vm.id_type = vm.id_types[0];
        }

        vm.driving_licenses = [{
            label: 'Class 3',
            data: 'Class 3',
            selected: false,
        }, {
            label: 'Class 2',
            data: 'Class 2',
            selected: false,
        }, {
            label: 'I don\'t have driving license',
            data: 'donthave',
            selected: false,
        }];

        if (!_.isUndefined(vm.profile.employee) && !_.isNil(vm.profile.employee.driving_license)) {
            if (vm.profile.employee.driving_license !== '') {
                _.map(vm.driving_licenses, (item) => {
                    if (vm.profile.employee.driving_license.indexOf(item.data) > -1 && item.data !== '') {
                        item.selected = true;
                    }
                    return item;
                });
            }
        }

        vm.vehicles = [{
            label: 'Car',
            data: 'Car',
            selected: false,
        }, {
            label: 'Motobike',
            data: 'Motobike',
            selected: false,
        }, {
            label: 'Bicycle',
            data: 'Bicycle',
            selected: false,
        }, {
            label: 'I don\'t have vehicle',
            data: 'donthave',
            selected: false,
        }];

        if (!_.isUndefined(vm.profile.employee) && !_.isNil(vm.profile.employee.vehicle)) {
            if (vm.profile.employee.vehicle !== '') {
                _.map(vm.vehicles, (item) => {
                    if (vm.profile.employee.vehicle.indexOf(item.data) > -1 && item.data !== '') {
                        item.selected = true;
                    }
                    return item;
                });
            }
        }

        API.admin.tag.get({
                active: 1
            })
            .then(function(tags) {
                vm.tags = tags.map((item) => {
                    if (!_.isUndefined(vm.profile.getTags().find(i => i.getId() === item.getId()))) {
                        item.selected = true;
                    }
                    return item;
                });
            }, function(error) {
                throw error;
            });

        API.admin.job.jobHistory({
                user_id: vm.profile.getId(),
                type: 'completed'
            })
            .then(function(completed_jobs) {
                vm.completed_jobs = completed_jobs;
            })
            .catch(function(error) {
                throw error;
            });


        API.admin.job.jobHistory({
                user_id: vm.profile.getId(),
                type: 'applied'
            })
            .then(function(applied_jobs) {
                vm.applied_jobs = applied_jobs;
            })
            .catch(function(error) {
                throw error;
            });

        API.admin.job.jobHistory({
                user_id: vm.profile.getId(),
                type: 'accepted'
            })
            .then(function(accepted_jobs) {
                vm.accepted_jobs = accepted_jobs;
            })
            .catch(function(error) {
                throw error;
            });


        API.admin.job.jobHistory({
                user_id: vm.profile.getId(),
                type: 'rejected'
            })
            .then(function(rejected_jobs) {
                vm.rejected_jobs = rejected_jobs;
            })
            .catch(function(error) {
                throw error;
            });

        API.admin.job.jobHistory({
                user_id: vm.profile.getId(),
                type: 'canceled'
            })
            .then(function(canceled_jobs) {
                vm.canceled_jobs = canceled_jobs;
            })
            .catch(function(error) {
                throw error;
            });

        if (_.isArray(vm.profile.user_not_avaiables) && vm.profile.user_not_avaiables.length > 0) {
            vm.profile.user_not_avaiables = _.map(vm.profile.user_not_avaiables, function(user_not_avaiable) {
                user_not_avaiable.start_time = moment(user_not_avaiable.getStart());
                user_not_avaiable.end_time = moment(user_not_avaiable.getEnd());
                return user_not_avaiable;
            });
        }

        API.admin.payment.getAmountReceived({
                user_id: vm.profile.getId(),
                paid: true,
            })
            .then(result => {
                console.log(result);
                vm.salaries = result.salaries;
                vm.amount_received = result.meta;
            })
            .catch(error => {
                throw error;
            });

        function changeUniversity(isStudent) {
            if (!_.isNil(vm.universities) && vm.universities.length > 0 && isStudent) {
                vm.profile.university = vm.universities[0];
            }
        }

        function showMessage(type, messsage, time) {
            Notification.show(type, messsage, time);
        }

        function searchTextChange(text) {}

        function selectedItemChange(item, interested_job) {}

        function newState(state, interested_job) {
            preloader.show();
            API.admin.tag.create({
                    name: state,
                    type: APP_CONSTANTS.TAG_TYPE_COMPANY
                })
                .then((tag) => {
                    vm.company_tags = vm.company_tags || [];
                    interested_job.tags = interested_job.tags || [];
                    vm.company_tags = _.concat(vm.company_tags, tag);
                    interested_job.tags = _.uniq(_.concat(interested_job.tags, tag));
                    interested_job.tag = undefined;
                    interested_job.searchText = undefined;
                    $('.interested-job-duration').find('input[type="search"]').focusout();
                    preloader.hide();
                })
                .catch(error => {
                    preloader.hide();
                    throw error;
                });
        }

        function addInterestedJobTag(interested_job) {
            console.log(interested_job);
            interested_job.tags = interested_job.tags || [];
            interested_job.tags = _.uniq(_.concat(interested_job.tags, interested_job.tag));
            interested_job.tag = undefined;
        }

        function getPrivateNoteAndSalary(profile) {
            if (_.isNil(profile.employee)) {
                profile.private_note = '';
                profile.salary = '';
            } else {
                profile.private_note = profile.employee.private_note;
                profile.salary = profile.employee.salary;
            }
        }

        function getProfile() {
            API.admin.user.show($stateParams.id)
                .then(function(profile) {
                    vm.profile = profile;
                    if (_.isArray(vm.profile.user_not_avaiables) && vm.profile.user_not_avaiables.length > 0) {
                        vm.profile.user_not_avaiables = _.map(vm.profile.user_not_avaiables, function(user_not_avaiable) {
                            user_not_avaiable.start_time = moment(user_not_avaiable.getStart());
                            user_not_avaiable.end_time = moment(user_not_avaiable.getEnd());
                            return user_not_avaiable;
                        });
                    }
                    vm.getPrivateNoteAndSalary(profile);
                    $scope.$watch('vm.countries', function(countries) {
                        if (!_.isUndefined(countries)) {
                            vm.profile.country = _.find(countries, item => parseInt(item.phone_area_code) === parseInt(vm.profile.getPhoneAreaCode()));
                        }
                    });

                    vm.profile.nationality = _.find(vm.countries, function(item) {
                        return item.name === vm.profile.nationality;
                    });

                    if (_.isNil(vm.profile.nationality) || vm.profile.nationality === '') {
                        vm.profile.nationality = _.find(vm.countries, (item) => item.is_default);
                    }

                    vm.profile.university = _.find(vm.universities, function(item) {
                        return item.name === vm.profile.university;
                    });

                    if (!_.isNil(vm.profile.optional_phone_area_code) && vm.profile.optional_phone_area_code !== '') {
                        vm.profile.optional_phone_area_code = _.find(vm.countries, function(item) {
                            return item.phone_area_code === vm.profile.optional_phone_area_code;
                        });
                    } else {
                        vm.profile.optional_phone_area_code = _.head(vm.countries);
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        }

        function avatarUploaded(response) {
            vm.avatar_upload_percentage = true;
            API.admin.user.updateImage(vm.profile.getId(), {
                type: 'avatar',
                url: response.data.path
            }).then(() => {
                vm.profile.setImage(response.data.path);
                vm.avatar_upload_percentage = false;
            }).catch((error) => {
                vm.avatar_upload_percentage = false;
                throw error;
            });
        }

        function verifyIdImage(user) {
            preloader.show();
            API.admin.user.verifyIdImage(user.getId(), 'verified')
                .then(function(response) {
                    vm.profile.setIdentityCheckStatus('verified');
                    preloader.hide();
                })
                .catch(function(error) {
                    preloader.hide();
                    throw error;
                });
        }

        function rejectIdImage(user) {
            var content = 'Are you sure you want to reject this id image';
            Modal.confirm({
                title: 'Warning',
                htmlContent: content,
                ok: 'YES',
                cancel: 'NO',
            }, function() {
                preloader.show();
                API.admin.user.verifyIdImage(user.getId(), 'rejected')
                    .then(function(response) {
                        vm.profile.setIdentityCheckStatus('rejected');
                        preloader.hide();
                    })
                    .catch(function(error) {
                        preloader.hide();
                        throw error;
                    });
            }, function() {});
        }

        function setStatusUser(type) {
            switch (type) {
                case 'active':
                    vm.profile.setActive(true);
                    vm.profile.setBanned(false);
                    break;
                case 'pending':
                    vm.profile.setActive(false);
                    vm.profile.setBanned(false);
                    break;
                case 'banned':
                    vm.profile.setActive(false);
                    vm.profile.setBanned(true);
                    break;
            }
        }

        function resendVerifyEmail(user) {
            preloader.show();
            API.admin.user.resendVerifyEmail(user.getId())
                .then(function(response) {
                    Notification.show('success', 'Send verify email success!', 3000);
                    preloader.hide();
                })
                .catch(function(error) {
                    preloader.hide();
                    throw error;
                });
        }

        function hasSelectedAvailability(day) {
            return !_.isUndefined(_.find(day.availabilities, item => item.selected));
        }

        function toggleSelectDay(day) {
            day.selected = !day.selected;
            day.availabilities = day.availabilities.map((item) => {
                item.selected = day.selected;
                return item;
            });
        }

        function addLastPosition() {
            vm.last_positions = vm.last_positions || [];
            vm.last_positions.push(new LastPosition());
        }

        function deleteLastPosition(index, item) {
            var user_id = vm.profile.getId();
            if (item.id === undefined || item.id === '' || item.id === null) {
                vm.last_positions.splice(index, 1);
            } else {
                API.admin.user.delete_last_position(user_id, item.id)
                    .then(function(result) {
                        vm.profile.employee.last_positions.splice(index, 1);
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        }

        function updateLastPosition(profile, last_positions) {
            if (!_.isEmpty(last_positions)) {
                var count = 1;
                _.each(last_positions, function(item) {
                    item.end_date = new moment(item.end_date).format('YYYY-MM-DD');
                    item.start_date = new moment(item.start_date).format('YYYY-MM-DD');
                    API.admin.user.add_last_position(profile.getId(), item)
                        .then(function(position) {})
                        .catch(function(error) {
                            throw error;
                        });
                });
            }
        }

        function addInterviewer() {
            vm.profile.interviewers.push(API.admin.user.createNewInterviewerInstance({
                interviewer_name: ''
            }));
        }

        function deleteInterviewer(item) {
            Modal.confirm('Are you sure you want to delete this interviewer', function() {
                _.remove(vm.profile.interviewers, (i) => _.isEqual(item, i));
            });
        }

        function idImageUploadCompleted(response) {
            API.admin.user.updateImage(vm.profile.getId(), {
                type: 'id',
                url: response.data.path
            }).then(() => {
                vm.profile.setIdImage(response.data.path);
                vm.profile.setIdentityCheckStatus('pending');
            }).catch((error) => {
                throw error;
            });
        }

        function drivingLicenseImageUploadCompleted(response) {
            API.admin.user.updateImage(vm.profile.getId(), {
                type: 'license',
                url: response.data.path
            }).then(() => {
                vm.profile.setDrivingLicenseImage(response.data.path);
            }).catch((error) => {
                throw error;
            });
        }

        function transformChip(chip) {
            if (angular.isObject(chip)) {
                return chip;
            }
            return {
                name: chip,
                type: 'new'
            };
        }

        function querySearch(query) {
            return API.admin.tag.get({
                    active: 1
                })
                .then((tags) => {
                    return tags.filter((item) => undefined === vm.profile.tags.find(i => i.getId() === item.getId()));
                })
                .catch((error) => {
                    return [];
                });
        }

        function addMoreNotAvailable() {
            vm.profile.addNotAvailable({
                start_time: '',
                end_time: '',
            });
        }

        function removeMoreNotAvailable(user_not_avaiable) {
            vm.profile.user_not_avaiables = _.pull(vm.profile.user_not_avaiables, user_not_avaiable);
        }

        vm.industryValid = false;
        vm.isSelectedIndustries = function(industries) {
            let selectedIndustries = industries.filter(item => item.selected);
            if (selectedIndustries.length === 0) {
                vm.industryValid = false;
            } else {
                _.forEach(selectedIndustries, function(selectedIndustry) {
                    if (!_.isNil(selectedIndustry.interested_jobs) && selectedIndustry.interested_jobs.length > 0) {
                        let selectedInterestedJobs = selectedIndustry.interested_jobs.filter(i => i.selected);
                        if (selectedInterestedJobs.length > 0) {
                            selectedIndustry.isValid = true;
                        } else {
                            selectedIndustry.isValid = false;
                        }
                    } else {
                        selectedIndustry.isValid = true;
                    }
                });
                let isValidArr = _.map(selectedIndustries, 'isValid');
                if (isValidArr.indexOf(false) !== -1) {
                    vm.industryValid = false;
                } else {
                    vm.industryValid = true;
                }
            }
        };
        vm.changeIndustry = function(industries) {
            vm.isSelectedIndustries(industries);
        };

        function update(profile) {
            vm.isSelectedIndustries(vm.industries);
            if (_.isUndefined(vm.industryValid) || !vm.industryValid) {
                return false;
            }
            if (!vm.isDifferentPhoneNumber) {
                delete profile.optional_phone_area_code;
                profile.optional_phone_number = '';
            }
            if (!vm.isStudent) {
                profile.university = '';
            }
            if (vm.id_type.label != 'Other') {
                if (_.isUndefined(profile.employee)) {
                    profile.employee = {};
                }
                profile.employee.id_type = vm.id_type.data;
            }
            profile.employee.driving_license = _.map(_.filter(vm.driving_licenses, (item) => item.selected), (item) => item.data).join(',');
            profile.employee.vehicle = _.map(_.filter(vm.vehicles, (item) => item.selected), (item) => item.data).join(',');

            vm.updateLastPosition(vm.profile, vm.last_positions);

            if (_.isArray(vm.industries)) {
                profile.industries = _.filter(vm.industries, item => item.selected);
            }

            if (_.isArray(vm.languages)) {
                profile.languages = _.filter(vm.languages, item => item.selected);
            }

            if (_.isArray(vm.preferred_locations)) {
                profile.preferred_locations = _.filter(vm.preferred_locations, item => item.selected);
            }

            if (!_.isNil(profile.birth) && profile.birth !== '') {
                profile.birth = new moment(profile.birth).format('YYYY-MM-DD');
            }

            var availabilities = [];
            _.forEach(vm.dayAvailabilities, (item) => {
                availabilities = _.concat(availabilities, item.availabilities);
            });
            availabilities = _.filter(availabilities, item => item.selected);
            var data = profile.getRaw();
            data.profile_completed_percent = profile.profileCompletedPercent();
            data.availabilities = _.map(availabilities, item => item.getId());

            preloader.show();
            API.admin.user.update_employee(profile.getId(), data)
                .then(function(result) {
                    preloader.hide();
                    Notification.show('success', 'Update Success', 5000);
                    vm.getProfile();
                    vm.last_positions = null;
                    vm.update_profile_form.$setPristine();
                })
                .catch(function(error) {
                    preloader.hide();
                    throw error;
                });
        }

        function updateStatus(profile) {
            preloader.show();
            API.admin.user.employeeChangeStatus(profile.getId(), {
                    status: profile.status
                })
                .then(function(result) {
                    preloader.hide();
                    Notification.show('success', 'Success', 5000);
                })
                .catch(function(error) {
                    preloader.hide();
                    throw error;
                });
        }

        function generateShortenUrl(user) {
            API.admin.user.getShortenReferralUrl(user.getId(), {
                    type: 'referral_url',
                })
                .then((result) => {
                    console.log(result);
                    vm.referral_url = result.getUrl();
                })
                .catch((error) => {
                    throw error;
                });
        }
    }

    app.filter('tagsMatcher', ['$rootScope', function($rootScope) {
        return function(tags, q) {
            if (!_.isNil(q) && q !== '') {
                return _.filter(tags, item => {
                    return item.name.toLowerCase().indexOf(q.toLowerCase()) > -1;
                });
            } else {
                return tags;
            }
        };
    }]);

})(angular.module('app.components.employee.edit', []));
