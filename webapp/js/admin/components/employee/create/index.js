(function(app) {

    app.controller('CreateEmployeeCtrl', CreateEmployeeCtrl);
    CreateEmployeeCtrl.$inject = ['$rootScope', '$scope', '$stateParams', 'API', 'Notification', 'preloader', 'Modal', '$state'];

    function CreateEmployeeCtrl($rootScope, $scope, $stateParams, API, Notification, preloader, Modal, $state) {
        if (!$rootScope.user.can('create.users')) {
            Modal.alert({
                title: "You don't have a required Create Users permission.",
                ok: 'Go to Dashboard',
                cancel: 'NO',
            }, function() {
                $state.go('app.admin_home');
            });
            return false;
        }

        var vm = this;

        vm.today = new moment();
        vm.genders = {
            male: 'male',
            female: 'female'
        };
        vm.maxBirthYear = moment().endOf("year").subtract(15, 'years').toDate();

        API.admin.industry.get()
            .then((industries) => {
                vm.industries = industries;
                vm.resetIndustries = angular.copy(vm.industries);
            })
            .catch((error) => {
                throw error;
            });

        API.country.get()
            .then((countries) => _.filter(countries, (item) => item.is_supported_phone_area))
            .then((countries) => {
                vm.countries = _.map(countries, (item) => {
                    item.display = function() {
                        return `${this.name}(+${this.phone_area_code})`;
                    };
                    return item;
                });
                vm.country = vm.countries[0];
            })
            .catch((error) => {
                throw error;
            });

        API.admin.tag.get()
            .then(function(tags) {
                vm.tags = tags;
            }, function(error) {
                throw error;
            });

        vm.user = {
            tags: []
        };

        vm.searchTag = null;
        vm.selectedTag = null;
        vm.autocompleteRequireMatch = true;
        vm.transformChip = function(chip) {
            if (angular.isObject(chip)) {
                return chip;
            }
            return {
                name: chip,
                type: 'new'
            };
        };
        vm.querySearch = function(query) {
            return API.admin.tag.get({
                    active: 1
                })
                .then((tags) => {
                    return tags.filter((item) => undefined === vm.user.tags.find(i => i.getId() === item.getId()));
                })
                .catch((error) => {
                    return [];
                });
        };

        vm.registerEmployeeProfile = function(user) {
            if (_.isNil(user.birth)) {
                return false;
            }
            user.role = 'employee';
            user.phone_area_code = vm.country.phone_area_code;
            user.country = vm.country.name;
            user.gender = user.gender === 'male' ? 0 : 1;
            preloader.show();
            API.admin.user.create(user)
                .then(function(result) {
                    API.admin.user.profileWithToken(result.token)
                        .then(function(profile) {
                            profile.birth = new moment(profile.birth).format('YYYY-MM-DD');
                            profile.industries = vm.industries.filter(item => item.selected);
                            profile.tags = user.tags;
                            profile.salary = user.salary;
                            API.admin.user.update_employee(profile.getId(), profile.getRaw())
                                .then(function(result) {
                                    preloader.hide();
                                    Notification.show('success', 'create employee success', 5000);
                                    vm.user = {
                                        tags: []
                                    };
                                    vm.country = vm.countries[0];
                                    vm.industries = angular.copy(vm.resetIndustries);
                                    vm.employee_register_form.$setPristine();
                                })
                                .catch(function(error) {
                                    preloader.hide();
                                    throw error;
                                });
                        })
                        .catch(function(error) {
                            preloader.hide();
                            throw error;
                        });
                })
                .catch(function(error) {
                    preloader.hide();
                    throw error;
                });
        };
    }

})(angular.module('app.components.employee.create', []));
