(function(app) {

    app.controller('ListEmployeeCtrl', ListEmployeeCtrl);
    ListEmployeeCtrl.$inject = ['$rootScope', '$scope', 'API', '$state', '$stateParams', 'Notification', 'Modal', 'preloader', '$mdDialog', 'user_status', 'tags', '$window', '$cookies'];

    function ListEmployeeCtrl($rootScope, $scope, API, $state, $stateParams, Notification, Modal, preloader, $mdDialog, user_status, tags, $window, $cookies) {
        if (!$rootScope.user.can('view.users')) {
            Modal.alert({
                title: "You don't have a required View Users permission.",
                ok: 'Go to Dashboard',
                cancel: 'NO',
            }, function() {
                $state.go('app.admin_home');
            });
            return false;
        }

        var vm = this;

        vm.tags = tags;
        vm.loginAsUser = loginAsUser;

        vm.params = $stateParams;

        vm.params.per_page = 100;

        vm.params.roles = 'employee';

        vm.user_status = user_status;
        vm.user_status.unshift({
            status: 'All'
        });

        vm.filterStatus = function(status) {
            if (status.status === 'All') {
                let params = $stateParams;
                if (!_.isUndefined(params.status)) {
                    delete params.status;
                }
                params.page = 1;
                $state.go($state.current, params, {
                    inherit: false,
                    reload: $state.current.name,
                });
            } else {
                $state.go($state.current, {
                    'page': 1,
                    'status': status.id
                });
            }
        };

        if (!_.isUndefined($stateParams.status)) {
            vm.status = _.find(vm.user_status, (item) => parseInt(item.id) === parseInt($stateParams.status));
        } else {
            vm.status = _.head(vm.user_status);
        }


        vm.filterPage = function(column) {
            vm.sortReverse = !vm.sortReverse;
            var order_by = {};
            order_by[column] = vm.sortReverse === true ? 'desc' : 'asc';
            $state.go($state.current, {
                'page': 1,
                'order_by': JSON.stringify(order_by)
            });
        };

        vm.searchPage = function(search) {
            $state.go($state.current, {
                page: 1,
                search: search
            }, {
                reload: true
            });
        };

        vm.searchKeyEvent = function(event, search) {
            if (event.which === 13) {
                $state.go($state.current, {
                    page: 1,
                    search: search
                }, {
                    reload: true
                });
            }
        };

        $scope.sortType = 'id';
        $scope.sortReverse = true;
        $scope.search = '';
        vm.getUsers = function(params) {
            vm.users = undefined;
            // params.includes = 'reviews';
            API.admin.user.get(params)
                .then(function(result) {
                    vm.users = result.users;
                    vm.pagination = result.pagination;
                    if (vm.users.length > 0) {
                        _.forEach(vm.users, function(user) {
                            let status = _.find(user_status, (i) => parseInt(i.id) === parseInt(user.status));
                            user.user_status = _.isUndefined(status) ? '' : status.status;
                            if (!_.isUndefined(user.employee) && user.employee.private_note !== '') {
                                user.hasPrivateNote = true;
                            } else {
                                user.hasPrivateNote = false;
                            }
                            if (!_.isUndefined(user.employee) && user.employee.salary !== '') {
                                user.hasSalary = true;
                            } else {
                                user.hasSalary = false;
                            }
                        });
                    }
                    console.log("users ", vm.users);
                })
                .catch(function(error) {
                    throw error;
                });
        };
        vm.getUsers(vm.params);

        vm.updateSelectedUser = function() {
            vm.selectedUser = _.filter(vm.users, {
                selected: true
            });
        };
        vm.selectAll = function() {
            $scope.$watch('vm.users', function(users) {
                if (!_.isUndefined(users)) {
                    _.forEach(users, function(user) {
                        if (vm.selectedAll) {
                            user.selected = true;
                        } else {
                            user.selected = false;
                        }
                    });
                    vm.selectedUser = _.filter(users, {
                        selected: true
                    });
                }
            });
        };

        /**
         * active, pending or banned
         * @param String type ('active', 'pending', 'banned')
         */
        vm.changeStatusAllUser = function(type) {
            vm.selectedUser = _.filter(vm.users, {
                selected: true
            });
            if (vm.selectedUser.length === 0) {
                Notification.show('warning', 'Please select at least one user', 5000);
                return false;
            } else {
                preloader.show();
                var user_ids = _.map(vm.selectedUser, 'id');
                API.admin.user.changeStatusAllUser(user_ids, type)
                    .then(function(response) {
                        Notification.show('success', 'Update user success!', 5000);
                        vm.getUsers(vm.params);
                        vm.selectedAll = false;
                        vm.selectAll();
                        preloader.hide();
                    })
                    .catch(function(error) {
                        preloader.hide();
                        throw error;
                    });
            }
        };

        vm.verifyIdStatus = function(type) {
            vm.selectedUser = _.filter(vm.users, {
                'selected': true
            });
            if (vm.selectedUser.length === 0) {
                Notification.show('warning', 'Please select at least one user', 5000);
                return false;
            } else {
                preloader.show();
                vm.verify_number = 0;
                _.forEach(vm.selectedUser, function(user) {
                    API.admin.user.verifyIdImage(user.getId(), type)
                        .then(function(response) {
                            vm.verify_number++;
                        })
                        .catch(function(error) {
                            preloader.hide();
                            throw error;
                        });
                });
                $scope.$watch('vm.verify_number', function(verify_number) {
                    if (verify_number === vm.selectedUser.length) {
                        Notification.show('success', 'Verify ID success!', 5000);
                        vm.getUsers(vm.params);
                        vm.selectedAll = false;
                        vm.selectAll();
                        preloader.hide();
                    }
                });
            }
        };

        vm.resendVerifyEmail = function() {
            vm.selectedUser = _.filter(vm.users, {
                'selected': true
            });
            if (vm.selectedUser.length === 0) {
                Notification.show('warning', 'Please select at least one user', 5000);
                return false;
            } else {
                preloader.show();
                vm.resend_verify_email_number = 0;
                _.forEach(vm.selectedUser, function(user) {
                    API.admin.user.resendVerifyEmail(user.getId())
                        .then(function(response) {
                            vm.resend_verify_email_number++;
                        })
                        .catch(function(error) {
                            preloader.hide();
                            throw error;
                        });
                });
                $scope.$watch('vm.resend_verify_email_number', function(resend_verify_email_number) {
                    if (resend_verify_email_number === vm.selectedUser.length) {
                        Notification.show('success', 'Send verify email success!', 5000);
                        vm.getUsers(vm.params);
                        vm.selectedAll = false;
                        vm.selectAll();
                        preloader.hide();
                    }
                });
            }
        };

        vm.editUser = function(id) {
            API.admin.user.show(id, ['roles'])
                .then(function(response) {
                    vm.user = response;
                    if (!vm.user.isEmployer() && !vm.user.isEmployee()) {
                        Notification.show('warning', 'You do not have permission to edit this user!', 5000);
                        return false;
                    } else {
                        $state.go('app.user.edit', {
                            'id': id
                        });
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        };

        vm.deleteUser = function(user) {
            API.admin.user.show(user.id, ['roles'])
                .then(function(response) {
                    vm.user = response;
                    if (!vm.user.isEmployer() && !vm.user.isEmployee()) {
                        Notification.show('warning', 'You do not have permission to delete this user!', 5000);
                        return false;
                    } else {
                        var content = 'Are you sure you want to remove ' + user.getEmail();
                        Modal.confirm({
                            title: 'Warning',
                            textContent: content,
                            ok: 'YES',
                            cancel: 'NO',
                        }, function() {
                            API.admin.user.delete(user.id)
                                .then(function(response) {
                                    $state.reload($state.current.name);
                                })
                                .catch(function(error) {
                                    throw error;
                                });
                        }, function() {});
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        };

        if (!_.isUndefined($stateParams.tag)) {
            var tagsParam = $stateParams.tag.split(',');
            var v = _.filter(vm.tags, function(item) {
                var data = _.find(tagsParam, function(tag) {
                    return parseInt(tag) === item.id;
                });
                return _.isUndefined(data) ? false : true;
            });
            vm.tag = v;
        }
        vm.changeTag = function(tags) {
            var tag_ids = _.map(tags, 'id').join(',');
            $state.go($state.current, {
                page: 1,
                tag: tag_ids
            });
        };

        $scope.$on('reloadEmployeePage', function(event, args) {
            vm.getUsers(vm.params);
        });

        vm.addPrivateNote = function(ev, user) {
            $mdDialog.show({
                    controller: PrivateNoteCtrl,
                    templateUrl: '/webapp/js/admin/components/employee/list/_private_note.tpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    fullscreen: $scope.customFullscreen,
                    locals: {
                        user: user
                    },
                })
                .then(function() {}, function() {});
        };

        PrivateNoteCtrl.$inject = ['$scope', '$mdDialog', 'user', 'Notification', 'API'];

        function PrivateNoteCtrl($scope, $mdDialog, user, Notification, API) {
            $scope.user = user;
            if (!user.hasPrivateNote) {
                $scope.private_note = '';
            } else {
                $scope.private_note = user.employee.private_note;
            }

            $scope.cancel = function() {
                $mdDialog.cancel();
            };

            $scope.privateNoteEmployee = function(private_note) {
                $scope.inProgress = true;
                API.admin.user.addPrivateNoteEmployee(user.getId(), private_note)
                    .then(function(response) {
                        $scope.inProgress = false;
                        $scope.private_note = null;
                        $scope.privateNoteEmployeeForm.$setPristine();
                        $mdDialog.cancel();
                        Notification.show('success', 'add private note success', 5000);
                        $rootScope.$broadcast('reloadEmployeePage', true);
                    })
                    .catch(function(error) {
                        $scope.inProgress = false;
                        throw error;
                    });
            };
        }

        vm.addSalary = function(ev, user) {
            $mdDialog.show({
                    controller: EmployeeSalaryCtrl,
                    templateUrl: '/webapp/js/admin/components/employee/list/_salary.tpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    fullscreen: $scope.customFullscreen,
                    locals: {
                        user: user
                    },
                })
                .then(function() {}, function() {});
        };

        EmployeeSalaryCtrl.$inject = ['$scope', '$mdDialog', 'user', 'Notification', 'API'];

        function EmployeeSalaryCtrl($scope, $mdDialog, user, Notification, API) {
            $scope.user = user;
            if (!user.hasSalary) {
                $scope.salary = '';
            } else {
                $scope.salary = user.employee.salary;
            }

            $scope.cancel = function() {
                $mdDialog.cancel();
            };

            $scope.EmployeeSalary = function(salary) {
                $scope.inProgress = true;
                API.admin.user.addSalaryEmployee(user.getId(), salary)
                    .then(function(response) {
                        $scope.inProgress = false;
                        $scope.salary = null;
                        $scope.EmployeeSalaryForm.$setPristine();
                        $mdDialog.cancel();
                        Notification.show('success', 'add salary success', 5000);
                        $rootScope.$broadcast('reloadEmployeePage', true);
                    })
                    .catch(function(error) {
                        $scope.inProgress = false;
                        throw error;
                    });
            };
        }

        function loginAsUser(user_id) {
            var redirectWindow = $window.open('', '_blank');
            preloader.show();
            API.admin.user.loginAsUser({
                    id: user_id
                })
                .then(function(response) {
                    preloader.hide();
                    $cookies.remove(USER_JWT_TOKEN_COOKIE_KEY);
                    $cookies.remove(USER_JWT_TOKEN_COOKIE_KEY, {
                        path: '/'
                    });
                    console.log('Cookies removed');
                    $cookies.put(USER_JWT_TOKEN_COOKIE_KEY, `"${response.token}"`, {
                        path: '/',
                    });
                    $cookies.put(ADMIN_SESSION, `${response.admin_session}`, {
                        path: '/',
                    });
                    $cookies.put(ADMIN_PROFILE, `${response.admin_profile}`, {
                        path: '/',
                    });
                    redirectWindow.location = '/dashboard/jobs/all';
                })
                .catch(function(error) {
                    throw error;
                });
        }
    }

})(angular.module('app.components.employee.list', []));
