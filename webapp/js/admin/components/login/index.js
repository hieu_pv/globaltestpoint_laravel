require('./router');

let {
    USER_LOGGED_IN,
    FETCH_LOGGED_IN_USER_SUCCESSED,
} = require('./action');

(function(app) {

    app.controller('LoginCtrl', LoginCtrl);
    LoginCtrl.$inject = ['$rootScope', '$state', '$stateParams', '$cookies', 'store', 'API', 'Notification', 'Modal'];

    function LoginCtrl($rootScope, $state, $stateParams, $cookies, store, API, Notification, Modal) {
        var vm = this;
        vm.store = store;

        $rootScope.title = 'Login';
        if (!_.isNil($cookies.get(JWT_TOKEN_COOKIE_KEY))) {
            API.admin.user.profile()
                .then(function(user) {
                    vm.store.dispatch({
                        type: FETCH_LOGGED_IN_USER_SUCCESSED,
                        data: user
                    });
                    if (user.isAdmin()) {
                        var content = 'You have logged in as ' + user.getName();
                        Modal.confirm({
                            title: 'You are already logged in',
                            textContent: content,
                            ok: 'LOG OUT',
                            cancel: 'GO TO DASHBOARD',
                        }, function() {
                            $rootScope.logout();
                        }, function() {
                            $state.go('app.jobs.all');
                        });
                    } else {
                        $cookies.remove(JWT_TOKEN_COOKIE_KEY, {
                            path: '/'
                        });
                    }
                })
                .catch(function(error) {
                    $cookies.remove(JWT_TOKEN_COOKIE_KEY, {
                        path: '/'
                    });
                });
        }

        vm.login = function(user) {
            vm.inProgress = true;
            API.auth.login(user.email, user.password)
                .then(function(result) {
                    $cookies.put(JWT_TOKEN_COOKIE_KEY, `"${result.token}"`, {
                        path: '/',
                    });
                    vm.inProgress = false;
                    API.admin.user.profile()
                        .then(function(user) {
                            vm.store.dispatch({
                                type: FETCH_LOGGED_IN_USER_SUCCESSED,
                                data: user
                            });
                            $rootScope.user = user;
                            $rootScope.isLoggedIn = true;
                            if (_.isUndefined($stateParams.redirect)) {
                                if (!user.isEmployer() && !user.isEmployee()) {
                                    $state.go('app.jobs.all');
                                } else {
                                    window.location.href = '/';
                                }
                            } else {
                                window.location.href = $stateParams.redirect;
                            }
                        })
                        .catch(function(error) {
                            throw error;
                        });
                })
                .catch(function(error) {
                    vm.inProgress = false;
                    throw (error);
                });
        };
    }

})(angular.module('app.components.login', ['app.components.login.router']));
