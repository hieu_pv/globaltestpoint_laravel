(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('user.login', {
                url: '/admin/login?redirect',
                views: {
                    'content@user': {
                        templateUrl: "/webapp/js/admin/components/login/login.tpl.html",
                        controller: 'LoginCtrl',
                        controllerAs: 'vm',
                    }
                }
            });
    }]);
})(angular.module('app.components.login.router', []));
