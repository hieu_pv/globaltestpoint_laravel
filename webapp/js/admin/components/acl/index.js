require('./router');
require('./list/');
require('./roles/');
(function(app) {
    app.controller('AdminRoleAndPermissionCtrl', AdminRoleAndPermissionCtrl);

    AdminRoleAndPermissionCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API'];

    function AdminRoleAndPermissionCtrl($rootScope, $scope, $state, $stateParams, API) {
        $rootScope.title = 'Roles and Permissions Management';
        if ($state.current.name === 'app.acl') {
            $state.go('app.acl.list');
        }
    }

    app.directive('materialTabs', ['$rootScope', function($rootScope) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                setTimeout(function() {
                    $(element).tabs();
                }, 500);
            }
        };
    }]);
})(angular.module('app.components.acl', [
    'app.components.acl.router',
    'app.components.acl.list',
    'app.components.acl.roles',
]));
