(function(app) {

    app.controller('AdminRoleAndPermissonRolesCtrl', AdminRoleAndPermissonRolesCtrl);
    AdminRoleAndPermissonRolesCtrl.$inject = ['$rootScope', '$scope', 'API', '$state', '$stateParams', '$filter', 'Notification', 'Modal', 'roles', 'permissions', '$mdDialog', '$window'];

    function AdminRoleAndPermissonRolesCtrl($rootScope, $scope, API, $state, $stateParams, $filter, Notification, Modal, roles, permissions, $mdDialog, $window) {
        var vm = this;
        vm.roles = roles;
        vm.permissions = permissions;

        var originatorEv;
        vm.openMenu = function($mdOpenMenu, ev) {
            originatorEv = ev;
            $mdOpenMenu(ev);
        };

        vm.addPermission = function(role, permission) {
            role.addPermission(permission);
        };

        vm.removePermission = function(role, permission) {
            role.removePermission(permission);
        };

        vm.save = function(roles) {
            vm.inProgress = true;
            var data = roles.map(function(item) {
                return {
                    role_id: item.getId(),
                    permissions: item.permissions.map(i => i.getId())
                };
            });
            API.admin.role.savePermissions(data)
                .then(function(response) {
                    $window.location.reload();
                    // $state.reload('app.acl');
                    // $state.reload($state.current.name);
                    Notification.show('success', 'Updated', 3000);
                    vm.inProgress = false;
                })
                .catch(function(error) {
                    vm.inProgress = false;
                    throw error;
                });
        };

        vm.deleteRole = function(role) {
            var content = 'Are you sure you want to remove <strong>' + role.getName() + '</strong>';
            Modal.confirm({
                title: 'Warning',
                htmlContent: content,
                ok: 'YES',
                cancel: 'NO',
            }, function() {
                API.admin.role.delete(role.getId())
                    .then(function(response) {
                        _.remove(vm.roles, role);
                        Notification.show('success', 'Deleted', 3000);
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }, function() {});
        };

        $scope.$on('reloadRoleData', function(event, args) {
            API.admin.role.get()
                .then(function(response) {
                    vm.roles = response;
                })
                .catch(function(error) {
                    throw error;
                });
        });

        $scope.createRole = function(ev) {
            $mdDialog.show({
                    controller: CreateRoleController,
                    templateUrl: '/webapp/js/admin/components/acl/roles/_create.tpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    fullscreen: $scope.customFullscreen
                })
                .then(function(answer) {}, function() {});
        };

        CreateRoleController.$inject = ['$scope', '$mdDialog', 'Notification', 'API'];

        function CreateRoleController($scope, $mdDialog, Notification, API) {
            $scope.cancel = function() {
                $scope.createRoleForm.$setPristine();
                $scope.role = null;
                $mdDialog.cancel();
            };
            $scope.createRole = function(role) {
                $scope.inProgress = true;
                API.admin.role.create(role)
                    .then(function(response) {
                        $scope.inProgress = false;
                        $scope.role = null;
                        $scope.createRoleForm.$setPristine();
                        $mdDialog.cancel();
                        Notification.show('success', 'role create success', 3000);
                        $rootScope.$broadcast('reloadRoleData', true);
                    })
                    .catch(function(error) {
                        $scope.inProgress = false;
                        $mdDialog.cancel();
                        throw error;
                    });
            };
            $scope.changeRole = function() {
                $scope.messageType = undefined;
            };
        }

        $scope.editRole = function(ev, role) {
            $mdDialog.show({
                    controller: EditRoleController,
                    templateUrl: '/webapp/js/admin/components/acl/roles/_edit.tpl.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    fullscreen: $scope.customFullscreen,
                    locals: {
                        role: role
                    },
                })
                .then(function(answer) {}, function() {});
        };

        EditRoleController.$inject = ['$scope', '$mdDialog', 'role', 'Notification', 'API'];

        function EditRoleController($scope, $mdDialog, role, Notification, API) {
            $scope.role = angular.copy(role);
            $scope.cancel = function() {
                $scope.editRoleForm.$setPristine();
                $scope.role = angular.copy(role);
                $mdDialog.cancel();
            };
            $scope.editRole = function(role) {
                $scope.inProgress = true;
                API.admin.role.update(role)
                    .then(function(response) {
                        $scope.inProgress = false;
                        $scope.role = null;
                        $scope.editRoleForm.$setPristine();
                        $mdDialog.cancel();
                        Notification.show('success', 'role update success', 3000);
                        $rootScope.$broadcast('reloadRoleData', true);
                    })
                    .catch(function(error) {
                        $scope.inProgress = false;
                        $mdDialog.hide();
                        throw error;
                    });
            };
        }
    }

    app.filter('availablePermissions', function() {
        return function(permissions, role) {
            return permissions.filter(item => _.isUndefined(_.find(role.getPermissions(), i => i.getId() === item.getId())));
        };
    });

})(angular.module('app.components.acl.roles', []));
