(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.acl', {
                url: '/acl',
                views: {
                    'content@app': {
                        templateUrl: '/webapp/js/admin/components/acl/_acl.tpl.html',
                        controller: 'AdminRoleAndPermissionCtrl',
                        controllerAs: 'vm',
                    }
                },
                resolve: {
                    roles: ['API', function(API) {
                        return API.admin.role.get();
                    }]
                }
            })
            .state('app.acl.list', {
                url: '/users',
                views: {
                    'acl-admin@app.acl': {
                        templateUrl: "/webapp/js/admin/components/acl/list/_list.tpl.html",
                        controller: 'AdminRoleAndPermissonUsersCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.acl.roles', {
                url: '/roles',
                views: {
                    'acl-admin@app.acl': {
                        templateUrl: "/webapp/js/admin/components/acl/roles/_acl_roles.tpl.html",
                        controller: 'AdminRoleAndPermissonRolesCtrl',
                        controllerAs: 'vm',
                    }
                },
                resolve: {
                    permissions: ['API', function(API) {
                        return API.admin.permission.get();
                    }]
                }
            });
    }]);

})(angular.module('app.components.acl.router', []));
