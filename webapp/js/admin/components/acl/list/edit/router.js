(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.acl.list.edit', {
                url: '/:id/edit',
                views: {
                    'acl-admin@app.acl': {
                        templateUrl: "/webapp/js/admin/components/acl/list/edit/_edit.tpl.html",
                        controller: 'AdminEditACLCtrl',
                        controllerAs: 'vm',
                    }
                },
                resolve: {
                    user: ['API', '$stateParams', function(API, $stateParams) {
                        return API.admin.user.show($stateParams.id);
                    }],
                    roles: ['API', function(API) {
                        return API.admin.role.get();
                    }]
                }
            });
    }]);
})(angular.module('app.components.acl.list.edit.router', []));
