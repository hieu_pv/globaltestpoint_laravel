require('./router');
(function(app) {

    app.controller('AdminEditACLCtrl', AdminEditACLCtrl);
    AdminEditACLCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'Notification', 'user', 'roles', 'API', 'Modal', 'preloader'];

    function AdminEditACLCtrl($rootScope, $scope, $state, $stateParams, Notification, user, roles, API, Modal, preloader) {
        if (!$rootScope.user.can('update.admins')) {
            Modal.alert({
                title: "You don't have a required Update Admins permission.",
                ok: 'Go to Dashboard',
                cancel: 'NO',
            }, function() {
                $state.go('app.admin_home');
            });
            return false;
        }

        var vm = this;
        vm.user = user;
        vm.roles = roles;

        if (!_.isNil(vm.user.roles) && vm.user.roles.length > 0 && vm.roles.length > 0) {
            _.forEach(vm.user.roles, function(user_role) {
                vm.role = _.find(vm.roles, function(role) {
                    return role.slug === user_role.slug;
                });
            });
        }

        vm.editUser = function(user) {
            preloader.show();
            var data = {
                role: vm.role.slug,
                first_name: user.first_name,
                last_name: user.last_name,
                email: user.email
            };
            API.admin.user.update_admin(user.getId(), data)
                .then(function(response) {
                    Notification.show('success', 'Update success', 5000);
                    preloader.hide();
                }, function(error) {
                    preloader.hide();
                    throw error;
                });
        };
    }

})(angular.module('app.components.acl.list.edit', [
    'app.components.acl.list.edit.router'
]));
