require('./router');
(function(app) {

    app.controller('AdminCreateACLCtrl', AdminCreateACLCtrl);
    AdminCreateACLCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'Notification', 'roles', 'API', 'preloader'];

    function AdminCreateACLCtrl($rootScope, $scope, $state, $stateParams, Notification, roles, API, preloader) {
        if (!$rootScope.user.can('create.admins')) {
            Modal.alert({
                title: "You don't have a required Create Admins permission.",
                ok: 'Go to Dashboard',
                cancel: 'NO',
            }, function() {
                $state.go('app.admin_home');
            });
            return false;
        }

        var vm = this;
        vm.roles = roles;

        vm.createUser = function() {
            preloader.show();
            vm.user = vm.user || {};
            vm.user.role = vm.role.slug;
            API.admin.user.createAdmin(vm.user)
                .then(function(result) {
                    preloader.hide();
                    vm.user = null;
                    vm.role = null;
                    vm.createUserForm.$setPristine();
                    Notification.show('success', 'New user is added successfully', 3000);
                })
                .catch(function(error) {
                    preloader.hide();
                    throw error;
                });
        };
    }

})(angular.module('app.components.acl.list.create', [
    'app.components.acl.list.create.router'
]));
