(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.acl.list.create', {
                url: '/create',
                views: {
                    'acl-admin@app.acl': {
                        templateUrl: "/webapp/js/admin/components/acl/list/create/_create.tpl.html",
                        controller: 'AdminCreateACLCtrl',
                        controllerAs: 'vm',
                    }
                },
                resolve: {
                    roles: ['API', function(API) {
                        return API.admin.role.get();
                    }]
                }
            });
    }]);
})(angular.module('app.components.acl.list.create.router', []));
