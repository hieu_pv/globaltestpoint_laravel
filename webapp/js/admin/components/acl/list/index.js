require('./create/');
require('./edit/');
(function(app) {

    app.controller('AdminRoleAndPermissonUsersCtrl', AdminRoleAndPermissonUsersCtrl);
    AdminRoleAndPermissonUsersCtrl.$inject = ['$rootScope', '$scope', 'API', '$state', '$stateParams', '$filter', 'Notification', 'Modal', 'roles'];

    function AdminRoleAndPermissonUsersCtrl($rootScope, $scope, API, $state, $stateParams, $filter, Notification, Modal, roles) {
        var vm = this;
        vm.roles = roles;
        vm.page = $stateParams.page ? parseInt($stateParams.page) : 1;

        console.log(roles);

        var params = {
            page: vm.page,
            roles_excerpt: 'superadmin,employer,employee',
            order_by: {
                id: 'desc'
            }
        };

        API.admin.user.get(params)
            .then(function(response) {
                vm.users = _.map(response.users, function(user) {
                    user.selected_role = _.find(vm.roles, role => role.getId() === _.head(user.getRoles()).getId());
                    return user;
                });
                vm.pagination = response.pagination;
                console.log(vm.users);
                console.log(vm.pagination);
            })
            .catch(function(error) {
                throw error;
            });

        vm.save = function(users) {
            vm.inProgress = true;
            var params = users.filter(item => $filter('roleChanged')(item)).map(function(item) {
                return {
                    user_id: item.getId(),
                    role_id: item.selected_role.getId()
                };
            });
            API.admin.user.updateRole(params)
                .then(function(response) {
                    vm.inProgress = false;
                    $state.reload('app.acl');
                    $state.reload($state.current.name);
                    Notification.show('success', 'Success', 3000);
                })
                .catch(function(error) {
                    vm.inProgress = false;
                    throw error;
                });
        };

        vm.deleteUser = function(user) {
            var content = 'Are you sure you want to remove <strong>' + user.getEmail() + '</strong>';
            Modal.confirm({
                title: 'Warning',
                htmlContent: content,
                ok: 'YES',
                cancel: 'NO',
            }, function() {
                API.admin.user.deleteAdmin(user.id)
                    .then(function(response) {
                        $state.reload('app.acl');
                        $state.reload($state.current.name);
                        Notification.show('success', 'Deleted', 3000);
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }, function() {});
        };
    }

    app.filter('roleChanged', function() {
        return function(user) {
            return _.isUndefined(_.find(user.getRoles(), item => item.getId() === user.selected_role.getId()));
        };
    });
    app.filter('hasUpdateRole', ['$filter', function($filter) {
        return function(users) {
            if (_.isUndefined(users)) {
                return false;
            } else {
                return users.filter(item => $filter('roleChanged')(item)).length > 0;
            }
        };
    }]);

})(angular.module('app.components.acl.list', [
    'app.components.acl.list.create',
    'app.components.acl.list.edit'
]));
