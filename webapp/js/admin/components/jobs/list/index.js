require('./router');
(function(app) {
    app.controller('AdminJobsListCtrl', AdminJobsListCtrl);
    AdminJobsListCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API', 'Modal', 'Notification', 'preloader', 'tags'];

    function AdminJobsListCtrl($rootScope, $scope, $state, $stateParams, API, Modal, Notification, preloader, tags) {
        var vm = this;
        vm.tags = tags;

        vm.params = $stateParams;

        vm.params.per_page = 100;
        vm.params.includes = 'employer,tags';

        vm.getJobs = getJobs;
        vm.searchPage = searchPage;
        vm.searchKeyEvent = searchKeyEvent;
        vm.filterStatus = filterStatus;
        vm.changeStatusAllItem = changeStatusAllItem;
        vm.updateSelectedItem = updateSelectedItem;
        vm.selectAll = selectAll;
        vm.deleteItem = deleteItem;

        vm.statuses = [{
            value: null,
            name: 'All'
        }, {
            value: 1,
            name: 'Active'
        }, {
            value: 0,
            name: 'Pending'
        }];
        vm.status = _.head(vm.statuses);
        if (!_.isNil($stateParams.constraints)) {
            var constraints = JSON.parse($stateParams.constraints);
            if (!_.isNil(constraints.status)) {
                vm.status = _.find(vm.statuses, {
                    value: constraints.status
                });
            } else {
                vm.status = _.head(vm.statuses);
            }
        }

        vm.getJobs(vm.params);

        function getJobs(params) {
            API.admin.job.get(params)
                .then(function(response) {
                    vm.jobs = response.jobs;
                    vm.pagination = response.pagination;
                })
                .catch(function(error) {
                    throw error;
                });
        }

        function searchPage(search) {
            $state.go($state.current, {
                page: 1,
                search: search
            }, {
                reload: true
            });
        }

        function searchKeyEvent(event, search) {
            if (event.which === 13) {
                searchPage(search);
            }
        }

        function filterStatus(status) {
            var constraints = {};

            if (_.isNil(status.value)) {
                delete constraints.status;
            } else {
                constraints.status = status.value;
            }
            $state.go($state.current, {
                page: 1,
                constraints: JSON.stringify(constraints)
            });
        }

        function updateSelectedItem() {
            vm.selectedItem = _.filter(vm.jobs, {
                selected: true
            });
        }

        function selectAll() {
            $scope.$watch('vm.jobs', function(jobs) {
                if (!_.isUndefined(jobs)) {
                    _.forEach(jobs, function(item) {
                        if (vm.selectedAll) {
                            item.selected = true;
                        } else {
                            item.selected = false;
                        }
                    });
                    vm.selectedItem = _.filter(jobs, {
                        selected: true
                    });
                }
            });
        }

        function deleteItem(item) {
            var content = 'Are you sure you want to remove ' + item.getTitle();
            Modal.confirm({
                title: 'Warning',
                content: content,
                ok: 'YES',
                cancel: 'NO',
            }, function() {
                preloader.show();
                API.admin.job.delete(item.getId())
                    .then(function(response) {
                        _.remove(vm.jobs, item);
                        preloader.hide();
                    })
                    .catch(function(error) {
                        preloader.hide();
                        throw error;
                    });
            }, function() {});
        }

        function changeStatusAllItem(type) {
            vm.selectedItem = _.filter(vm.jobs, {
                selected: true
            });
            if (vm.selectedItem.length === 0) {
                Notification.show('warning', 'Please select at least one item', 5000);
                return false;
            } else {
                preloader.show();
                var item_ids = _.map(vm.selectedItem, 'id');
                API.admin.job.changeStatusAllItem(item_ids, type)
                    .then(function(response) {
                        Notification.show('success', 'Change status success!', 5000);
                        vm.getJobs(vm.params);
                        vm.selectedAll = false;
                        vm.selectAll();
                        preloader.hide();
                    })
                    .catch(function(error) {
                        preloader.hide();
                        throw error;
                    });
            }
        }

    }
})(angular.module('app.components.jobs.list', [
    'app.components.jobs.list.router',
]));