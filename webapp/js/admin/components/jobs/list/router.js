(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.jobs.all', {
                url: '/all?page&tag&status&list&search&order_by&job_filter',
                views: {
                    'admin-jobs@app.jobs': {
                        templateUrl: "/webapp/js/admin/components/jobs/list/_list.tpl.html",
                        controller: 'AdminJobsListCtrl',
                        controllerAs: 'vm',
                    }
                },
                resolve: {
                    user_status: ['API', function(API) {
                        return API.admin.user.getStatus();
                    }],
                    tags: ['API', function(API) {
                        return API.admin.tag.get({
                                active: 1
                            })
                            .then(function(tags) {
                                return tags;
                            })
                            .catch(function(error) {
                                return [];
                            });

                    }],
                }
            });
    }]);
})(angular.module('app.components.jobs.list.router', []));
