(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.jobs', {
                url: '/jobs',
                views: {
                    'content@app': {
                        templateUrl: "/webapp/js/admin/components/jobs/_admin_jobs.tpl.html",
                        controller: 'AdminJobsCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.jobs.edit', {
                url: '/:id/edit',
                views: {
                    'admin-jobs@app.jobs': {
                        templateUrl: "/webapp/js/admin/components/jobs/_admin_jobs.tpl.html",
                        controller: 'AdminJobsCtrl',
                        controllerAs: 'vm',
                    }
                }
            });
    }]);
})(angular.module('app.components.jobs.router', []));
