let {
    call,
    put,
    take,
    takeEvery,
    takeLatest,
    select,
    fork
} = require('redux-saga/effects');

require('./create/sagas');
require('./update/sagas');

let {
    FETCH_JOB_TITLE_REQUESTED,
    FETCH_JOB_TITLE_SUCCESSED,

    FETCH_EXPERIENCE_REQUESTED,
    FETCH_EXPERIENCE_SUCCESSED,

    FETCH_NATIONALITY_REQUESTED,
    FETCH_NATIONALITY_SUCCESSED,

    FETCH_QUALIFICATION_REQUESTED,
    FETCH_QUALIFICATION_SUCCESSED,

    FETCH_COUNTRY_REQUESTED,
    FETCH_COUNTRY_SUCCESSED,

    FETCH_STATE_REQUESTED,
    FETCH_STATE_SUCCESSED,

    FETCH_CITY_REQUESTED,
    FETCH_CITY_SUCCESSED,

    FETCH_INDUSTRY_REQUESTED,
    FETCH_INDUSTRY_SUCCESSED,

    FETCH_TAG_REQUESTED,
    FETCH_TAG_SUCCESSED,
    SELECT_STATE,
} = require('./action');

(function(app) {
    app.factory('JobSagas', JobSagas);
    JobSagas.$inject = ['$state', 'API', 'Notification', 'preloader', 'PostJobSagas', 'UpdateJobSagas'];

    function JobSagas($state, API, Notification, preloader, PostJobSagas, UpdateJobSagas) {

        function* fetchJobTitle(action) {
            try {
                let request = yield API.admin.job_title.getAll();
                yield put({
                    type: FETCH_JOB_TITLE_SUCCESSED,
                    data: request,
                });
            } catch (error) {
                yield put({
                    type: 'API_CALL_ERROR',
                    error: error,
                });
            }
        }

        function* watchFetchJobTitle() {
            yield takeLatest(FETCH_JOB_TITLE_REQUESTED, fetchJobTitle);
        }

        function* fetchExperience(action) {
            try {
                let request = yield API.admin.experience.getAll();
                yield put({
                    type: FETCH_EXPERIENCE_SUCCESSED,
                    data: request
                });
            } catch (error) {
                yield put({
                    type: 'API_CALL_ERROR',
                    error: error
                });
            }
        }

        function* watchFetchExperience() {
            yield takeLatest(FETCH_EXPERIENCE_REQUESTED, fetchExperience);
        }

        function* fetchNationality(action) {
            try {
                let request = yield API.admin.nationality.getAll();
                yield put({
                    type: FETCH_NATIONALITY_SUCCESSED,
                    data: request
                });
            } catch (error) {
                yield put({
                    type: 'API_CALL_ERROR',
                    error: error
                });
            }
        }

        function* watchFetchNationality() {
            yield takeLatest(FETCH_NATIONALITY_REQUESTED, fetchNationality);
        }

        function* fetchQualification(action) {
            try {
                let request = yield API.admin.qualification.getAll();
                yield put({
                    type: FETCH_QUALIFICATION_SUCCESSED,
                    data: request
                });
            } catch (error) {
                yield put({
                    type: 'API_CALL_ERROR',
                    error: error
                });
            }
        }

        function* watchFetchQualification() {
            yield takeLatest(FETCH_QUALIFICATION_REQUESTED, fetchQualification);
        }

        function* fetchCountry(action) {
            try {
                let request = yield API.admin.country.getAll();
                yield put({
                    type: FETCH_COUNTRY_SUCCESSED,
                    data: request
                });
            } catch (error) {
                yield put({
                    type: 'API_CALL_ERROR',
                    error: error
                });
            }
        }

        function* watchFetchCountry() {
            yield takeLatest(FETCH_COUNTRY_REQUESTED, fetchCountry);
        }

        function* fetchState(action) {
            try {
                let country_id = action.data;
                let request = yield API.admin.country.getStates(country_id);
                yield put({
                    type: FETCH_STATE_SUCCESSED,
                    data: request
                });
            } catch (error) {
                yield put({
                    type: 'API_CALL_ERROR',
                    error: error
                });
            }
        }

        function* watchFetchState() {
            yield takeLatest(FETCH_STATE_REQUESTED, fetchState);
        }

        function* fetchSelectState(action) {
            let job = yield select(state => state.UpdateJob);
            if(!_.isUndefined(job.state_id)) {
                yield put({
                    type: SELECT_STATE,
                    data: _.find(action.data, item => item.getId() === job.state_id),
                });
            }
        }

        function* watchFetchSelectState() {
            yield takeLatest(FETCH_STATE_SUCCESSED, onSelectJobTitle);
        }

        function* fetchCity(action) {
            try {
                let state_id = action.data;
                let request = yield API.admin.state.getCities(state_id);
                yield put({
                    type: FETCH_CITY_SUCCESSED,
                    data: request
                });
            } catch (error) {
                yield put({
                    type: 'API_CALL_ERROR',
                    error: error
                });
            }
        }

        function* watchFetchCity() {
            yield takeLatest(FETCH_CITY_REQUESTED, fetchCity);
        }

        function* fetchIndustry(action) {
            try {
                let request = yield API.admin.industry.getAll();
                yield put({
                    type: FETCH_INDUSTRY_SUCCESSED,
                    data: request
                });
            } catch (error) {
                yield put({
                    type: 'API_CALL_ERROR',
                    error: error
                });
            }
        }

        function* watchFetchIndustry() {
            yield takeLatest(FETCH_INDUSTRY_REQUESTED, fetchIndustry);
        }

        function* fetchTag(action) {
            try {
                let request = yield API.admin.tag.getAll();
                yield put({
                    type: FETCH_TAG_SUCCESSED,
                    data: request
                });
            } catch (error) {
                yield put({
                    type: 'API_CALL_ERROR',
                    error: error
                });
            }
        }

        function* watchFetchTag() {
            yield takeLatest(FETCH_TAG_REQUESTED, fetchTag);
        }

        let Sagas = _.map([
            watchFetchJobTitle,
            watchFetchExperience,
            watchFetchNationality,
            watchFetchQualification,
            watchFetchCountry,
            watchFetchState,
            watchFetchCity,
            watchFetchIndustry,
            watchFetchTag
        ], item => fork(item));

        return _.concat(
            Sagas,
            PostJobSagas,
            UpdateJobSagas
        );
    }
})(angular.module('app.components.jobs.sagas', [
    'app.components.jobs.create.sagas',
    'app.components.jobs.update.sagas',
]));