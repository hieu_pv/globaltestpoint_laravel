let {
    call,
    put,
    take,
    takeEvery,
    takeLatest,
    select,
    fork
} = require('redux-saga/effects');

let {
    channel
} = require('redux-saga');

const fetchShiftGeoLocationChannel = channel();

let {
    POST_JOB_OFFER,
    POST_JOB_OFFER_SUCCESSED,
    RESET_POST_JOB_OBJECT,
    FETCH_DUPLICATE_JOB_REQUESTED,
    FETCH_DUPLICATE_JOB_SUCCESSED,
    UPDATE_POST_JOB,
    SET_DRESS_CODE_REQUIRED,
    SELECT_INDUSTRY,
    FETCH_INDUSTRIES_SUCCESSED,
    FETCH_LANGUAGES_SUCCESSED,
    UPDATE_POST_JOB_INSTANCE,
    UPDATE_SHIFT_ADDRESS,
    FETCH_SHIFT_GEO_LOCATION_SUCCESSED,
    CAN_NOT_FETCH_SHIFT_GEO_LOCATION,
} = require('./action');

let newJob = state => state.PostJob;
let jobTemplateSelector = state => state.JobTemplates.templates;

(function(app) {
    app.factory('PostJobSagas', ['$state', 'API', 'Notification', 'preloader', function($state, API, Notification, preloader) {

        function processFetchShiftGeoLocation(action) {
            let shift = action.data;
            try {
                let geocoder = new google.maps.Geocoder();
                geocoder.geocode({
                    'address': shift.location,
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        return fetchShiftGeoLocationChannel.put({
                            type: FETCH_SHIFT_GEO_LOCATION_SUCCESSED,
                            data: results,
                            shift: shift,
                        });
                    } else {
                        return fetchShiftGeoLocationChannel.put({
                            type: CAN_NOT_FETCH_SHIFT_GEO_LOCATION,
                            data: shift,
                        });
                    }
                });
            } catch (error) {
                return fetchShiftGeoLocationChannel.put({
                    type: CAN_NOT_FETCH_SHIFT_GEO_LOCATION,
                    data: shift,
                });
            }
        }

        function* watchFetchShiftGeoLocation() {
            yield takeLatest(UPDATE_SHIFT_ADDRESS, processFetchShiftGeoLocation);
        }

        function* watchFetchShiftGeoLocationChannel() {
            while (true) {
                const action = yield take(fetchShiftGeoLocationChannel);
                yield put(action);
            }
        }



        function* processPostJobOffer(action) {
            preloader.show();
            try {
                let job = yield select(newJob);
                let request = yield API.admin.job.create(job);
                yield put({
                    type: POST_JOB_OFFER_SUCCESSED,
                    data: request,
                });
                preloader.hide();
            } catch (error) {
                yield put({
                    type: 'API_CALL_ERROR',
                    error: error,
                });
            }
        }

        function* watchPostJobOffer() {
            yield takeEvery(POST_JOB_OFFER, processPostJobOffer);
        }

        function* redirectAfterPostJob(action) {
            try {
                if (action.data.isDraft()) {
                    Notification.show('success', 'Your job is saved as draft', 5000);
                } else {
                    Notification.show('success', 'Your job is saved', 5000);
                }
                yield put({
                    type: RESET_POST_JOB_OBJECT,
                });
            } catch (error) {
                yield put({
                    type: 'API_CALL_ERROR',
                    error: error,
                });
            }
        }

        function* watchRedirectAfterPostJob() {
            yield takeEvery(POST_JOB_OFFER_SUCCESSED, redirectAfterPostJob);
        }

        function* processFetchDuplicateJob(action) {
            preloader.show();
            try {
                let job_id = action.data;
                let request = yield API.job.show(job_id);
                yield put({
                    type: FETCH_DUPLICATE_JOB_SUCCESSED,
                    data: request,
                });
                preloader.hide();
            } catch (error) {
                yield put({
                    type: 'API_CALL_ERROR',
                    error: error,
                });
            }
        }

        function* watchFetchDuplicateJob() {
            yield takeEvery(FETCH_DUPLICATE_JOB_REQUESTED, processFetchDuplicateJob);
        }

        function updatePostJobInstance(action) {

        }

        function* watchUpdatePostJobInstance() {
            yield takeEvery(UPDATE_POST_JOB_INSTANCE, updatePostJobInstance);
        }

        function* watchAllDataFetched() {
            while (true) {
                const result = yield take([FETCH_DUPLICATE_JOB_SUCCESSED, FETCH_INDUSTRIES_SUCCESSED, FETCH_LANGUAGES_SUCCESSED]);
                const job = yield select(state => state.PostJob);
                const available_industries = yield select(state => state.PostJob.available_industries);
                const job_titles = yield select(state => state.JobTitle.items);
                yield put({
                    type: UPDATE_POST_JOB_INSTANCE,
                    job,
                    available_industries,
                    job_titles,
                });
            }
        }

        return _.map([
            watchFetchShiftGeoLocation,
            watchFetchShiftGeoLocationChannel,
            watchPostJobOffer,
            watchRedirectAfterPostJob,
            watchFetchDuplicateJob,
            watchUpdatePostJobInstance,
            watchAllDataFetched,
        ], item => fork(item));
    }]);
})(angular.module('app.components.jobs.create.sagas', []));