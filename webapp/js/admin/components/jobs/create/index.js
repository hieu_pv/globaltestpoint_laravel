require('./router');

let Shift = require('../../../../models/Shift');

let {
    POST_JOB_REMOVE_SHIFT,
    SELECT_INDUSTRY,
    REMOVE_INDUSTRY,
    FETCH_INDUSTRIES_SUCCESSED,
    FETCH_LANGUAGES_SUCCESSED,
    POST_JOB_OFFER,
    SET_NEW_JOB_DRAFT,
    UPDATE_POST_JOB,
    SELECT_JOB_TITLE,
    SELECT_TAG,
    REMOVE_TAG
} = require('./action');

let {
    FETCH_JOB_TITLE_REQUESTED,
    FETCH_EXPERIENCE_REQUESTED,
    FETCH_NATIONALITY_REQUESTED,
    FETCH_QUALIFICATION_REQUESTED,
    FETCH_COUNTRY_REQUESTED,
    FETCH_STATE_REQUESTED,
    FETCH_CITY_REQUESTED,
    FETCH_INDUSTRY_REQUESTED,
    FETCH_TAG_REQUESTED
} = require('../action');

let {
    INIT_GOOGLE_MAP_SDK,
} = require('../../../store/action');



(function(app) {

    app.controller('AdminCreateJobCtrl', AdminCreateJobCtrl);
    AdminCreateJobCtrl.$inject = ['$rootScope', '$scope', '$stateParams', 'store', 'API', 'UshiftApp', 'APP_CONSTANTS', 'Notification', 'Modal', '$state', 'preloader'];

    function AdminCreateJobCtrl($rootScope, $scope, $stateParams, store, API, UshiftApp, APP_CONSTANTS, Notification, Modal, $state, preloader) {

        var vm = this;
        vm.store = store;
        vm.user = $rootScope.user;
        vm.store.getState().PostJob = {
            type: 1
        };

        vm.store.dispatch({ type: INIT_GOOGLE_MAP_SDK });

        if (!vm.store.getState().Industry.fetched) {
            vm.store.dispatch({ type: FETCH_INDUSTRY_REQUESTED });
        }

        if (!vm.store.getState().JobTitle.fetched) {
            vm.store.dispatch({ type: FETCH_JOB_TITLE_REQUESTED });
        }

        if (!vm.store.getState().Experience.fetched) {
            vm.store.dispatch({ type: FETCH_EXPERIENCE_REQUESTED });
        }

        if (!vm.store.getState().Nationality.fetched) {
            vm.store.dispatch({ type: FETCH_NATIONALITY_REQUESTED });
        }

        if (!vm.store.getState().Qualification.fetched) {
            vm.store.dispatch({ type: FETCH_QUALIFICATION_REQUESTED });
        }

        if (!vm.store.getState().Country.fetched) {
            vm.store.dispatch({ type: FETCH_COUNTRY_REQUESTED });
        }

        if (!vm.store.getState().Tag.fetched) {
            vm.store.dispatch({ type: FETCH_TAG_REQUESTED });
        }

        preloader.show();
        API.admin.user.getAllEmployer({
                status: 'active'
            })
            .then(function(employers) {
                vm.employers = employers;
                preloader.hide();
            })
            .catch(function(error) {
                throw error;
            });

        vm.onSelectJobTitle = onSelectJobTitle;
        vm.onSelectExperience = onSelectExperience;
        vm.onSelectGender = onSelectGender;
        vm.onSelectEmployer = onSelectEmployer;
        vm.onSelectIndustry = onSelectIndustry;
        vm.onRemoveIndustry = onRemoveIndustry;
        vm.onSelectCountry = onSelectCountry;
        vm.onSelectState = onSelectState;
        vm.onSelectTag = onSelectTag;
        vm.onRemoveTag = onRemoveTag;

        vm.create = create;
        vm.saveAsDraft = saveAsDraft;

        vm.today = moment().toDate();
        vm.candidate_genders = UshiftApp.genders;
        vm.currency = _.find(UshiftApp.currencies, item => item.code === 'SGD');

        API.admin.industry.list({ per_page: 100 })
            .then(function(industries) {
                vm.store.dispatch({
                    type: FETCH_INDUSTRIES_SUCCESSED,
                    data: industries
                });
            })
            .catch(function(error) {
                throw error;
            });

        function onSelectJobTitle(item) {
            vm.store.dispatch({
                type: SELECT_JOB_TITLE,
                data: item
            });
        }

        function onSelectExperience(experience) {
            vm.store.dispatch({
                type: UPDATE_POST_JOB,
                field: 'experience',
                data: experience
            });
        }

        function onSelectGender(gender) {
            vm.store.dispatch({
                type: UPDATE_POST_JOB,
                field: 'gender',
                data: gender
            });
        }

        function onSelectEmployer(user) {
            vm.store.dispatch({
                type: UPDATE_POST_JOB,
                field: 'company_name',
                data: user.getName(),
            });
        }

        function onSelectIndustry(industries) {
            vm.store.dispatch({
                type: SELECT_INDUSTRY,
                data: industries,
            });
        }

        function onRemoveIndustry(industries) {
            vm.store.dispatch({
                type: REMOVE_INDUSTRY,
                data: industries,
            });
        }

        function onSelectTag(tags) {
            vm.store.dispatch({
                type: SELECT_TAG,
                data: tags,
            });
        }

        function onRemoveTag(tags) {
            vm.store.dispatch({
                type: REMOVE_TAG,
                data: tags,
            });
        }

        function onSelectCountry(country) {
            vm.store.getState().PostJob.state = null;

            vm.store.dispatch({
                type: FETCH_STATE_REQUESTED,
                data: country.getId()
            });
        }

        function onSelectState(state) {
            vm.store.getState().PostJob.city = null;

            vm.store.dispatch({
                type: FETCH_CITY_REQUESTED,
                data: state.getId()
            });
        }

        function create(data, options) {
            if (vm.store.getState().PostJob.gender_required && !vm.store.getState().PostJob.gender) {
                var t = $(`[name="language-required"]`).offset().top;
                if (_.isUndefined(top)) {
                    top = t;
                } else {
                    if (t < top) {
                        top = t;
                    }
                }
                if (!_.isUndefined(top)) {
                    $("html, body").animate({
                        scrollTop: top - 90
                    }, 500);
                }
                return false;
            }

            if (parseFloat(vm.store.getState().PostJob.min_salary) > parseFloat(vm.store.getState().PostJob.max_salary)) {
                Notification.show('warning', 'Min Salary can not larger Max Salary');
                return false;
            }
            if (_.isUndefined(vm.store.getState().PostJob.employer)) {
                Notification.show('warning', 'Please select employer', 3000);
                return false;
            }
            if (_.isUndefined(vm.store.getState().PostJob.job_title)) {
                Notification.show('warning', 'Please select Job Title', 3000);
                return false;
            }
            if (_.isUndefined(vm.store.getState().PostJob.industries) || vm.store.getState().PostJob.industries.length === 0) {
                Notification.show('warning', 'Please select industry', 3000);
                return false;
            }
            if (_.isUndefined(vm.store.getState().PostJob.experience)) {
                Notification.show('warning', 'Please select experience', 3000);
                return false;
            }
            if (_.isUndefined(vm.store.getState().PostJob.qualification)) {
                Notification.show('warning', 'Please select qualification', 3000);
                return false;
            }
            if (_.isUndefined(vm.store.getState().PostJob.nationality)) {
                Notification.show('warning', 'Please select nationality', 3000);
                return false;
            }

            if (vm.store.getState().PostJob.type === 1) {
                if (_.isUndefined(vm.store.getState().PostJob.country)) {
                    Notification.show('warning', 'Please select country', 3000);
                    return false;
                }
                if (_.isUndefined(vm.store.getState().PostJob.state)) {
                    Notification.show('warning', 'Please select state', 3000);
                    return false;
                }
                if (_.isUndefined(vm.store.getState().PostJob.city)) {
                    Notification.show('warning', 'Please select city', 3000);
                    return false;
                }
            }

            vm.store.dispatch({
                type: POST_JOB_OFFER
            });
            vm.create_job_form.$setPristine();
        }

        function saveAsDraft(data) {
            vm.create_job_form.$setSubmitted();
            if ($rootScope.isValidFormOrScrollToError(vm.create_job_form)) {
                vm.store.dispatch({
                    type: SET_NEW_JOB_DRAFT,
                    data: true
                });
                create(data);
            }
        }
    }

    app.filter('postJobEmployerSelectionFilter', function() {
        return function(items, search) {
            var out = [];

            if (_.isArray(items) && items.length > 0) {

                items.forEach(function(item) {
                    var flag = false;

                    var text = search.toLowerCase();
                    if (item.first_name.toString().toLowerCase().indexOf(text) !== -1) {
                        flag = true;
                    }
                    if (item.last_name.toString().toLowerCase().indexOf(text) !== -1) {
                        flag = true;
                    }
                    if (item.email.toString().toLowerCase().indexOf(text) !== -1) {
                        flag = true;
                    }
                    if (flag) {
                        out.push(item);
                    }
                });
            } else {
                out = items;
            }
            return out;
        };
    });

    app.directive('postJobUiSelect', function() {
        return {
            require: 'uiSelect',
            link: function(scope, element, attrs, $select) {
                $select.dropdownPosition = 'down';
            }
        };
    });

})(angular.module('app.components.jobs.create', [
    'app.components.jobs.create.router'
]));