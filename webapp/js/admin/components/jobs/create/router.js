(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.jobs.create', {
                url: '/create',
                views: {
                    'admin-jobs@app.jobs': {
                        templateUrl: "/webapp/js/admin/components/jobs/create/_create.tpl.html",
                        controller: 'AdminCreateJobCtrl',
                        controllerAs: 'vm',
                    }
                }
            });
    }]);
})(angular.module('app.components.jobs.create.router', []));
