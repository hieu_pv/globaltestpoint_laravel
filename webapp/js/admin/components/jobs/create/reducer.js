let Job = require('../../../../models/Job');
let Shift = require('../../../../models/Shift');

let {
    POST_JOB_REMOVE_SHIFT,
    SELECT_INDUSTRY,
    REMOVE_INDUSTRY,
    FETCH_INDUSTRIES_SUCCESSED,
    FETCH_LANGUAGES_SUCCESSED,
    RESET_POST_JOB_OBJECT,
    SET_NEW_JOB_DRAFT,
    UPDATE_POST_JOB,
    FETCH_DUPLICATE_JOB_SUCCESSED,
    FETCH_SHIFT_GEO_LOCATION_SUCCESSED,
    CAN_NOT_FETCH_SHIFT_GEO_LOCATION,
    SELECT_JOB_TITLE,
    SELECT_TAG,
    REMOVE_TAG
} = require('./action');


const PostJob = (state, action) => {
    if (_.isUndefined(state)) {
        state = new Job({
            show_industry_selection: true,
            gender_required: false,
            type: 1,
        });
    }
    switch (action.type) {
        case RESET_POST_JOB_OBJECT:
            return new Job({
                show_industry_selection: true,
                language_required: false,
                gender_required: false,
                driving_license_required: false,
                type: 1,
                min_salary: '',
                max_salary: '',
                currency: 'USD',
                desired_candidate_profile: '',
            });
        case UPDATE_POST_JOB:
            let tmp = {};
            tmp[action.field] = action.data;
            return _.assign(state, tmp);
        case FETCH_DUPLICATE_JOB_SUCCESSED:
            delete action.data.shifts;
            return _.assign(state, action.data);
        case FETCH_SHIFT_GEO_LOCATION_SUCCESSED:
            return _.assign(state, {
                shifts: _.map(state.shifts, item => {
                    if (_.isEqual(item, action.shift)) {
                        var latlng = action.data[0].geometry.location.toJSON();
                        item.latitude = latlng.lat;
                        item.longitude = latlng.lng;
                    }
                    return item;
                }),
            });
        case CAN_NOT_FETCH_SHIFT_GEO_LOCATION:
            return _.assign(state, {
                shifts: _.map(state.shifts, item => {
                    if (_.isEqual(item, action.data)) {
                        item.latitude = 0;
                        item.longitude = 0;
                    }
                    return item;
                }),
            });
        case POST_JOB_REMOVE_SHIFT:
            return _.assign(state, {
                shifts: _.filter(state.shifts, item => item !== action.data),
            });
        case SELECT_INDUSTRY:
            return _.assign(state, {
                selected_industry: action.data,
                industry_ids: _.map(action.data, 'id'),
            });
        case REMOVE_INDUSTRY:
            return _.assign(state, {
                selected_industry: action.data,
                industry_ids: _.map(action.data, 'id'),
            });
        case SELECT_TAG:
            return _.assign(state, {
                selected_tag: action.data,
                tag_ids: _.map(action.data, 'id'),
            });
        case REMOVE_TAG:
            return _.assign(state, {
                selected_tag: action.data,
                tag_ids: _.map(action.data, 'id'),
            });
        case FETCH_INDUSTRIES_SUCCESSED:
            return _.assign(state, {
                available_industries: action.data,
            });
        case FETCH_LANGUAGES_SUCCESSED:
            return _.assign(state, {
                languages: _.filter(action.data, item => item.slug !== 'english'),
            });
        case SET_NEW_JOB_DRAFT:
            return _.assign(state, {
                draft: action.data,
            });
        case SELECT_JOB_TITLE:
            return _.assign(state, {
                job_title: action.data,
                title: `${action.data.job_title}`,
                description: action.data.job_description
            });
        default:
            return state;
    }
};

module.exports = {
    PostJob,
};