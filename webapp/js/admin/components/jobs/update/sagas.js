let {
    call,
    put,
    take,
    takeEvery,
    takeLatest,
    select,
    fork
} = require('redux-saga/effects');

let {
    FETCH_JOB_OFFER_SUCCESSED,
    FETCH_INDUSTRY_SUCCESSED,
    FETCH_JOB_TITLE_SUCCESSED,
    FETCH_EXPERIENCE_SUCCESSED,
    FETCH_QUALIFICATION_SUCCESSED,
    FETCH_NATIONALITY_SUCCESSED,
    FETCH_COUNTRY_SUCCESSED,
    FETCH_CITY_SUCCESSED,
    UPDATE_JOB_INSTANCE,
    UPDATE_JOB_OFFER,
    DISABLE_UPDATE_JOB_OFFER,
    SAVE_JOB_OFFER,
    SAVE_JOB_OFFER_SUCCESSED,
    RESET_UPDATE_JOB_OBJECT,
    FETCH_STATE_REQUESTED,
    SELECT_STATE
} = require('./action');

let {
    FETCH_CITY_REQUESTED
} = require('../action');

let newJob = state => state.PostJob;
let ServiceFeeSelector = state => state.ServiceFee;

(function(app) {
    app.factory('UpdateJobSagas', ['$rootScope', '$state', '$stateParams', 'API', 'Notification', 'preloader', function($rootScope, $state, $stateParams, API, Notification, preloader) {

        function* watchAllDataFetched() {
            while (true) {
                const result = yield take([
                    FETCH_JOB_OFFER_SUCCESSED,
                    FETCH_INDUSTRY_SUCCESSED,
                    FETCH_JOB_TITLE_SUCCESSED,
                    FETCH_EXPERIENCE_SUCCESSED,
                    FETCH_QUALIFICATION_SUCCESSED,
                    FETCH_NATIONALITY_SUCCESSED,
                    FETCH_COUNTRY_SUCCESSED
                ]);

                const job = yield select(state => state.UpdateJob);
                const available_industries = yield select(state => state.Industry.items);
                const job_titles = yield select(state => state.JobTitle.items);
                const experiences = yield select(state => state.Experience.items);
                const qualifications = yield select(state => state.Qualification.items);
                const nationalities = yield select(state => state.Nationality.items);
                const countries = yield select(state => state.Country.items);

                yield put({
                    type: UPDATE_JOB_INSTANCE,
                    job,
                    available_industries,
                    job_titles,
                    experiences,
                    qualifications,
                    nationalities,
                    countries
                });
            }
        }

        function* updateJobInstance(action) {
            if (!_.isUndefined(action.job) &&
                _.isArray(action.available_industries) &&
                _.isArray(action.job_titles) &&
                _.isArray(action.experiences) &&
                _.isArray(action.qualifications) &&
                _.isArray(action.nationalities) &&
                _.isArray(action.countries)) {

                let job = yield select(state => state.UpdateJob);
                let available_industries = yield select(state => state.Industry.items);
                let job_titles = yield select(state => state.JobTitle.items);
                let experiences = yield select(state => state.Experience.items);
                let qualifications = yield select(state => state.Qualification.items);
                let nationalities = yield select(state => state.Nationality.items);
                let countries = yield select(state => state.Country.items);
                // let cities = yield select(state => state.City.items);

                if (!_.isUndefined(job.job_title_id) && job.job_title_id !== 0) {
                    yield put({
                        type: UPDATE_JOB_OFFER,
                        field: 'job_title_required',
                        data: true,
                    });
                    let job_title = _.find(job_titles, item => item.getId() === job.job_title_id);
                    if (!_.isUndefined(job_title)) {
                        yield put({
                            type: UPDATE_JOB_OFFER,
                            field: 'job_title',
                            data: job_title,
                        });
                    }
                }

                if (!_.isUndefined(job.experience_id) && job.experience_id !== 0) {
                    yield put({
                        type: UPDATE_JOB_OFFER,
                        field: 'experience_required',
                        data: true,
                    });
                    let experience = _.find(experiences, item => item.getId() === job.experience_id);
                    if (!_.isUndefined(experience)) {
                        yield put({
                            type: UPDATE_JOB_OFFER,
                            field: 'experience',
                            data: experience,
                        });
                    }
                }

                if (!_.isUndefined(job.qualification_id) && job.qualification_id !== 0) {
                    yield put({
                        type: UPDATE_JOB_OFFER,
                        field: 'qualification_required',
                        data: true,
                    });
                    let qualification = _.find(qualifications, item => item.getId() === job.qualification_id);
                    if (!_.isUndefined(qualification)) {
                        yield put({
                            type: UPDATE_JOB_OFFER,
                            field: 'qualification',
                            data: qualification,
                        });
                    }
                }

                if (!_.isUndefined(job.nationality_id) && job.nationality_id !== 0) {
                    yield put({
                        type: UPDATE_JOB_OFFER,
                        field: 'nationality_required',
                        data: true,
                    });
                    let nationality = _.find(nationalities, item => item.getId() === job.nationality_id);
                    if (!_.isUndefined(nationality)) {
                        yield put({
                            type: UPDATE_JOB_OFFER,
                            field: 'nationality',
                            data: nationality,
                        });
                    }
                }

                console.log('test country_id ', job.country_id);
                if (!_.isUndefined(job.country_id) && job.country_id !== 0) {
                    yield put({
                        type: UPDATE_JOB_OFFER,
                        field: 'country_required',
                        data: true,
                    });
                    let country = _.find(countries, item => item.getId() === job.country_id);
                    console.log('test country ', country);
                    if (!_.isUndefined(country)) {
                        yield put({
                            type: UPDATE_JOB_OFFER,
                            field: 'country',
                            data: country,
                        });
                    }
                }

                // if (!_.isUndefined(job.city_id) && job.city_id !== 0) {
                //     yield put({
                //         type: UPDATE_JOB_OFFER,
                //         field: 'city_required',
                //         data: true,
                //     });
                //     let city = _.find(cities, item => item.getId() === job.city_id);
                //     if (!_.isUndefined(city)) {
                //         yield put({
                //             type: UPDATE_JOB_OFFER,
                //             field: 'city',
                //             data: city,
                //         });
                //     }
                // }

                if (_.isArray(job.industries) && job.industries.length > 0) {
                    yield put({
                        type: UPDATE_JOB_OFFER,
                        field: 'industry_id',
                        data: _.head(job.industries).getId(),
                    });

                    let industry = _.find(available_industries, item => item.getId() === _.head(job.industries).getId());
                    if (!_.isUndefined(industry)) {
                        yield put({
                            type: UPDATE_JOB_OFFER,
                            field: 'selected_industry',
                            data: industry,
                        });
                    }
                }
            }
        }

        function* watchUpdateJobInstance() {
            yield takeLatest(UPDATE_JOB_INSTANCE, updateJobInstance);
        }

        function* processSaveJobOffer(action) {
            preloader.show();
            try {
                let job = yield select(state => state.UpdateJob);
                let request = yield API.admin.job.update(job);
                yield put({
                    type: SAVE_JOB_OFFER_SUCCESSED,
                    data: request,
                });
                preloader.hide();
            } catch (error) {
                yield put({
                    type: 'API_CALL_ERROR',
                    error: error,
                });
            }
        }

        function* watchSaveJobOffer() {
            yield takeLatest(SAVE_JOB_OFFER, processSaveJobOffer);
        }

        function redirectAfterSaveJob(action) {
            Notification.show('success', 'Job is updated successfully', 3000);
        }

        function* watchRedirectAfterSaveJob() {
            yield takeEvery(SAVE_JOB_OFFER_SUCCESSED, redirectAfterSaveJob);
        }

        function* fetchCity(action) {
            yield put({
                type: FETCH_CITY_REQUESTED,
                data: action.data.id
            });
        }

        function* watchSelectState() {
            yield takeLatest(SELECT_STATE, fetchCity);
        }

        return _.map([
            watchAllDataFetched,
            watchUpdateJobInstance,
            watchSaveJobOffer,
            watchRedirectAfterSaveJob,
            watchSelectState
        ], item => fork(item));
    }]);
})(angular.module('app.components.jobs.update.sagas', []));