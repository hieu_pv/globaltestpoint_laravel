require('./router');

let {
    FETCH_JOB_OFFER_SUCCESSED,
    UPDATE_JOB_REMOVE_SHIFT,
    UPDATE_JOB_OFFER,
    SELECT_INDUSTRY,
    REMOVE_INDUSTRY,
    FETCH_INDUSTRIES_SUCCESSED,
    FETCH_LANGUAGES_SUCCESSED,
    SAVE_JOB_OFFER,
    SET_UPDATE_JOB_STATE,
    SELECT_TAG,
    REMOVE_TAG,
    SELECT_JOB_TITLE,
    SELECT_STATE
} = require('./action');

let {
    FETCH_JOB_TITLE_REQUESTED,
    FETCH_EXPERIENCE_REQUESTED,
    FETCH_NATIONALITY_REQUESTED,
    FETCH_QUALIFICATION_REQUESTED,
    FETCH_COUNTRY_REQUESTED,
    FETCH_STATE_REQUESTED,
    FETCH_CITY_REQUESTED,
    FETCH_INDUSTRY_REQUESTED,
    FETCH_TAG_REQUESTED
} = require('../action');


let {
    INIT_GOOGLE_MAP_SDK,
} = require('../../../store/action');

(function(app) {

    app.controller('AdminUpdateJobCtrl', AdminUpdateJobCtrl);
    AdminUpdateJobCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'store', 'UshiftApp', 'APP_CONSTANTS', 'API', 'Notification', 'Modal', 'job', 'preloader'];

    function AdminUpdateJobCtrl($rootScope, $scope, $state, $stateParams, store, UshiftApp, APP_CONSTANTS, API, Notification, Modal, job, preloader) {
        var vm = this;
        vm.store = store;
        vm.user = $rootScope.user;

        $rootScope.title = `Update - ${job.getTitle()}`;
        vm.candidate_genders = UshiftApp.genders;

        vm.onSelectJobTitle = onSelectJobTitle;
        vm.onSelectExperience = onSelectExperience;
        vm.onSelectGender = onSelectGender;
        vm.onSelectEmployer = onSelectEmployer;
        vm.onSelectIndustry = onSelectIndustry;
        vm.onRemoveIndustry = onRemoveIndustry;
        vm.onSelectCountry = onSelectCountry;
        vm.onSelectState = onSelectState;
        vm.onSelectTag = onSelectTag;
        vm.onRemoveTag = onRemoveTag;

        vm.remove = remove;
        vm.update = update;

        vm.store.dispatch({ type: INIT_GOOGLE_MAP_SDK });

        if (!vm.store.getState().Industry.fetched) {
            vm.store.dispatch({ type: FETCH_INDUSTRY_REQUESTED });
        }

        if (!vm.store.getState().JobTitle.fetched) {
            vm.store.dispatch({ type: FETCH_JOB_TITLE_REQUESTED });
        }

        if (!vm.store.getState().Experience.fetched) {
            vm.store.dispatch({ type: FETCH_EXPERIENCE_REQUESTED });
        }

        if (!vm.store.getState().Nationality.fetched) {
            vm.store.dispatch({ type: FETCH_NATIONALITY_REQUESTED });
        }

        if (!vm.store.getState().Qualification.fetched) {
            vm.store.dispatch({ type: FETCH_QUALIFICATION_REQUESTED });
        }

        if (!vm.store.getState().Country.fetched) {
            vm.store.dispatch({ type: FETCH_COUNTRY_REQUESTED });
        }

        if (!vm.store.getState().Tag.fetched) {
            vm.store.dispatch({ type: FETCH_TAG_REQUESTED });
        }

        preloader.show();
        API.admin.user.getAllEmployer({
                status: 'active'
            })
            .then(function(employers) {
                vm.employers = employers;
                preloader.hide();
            })
            .catch(function(error) {
                throw error;
            });

        vm.store.dispatch({
            type: FETCH_JOB_OFFER_SUCCESSED,
            data: job,
        });

        if (!_.isNil(job.country_id) && job.country_id !== 0) {
            vm.store.dispatch({
                type: FETCH_STATE_REQUESTED,
                data: job.country_id
            });
        }

        if (!_.isNil(job.state_id) && job.state_id !== 0) {
            vm.store.dispatch({
                type: FETCH_CITY_REQUESTED,
                data: job.state_id
            });
        }

        function onSelectJobTitle(item) {
            vm.store.dispatch({
                type: SELECT_JOB_TITLE,
                data: item
            });
        }

        function onSelectExperience(experience) {
            vm.store.dispatch({
                type: UPDATE_JOB_OFFER,
                field: 'experience',
                data: experience
            });
        }

        function onSelectGender(gender) {
            vm.store.dispatch({
                type: UPDATE_JOB_OFFER,
                field: 'gender',
                data: gender
            });
        }

        function onSelectEmployer(user) {
            vm.store.dispatch({
                type: UPDATE_JOB_OFFER,
                field: 'company_name',
                data: user.getName(),
            });
        }

        function onSelectIndustry(industry) {
            vm.store.dispatch({
                type: SELECT_INDUSTRY,
                data: industry,
            });
        }

        function onRemoveIndustry(industries) {
            vm.store.dispatch({
                type: REMOVE_INDUSTRY,
                data: industries,
            });
        }

        function onSelectTag(tags) {
            vm.store.dispatch({
                type: SELECT_TAG,
                data: tags,
            });
        }

        function onRemoveTag(tags) {
            vm.store.dispatch({
                type: REMOVE_TAG,
                data: tags,
            });
        }

        function onSelectCountry(country) {
            vm.store.getState().UpdateJob.state = null;

            vm.store.dispatch({
                type: FETCH_STATE_REQUESTED,
                data: country.getId()
            });
            
            // vm.store.dispatch({
            //     type: UPDATE_JOB_OFFER,
            //     field: 'country',
            //     data: country,
            // });
        }

        function onSelectState(state) {
            vm.store.getState().UpdateJob.city = null;

            vm.store.dispatch({
                type: SELECT_STATE,
                data: state
            });

            // vm.store.dispatch({
            //     type: FETCH_CITY_REQUESTED,
            //     data: state.getId()
            // });
        }

        function remove(shift) {
            vm.submitted = false;
            if (_.isArray(vm.store.getState().UpdateJob.shifts) && vm.store.getState().UpdateJob.shifts.length.length < 2) {
                Notification.show('warning', 'You can\'t delete last shift', 3000);
                return false;
            }
            vm.store.dispatch({
                type: UPDATE_JOB_REMOVE_SHIFT,
                data: shift
            });
        }

        function update(job) {
            if (vm.store.getState().UpdateJob.gender_required && !vm.store.getState().UpdateJob.gender) {
                var t = $(`[name="language-required"]`).offset().top;
                if (_.isUndefined(top)) {
                    top = t;
                } else {
                    if (t < top) {
                        top = t;
                    }
                }
                if (!_.isUndefined(top)) {
                    $("html, body").animate({
                        scrollTop: top - 90
                    }, 500);
                }
                return false;
            }

            if (parseFloat(vm.store.getState().UpdateJob.min_salary) > parseFloat(vm.store.getState().UpdateJob.max_salary)) {
                Notification.show('warning', 'Min Salary can not larger Max Salary');
                return false;
            }
            if (_.isUndefined(vm.store.getState().UpdateJob.employer)) {
                Notification.show('warning', 'Please select employer', 3000);
                return false;
            }
            if (_.isUndefined(vm.store.getState().UpdateJob.job_title)) {
                Notification.show('warning', 'Please select Job Title', 3000);
                return false;
            }
            if (_.isUndefined(vm.store.getState().UpdateJob.industries) || vm.store.getState().UpdateJob.industries.length === 0) {
                Notification.show('warning', 'Please select industry', 3000);
                return false;
            }
            if (_.isUndefined(vm.store.getState().UpdateJob.experience)) {
                Notification.show('warning', 'Please select experience', 3000);
                return false;
            }
            if (_.isUndefined(vm.store.getState().UpdateJob.qualification)) {
                Notification.show('warning', 'Please select qualification', 3000);
                return false;
            }
            if (_.isUndefined(vm.store.getState().UpdateJob.nationality)) {
                Notification.show('warning', 'Please select nationality', 3000);
                return false;
            }

            if (vm.store.getState().UpdateJob.type === 1) {
                if (_.isUndefined(vm.store.getState().UpdateJob.country)) {
                    Notification.show('warning', 'Please select country', 3000);
                    return false;
                }
                if (_.isUndefined(vm.store.getState().UpdateJob.state)) {
                    Notification.show('warning', 'Please select state', 3000);
                    return false;
                }
                if (_.isUndefined(vm.store.getState().UpdateJob.city)) {
                    Notification.show('warning', 'Please select city', 3000);
                    return false;
                }
                vm.store.getState().UpdateJob.location = '';
            } else {
                vm.store.getState().UpdateJob.country = null;
                vm.store.getState().UpdateJob.state = null;
                vm.store.getState().UpdateJob.city = null;
            }

            vm.store.dispatch({
                type: SAVE_JOB_OFFER,
                data: job
            });
            vm.update_job_form.$setPristine();
        }

        function saveAsDraft(data) {
            vm.update_job_form.$setSubmitted();
            if ($rootScope.isValidFormOrScrollToError(vm.update_job_form)) {
                update(data, {
                    draft: true
                });
            }
        }

    }

})(angular.module('app.components.jobs.update', [
    'app.components.jobs.update.router',
]));