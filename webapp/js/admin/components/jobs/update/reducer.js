let Job = require('../../../../models/Job');
let Shift = require('../../../../models/Shift');

let {
    UPDATE_IMMEDIATE_SELECTION,
    ADD_BLANK_SHIFT,
    UPDATE_JOB_REMOVE_SHIFT,
    RESET_UPDATE_JOB_OBJECT,
    FETCH_JOB_OFFER_SUCCESSED,
    UPDATE_JOB_OFFER,
    SELECT_INDUSTRY,
    FETCH_INDUSTRIES_SUCCESSED,
    FETCH_LANGUAGES_SUCCESSED,
    DISABLE_UPDATE_JOB_OFFER,
    SET_UPDATE_JOB_STATE,
    SELECT_STATE
} = require('./action');

let {
    FETCH_STATE_SUCCESSED,
    FETCH_CITY_SUCCESSED
} = require('../action');

let {
    FETCH_SHIFT_GEO_LOCATION_SUCCESSED,
    CAN_NOT_FETCH_SHIFT_GEO_LOCATION,
} = require('../create/action');

const UpdateJob = (state, action) => {
    if (_.isUndefined(state)) {
        state = {
            disable_update: false,
        };
    }
    switch (action.type) {
        case DISABLE_UPDATE_JOB_OFFER:
            return _.assign(state, {
                disable_update: true,
            });
        case SET_UPDATE_JOB_STATE:
            return _.assign(state, state.setState(action.data));
        case FETCH_JOB_OFFER_SUCCESSED:
            return _.assign(action.data, {
                disable_update: false,
            });
        case RESET_UPDATE_JOB_OBJECT:
            return {
                disable_update: false,
            };
        case UPDATE_JOB_OFFER:
            let tmp = {};
            tmp[action.field] = action.data;
            return _.assign(state, tmp);
        case FETCH_SHIFT_GEO_LOCATION_SUCCESSED:
            return _.assign(state, {
                shifts: _.map(state.shifts, item => {
                    if (_.isEqual(item, action.shift)) {
                        var latlng = action.data[0].geometry.location.toJSON();
                        item.latitude = latlng.lat;
                        item.longitude = latlng.lng;
                    }
                    return item;
                }),
            });
        case CAN_NOT_FETCH_SHIFT_GEO_LOCATION:
            return _.assign(state, {
                shifts: _.map(state.shifts, item => {
                    if (_.isEqual(item, action.data)) {
                        item.latitude = 0;
                        item.longitude = 0;
                    }
                    return item;
                }),
            });
        case UPDATE_JOB_REMOVE_SHIFT:
            return _.assign(state, {
                shifts: _.filter(state.shifts, item => item !== action.data),
            });
        case UPDATE_IMMEDIATE_SELECTION:
            return _.assign(state, {
                immediate_selection: action.data,
            });
        case FETCH_INDUSTRIES_SUCCESSED:
            return _.assign(state, {
                available_industries: action.data,
            });
        case SELECT_INDUSTRY:
            return _.assign(state, {
                selected_industry: action.data,
                industry_id: action.data.id,
            });
        case FETCH_LANGUAGES_SUCCESSED:
            return _.assign(state, {
                languages: action.data,
            });
        case SELECT_STATE:
            return _.assign(state, {
                state: action.data,
            });
        case FETCH_STATE_SUCCESSED:
            return _.assign(state, {
                state: _.isUndefined(state.state) ? _.find(action.data, item => item.getId() === state.state_id) : state.state,
            });
        case FETCH_CITY_SUCCESSED:
            return _.assign(state, {
                city: _.isUndefined(state.city) ? _.find(action.data, item => item.getId() === state.city_id) : state.city,
            });
        default:
            return state;
    }
};

module.exports = {
    UpdateJob,
};