(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.jobs.update', {
                url: '/:slug/update',
                views: {
                    'admin-jobs@app.jobs': {
                        templateUrl: "/webapp/js/admin/components/jobs/update/_update.tpl.html",
                        controller: 'AdminUpdateJobCtrl',
                        controllerAs: 'vm',
                    }
                },
                resolve: {
                    job: ['API', '$stateParams', function(API, $stateParams) {
                        return API.admin.job.getBySlug($stateParams.slug);
                    }]
                }
            });
    }]);
})(angular.module('app.components.jobs.update.router', []));