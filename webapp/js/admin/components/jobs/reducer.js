let {
    FETCH_JOB_TITLE_SUCCESSED,
    FETCH_EXPERIENCE_SUCCESSED,
    FETCH_NATIONALITY_SUCCESSED,
    FETCH_QUALIFICATION_SUCCESSED,
    FETCH_COUNTRY_SUCCESSED,
    FETCH_STATE_SUCCESSED,
    FETCH_CITY_SUCCESSED,
    FETCH_INDUSTRY_SUCCESSED,
    FETCH_TAG_SUCCESSED
} = require('./action');


const JobTitle = (state, action) => {
    if (_.isUndefined(state)) {
        state = {
            fetched: false,
            items: [],
        };
    }
    switch (action.type) {
        case FETCH_JOB_TITLE_SUCCESSED:
            return _.assign(state, {
                fetched: true,
                items: action.data,
            });
        default:
            return state;
    }
};

const Experience = (state, action) => {
    if (_.isUndefined(state)) {
        state = {
            fetched: false,
            items: [],
        };
    }
    switch (action.type) {
        case FETCH_EXPERIENCE_SUCCESSED:
            return _.assign(state, {
                fetched: true,
                items: action.data,
            });
        default:
            return state;
    }
};

const Nationality = (state, action) => {
    if (_.isUndefined(state)) {
        state = {
            fetched: false,
            items: [],
        };
    }
    switch (action.type) {
        case FETCH_NATIONALITY_SUCCESSED:
            return _.assign(state, {
                fetched: true,
                items: action.data,
            });
        default:
            return state;
    }
};

const Qualification = (state, action) => {
    if (_.isUndefined(state)) {
        state = {
            fetched: false,
            items: [],
        };
    }
    switch (action.type) {
        case FETCH_QUALIFICATION_SUCCESSED:
            return _.assign(state, {
                fetched: true,
                items: action.data,
            });
        default:
            return state;
    }
};

const Country = (state, action) => {
    if (_.isUndefined(state)) {
        state = {
            fetched: false,
            items: [],
        };
    }
    switch (action.type) {
        case FETCH_COUNTRY_SUCCESSED:
            return _.assign(state, {
                fetched: true,
                items: action.data,
            });
        default:
            return state;
    }
};

const State = (state, action) => {
    if (_.isUndefined(state)) {
        state = {
            fetched: false,
            items: [],
        };
    }
    switch (action.type) {
        case FETCH_STATE_SUCCESSED:
            return _.assign(state, {
                fetched: true,
                items: action.data,
            });
        default:
            return state;
    }
};

const City = (state, action) => {
    if (_.isUndefined(state)) {
        state = {
            fetched: false,
            items: [],
        };
    }
    switch (action.type) {
        case FETCH_CITY_SUCCESSED:
            return _.assign(state, {
                fetched: true,
                items: action.data,
            });
        default:
            return state;
    }
};

const Industry = (state, action) => {
    if (_.isUndefined(state)) {
        state = {
            fetched: false,
            items: [],
        };
    }
    switch (action.type) {
        case FETCH_INDUSTRY_SUCCESSED:
            return _.assign(state, {
                fetched: true,
                items: action.data,
            });
        default:
            return state;
    }
};

const Tag = (state, action) => {
    if (_.isUndefined(state)) {
        state = {
            fetched: false,
            items: [],
        };
    }
    switch (action.type) {
        case FETCH_TAG_SUCCESSED:
            return _.assign(state, {
                fetched: true,
                items: action.data,
            });
        default:
            return state;
    }
};

module.exports = {
    JobTitle,
    Experience,
    Nationality,
    Qualification,
    Country,
    State,
    City,
    Industry,
    Tag
};