require('./router');
require('./list/');
require('./create/');
require('./update/');
(function(app) {
    app.controller('AdminJobsCtrl', AdminJobsCtrl);
    AdminJobsCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API'];

    function AdminJobsCtrl($rootScope, $scope, $state, $stateParams, API) {

    }
})(angular.module('app.components.jobs', [
    'app.components.jobs.router',
    'app.components.jobs.list',
    'app.components.jobs.create',
    'app.components.jobs.update',
]));
