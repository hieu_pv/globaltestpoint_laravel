(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.qualification', {
                url: '/qualification',
                views: {
                    'content@app': {
                        templateUrl: "/webapp/js/admin/components/qualification/_qualification.tpl.html",
                        controller: 'AdminQualificationCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.qualification.list', {
                url: '/list?page&search&order_by&constraints',
                views: {
                    'admin-qualification@app.qualification': {
                        templateUrl: "/webapp/js/admin/components/qualification/list/_list.tpl.html",
                        controller: 'AdminListQualificationsCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.qualification.create', {
                url: '/create',
                views: {
                    'admin-qualification@app.qualification': {
                        templateUrl: "/webapp/js/admin/components/qualification/create/_create.tpl.html",
                        controller: 'AdminCreateQualificationCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.qualification.edit', {
                url: '/:id/edit',
                views: {
                    'admin-qualification@app.qualification': {
                        templateUrl: "/webapp/js/admin/components/qualification/edit/_edit.tpl.html",
                        controller: 'AdminEditQualificationCtrl',
                        controllerAs: 'vm',
                    }
                },
                resolve: {
                    qualification: ['API', '$stateParams', function(API, $stateParams) {
                        return API.admin.qualification.show($stateParams.id);
                    }]
                }
            });
    }]);
})(angular.module('app.components.qualification.router', []));