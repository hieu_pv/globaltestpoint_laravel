(function(app) {

    app.controller('AdminEditQualificationCtrl', AdminEditQualificationCtrl);

    AdminEditQualificationCtrl.$inject = ['$rootScope', '$scope', 'API', 'qualification', '$state', '$stateParams', 'Notification', 'preloader'];

    function AdminEditQualificationCtrl($rootScope, $scope, API, qualification, $state, $stateParams, Notification, preloader) {
        var vm = this;

        vm.qualification = qualification;

        vm.editQualification = editQualification;

        function editQualification(qualification) {
            preloader.show();
            API.admin.qualification.update(qualification)
                .then(function(response) {
                    preloader.hide();
                    vm.edit_qualification_form.$setPristine();
                    Notification.show('success', 'Updated success!', 5000);
                })
                .catch(function(error) {
                    preloader.hide();
                    vm.edit_qualification_form.$setPristine();
                    throw error;
                });
        }
    }

})(angular.module('app.components.qualification.edit', []));