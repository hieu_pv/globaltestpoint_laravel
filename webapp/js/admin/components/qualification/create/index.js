(function(app) {

    app.controller('AdminCreateQualificationCtrl', AdminCreateQualificationCtrl);

    AdminCreateQualificationCtrl.$inject = ['$rootScope', '$scope', 'API', 'preloader', 'Notification'];

    function AdminCreateQualificationCtrl($rootScope, $scope, API, preloader, Notification) {
        var vm = this;

        vm.createQualification = createQualification;

        function createQualification(qualification) {
            preloader.show();
            API.admin.qualification.create(qualification)
                .then(function(response) {
                    preloader.hide();
                    vm.create_qualification_form.$setPristine();
                    vm.qualification = null;
                    Notification.show('success', 'Added success!', 5000);
                })
                .catch(function(error) {
                    preloader.hide();
                    vm.create_qualification_form.$setPristine();
                    vm.qualification = null;
                    throw error;
                });
        }
    }

})(angular.module('app.components.qualification.create', []));