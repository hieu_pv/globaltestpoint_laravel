require('./router');
require('./list/');
require('./create/');
require('./edit/');

(function(app) {

    app.controller('AdminQualificationCtrl', AdminQualificationCtrl);
    AdminQualificationCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API'];

    function AdminQualificationCtrl($rootScope, $scope, $state, $stateParams, API) {}
    
})(angular.module('app.components.qualification', [
    'app.components.qualification.router',
    'app.components.qualification.list',
    'app.components.qualification.create',
    'app.components.qualification.edit'
]));
