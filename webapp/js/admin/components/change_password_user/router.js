(function(app) {
	app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.change-password-user', {
                url: '/change-password-user?id',
                views: {
                    'content@app': {
                        templateUrl: "/webapp/js/admin/components/change_password_user/change_password_user.tpl.html",
                        controller: 'ChangePasswordUserCtrl',
                        controllerAs: 'vm',
                    }
                },
                resolve: {
                    profile: ['API', '$stateParams', function(API, $stateParams) {
                        return API.admin.user.show($stateParams.id);
                    }]
                }
            });
    }]);
})(angular.module('app.components.change_password_user.router', []));