require('./router');
(function(app) {

    app.controller('ChangePasswordUserCtrl', ChangePasswordUserCtrl);
    ChangePasswordUserCtrl.$inject = ['$rootScope', '$scope', 'Modal', 'profile', 'preloader', '$stateParams', 'API', 'Notification'];

    function ChangePasswordUserCtrl($rootScope, $scope, Modal, profile, preloader, $stateParams, API, Notification) {
        if (!$rootScope.user.can('update.users') || !$rootScope.user.can('update.companies')) {
            Modal.alert({
                title: "You don't have a required Update Users or Update Companies permission.",
                ok: 'Go to Dashboard',
                cancel: 'NO',
            }, function() {
                $state.go('app.admin_home');
            });
            return false;
        }

        var vm = this;
        vm.profile = profile;

        vm.changePassword = function(password) {
            if (password.new_password !== password.password_confirmation) {
                return false;
            }
            password.id = $stateParams.id;
            preloader.show();
            API.admin.user.changePasswordUser(password)
                .then(function(result) {
                    preloader.hide();
                    API.user.profile()
                        .then(function(response) {
                            Notification.show('success', 'Change password success', 5000);
                            vm.password = null;
                            vm.change_password_form.$setPristine();
                        })
                        .catch(function(error) {
                            throw error;
                        });
                })
                .catch(function(error) {
                    preloader.hide();
                    throw error;
                });
        };
    }

})(angular.module('app.components.change_password_user', [
    'app.components.change_password_user.router'
]));
