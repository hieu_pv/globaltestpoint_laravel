require('./admin_home/');
require('./login/');
require('./employee/');
require('./jobs/');
require('./acl/');
require('./company/');
require('./tag/');
require('./change_password_user/');
require('./email_management/');
require('./industries/');
require('./job_titles/');
require('./advertisements/');
require('./city/');
require('./nationality/');
require('./experience/');
require('./qualification/');

(function(app) {

})(angular.module('app.components', [
    'app.components.admin_home',
    'app.components.login',
    'app.components.employee',
    'app.components.jobs',
    'app.components.acl',
    'app.components.company',
    'app.components.tag',
    'app.components.change_password_user',
    'app.components.email_management',
    'app.components.industries',
    'app.components.job_titles',
    'app.components.advertisements',
    'app.components.city',
    'app.components.nationality',
    'app.components.experience',
    'app.components.qualification'
]));
