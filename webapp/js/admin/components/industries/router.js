(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.industries', {
                url: '/industries',
                views: {
                    'content@app': {
                        templateUrl: "/webapp/js/admin/components/industries/_industries.tpl.html",
                        controller: 'AdminIndustriesCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.industries.list', {
                url: '/list?page&search&order_by&constraints',
                views: {
                    'admin-industries@app.industries': {
                        templateUrl: "/webapp/js/admin/components/industries/list/_list.tpl.html",
                        controller: 'AdminListIndustriesCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.industries.create', {
                url: '/create',
                views: {
                    'admin-industries@app.industries': {
                        templateUrl: "/webapp/js/admin/components/industries/create/_create.tpl.html",
                        controller: 'AdminCreateIndustryCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.industries.edit', {
                url: '/:id/edit',
                views: {
                    'admin-industries@app.industries': {
                        templateUrl: "/webapp/js/admin/components/industries/edit/_edit.tpl.html",
                        controller: 'AdminEditIndustryCtrl',
                        controllerAs: 'vm',
                    }
                },
                resolve: {
                    industry: ['API', '$stateParams', function(API, $stateParams) {
                        return API.admin.industry.show($stateParams.id);
                    }]
                }
            });
    }]);
})(angular.module('app.components.industries.router', []));