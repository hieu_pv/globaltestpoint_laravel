let {
    call,
    put,
    take,
    takeEvery,
    takeLatest,
    select,
    fork
} = require('redux-saga/effects');

let {
    CREATE_INDUSTRY,
    CREATE_INDUSTRY_SUCCESSED
} = require('./action');

let newIndustry = state => state.CreateIndustry;

(function(app) {

    app.factory('CreateIndustrySagas', ['$state', 'API', 'Notification', 'preloader',
        function($state, API, Notification, preloader) {
            function* processCreateIndustry(action) {
                preloader.show();
                try {
                    let industry = yield select(newIndustry);
                    let request = yield API.admin.industry.create(industry);
                    yield put({
                        type: CREATE_INDUSTRY_SUCCESSED,
                        data: request
                    });
                    preloader.hide();
                    Notification.show('success', 'Added success!', 5000);
                } catch (error) {
                    yield put({
                        type: 'API_CALL_ERROR',
                        error: error,
                    });
                }
            }

            function* watchCreateIndustry() {
                yield takeEvery(CREATE_INDUSTRY, processCreateIndustry);
            }

            return _.map([
                watchCreateIndustry
            ], item => fork(item));
        }
    ]);

})(angular.module('app.components.industries.create.sagas', []));