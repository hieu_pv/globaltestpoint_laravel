let Industry = require('../../../../models/Industry');

let {
    UPDATE_DATA_FOR_CREATE_INDUSTRY,
    CREATE_INDUSTRY_SUCCESSED
} = require('./action');

const STATUS_ACTIVE = 1;
const STATUS_PENDING = 0;

const CreateIndustry = (state, action) => {
    if (_.isNil(state)) {
        state = new Industry({
            status: STATUS_ACTIVE
        });
    }
    switch (action.type) {
        case UPDATE_DATA_FOR_CREATE_INDUSTRY:
            let tmp = {};
            tmp[action.field] = action.data;
            return _.assign(state, tmp);
        case CREATE_INDUSTRY_SUCCESSED:
            return new Industry({
                status: STATUS_ACTIVE
            });
        default:
            return state;
    }
};

module.exports = {
    CreateIndustry
};