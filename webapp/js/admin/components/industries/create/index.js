let {
    UPDATE_DATA_FOR_CREATE_INDUSTRY,
    CREATE_INDUSTRY
} = require('./action');

(function(app) {

    app.controller('AdminCreateIndustryCtrl', AdminCreateIndustryCtrl);

    AdminCreateIndustryCtrl.$inject = ['$rootScope', '$scope', 'store', 'API', 'preloader', 'Notification'];

    function AdminCreateIndustryCtrl($rootScope, $scope, store, API, preloader, Notification) {
        var vm = this;
        vm.store = store;

        vm.changeName = changeName;
        vm.changeOrder = changeOrder;
        vm.changeStatus = changeStatus;
        vm.createIndustry = createIndustry;

        function changeName(item) {
            vm.store.dispatch({
                type: UPDATE_DATA_FOR_CREATE_INDUSTRY,
                field: 'name',
                data: item
            });
        }

        function changeOrder(item) {
            vm.store.dispatch({
                type: UPDATE_DATA_FOR_CREATE_INDUSTRY,
                field: 'order',
                data: item
            });
        }

        function changeStatus(item) {
            vm.store.dispatch({
                type: UPDATE_DATA_FOR_CREATE_INDUSTRY,
                field: 'status',
                data: item
            });   
        }

        function createIndustry(industry) {
            vm.store.dispatch({
                type: CREATE_INDUSTRY
            });
            vm.create_industry_form.$setPristine();
        }
    }

})(angular.module('app.components.industries.create', []));