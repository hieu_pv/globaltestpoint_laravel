let Industry = require('../../../../models/Industry');

let {
    FETCH_INDUSTRY_SUCCESSED,
    UPDATE_DATA_FOR_EDIT_INDUSTRY
} = require('./action');

const STATUS_ACTIVE = 1;
const STATUS_PENDING = 0;

const UpdateIndustry = (state, action) => {
    if (_.isNil(state)) {
        state = new Industry({
            status: STATUS_ACTIVE
        });
    }

    switch (action.type) {
        case FETCH_INDUSTRY_SUCCESSED:
            return _.assign(state, action.data);

        case UPDATE_DATA_FOR_EDIT_INDUSTRY:
            let tmp = {};
            tmp[action.field] = action.data;
            return _.assign(state, tmp);
            
        default:
            return state;
    }
};

module.exports = {
    UpdateIndustry
};