let {
    call,
    put,
    take,
    takeEvery,
    takeLatest,
    select,
    fork
} = require('redux-saga/effects');

let {
    EDIT_INDUSTRY,
    EDIT_INDUSTRY_SUCCESSED
} = require('./action');

let editIndustry = state => state.UpdateIndustry;

(function(app) {

    app.factory('EditIndustrySagas', ['$state', 'API', 'Notification', 'preloader',
        function($state, API, Notification, preloader) {
            function* processEditIndustry(action) {
                console.log('edit industry action ', action);
                preloader.show();
                try {
                    let industry = yield select(editIndustry);
                    let request = yield API.admin.industry.update(industry);
                    yield put({
                        type: EDIT_INDUSTRY_SUCCESSED,
                        data: request
                    });
                    preloader.hide();
                    Notification.show('success', 'Updated success!', 5000);
                } catch (error) {
                	yield put({
                        type: 'API_CALL_ERROR',
                        error: error,
                    });
                }
            }

            function* watchEditIndustry() {
                yield takeEvery(EDIT_INDUSTRY, processEditIndustry);
            }

            return _.map([
                watchEditIndustry
            ], item => fork(item));
        }
    ]);

})(angular.module('app.components.industries.edit.sagas', []));