let {
    FETCH_INDUSTRY_SUCCESSED,
    UPDATE_DATA_FOR_EDIT_INDUSTRY,
    EDIT_INDUSTRY
} = require('./action');

(function(app) {

    app.controller('AdminEditIndustryCtrl', AdminEditIndustryCtrl);

    AdminEditIndustryCtrl.$inject = ['$rootScope', '$scope', 'API', 'industry', '$state', '$stateParams', 'Notification', 'preloader', 'store'];

    function AdminEditIndustryCtrl($rootScope, $scope, API, industry, $state, $stateParams, Notification, preloader, store) {
        var vm = this;
        vm.store = store;

        vm.store.dispatch({
            type: FETCH_INDUSTRY_SUCCESSED,
            data: industry
        });

        vm.changeName = changeName;
        vm.changeOrder = changeOrder;
        vm.changeStatus = changeStatus;
        vm.editIndustry = editIndustry;

        function changeName(item) {
            vm.store.dispatch({
                type: UPDATE_DATA_FOR_EDIT_INDUSTRY,
                field: 'name',
                data: item
            });
        }

        function changeOrder(item) {
            vm.store.dispatch({
                type: UPDATE_DATA_FOR_EDIT_INDUSTRY,
                field: 'order',
                data: item
            });
        }

        function changeStatus(item) {
            vm.store.dispatch({
                type: UPDATE_DATA_FOR_EDIT_INDUSTRY,
                field: 'status',
                data: item
            });
        }


        function editIndustry(industry) {
            vm.store.dispatch({
                type: EDIT_INDUSTRY
            });
            vm.edit_industry_form.$setPristine();
        }
    }

})(angular.module('app.components.industries.edit', []));