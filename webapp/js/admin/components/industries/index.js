require('./router');
require('./list/');
require('./create/');
require('./edit/');

(function(app) {

    app.controller('AdminIndustriesCtrl', AdminIndustriesCtrl);

    AdminIndustriesCtrl.$inject = ['$rootScope', '$scope', '$state'];

    function AdminIndustriesCtrl($rootScope, $scope, $state) {
    }

})(angular.module('app.components.industries', [
    'app.components.industries.router',
    'app.components.industries.list',
    'app.components.industries.create',
    'app.components.industries.edit'
]));