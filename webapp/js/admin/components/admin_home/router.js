(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.admin_home', {
                url: '/dashboard',
                views: {
                    'content@app': {
                        templateUrl: "/webapp/js/admin/components/admin_home/_admin_home.tpl.html",
                        controller: 'AdminHomeCtrl',
                        controllerAs: 'vm',
                    }
                }
            });
    }]);
})(angular.module('app.components.admin_home.router', []));