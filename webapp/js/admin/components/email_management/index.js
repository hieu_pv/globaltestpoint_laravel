require('./router');


let {
    FETCH_USERS_SUCCESSED,
    EMAIL_MAMAGEMENT_SELECT_USER_STATUS_FILTER,
    TOGGLE_SUBSCRIBE_STATUS_LIST,
} = require('./action');

let {
    FETCH_USER_STATUS_REQUESTED,
} = require('../../store/action');

(function(app) {

    app.controller('AdminEmailManagementCtrl', AdminEmailManagementCtrl);
    AdminEmailManagementCtrl.$inject = ['$rootScope', '$scope', '$stateParams', 'store', 'API', 'UshiftApp', 'APP_CONSTANTS', 'Notification', 'Modal', '$state', 'preloader'];

    function AdminEmailManagementCtrl($rootScope, $scope, $stateParams, store, API, UshiftApp, APP_CONSTANTS, Notification, Modal, $state, preloader) {
        var vm = this;
        vm.store = store;

        vm.subscribe = subscribe;
        vm.unsubscribe = unsubscribe;
        vm.searchPage = searchPage;
        vm.searchKeyEvent = searchKeyEvent;
        vm.filterStatus = filterStatus;

        if (!vm.store.getState().UserStatus.fetched) {
            vm.store.dispatch({
                type: FETCH_USER_STATUS_REQUESTED,
            });
        }

        let params = {
            per_page: 100,
            roles: 'employee,employer',
            includes: 'roles',
            page: _.isUndefined($stateParams.page) ? 1 : parseInt($stateParams.page),
        };
        if (!_.isUndefined($stateParams.search)) {
            params.search = $stateParams.search;
        }
        if (!_.isUndefined($stateParams.status)) {
            params.status = $stateParams.status;
        }
        preloader.show();
        API.admin.user.get(params)
            .then(function(result) {
                vm.store.dispatch({
                    type: FETCH_USERS_SUCCESSED,
                    data: result.users,
                    pagination: result.pagination,
                });
                preloader.hide();
            })
            .catch(function(error) {
                throw error;
            });

        function subscribe(field) {
            let users = _.filter(vm.store.getState().EmailManagement.users, item => item.selected);
            if (!_.isArray(users) || users.length === 0) {
                Notification.show('warning', 'Please select at least one user', 3000);
                return false;
            }
            vm.store.dispatch({
                type: TOGGLE_SUBSCRIBE_STATUS_LIST,
                data: true,
                field: field,
                user_ids: _.map(users, item => item.getId())
            });
        }

        function unsubscribe(field) {
            let users = _.filter(vm.store.getState().EmailManagement.users, item => item.selected);
            if (!_.isArray(users) || users.length === 0) {
                Notification.show('warning', 'Please select at least one user', 3000);
                return false;
            }
            vm.store.dispatch({
                type: TOGGLE_SUBSCRIBE_STATUS_LIST,
                data: false,
                field: field,
                user_ids: _.map(users, item => item.getId())
            });
        }

        function searchPage(search) {
            $state.go($state.current, {
                page: 1,
                search: search
            }, {
                reload: true
            });
        }

        function searchKeyEvent(event, search) {
            if (event.which === 13) {
                $state.go($state.current, {
                    page: 1,
                    search: search
                }, {
                    reload: true
                });
            }
        }

        function filterStatus(status) {
            vm.store.dispatch({
                type: EMAIL_MAMAGEMENT_SELECT_USER_STATUS_FILTER,
                data: status,
            });
        }
    }
})(angular.module('app.components.email_management', [
    'app.components.email_management.router'
]));
