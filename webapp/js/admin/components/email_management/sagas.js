let {
    call, put, take, takeEvery, takeLatest, select, fork
} = require('redux-saga/effects');

let {
    FETCH_USERS_SUCCESSED,
    EMAIL_MAMAGEMENT_SELECT_USER_STATUS_FILTER,
    EMAIL_MAMAGEMENT_UPDATE_SELECTED_USER_STATUS,
    TOGGLE_SUBSCRIBE_STATUS,
    TOGGLE_SUBSCRIBE_STATUS_SUCCESSED,
    TOGGLE_SUBSCRIBE_STATUS_LIST,
    TOGGLE_SUBSCRIBE_STATUS_LIST_SUCCESSED,
    EMAIL_MANAGEMENT_MAP_USER_STATUS,
} = require('./action');

let {
    FETCH_USER_STATUS_SUCCESSED,
} = require('../../store/action');

(function(app) {
    app.factory('EmailManagementSagas', ['$state', '$stateParams', 'UshiftApp', 'API', 'Notification', 'Modal', 'preloader', function($state, $stateParams, UshiftApp, API, Notification, Modal, preloader) {
        function* proccessUpdateSubscribeStatus(action) {
            try {
                preloader.show();
                let {
                    user_id,
                    field,
                    data,
                } = action;
                let request = yield API.admin.user.updateSubscribeStatus({
                    user_ids: [user_id],
                    field: field,
                    data: data,
                });
                yield put({
                    type: TOGGLE_SUBSCRIBE_STATUS_SUCCESSED,
                    user_id: user_id,
                    field: field,
                    data: data,
                });
                preloader.hide();
            } catch (error) {
                yield put({
                    type: 'API_CALL_ERROR',
                    error: error,
                });
            }
        }

        function* watchUpdateSubscibeStatus() {
            yield takeEvery(TOGGLE_SUBSCRIBE_STATUS, proccessUpdateSubscribeStatus);
        }

        function* proccessUpdateSubscribeMultiUserStatus(action) {
            try {
                preloader.show();
                let {
                    user_ids,
                    field,
                    data,
                } = action;
                let request = yield API.admin.user.updateSubscribeStatus({
                    user_ids: user_ids,
                    field: field,
                    data: data,
                });
                yield put({
                    type: TOGGLE_SUBSCRIBE_STATUS_LIST_SUCCESSED,
                    user_ids: user_ids,
                    field: field,
                    data: data,
                });
                preloader.hide();
            } catch (error) {
                yield put({
                    type: 'API_CALL_ERROR',
                    error: error,
                });
            }
        }

        function* watchUpdateSubscibeMultiUserStatus() {
            yield takeEvery(TOGGLE_SUBSCRIBE_STATUS_LIST, proccessUpdateSubscribeMultiUserStatus);
        }

        function proccessSelectUserStatus(action) {
            let status = action.data;
            if (status.status === 'All') {
                let params = $stateParams;
                if (!_.isUndefined(params.status)) {
                    delete params.status;
                }
                params.page = 1;
                $state.go($state.current, params, {
                    inherit: false,
                    reload: $state.current.name,
                });
            } else {
                $state.go($state.current, {
                    'page': 1,
                    'status': status.id
                });
            }
        }

        function* watchSelectUserStatus() {
            yield takeEvery(EMAIL_MAMAGEMENT_SELECT_USER_STATUS_FILTER, proccessSelectUserStatus);
        }

        function* updateSelectedUserStatus(action) {
            if (!_.isUndefined($stateParams.status)) {
                let user_status = yield select(state => state.UserStatus.forFilterSelection);
                let status = _.find(user_status, item => item.id === parseInt($stateParams.status));
                if (!_.isUndefined(status)) {
                    yield put({
                        type: EMAIL_MAMAGEMENT_UPDATE_SELECTED_USER_STATUS,
                        data: status,
                    });
                }
            }
        }

        function* watchUpdateSelectedUserStatus() {
            yield takeEvery(FETCH_USER_STATUS_SUCCESSED, updateSelectedUserStatus);
        }

        function* proccessMapUserStatus(action) {
            let user_status = yield select(state => state.UserStatus.status);
            if (_.isArray(user_status) && user_status.length) {
                yield put({
                    type: EMAIL_MANAGEMENT_MAP_USER_STATUS,
                    data: user_status,
                });
            }
        }

        function* watchMapUserStatus() {
            yield takeEvery(FETCH_USERS_SUCCESSED, proccessMapUserStatus);
        }

        return _.map([
            watchUpdateSubscibeStatus,
            watchUpdateSubscibeMultiUserStatus,
            watchSelectUserStatus,
            watchUpdateSelectedUserStatus,
            watchMapUserStatus,
        ], item => fork(item));
    }]);
})(angular.module('app.components.email_management.sagas', []));
