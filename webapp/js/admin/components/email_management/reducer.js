let {
    FETCH_USERS_SUCCESSED,
    EMAIL_MAMAGEMENT_SELECT_USER_STATUS_FILTER,
    EMAIL_MAMAGEMENT_UPDATE_SELECTED_USER_STATUS,
    EMAIL_MANAGEMENT_SET_SELECT_ALL,
    TOGGLE_SUBSCRIBE_STATUS_SUCCESSED,
    TOGGLE_SUBSCRIBE_STATUS_LIST_SUCCESSED,
    EMAIL_MANAGEMENT_MAP_USER_STATUS,
} = require('./action');

let {
    FETCH_USER_STATUS_SUCCESSED,
} = require('../../store/action');


const User = (state, action) => {
    if (_.isUndefined(state)) {
        state = _.assign(action.data);
    }
    switch (action.type) {
        case EMAIL_MANAGEMENT_SET_SELECT_ALL:
            return _.assign(state, {
                selected: action.data,
            });
        case TOGGLE_SUBSCRIBE_STATUS_SUCCESSED:
            let tmp = {};
            tmp[action.field] = action.data;
            return state.getId() === action.user_id ? _.assign(state, tmp) : state;
        case TOGGLE_SUBSCRIBE_STATUS_LIST_SUCCESSED:
            let list_tmp = {};
            list_tmp[action.field] = action.data;
            return !_.isUndefined(_.find(action.user_ids, item => item === state.getId())) ? _.assign(state, list_tmp) : state;
        case FETCH_USER_STATUS_SUCCESSED:
        case EMAIL_MANAGEMENT_MAP_USER_STATUS:
            return _.assign(state, {
                user_status: _.find(action.data, item => item.id === state.status),
            });
        default:
            return state;
    }
};

const EmailManagement = (state, action) => {
    if (_.isUndefined(state)) {
        state = {
            fetched: false,
            users: [],
        };
    }
    switch (action.type) {
        case FETCH_USERS_SUCCESSED:
            return _.assign(state, {
                fetched: true,
                users: _.map(action.data, item => User(undefined, {
                    data: item
                })),
                pagination: action.pagination,
            });
        case EMAIL_MAMAGEMENT_SELECT_USER_STATUS_FILTER:
        case EMAIL_MAMAGEMENT_UPDATE_SELECTED_USER_STATUS:
            return _.assign(state, {
                selected_status: action.data,
            });
        case EMAIL_MANAGEMENT_SET_SELECT_ALL:
        case TOGGLE_SUBSCRIBE_STATUS_SUCCESSED:
        case TOGGLE_SUBSCRIBE_STATUS_LIST_SUCCESSED:
        case FETCH_USER_STATUS_SUCCESSED:
        case EMAIL_MANAGEMENT_MAP_USER_STATUS:
            return _.assign(state, {
                users: _.map(state.users, item => User(item, action)),
            });
        default:
            return state;
    }
};


module.exports = {
    EmailManagement,
};
