(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.email_management', {
                url: '/email-management?page&search&status',
                views: {
                    'content@app': {
                        templateUrl: "/webapp/js/admin/components/email_management/_email_management.tpl.html",
                        controller: 'AdminEmailManagementCtrl',
                        controllerAs: 'vm',
                    }
                }
            });
    }]);
})(angular.module('app.components.email_management.router', []));
