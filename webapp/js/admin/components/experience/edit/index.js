(function(app) {

    app.controller('AdminEditExperienceCtrl', AdminEditExperienceCtrl);

    AdminEditExperienceCtrl.$inject = ['$rootScope', '$scope', 'API', 'experience', '$state', '$stateParams', 'Notification', 'preloader'];

    function AdminEditExperienceCtrl($rootScope, $scope, API, experience, $state, $stateParams, Notification, preloader) {
        var vm = this;

        vm.experience = experience;

        vm.editExperience = editExperience;

        function editExperience(experience) {
            preloader.show();
            API.admin.experience.update(experience)
                .then(function(response) {
                    preloader.hide();
                    vm.edit_experience_form.$setPristine();
                    Notification.show('success', 'Updated success!', 5000);
                })
                .catch(function(error) {
                    preloader.hide();
                    vm.edit_experience_form.$setPristine();
                    throw error;
                });
        }
    }

})(angular.module('app.components.experience.edit', []));