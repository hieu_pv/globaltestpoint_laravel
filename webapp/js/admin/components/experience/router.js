(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.experience', {
                url: '/experience',
                views: {
                    'content@app': {
                        templateUrl: "/webapp/js/admin/components/experience/_experience.tpl.html",
                        controller: 'AdminExperienceCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.experience.list', {
                url: '/list?page&search&order_by&constraints',
                views: {
                    'admin-experience@app.experience': {
                        templateUrl: "/webapp/js/admin/components/experience/list/_list.tpl.html",
                        controller: 'AdminListExperiencesCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.experience.create', {
                url: '/create',
                views: {
                    'admin-experience@app.experience': {
                        templateUrl: "/webapp/js/admin/components/experience/create/_create.tpl.html",
                        controller: 'AdminCreateExperienceCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.experience.edit', {
                url: '/:id/edit',
                views: {
                    'admin-experience@app.experience': {
                        templateUrl: "/webapp/js/admin/components/experience/edit/_edit.tpl.html",
                        controller: 'AdminEditExperienceCtrl',
                        controllerAs: 'vm',
                    }
                },
                resolve: {
                    experience: ['API', '$stateParams', function(API, $stateParams) {
                        return API.admin.experience.show($stateParams.id);
                    }]
                }
            });
    }]);
})(angular.module('app.components.experience.router', []));