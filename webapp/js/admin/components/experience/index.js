require('./router');
require('./list/');
require('./create/');
require('./edit/');

(function(app) {

    app.controller('AdminExperienceCtrl', AdminExperienceCtrl);
    AdminExperienceCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API'];

    function AdminExperienceCtrl($rootScope, $scope, $state, $stateParams, API) {}
    
})(angular.module('app.components.experience', [
    'app.components.experience.router',
    'app.components.experience.list',
    'app.components.experience.create',
    'app.components.experience.edit'
]));
