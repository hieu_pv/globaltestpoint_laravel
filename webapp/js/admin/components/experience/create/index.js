(function(app) {

    app.controller('AdminCreateExperienceCtrl', AdminCreateExperienceCtrl);

    AdminCreateExperienceCtrl.$inject = ['$rootScope', '$scope', 'API', 'preloader', 'Notification'];

    function AdminCreateExperienceCtrl($rootScope, $scope, API, preloader, Notification) {
        var vm = this;

        vm.createExperience = createExperience;

        function createExperience(experience) {
            preloader.show();
            API.admin.experience.create(experience)
                .then(function(response) {
                    preloader.hide();
                    vm.create_experience_form.$setPristine();
                    vm.experience = null;
                    Notification.show('success', 'Added success!', 5000);
                })
                .catch(function(error) {
                    preloader.hide();
                    vm.create_experience_form.$setPristine();
                    vm.experience = null;
                    throw error;
                });
        }
    }

})(angular.module('app.components.experience.create', []));