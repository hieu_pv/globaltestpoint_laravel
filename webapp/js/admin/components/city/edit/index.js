(function(app) {

    app.controller('AdminEditCityCtrl', AdminEditCityCtrl);

    AdminEditCityCtrl.$inject = ['$rootScope', '$scope', 'API', 'city', 'states', '$state', '$stateParams', 'Notification', 'preloader'];

    function AdminEditCityCtrl($rootScope, $scope, API, city, states, $state, $stateParams, Notification, preloader) {
        var vm = this;

        vm.states = states;
        vm.city = city;
        vm.city.state = _.find(vm.states, { id: vm.city.getStateId() });

        vm.editCity = editCity;

        function editCity(city) {
            city.state_id = city.state.getId();
            
            preloader.show();
            API.admin.city.update(city)
                .then(function(response) {
                    preloader.hide();
                    vm.edit_city_form.$setPristine();
                    Notification.show('success', 'Updated success!', 5000);
                })
                .catch(function(error) {
                    preloader.hide();
                    vm.edit_city_form.$setPristine();
                    throw error;
                });
        }
    }

})(angular.module('app.components.city.edit', []));