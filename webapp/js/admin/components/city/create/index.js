(function(app) {

    app.controller('AdminCreateCityCtrl', AdminCreateCityCtrl);

    AdminCreateCityCtrl.$inject = ['$rootScope', '$scope', 'states', 'API', 'preloader', 'Notification'];

    function AdminCreateCityCtrl($rootScope, $scope, states, API, preloader, Notification) {
        var vm = this;

        vm.states = states;

        vm.createCity = createCity;

        function createCity(city) {
            city.state_id = city.state.getId();
            
            preloader.show();
            API.admin.city.create(city)
                .then(function(response) {
                    preloader.hide();
                    vm.create_city_form.$setPristine();
                    vm.city = null;
                    Notification.show('success', 'Added success!', 5000);
                })
                .catch(function(error) {
                    preloader.hide();
                    vm.create_city_form.$setPristine();
                    vm.city = null;
                    throw error;
                });
        }
    }

})(angular.module('app.components.city.create', []));