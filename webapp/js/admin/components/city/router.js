(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.city', {
                url: '/city',
                views: {
                    'content@app': {
                        templateUrl: "/webapp/js/admin/components/city/_city.tpl.html",
                        controller: 'AdminCityCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.city.list', {
                url: '/list?page&search&order_by&constraints',
                views: {
                    'admin-city@app.city': {
                        templateUrl: "/webapp/js/admin/components/city/list/_list.tpl.html",
                        controller: 'AdminListCitiesCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.city.create', {
                url: '/create',
                views: {
                    'admin-city@app.city': {
                        templateUrl: "/webapp/js/admin/components/city/create/_create.tpl.html",
                        controller: 'AdminCreateCityCtrl',
                        controllerAs: 'vm',
                    }
                },
                resolve: {
                    states: ['API', function(API) {
                        return API.admin.state.getAll();
                    }]
                }
            })
            .state('app.city.edit', {
                url: '/:id/edit',
                views: {
                    'admin-city@app.city': {
                        templateUrl: "/webapp/js/admin/components/city/edit/_edit.tpl.html",
                        controller: 'AdminEditCityCtrl',
                        controllerAs: 'vm',
                    }
                },
                resolve: {
                    city: ['API', '$stateParams', function(API, $stateParams) {
                        return API.admin.city.show($stateParams.id);
                    }],
                    states: ['API', function(API) {
                        return API.admin.state.getAll();
                    }]
                }
            });
    }]);
})(angular.module('app.components.city.router', []));