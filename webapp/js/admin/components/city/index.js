require('./router');
require('./list/');
require('./create/');
require('./edit/');

(function(app) {

    app.controller('AdminCityCtrl', AdminCityCtrl);
    AdminCityCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API'];

    function AdminCityCtrl($rootScope, $scope, $state, $stateParams, API) {}
    
})(angular.module('app.components.city', [
    'app.components.city.router',
    'app.components.city.list',
    'app.components.city.create',
    'app.components.city.edit'
]));
