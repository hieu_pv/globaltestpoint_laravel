require('./router');
require('./list/');
require('./create/');
require('./edit/');

(function(app) {

    app.controller('AdminCompanyCtrl', AdminCompanyCtrl);
    AdminCompanyCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'API'];

    function AdminCompanyCtrl($rootScope, $scope, $state, $stateParams, API) {}
    
})(angular.module('app.components.company', [
    'app.components.company.router',
    'app.components.company.list',
    'app.components.company.create',
    'app.components.company.edit'
]));
