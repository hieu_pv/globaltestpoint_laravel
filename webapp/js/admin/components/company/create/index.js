(function(app) {

    app.controller('AdminCreateCompanyCtrl', AdminCreateCompanyCtrl);
    AdminCreateCompanyCtrl.$inject = ['$rootScope', '$scope', '$stateParams', 'API', 'Notification', 'preloader', 'Modal', '$state'];

    function AdminCreateCompanyCtrl($rootScope, $scope, $stateParams, API, Notification, preloader, Modal, $state) {
        var vm = this;

        API.country.get()
            .then((countries) => _.filter(countries, (item) => item.is_supported_phone_area))
            .then((countries) => {
                vm.countries = _.map(countries, (item) => {
                    item.display = function() {
                        return `${this.name}(+${this.phone_area_code})`;
                    };
                    return item;
                });
                vm.country = vm.countries[0];
            })
            .catch((error) => {
                throw error;
            });

        vm.registerEmployerProfile = function(user) {
            preloader.show();
            user.phone_area_code = vm.country.phone_area_code;
            user.country = vm.country.name;
            API.admin.user.createEmployerProfile(user)
                .then(function(result) {
                    preloader.hide();
                    Notification.show('success', 'create company success', 5000);
                    vm.user = null;
                    vm.employer_register_form.$setPristine();
                })
                .catch(function(error) {
                    preloader.hide();
                    throw error;
                });
        };
    }

})(angular.module('app.components.company.create', []));
