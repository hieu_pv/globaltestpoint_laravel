(function(app) {

    app.controller('AdminListCompanyCtrl', AdminListCompanyCtrl);
    AdminListCompanyCtrl.$inject = ['$rootScope', '$scope', 'API', '$state', '$stateParams', 'Notification', 'Modal', 'tags', 'user_status', '$window', '$cookieStore', '$cookies', 'preloader'];

    function AdminListCompanyCtrl($rootScope, $scope, API, $state, $stateParams, Notification, Modal, tags, user_status, $window, $cookieStore, $cookies, preloader) {
        var vm = this;
        vm.params = $stateParams;
        vm.params.roles = 'employer';
        vm.params.per_page = 100;

        vm.tags = tags;

        vm.user_status = user_status;
        vm.user_status.unshift({
            status: 'All'
        });

        vm.loginAsUser = loginAsUser;

        vm.filterStatus = function(status) {
            if (status.status === 'All') {
                let params = $stateParams;
                if (!_.isUndefined(params.status)) {
                    delete params.status;
                }
                params.page = 1;
                $state.go($state.current, params, {
                    inherit: false,
                    reload: $state.current.name,
                });
            } else {
                $state.go($state.current, {
                    page: 1,
                    status: status.id
                });
            }
        };


        if (!_.isUndefined($stateParams.status)) {
            vm.status = _.find(vm.user_status, (item) => parseInt(item.id) === parseInt($stateParams.status));
        } else {
            vm.status = _.head(vm.user_status);
        }

        API.admin.user.get(vm.params)
            .then(function(result) {
                vm.users = result.users;
                vm.pagination = result.pagination;
                if (vm.users.length > 0) {
                    _.forEach(vm.users, function(user) {
                        let status = _.find(vm.user_status, (i) => parseInt(i.id) === parseInt(user.status));
                        user.user_status = _.isUndefined(status) ? '' : status.status;
                    });
                }
            })
            .catch(function(error) {
                throw error;
            });

        vm.searchPage = function(search) {
            $state.go($state.current, {
                page: 1,
                search: search
            }, {
                reload: true
            });
        };

        vm.searchKeyEvent = function(event, search) {
            if (event.which === 13) {
                $state.go($state.current, {
                    page: 1,
                    search: search
                }, {
                    reload: true
                });
            }
        };

        if (!_.isUndefined($stateParams.tag)) {
            var tagsParam = $stateParams.tag.split(',');
            var v = _.filter(vm.tags, function(item) {
                var data = _.find(tagsParam, function(tag) {
                    return parseInt(tag) === item.id;
                });
                return _.isUndefined(data) ? false : true;
            });
            vm.tag = v;
        }

        vm.changeTag = function(tags) {
            var tag_ids = _.map(tags, 'id').join(',');
            $state.go($state.current, {
                page: 1,
                tag: tag_ids
            });
        };

        vm.updateSelectedUser = function() {
            vm.selectedUser = _.filter(vm.users, {
                selected: true
            });
        };

        vm.selectAll = function() {
            $scope.$watch('vm.users', function(users) {
                if (!_.isUndefined(users)) {
                    _.forEach(users, function(user) {
                        if (vm.selectedAll) {
                            user.selected = true;
                        } else {
                            user.selected = false;
                        }
                    });
                    vm.selectedUser = _.filter(users, {
                        selected: true
                    });
                }
            });
        };

        /**
         * active, pending or banned
         * @param String type ('active', 'pending', 'banned')
         */
        vm.changeStatusAllUser = function(type) {
            vm.selectedUser = _.filter(vm.users, {
                selected: true
            });
            if (vm.selectedUser.length === 0) {
                Notification.show('warning', 'Please select at least one company', 3000);
                return false;
            } else {
                var filterData = _.map(vm.selectedUser, 'id');
                API.admin.user.changeStatusAllUser(filterData, type)
                    .then(function(response) {
                        Notification.show('success', 'Update company success!', 3000);
                        $state.reload($state.current.name);
                        vm.selectedAll = false;
                        vm.selectAll();
                    })
                    .catch(function(error) {
                        throw error;
                    });
            }
        };

        vm.editUser = function(id) {
            API.admin.user.show(id, ['roles'])
                .then(function(response) {
                    vm.user = response;
                    if (!vm.user.isEmployer() && !vm.user.isEmployee()) {
                        Notification.show('warning', 'You do not have permission to edit this company!', 3000);
                        return false;
                    } else {
                        $state.go('app.user.edit', {
                            id: id
                        });
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        };

        vm.deleteUser = function(user) {
            API.admin.user.show(user.id, ['roles'])
                .then(function(response) {
                    vm.user = response;
                    if (!vm.user.isEmployer() && !vm.user.isEmployee()) {
                        Notification.show('warning', 'You do not have permission to delete this company!', 3000);
                        return false;
                    } else {
                        var content = 'Are you sure you want to remove ' + user.getEmail();
                        Modal.confirm({
                            title: 'Warning',
                            content: content,
                            ok: 'YES',
                            cancel: 'NO',
                        }, function() {
                            API.admin.user.deleteCompany(user.id)
                                .then(function(response) {
                                    _.remove(vm.users, user);
                                    preloader.hide();
                                })
                                .catch(function(error) {
                                    throw error;
                                });
                        }, function() {});
                    }
                })
                .catch(function(error) {
                    throw error;
                });
        };

        function loginAsUser(user_id) {
            var redirectWindow = $window.open('', '_blank');
            preloader.show();
            API.admin.user.loginAsUser({
                    id: user_id
                })
                .then(function(response) {
                    preloader.hide();
                    $cookies.remove(USER_JWT_TOKEN_COOKIE_KEY, {
                        path: '/'
                    });
                    $cookies.put(USER_JWT_TOKEN_COOKIE_KEY, `"${response.token}"`, {
                        path: '/',
                    });
                    $cookies.put(ADMIN_SESSION, `${response.admin_session}`, {
                        path: '/',
                    });
                    $cookies.put(ADMIN_PROFILE, `${response.admin_profile}`, {
                        path: '/',
                    });
                    redirectWindow.location = '/dashboard/jobs/all';
                })
                .catch(function(error) {
                    throw error;
                });
        }
    }

})(angular.module('app.components.company.list', []));