(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.companies', {
                url: '/companies',
                views: {
                    'content@app': {
                        templateUrl: "/webapp/js/admin/components/company/_admin_companies.tpl.html",
                        controller: 'AdminCompanyCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.companies.all', {
                url: '/all?page&list&status&search&order_by&tag',
                views: {
                    'admin-companies@app.companies': {
                        templateUrl: "/webapp/js/admin/components/company/list/_list.tpl.html",
                        controller: 'AdminListCompanyCtrl',
                        controllerAs: 'vm',
                    }
                },
                resolve: {
                    user_status: ['API', function(API) {
                        return API.admin.user.getStatus();
                    }],
                    tags: ['API', function(API) {
                        return API.admin.tag.get({
                                active: 1
                            })
                            .then(function(tags) {
                                return tags;
                            })
                            .catch(function(error) {
                                return [];
                            });

                    }]
                }
            })
            .state('app.companies.create', {
                url: '/create',
                views: {
                    'admin-companies@app.companies': {
                        templateUrl: "/webapp/js/admin/components/company/create/_create.tpl.html",
                        controller: 'AdminCreateCompanyCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.companies.edit', {
                url: '/:id/edit',
                views: {
                    'admin-companies@app.companies': {
                        templateUrl: "/webapp/js/admin/components/company/edit/_edit.tpl.html",
                        controller: 'AdminEditCompanyCtrl',
                        controllerAs: 'vm',
                    }
                },
                resolve: {
                    profile: ['API', '$stateParams', function(API, $stateParams) {
                        return API.admin.user.show($stateParams.id);
                    }]
                }
            });
    }]);

})(angular.module('app.components.company.router', []));
