(function(app) {

    app.controller('AdminEditCompanyCtrl', AdminEditCompanyCtrl);
    AdminEditCompanyCtrl.$inject = ['$rootScope', '$scope', 'profile', '$state', '$stateParams', 'Notification', 'API', 'preloader', 'Modal'];

    function AdminEditCompanyCtrl($rootScope, $scope, profile, $state, $stateParams, Notification, API, preloader, Modal) {
        var vm = this;
        
        vm.updateStatus = updateStatus;
        vm.avatarUploaded = avatarUploaded;
        vm.update = update;

        vm.profile = profile;

        API.admin.user.getStatus()
            .then((status) => {
                vm.user_status = status;
            })
            .catch((error) => {
                throw error;
            });

        function avatarUploaded(response) {
            vm.avatar_upload_percentage = true;
            API.admin.user.updateImage(vm.profile.getId(), {
                type: 'avatar',
                url: response.data.path
            }).then(() => {
                vm.profile.setAvatar(response.data.path);
                vm.avatar_upload_percentage = false;
            }).catch((error) => {
                vm.avatar_upload_percentage = false;
                throw error;
            });
        }

        API.country.get()
            .then((countries) => _.filter(countries, (item) => item.is_supported_phone_area))
            .then((countries) => {
                vm.countries = _.map(countries, (item) => {
                    item.display = function() {
                        return `${this.name}(+${this.phone_area_code})`;
                    };
                    return item;
                });
                vm.profile.country = _.find(vm.countries, item => parseInt(item.phone_area_code) === parseInt(vm.profile.getPhoneAreaCode()));
                if (_.isUndefined(vm.profile.country)) {
                    vm.profile.country = vm.countries[0];
                }
            })
            .catch((error) => {
                throw error;
            });

        function update(profile) {
            preloader.show();
            var data = profile.getRaw();
            data.profile_completed_percent = profile.profileCompletedPercent();
            var user_id = profile.getId();
            console.log('data ', data);
            API.admin.user.update_employer(user_id, data)
                .then(function(result) {
                    preloader.hide();
                    Notification.show('success', 'Update Success', 5000);
                    // $state.reload($state.current.name);
                })
                .catch(function(error) {
                    preloader.hide();
                    throw error;
                });
        }

        function updateStatus(profile) {
            preloader.show();
            API.admin.user.employerChangeStatus(profile.getId(), {
                    status: profile.status
                })
                .then(function(result) {
                    preloader.hide();
                    Notification.show('success', 'Success', 5000);
                })
                .catch(function(error) {
                    preloader.hide();
                    throw error;
                });
        }
    }

})(angular.module('app.components.company.edit', []));
