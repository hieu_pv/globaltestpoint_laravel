(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.admin_assign_role', {
                url: '/admin-assign-role?page',
                views: {
                    'content@app': {
                        templateUrl: "/webapp/js/admin/components/admin_assign_role/_admin_assign_role.tpl.html",
                        controller: 'AdminAssignRoleCtrl',
                        controllerAs: 'vm',
                    }
                }
            });
    }]);
})(angular.module('app.components.admin_assign_role.router', []));
