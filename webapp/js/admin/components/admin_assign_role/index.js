require('./router');
(function(app) {

    app.controller('AdminAssignRoleCtrl', AdminAssignRoleCtrl);
    AdminAssignRoleCtrl.$inject = ['$rootScope', '$scope', 'API', '$state', '$stateParams', 'Notification', '$http'];

    function AdminAssignRoleCtrl($rootScope, $scope, API, $state, $stateParams, Notification, $http) {
        var vm = this;

        vm.page = $stateParams.page ? parseInt($stateParams.page) : 1;

        var params = {
            page: vm.page
        };

        API.role.get()
            .then(function(response) {
                vm.roles = _.remove(response, function(role) {
                    return role.slug !== 'superadmin';
                });
            })
            .catch(function(error) {
                throw error;
            });

        API.admin.user.profile(['roles'])
            .then(function(response) {
                vm.userLogged = response;
            })
            .catch(function(error) {
                throw error;
            });

        API.admin.user.get(params)
            .then(function(result) {
                vm.users = result;
                $scope.$watch('vm.roles', function(roles) {
                    if (!_.isUndefined(roles)) {
                        if (vm.users.length > 0) {
                            _.forEach(vm.users, function(user) {
                                var roleUser = user.roles.data[0];
                                user.assignRole = _.find(roles, {
                                    'id': roleUser.id
                                });
                            });
                        }
                    }
                });
            })
            .catch(function(error) {
                throw error;
            });

        $scope.$watch('vm.page', function(page) {
            $state.go($state.current.name, {
                'page': page
            });
        });

        vm.assignRole = function(role, user) {
            var hasPermission = true;
            if (vm.userLogged.is('superadmin') && user.is('superadmin')) {
                hasPermission = false;
            }
            if (vm.userLogged.is('admin')) {
                if (user.is('admin') || user.is('superadmin')) {
                    hasPermission = false;
                }
                if (role.slug === 'superadmin') {
                    hasPermission = false;
                }
            }
            if (!hasPermission) {
                Notification.show('warning', 'You do not have permission to perform this action', 3000);
                return false;
            }
            API.admin.user.assignRole(user.id, {
                    role: role
                })
                .then(function(response) {
                    Notification.show('success', 'assign role success', 3000);
                })
                .catch(function(error) {
                    throw error;
                });
        };
    }

})(angular.module('app.components.admin_assign_role', [
    'app.components.admin_assign_role.router'
]));
