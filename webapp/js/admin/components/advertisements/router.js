(function(app) {
    app.config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('app.advertisements', {
                url: '/advertisements',
                views: {
                    'content@app': {
                        templateUrl: "/webapp/js/admin/components/advertisements/_advertisements.tpl.html",
                        controller: 'AdminAdvertisementsCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.advertisements.list', {
                url: '/list?page&search&order_by&constraints',
                views: {
                    'admin-advertisements@app.advertisements': {
                        templateUrl: "/webapp/js/admin/components/advertisements/list/_list.tpl.html",
                        controller: 'AdminListAdvertisementsCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.advertisements.create', {
                url: '/create',
                views: {
                    'admin-advertisements@app.advertisements': {
                        templateUrl: "/webapp/js/admin/components/advertisements/create/_create.tpl.html",
                        controller: 'AdminCreateAdvertisementCtrl',
                        controllerAs: 'vm',
                    }
                }
            })
            .state('app.advertisements.edit', {
                url: '/:id/edit',
                views: {
                    'admin-advertisements@app.advertisements': {
                        templateUrl: "/webapp/js/admin/components/advertisements/edit/_edit.tpl.html",
                        controller: 'AdminEditAdvertisementCtrl',
                        controllerAs: 'vm',
                    }
                },
                resolve: {
                    advertisement: ['API', '$stateParams', function(API, $stateParams) {
                        return API.admin.advertisement.show($stateParams.id);
                    }]
                }
            });
    }]);
})(angular.module('app.components.advertisements.router', []));