(function(app) {

    app.controller('AdminEditAdvertisementCtrl', AdminEditAdvertisementCtrl);

    AdminEditAdvertisementCtrl.$inject = ['$rootScope', '$scope', 'advertisement', 'API', '$state', '$stateParams', 'Notification', 'preloader'];

    function AdminEditAdvertisementCtrl($rootScope, $scope, advertisement, API, $state, $stateParams, Notification, preloader) {
        var vm = this;

        vm.advertisement = advertisement;

        vm.editAdvertisement = editAdvertisement;
        vm.imageUploaded = imageUploaded;

        function editAdvertisement(advertisement) {
            preloader.show();
            API.admin.advertisement.update(advertisement)
                .then(function(response) {
                    preloader.hide();
                    vm.edit_advertisement_form.$setPristine();
                    Notification.show('success', 'Updated success!', 5000);
                })
                .catch(function(error) {
                    preloader.hide();
                    vm.edit_advertisement_form.$setPristine();
                    throw error;
                });
        }

        function imageUploaded(response) {
            vm.image_upload_percentage = true;
            API.admin.advertisement.updateImage(vm.advertisement.getId(), {
                url: response.data.path
            }).then(() => {
                vm.advertisement.image = response.data.path;
                vm.image_upload_percentage = false;
            }).catch((error) => {
                vm.image_upload_percentage = false;
                throw error;
            });
        }
    }

})(angular.module('app.components.advertisements.edit', []));
