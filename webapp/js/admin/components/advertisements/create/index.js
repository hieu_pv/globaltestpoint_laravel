(function(app) {

    app.controller('AdminCreateAdvertisementCtrl', AdminCreateAdvertisementCtrl);

    AdminCreateAdvertisementCtrl.$inject = ['$rootScope', '$scope', 'API', 'preloader', 'Notification'];

    function AdminCreateAdvertisementCtrl($rootScope, $scope, API, preloader, Notification) {
        var vm = this;

        const POSITION_ABOVE_RELATIVE_SEARCH = 1;
        const POSITION_UNDER_RELATIVE_SEARCH = 2;

        vm.advertisement = {
            position: POSITION_ABOVE_RELATIVE_SEARCH
        };

        vm.createAdvertisement = createAdvertisement;

        function createAdvertisement(advertisement) {
            preloader.show();
            API.admin.advertisement.create(advertisement)
                .then(function(response) {
                    preloader.hide();
                    vm.create_advertisement_form.$setPristine();
                    vm.advertisement = {
                        position: POSITION_ABOVE_RELATIVE_SEARCH
                    };
                    Notification.show('success', 'Added success!', 5000);
                })
                .catch(function(error) {
                    preloader.hide();
                    vm.create_advertisement_form.$setPristine();
                    vm.advertisement = {
                        position: POSITION_ABOVE_RELATIVE_SEARCH
                    };
                    throw error;
                });
        }
    }

})(angular.module('app.components.advertisements.create', []));