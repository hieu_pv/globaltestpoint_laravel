require('./router');
require('./list/');
require('./create/');
require('./edit/');

(function(app) {

    app.controller('AdminAdvertisementsCtrl', AdminAdvertisementsCtrl);

    AdminAdvertisementsCtrl.$inject = ['$rootScope', '$scope', '$state'];

    function AdminAdvertisementsCtrl($rootScope, $scope, $state) {

        var current_state = $state.current.name;
        if (!_.isNil(current_state) && current_state === 'app.advertisements') {
            $state.go('app.advertisements.list', {
                page: 1
            });
        }
    }

})(angular.module('app.components.advertisements', [
    'app.components.advertisements.router',
    'app.components.advertisements.list',
    'app.components.advertisements.create',
    'app.components.advertisements.edit'
]));