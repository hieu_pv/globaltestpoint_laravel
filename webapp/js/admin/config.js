(function(app) {
    app.config(['$httpProvider', 'UshiftAppProvider', function($httpProvider, UshiftAppProvider) {
        $httpProvider.interceptors.push(['$cookieStore', function($cookieStore) {
            return {
                request: function(config) {
                    if (!_.isNil($cookieStore.get(JWT_TOKEN_COOKIE_KEY))) {
                        config.headers.Authorization = 'Bearer ' + $cookieStore.get(JWT_TOKEN_COOKIE_KEY);
                    }
                    return config;
                },
                response: function(response) {
                    if (!_.isNil(response.headers().authorization)) {
                        $cookieStore.put(JWT_TOKEN_COOKIE_KEY, response.headers().authorization.substr(7));
                    }
                    return response;
                }
            };
        }]);

        UshiftAppProvider.currencies([{
            id: 1,
            country: 'Singapore',
            name: 'Singapore Dolar',
            code: 'SGD',
            symbols: '$',

        }]);

        // https://github.com/moment/moment-timezone/blob/develop/data/packed/latest.json
        UshiftAppProvider.timezones([
            "Asia/Singapore|SMT MALT MALST MALT MALT JST SGT SGT|-6T.p -70 -7k -7k -7u -90 -7u -80|012345467|-2Bg6T.p 17anT.p 7hXE dM00 17bO 8Fyu Mspu DTA0|56e5",
            "Etc/UTC|UTC|0|0|",
            "Asia/Ho_Chi_Minh|LMT PLMT ICT IDT JST|-76.E -76.u -70 -80 -90|0123423232|-2yC76.E bK00.a 1h7b6.u 5lz0 18o0 3Oq0 k5b0 aW00 BAM0|90e5",
        ]);
    }]);
})(angular.module('app.config', []));
