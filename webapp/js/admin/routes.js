require('ui-router');

let User = require('../models/User');

(function(app) {
    app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider', function($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {

        $urlRouterProvider.otherwise('/admin/job-listing');

        $stateProvider
            .state('user', {
                views: {
                    'main@': {
                        templateUrl: '/webapp/js/admin/common/partials/layouts/_no_login.tpl.html'
                    }
                }
            })
            .state('app', {
                url: '/admin/job-listing',
                views: {
                    'main@': {
                        templateUrl: '/webapp/js/admin/common/partials/layouts/_master.tpl.html',
                        controller: 'AdminMainController',
                        controllerAs: 'vm',
                    }
                },
                resolve: {
                    user: function() {
                        let user = new User(ADMIN_USER);
                        console.log(user);
                        return user;
                    },
                    // user: ['$rootScope', '$state', '$cookieStore', '$cookies', 'API', function($rootScope, $state, $cookieStore, $cookies, API) {
                    //     if (!_.isUndefined($rootScope.user)) {
                    //         return $rootScope.user;
                    //     } else {
                    //         if (!_.isNil($cookieStore.get(JWT_TOKEN_COOKIE_KEY))) {
                    //             var user = API.admin.user.profile()
                    //                 .then(function(user) {
                    //                     console.log(user);
                    //                     if (user.isEmployer() || user.isEmployee()) {
                    //                         $rootScope.logout();
                    //                         throw {
                    //                             message: 'This user does not has admin permission'
                    //                         };
                    //                     }
                    //                     $rootScope.user = user;
                    //                     $rootScope.isLoggedIn = true;
                    //                     return user;
                    //                 })
                    //                 .catch(function(error) {
                    //                     $state.go('user.login', {
                    //                         redirect: window.location.href
                    //                     });
                    //                 });
                    //             return user;
                    //         } else {
                    //             $cookies.remove(JWT_TOKEN_COOKIE_KEY, {
                    //                 path: '/'
                    //             });
                    //             document.location.href = BASE_URL + 'admin/login?redirect=' + document.location.href;
                    //             return undefined;
                    //         }
                    //     }
                    // }]
                }
            });

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    }]);
})(angular.module('app.router', ['ui.router']));