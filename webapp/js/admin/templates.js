(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/common/partials/_application_paginator.tpl.html',
    '<div class="application-paginator">\n' +
    '    <ul>\n' +
    '        <li>\n' +
    '            <a ng-click="page = page - 1" ng-show="page > 1"><i class="material-icons">keyboard_arrow_left</i></a>\n' +
    '        </li>\n' +
    '        <li>Page <span ng-bind="page"></span> of <span ng-bind="data.getTotalPages()"></span></li>\n' +
    '        <li>\n' +
    '            <a ng-click="page = page + 1" ng-show="page < data.getTotalPages()"><i class="material-icons">keyboard_arrow_right</i></a>\n' +
    '        </li>\n' +
    '    </ul>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/common/partials/_breadcrumb.tpl.html',
    '<nav class="breadcrumb-navbar" ng-if="items | hasItem">\n' +
    '    <div class="container">\n' +
    '        <div class="nav-wrapper">\n' +
    '            <div class="col s12">\n' +
    '                <a ng-repeat="item in items" class="breadcrumb" ng-class="{\'breadcrumb-item-first\': $first}" ng-bind="item.name" ng-href="{{item.href}}"></a>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</nav>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/common/partials/_pagination.tpl.html',
    '<div class="show-entities" ng-show="data.getTotalPages() > 1">\n' +
    '    <p ng-if="minEntity">{{minEntity}} - {{maxEntity}} / {{total}}</p>\n' +
    '    <p ng-if="!minEntity">{{maxEntity}} / {{total}}</p>\n' +
    '</div>\n' +
    '<ul class="pagination text-right pull-right pagination-box" ng-show="data.getTotalPages() > 1">\n' +
    '    <li ng-repeat="page in pages" ng-click="goto(page)" ng-class="{active: page === currentPage}">\n' +
    '        <a ng-bind="page"></a>\n' +
    '    </li>\n' +
    '</ul>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/common/partials/_preloader.tpl.html',
    '<div class="preloader-wrapper active small">\n' +
    '    <div class="spinner-layer spinner-red-only">\n' +
    '        <div class="circle-clipper left">\n' +
    '            <div class="circle"></div>\n' +
    '        </div>\n' +
    '        <div class="gap-patch">\n' +
    '            <div class="circle"></div>\n' +
    '        </div>\n' +
    '        <div class="circle-clipper right">\n' +
    '            <div class="circle"></div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/common/partials/_simple_pagination.tpl.html',
    '<ul class="pagination right pagination-box">\n' +
    '    <li ng-show="page !== 1" class="waves-effect" ng-click="prev()"><a href="javascript:;"><i class="material-icons">chevron_left</i></a></li>\n' +
    '    <li class="waves-effect active">\n' +
    '        Page <span ng-bind="page"></span>\n' +
    '    </li>\n' +
    '    <li class="waves-effect" ng-click="next(nextPage)"><a><i class="material-icons">chevron_right</i></a></li>\n' +
    '</ul>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/acl/_acl.tpl.html',
    '<div class="row acl-tabs">\n' +
    '    <ul class="tabs" material-tabs>\n' +
    '        <li ng-show="user.can(\'view.admins\')" class="tab col s3 m2"><a ui-sref="app.acl.list" ng-class="{\'active\': $state.includes(\'app.acl.list\')}">Users</a></li>\n' +
    '        <li ng-show="user.can(\'view.roles\')" class="tab col s3 m2"><a ui-sref="app.acl.roles" ng-class="{\'active\': $state.includes(\'app.acl.roles\')}">Roles</a></li>\n' +
    '    </ul>\n' +
    '</div>\n' +
    '<div ui-view="acl-admin"></div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/admin_assign_role/_admin_assign_role.tpl.html',
    '<div class="admin-assign-role">\n' +
    '    <table class="striped table-users">\n' +
    '        <thead>\n' +
    '            <tr>\n' +
    '                <th>ID</th>\n' +
    '                <th>Email</th>\n' +
    '                <th>Name</th>\n' +
    '                <th>Mobile</th>\n' +
    '                <th class="right-align">Assign Role</th>\n' +
    '            </tr>\n' +
    '        </thead>\n' +
    '        <tbody>\n' +
    '            <tr ng-repeat="user in vm.users">\n' +
    '                <td ng-bind="user.getId()"></td>\n' +
    '                <td ng-bind="user.getEmail()"></td>\n' +
    '                <td ng-bind="user.getName()"></td>\n' +
    '                <td ng-bind="user.getMobile()"></td>\n' +
    '                <td class="right-align">\n' +
    '                    <select class="roles" ng-model="user.assignRole" ng-change="vm.assignRole(user.assignRole, user)" ng-options="options as options.name for options in vm.roles"></select>\n' +
    '                </td>\n' +
    '            </tr>\n' +
    '        </tbody>\n' +
    '    </table>\n' +
    '    <simple-pagination page="vm.page"></simple-pagination>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/admin_home/_admin_home.tpl.html',
    '<div class="row">\n' +
    '    <div class="admin-dashboard col s12">\n' +
    '        Admin Dashboard page - Feature coming soon :)\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/advertisements/_advertisements.tpl.html',
    '<div id="admin-advertisements" ui-view="admin-advertisements"></div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/change_password_user/change_password_user.tpl.html',
    '<section id="change-password-user">\n' +
    '    <div class="container">\n' +
    '        <div class="row">\n' +
    '            <div class="col s9 offset-s1">\n' +
    '                <div class="edit-right">\n' +
    '                    <h2 class="center-align">Change password for {{vm.profile.getName()}}</h2>\n' +
    '                    <form ng-submit="isValidFormOrScrollToError(vm.change_password_form) && vm.changePassword(vm.password)" name="vm.change_password_form" novalidate>\n' +
    '                        <div class="edit-item">\n' +
    '                            <div class="row">\n' +
    '                                <div class="col s12">\n' +
    '                                    <div class="form-group">\n' +
    '                                        <label>New password *</label>\n' +
    '                                        <input type="password" ng-focus="vm.change_password_form.new_password.$focused = true" class="form-control" name="new_password" ng-model="vm.password.new_password" ng-minlength="6" ng-maxlength="40" required>\n' +
    '                                        <div class="errors">\n' +
    '                                            <span ng-show="vm.change_password_form.$submitted && vm.change_password_form.new_password.$error.required">Required</span>\n' +
    '                                            <span ng-show="vm.change_password_form.$submitted && vm.change_password_form.new_password.$error.minlength">The value is too short</span>\n' +
    '                                            <span ng-show="vm.change_password_form.$submitted && vm.change_password_form.new_password.$error.maxlength">The value is too long</span>\n' +
    '                                            <span ng-show="vm.change_password_form.$submitted && vm.password.new_password !== vm.password.password_confirmation">Two password does not match</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="col s12">\n' +
    '                                    <div class="form-group">\n' +
    '                                        <label>Password confirmation *</label>\n' +
    '                                        <input type="password" ng-focus="vm.change_password_form.password_confirmation.$focused = true" class="form-control" name="password_confirmation" ng-model="vm.password.password_confirmation" ng-minlength="6" ng-maxlength="40" required>\n' +
    '                                        <div class="errors">\n' +
    '                                            <span ng-show="vm.change_password_form.$submitted && vm.change_password_form.password_confirmation.$error.required">Required</span>\n' +
    '                                            <span ng-show="vm.change_password_form.$submitted && vm.change_password_form.password_confirmation.$error.minlength">The value is too short</span>\n' +
    '                                            <span ng-show="vm.change_password_form.$submitted && vm.change_password_form.password_confirmation.$error.maxlength">The value is too long</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="col s12">\n' +
    '                                    <div class="change-password">\n' +
    '                                        <input type="submit" ng-disabled="inProgress" class="btn btn-primary orange-btn-sm" value="Change Password">\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </form>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</section>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/city/_city.tpl.html',
    '<div id="admin-city" ui-view="admin-city"></div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/company/_admin_companies.tpl.html',
    '<div id="admin-companies" ui-view="admin-companies"></div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/email_management/_email_management.tpl.html',
    '<div class="email-management-users">\n' +
    '    <div ng-if="!user.can(\'update.email.management\')">\n' +
    '        <p class="center-align">Permission denied</p>\n' +
    '    </div>\n' +
    '    <div class="list-users" ng-if="user.can(\'update.email.management\')">\n' +
    '        <div class="row action-box">\n' +
    '            <div class="col s12 user-action right-align">\n' +
    '                <ul>\n' +
    '                    <li>\n' +
    '                        <button class="btn bg-green" ng-click="vm.subscribe(\'enable_job_email\')">Subscribe Job Email</button>\n' +
    '                        <button class="btn bg-green" ng-click="vm.subscribe(\'enable_marketing_email\')">Subscribe Marketing Email</button>\n' +
    '                    </li>\n' +
    '                    <li>\n' +
    '                        <button class="btn red" ng-click="vm.unsubscribe(\'enable_job_email\')">Unsubscribe Job Email</button>\n' +
    '                        <button class="btn red" ng-click="vm.unsubscribe(\'enable_marketing_email\')">Unsubscribe Marketing Email</button>\n' +
    '                    </li>\n' +
    '                </ul>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="row filter">\n' +
    '            <div class="col m6 s6 filter-status">\n' +
    '                <label for="status">Status</label>\n' +
    '                <select ng-model="vm.store.getState().EmailManagement.selected_status" ng-change="vm.filterStatus(vm.store.getState().EmailManagement.selected_status)" ng-options="options as options.status for options in vm.store.getState().UserStatus.forFilterSelection"></select>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="row search-box">\n' +
    '            <div class="col s12 m6 select-all">\n' +
    '                <input type="checkbox" id="select-all" ng-model="vm.selectedAll" ng-click="vm.store.dispatch({type: \'EMAIL_MANAGEMENT_SET_SELECT_ALL\', data :vm.selectedAll})">\n' +
    '                <label for="select-all">Select all</label>\n' +
    '                <span class="number-select" ng-show="vm.selectedUser.length > 0"> ( All {{vm.selectedUser.length}} {{vm.selectedUser.length === 1 ? \'item\' : \'items\'}}  on this page are selected.)</span>\n' +
    '            </div>\n' +
    '            <div class="search col m6 s12">\n' +
    '                <div class="search-wrapper card">\n' +
    '                    <input id="search" class="input-search" ng-model="vm.search" placeholder="Search..." ng-keydown="vm.searchKeyEvent($event, vm.search)"><i class="icon-search material-icons" ng-click="vm.searchPage(vm.search)">search</i>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row">\n' +
    '                <div class="col s12 m6">\n' +
    '                    Sort By:\n' +
    '                    <sortable-attributes></sortable-attributes>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <preloader ng-if="!vm.store.getState().EmailManagement.fetched"></preloader>\n' +
    '        <div class="row">\n' +
    '            <div class="col s12">\n' +
    '                <table class="striped table-users" ng-if="vm.store.getState().EmailManagement.fetched">\n' +
    '                    <thead>\n' +
    '                        <tr>\n' +
    '                            <th></th>\n' +
    '                            <th class="clickable id-col">ID <span sortable="id"></span></th>\n' +
    '                            <th class="clickable email-col">Email <span sortable="email"></span></th>\n' +
    '                            <th class="clickable name-col">Name <span sortable="first_name"></span></th>\n' +
    '                            <th class="clickable city-col">City <span sortable="city"></span></th>\n' +
    '                            <th>Status <span sortable="status"></span></th>\n' +
    '                            <th>Date of registration <span sortable="created_at"></span></th>\n' +
    '                            <th>Activated <span sortable="activated"></span></th>\n' +
    '                            <th>Job Email</th>\n' +
    '                            <th>Marketing Email</th>\n' +
    '                        </tr>\n' +
    '                    </thead>\n' +
    '                    <tbody>\n' +
    '                        <tr ng-repeat="u in vm.store.getState().EmailManagement.users">\n' +
    '                            <td>\n' +
    '                                <md-checkbox ng-model="u.selected" aria-label="Select User" ng-change="vm.updateSelectedUser()"></md-checkbox>\n' +
    '                            </td>\n' +
    '                            <td ng-bind="u.getId()"></td>\n' +
    '                            <td>\n' +
    '                                <a ui-sref="app.employees.edit({id: u.id})"><span ng-bind="u.getEmail()"></span></a>\n' +
    '                                <i ng-if="u.getEmailVerified()" class="icon-verify material-icons green-text">check_circle</i>\n' +
    '                                <i ng-if="u.getIsLoggedOnApp()" class="material-icons green-text">stay_current_portrait</i>\n' +
    '                            </td>\n' +
    '                            <td>\n' +
    '                                <span ng-bind="u.getName()"></span>\n' +
    '                            </td>\n' +
    '                            <td ng-bind="u.getCity()"></td>\n' +
    '                            <td ng-bind="u.user_status.status"></td>\n' +
    '                            <td ng-bind="u.getCreatedAt() | timeFormat: \'lll\'"></td>\n' +
    '                            <td>\n' +
    '                                <span ng-show="u.getActivated() != \'0000-00-00 00:00:00\'" ng-bind="u.getActivated() | timeFormat: \'DD/MM/YYYY\'"></span>\n' +
    '                            </td>\n' +
    '                            <td>\n' +
    '                                <button style="transition: none" class="btn bg-green" ng-if="!u.getEnableJobEmail()" ng-click="vm.store.dispatch({type: \'TOGGLE_SUBSCRIBE_STATUS\', field: \'enable_job_email\', user_id: u.getId(), data: true})">Subscribe</button>\n' +
    '                                <button style="transition: none" class="btn red" ng-if="u.getEnableJobEmail()" ng-click="vm.store.dispatch({type: \'TOGGLE_SUBSCRIBE_STATUS\', field: \'enable_job_email\', user_id: u.getId(), data: false})">Unsubscribe</button>\n' +
    '                            </td>\n' +
    '                            <td>\n' +
    '                                <button style="transition: none" class="btn bg-green" ng-if="!u.getEnableMarketingEmail()" ng-click="vm.store.dispatch({type: \'TOGGLE_SUBSCRIBE_STATUS\', field: \'enable_marketing_email\', user_id: u.getId(), data: true})">Subscribe</button>\n' +
    '                                <button style="transition: none" class="btn red" ng-if="u.getEnableMarketingEmail()" ng-click="vm.store.dispatch({type: \'TOGGLE_SUBSCRIBE_STATUS\', field: \'enable_marketing_email\', user_id: u.getId(), data: false})">Unubscribe</button>\n' +
    '                            </td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '                <pagination data="vm.store.getState().EmailManagement.pagination"></pagination>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/employee/_admin_employee.tpl.html',
    '<div id="admin-employees" ui-view="admin-employees"></div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/experience/_experience.tpl.html',
    '<div id="admin-experience" ui-view="admin-experience"></div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/industries/_industries.tpl.html',
    '<div id="admin-industries" ui-view="admin-industries"></div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/job_titles/_job_titles.tpl.html',
    '<div id="admin-job-titles" ui-view="admin-job-titles"></div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/jobs/_admin_jobs.tpl.html',
    '<div id="admin-jobs" ui-view="admin-jobs"></div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/login/login.tpl.html',
    '<div id="login" update-text-fields>\n' +
    '    <div class="row">\n' +
    '        <div class="col s12 m6 l4 offset-m3 offset-l4">\n' +
    '            <div class="login-form">\n' +
    '                <div class="row">\n' +
    '                    <form class="col s12" name="vm.loginForm" ng-submit="vm.loginForm.$valid && vm.login(vm.user)" novalidate>\n' +
    '                        <div class="row">\n' +
    '                            <md-input-container class="col s12">\n' +
    '                                <label class="required" for="email">Email</label>\n' +
    '                                <input id="email" type="email" ng-model="vm.user.email" name="email" required ng-maxlength="40">\n' +
    '                                <div class="errors">\n' +
    '                                    <span ng-show="vm.loginForm.$submitted && vm.loginForm.email.$error.required">Required</span>\n' +
    '                                    <span ng-show="vm.loginForm.$submitted && vm.loginForm.email.$error.email">Please enter a valid email</span>\n' +
    '                                    <span ng-show="vm.loginForm.$submitted && vm.loginForm.email.$error.maxlength">The value is too long</span>\n' +
    '                                </div>\n' +
    '                            </md-input-container>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <md-input-container class="col s12">\n' +
    '                                <label class="required" for="email">Password</label>\n' +
    '                                <input id="email" type="password" class="validate" ng-model="vm.user.password" name="password" required ng-maxlength="40">\n' +
    '                                <div class="errors">\n' +
    '                                    <span ng-show="vm.loginForm.$submitted && vm.loginForm.password.$error.required">Required</span>\n' +
    '                                    <span ng-show="vm.loginForm.$submitted && vm.loginForm.password.$error.maxlength">The value is too long</span>\n' +
    '                                </div>\n' +
    '                            </md-input-container>\n' +
    '                        </div>\n' +
    '                        <div class="row login-btn">\n' +
    '                            <button class="btn waves-effect waves-light" ng-class="{\'disabled\': vm.inProgress}" ng-disabled="vm.inProgress" type="submit" name="action">Login\n' +
    '                                <i class="material-icons right">trending_flat</i>\n' +
    '                                <i class="material-icons right icon-pending">replay</i>\n' +
    '                            </button>\n' +
    '                        </div>\n' +
    '                    </form>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/nationality/_nationality.tpl.html',
    '<div id="admin-nationality" ui-view="admin-nationality"></div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/payment/_payment.tpl.html',
    '<div class="row">\n' +
    '    <ul class="tabs" material-tabs>\n' +
    '        <li class="tab col s3 m2"><a ui-sref="app.payment.transfers.list" ng-class="{\'active\': $state.includes(\'app.payment.transfers.list\')}">Transfer</a></li>\n' +
    '        <li class="tab col s3 m2"><a ui-sref="app.payment.transactions.list" ng-class="{\'active\': $state.includes(\'app.payment.transactions.list\')}">Transaction</a></li>\n' +
    '        <li class="tab col s3 m2"><a ui-sref="app.payment.create" ng-class="{\'active\': $state.includes(\'app.payment.create\')}">Create Transfer</a></li>\n' +
    '        <li class="tab col s3 m2"><a ui-sref="app.payment.connected_accounts" ng-class="{\'active\': $state.includes(\'app.payment.connected_accounts\')}">Connected Accounts</a></li>\n' +
    '    </ul>\n' +
    '</div>\n' +
    '<div ui-view="payment"></div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/qualification/_qualification.tpl.html',
    '<div id="admin-qualification" ui-view="admin-qualification"></div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/tag/_tag.tpl.html',
    '<div id="admin-tag" ui-view="admin-tag"></div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/common/partials/layouts/_master.tpl.html',
    '<header ng-class="{\'section-collapse\': !vm.expandSidebar}"></header>\n' +
    '<main ng-class="{\'section-collapse\': !vm.expandSidebar}">\n' +
    '    <div class="admin-header">\n' +
    '        <nav>\n' +
    '            <div class="nav-wrapper">\n' +
    '                <ul class="nav nav-pills nav-stacked side-nav" ng-class="{\'aria-expand\': vm.expandSidebar}">\n' +
    '                    <li role="presentation" ng-class="{\'bold\': !vm.hasSubmenu(item), \'no-padding\': vm.hasSubmenu(item)}" ng-repeat="item in vm.adminSidebarMenus" ui-sref-active="active">\n' +
    '                        <a ng-if="item.state" ui-sref="{{item.state}}" ui-sref-opts="{reload: item.state}" title="{{item.menu}}">\n' +
    '                            <i class="icon {{item.icon}}"></i>\n' +
    '                            <span class="text">{{item.menu}}</span>\n' +
    '                        </a>\n' +
    '                        <a ng-if="!item.state" ng-href="{{ item.custom_url }}" force-reload>\n' +
    '                            <i class="icon {{item.icon}}"></i>\n' +
    '                            <span class="text">{{item.menu}}</span>\n' +
    '                        </a>\n' +
    '                    </li>\n' +
    '                </ul>\n' +
    '                <div class="sidebar-footer" ng-class="{\'footer-expand\': vm.expandSidebar}">\n' +
    '                    <a ng-if="!vm.expandSidebar" ng-click="vm.expandSidebar = !vm.expandSidebar" class="sidebar-footer-toggle" title="Expand sidebar"><i class="icon fa fa-backward"></i></a>\n' +
    '                    <a ng-if="vm.expandSidebar" ng-click="vm.expandSidebar = !vm.expandSidebar" class="sidebar-footer-toggle" title="Collapse sidebar"><i class="icon fa fa-forward"></i></a>\n' +
    '                </div>\n' +
    '                <div class="brand-wrapper text-center">\n' +
    '                    <a href="{{ asset(BASE_URL + \'/admin\') }}" class="brand-logo">\n' +
    '                        <img ng-src="{{ asset(\'/assets/images/button_home.png\')}}" alt="Globaltestpoint logo" />\n' +
    '                    </a>\n' +
    '                </div>\n' +
    '                <!-- <ul class="right">\n' +
    '                    <li>\n' +
    '                        <md-menu md-position-mode="target-right target" md-offset="0 60" width="4">\n' +
    '                            <md-button aria-label="Open phone interactions menu" ng-click="openMenu($mdOpenMenu, $event)">\n' +
    '                                Account\n' +
    '                            </md-button>\n' +
    '                            <md-menu-content width="4">\n' +
    '                                <md-menu-item>\n' +
    '                                    <md-button ng-click="coming()">\n' +
    '                                        Profile\n' +
    '                                    </md-button>\n' +
    '                                </md-menu-item>\n' +
    '                                <md-menu-divider></md-menu-divider>\n' +
    '                                <md-menu-item>\n' +
    '                                    <md-button ng-click="logout()">\n' +
    '                                        Logout\n' +
    '                                    </md-button>\n' +
    '                                </md-menu-item>\n' +
    '                            </md-menu-content>\n' +
    '                        </md-menu>\n' +
    '                    </li>\n' +
    '                </ul> -->\n' +
    '            </div>\n' +
    '        </nav>\n' +
    '    </div>\n' +
    '    <div class="container-fluid">\n' +
    '        <div ui-view="content"></div>\n' +
    '    </div>\n' +
    '</main>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/common/partials/layouts/_no_login.tpl.html',
    '<div class="fixed-header" ng-include="\'/webapp/js/admin/common/partials/layouts/_no_login_header.tpl.html\'"></div>\n' +
    '<div ui-view="content"></div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/common/partials/layouts/_no_login_header.tpl.html',
    '<header class="white">\n' +
    '    <div class="container">\n' +
    '        <nav>\n' +
    '            <div class="nav-wrapper">\n' +
    '                <a ui-sref="app.homepage" class="brand-logo"><img ng-src="{{ asset(\'/assets/images/logo.png\')}}" alt="Ushift logo" /></a>\n' +
    '            </div>\n' +
    '        </nav>\n' +
    '    </div>\n' +
    '    <breadcrumb ng-if="isLoggedIn" items="breadcrumbItems"></breadcrumb>\n' +
    '</header>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/common/partials/modal/_alert.tpl.html',
    '<div class="modal-system">\n' +
    '    <div class="modal-header">\n' +
    '        <h2 class="modal-title" id="modal-title" ng-bind="vm.data.title"></h2>\n' +
    '    </div>\n' +
    '    <div class="modal-footer">\n' +
    '        <button class="btn ui-btn btn-orange" type="button" ng-click="vm.ok()" ng-bind="vm.data.ok"></button>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/common/partials/modal/_confirm.tpl.html',
    '<div class="modal-system">\n' +
    '    <div class="modal-header">\n' +
    '        <h2 class="modal-title" id="modal-title" ng-bind="vm.data.title"></h2>\n' +
    '    </div>\n' +
    '    <div class="modal-body" id="modal-body">\n' +
    '        <p ng-if="vm.data.content" ng-bind="vm.data.content"></p>\n' +
    '        <div ng-if="vm.data.htmlContent" ng-bind-html="vm.data.htmlContent | trustAsHtml" compile-template></div>\n' +
    '    </div>\n' +
    '    <div class="modal-footer">\n' +
    '        <button class="ui-btn btn-white" type="button" ng-click="vm.cancel()" ng-bind="vm.data.cancel"></button>\n' +
    '        <button class="ui-btn btn-orange" type="button" ng-click="vm.ok()" ng-bind="vm.data.ok"></button>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/acl/list/_list.tpl.html',
    '<div class="acl-users">\n' +
    '    <div class="user-action">\n' +
    '        <div class="new-user-box right">\n' +
    '            <a ui-sref="app.acl.list.create" ng-if="user.can(\'create.admins\')" class="add-new bg-box"><i class="material-icons">add</i>Create new user</a>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="col s12">\n' +
    '        <div class="row">\n' +
    '            <div class="users">\n' +
    '                <table class="striped table-users">\n' +
    '                    <thead>\n' +
    '                        <tr>\n' +
    '                            <th>ID</th>\n' +
    '                            <th>Email</th>\n' +
    '                            <th>Name</th>\n' +
    '                            <th>Role</th>\n' +
    '                            <th class="right-align" ng-if="user.can(\'update.admins\')">Action</th>\n' +
    '                        </tr>\n' +
    '                    </thead>\n' +
    '                    <tbody>\n' +
    '                        <tr ng-repeat="u in vm.users">\n' +
    '                            <td ng-bind="u.getId()"></td>\n' +
    '                            <td ng-bind="u.getEmail()"></td>\n' +
    '                            <td ng-bind="u.getName()"></td>\n' +
    '                            <td class="action role">\n' +
    '                                <md-input-container>\n' +
    '                                    <md-select ng-model="u.selected_role" ng-disabled="!user.can(\'update.roles\')" aria-label="role">\n' +
    '                                        <md-option ng-value="role" ng-repeat="role in vm.roles">{{ role.name }}</md-option>\n' +
    '                                    </md-select>\n' +
    '                                </md-input-container>\n' +
    '                                <span ng-show="u|roleChanged"><i class="material-icons">restore_page</i></span>\n' +
    '                            </td>\n' +
    '                            <td class="right-align action">\n' +
    '                                <a ng-if="user.can(\'update.admins\')" ui-sref="app.acl.list.edit({id: u.getId()})" class="edit"><i class="material-icons">mode_edit</i></a>\n' +
    '                                <a ng-if="user.can(\'delete.admins\')" ng-click="vm.deleteUser(u)" class="delete"><i class="material-icons">delete</i></a>\n' +
    '                            </td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="row">\n' +
    '            <div class="left">\n' +
    '                <button ng-click="vm.save(vm.users)" ng-show="vm.users | hasUpdateRole" class="btn waves-effect waves-light " ng-class="{ \'disabled\': vm.inProgress} " ng-disabled="vm.inProgress " type="submit " name="action ">Save\n' +
    '                    <i class="material-icons right ">trending_flat</i>\n' +
    '                    <i class="material-icons right icon-pending ">replay</i>\n' +
    '                </button>\n' +
    '            </div>\n' +
    '            <div class="right">\n' +
    '                <pagination data="vm.pagination"></pagination>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/acl/roles/_acl_roles.tpl.html',
    '<div class="acl-roles">\n' +
    '    <div class="role-action">\n' +
    '        <div class="new-role-box right">\n' +
    '            <a ng-click="createRole($event)" ng-if="user.can(\'create.roles\')" class="add-new bg-box"><i class="material-icons">add</i>Create new role</a>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="users">\n' +
    '        <table class="striped table-users">\n' +
    '            <thead>\n' +
    '                <tr>\n' +
    '                    <th>ID</th>\n' +
    '                    <th>Role</th>\n' +
    '                    <th>Permissions</th>\n' +
    '                    <th class="right-align" ng-if="user.can(\'update.roles\')">Action</th>\n' +
    '                </tr>\n' +
    '            </thead>\n' +
    '            <tbody>\n' +
    '                <tr ng-repeat="role in vm.roles">\n' +
    '                    <td ng-bind="role.getId()"></td>\n' +
    '                    <td ng-bind="role.getName()"></td>\n' +
    '                    <td>\n' +
    '                        <ul class="permissions">\n' +
    '                            <li class="item" ng-repeat="permission in role.getPermissions()">{{permission.getName()}}<i ng-show="user.can(\'update.roles\')" ng-click="vm.removePermission(role, permission)" class="material-icons close">close</i></li>\n' +
    '                        </ul>\n' +
    '                    </td>\n' +
    '                    <td class="right-align actions" ng-if="user.can(\'update.roles\')">\n' +
    '                        <ul>\n' +
    '                            <li ng-if="vm.permissions|availablePermissions:role|hasItem">\n' +
    '                                <md-menu>\n' +
    '                                    <md-button aria-label="role" class="md-icon-button add-permisson" ng-click="vm.openMenu($mdOpenMenu, $event)">\n' +
    '                                        <i class="material-icons">add</i>\n' +
    '                                    </md-button>\n' +
    '                                    <md-menu-content>\n' +
    '                                        <md-menu-item ng-repeat="permission in vm.permissions|availablePermissions:role">\n' +
    '                                            <md-button ng-click="vm.addPermission(role, permission)">{{permission.getName()}}</md-button>\n' +
    '                                        </md-menu-item>\n' +
    '                                    </md-menu-content>\n' +
    '                                </md-menu>\n' +
    '                            </li>\n' +
    '                            <li><a class="edit" ng-click="editRole($event, role)"><i class="material-icons">mode_edit</i></a></li>\n' +
    '                            <li><a class="delete" ng-click="vm.deleteRole(role)"><i class="material-icons">delete</i></a></li>\n' +
    '                        </ul>\n' +
    '                    </td>\n' +
    '                </tr>\n' +
    '            </tbody>\n' +
    '        </table>\n' +
    '    </div>\n' +
    '    <button ng-show="user.can(\'update.roles\')" ng-click="vm.save(vm.roles)" class="btn waves-effect waves-light " ng-class="{ \'disabled\': vm.inProgress} " ng-disabled="vm.inProgress " type="submit " name="action ">Save\n' +
    '        <i class="material-icons right ">trending_flat</i>\n' +
    '        <i class="material-icons right icon-pending ">replay</i>\n' +
    '    </button>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/acl/roles/_create.tpl.html',
    '<div class="create-role-modal">\n' +
    '    <div class="modal-header">\n' +
    '        <h2 class="title">Create Role</h2>\n' +
    '    </div>\n' +
    '    <div class="modal-body">\n' +
    '        <form name="createRoleForm" ng-submit="createRoleForm.$valid && createRole(role)" novalidate>\n' +
    '            <div class="row">\n' +
    '                <md-input-container class="col s12">\n' +
    '                    <label class="required">Role Name *</label>\n' +
    '                    <input type="text" ng-model="role.name" name="name" required ng-maxlength="255">\n' +
    '                    <div class="errors">\n' +
    '                        <span ng-show="createRoleForm.$submitted && createRoleForm.name.$error.required">Required</span>\n' +
    '                        <span ng-show="createRoleForm.$submitted && createRoleForm.name.$error.maxlength">The value is too long</span>\n' +
    '                    </div>\n' +
    '                </md-input-container>\n' +
    '            </div>\n' +
    '            <div class="row action-btn">\n' +
    '                <button class="btn waves-effect waves-light" ng-class="{\'disabled\': inProgress}" type="submit" name="action">create new\n' +
    '                    <i class="material-icons right">trending_flat</i>\n' +
    '                    <i class="material-icons right icon-pending">replay</i>\n' +
    '                </button>\n' +
    '                <a ng-click="cancel()" class="cancel waves-effect waves-light btn right">cancel<i class="material-icons left">skip_previous</i></a>\n' +
    '            </div>\n' +
    '        </form>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/acl/roles/_edit.tpl.html',
    '<div class="edit-role-modal">\n' +
    '    <div class="modal-header">\n' +
    '        <h2 class="title">Edit Role</h2>\n' +
    '    </div>\n' +
    '    <div class="modal-body">\n' +
    '        <form name="editRoleForm" ng-submit="editRoleForm.$valid && editRole(role)" novalidate>\n' +
    '            <div class="row">\n' +
    '                <md-input-container class="col s12">\n' +
    '                    <label class="required">Role Name *</label>\n' +
    '                    <input type="text" ng-model="role.name" name="name" required ng-maxlength="255">\n' +
    '                    <div class="errors">\n' +
    '                        <span ng-show="editRoleForm.$submitted && editRoleForm.name.$error.required">Required</span>\n' +
    '                        <span ng-show="editRoleForm.$submitted && editRoleForm.name.$error.maxlength">The value is too long</span>\n' +
    '                    </div>\n' +
    '                </md-input-container>\n' +
    '            </div>\n' +
    '            <div class="row action-btn">\n' +
    '                <button class="btn waves-effect waves-light" ng-class="{\'disabled\': inProgress}" type="submit" name="action">update\n' +
    '                    <i class="material-icons right">trending_flat</i>\n' +
    '                    <i class="material-icons right icon-pending">replay</i>\n' +
    '                </button>\n' +
    '                <a ng-click="cancel()" class="cancel waves-effect waves-light btn right">cancel<i class="material-icons left">skip_previous</i></a>\n' +
    '            </div>\n' +
    '        </form>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/advertisements/create/_create.tpl.html',
    '<div class="create-page create-advertisement">\n' +
    '    <div class="col-xs-12">\n' +
    '        <div class="create-form create-advertisement-form">\n' +
    '            <h2 class="text-center">Create new Advertisement</h2>\n' +
    '            <form name="vm.create_advertisement_form" ng-submit="vm.create_advertisement_form.$valid && vm.createAdvertisement(vm.advertisement)" novalidate>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Name *</label>\n' +
    '                            <input type="text" ng-focus="vm.create_advertisement_form.name.$focused = true" class="form-control" ng-model="vm.advertisement.name" name="name" required ng-maxlength="255">\n' +
    '                            <div class="icons" ng-show="vm.create_advertisement_form.name.$focused">\n' +
    '                                <i ng-if="!vm.create_advertisement_form.name.$error.required && !vm.create_advertisement_form.name.$error.maxlength" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.create_advertisement_form.name.$error.required || vm.create_advertisement_form.name.$error.maxlength" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.create_advertisement_form.$submitted && vm.create_advertisement_form.name.$error.required">Required</span>\n' +
    '                                <span ng-show="vm.create_advertisement_form.$submitted && vm.create_advertisement_form.name.$error.maxlength">The value is too long</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">URL *</label>\n' +
    '                            <input type="text" ng-focus="vm.create_advertisement_form.url.$focused = true" class="form-control" ng-model="vm.advertisement.url" name="url" required ng-maxlength="255" ng-pattern="/(ftp|http|https):\\/\\/(\\w+:{0,1}\\w*@)?(\\S+)(:[0-9]+)?(\\/|\\/([\\w#!:.?+=&%@!\\-\\/]))?/">\n' +
    '                            <div class="icons" ng-show="vm.create_advertisement_form.url.$focused">\n' +
    '                                <i ng-if="!vm.create_advertisement_form.url.$error.required && !vm.create_advertisement_form.url.$error.maxlength && !vm.create_advertisement_form.url.$error.pattern" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.create_advertisement_form.url.$error.required || vm.create_advertisement_form.url.$error.maxlength || vm.create_advertisement_form.url.$error.pattern" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.create_advertisement_form.$submitted && vm.create_advertisement_form.url.$error.required">Required</span>\n' +
    '                                <span ng-show="vm.create_advertisement_form.$submitted && vm.create_advertisement_form.url.$error.maxlength">The value is too long</span>\n' +
    '                                <span ng-show="vm.create_advertisement_form.$submitted && vm.create_advertisement_form.url.$error.pattern">URL format invalid</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Order</label>\n' +
    '                            <input type="text" ng-focus="vm.create_advertisement_form.order.$focused = true" class="form-control" ng-model="vm.advertisement.order" name="order" ng-pattern="/^\\d+$/">\n' +
    '                            <div class="icons" ng-show="vm.create_advertisement_form.order.$focused">\n' +
    '                                <i ng-if="!vm.create_advertisement_form.order.$error.pattern" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.create_advertisement_form.order.$error.pattern" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.create_advertisement_form.$submitted && vm.create_advertisement_form.order.$error.pattern">Please enter a number</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12">\n' +
    '                        <div class="form-group float-input advertisement-position-wrapper">\n' +
    '                            <label class="active">Position</label>\n' +
    '                            <div class="content">\n' +
    '                                <label class="relative" for="above-relative-search">Display above Related search</label>\n' +
    '                                <input type="radio" id="above-relative-search" ng-focus="vm.create_advertisement_form.position.$focused = true" ng-model="vm.advertisement.position" name="position" ng-value="1">\n' +
    '                                <label class="relative" for="under-relative-search">Display under Related search</label>\n' +
    '                                <input type="radio" id="under-relative-search" ng-focus="vm.create_advertisement_form.position.$focused = true" ng-model="vm.advertisement.position" name="position" ng-value="2">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12">\n' +
    '                        <div class="form-group">\n' +
    '                            <label class="active">Active</label>\n' +
    '                            <input type="checkbox" ng-focus="vm.create_advertisement_form.status.$focused = true" ng-model="vm.advertisement.status" name="status" ng-true-value="1" ng-false-value="0">\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="form-group">\n' +
    '                    <input ng-disabled="inProgress" type="submit" class="btn ui-btn btn-full-width orange-btn-sm" value="Add">\n' +
    '                </div>\n' +
    '            </form>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/advertisements/edit/_edit.tpl.html',
    '<div class="edit-page edit-advertisement">\n' +
    '    <div class="col-xs-12">\n' +
    '        <div class="edit-form edit-advertisement-form">\n' +
    '            <h2 class="text-center">Edit Advertisement</h2>\n' +
    '            <div class="row">\n' +
    '                <div class="col-sm-3">\n' +
    '                    <div class="img-wrapper">\n' +
    '                        <div class="advertisement-image" upload-file="vm.image" accept="png|jpg|jpeg" on-complete="vm.imageUploaded" progress-percentage="vm.image_upload_percentage" in-progress="vm.imageUploadInProgress" upload-type="advertis" data-id="{{ vm.advertisement.getId() }}">\n' +
    '                            <user-profile-image image="vm.advertisement.getImage()" is-update-profile="true"></user-profile-image>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="progress" ng-show="vm.imageUploadInProgress">\n' +
    '                        <div class="determinate" style="width: {{vm.image_upload_percentage}}%">\n' +
    '                            <span ng-if="vm.image_upload_percentage < 100">{{vm.image_upload_percentage}}%</span>\n' +
    '                            <span ng-if="vm.image_upload_percentage == 100">Processing</span>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <a upload-file="vm.image" accept="png|jpg|jpeg" on-complete="vm.imageUploaded" progress-percentage="vm.image_upload_percentage" in-progress="vm.imageUploadInProgress" class="btn btn-primary orange-btn-sm btn-upload-picture">Upload Picture</a>\n' +
    '                </div>\n' +
    '                <div class="col-sm-9">\n' +
    '                    <form name="vm.edit_advertisement_form" ng-submit="vm.edit_advertisement_form.$valid && vm.editAdvertisement(vm.advertisement)" novalidate>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-xs-12">\n' +
    '                                <div class="form-group float-input">\n' +
    '                                    <label class="active">Name *</label>\n' +
    '                                    <input type="text" ng-focus="vm.edit_advertisement_form.name.$focused = true" class="form-control" ng-model="vm.advertisement.name" name="name" required ng-maxlength="255">\n' +
    '                                    <div class="icons" ng-show="vm.edit_advertisement_form.name.$focused">\n' +
    '                                        <i ng-if="!vm.edit_advertisement_form.name.$error.required && !vm.edit_advertisement_form.name.$error.maxlength" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                        <i ng-if="vm.edit_advertisement_form.name.$error.required || vm.edit_advertisement_form.name.$error.maxlength" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                                    </div>\n' +
    '                                    <div class="errors">\n' +
    '                                        <span ng-show="vm.edit_advertisement_form.$submitted && vm.edit_advertisement_form.name.$error.required">Required</span>\n' +
    '                                        <span ng-show="vm.edit_advertisement_form.$submitted && vm.edit_advertisement_form.name.$error.maxlength">The value is too long</span>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-xs-12">\n' +
    '                                <div class="form-group float-input">\n' +
    '                                    <label class="active">URL *</label>\n' +
    '                                    <input type="text" ng-focus="vm.edit_advertisement_form.url.$focused = true" class="form-control" ng-model="vm.advertisement.url" name="url" required ng-maxlength="255" ng-pattern="/(ftp|http|https):\\/\\/(\\w+:{0,1}\\w*@)?(\\S+)(:[0-9]+)?(\\/|\\/([\\w#!:.?+=&%@!\\-\\/]))?/">\n' +
    '                                    <div class="icons" ng-show="vm.edit_advertisement_form.url.$focused">\n' +
    '                                        <i ng-if="!vm.edit_advertisement_form.url.$error.required && !vm.edit_advertisement_form.url.$error.maxlength && !vm.edit_advertisement_form.url.$error.pattern" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                        <i ng-if="vm.edit_advertisement_form.url.$error.required || vm.edit_advertisement_form.url.$error.maxlength || vm.edit_advertisement_form.url.$error.pattern" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                                    </div>\n' +
    '                                    <div class="errors">\n' +
    '                                        <span ng-show="vm.edit_advertisement_form.$submitted && vm.edit_advertisement_form.url.$error.required">Required</span>\n' +
    '                                        <span ng-show="vm.edit_advertisement_form.$submitted && vm.edit_advertisement_form.url.$error.maxlength">The value is too long</span>\n' +
    '                                        <span ng-show="vm.edit_advertisement_form.$submitted && vm.edit_advertisement_form.url.$error.pattern">URL format invalid</span>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-xs-12">\n' +
    '                                <div class="form-group float-input">\n' +
    '                                    <label class="active">Order</label>\n' +
    '                                    <input type="text" ng-focus="vm.edit_advertisement_form.order.$focused = true" class="form-control" ng-model="vm.advertisement.order" name="order" ng-pattern="/^\\d+$/">\n' +
    '                                    <div class="icons" ng-show="vm.edit_advertisement_form.order.$focused">\n' +
    '                                        <i ng-if="!vm.edit_advertisement_form.order.$error.pattern" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                        <i ng-if="vm.edit_advertisement_form.order.$error.pattern" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                                    </div>\n' +
    '                                    <div class="errors">\n' +
    '                                        <span ng-show="vm.edit_advertisement_form.$submitted && vm.edit_advertisement_form.order.$error.pattern">Please enter a number</span>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-xs-12">\n' +
    '                                <div class="form-group float-input advertisement-position-wrapper">\n' +
    '                                    <label class="active">Position</label>\n' +
    '                                    <div class="content">\n' +
    '                                        <label class="relative" for="above-relative-search">Display above Related search</label>\n' +
    '                                        <input type="radio" id="above-relative-search" ng-focus="vm.edit_advertisement_form.position.$focused = true" ng-model="vm.advertisement.position" name="position" ng-value="1">\n' +
    '                                        <label class="relative" for="under-relative-search">Display under Related search</label>\n' +
    '                                        <input type="radio" id="under-relative-search" ng-focus="vm.edit_advertisement_form.position.$focused = true" ng-model="vm.advertisement.position" name="position" ng-value="2">\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-xs-12">\n' +
    '                                <div class="form-group">\n' +
    '                                    <label class="active">Active</label>\n' +
    '                                    <input type="checkbox" ng-focus="vm.edit_advertisement_form.status.$focused = true" ng-model="vm.advertisement.status" name="status" ng-true-value="1" ng-false-value="0">\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="form-group">\n' +
    '                            <input ng-disabled="inProgress" type="submit" class="btn ui-btn btn-full-width orange-btn-sm" value="Edit">\n' +
    '                        </div>\n' +
    '                    </form>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/advertisements/list/_list.tpl.html',
    '<div class="list-page list-advertisements">\n' +
    '    <div class="row filter">\n' +
    '        <div class="col-sm-2 col-xs-12 filter-status">\n' +
    '            <label class="ui-label" for="status">Status</label>\n' +
    '            <select class="ui-select" ng-model="vm.status" ng-change="vm.filterStatus(vm.status)" ng-options="options as options.name for options in vm.statuses"></select>\n' +
    '        </div>\n' +
    '        <div class="col-sm-3 col-sm-offset-7 text-right item-action new-record-wrapper">\n' +
    '            <ul>\n' +
    '                <li class="active">\n' +
    '                    <a ng-click="vm.changeStatusAllItem(\'active\')" title="Active">\n' +
    '                        <i class="icon fa fa-check-circle"></i>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '                <li class="pending">\n' +
    '                    <a ng-click="vm.changeStatusAllItem(\'pending\')" title="Pending">\n' +
    '                        <i class="icon fa fa-pause-circle"></i>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '                <li class="new-item">\n' +
    '                    <a ui-sref="app.advertisements.create" title="Create new Job Title">\n' +
    '                        <i class="icon fa fa-plus-circle"></i>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '            </ul>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row search-box">\n' +
    '        <div class="col-xs-12 col-sm-6 select-all">\n' +
    '            <input type="checkbox" id="select-all" ng-model="vm.selectedAll" ng-change="vm.selectAll()">\n' +
    '            <label for="select-all">Select all</label>\n' +
    '            <span class="number-select" ng-show="vm.selectedItem.length > 0"> ( All {{vm.selectedItem.length}} {{vm.selectedItem.length === 1 ? \'item\' : \'items\'}}  on this page are selected.)</span>\n' +
    '        </div>\n' +
    '        <div class="search col-xs-12 col-sm-6">\n' +
    '            <div class="search-wrapper card">\n' +
    '                <input id="search" class="input-search form-control" ng-model="vm.params.search" placeholder="Search..." ng-keydown="vm.searchKeyEvent($event, vm.params.search)"><i class="icon-search fa fa-search" ng-click="vm.searchPage(vm.params.search)"></i>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- <div class="row">\n' +
    '        <div class="col-xs-12 col-sm-6">\n' +
    '            Sort By:\n' +
    '            <sortable-attributes></sortable-attributes>\n' +
    '        </div>\n' +
    '    </div> -->\n' +
    '    <preloader ng-if="vm.advertisements | isUndefined"></preloader>\n' +
    '    <div class="row">\n' +
    '        <div class="col-xs-12">\n' +
    '            <table class="table table-striped ui-table table-items table-job-titles" ng-if="vm.advertisements !== undefined">\n' +
    '                <thead>\n' +
    '                    <tr>\n' +
    '                        <th></th>\n' +
    '                        <th>ID <!-- <span sortable="id"></span> --></th>\n' +
    '                        <th>Image</th>\n' +
    '                        <th>Name <!-- <span sortable="name"></span> --></th>\n' +
    '                        <th>Position</th>\n' +
    '                        <th>Order <!-- <span sortable="order"></span> --></th>\n' +
    '                        <th>Active <!-- <span sortable="status"></span> --></th>\n' +
    '                        <th>Status</th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody>\n' +
    '                    <tr ng-repeat="advertisement in vm.advertisements">\n' +
    '                        <td>\n' +
    '                            <input type="checkbox" ng-model="advertisement.selected" ng-change="vm.updateSelectedItem()">\n' +
    '                        </td>\n' +
    '                        <td ng-bind="advertisement.getId()"></td>\n' +
    '                        <td>\n' +
    '                            <img class="img-thumbnail" ng-if="advertisement.getImage() !== \'\'" ng-src="{{ advertisement.getImage() }}">\n' +
    '                        </td>\n' +
    '                        <td ng-bind="advertisement.getName()"></td>\n' +
    '                        <td>{{ advertisement.getPosition() === 1 ? \'Above Related search\' : \'Under Related search\' }}</td>\n' +
    '                        <td ng-bind="advertisement.getOrder()"></td>\n' +
    '                        <td>\n' +
    '                            {{ advertisement.getStatus() === 1 ? \'Active\' : \'Pending\' }}\n' +
    '                        </td>\n' +
    '                        <td class="text-right action">\n' +
    '                            <a ui-sref="app.advertisements.edit({id: advertisement.getId()})" class="edit">\n' +
    '                                <p class="ui-p"><i class="icon fa fa-pencil-square-o"></i></p>\n' +
    '                            </a>\n' +
    '                            <a ng-click="vm.deleteItem(advertisement)" class="delete">\n' +
    '                                <p class="ui-p"><i class="icon fa fa-trash"></i></p>\n' +
    '                            </a>\n' +
    '                        </td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '            <pagination data="vm.pagination"></pagination>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/city/create/_create.tpl.html',
    '<div class="create-page create-city">\n' +
    '    <div class="col-xs-12">\n' +
    '        <div class="create-form create-city-form">\n' +
    '            <h2 class="text-center">Create new City</h2>\n' +
    '            <form name="vm.create_city_form" ng-submit="vm.create_city_form.$valid && vm.createCity(vm.city)" novalidate>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">State *</label>\n' +
    '                            <select name="state" ng-focus="vm.create_city_form.state.$focused = true" class="form-control" ng-model="vm.city.state" name="state" required ng-options="option as option.name for option in vm.states"></select>\n' +
    '                            <div class="icons" ng-show="vm.create_city_form.state.$focused">\n' +
    '                                <i ng-if="!vm.create_city_form.state.$error.required" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.create_city_form.state.$error.required" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.create_city_form.$submitted && vm.create_city_form.state.$error.required">Required</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12 col-sm-6">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Name *</label>\n' +
    '                            <input type="text" ng-focus="vm.create_city_form.name.$focused = true" class="form-control" ng-model="vm.city.name" name="name" required ng-maxlength="255">\n' +
    '                            <div class="icons" ng-show="vm.create_city_form.name.$focused">\n' +
    '                                <i ng-if="!vm.create_city_form.name.$error.required && !vm.create_city_form.name.$error.maxlength" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.create_city_form.name.$error.required || vm.create_city_form.name.$error.maxlength" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.create_city_form.$submitted && vm.create_city_form.name.$error.required">Required</span>\n' +
    '                                <span ng-show="vm.create_city_form.$submitted && vm.create_city_form.name.$error.maxlength">The value is too long</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="col-xs-12 col-sm-6">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Order</label>\n' +
    '                            <input type="text" ng-focus="vm.create_city_form.order.$focused = true" class="form-control" ng-model="vm.city.order" name="order" ng-pattern="/^\\d+$/">\n' +
    '                            <div class="icons" ng-show="vm.create_city_form.order.$focused">\n' +
    '                                <i ng-if="!vm.create_city_form.order.$error.pattern" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.create_city_form.order.$error.pattern" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.create_city_form.$submitted && vm.create_city_form.order.$error.pattern">Please enter a number</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12">\n' +
    '                        <div class="form-group">\n' +
    '                            <label class="active">Active</label>\n' +
    '                            <input type="checkbox" ng-focus="vm.create_city_form.status.$focused = true" ng-model="vm.city.status" name="status" ng-true-value="1" ng-false-value="0">\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="form-group">\n' +
    '                    <input ng-disabled="inProgress" type="submit" class="btn ui-btn btn-full-width orange-btn-sm" value="Add">\n' +
    '                </div>\n' +
    '            </form>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/city/edit/_edit.tpl.html',
    '<div class="edit-page edit-city">\n' +
    '    <div class="col-xs-12">\n' +
    '        <div class="edit-form edit-city-form">\n' +
    '            <h2 class="text-center">Edit City</h2>\n' +
    '            <form name="vm.edit_city_form" ng-submit="vm.edit_city_form.$valid && vm.editCity(vm.city)" novalidate>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">State *</label>\n' +
    '                            <select name="state" ng-focus="vm.edit_city_form.state.$focused = true" class="form-control" ng-model="vm.city.state" name="state" required ng-options="option as option.name for option in vm.states"></select>\n' +
    '                            <div class="icons" ng-show="vm.edit_city_form.state.$focused">\n' +
    '                                <i ng-if="!vm.edit_city_form.state.$error.required" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.edit_city_form.state.$error.required" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.edit_city_form.$submitted && vm.edit_city_form.state.$error.required">Required</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12 col-sm-6">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Name *</label>\n' +
    '                            <input type="text" ng-focus="vm.edit_city_form.name.$focused = true" class="form-control" ng-model="vm.city.name" name="name" required ng-maxlength="255">\n' +
    '                            <div class="icons" ng-show="vm.edit_city_form.name.$focused">\n' +
    '                                <i ng-if="!vm.edit_city_form.name.$error.required && !vm.edit_city_form.name.$error.maxlength" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.edit_city_form.name.$error.required || vm.edit_city_form.name.$error.maxlength" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.edit_city_form.$submitted && vm.edit_city_form.name.$error.required">Required</span>\n' +
    '                                <span ng-show="vm.edit_city_form.$submitted && vm.edit_city_form.name.$error.maxlength">The value is too long</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="col-xs-12 col-sm-6">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Order</label>\n' +
    '                            <input type="text" ng-focus="vm.edit_city_form.order.$focused = true" class="form-control" ng-model="vm.city.order" name="order" ng-pattern="/^\\d+$/">\n' +
    '                            <div class="icons" ng-show="vm.edit_city_form.order.$focused">\n' +
    '                                <i ng-if="!vm.edit_city_form.order.$error.pattern" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.edit_city_form.order.$error.pattern" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.edit_city_form.$submitted && vm.edit_city_form.order.$error.pattern">Please enter a number</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12">\n' +
    '                        <div class="form-group">\n' +
    '                            <label class="active">Active</label>\n' +
    '                            <input type="checkbox" ng-focus="vm.edit_city_form.status.$focused = true" ng-model="vm.city.status" name="status" ng-true-value="1" ng-false-value="0">\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="form-group">\n' +
    '                    <input ng-disabled="inProgress" type="submit" class="btn ui-btn btn-full-width orange-btn-sm" value="Edit">\n' +
    '                </div>\n' +
    '            </form>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/city/list/_list.tpl.html',
    '<div class="list-page list-cities">\n' +
    '    <div class="row filter">\n' +
    '        <div class="col-sm-2 col-xs-12 filter-status">\n' +
    '            <label class="ui-label" for="status">Status</label>\n' +
    '            <select class="ui-select" ng-model="vm.status" ng-change="vm.filterStatus(vm.status)" ng-options="options as options.name for options in vm.statuses"></select>\n' +
    '        </div>\n' +
    '        <div class="col-sm-3 col-sm-offset-7 text-right item-action new-record-wrapper">\n' +
    '            <ul>\n' +
    '                <li class="active">\n' +
    '                    <a ng-click="vm.changeStatusAllItem(\'active\')" title="Active">\n' +
    '                        <i class="icon fa fa-check-circle"></i>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '                <li class="pending">\n' +
    '                    <a ng-click="vm.changeStatusAllItem(\'pending\')" title="Pending">\n' +
    '                        <i class="icon fa fa-pause-circle"></i>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '                <li class="new-item">\n' +
    '                    <a ui-sref="app.city.create" title="Create new City">\n' +
    '                        <i class="icon fa fa-plus-circle"></i>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '            </ul>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row search-box">\n' +
    '        <div class="col-xs-12 col-sm-6 select-all">\n' +
    '            <input type="checkbox" id="select-all" ng-model="vm.selectedAll" ng-change="vm.selectAll()">\n' +
    '            <label for="select-all">Select all</label>\n' +
    '            <span class="number-select" ng-show="vm.selectedItem.length > 0"> ( All {{vm.selectedItem.length}} {{vm.selectedItem.length === 1 ? \'item\' : \'items\'}}  on this page are selected.)</span>\n' +
    '        </div>\n' +
    '        <div class="search col-xs-12 col-sm-6">\n' +
    '            <div class="search-wrapper card">\n' +
    '                <input id="search" class="input-search form-control" ng-model="vm.params.search" placeholder="Search..." ng-keydown="vm.searchKeyEvent($event, vm.params.search)"><i class="icon-search fa fa-search" ng-click="vm.searchPage(vm.params.search)"></i>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- <div class="row">\n' +
    '        <div class="col-xs-12 col-sm-6">\n' +
    '            Sort By:\n' +
    '            <sortable-attributes></sortable-attributes>\n' +
    '        </div>\n' +
    '    </div> -->\n' +
    '    <preloader ng-if="vm.cities | isUndefined"></preloader>\n' +
    '    <div class="row">\n' +
    '        <div class="col-xs-12">\n' +
    '            <table class="table table-striped ui-table table-items table-cities" ng-if="vm.cities !== undefined">\n' +
    '                <thead>\n' +
    '                    <tr>\n' +
    '                        <th></th>\n' +
    '                        <th>ID <!-- <span sortable="id"></span> --></th>\n' +
    '                        <th>Name <!-- <span sortable="name"></span> --></th>\n' +
    '                        <th>State</th>\n' +
    '                        <th>Order <!-- <span sortable="order"></span> --></th>\n' +
    '                        <th>Active <!-- <span sortable="status"></span> --></th>\n' +
    '                        <th>Status</th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody>\n' +
    '                    <tr ng-repeat="city in vm.cities">\n' +
    '                        <td>\n' +
    '                            <input type="checkbox" ng-model="city.selected" ng-change="vm.updateSelectedItem()">\n' +
    '                        </td>\n' +
    '                        <td ng-bind="city.getId()"></td>\n' +
    '                        <td ng-bind="city.getName()"></td>\n' +
    '                        <td ng-bind="city.state.getName()"></td>\n' +
    '                        <td ng-bind="city.getOrder()"></td>\n' +
    '                        <td>\n' +
    '                            {{ city.getStatus() === 1 ? \'Active\' : \'Pending\' }}\n' +
    '                        </td>\n' +
    '                        <td class="text-right action">\n' +
    '                            <a ui-sref="app.city.edit({id: city.getId()})" class="edit">\n' +
    '                                <p class="ui-p"><i class="icon fa fa-pencil-square-o"></i></p>\n' +
    '                            </a>\n' +
    '                            <a ng-click="vm.deleteItem(city)" class="delete">\n' +
    '                                <p class="ui-p"><i class="icon fa fa-trash"></i></p>\n' +
    '                            </a>\n' +
    '                        </td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '            <pagination data="vm.pagination"></pagination>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/company/create/_create.tpl.html',
    '<div id="register" class="create-page employer-register">\n' +
    '    <div class="col-xs-12">\n' +
    '        <div class="employer-register-form">\n' +
    '            <h2 class="text-center">Create new Company</h2>\n' +
    '            <form name="vm.employer_register_form" ng-submit="vm.employer_register_form.$valid && vm.registerEmployerProfile(vm.user)" class="animated fadeInUp" novalidate>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12 col-sm-6">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Company Name *</label>\n' +
    '                            <input type="text" ng-focus="vm.employer_register_form.company_name.$focused = true" class="form-control" ng-model="vm.user.company_name" name="company_name" required ng-maxlength="64">\n' +
    '                            <div class="icons" ng-show="vm.employer_register_form.company_name.$focused">\n' +
    '                                <i ng-if="!vm.employer_register_form.company_name.$error.required && !vm.employer_register_form.company_name.$error.maxlength" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.employer_register_form.company_name.$error.required || vm.employer_register_form.company_name.$error.maxlength" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.employer_register_form.$submitted && vm.employer_register_form.company_name.$error.required">Required</span>\n' +
    '                                <span ng-show="vm.employer_register_form.$submitted && vm.employer_register_form.company_name.$error.maxlength">The value is too long</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="col-xs-12 col-sm-6">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Registration Number *</label>\n' +
    '                            <input type="text" class="form-control" ng-focus="vm.employer_register_form.registration_number.$focused = true" ng-model="vm.user.registration_number" name="registration_number" required ng-maxlength="20">\n' +
    '                            <div class="icons" ng-show="vm.employer_register_form.registration_number.$focused">\n' +
    '                                <i ng-if="!vm.employer_register_form.registration_number.$error.required && !vm.employer_register_form.registration_number.$error.maxlength" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.employer_register_form.registration_number.$error.required || vm.employer_register_form.registration_number.$error.maxlength" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.employer_register_form.$submitted && vm.employer_register_form.registration_number.$error.required">Required</span>\n' +
    '                                <span ng-show="vm.employer_register_form.$submitted && vm.employer_register_form.registration_number.$error.maxlength">The value is too long</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="form-group float-input">\n' +
    '                    <label class="active">Company E-mail *</label>\n' +
    '                    <input id="email" type="email" ng-focus="vm.employer_register_form.email.$focused = true" class="form-control" ng-model="vm.user.email" name="email" required ng-maxlength="40">\n' +
    '                    <div class="icons" ng-show="vm.employer_register_form.email.$focused">\n' +
    '                        <i ng-if="!vm.employer_register_form.email.$error.required && !vm.employer_register_form.email.$error.maxlength && !vm.employer_register_form.email.$error.email" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                        <i ng-if="vm.employer_register_form.email.$error.required || vm.employer_register_form.email.$error.maxlength || vm.employer_register_form.email.$error.email" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                    </div>\n' +
    '                    <div class="errors">\n' +
    '                        <span ng-show="vm.employer_register_form.$submitted && vm.employer_register_form.email.$error.required">Required</span>\n' +
    '                        <span ng-show="vm.employer_register_form.$submitted && vm.employer_register_form.email.$error.email">Please enter a valid email</span>\n' +
    '                        <span ng-show="vm.employer_register_form.$submitted && vm.employer_register_form.email.$error.maxlength">The value is too long</span>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="form-group float-input">\n' +
    '                    <label class="active">Website URL</label>\n' +
    '                    <input id="website" type="text" ng-focus="vm.employer_register_form.website.$focused = true" class="form-control" ng-model="vm.user.website" name="website" ng-pattern="/(ftp|http|https):\\/\\/(\\w+:{0,1}\\w*@)?(\\S+)(:[0-9]+)?(\\/|\\/([\\w#!:.?+=&%@!\\-\\/]))?/">\n' +
    '                    <div class="icons" ng-show="vm.employer_register_form.website.$focused">\n' +
    '                        <i ng-if="!vm.employer_register_form.website.$error.pattern" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                        <i ng-if="vm.employer_register_form.website.$error.pattern" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                    </div>\n' +
    '                    <div class="errors">\n' +
    '                        <span ng-show="vm.employer_register_form.$submitted && vm.employer_register_form.website.$error.pattern">Website URL format invalid</span>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="form-group float-input">\n' +
    '                    <label class="active">Company Address *</label>\n' +
    '                    <input type="text" ng-focus="vm.employer_register_form.company_address.$focused = true" ng-model="vm.user.company_address" class="form-control" name="company_address" required>\n' +
    '                    <div class="icons" ng-show="vm.employer_register_form.company_address.$focused">\n' +
    '                        <i ng-if="!vm.employer_register_form.company_address.$error.required" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                        <i ng-if="vm.employer_register_form.company_address.$error.required" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                    </div>\n' +
    '                    <div class="errors">\n' +
    '                        <span ng-show="vm.employer_register_form.$submitted && vm.employer_register_form.company_address.$error.required">Required</span>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="form-group float-input">\n' +
    '                    <label class="active">Contact Name *</label>\n' +
    '                    <input type="text" ng-focus="vm.employer_register_form.contact_name.$focused = true" ng-model="vm.user.contact_name" class="form-control" name="contact_name" required ng-maxlength="40">\n' +
    '                    <div class="icons" ng-show="vm.employer_register_form.contact_name.$focused">\n' +
    '                        <i ng-if="!vm.employer_register_form.contact_name.$error.required && !vm.employer_register_form.contact_name.$error.maxlength" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                        <i ng-if="vm.employer_register_form.contact_name.$error.required || vm.employer_register_form.contact_name.$error.maxlength" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                    </div>\n' +
    '                    <div class="errors">\n' +
    '                        <span ng-show="vm.employer_register_form.$submitted && vm.employer_register_form.contact_name.$error.required">Required</span>\n' +
    '                        <span ng-show="vm.employer_register_form.$submitted && vm.employer_register_form.contact_name.$error.maxlength">The value is too long</span>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="form-group float-input">\n' +
    '                    <label class="active">Contact Position *</label>\n' +
    '                    <input type="text" ng-focus="vm.employer_register_form.contact_position.$focused = true" ng-model="vm.user.contact_position" class="form-control" name="contact_position" required>\n' +
    '                    <div class="icons" ng-show="vm.employer_register_form.contact_position.$focused">\n' +
    '                        <i ng-if="!vm.employer_register_form.contact_position.$error.required" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                        <i ng-if="vm.employer_register_form.contact_position.$error.required" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                    </div>\n' +
    '                    <div class="errors">\n' +
    '                        <span ng-show="vm.employer_register_form.$submitted && vm.employer_register_form.contact_position.$error.required">Required</span>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-6 country-box">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <select name="country" ng-focus="vm.employer_register_form.country.$focused = true" class="form-control select-box" ng-model="vm.country" ng-options="country as country.display() for country in vm.countries" required></select>\n' +
    '                            <div class="icons" ng-show="vm.employer_register_form.country.$focused">\n' +
    '                                <i ng-if="!vm.employer_register_form.country.$error.required" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.employer_register_form.country.$error.require" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.employer_register_form.$submitted && vm.employer_register_form.country.$error.required">Required</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="col-xs-6 phone-box">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Phone No. *</label>\n' +
    '                            <input type="text" ng-focus="vm.employer_register_form.mobile.$focused = true" ng-model="vm.user.mobile" class="form-control" name="mobile" ng-pattern="/^(\\d{6,12})$/" required ng-maxlength="40">\n' +
    '                            <div class="icons" ng-show="vm.employer_register_form.mobile.$focused">\n' +
    '                                <i ng-if="!vm.employer_register_form.mobile.$error.required && !vm.employer_register_form.mobile.$error.maxlength && !vm.employer_register_form.mobile.$error.pattern" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.employer_register_form.mobile.$error.required || vm.employer_register_form.mobile.$error.maxlength || vm.employer_register_form.mobile.$error.pattern" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.employer_register_form.$submitted && vm.employer_register_form.mobile.$error.required">Required</span>\n' +
    '                                <span ng-show="vm.employer_register_form.$submitted && vm.employer_register_form.mobile.$error.maxlength">The value is too long</span>\n' +
    '                                <span ng-show="vm.employer_register_form.$submitted && vm.employer_register_form.mobile.$error.pattern">Please enter a valid phone number</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="form-group float-input">\n' +
    '                    <label class="active">Password *</label>\n' +
    '                    <input id="email" ng-focus="vm.employer_register_form.password.$focused = true" type="password" class="form-control" ng-model="vm.user.password" name="password" required ng-minlength="6" ng-maxlength="40">\n' +
    '                    <div class="icons" ng-show="vm.employer_register_form.password.$focused">\n' +
    '                        <i ng-if="!vm.employer_register_form.password.$error.required && !vm.employer_register_form.password.$error.maxlength && vm.employer_register_form.password.$error.minlength" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                        <i ng-if="vm.employer_register_form.password.$error.required || vm.employer_register_form.password.$error.maxlength || vm.employer_register_form.password.$error.minlength" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                    </div>\n' +
    '                    <div class="errors">\n' +
    '                        <span ng-show="vm.employer_register_form.$submitted && vm.employer_register_form.password.$error.required">Required</span>\n' +
    '                        <span ng-show="vm.employer_register_form.$submitted && vm.employer_register_form.password.$error.maxlength">The value is too long</span>\n' +
    '                        <span ng-show="vm.employer_register_form.$submitted && vm.employer_register_form.password.$error.minlength">Please enter your password at least 6 characters</span>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="form-group float-input company-description-wrapper">\n' +
    '                    <label class="active">Company Description</label>\n' +
    '                    <textarea ng-focus="vm.employer_register_form.company_about.$focused = true" class="form-control" ng-model="vm.user.company_about" name="company_about" cols="30" rows="5"></textarea>\n' +
    '                    <div class="icons" ng-show="vm.employer_register_form.company_about.$focused">\n' +
    '                        <i ng-if="!vm.employer_register_form.company_about.$error.maxlength" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                        <i ng-if="vm.employer_register_form.company_about.$error.maxlength" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="form-group text-center">\n' +
    '                    <input ng-disabled="inProgress" type="submit" class="btn ui-btn btn-full-width orange-btn-sm" value="REGISTER COMPANY">\n' +
    '                </div>\n' +
    '            </form>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/company/edit/_edit.tpl.html',
    '<section id="edit-profile" class="edit-employer-profile">\n' +
    '    <div class="er-actions">\n' +
    '        <div class="container-fluid">\n' +
    '            <div class="row">\n' +
    '                <div class="item status pull-right">\n' +
    '                    <label class="ui-label">Status</label>\n' +
    '                    <select class="ui-select" ng-model="vm.profile.status" ng-options="status.id as status.status for status in vm.user_status" ng-change="vm.updateStatus(vm.profile)"></select>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row">\n' +
    '        <div class="col-sm-3">\n' +
    '            <div class="edit-left">\n' +
    '                <div class="img-wrapper" upload-file="vm.avatar" on-complete="vm.avatarUploaded" progress-percentage="vm.avatar_upload_percentage" in-progress="vm.avatarUploadInProgress" accept="png|jpg|jpeg" upload-type="companies" data-id="{{ vm.profile.getId() }}">\n' +
    '                    <user-profile-image image="vm.profile.getAvatar()" is-update-profile="true" user-role="employer"></user-profile-image>\n' +
    '                </div>\n' +
    '                <div class="progress" ng-show="vm.avatarUploadInProgress">\n' +
    '                    <div class="determinate" style="width: {{vm.avatar_upload_percentage}}%">\n' +
    '                        <span ng-if="vm.avatar_upload_percentage < 100">{{vm.avatar_upload_percentage}}%</span>\n' +
    '                        <span ng-if="vm.avatar_upload_percentage == 100">Processing</span>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <a upload-file="vm.avatar" on-complete="vm.avatarUploaded" progress-percentage="vm.avatar_upload_percentage" in-progress="vm.avatarUploadInProgress" accept="png|jpg|jpeg" class="btn btn-primary orange-btn-sm" id="up-pic-btn" upload-type="companies" data-id="{{ vm.profile.getId() }}">Upload Picture</a>\n' +
    '                <input type="file" name="" id="up-picture" style="display: none;">\n' +
    '                <div class="star" ng-if="vm.analyticReview && vm.analyticReview.total_reviews !== 0">\n' +
    '                    <input-stars max="5" ng-model="vm.analyticReview.avg_star" name="star" readonly></input-stars>\n' +
    '                    <p class="total-reviews">{{vm.analyticReview.total_reviews}} {{vm.analyticReview.message_total_reviews}}</p>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="col-sm-9">\n' +
    '            <div class="edit-right">\n' +
    '                <form id="update-profile-employer-form" name="vm.employer_update_form" ng-submit="isValidFormOrScrollToError(vm.employer_update_form) && vm.update(vm.profile)" update-text-fields novalidate>\n' +
    '                    <div class="edit-item">\n' +
    '                        <h3 class="left-dash">Company information</h3>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-sm-6">\n' +
    '                                <div class="form-group float-input">\n' +
    '                                    <label class="active">Company Name *</label>\n' +
    '                                    <input class="form-control" type="text" ng-model="vm.profile.employer.company_name" name="company_name" required ng-maxlength="100">\n' +
    '                                    <div class="errors">\n' +
    '                                        <span ng-show="vm.employer_update_form.$submitted && vm.employer_update_form.company_name.$error.required">Required</span>\n' +
    '                                        <span ng-show="vm.employer_update_form.$submitted && vm.employer_update_form.company_name.$error.maxlength">The value is too long</span>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="col-sm-6">\n' +
    '                                <div class="form-group float-input">\n' +
    '                                    <label class="active">Registed Number *</label>\n' +
    '                                    <input type="text" class="form-control" ng-model="vm.profile.employer.registration_number" name="registration_number" required ng-maxlength="15">\n' +
    '                                    <div class="errors">\n' +
    '                                        <span ng-show="vm.employer_update_form.$submitted && vm.employer_update_form.registration_number.$error.required">Required</span>\n' +
    '                                        <span ng-show="vm.employer_update_form.$submitted && vm.employer_update_form.registration_number.$error.maxlength">The value is too long</span>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-sm-6">\n' +
    '                                <div class="form-group float-input">\n' +
    '                                    <label class="active">Website URL</label>\n' +
    '                                    <input type="text" name="website" ng-model="vm.profile.employer.website" class="form-control" ng-pattern="/(ftp|http|https):\\/\\/(\\w+:{0,1}\\w*@)?(\\S+)(:[0-9]+)?(\\/|\\/([\\w#!:.?+=&%@!\\-\\/]))?/">\n' +
    '                                    <div class="errors">\n' +
    '                                        <span ng-show="vm.employer_update_form.$submitted && vm.employer_update_form.website.$error.pattern">Website URL format invalid</span>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-sm-12">\n' +
    '                                <div class="form-group float-input">\n' +
    '                                    <label class="active">Company Address *</label>\n' +
    '                                    <input type="text" class="form-control" ng-model="vm.profile.employer.company_address" name="company_address" required>\n' +
    '                                    <div class="errors">\n' +
    '                                        <span ng-show="vm.employer_update_form.$submitted && vm.employer_update_form.company_address.$error.required">Required</span>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <!-- <div class="row">\n' +
    '                            <div class="col s12">\n' +
    '                                <a class="change-password" ui-sref="app.change-password({email: vm.profile.email})">Change password</a>\n' +
    '                            </div>\n' +
    '                        </div> -->\n' +
    '                    </div>\n' +
    '                    <div class="edit-item">\n' +
    '                        <h3 class="left-dash">Company description</h3>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-sm-12">\n' +
    '                                <div class="form-group">\n' +
    '                                    <textarea class="company-description" placeholder="Write few words about your company here..." ng-model="vm.profile.employer.company_about"></textarea>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="edit-item">\n' +
    '                        <h3 class="left-dash">Contact</h3>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-sm-6">\n' +
    '                                <div class="form-group float-input">\n' +
    '                                    <label class="active">Contact Name</label>\n' +
    '                                    <input type="text" class="form-control" ng-model="vm.profile.employer.contact_name" name="contact_name" required>\n' +
    '                                    <div class="errors">\n' +
    '                                        <span ng-show="vm.employer_update_form.$submitted && vm.employer_update_form.contact_name.$error.required">Required</span>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="col-sm-6">\n' +
    '                                <div class="form-group float-input">\n' +
    '                                    <label class="active">Contact Position</label>\n' +
    '                                    <input type="text" class="form-control" ng-model="vm.profile.employer.contact_position" name="contact_position" required>\n' +
    '                                    <div class="errors">\n' +
    '                                        <span ng-show="vm.employer_update_form.$submitted && vm.employer_update_form.contact_position.$error.required">Required</span>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-sm-6">\n' +
    '                                <div class="form-group float-input">\n' +
    '                                    <label class="active">Email *</label>\n' +
    '                                    <input type="email" class="form-control" ng-model="vm.profile.email" name="email" required ng-maxlength="40">\n' +
    '                                    <div class="errors">\n' +
    '                                        <span ng-show="vm.employer_update_form.$submitted && vm.employer_update_form.email.$error.required">Required</span>\n' +
    '                                        <span ng-show="vm.employer_update_form.$submitted && vm.employer_update_form.email.$error.email">Please enter a valid email</span>\n' +
    '                                        <span ng-show="vm.employer_update_form.$submitted && vm.employer_update_form.email.$error.maxlength">The value is too long</span>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="col-sm-6">\n' +
    '                                <div class="row">\n' +
    '                                    <div class="col-sm-4">\n' +
    '                                        <div class="form-group float-input">\n' +
    '                                            <select class="form-control selectpicker" ng-model="vm.profile.country" ng-options="country as country.name for country in vm.countries"></select>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="col-sm-8">\n' +
    '                                        <div class="form-group float-input">\n' +
    '                                            <label class="active">Phone Number *</label>\n' +
    '                                            <input type="text" class="form-control" ng-model="vm.profile.mobile" name="mobile" ng-pattern="/^(\\d{6,12})$/" required ng-maxlength="40">\n' +
    '                                            <div class="errors">\n' +
    '                                                <span ng-show="vm.employer_update_form.$submitted && vm.employer_update_form.mobile.$error.required">Required</span>\n' +
    '                                                <span ng-show="vm.employer_update_form.$submitted && vm.employer_update_form.mobile.$error.maxlength">The value is too long</span>\n' +
    '                                                <span ng-show="vm.employer_update_form.$submitted && vm.employer_update_form.mobile.$error.pattern">Please enter a valid phone number</span>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col-sm-12 warning">\n' +
    '                                <p ng-if="vm.profile.needVerifyNewEmail()"><i class="fa fa-warning"></i> You have updated your email address, Please verify your new email <strong>{{vm.profile.getPendingEmail()}}</strong></p>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="row">\n' +
    '                        <div class="col-sm-12">\n' +
    '                            <div class="save-all">\n' +
    '                                <input type="submit" ng-disabled="inProgress" class="btn ui-btn btn-full-width orange-btn-sm" value="Save all Changes">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </form>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</section>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/company/list/_company_expand.tpl.html',
    '<div class="company-expand">\n' +
    '    <h2 class="center-dash"><span ng-bind="data.user.employer.getCompanyName()"></span> <span ng-show="data.user.employer.getUrl()"> | </span><a href="{{data.user.employer.getUrl()}}" ng-bind="data.user.employer.getUrl()"></a></h2>\n' +
    '    <p class="center-align" ng-bind="data.user.employer.getCompanyAbout()"></p>\n' +
    '    <div class="row center-align">\n' +
    '        <div class="col s6">\n' +
    '            <div class="row">\n' +
    '                <div class="col s12">\n' +
    '                    <span class="key">Registration No.: </span>\n' +
    '                    <span class="value" ng-bind="data.user.employer.getRegistrationNumber()"></span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row">\n' +
    '                <div class="col s12">\n' +
    '                    <span class="key">Email address: </span>\n' +
    '                    <span class="value" ng-bind="data.user.getEmail()"></span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row">\n' +
    '                <div class="col s12">\n' +
    '                    <span class="key">Address: </span>\n' +
    '                    <span class="value" ng-bind="data.user.employer.getCompanyAddress()"></span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="col s6">\n' +
    '            <div class="row">\n' +
    '                <div class="col s12">\n' +
    '                    <span class="key">Contact Name: </span>\n' +
    '                    <span class="value" ng-bind="data.user.employer.getCompanyName()"></span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row">\n' +
    '                <div class="col s12">\n' +
    '                    <span class="key">Phone Number: </span>\n' +
    '                    <span class="value">+ {{data.user.getPhone()}}</span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row">\n' +
    '                <div class="col s12">\n' +
    '                    <span class="key">Industry: </span>\n' +
    '                    <span class="value">{{data.user.showIndustry}}</span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/company/list/_list.tpl.html',
    '<div class="list-page list-users">\n' +
    '    <div class="row filter">\n' +
    '        <div class="col-sm-2 col-xs-12 filter-status">\n' +
    '            <label class="ui-label" for="status">Status</label>\n' +
    '            <select class="ui-select" ng-model="vm.status" ng-change="vm.filterStatus(vm.status)" ng-options="options as options.status for options in vm.user_status"></select>\n' +
    '        </div>\n' +
    '        <div class="col-sm-2 col-sm-offset-8 text-right user-action new-record-wrapper">\n' +
    '            <ul>\n' +
    '                <li class="new-user">\n' +
    '                    <a ui-sref="app.companies.create" title="Create new Company">\n' +
    '                        <i class="icon fa fa-plus-circle"></i>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '            </ul>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row search-box">\n' +
    '        <div class="col-xs-12 col-sm-6 select-all">\n' +
    '            <input type="checkbox" id="select-all" ng-model="vm.selectedAll" ng-change="vm.selectAll()">\n' +
    '            <label for="select-all">Select all</label>\n' +
    '            <span class="number-select" ng-show="vm.selectedUser.length > 0"> ( All {{vm.selectedUser.length}} {{vm.selectedUser.length === 1 ? \'item\' : \'items\'}}  on this page are selected.)</span>\n' +
    '        </div>\n' +
    '        <div class="search col-xs-12 col-sm-6">\n' +
    '            <div class="search-wrapper card">\n' +
    '                <input id="search" class="input-search form-control" ng-model="vm.params.search" placeholder="Search..." ng-keydown="vm.searchKeyEvent($event, vm.params.search)"><i class="icon-search fa fa-search" ng-click="vm.searchPage(vm.params.search)"></i>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- <div class="row">\n' +
    '        <div class="col-xs-12 col-sm-6">\n' +
    '            Sort By:\n' +
    '            <sortable-attributes></sortable-attributes>\n' +
    '        </div>\n' +
    '    </div> -->\n' +
    '    <preloader ng-if="vm.users | isUndefined"></preloader>\n' +
    '    <div class="row">\n' +
    '        <div class="col-xs-12">\n' +
    '            <table class="table table-striped ui-table table-items table-users" ng-if="vm.users !== undefined">\n' +
    '                <thead>\n' +
    '                    <tr>\n' +
    '                        <th></th>\n' +
    '                        <th class="clickable id-col">ID <!-- <span sortable="id"></span> --></th>\n' +
    '                        <th class="clickable email-col">Email <!-- <span sortable="email"></span> --></th>\n' +
    '                        <th>Company Name</th>\n' +
    '                        <th>Registration Number</th>\n' +
    '                        <th>Contact Name</th>\n' +
    '                        <th>Mobile <!-- <span sortable="phone"></span> --></th>\n' +
    '                        <th>Status <!-- <span sortable="status"></span> --></th>\n' +
    '                        <th class="right-align">Action</th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody>\n' +
    '                    <tr ng-repeat="u in vm.users">\n' +
    '                        <td>\n' +
    '                            <input type="checkbox" ng-model="u.selected" ng-change="vm.updateSelectedUser()">\n' +
    '                        </td>\n' +
    '                        <td ng-bind="u.getId()"></td>\n' +
    '                        <td ng-bind="u.getEmail()"></td>\n' +
    '                        <td>\n' +
    '                            <span ng-bind="u.employer.getCompanyName()"></span>\n' +
    '                        </td>\n' +
    '                        <td ng-bind="u.employer.getRegistrationNumber()"></td>\n' +
    '                        <td ng-bind="u.employer.getContactName()"></td>\n' +
    '                        <td ng-bind="u.getPhone()"></td>\n' +
    '                        <td ng-bind="u.user_status"></td>\n' +
    '                        <td class="text-right action">\n' +
    '                            <!--  <a ui-sref="app.change-password-user({id: u.getId()})" class="change-password">\n' +
    '                                <p class="ui-p"><i class="material-icons">lock</i></p>\n' +
    '                            </a>\n' +
    '                            <a ng-click="vm.loginAsUser(u.getId())" class="view-dashboard">\n' +
    '                                <p class="ui-p"><i class="material-icons">visibility</i></p>\n' +
    '                            </a> -->\n' +
    '                            <a ui-sref="app.companies.edit({id: u.getId()})" class="edit">\n' +
    '                                <p class="ui-p"><i class="icon fa fa-pencil-square-o"></i></p>\n' +
    '                            </a>\n' +
    '                            <a ng-click="vm.deleteUser(u)" class="delete">\n' +
    '                                <p class="ui-p"><i class="icon fa fa-trash"></i></p>\n' +
    '                            </a>\n' +
    '                        </td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '            <pagination data="vm.pagination"></pagination>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/employee/create/_employee_register_form.tpl.html',
    '<div id="register">\n' +
    '    <div class="container">\n' +
    '        <div class="col-xs-12">\n' +
    '            <div class="employee-register-form">\n' +
    '                <div class="title animated nf-fade-out" ng-class="{\'fadeInUp\': vm.isStateEq(\'modal.register.employee.general_infomation\')}">\n' +
    '                    <h2 class="center-align">Create new Employee</h2>\n' +
    '                </div>\n' +
    '                <form name="vm.employee_register_form" ng-submit="vm.employee_register_form.$valid && vm.registerEmployeeProfile(vm.user)" novalidate>\n' +
    '                    <div class="row">\n' +
    '                        <div class="col s6">\n' +
    '                            <div class="form-group float-input">\n' +
    '                                <label>E-mail</label>\n' +
    '                                <input ng-focus="vm.employee_register_form.email.$focused = true" type="email" class="form-control" ng-model="vm.user.email" name="email" required ng-maxlength="40">\n' +
    '                                <div class="icons" ng-show="vm.employee_register_form.email.$focused">\n' +
    '                                    <i ng-if="!vm.employee_register_form.email.$error.required && !vm.employee_register_form.email.$error.maxlength && !vm.employee_register_form.email.$error.email" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                    <i ng-if="vm.employee_register_form.email.$error.required || vm.employee_register_form.email.$error.maxlength || vm.employee_register_form.email.$error.email" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                                </div>\n' +
    '                                <div class="errors">\n' +
    '                                    <span ng-show="vm.employee_register_form.$submitted && vm.employee_register_form.email.$error.required">Required</span>\n' +
    '                                    <span ng-show="vm.employee_register_form.$submitted && vm.employee_register_form.email.$error.email">Please enter a valid email</span>\n' +
    '                                    <span ng-show="vm.employee_register_form.$submitted && vm.employee_register_form.email.$error.maxlength">The value is too long</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="col s6 salary-box">\n' +
    '                            <div class="form-group float-input">\n' +
    '                                <label>Salary.</label>\n' +
    '                                <input ng-focus="vm.employee_register_form.salary.$focused = true" type="text" ng-model="vm.user.salary" class="form-control" name="salary" ng-pattern="/^\\d+$/" ng-maxlength="20">\n' +
    '                                <div class="icons" ng-show="vm.employee_register_form.salary.$focused">\n' +
    '                                    <i ng-if="!vm.employee_register_form.salary.$error.maxlength && !vm.employee_register_form.salary.$error.pattern" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                    <i ng-if="vm.employee_register_form.salary.$error.maxlength || vm.employee_register_form.salary.$error.pattern" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                                </div>\n' +
    '                                <div class="errors">\n' +
    '                                    <span ng-show="vm.employee_register_form.$submitted && vm.employee_register_form.salary.$error.maxlength">The value is too long</span>\n' +
    '                                    <span ng-show="vm.employee_register_form.$submitted && vm.employee_register_form.salary.$error.pattern">Please enter a series of numbers</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="form-group float-input">\n' +
    '                        <label>Password</label>\n' +
    '                        <input ng-focus="vm.employee_register_form.password.$focused = true" type="password" class="form-control" ng-model="vm.user.password" name="password" required ng-minlength="6" ng-maxlength="40">\n' +
    '                        <div class="icons" ng-show="vm.employee_register_form.password.$focused">\n' +
    '                            <i ng-if="!vm.employee_register_form.password.$error.required && !vm.employee_register_form.password.$error.maxlength && !vm.employee_register_form.password.$error.minlength" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                            <i ng-if="vm.employee_register_form.password.$error.required || vm.employee_register_form.password.$error.maxlength || vm.employee_register_form.password.$error.minlength" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                        </div>\n' +
    '                        <div class="errors">\n' +
    '                            <span ng-show="vm.employee_register_form.$submitted && vm.employee_register_form.password.$error.required">Required</span>\n' +
    '                            <span ng-show="vm.employee_register_form.$submitted && vm.employee_register_form.password.$error.maxlength">The value is too long</span>\n' +
    '                            <span ng-show="vm.employee_register_form.$submitted && vm.employee_register_form.password.$error.minlength">The value is too short</span>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="row">\n' +
    '                        <div class="col s6 m4 country-box">\n' +
    '                            <div class="form-group float-input">\n' +
    '                                <select name="country" ng-focus="vm.employee_register_form.country.$focused = true" class="form-control select-box" ng-model="vm.country" ng-options="country as country.display() for country in vm.countries" required></select>\n' +
    '                                <div class="icons" ng-show="vm.employee_register_form.country.$focused">\n' +
    '                                    <i ng-if="!vm.employee_register_form.country.$error.required" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                    <i ng-if="vm.employee_register_form.country.$error.require" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                                </div>\n' +
    '                                <div class="errors">\n' +
    '                                    <span ng-show="vm.employee_register_form.$submitted && vm.employee_register_form.country.$error.required">Required</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="col s6 m8 phone-box">\n' +
    '                            <div class="form-group float-input">\n' +
    '                                <label>Phone No.</label>\n' +
    '                                <input ng-focus="vm.employee_register_form.phone_number.$focused = true" type="text" ng-model="vm.user.phone_number" class="form-control" name="phone_number" ng-pattern="/^(\\d{6,12})$/" required ng-maxlength="40">\n' +
    '                                <div class="icons" ng-show="vm.employee_register_form.phone_number.$focused">\n' +
    '                                    <i ng-if="!vm.employee_register_form.phone_number.$error.required && !vm.employee_register_form.phone_number.$error.maxlength && !vm.employee_register_form.phone_number.$error.pattern" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                    <i ng-if="vm.employee_register_form.phone_number.$error.required || vm.employee_register_form.phone_number.$error.maxlength || vm.employee_register_form.phone_number.$error.pattern" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                                </div>\n' +
    '                                <div class="errors">\n' +
    '                                    <span ng-show="vm.employee_register_form.$submitted && vm.employee_register_form.phone_number.$error.required">Required</span>\n' +
    '                                    <span ng-show="vm.employee_register_form.$submitted && vm.employee_register_form.phone_number.$error.maxlength">The value is too long</span>\n' +
    '                                    <span ng-show="vm.employee_register_form.$submitted && vm.employee_register_form.phone_number.$error.pattern">Please enter a valid phone number</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="row">\n' +
    '                        <div class="col s12 m6">\n' +
    '                            <div class="form-group float-input">\n' +
    '                                <label>First Name</label>\n' +
    '                                <input ng-focus="vm.employee_register_form.first_name.$focused = true" type="text" class="form-control" ng-model="vm.user.first_name" name="first_name" required ng-maxlength="40">\n' +
    '                                <div class="icons" ng-show="vm.employee_register_form.first_name.$focused">\n' +
    '                                    <i ng-if="!vm.employee_register_form.first_name.$error.required && !vm.employee_register_form.first_name.$error.maxlength" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                    <i ng-if="vm.employee_register_form.first_name.$error.required || vm.employee_register_form.first_name.$error.maxlength" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                                </div>\n' +
    '                                <div class="errors">\n' +
    '                                    <span ng-show="vm.employee_register_form.$submitted && vm.employee_register_form.first_name.$error.required">Required</span>\n' +
    '                                    <span ng-show="vm.employee_register_form.$submitted && vm.employee_register_form.first_name.$error.maxlength">The value is too long</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="col s12 m6">\n' +
    '                            <div class="form-group float-input">\n' +
    '                                <label>Last name</label>\n' +
    '                                <input ng-focus="vm.employee_register_form.last_name.$focused = true" type="text" class="form-control" ng-model="vm.user.last_name" name="last_name" required ng-maxlength="40">\n' +
    '                                <div class="icons" ng-show="vm.employee_register_form.last_name.$focused">\n' +
    '                                    <i ng-if="!vm.employee_register_form.last_name.$error.required && !vm.employee_register_form.last_name.$error.maxlength" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                    <i ng-if="vm.employee_register_form.last_name.$error.required || vm.employee_register_form.last_name.$error.maxlength" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                                </div>\n' +
    '                                <div class="errors">\n' +
    '                                    <span ng-show="vm.employee_register_form.$submitted && vm.employee_register_form.last_name.$error.required">Required</span>\n' +
    '                                    <span ng-show="vm.employee_register_form.$submitted && vm.employee_register_form.last_name.$error.maxlength">The value is too long</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="row">\n' +
    '                        <div class="col s12 m6">\n' +
    '                            <div class="form-group float-input">\n' +
    '                                <label>Date of Birth</label>\n' +
    '                                <input type="text" pick-a-date="vm.user.birth" pick-a-date-options="{\'selectYears\': 60, \'selectMonths\': true, \'today\': \'\', \'clear\': \'\',\'close\': \'Close\'}" class="form-control" name="birth" max-date="vm.maxBirthYear" required>\n' +
    '                                <div class="icons" ng-show="vm.employee_register_form.birth.$focused">\n' +
    '                                    <i ng-if="!vm.employee_register_form.birth.$error.required" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                    <i ng-if="vm.employee_register_form.birth.$error.required" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                                </div>\n' +
    '                                <div class="errors">\n' +
    '                                    <span ng-show="vm.employee_register_form.$submitted && !vm.user.birth">Required</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="col s12 m6 gender">\n' +
    '                            <div class="form-group">\n' +
    '                                <label for="male">Male</label>\n' +
    '                                <md-radio-group ng-model="vm.user.gender" name="gender" required>\n' +
    '                                    <md-radio-button ng-value="vm.genders.male" aria-label="Male">\n' +
    '                                        {{ d.label }}\n' +
    '                                    </md-radio-button>\n' +
    '                                </md-radio-group>\n' +
    '                            </div>\n' +
    '                            <div class="form-group">\n' +
    '                                <label for="female">Female</label>\n' +
    '                                <md-radio-group ng-model="vm.user.gender" name="gender" required>\n' +
    '                                    <md-radio-button ng-value="vm.genders.female" aria-label="Female">\n' +
    '                                        {{ d.label }}\n' +
    '                                    </md-radio-button>\n' +
    '                                </md-radio-group>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.employee_register_form.$submitted && vm.employee_register_form.gender.$error.required">Required</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="row">\n' +
    '                        <div class="col s12">\n' +
    '                            <p>Select Industry:</p>\n' +
    '                            <ul class="industries">\n' +
    '                                <li class="industry-item" ng-repeat="industry in vm.industries" ng-class="{\'active\': industry.selected}" ng-click="industry.selected = !industry.selected">{{industry.getName()}}</li>\n' +
    '                            </ul>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="row">\n' +
    '                        <div class="col s12">\n' +
    '                            <p>Select Tags:</p>\n' +
    '                            <md-chips ng-model="vm.user.tags" md-autocomplete-snap md-transform-chip="vm.transformChip($chip)" md-require-match="vm.autocompleteRequireMatch">\n' +
    '                                <md-autocomplete md-no-cache="true" md-selected-item="vm.selectedTag" md-search-text="searchTag" md-items="item in vm.querySearch(searchTag)" md-item-text="item.name" placeholder="Enter Tags">\n' +
    '                                    <span md-highlight-text="searchTag">{{item.name}}</span>\n' +
    '                                </md-autocomplete>\n' +
    '                                <md-chip-template>\n' +
    '                                    <span><strong>{{$chip.name}}</strong></span>\n' +
    '                                </md-chip-template>\n' +
    '                            </md-chips>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="form-group float-input height-auto about-yourself">\n' +
    '                        <label>Write a few words about yourself. <span ng-show="vm.user.summary != undefined && vm.user.summary != \'\'">({{vm.user.summary | length}}/1000)</span></label>\n' +
    '                        <textarea ng-focus="vm.employee_register_form.summary.$focused = true" type="text" class="form-control animated" ng-model="vm.user.summary" name="summary" ng-maxlength="1000" maxlength="1000"></textarea>\n' +
    '                        <div class="icons" ng-show="vm.employee_register_form.summary.$focused">\n' +
    '                            <i ng-if="!vm.employee_register_form.summary.$error.required && !vm.employee_register_form.summary.$error.maxlength" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                            <i ng-if="vm.employee_register_form.summary.$error.required || vm.employee_register_form.summary.$error.maxlength" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                        </div>\n' +
    '                        <div class="errors">\n' +
    '                            <span ng-show="vm.employee_register_form.$submitted && vm.employee_register_form.summary.$error.maxlength">The value is too long</span>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="form-group center-align">\n' +
    '                        <input ng-disabled="inProgress" type="submit" class="btn btn-primary orange-btn-sm" value="REGISTER EMPLOYEE">\n' +
    '                    </div>\n' +
    '                </form>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/employee/edit/_edit.tpl.html',
    '<section id="edit-user" class="edit-employee-profile">\n' +
    '    <div class="ee-actions">\n' +
    '        <div class="container-fluid">\n' +
    '            <div class="row">\n' +
    '                <div class="more-action item pull-right">\n' +
    '                    <md-menu>\n' +
    '                        <md-button aria-label="Open phone interactions menu" ng-click="openMenu($mdOpenMenu, $event)">\n' +
    '                            More Action <i class="icon material-icons">expand_more</i>\n' +
    '                        </md-button>\n' +
    '                        <md-menu-content width="4">\n' +
    '                            <md-menu-item>\n' +
    '                                <md-button ng-click="vm.resendVerifyEmail(vm.profile)">\n' +
    '                                    Resend verify Email\n' +
    '                                </md-button>\n' +
    '                            </md-menu-item>\n' +
    '                            <md-menu-item ng-if="user.can(\'send.sms\')">\n' +
    '                                <md-button ui-sref="app.employees.send-sms({id: vm.profile.getId()})">\n' +
    '                                    Send SMS\n' +
    '                                </md-button>\n' +
    '                            </md-menu-item>\n' +
    '                        </md-menu-content>\n' +
    '                    </md-menu>\n' +
    '                </div>\n' +
    '                <div class="item status pull-right">\n' +
    '                    <label>Status</label>\n' +
    '                    <select ng-model="vm.profile.status" ng-options="status.id as status.status for status in vm.user_status" ng-change="vm.updateStatus(vm.profile)"></select>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row">\n' +
    '        <div class="col m3">\n' +
    '            <div class="edit-left">\n' +
    '                <div class="img-wrapper">\n' +
    '                    <div class="user-image" upload-file="vm.avatar" accept="png|jpg|jpeg" on-complete="vm.avatarUploaded" progress-percentage="vm.avatar_upload_percentage" in-progress="vm.avatarUploadInProgress">\n' +
    '                        <user-profile-image image="vm.profile.getImage()" is-update-profile="true" user-role="employee"></user-profile-image>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="progress" ng-show="vm.avatarUploadInProgress">\n' +
    '                    <div class="determinate" style="width: {{vm.avatar_upload_percentage}}%">\n' +
    '                        <span ng-if="vm.avatar_upload_percentage < 100">{{vm.avatar_upload_percentage}}%</span>\n' +
    '                        <span ng-if="vm.avatar_upload_percentage == 100">Processing</span>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <a upload-file="vm.avatar" accept="png|jpg|jpeg" on-complete="vm.avatarUploaded" progress-percentage="vm.avatar_upload_percentage" in-progress="vm.avatarUploadInProgress" class="btn btn-primary orange-btn-sm upload-picture">Upload Picture</a>\n' +
    '                <!-- <a ng-click="vm.showMessage(\'warning\', \'Feature coming soon\', 5000)" class="btn btn-primary orange-btn-sm upload-video">Upload Video</a> -->\n' +
    '                <div class="star" ng-if="vm.analyticReview && vm.analyticReview.total_reviews !== 0">\n' +
    '                    <input-stars max="5" ng-model="vm.analyticReview.avg_star" name="star" readonly></input-stars>\n' +
    '                    <p class="total-reviews">{{vm.analyticReview.total_reviews}} {{vm.analyticReview.message_total_reviews}}</p>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="col m9">\n' +
    '            <div class="edit-right">\n' +
    '                <h2>Edit Profile</h2>\n' +
    '                <form name="vm.update_profile_form" ng-submit="isValidFormOrScrollToError(vm.update_profile_form) && vm.update(vm.profile)" novalidate>\n' +
    '                    <div class="edit-item">\n' +
    '                        <h3 class="left-dash">General Information</h3>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col m6">\n' +
    '                                <div class="form-group">\n' +
    '                                    <input type="text" ng-model="vm.profile.first_name" name="first_name" class="form-control" placeholder="First Name *" required>\n' +
    '                                    <div class="errors">\n' +
    '                                        <span ng-show="vm.update_profile_form.$submitted && vm.update_profile_form.first_name.$error.required">Required</span>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="col m6">\n' +
    '                                <div class="form-group">\n' +
    '                                    <input type="text" ng-model="vm.profile.last_name" name="last_name" class="form-control" placeholder="Last Name *" required>\n' +
    '                                    <div class="errors">\n' +
    '                                        <span ng-show="vm.update_profile_form.$submitted && vm.update_profile_form.last_name.$error.required">Required</span>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col s6">\n' +
    '                                <div class="form-group">\n' +
    '                                    <input type="text" pick-a-date="vm.profile.birth" pick-a-date-options="{\'selectYears\': 60, \'selectMonths\': true, \'today\': \'\', \'clear\': \'\', \'close\': \'\'}" placeholder="Birthday *" ng-model="birth" class="form-control" name="birth" min-date="vm.minBirthYear" max-date="vm.maxBirthYear" required>\n' +
    '                                    <div class="errors">\n' +
    '                                        <span ng-show="vm.update_profile_form.$submitted && vm.update_profile_form.birth.$error.required">Required</span>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="col s6 gender">\n' +
    '                                <div class="form-group">\n' +
    '                                    <label>Male\n' +
    '                                        <input type="radio" ng-model="vm.profile.gender" ng-value="\'male\'">\n' +
    '                                    </label>\n' +
    '                                    <label> Female\n' +
    '                                        <input type="radio" ng-model="vm.profile.gender" ng-value="\'female\'">\n' +
    '                                    </label>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col s12">\n' +
    '                                <a class="change-password" ui-sref="app.change-password-user({id: vm.profile.getId()})">Change password</a>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="edit-item">\n' +
    '                        <h3 class="left-dash">Contact info</h3>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col m12 l6">\n' +
    '                                <div class="form-group">\n' +
    '                                    <input type="email" class="form-control" ng-model="vm.profile.email" name="email" placeholder="Email *" required>\n' +
    '                                    <div class="errors">\n' +
    '                                        <span ng-show="vm.update_profile_form.$submitted && vm.update_profile_form.email.$error.required">Required</span>\n' +
    '                                        <span ng-show="vm.update_profile_form.$submitted && vm.update_profile_form.email.$error.email">Please enter a valid email address</span>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="col s12 l3">\n' +
    '                                <div class="form-group">\n' +
    '                                    <select class="form-control" title="Country" ng-model="vm.profile.country" ng-options="country as country.display() for country in vm.countries | filterBy:[\'is_supported_phone_area\']:1"></select>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="col s12 l3">\n' +
    '                                <div class="form-group">\n' +
    '                                    <input type="text" class="form-control" ng-model="vm.profile.phone_number" name="phone_number" ng-pattern="/^(\\d{6,12})$/" placeholder="Phone Number *" required>\n' +
    '                                    <div class="errors">\n' +
    '                                        <span ng-show="vm.update_profile_form.$submitted && vm.update_profile_form.phone_number.$error.required">Required</span>\n' +
    '                                        <span ng-show="vm.update_profile_form.$submitted && vm.update_profile_form.phone_number.$error.pattern">Please enter a valid phone number</span>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row whats-app-different-mobile">\n' +
    '                            <div class="col m12">\n' +
    '                                <input id="is-whats-app" type="checkbox" ng-model="vm.isDifferentPhoneNumber" ng-true-value="true" ng-false-value="false">\n' +
    '                                <label for="is-whats-app">My whats app phone number is different</label>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row" ng-if="vm.isDifferentPhoneNumber">\n' +
    '                            <div class="col m12 l3">\n' +
    '                                <div class="form-group">\n' +
    '                                    <select class="form-control" title="Phone area code" ng-model="vm.profile.optional_phone_area_code" ng-options="country as country.display() for country in vm.countries"></select>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="col m12 l3">\n' +
    '                                <div class="form-group">\n' +
    '                                    <input type="text" class="form-control" ng-model="vm.profile.optional_phone_number" name="optional_phone_number" ng-pattern="/^(\\d{6,12})$/" placeholder="What\'s app *" required>\n' +
    '                                    <div class="errors">\n' +
    '                                        <span ng-show="vm.update_profile_form.$submitted && vm.update_profile_form.optional_phone_number.$error.required">Required</span>\n' +
    '                                        <span ng-show="vm.update_profile_form.$submitted && vm.update_profile_form.optional_phone_number.$error.pattern">Please enter a valid phone number</span>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col s12">\n' +
    '                                <div class="form-group">\n' +
    '                                    <input autocomplete-address="vm.place" type="text" class="form-control" ng-model="vm.profile.address" placeholder="Address 1">\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col s12">\n' +
    '                                <div class="form-group">\n' +
    '                                    <input autocomplete-address type="text" class="form-control" ng-model="vm.profile.address2" placeholder="Address 2">\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col s6 m3">\n' +
    '                                <div class="form-group">\n' +
    '                                    <input type="text" class="form-control" placeholder="Zipcode" name="zipcode" ng-model="vm.profile.zipcode" ng-pattern="/^\\d{4,7}$/">\n' +
    '                                    <div class="errors">\n' +
    '                                        <span ng-show="vm.update_profile_form.$submitted && vm.update_profile_form.zipcode.$error.pattern">Please enter a valid zipcode</span>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="col s6 m3">\n' +
    '                                <div class="form-group">\n' +
    '                                    <input type="text" class="form-control" ng-model="vm.profile.city" placeholder="City">\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="col s6 m6">\n' +
    '                                <div class="form-group">\n' +
    '                                    <select class="form-control" title="Nationality" ng-model="vm.profile.nationality" ng-options="nationality as nationality.name for nationality in vm.countries"></select>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col m12">\n' +
    '                                <input id="verify-email-cb" type="checkbox" ng-model="vm.profile.email_verified" ng-disabled="vm.profile.email_verified" ng-true-value="true" ng-false-value="false">\n' +
    '                                <label class="text" for="verify-email-cb">Verify Email</label>\n' +
    '                            </div>\n' +
    '                            <div class="col m12">\n' +
    '                                <input id="verify-phone_number-cb" type="checkbox" ng-model="vm.profile.phone_number_verified" ng-disabled="vm.profile.phone_number_verified" ng-true-value="true" ng-false-value="false">\n' +
    '                                <label class="text" for="verify-phone_number-cb">Verify Phone Number</label>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="edit-item">\n' +
    '                        <h3 class="left-dash">Write a few words about yourself</h3>\n' +
    '                        <div class="row about-yourself-box">\n' +
    '                            <div class="col s12">\n' +
    '                                <div class="form-group">\n' +
    '                                    <textarea class="form-control" ng-model="vm.profile.summary"></textarea>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="edit-item industry-interest-box" ng-if="vm.industries | hasItem">\n' +
    '                        <h3 class="left-dash">Industry/Job Interest *</h3>\n' +
    '                        <div class="row industry-item" ng-repeat="industry in vm.industries">\n' +
    '                            <div class="col s12">\n' +
    '                                <div class="industry-title">\n' +
    '                                    <label class="no-placeholder">\n' +
    '                                        <input type="checkbox" ng-model="industry.selected" ng-true-value="true" ng-false-value="false" ng-change="vm.changeIndustry(vm.industries)">{{industry.getName()}}\n' +
    '                                    </label>\n' +
    '                                </div>\n' +
    '                                <div class="industry-wrapper" ng-if="industry.interested_jobs.length > 0 && industry.selected">\n' +
    '                                    <div class="job-duration col s12" ng-repeat="interested_job in industry.interested_jobs">\n' +
    '                                        <div class="interested-job col s6 m3">\n' +
    '                                            <label class="no-placeholder">\n' +
    '                                                <input type="checkbox" ng-model="interested_job.selected" ng-true-value="true" ng-false-value="false" ng-change="vm.changeIndustry(vm.industries)"> {{interested_job.getName()}}\n' +
    '                                            </label>\n' +
    '                                        </div>\n' +
    '                                        <div class="interested-job-duration col s6 m3">\n' +
    '                                            <select class="form-control" ng-disabled="!interested_job.selected" ng-model="interested_job.duration" ng-options="option as option.getName() for option in vm.durations"></select>\n' +
    '                                        </div>\n' +
    '                                        <div class="interested-job-duration col s12 m6" ng-if="interested_job.selected">\n' +
    '                                            <div class="row">\n' +
    '                                                <div class="tags">\n' +
    '                                                    <md-chips ng-model="interested_job.tags">\n' +
    '                                                        <md-chip-template>\n' +
    '                                                            <span><strong>{{$chip.name}}</strong></span>\n' +
    '                                                        </md-chip-template>\n' +
    '                                                    </md-chips>\n' +
    '                                                </div>\n' +
    '                                                <div class="company">\n' +
    '                                                    <md-autocomplete md-min-length="1" md-dropdown-position="bottom" md-selected-item="interested_job.tag" md-search-text-change="vm.searchTextChange(interested_job.searchText)" md-search-text="interested_job.searchText" md-selected-item-change="vm.selectedItemChange(item, interested_job)" md-items="item in vm.company_tags|tagsMatcher:interested_job.searchText" md-item-text="item.getName()" md-min-length="0" placeholder="Enter company name">\n' +
    '                                                        <md-item-template>\n' +
    '                                                            <span md-highlight-text="interested_job.searchText" md-highlight-flags="^i">{{item.getName()}}</span>\n' +
    '                                                        </md-item-template>\n' +
    '                                                        <md-not-found>\n' +
    '                                                            No states matching "{{interested_job.searchText}}" were found.\n' +
    '                                                            <a ng-click="vm.newState(interested_job.searchText, interested_job)">Create a new one!</a>\n' +
    '                                                        </md-not-found>\n' +
    '                                                    </md-autocomplete>\n' +
    '                                                </div>\n' +
    '                                                <div class="add">\n' +
    '                                                    <a class="btn btn-primary orange-btn-sm" ng-click="vm.addInterestedJobTag(interested_job)">Add</a>\n' +
    '                                                </div>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="errors">\n' +
    '                            <span ng-show="vm.update_profile_form.$submitted && !vm.industryValid">Required</span>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="edit-item employee-id">\n' +
    '                        <h3 class="left-dash">ID</h3>\n' +
    '                        <div class="form-group">\n' +
    '                            <h4>ID Type</h4>\n' +
    '                            <select class="form-control" ng-model="vm.id_type" ng-options="item as item.label for item in vm.id_types"></select>\n' +
    '                            <input ng-show="vm.id_type.label == \'Other\'" placeholder="Your ID Type" type="text" class="form-control" ng-model="vm.profile.employee.id_type">\n' +
    '                        </div>\n' +
    '                        <div class="form-group">\n' +
    '                            <div class="row">\n' +
    '                                <div class="col s12 m4 id-image">\n' +
    '                                    <nf-pdf ng-if="vm.profile.getIdImageExtension() === \'pdf\'" url="vm.profile.getIdImage()" options="vm.nfPdfOptions"></nf-pdf>\n' +
    '                                    <img ng-if="vm.profile.getIdImageExtension() !== \'pdf\'" ng-src="{{vm.profile.getIdImage()}}">\n' +
    '                                    <div class="progress" ng-show="vm.idImageUploadInProgress">\n' +
    '                                        <div class="determinate" style="width: {{vm.id_image_upload_percentage}}%">\n' +
    '                                            <span ng-if="vm.id_image_upload_percentage < 100">{{vm.id_image_upload_percentage}}%</span>\n' +
    '                                            <span ng-if="vm.id_image_upload_percentage == 100">Processing</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="col s12 warning">\n' +
    '                                    <p ng-if="vm.profile.getIdImage() != \'\' && vm.profile.getIdentityCheckStatus() == \'verified\'"><i class="fa fa-check"></i>Verified</p>\n' +
    '                                    <p ng-if="vm.profile.getIdImage() != \'\' && vm.profile.getIdentityCheckStatus() == \'pending\'"><i class="fa fa-warning"></i>This ID is pending validation</p>\n' +
    '                                    <p ng-if="vm.profile.getIdImage() != \'\' && vm.profile.getIdentityCheckStatus() == \'rejected\'"><i class="fa fa-warning"></i>This ID image is not good enough, requested user to updated new one.</p>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <a upload-file="vm.id_image" type="id" accept="png|jpg|jpeg" on-complete="vm.idImageUploadCompleted" progress-percentage="vm.id_image_upload_percentage" in-progress="vm.idImageUploadInProgress" class="btn btn-primary orange-btn-sm">Upload ID Image</a>\n' +
    '                            <a ng-cloak ng-if="vm.profile.getIdImage() != \'\'" ng-click="vm.verifyIdImage(vm.profile)" ng-hide="vm.profile.getIdImage() != \'\' && vm.profile.getIdentityCheckStatus() == \'verified\'" class="btn btn-primary orange-btn-sm verify-id-image">Verify this Image</a>\n' +
    '                            <a ng-cloak ng-if="vm.profile.getIdImage() != \'\'" ng-click="vm.rejectIdImage(vm.profile)" class="btn btn-primary orange-btn-sm verify-id-image">Reject this Image</a>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="edit-item">\n' +
    '                        <h3 class="left-dash">Language</h3>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col s12">\n' +
    '                                <a ng-repeat="language in vm.languages" class="btn btn-primary language" ng-class="{\'active\': language.selected}" ng-click="language.selected = !language.selected" ng-bind="language.getName()"></a>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="edit-item location-box">\n' +
    '                        <h3 class="left-dash">Preferred Location</h3>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col s12">\n' +
    '                                <a ng-repeat="preferred_location in vm.preferred_locations" class="btn btn-primary preferred_location" ng-class="{\'active\': preferred_location.selected}" ng-click="preferred_location.selected = !preferred_location.selected" ng-bind="preferred_location.getName()"></a>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="edit-item">\n' +
    '                        <h3 class="left-dash">Availabilities</h3>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col s12">\n' +
    '                                <div class="day-wrapper set-availablity" ng-repeat="(key, day) in  vm.dayAvailabilities">\n' +
    '                                    <h5>{{day.name}} <span class="pull-right day-show" ng-click="vm.toggleSelectDay(day)" ng-class="{\'active\': day.selected}"></span></h5>\n' +
    '                                    <div class="select-wrap">\n' +
    '                                        <p id="day_{{key}}_{{k}}" ng-repeat="(k, availability) in day.availabilities">{{availability.getTitle()}} <span class="pull-right time-select" ng-click="availability.selected = !availability.selected" ng-class="{\'time-color\': availability.selected}"></span></p>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="edit-item employee-id university-box">\n' +
    '                        <h3 class="left-dash">University</h3>\n' +
    '                        <div class="form-group university">\n' +
    '                            <label for="student"> I am a student\n' +
    '                                <input type="checkbox" id="student" ng-model="vm.isStudent" ng-true-value="true" ng-false-value="false" ng-change="vm.changeUniversity(vm.isStudent)">\n' +
    '                            </label>\n' +
    '                            <div class="university-select">\n' +
    '                                <select class="form-control" title="University" ng-show="vm.isStudent" ng-model="vm.profile.university" ng-options="university as university.getName() for university in vm.universities"></select>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="edit-item">\n' +
    '                        <h3 class="left-dash">Driving License</h3>\n' +
    '                        <div class="form-group driving-license">\n' +
    '                            <h4>Your License</h4>\n' +
    '                            <label ng-repeat="item in vm.driving_licenses"> {{item.label}}\n' +
    '                                <input type="checkbox" ng-model="item.selected">\n' +
    '                            </label>\n' +
    '                            <div class="row">\n' +
    '                                <div class="col s12 m4 driving-license-image">\n' +
    '                                    <nf-pdf ng-if="vm.profile.getDrivingLicenseImageExtension() === \'pdf\'" url="vm.profile.getDrivingLicenseImage()" options="vm.nfPdfOptions"></nf-pdf>\n' +
    '                                    <img ng-if="vm.profile.getDrivingLicenseImageExtension() !== \'pdf\'" ng-src="{{vm.profile.getDrivingLicenseImage()}}">\n' +
    '                                    <div class="progress" ng-show="vm.drivingLicenseImageUploadInProgress">\n' +
    '                                        <div class="determinate" style="width: {{vm.driving_license_image_upload_percentage}}%">\n' +
    '                                            <span ng-if="vm.driving_license_image_upload_percentage < 100">{{vm.driving_license_image_upload_percentage}}%</span>\n' +
    '                                            <span ng-if="vm.driving_license_image_upload_percentage == 100">Processing</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <div class="row">\n' +
    '                                        <div class="col s12">\n' +
    '                                            <a upload-file="vm.driving_license_image" type="license" accept="png|jpg|jpeg|pdf" on-complete="vm.drivingLicenseImageUploadCompleted" progress-percentage="vm.driving_license_image_upload_percentage" in-progress="vm.drivingLicenseImageUploadInProgress" class="btn btn-primary orange-btn-sm upload_driving_license_image">Upload Driving License Image</a>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                        <div class="form-group vehicles">\n' +
    '                            <h4>Your Vehicle</h4>\n' +
    '                            <label ng-repeat="item in vm.vehicles"> {{item.label}}\n' +
    '                                <input type="checkbox" ng-model="item.selected">\n' +
    '                            </label>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="edit-item background-experience-box">\n' +
    '                        <h3 class="left-dash">Background/Experience</h3>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col s12">\n' +
    '                                <div class="table-responsive">\n' +
    '                                    <table class="table table-hover last-positions">\n' +
    '                                        <thead>\n' +
    '                                            <tr>\n' +
    '                                                <th>Role</th>\n' +
    '                                                <th>Company</th>\n' +
    '                                                <th>Start</th>\n' +
    '                                                <th>End</th>\n' +
    '                                                <th>Comment</th>\n' +
    '                                                <th></th>\n' +
    '                                            </tr>\n' +
    '                                        </thead>\n' +
    '                                        <tbody>\n' +
    '                                            <tr ng-repeat="item in vm.profile.employee.last_positions" ng-show="vm.profile.employee.last_positions | isArray">\n' +
    '                                                <td ng-bind="item.getRole()"></td>\n' +
    '                                                <td ng-bind="item.getCompanyName()"></td>\n' +
    '                                                <td ng-bind="item.getStartDate() | timeFormat:\'DD MMM, YYYY\'"></td>\n' +
    '                                                <td ng-bind="item.getEndDate() | timeFormat:\'DD MMM, YYYY\'"></td>\n' +
    '                                                <td ng-bind="item.comment"></td>\n' +
    '                                                <td class="trash-column"><a ng-click="vm.deleteLastPosition($index, item)" class="red-text"><i class="material-icons">delete</i></a></td>\n' +
    '                                            </tr>\n' +
    '                                            <tr ng-repeat="item in vm.last_positions">\n' +
    '                                                <td>\n' +
    '                                                    <div class="form-group">\n' +
    '                                                        <input type="text" name="role_{{$index}}" ng-model="item.role" required>\n' +
    '                                                        <div class="errors">\n' +
    '                                                            <span ng-show="vm.update_profile_form.$submitted && vm.update_profile_form.role_{{$index}}.$error.required">Required</span>\n' +
    '                                                        </div>\n' +
    '                                                    </div>\n' +
    '                                                </td>\n' +
    '                                                <td>\n' +
    '                                                    <div class="form-group">\n' +
    '                                                        <input type="text" name="company_name_{{$index}}" ng-model="item.company_name" required>\n' +
    '                                                        <div class="errors">\n' +
    '                                                            <span ng-show="vm.update_profile_form.$submitted && vm.update_profile_form.company_name_{{$index}}.$error.required">Required</span>\n' +
    '                                                        </div>\n' +
    '                                                    </div>\n' +
    '                                                </td>\n' +
    '                                                <td>\n' +
    '                                                    <div class="form-group">\n' +
    '                                                        <input type="text" ng-model="start_date" name="start_date_{{$index}}" pick-a-date-options="{\'selectYears\': 20, \'selectMonths\': true, \'today\': \'\', \'clear\': \'\', \'close\': \'\'}" max-date="vm.today" pick-a-date="item.start_date" required>\n' +
    '                                                        <div class="errors">\n' +
    '                                                            <span ng-show="vm.update_profile_form.$submitted && vm.update_profile_form.start_date_{{$index}}.$error.required">Required</span>\n' +
    '                                                        </div>\n' +
    '                                                    </div>\n' +
    '                                                </td>\n' +
    '                                                <td>\n' +
    '                                                    <div class="form-group">\n' +
    '                                                        <input type="text" ng-model="end_date" name="end_date_{{$index}}" pick-a-date-options="{\'selectYears\': 20, \'selectMonths\': true, \'today\': \'\', \'clear\': \'\', \'close\': \'\'}" max-date="vm.today" min-date="item.start_date" pick-a-date="item.end_date" required>\n' +
    '                                                        <div class="errors">\n' +
    '                                                            <span ng-show="vm.update_profile_form.$submitted && vm.update_profile_form.end_date_{{$index}}.$error.required">Required</span>\n' +
    '                                                        </div>\n' +
    '                                                    </div>\n' +
    '                                                </td>\n' +
    '                                                <td>\n' +
    '                                                    <div class="form-group">\n' +
    '                                                        <input type="text" ng-model="item.comment">\n' +
    '                                                        <div class="errors"></div>\n' +
    '                                                    </div>\n' +
    '                                                </td>\n' +
    '                                                <td class="trash-column"><a ng-click="vm.deleteLastPosition($index, item)" class="red-text"><i class="material-icons">delete</i></a></td>\n' +
    '                                            </tr>\n' +
    '                                        </tbody>\n' +
    '                                    </table>\n' +
    '                                </div>\n' +
    '                                <a ng-click="vm.addLastPosition()" class="btn btn-primary white-btn-2 active-white" id="up-pic-btn">Add More Work Experience</a>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="edit-item">\n' +
    '                        <h3 class="left-dash">Tags</h3>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col s12">\n' +
    '                                <md-chips ng-model="vm.profile.tags" md-autocomplete-snap md-transform-chip="vm.transformChip($chip)" md-require-match="vm.autocompleteRequireMatch">\n' +
    '                                    <md-autocomplete md-no-cache="true" md-selected-item="vm.selectedTag" md-search-text="searchTag" md-items="item in vm.querySearch(searchTag)" md-item-text="item.name" placeholder="Enter Tags">\n' +
    '                                        <span md-highlight-text="searchTag">{{item.name}}</span>\n' +
    '                                    </md-autocomplete>\n' +
    '                                    <md-chip-template>\n' +
    '                                        <span><strong>{{$chip.name}}</strong></span>\n' +
    '                                    </md-chip-template>\n' +
    '                                </md-chips>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="edit-item not-available-period">\n' +
    '                        <h3 class="left-dash title">Not Available Period</h3>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col s12">\n' +
    '                                <p>If you are in holiday, please let us know.</p>\n' +
    '                                <div class="table-responsive">\n' +
    '                                    <table class="table">\n' +
    '                                        <thead>\n' +
    '                                            <tr>\n' +
    '                                                <th>Start Time</th>\n' +
    '                                                <th>End Time</th>\n' +
    '                                                <th></th>\n' +
    '                                            </tr>\n' +
    '                                        </thead>\n' +
    '                                        <tbody>\n' +
    '                                            <tr ng-repeat="user_not_avaiable in vm.profile.user_not_avaiables">\n' +
    '                                                <td>\n' +
    '                                                    <div class="form-group" name="date-{{$index}}">\n' +
    '                                                        <input class="user-not-avaiable-date" type="text" pick-a-date="user_not_avaiable.start_time" ng-model="start_time" pick-a-date-options="{\'format\': \'dd/mm/yyyy\', \'selectYears\': false, \'selectMonths\': false, \'today\': \'\', \'clear\': \'\', \'close\': \'\'}" min-date="vm.today" class="form-control" name="start{{$index}}" required>\n' +
    '                                                        <div class="errors">\n' +
    '                                                            <span ng-show="vm.update_profile_form.$submitted && vm.update_profile_form.start{{$index}}.$error.required">Required</span>\n' +
    '                                                        </div>\n' +
    '                                                    </div>\n' +
    '                                                </td>\n' +
    '                                                <td>\n' +
    '                                                    <div class="form-group" name="date-{{$index}}">\n' +
    '                                                        <input class="user-not-avaiable-date" type="text" pick-a-date="user_not_avaiable.end_time" ng-model="end_time" pick-a-date-options="{\'format\': \'dd/mm/yyyy\', \'selectYears\': false, \'selectMonths\': false, \'today\': \'\', \'clear\': \'\', \'close\': \'\'}" min-date="user_not_avaiable.start_time" class="form-control" name="end{{$index}}" required>\n' +
    '                                                        <div class="errors">\n' +
    '                                                            <span ng-show="vm.update_profile_form.$submitted && vm.update_profile_form.end{{$index}}.$error.required">Required</span>\n' +
    '                                                        </div>\n' +
    '                                                    </div>\n' +
    '                                                </td>\n' +
    '                                                <td class="remove-btn" ng-click="vm.removeMoreNotAvailable(user_not_avaiable)"><i class="fa fa-times delete-row"></i></td>\n' +
    '                                            </tr>\n' +
    '                                        </tbody>\n' +
    '                                    </table>\n' +
    '                                </div>\n' +
    '                                <a ng-click="vm.addMoreNotAvailable()" class="btn btn-primary white-btn-2 active-white add-more-not-available" id="add-more-2">Add Not Available Period</a>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="edit-item referral-program">\n' +
    '                        <h3 class="left-dash">Referral Program</h3>\n' +
    '                        <div class="form-group">\n' +
    '                            <p>Activated Users: <strong ng-bind="vm.profile.getActivatedUsers()"></strong></p>\n' +
    '                            <p>Referral Code: <strong ng-bind="vm.profile.getReferralCode()"></strong></p>\n' +
    '                            <p ng-if="vm.referral_url">Referral Url: <strong ng-bind="vm.referral_url"></strong></p>\n' +
    '                            <a ng-click="vm.generateShortenUrl(vm.profile)" class="btn btn-primary orange-btn-sm">Generate easy share URL</a>\n' +
    '                            <!--                                 <h4>Your referral link below uniquely identifies your account. Use this code when register new Ushift account!</h4>\n' +
    '                                <ul>\n' +
    '                                    <li>\n' +
    '                                        <p>Referal Url: <span>{{vm.affiliate_url}}</span></p>\n' +
    '                                    </li>\n' +
    '                                    <li>\n' +
    '                                        <a class="action btn btn-default" ng-click="coming()">Copy URL</a>\n' +
    '                                    </li>\n' +
    '                                    <li>\n' +
    '                                        <a class="action btn btn-default" ng-click="coming()">Share</a>\n' +
    '                                    </li>\n' +
    '                                </ul> -->\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="edit-item interviewers">\n' +
    '                        <h3 class="left-dash">Interviewers</h3>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col s12">\n' +
    '                                <div class="table-responsive">\n' +
    '                                    <table class="table table-hover">\n' +
    '                                        <thead>\n' +
    '                                            <tr>\n' +
    '                                                <th class="text-center">Staff</th>\n' +
    '                                                <th class="text-center">Date</th>\n' +
    '                                            </tr>\n' +
    '                                        </thead>\n' +
    '                                        <tbody>\n' +
    '                                            <tr ng-repeat="item in vm.profile.getInterviewers()">\n' +
    '                                                <td ng-if="item.id != undefined" ng-bind="item.getInterviewerName()"></td>\n' +
    '                                                <td ng-if="item.id == undefined">\n' +
    '                                                    <div class="form-group">\n' +
    '                                                        <input type="text" name="interviewer_{{$index}}" ng-model="item.interviewer_name" required>\n' +
    '                                                        <div class="errors">\n' +
    '                                                            <span ng-show="vm.update_profile_form.$submitted && vm.update_profile_form.interviewer_{{$index}}.$error.required">Required</span>\n' +
    '                                                        </div>\n' +
    '                                                    </div>\n' +
    '                                                </td>\n' +
    '                                                <td ng-if="item.id != undefined" ng-bind="item.getCreatedAt() | timeFormat:\'MMM DD YYYY, h:mm\'"></td>\n' +
    '                                                <td ng-if="item.id == undefined" ng-bind="vm.today | timeFormat:\'MMM DD YYYY, h:mm\'"></td>\n' +
    '                                                <td class="trash-column"><a ng-click="vm.deleteInterviewer(item)" class="red-text"><i class="material-icons">delete</i></a></td>\n' +
    '                                            </tr>\n' +
    '                                        </tbody>\n' +
    '                                    </table>\n' +
    '                                </div>\n' +
    '                                <a ng-click="vm.addInterviewer()" class="btn btn-primary white-btn-2 active-white" id="up-pic-btn">Add Interviewer</a>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="edit-item history-status">\n' +
    '                        <h3 class="left-dash">Status history</h3>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col s12">\n' +
    '                                <div class="table-responsive history-status-table">\n' +
    '                                    <table class="table table-hover">\n' +
    '                                        <thead>\n' +
    '                                            <tr>\n' +
    '                                                <th class="text-center">Activity</th>\n' +
    '                                                <th class="text-center">Date</th>\n' +
    '                                            </tr>\n' +
    '                                        </thead>\n' +
    '                                        <tbody>\n' +
    '                                            <tr ng-repeat="activity in vm.historyStatus">\n' +
    '                                                <td ng-if="activity.event == \'UserReactiveAccountEvent\'"><a>{{activity.payload.user.email}}</a> have re-activated account</td>\n' +
    '                                                <td ng-if="activity.event == \'UserDeactiveAccountEvent\'"><a>{{activity.payload.user.email}}</a> have deactivated account</td>\n' +
    '                                                <td ng-if="activity.event == \'UserUpdateStatusEvent\'"><a>{{activity.payload.user.email}}</a> has marked <a>{{activity.payload.user.email}}</a> as {{activity.payload.user_status.status}}</td>\n' +
    '                                                <td ng-if="activity.event == \'AdminChangeStatusEmployee\'"><a>{{activity.payload.admin.email}} {{activity.payload.admin.roles | showRoles}}</a> has marked <a>{{activity.payload.user.email}} {{activity.payload.user.roles | showRoles}}</a> as {{activity.payload.user_status.status}}</td>\n' +
    '                                                <td ng-bind="activity.getCreatedAt() | timeFormat:\'MMM DD YYYY, h:mm\'"></td>\n' +
    '                                            </tr>\n' +
    '                                        </tbody>\n' +
    '                                    </table>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="edit-item">\n' +
    '                        <h3 class="left-dash">Salary Expectation</h3>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col s12">\n' +
    '                                <div class="form-group">\n' +
    '                                    <input type="text" class="form-control" ng-model="vm.profile.salary">\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="edit-item history-transferred" ng-if="vm.salaries | hasItem">\n' +
    '                        <h3 class="left-dash">Transfer History</h3>\n' +
    '                        <p>No of Transfers: {{vm.amount_received.no_of_transfer}}</p>\n' +
    '                        <p>Total Amount: {{vm.amount_received.total_amount | number:2}}</p>\n' +
    '                        <p>Amount Transferred: {{vm.amount_received.amount_transferred | number:2}}</p>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col s12">\n' +
    '                                <div class="table-responsive history-transferred-table">\n' +
    '                                    <table class="table table-hover">\n' +
    '                                        <thead>\n' +
    '                                            <tr>\n' +
    '                                                <th class="text-center">Job</th>\n' +
    '                                                <th class="text-center">Shift</th>\n' +
    '                                                <th class="text-center">Expectation</th>\n' +
    '                                                <th class="text-center">Transferred</th>\n' +
    '                                                <th class="text-center">Status</th>\n' +
    '                                            </tr>\n' +
    '                                        </thead>\n' +
    '                                        <tbody>\n' +
    '                                            <tr ng-repeat="salary in vm.salaries">\n' +
    '                                                <td>\n' +
    '                                                    <a ui-sref="app.jobs.update({slug: salary.shift.job.getSlug()})" ng-bind="salary.shift.job.getTitle()"></a>\n' +
    '                                                </td>\n' +
    '                                                <td>\n' +
    '                                                    <p>{{salary.shift.getStartTime() | timeFormat:\'ddd, MMM Do, YYYY\'}}</p>\n' +
    '                                                    <p><span ng-bind="salary.shift.getStartTime() | timeFormat:\'h:mma\'"></span> - <span ng-bind="salary.shift.getEndTime() | timeFormat:\'h:mma\'"></span></p>\n' +
    '                                                </td>\n' +
    '                                                <td><span ng-bind="salary.shift.getSalary() | number:2"></span> <span ng-bind="salary.shift.job.getCurrency()"></span></td>\n' +
    '                                                <td><span ng-bind="salary.getSalary() | number:2"></span> <span ng-bind="salary.shift.job.getCurrency()"></span></td>\n' +
    '                                                <td><span ng-if="salary.isPaid()">Transferred</span></td>\n' +
    '                                            </tr>\n' +
    '                                        </tbody>\n' +
    '                                    </table>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="edit-item points">\n' +
    '                        <h3 class="left-dash">Points</h3>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col s12">\n' +
    '                                <ul class="points">\n' +
    '                                    <li>\n' +
    '                                        <p class="good-point" ng-if="vm.profile.point > 0"><span>+{{vm.profile.point}}</span> <span>point</span></p>\n' +
    '                                        <p ng-if="vm.profile.point < 0"><span>{{vm.profile.point}}</span> <span>point</span></p>\n' +
    '                                        <p class="bad-point" ng-if="vm.profile.point == 0"><span>{{vm.profile.point}}</span> <span>point</span></p>\n' +
    '                                    </li>\n' +
    '                                    <li>\n' +
    '                                        <a ng-click="vm.profile.point = vm.profile.point + 1">\n' +
    '                                            <button class="ui-btn btn-white btn-small btn-green"><i class="fa fa-plus" aria-hidden="true"></i><span>1</span></button>\n' +
    '                                        </a>\n' +
    '                                    </li>\n' +
    '                                    <li>\n' +
    '                                        <a ng-click="vm.profile.point = vm.profile.point - 1">\n' +
    '                                            <button class="ui-btn btn-white btn-small btn-red"><i class="fa fa-minus" aria-hidden="true"></i><span>1</span></button>\n' +
    '                                        </a>\n' +
    '                                    </li>\n' +
    '                                </ul>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="edit-item private-note">\n' +
    '                        <h3 class="left-dash">Private note</h3>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col s12">\n' +
    '                                <div class="form-group">\n' +
    '                                    <textarea class="form-control" ng-model="vm.profile.private_note"></textarea>\n' +
    '                                    <span ng-show="vm.profile.private_note != undefined && vm.profile.private_note != \'\'">({{vm.profile.private_note | length}}/1000)</span>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="edit-item review-score" ng-if="vm.profile.getAvgStar() != 0 && vm.profile.getTotalReview() != 0">\n' +
    '                        <h3 class="left-dash">Review Score: {{vm.profile.getAvgStar()}}({{vm.profile.getTotalReview()}})</h3>\n' +
    '                    </div>\n' +
    '                    <div class="edit-item history-jobs">\n' +
    '                        <h3 class="left-dash">Job history</h3>\n' +
    '                        <div class="form-group">\n' +
    '                            <p>No. of job Applied: <strong ng-bind="vm.applied_jobs.length"></strong></p>\n' +
    '                            <div class="col s12" ng-if="vm.applied_jobs.length > 0">\n' +
    '                                <div class="table-responsive job-accepted-table row">\n' +
    '                                    <table class="table table-hover">\n' +
    '                                        <thead>\n' +
    '                                            <tr>\n' +
    '                                                <th class="col s9">Job Title</th>\n' +
    '                                                <th class="col s3">Employer</th>\n' +
    '                                            </tr>\n' +
    '                                        </thead>\n' +
    '                                        <tbody>\n' +
    '                                            <tr ng-repeat="job in vm.applied_jobs">\n' +
    '                                                <td class="col s9">\n' +
    '                                                    <a ui-sref="app.jobs.update({slug: job.getSlug()})" ng-bind="job.getTitle()"></a>\n' +
    '                                                </td>\n' +
    '                                                <td class="col s3">\n' +
    '                                                    <a ui-sref="app.companies.edit({id: job.employer.getId()})" ng-bind="job.employer.getName()"></a>\n' +
    '                                                </td>\n' +
    '                                            </tr>\n' +
    '                                        </tbody>\n' +
    '                                    </table>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <p>No. of job Accepted: <strong ng-bind="vm.accepted_jobs.length"></strong></p>\n' +
    '                            <div class="col s12" ng-if="vm.accepted_jobs.length > 0">\n' +
    '                                <div class="table-responsive job-accepted-table row">\n' +
    '                                    <table class="table table-hover">\n' +
    '                                        <thead>\n' +
    '                                            <tr>\n' +
    '                                                <th class="col s9">Job Title</th>\n' +
    '                                                <th class="col s3">Employer</th>\n' +
    '                                            </tr>\n' +
    '                                        </thead>\n' +
    '                                        <tbody>\n' +
    '                                            <tr ng-repeat="job in vm.accepted_jobs">\n' +
    '                                                <td class="col s9">\n' +
    '                                                    <a ui-sref="app.jobs.update({slug: job.getSlug()})" ng-bind="job.getTitle()"></a>\n' +
    '                                                </td>\n' +
    '                                                <td class="col s3">\n' +
    '                                                    <a ui-sref="app.companies.edit({id: job.employer.getId()})" ng-bind="job.employer.getName()"></a>\n' +
    '                                                </td>\n' +
    '                                            </tr>\n' +
    '                                        </tbody>\n' +
    '                                    </table>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <p>No. of job Completed: <strong ng-bind="vm.completed_jobs.length"></strong></p>\n' +
    '                            <div class="col s12" ng-if="vm.completed_jobs.length > 0">\n' +
    '                                <div class="table-responsive job-completed-table row">\n' +
    '                                    <table class="table table-hover">\n' +
    '                                        <thead>\n' +
    '                                            <tr>\n' +
    '                                                <th class="col s9">Job Title</th>\n' +
    '                                                <th class="col s3">Employer</th>\n' +
    '                                            </tr>\n' +
    '                                        </thead>\n' +
    '                                        <tbody>\n' +
    '                                            <tr ng-repeat="job in vm.completed_jobs">\n' +
    '                                                <td class="col s9">\n' +
    '                                                    <a ui-sref="app.jobs.update({slug: job.getSlug()})" ng-bind="job.getTitle()"></a>\n' +
    '                                                </td>\n' +
    '                                                <td class="col s3">\n' +
    '                                                    <a ui-sref="app.companies.edit({id: job.employer.getId()})" ng-bind="job.employer.getName()"></a>\n' +
    '                                                </td>\n' +
    '                                            </tr>\n' +
    '                                        </tbody>\n' +
    '                                    </table>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <p>No. of job Rejected: <strong ng-bind="vm.rejected_jobs.length"></strong></p>\n' +
    '                            <div class="col s12" ng-if="vm.rejected_jobs.length > 0">\n' +
    '                                <div class="table-responsive job-rejected-table row">\n' +
    '                                    <table class="table table-hover">\n' +
    '                                        <thead>\n' +
    '                                            <tr>\n' +
    '                                                <th class="col s9">Job Title</th>\n' +
    '                                                <th class="col s3">Employer</th>\n' +
    '                                            </tr>\n' +
    '                                        </thead>\n' +
    '                                        <tbody>\n' +
    '                                            <tr ng-repeat="job in vm.rejected_jobs">\n' +
    '                                                <td class="col s9">\n' +
    '                                                    <a ui-sref="app.jobs.update({slug: job.getSlug()})" ng-bind="job.getTitle()"></a>\n' +
    '                                                </td>\n' +
    '                                                <td class="col s3">\n' +
    '                                                    <a ui-sref="app.companies.edit({id: job.employer.getId()})" ng-bind="job.employer.getName()"></a>\n' +
    '                                                </td>\n' +
    '                                            </tr>\n' +
    '                                        </tbody>\n' +
    '                                    </table>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <p>No. of job Canceled: <strong ng-bind="vm.canceled_jobs.length"></strong></p>\n' +
    '                            <div class="col s12" ng-if="vm.canceled_jobs.length > 0">\n' +
    '                                <div class="table-responsive job-canceled-table row">\n' +
    '                                    <table class="table table-hover">\n' +
    '                                        <thead>\n' +
    '                                            <tr>\n' +
    '                                                <th class="col s9">Job Title</th>\n' +
    '                                                <th class="col s3">Employer</th>\n' +
    '                                            </tr>\n' +
    '                                        </thead>\n' +
    '                                        <tbody>\n' +
    '                                            <tr ng-repeat="job in vm.canceled_jobs">\n' +
    '                                                <td class="col s9">\n' +
    '                                                    <a ui-sref="app.jobs.update({slug: job.getSlug()})" ng-bind="job.getTitle()"></a>\n' +
    '                                                </td>\n' +
    '                                                <td class="col s3">\n' +
    '                                                    <a ui-sref="app.companies.edit({id: job.employer.getId()})" ng-bind="job.employer.getName()"></a>\n' +
    '                                                </td>\n' +
    '                                            </tr>\n' +
    '                                        </tbody>\n' +
    '                                    </table>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <!-- <div class="edit-item history-status">\n' +
    '                        <h3 class="left-dash">History Status</h3>\n' +
    '                        <div class="row">\n' +
    '                            <div class="col s12">\n' +
    '                                <div class="table-responsive history-status-table">\n' +
    '                                    <table class="table table-hover">\n' +
    '                                        <thead>\n' +
    '                                            <tr>\n' +
    '                                                <th class="text-center">Activity</th>\n' +
    '                                                <th class="text-center">Date</th>\n' +
    '                                            </tr>\n' +
    '                                        </thead>\n' +
    '                                        <tbody>\n' +
    '                                            <tr ng-repeat="activity in vm.historyStatus">\n' +
    '                                                <td><a>{{activity.payload.admin.email}} {{activity.payload.admin.roles | showRoles}}</a> has mark <a>{{activity.payload.user.email}} {{activity.payload.user.roles | showRoles}}</a> as {{activity.payload.user_status.status}}</td>\n' +
    '                                                <td ng-bind="activity.getCreatedAt() | timeFormat:\'MMM DD YYYY, h:mm\'"></td>\n' +
    '                                            </tr>\n' +
    '                                        </tbody>\n' +
    '                                    </table>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div> -->\n' +
    '                    <div class="row save-changes">\n' +
    '                        <div class="col s12">\n' +
    '                            <div class="save-all">\n' +
    '                                <input type="submit" ng-disabled="inProgress || !(vm.industries | hasItem)" class="btn btn-primary orange-btn-sm" value="Save all Changes">\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </form>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</section>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/employee/list/_employee_expand.tpl.html',
    '<div class="employee-expand">\n' +
    '    <h4 class="center-dash" ng-bind="data.user.getName()"></h4>\n' +
    '    <div class="row center-align">\n' +
    '        <div class="col s6">\n' +
    '            <div class="row">\n' +
    '                <div class="col s12">\n' +
    '                    <strong class="key">Date of Birth: </strong>\n' +
    '                    <span class="value" ng-bind="data.user.getBirth() | timeFormat:\'MMM DD, YYYY\'"></span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row">\n' +
    '                <div class="col s12">\n' +
    '                    <strong class="key">Country: </strong>\n' +
    '                    <span class="value" ng-bind="data.user.getCountry()"></span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row address">\n' +
    '                <div class="col s12">\n' +
    '                    <strong class="key">Address: </strong>\n' +
    '                    <span class="value" ng-bind="data.user.getAddress()"></span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="col s6">\n' +
    '            <div class="row">\n' +
    '                <div class="col s12">\n' +
    '                    <strong class="key">Gender: </strong>\n' +
    '                    <span class="value" ng-bind="data.user.getGender()"></span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <div class="row industry">\n' +
    '                <div class="col s12">\n' +
    '                    <strong class="key">Industry: </strong>\n' +
    '                    <span class="value" ng-bind="data.user.showIndustry"></span>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row" ng-show="data.user.getEmployee().last_positions | hasItem">\n' +
    '        <h4 class="center-dash">Background/Experience</h4>\n' +
    '        <div class="row">\n' +
    '            <div class="col-md-12">\n' +
    '                <div class="table-responsive">\n' +
    '                    <table class="table exp">\n' +
    '                        <thead>\n' +
    '                            <tr>\n' +
    '                                <th>Role</th>\n' +
    '                                <th>Company</th>\n' +
    '                                <th>Start</th>\n' +
    '                                <th>End</th>\n' +
    '                                <th>Comment</th>\n' +
    '                            </tr>\n' +
    '                        </thead>\n' +
    '                        <tbody>\n' +
    '                            <tr ng-repeat="position in data.user.getEmployee().last_positions">\n' +
    '                                <td ng-bind="position.role"></td>\n' +
    '                                <td ng-bind="position.company_name"></td>\n' +
    '                                <td ng-bind="position.start_date | timeFormat:\'LL\'"></td>\n' +
    '                                <td ng-bind="position.end_date | timeFormat:\'LL\'"></td>\n' +
    '                                <td ng-bind="position.comment"></td>\n' +
    '                            </tr>\n' +
    '                        </tbody>\n' +
    '                    </table>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row">\n' +
    '        <h4 class="center-dash">Availabilities</h4>\n' +
    '        <div class="row">\n' +
    '            <div class="col-md-12 availablity-wrapper">\n' +
    '                <div class="day-wrapper set-availablity" ng-repeat="(key, day) in  data.dayAvailabilities">\n' +
    '                    <h5>{{day.name}} <span class="pull-right day-show" ng-class="{\'active\': day.selected}"></span></h5>\n' +
    '                    <div class="select-wrap">\n' +
    '                        <p id="day_{{key}}_{{k}}" ng-repeat="(k, availability) in day.availabilities">{{availability.getTitle()}} <span class="pull-right time-select" ng-class="{\'time-color\': availability.selected}"></span></p>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row center-align" ng-if="data.user.employee.getDrivingLicense() != \'\'">\n' +
    '        <h4 class="center-dash">Driving License</h4>\n' +
    '        <div class="driving-license">\n' +
    '            <p ng-bind="data.user.employee.getDrivingLicense()"></p>\n' +
    '        </div>\n' +
    '        <div class="row">\n' +
    '            <div class="col s4 driving-license-image">\n' +
    '                <!-- <img ng-src="{{data.user.getDrivingLicenseImage()}}"> -->\n' +
    '                <nf-pdf ng-if="data.user.getDrivingLicenseImageExtension() === \'pdf\'" url="data.user.getDrivingLicenseImage()" options="vm.nfPdfOptions"></nf-pdf>\n' +
    '                <img ng-if="data.user.getDrivingLicenseImageExtension() !== \'pdf\'" ng-src="{{data.user.getDrivingLicenseImage()}}">\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row center-align" ng-if="data.user.employee.getVehicle() != \'\'">\n' +
    '        <h4 class="center-dash">Vehicle</h4>\n' +
    '        <div class="vehicles">\n' +
    '            <p ng-bind="data.user.employee.getVehicle()"></p>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/employee/list/_list.tpl.html',
    '<div class="list-users">\n' +
    '    <div class="row action-box">\n' +
    '        <div class="col offset-s8 s4 user-action right-align">\n' +
    '            <div class="more-action">\n' +
    '                <md-menu>\n' +
    '                    <md-button aria-label="Open phone interactions menu" ng-click="openMenu($mdOpenMenu, $event)">\n' +
    '                        More <i class="icon material-icons">expand_more</i>\n' +
    '                    </md-button>\n' +
    '                    <md-menu-content width="4">\n' +
    '                        <md-menu-item>\n' +
    '                            <md-button ng-click="vm.verifyIdStatus(\'verified\')">\n' +
    '                                Verify ID\n' +
    '                            </md-button>\n' +
    '                        </md-menu-item>\n' +
    '                        <md-menu-divider></md-menu-divider>\n' +
    '                        <md-menu-item>\n' +
    '                            <md-button ng-click="vm.verifyIdStatus(\'rejected\')">\n' +
    '                                Reject ID\n' +
    '                            </md-button>\n' +
    '                        </md-menu-item>\n' +
    '                        <md-menu-divider></md-menu-divider>\n' +
    '                        <md-menu-item>\n' +
    '                            <md-button ng-click="vm.resendVerifyEmail()">\n' +
    '                                Resend verify Email\n' +
    '                            </md-button>\n' +
    '                        </md-menu-item>\n' +
    '                    </md-menu-content>\n' +
    '                </md-menu>\n' +
    '            </div>\n' +
    '            <ul>\n' +
    '                <!-- <li class="active" ng-if="user.can(\'update.users\')">\n' +
    '                    <a ng-click="vm.changeStatusAllUser(\'active\')" title="Active User"><i class="material-icons">check_circle</i></a>\n' +
    '                </li>\n' +
    '                <li class="pending" ng-if="user.can(\'update.users\')">\n' +
    '                    <a ng-click="vm.changeStatusAllUser(\'pending\')" title="Pending User"><i class="material-icons">pause_circle_filled</i></a>\n' +
    '                </li>\n' +
    '                <li class="archive" ng-if="user.can(\'update.users\')">\n' +
    '                    <a ng-click="vm.changeStatusAllUser(\'banned\')" title="Ban User"><i class="material-icons">cancel</i></a>\n' +
    '                </li> -->\n' +
    '                <li class="new-user" ng-if="user.can(\'create.users\')">\n' +
    '                    <a ui-sref="app.employees.create" title="Create new Employee"><i class="material-icons">add_circle</i></a>\n' +
    '                </li>\n' +
    '            </ul>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row filter">\n' +
    '        <div class="col m6 s12 filter-tag">\n' +
    '            <label for="tag">Tag</label>\n' +
    '            <select ui-select2 multiple ng-model="vm.tag" ng-options="options as options.getName() for options in vm.tags" ng-change="vm.changeTag(vm.tag)"></select>\n' +
    '        </div>\n' +
    '        <div class="col m2 s12 filter-status">\n' +
    '            <label for="status">Status</label>\n' +
    '            <select ng-model="vm.status" ng-change="vm.filterStatus(vm.status)" ng-options="options as options.status for options in vm.user_status"></select>\n' +
    '        </div>\n' +
    '        <div class="col m2 s12 right-align all-box">\n' +
    '            <a class="full-width waves-effect waves-light btn bg-green" ng-class="{\'disabled\': vm.params.list !== \'today\'}" ui-sref="app.employees.all({list: \'all\', page: 1})">All</a>\n' +
    '        </div>\n' +
    '        <div class="col m2 s12 right-align today">\n' +
    '            <a class="full-width waves-effect waves-light btn bg-green" ng-class="{\'disabled\': vm.params.list === \'today\'}" ui-sref="app.employees.all({list: \'today\', page: 1})">Today</a>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row search-box">\n' +
    '        <div class="col s12 m6 select-all">\n' +
    '            <input type="checkbox" id="select-all" ng-model="vm.selectedAll" ng-click="vm.selectAll()">\n' +
    '            <label for="select-all">Select all</label>\n' +
    '            <span class="number-select" ng-show="vm.selectedUser.length > 0"> ( All {{vm.selectedUser.length}} {{vm.selectedUser.length === 1 ? \'item\' : \'items\'}}  on this page are selected.)</span>\n' +
    '        </div>\n' +
    '        <div class="search col m6 s12">\n' +
    '            <div class="search-wrapper card">\n' +
    '                <input id="search" class="input-search" ng-model="vm.params.search" placeholder="Search..." ng-keydown="vm.searchKeyEvent($event, vm.params.search)"><i class="icon-search material-icons" ng-click="vm.searchPage(vm.params.search)">search</i>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '        <div class="row">\n' +
    '            <div class="col s12 m6">\n' +
    '                Sort By:\n' +
    '                <sortable-attributes></sortable-attributes>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <preloader ng-if="vm.users | isUndefined"></preloader>\n' +
    '    <div class="row">\n' +
    '        <div class="col s12">\n' +
    '            <table class="striped table-users" ng-if="vm.users !== undefined">\n' +
    '                <thead>\n' +
    '                    <tr>\n' +
    '                        <th></th>\n' +
    '                        <th class="clickable id-col">ID <span sortable="id"></span></th>\n' +
    '                        <th class="clickable email-col">Email <span sortable="email"></span></th>\n' +
    '                        <th class="clickable name-col">Name <span sortable="first_name"></span></th>\n' +
    '                        <th class="clickable city-col">City <span sortable="city"></span></th>\n' +
    '                        <th class="clickable city-col">Profile Completed <span sortable="profile_completed_percent"></span></th>\n' +
    '                        <th>Status <span sortable="status"></span></th>\n' +
    '                        <th>ID verification <span sortable="identity_check_status"></span></th>\n' +
    '                        <th>What\'s app</th>\n' +
    '                        <th>Date of registration <span sortable="created_at"></span></th>\n' +
    '                        <th>Activated <span sortable="activated"></span></th>\n' +
    '                        <th>Review score</th>\n' +
    '                        <th>Tags</th>\n' +
    '                        <th>Description</th>\n' +
    '                        <th>Private Note</th>\n' +
    '                        <th>Salary</th>\n' +
    '                        <th class="right-align">Action</th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody>\n' +
    '                    <tr ng-repeat="u in vm.users">\n' +
    '                        <td>\n' +
    '                            <md-checkbox ng-model="u.selected" aria-label="Select User" ng-change="vm.updateSelectedUser()">\n' +
    '                            </md-checkbox>\n' +
    '                        </td>\n' +
    '                        <td ng-bind="u.getId()"></td>\n' +
    '                        <td>\n' +
    '                            <span ng-bind="u.getEmail()"></span>\n' +
    '                            <i ng-if="u.getEmailVerified()" class="icon-verify material-icons green-text">check_circle</i>\n' +
    '                            <i ng-if="u.getIsLoggedOnApp()" class="material-icons green-text">stay_current_portrait</i>\n' +
    '                        </td>\n' +
    '                        <td>\n' +
    '                            <span ng-bind="u.getNameAdminList()"></span>\n' +
    '                            <a class="hint" expand-data data-id="u.getId()" api="user" template="/webapp/js/admin/components/employee/list/_employee_expand.tpl.html">\n' +
    '                                <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Show expand data"></i>\n' +
    '                            </a>\n' +
    '                        </td>\n' +
    '                        <td ng-bind="u.getCity()"></td>\n' +
    '                        <td>{{u.getProfileCompletedPercent()}}%</td>\n' +
    '                        <td ng-bind="u.user_status"></td>\n' +
    '                        <td class="text-capitalize">\n' +
    '                            <span ng-if="u.getIdImage()" ng-bind="u.getIdentityCheckStatus()"></span>\n' +
    '                        </td>\n' +
    '                        <td ng-bind="u.getWhatsApp()"></td>\n' +
    '                        <td ng-bind="u.getCreatedAt() | timeFormat: \'lll\'"></td>\n' +
    '                        <td>\n' +
    '                            <span ng-show="u.getActivated() != \'0000-00-00 00:00:00\'" ng-bind="u.getActivated() | timeFormat: \'DD/MM/YYYY\'"></span>\n' +
    '                        </td>\n' +
    '                        <td><span ng-if="u.getAvgStar() != 0 && u.getTotalReview() != 0">{{u.getAvgStar()}} ({{u.getTotalReview()}})</span></td>\n' +
    '                        <td class="ee-tags">\n' +
    '                            <md-chips readonly="true" ng-model="u.tags">\n' +
    '                                <md-chip-template>\n' +
    '                                    <span><strong>{{$chip.name}}</strong></span>\n' +
    '                                </md-chip-template>\n' +
    '                            </md-chips>\n' +
    '                        </td>\n' +
    '                        <td><a ng-if="u.getSummary() != \'\'" title="{{u.getSummary()}}"><i class="material-icons">more_horiz</i></a></td>\n' +
    '                        <td class="private-note center-align">\n' +
    '                            <a ng-if="!u.hasPrivateNote" title="Write note" ng-click="vm.addPrivateNote($event, u)">\n' +
    '                                <i class="icon material-icons">add</i>\n' +
    '                            </a>\n' +
    '                            <div ng-if="u.hasPrivateNote">\n' +
    '                                <p ng-bind="u.employee.private_note"></p>\n' +
    '                                <a title="Edit note" ng-click="vm.addPrivateNote($event, u)">\n' +
    '                                    <i class="icon material-icons">edit</i>\n' +
    '                                </a>\n' +
    '                            </div>\n' +
    '                        </td>\n' +
    '                        <td class="private-note center-align">\n' +
    '                            <a ng-if="!u.hasSalary" title="Add salary" ng-click="vm.addSalary($event, u)">\n' +
    '                                <i class="icon material-icons">add</i>\n' +
    '                            </a>\n' +
    '                            <div ng-if="u.hasSalary">\n' +
    '                                <p ng-bind="u.employee.salary"></p>\n' +
    '                                <a title="Edit salary" ng-click="vm.addSalary($event, u)">\n' +
    '                                    <i class="icon material-icons">edit</i>\n' +
    '                                </a>\n' +
    '                            </div>\n' +
    '                        </td>\n' +
    '                        <td class="right-align action">\n' +
    '                            <a ng-if="user.can(\'update.users\')" ui-sref="app.change-password-user({id: u.getId()})" class="change-password">\n' +
    '                                <p><i class="material-icons">lock</i></p>\n' +
    '                            </a>\n' +
    '                            <a ng-if="user.can(\'view.dashboard.as.employee\')" ng-click="vm.loginAsUser(u.getId())" class="view-dashboard">\n' +
    '                                <p><i class="material-icons">visibility</i></p>\n' +
    '                            </a>\n' +
    '                            <a ng-if="user.can(\'update.users\')" ui-sref="app.employees.edit({id: u.getId()})" class="edit">\n' +
    '                                <p><i class="material-icons">mode_edit</i></p>\n' +
    '                            </a>\n' +
    '                            <a ng-if="user.can(\'delete.users\')" ng-click="vm.deleteUser(u)" class="delete">\n' +
    '                                <p><i class="material-icons">delete</i></p>\n' +
    '                            </a>\n' +
    '                        </td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '            <pagination data="vm.pagination"></pagination>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/employee/list/_private_note.tpl.html',
    '<div class="private-note-employee-modal">\n' +
    '    <div class="modal-header">\n' +
    '        Private Note for {{user.getName()}}\n' +
    '        <a class="right close" ng-click="cancel()">\n' +
    '            <i class="material-icons">clear</i>\n' +
    '        </a>\n' +
    '    </div>\n' +
    '    <div class="modal-body">\n' +
    '        <form name="privateNoteEmployeeForm" ng-submit="privateNoteEmployeeForm.$valid && privateNoteEmployee(private_note)" novalidate>\n' +
    '            <div class="note-box">\n' +
    '                <div class="content">\n' +
    '                    <textarea ng-model="private_note" cols="30" rows="10" name="private_note"></textarea>\n' +
    '                </div>\n' +
    '                <div class="action">\n' +
    '                    <button class="note btn waves-effect waves-light" ng-class="{\'disabled\': inProgress}" type="submit" name="action">submit\n' +
    '                        <i class="material-icons right">send</i>\n' +
    '                        <i class="material-icons right icon-pending">replay</i>\n' +
    '                    </button>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </form>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/employee/list/_salary.tpl.html',
    '<div class="employee-salary-modal">\n' +
    '    <div class="modal-header">\n' +
    '        Salary for {{user.getName()}}\n' +
    '        <a class="right close" ng-click="cancel()">\n' +
    '            <i class="material-icons">clear</i>\n' +
    '        </a>\n' +
    '    </div>\n' +
    '    <div class="modal-body">\n' +
    '        <form name="EmployeeSalaryForm" ng-submit="EmployeeSalaryForm.$valid && EmployeeSalary(salary)" novalidate>\n' +
    '            <div class="note-box">\n' +
    '                <div class="content">\n' +
    '                    <input type="text" class="form-control" ng-model="salary" name="salary" ng-pattern="/^\\d+$/" ng-maxlength="20">\n' +
    '                    <div class="errors">\n' +
    '                        <span ng-show="EmployeeSalaryForm.$submitted && EmployeeSalaryForm.salary.$error.maxlength">The value is too long</span>\n' +
    '                        <span ng-show="EmployeeSalaryForm.$submitted && EmployeeSalaryForm.salary.$error.pattern">Please enter a series of numbers</span>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="action">\n' +
    '                    <button class="note btn waves-effect waves-light" ng-class="{\'disabled\': inProgress}" type="submit" name="action">submit\n' +
    '                        <i class="material-icons right">send</i>\n' +
    '                        <i class="material-icons right icon-pending">replay</i>\n' +
    '                    </button>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </form>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/experience/create/_create.tpl.html',
    '<div class="create-page create-experience">\n' +
    '    <div class="col-xs-12">\n' +
    '        <div class="create-form create-experience-form">\n' +
    '            <h2 class="text-center">Create new Experience</h2>\n' +
    '            <form name="vm.create_experience_form" ng-submit="vm.create_experience_form.$valid && vm.createExperience(vm.experience)" novalidate>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12 col-sm-6">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Label *</label>\n' +
    '                            <input type="text" ng-focus="vm.create_experience_form.label.$focused = true" class="form-control" ng-model="vm.experience.label" name="label" required ng-maxlength="255">\n' +
    '                            <div class="icons" ng-show="vm.create_experience_form.label.$focused">\n' +
    '                                <i ng-if="!vm.create_experience_form.label.$error.required && !vm.create_experience_form.label.$error.maxlength" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.create_experience_form.label.$error.required || vm.create_experience_form.label.$error.maxlength" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.create_experience_form.$submitted && vm.create_experience_form.label.$error.required">Required</span>\n' +
    '                                <span ng-show="vm.create_experience_form.$submitted && vm.create_experience_form.label.$error.maxlength">The value is too long</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="col-xs-12 col-sm-6">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Order</label>\n' +
    '                            <input type="text" ng-focus="vm.create_experience_form.order.$focused = true" class="form-control" ng-model="vm.experience.order" name="order" ng-pattern="/^\\d+$/">\n' +
    '                            <div class="icons" ng-show="vm.create_experience_form.order.$focused">\n' +
    '                                <i ng-if="!vm.create_experience_form.order.$error.pattern" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.create_experience_form.order.$error.pattern" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.create_experience_form.$submitted && vm.create_experience_form.order.$error.pattern">Please enter a number</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12">\n' +
    '                        <div class="form-group">\n' +
    '                            <label class="active">Active</label>\n' +
    '                            <input type="checkbox" ng-focus="vm.create_experience_form.status.$focused = true" ng-model="vm.experience.status" name="status" ng-true-value="1" ng-false-value="0">\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="form-group">\n' +
    '                    <input ng-disabled="inProgress" type="submit" class="btn ui-btn btn-full-width orange-btn-sm" value="Add">\n' +
    '                </div>\n' +
    '            </form>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/experience/edit/_edit.tpl.html',
    '<div class="edit-page edit-experience">\n' +
    '    <div class="col-xs-12">\n' +
    '        <div class="edit-form edit-experience-form">\n' +
    '            <h2 class="text-center">Edit Experience</h2>\n' +
    '            <form name="vm.edit_experience_form" ng-submit="vm.edit_experience_form.$valid && vm.editExperience(vm.experience)" novalidate>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12 col-sm-6">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Label *</label>\n' +
    '                            <input type="text" ng-focus="vm.edit_experience_form.label.$focused = true" class="form-control" ng-model="vm.experience.label" name="label" required ng-maxlength="255">\n' +
    '                            <div class="icons" ng-show="vm.edit_experience_form.label.$focused">\n' +
    '                                <i ng-if="!vm.edit_experience_form.label.$error.required && !vm.edit_experience_form.label.$error.maxlength" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.edit_experience_form.label.$error.required || vm.edit_experience_form.label.$error.maxlength" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.edit_experience_form.$submitted && vm.edit_experience_form.label.$error.required">Required</span>\n' +
    '                                <span ng-show="vm.edit_experience_form.$submitted && vm.edit_experience_form.label.$error.maxlength">The value is too long</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="col-xs-12 col-sm-6">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Order</label>\n' +
    '                            <input type="text" ng-focus="vm.edit_experience_form.order.$focused = true" class="form-control" ng-model="vm.experience.order" name="order" ng-pattern="/^\\d+$/">\n' +
    '                            <div class="icons" ng-show="vm.edit_experience_form.order.$focused">\n' +
    '                                <i ng-if="!vm.edit_experience_form.order.$error.pattern" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.edit_experience_form.order.$error.pattern" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.edit_experience_form.$submitted && vm.edit_experience_form.order.$error.pattern">Please enter a number</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12">\n' +
    '                        <div class="form-group">\n' +
    '                            <label class="active">Active</label>\n' +
    '                            <input type="checkbox" ng-focus="vm.edit_experience_form.status.$focused = true" ng-model="vm.experience.status" name="status" ng-true-value="1" ng-false-value="0">\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="form-group">\n' +
    '                    <input ng-disabled="inProgress" type="submit" class="btn ui-btn btn-full-width orange-btn-sm" value="Edit">\n' +
    '                </div>\n' +
    '            </form>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/experience/list/_list.tpl.html',
    '<div class="list-page list-experiences">\n' +
    '    <div class="row filter">\n' +
    '        <div class="col-sm-2 col-xs-12 filter-status">\n' +
    '            <label class="ui-label" for="status">Status</label>\n' +
    '            <select class="ui-select" ng-model="vm.status" ng-change="vm.filterStatus(vm.status)" ng-options="options as options.name for options in vm.statuses"></select>\n' +
    '        </div>\n' +
    '        <div class="col-sm-3 col-sm-offset-7 text-right item-action new-record-wrapper">\n' +
    '            <ul>\n' +
    '                <li class="active">\n' +
    '                    <a ng-click="vm.changeStatusAllItem(\'active\')" title="Active">\n' +
    '                        <i class="icon fa fa-check-circle"></i>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '                <li class="pending">\n' +
    '                    <a ng-click="vm.changeStatusAllItem(\'pending\')" title="Pending">\n' +
    '                        <i class="icon fa fa-pause-circle"></i>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '                <li class="new-item">\n' +
    '                    <a ui-sref="app.experience.create" title="Create new Experience">\n' +
    '                        <i class="icon fa fa-plus-circle"></i>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '            </ul>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row search-box">\n' +
    '        <div class="col-xs-12 col-sm-6 select-all">\n' +
    '            <input type="checkbox" id="select-all" ng-model="vm.selectedAll" ng-change="vm.selectAll()">\n' +
    '            <label for="select-all">Select all</label>\n' +
    '            <span class="number-select" ng-show="vm.selectedItem.length > 0"> ( All {{vm.selectedItem.length}} {{vm.selectedItem.length === 1 ? \'item\' : \'items\'}}  on this page are selected.)</span>\n' +
    '        </div>\n' +
    '        <div class="search col-xs-12 col-sm-6">\n' +
    '            <div class="search-wrapper card">\n' +
    '                <input id="search" class="input-search form-control" ng-model="vm.params.search" placeholder="Search..." ng-keydown="vm.searchKeyEvent($event, vm.params.search)"><i class="icon-search fa fa-search" ng-click="vm.searchPage(vm.params.search)"></i>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- <div class="row">\n' +
    '        <div class="col-xs-12 col-sm-6">\n' +
    '            Sort By:\n' +
    '            <sortable-attributes></sortable-attributes>\n' +
    '        </div>\n' +
    '    </div> -->\n' +
    '    <preloader ng-if="vm.experiences | isUndefined"></preloader>\n' +
    '    <div class="row">\n' +
    '        <div class="col-xs-12">\n' +
    '            <table class="table table-striped ui-table table-items table-experiences" ng-if="vm.experiences !== undefined">\n' +
    '                <thead>\n' +
    '                    <tr>\n' +
    '                        <th></th>\n' +
    '                        <th>ID <!-- <span sortable="id"></span> --></th>\n' +
    '                        <th>Label <!-- <span sortable="name"></span> --></th>\n' +
    '                        <th>Order <!-- <span sortable="order"></span> --></th>\n' +
    '                        <th>Active <!-- <span sortable="status"></span> --></th>\n' +
    '                        <th>Status</th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody>\n' +
    '                    <tr ng-repeat="experience in vm.experiences">\n' +
    '                        <td>\n' +
    '                            <input type="checkbox" ng-model="experience.selected" ng-change="vm.updateSelectedItem()">\n' +
    '                        </td>\n' +
    '                        <td ng-bind="experience.getId()"></td>\n' +
    '                        <td ng-bind="experience.getLabel()"></td>\n' +
    '                        <td ng-bind="experience.getOrder()"></td>\n' +
    '                        <td>\n' +
    '                            {{ experience.getStatus() === 1 ? \'Active\' : \'Pending\' }}\n' +
    '                        </td>\n' +
    '                        <td class="text-right action">\n' +
    '                            <a ui-sref="app.experience.edit({id: experience.getId()})" class="edit">\n' +
    '                                <p class="ui-p"><i class="icon fa fa-pencil-square-o"></i></p>\n' +
    '                            </a>\n' +
    '                            <a ng-click="vm.deleteItem(experience)" class="delete">\n' +
    '                                <p class="ui-p"><i class="icon fa fa-trash"></i></p>\n' +
    '                            </a>\n' +
    '                        </td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '            <pagination data="vm.pagination"></pagination>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/industries/create/_create.tpl.html',
    '<div class="create-page create-industry">\n' +
    '    <div class="col-xs-12">\n' +
    '        <div class="create-form create-industry-form">\n' +
    '            <h2 class="text-center">Create new Industry</h2>\n' +
    '            <form name="vm.create_industry_form" ng-submit="vm.create_industry_form.$valid && vm.createIndustry(vm.industry)" novalidate>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12 col-sm-6">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Name *</label>\n' +
    '                            <input type="text" ng-focus="vm.create_industry_form.name.$focused = true" class="form-control" ng-model="vm.store.getState().CreateIndustry.name" name="name" required ng-maxlength="255" ng-change="vm.changeName(vm.store.getState().CreateIndustry.name)">\n' +
    '                            <div class="icons" ng-show="vm.create_industry_form.name.$focused">\n' +
    '                                <i ng-if="!vm.create_industry_form.name.$error.required && !vm.create_industry_form.name.$error.maxlength" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.create_industry_form.name.$error.required || vm.create_industry_form.name.$error.maxlength" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.create_industry_form.$submitted && vm.create_industry_form.name.$error.required">Required</span>\n' +
    '                                <span ng-show="vm.create_industry_form.$submitted && vm.create_industry_form.name.$error.maxlength">The value is too long</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="col-xs-12 col-sm-6">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Order</label>\n' +
    '                            <input type="text" ng-focus="vm.create_industry_form.order.$focused = true" class="form-control" ng-model="vm.store.getState().CreateIndustry.order" name="order" ng-pattern="/^\\d+$/" ng-change="vm.changeOrder(vm.store.getState().CreateIndustry.order)">\n' +
    '                            <div class="icons" ng-show="vm.create_industry_form.order.$focused">\n' +
    '                                <i ng-if="!vm.create_industry_form.order.$error.pattern" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.create_industry_form.order.$error.pattern" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.create_industry_form.$submitted && vm.create_industry_form.order.$error.pattern">Please enter a number</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12">\n' +
    '                        <div class="form-group">\n' +
    '                            <label class="active">Active</label>\n' +
    '                            <input type="checkbox" ng-focus="vm.create_industry_form.status.$focused = true" ng-model="vm.store.getState().CreateIndustry.status" name="status" ng-true-value="1" ng-false-value="0" ng-change="vm.changeStatus(vm.store.getState().CreateIndustry.status)">\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="form-group">\n' +
    '                    <input ng-disabled="inProgress" type="submit" class="btn ui-btn btn-full-width orange-btn-sm" value="Add">\n' +
    '                </div>\n' +
    '            </form>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/industries/edit/_edit.tpl.html',
    '<div class="edit-page edit-industry">\n' +
    '    <div class="col-xs-12">\n' +
    '        <div class="edit-form edit-industry-form">\n' +
    '            <h2 class="text-center">Edit Industry</h2>\n' +
    '            <form name="vm.edit_industry_form" ng-submit="vm.edit_industry_form.$valid && vm.editIndustry(vm.industry)" novalidate>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12 col-sm-6">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Name *</label>\n' +
    '                            <input type="text" ng-focus="vm.edit_industry_form.name.$focused = true" class="form-control" ng-model="vm.store.getState().UpdateIndustry.name" name="name" required ng-maxlength="255" ng-change="vm.changeName(vm.store.getState().UpdateIndustry.name)">\n' +
    '                            <div class="icons" ng-show="vm.edit_industry_form.name.$focused">\n' +
    '                                <i ng-if="!vm.edit_industry_form.name.$error.required && !vm.edit_industry_form.name.$error.maxlength" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.edit_industry_form.name.$error.required || vm.edit_industry_form.name.$error.maxlength" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.edit_industry_form.$submitted && vm.edit_industry_form.name.$error.required">Required</span>\n' +
    '                                <span ng-show="vm.edit_industry_form.$submitted && vm.edit_industry_form.name.$error.maxlength">The value is too long</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="col-xs-12 col-sm-6">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Order</label>\n' +
    '                            <input type="text" ng-focus="vm.edit_industry_form.order.$focused = true" class="form-control" ng-model="vm.store.getState().UpdateIndustry.order" name="order" ng-pattern="/^\\d+$/" ng-change="vm.changeOrder(vm.store.getState().UpdateIndustry.order)">\n' +
    '                            <div class="icons" ng-show="vm.edit_industry_form.order.$focused">\n' +
    '                                <i ng-if="!vm.edit_industry_form.order.$error.pattern" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.edit_industry_form.order.$error.pattern" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.edit_industry_form.$submitted && vm.edit_industry_form.order.$error.pattern">Please enter a number</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12">\n' +
    '                        <div class="form-group">\n' +
    '                            <label class="active">Active</label>\n' +
    '                            <input type="checkbox" ng-focus="vm.edit_industry_form.status.$focused = true" ng-model="vm.store.getState().UpdateIndustry.status" name="status" ng-true-value="1" ng-false-value="0" ng-change="vm.changeStatus(vm.store.getState().UpdateIndustry.status)">\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="form-group">\n' +
    '                    <input ng-disabled="inProgress" type="submit" class="btn ui-btn btn-full-width orange-btn-sm" value="Edit">\n' +
    '                </div>\n' +
    '            </form>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/industries/list/_list.tpl.html',
    '<div class="list-page list-industries">\n' +
    '    <div class="row filter">\n' +
    '        <div class="col-sm-2 col-xs-12 filter-status">\n' +
    '            <label class="ui-label" for="status">Status</label>\n' +
    '            <select class="ui-select" ng-model="vm.status" ng-change="vm.filterStatus(vm.status)" ng-options="options as options.name for options in vm.statuses"></select>\n' +
    '        </div>\n' +
    '        <div class="col-sm-3 col-sm-offset-7 text-right item-action new-record-wrapper">\n' +
    '            <ul>\n' +
    '                <li class="active">\n' +
    '                    <a ng-click="vm.changeStatusAllItem(\'active\')" title="Active">\n' +
    '                        <i class="icon fa fa-check-circle"></i>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '                <li class="pending">\n' +
    '                    <a ng-click="vm.changeStatusAllItem(\'pending\')" title="Pending">\n' +
    '                        <i class="icon fa fa-pause-circle"></i>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '                <li class="new-item">\n' +
    '                    <a ui-sref="app.industries.create" title="Create new Industry">\n' +
    '                        <i class="icon fa fa-plus-circle"></i>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '            </ul>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row search-box">\n' +
    '        <div class="col-xs-12 col-sm-6 select-all">\n' +
    '            <input type="checkbox" id="select-all" ng-model="vm.selectedAll" ng-change="vm.selectAll()">\n' +
    '            <label for="select-all">Select all</label>\n' +
    '            <span class="number-select" ng-show="vm.selectedItem.length > 0"> ( All {{vm.selectedItem.length}} {{vm.selectedItem.length === 1 ? \'item\' : \'items\'}}  on this page are selected.)</span>\n' +
    '        </div>\n' +
    '        <div class="search col-xs-12 col-sm-6">\n' +
    '            <div class="search-wrapper card">\n' +
    '                <input id="search" class="input-search form-control" ng-model="vm.params.search" placeholder="Search..." ng-keydown="vm.searchKeyEvent($event, vm.params.search)"><i class="icon-search fa fa-search" ng-click="vm.searchPage(vm.params.search)"></i>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- <div class="row">\n' +
    '        <div class="col-xs-12 col-sm-6">\n' +
    '            Sort By:\n' +
    '            <sortable-attributes></sortable-attributes>\n' +
    '        </div>\n' +
    '    </div> -->\n' +
    '    <preloader ng-if="vm.industries | isUndefined"></preloader>\n' +
    '    <div class="row">\n' +
    '        <div class="col-xs-12">\n' +
    '            <table class="table table-striped ui-table table-items table-industries" ng-if="vm.industries !== undefined">\n' +
    '                <thead>\n' +
    '                    <tr>\n' +
    '                        <th></th>\n' +
    '                        <th>ID <!-- <span sortable="id"></span> --></th>\n' +
    '                        <th>Name <!-- <span sortable="name"></span> --></th>\n' +
    '                        <th>Order <!-- <span sortable="order"></span> --></th>\n' +
    '                        <th>Active <!-- <span sortable="status"></span> --></th>\n' +
    '                        <th>Status</th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody>\n' +
    '                    <tr ng-repeat="industry in vm.industries">\n' +
    '                        <td>\n' +
    '                            <input type="checkbox" ng-model="industry.selected" ng-change="vm.updateSelectedItem()">\n' +
    '                        </td>\n' +
    '                        <td ng-bind="industry.getId()"></td>\n' +
    '                        <td ng-bind="industry.getName()"></td>\n' +
    '                        <td ng-bind="industry.getOrder()"></td>\n' +
    '                        <td>\n' +
    '                            {{ industry.getStatus() === 1 ? \'Active\' : \'Pending\' }}\n' +
    '                        </td>\n' +
    '                        <td class="text-right action">\n' +
    '                            <a ui-sref="app.industries.edit({id: industry.getId()})" class="edit">\n' +
    '                                <p class="ui-p"><i class="icon fa fa-pencil-square-o"></i></p>\n' +
    '                            </a>\n' +
    '                            <a ng-click="vm.deleteItem(industry)" class="delete">\n' +
    '                                <p class="ui-p"><i class="icon fa fa-trash"></i></p>\n' +
    '                            </a>\n' +
    '                        </td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '            <pagination data="vm.pagination"></pagination>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/job_titles/create/_create.tpl.html',
    '<div class="create-page create-job-title">\n' +
    '    <div class="col-xs-12">\n' +
    '        <div class="create-form create-job-title-form">\n' +
    '            <h2 class="text-center">Create new Job Title</h2>\n' +
    '            <form name="vm.create_job_title_form" ng-submit="vm.create_job_title_form.$valid && vm.createJobTitle(vm.job_title)" novalidate>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Name *</label>\n' +
    '                            <input type="text" ng-focus="vm.create_job_title_form.name.$focused = true" class="form-control" ng-model="vm.job_title.name" name="name" required ng-maxlength="255">\n' +
    '                            <div class="icons" ng-show="vm.create_job_title_form.name.$focused">\n' +
    '                                <i ng-if="!vm.create_job_title_form.name.$error.required && !vm.create_job_title_form.name.$error.maxlength" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.create_job_title_form.name.$error.required || vm.create_job_title_form.name.$error.maxlength" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.create_job_title_form.$submitted && vm.create_job_title_form.name.$error.required">Required</span>\n' +
    '                                <span ng-show="vm.create_job_title_form.$submitted && vm.create_job_title_form.name.$error.maxlength">The value is too long</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Job Title *</label>\n' +
    '                            <input type="text" ng-focus="vm.create_job_title_form.job_title.$focused = true" class="form-control" ng-model="vm.job_title.job_title" name="job_title" required ng-maxlength="255">\n' +
    '                            <div class="icons" ng-show="vm.create_job_title_form.job_title.$focused">\n' +
    '                                <i ng-if="!vm.create_job_title_form.job_title.$error.required && !vm.create_job_title_form.job_title.$error.maxlength" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.create_job_title_form.job_title.$error.required || vm.create_job_title_form.job_title.$error.maxlength" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.create_job_title_form.$submitted && vm.create_job_title_form.job_title.$error.required">Required</span>\n' +
    '                                <span ng-show="vm.create_job_title_form.$submitted && vm.create_job_title_form.job_title.$error.maxlength">The value is too long</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12">\n' +
    '                        <div class="form-group">\n' +
    '                            <label class="active">Job Description</label>\n' +
    '                            <textarea name="job_description" cols="30" rows="5" ng-focus="vm.create_job_title_form.job_description.$focused = true" class="form-control" ng-model="vm.job_title.job_description" name="job_description"></textarea>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Order</label>\n' +
    '                            <input type="text" ng-focus="vm.create_job_title_form.order.$focused = true" class="form-control" ng-model="vm.job_title.order" name="order" ng-pattern="/^\\d+$/">\n' +
    '                            <div class="icons" ng-show="vm.create_job_title_form.order.$focused">\n' +
    '                                <i ng-if="!vm.create_job_title_form.order.$error.pattern" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.create_job_title_form.order.$error.pattern" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.create_job_title_form.$submitted && vm.create_job_title_form.order.$error.pattern">Please enter a number</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12">\n' +
    '                        <div class="form-group">\n' +
    '                            <label class="active">Active</label>\n' +
    '                            <input type="checkbox" ng-focus="vm.create_job_title_form.status.$focused = true" ng-model="vm.job_title.status" name="status" ng-true-value="1" ng-false-value="0">\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="form-group">\n' +
    '                    <input ng-disabled="inProgress" type="submit" class="btn ui-btn btn-full-width orange-btn-sm" value="Add">\n' +
    '                </div>\n' +
    '            </form>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/job_titles/edit/_edit.tpl.html',
    '<div class="edit-page edit-job-title">\n' +
    '    <div class="col-xs-12">\n' +
    '        <div class="edit-form edit-job-title-form">\n' +
    '            <h2 class="text-center">Edit Job Title</h2>\n' +
    '            <form name="vm.edit_job_title_form" ng-submit="vm.edit_job_title_form.$valid && vm.editJobTitle(vm.job_title)" novalidate>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Name *</label>\n' +
    '                            <input type="text" ng-focus="vm.create_job_title_form.name.$focused = true" class="form-control" ng-model="vm.job_title.name" name="name" required ng-maxlength="255">\n' +
    '                            <div class="icons" ng-show="vm.create_job_title_form.name.$focused">\n' +
    '                                <i ng-if="!vm.create_job_title_form.name.$error.required && !vm.create_job_title_form.name.$error.maxlength" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.create_job_title_form.name.$error.required || vm.create_job_title_form.name.$error.maxlength" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.create_job_title_form.$submitted && vm.create_job_title_form.name.$error.required">Required</span>\n' +
    '                                <span ng-show="vm.create_job_title_form.$submitted && vm.create_job_title_form.name.$error.maxlength">The value is too long</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Job Title *</label>\n' +
    '                            <input type="text" ng-focus="vm.create_job_title_form.job_title.$focused = true" class="form-control" ng-model="vm.job_title.job_title" name="job_title" required ng-maxlength="255">\n' +
    '                            <div class="icons" ng-show="vm.create_job_title_form.job_title.$focused">\n' +
    '                                <i ng-if="!vm.create_job_title_form.job_title.$error.required && !vm.create_job_title_form.job_title.$error.maxlength" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.create_job_title_form.job_title.$error.required || vm.create_job_title_form.job_title.$error.maxlength" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.create_job_title_form.$submitted && vm.create_job_title_form.job_title.$error.required">Required</span>\n' +
    '                                <span ng-show="vm.create_job_title_form.$submitted && vm.create_job_title_form.job_title.$error.maxlength">The value is too long</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12">\n' +
    '                        <div class="form-group">\n' +
    '                            <label class="active">Job Description</label>\n' +
    '                            <textarea name="job_description" cols="30" rows="5" ng-focus="vm.create_job_title_form.job_description.$focused = true" class="form-control" ng-model="vm.job_title.job_description" name="job_description"></textarea>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Order</label>\n' +
    '                            <input type="text" ng-focus="vm.edit_job_title_form.order.$focused = true" class="form-control" ng-model="vm.job_title.order" name="order" ng-pattern="/^\\d+$/">\n' +
    '                            <div class="icons" ng-show="vm.edit_job_title_form.order.$focused">\n' +
    '                                <i ng-if="!vm.edit_job_title_form.order.$error.pattern" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.edit_job_title_form.order.$error.pattern" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.edit_job_title_form.$submitted && vm.edit_job_title_form.order.$error.pattern">Please enter a number</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12">\n' +
    '                        <div class="form-group">\n' +
    '                            <label class="active">Active</label>\n' +
    '                            <input type="checkbox" ng-focus="vm.edit_job_title_form.status.$focused = true" ng-model="vm.job_title.status" name="status" ng-true-value="1" ng-false-value="0">\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="form-group">\n' +
    '                    <input ng-disabled="inProgress" type="submit" class="btn ui-btn btn-full-width orange-btn-sm" value="Edit">\n' +
    '                </div>\n' +
    '            </form>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/job_titles/list/_list.tpl.html',
    '<div class="list-page list-job_titles">\n' +
    '    <div class="row filter">\n' +
    '        <div class="col-sm-2 col-xs-12 filter-status">\n' +
    '            <label class="ui-label" for="status">Status</label>\n' +
    '            <select class="ui-select" ng-model="vm.status" ng-change="vm.filterStatus(vm.status)" ng-options="options as options.name for options in vm.statuses"></select>\n' +
    '        </div>\n' +
    '        <div class="col-sm-3 col-sm-offset-7 text-right item-action new-record-wrapper">\n' +
    '            <ul>\n' +
    '                <li class="active">\n' +
    '                    <a ng-click="vm.changeStatusAllItem(\'active\')" title="Active">\n' +
    '                        <i class="icon fa fa-check-circle"></i>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '                <li class="pending">\n' +
    '                    <a ng-click="vm.changeStatusAllItem(\'pending\')" title="Pending">\n' +
    '                        <i class="icon fa fa-pause-circle"></i>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '                <li class="new-item">\n' +
    '                    <a ui-sref="app.job_titles.create" title="Create new Job Title">\n' +
    '                        <i class="icon fa fa-plus-circle"></i>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '            </ul>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row search-box">\n' +
    '        <div class="col-xs-12 col-sm-6 select-all">\n' +
    '            <input type="checkbox" id="select-all" ng-model="vm.selectedAll" ng-change="vm.selectAll()">\n' +
    '            <label for="select-all">Select all</label>\n' +
    '            <span class="number-select" ng-show="vm.selectedItem.length > 0"> ( All {{vm.selectedItem.length}} {{vm.selectedItem.length === 1 ? \'item\' : \'items\'}}  on this page are selected.)</span>\n' +
    '        </div>\n' +
    '        <div class="search col-xs-12 col-sm-6">\n' +
    '            <div class="search-wrapper card">\n' +
    '                <input id="search" class="input-search form-control" ng-model="vm.params.search" placeholder="Search..." ng-keydown="vm.searchKeyEvent($event, vm.params.search)"><i class="icon-search fa fa-search" ng-click="vm.searchPage(vm.params.search)"></i>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- <div class="row">\n' +
    '        <div class="col-xs-12 col-sm-6">\n' +
    '            Sort By:\n' +
    '            <sortable-attributes></sortable-attributes>\n' +
    '        </div>\n' +
    '    </div> -->\n' +
    '    <preloader ng-if="vm.job_titles | isUndefined"></preloader>\n' +
    '    <div class="row">\n' +
    '        <div class="col-xs-12">\n' +
    '            <table class="table table-striped ui-table table-items table-job-titles" ng-if="vm.job_titles !== undefined">\n' +
    '                <thead>\n' +
    '                    <tr>\n' +
    '                        <th></th>\n' +
    '                        <th>ID <!-- <span sortable="id"></span> --></th>\n' +
    '                        <th>Name <!-- <span sortable="name"></span> --></th>\n' +
    '                        <th>Job Title <!-- <span sortable="job_title"></span> --></th>\n' +
    '                        <th>Order <!-- <span sortable="order"></span> --></th>\n' +
    '                        <th>Active <!-- <span sortable="status"></span> --></th>\n' +
    '                        <th>Status</th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody>\n' +
    '                    <tr ng-repeat="job_title in vm.job_titles">\n' +
    '                        <td>\n' +
    '                            <input type="checkbox" ng-model="job_title.selected" ng-change="vm.updateSelectedItem()">\n' +
    '                        </td>\n' +
    '                        <td ng-bind="job_title.getId()"></td>\n' +
    '                        <td ng-bind="job_title.getName()"></td>\n' +
    '                        <td ng-bind="job_title.getJobTitle()"></td>\n' +
    '                        <td ng-bind="job_title.getOrder()"></td>\n' +
    '                        <td>\n' +
    '                            {{ job_title.getStatus() === 1 ? \'Active\' : \'Pending\' }}\n' +
    '                        </td>\n' +
    '                        <td class="text-right action">\n' +
    '                            <a ui-sref="app.job_titles.edit({id: job_title.getId()})" class="edit">\n' +
    '                                <p class="ui-p"><i class="icon fa fa-pencil-square-o"></i></p>\n' +
    '                            </a>\n' +
    '                            <a ng-click="vm.deleteItem(job_title)" class="delete">\n' +
    '                                <p class="ui-p"><i class="icon fa fa-trash"></i></p>\n' +
    '                            </a>\n' +
    '                        </td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '            <pagination data="vm.pagination"></pagination>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/jobs/create/_create.tpl.html',
    '<div class="post-job">\n' +
    '    <div class="container-fluid">\n' +
    '        <div class="div-right">\n' +
    '            <section id="main-content">\n' +
    '                <section id="post-my-job" class="animated fadeIn">\n' +
    '                    <div class="form-container">\n' +
    '                        <form ng-submit="isValidFormOrScrollToError(vm.create_job_form) && vm.create()" name="vm.create_job_form" novalidate>\n' +
    '                            <h3>Job Info</h3>\n' +
    '                            <div class="row">\n' +
    '                                <div class="col-xs-12 col-sm-6">\n' +
    '                                    <div class="form-group">\n' +
    '                                        <label>Employer</label>\n' +
    '                                        <ui-select ng-model="vm.store.getState().PostJob.employer" post-job-ui-select on-select="vm.onSelectEmployer($item, $model)">\n' +
    '                                            <ui-select-match placeholder="Select company">\n' +
    '                                                <span>{{vm.store.getState().PostJob.employer.getName()}} - {{vm.store.getState().PostJob.employer.getEmail()}}</span>\n' +
    '                                            </ui-select-match>\n' +
    '                                            <ui-select-choices repeat="employer in vm.employers | postJobEmployerSelectionFilter:$select.search">\n' +
    '                                                <div class="item ">\n' +
    '                                                    <p>{{employer.getName()}} - {{employer.getEmail()}}</p>\n' +
    '                                                </div>\n' +
    '                                            </ui-select-choices>\n' +
    '                                        </ui-select>\n' +
    '                                        <div class="errors">\n' +
    '                                            <span ng-show="vm.create_job_form.$submitted && vm.create_job_form.employer.$error.required">Required</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="col-md-4 col-sm-6">\n' +
    '                                    <div class="form-group">\n' +
    '                                        <label>Job Title</label>\n' +
    '                                        <ui-select ng-model="vm.store.getState().PostJob.job_title" post-job-ui-select on-select="vm.onSelectJobTitle($item, $model)">\n' +
    '                                            <ui-select-match placeholder="Select job title">\n' +
    '                                                <span ng-bind="vm.store.getState().PostJob.job_title.name"></span>\n' +
    '                                            </ui-select-match>\n' +
    '                                            <ui-select-choices repeat="job_title in vm.store.getState().JobTitle.items">\n' +
    '                                                <div class="item ">\n' +
    '                                                    <p ng-bind="job_title.getName()"></p>\n' +
    '                                                </div>\n' +
    '                                            </ui-select-choices>\n' +
    '                                        </ui-select>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="col-xs-12 col-sm-6">\n' +
    '                                    <div class="form-group">\n' +
    '                                        <label>Title</label>\n' +
    '                                        <input type="text" name="title" class="form-control" ng-model="vm.store.getState().PostJob.title" ng-model-options="{debounce: 500}" ng-change="vm.store.dispatch({type: \'UPDATE_POST_JOB\', field: \'title\', data: vm.store.getState().PostJob.title})" required>\n' +
    '                                        <div class="errors">\n' +
    '                                            <span ng-show="vm.create_job_form.$submitted && vm.create_job_form.title.$error.required">Required</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="col-xs-12 col-sm-6">\n' +
    '                                    <div class="form-group">\n' +
    '                                        <label>Short Description</label>\n' +
    '                                        <textarea name="short_desc" ng-model="vm.store.getState().PostJob.short_desc" ng-model-options="{debounce: 500}" ng-change="vm.store.dispatch({type: \'UPDATE_POST_JOB\', field: \'short_desc\', data: vm.store.getState().PostJob.short_desc})" class="form-control" placeholder="..." rows="4" required></textarea>\n' +
    '                                        <div class="errors">\n' +
    '                                            <span ng-show="vm.create_job_form.$submitted && vm.create_job_form.short_desc.$error.required">Required</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="col-xs-12 col-sm-6">\n' +
    '                                    <div class="form-group">\n' +
    '                                        <label>Description</label>\n' +
    '                                        <textarea name="description" ng-model="vm.store.getState().PostJob.description" ng-model-options="{debounce: 500}" ng-change="vm.store.dispatch({type: \'UPDATE_POST_JOB\', field: \'description\', data: vm.store.getState().PostJob.description})" class="form-control" placeholder="..." rows="4" required></textarea>\n' +
    '                                        <div class="errors">\n' +
    '                                            <span ng-show="vm.create_job_form.$submitted && vm.create_job_form.description.$error.required">Required</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="col-xs-12 col-sm-6">\n' +
    '                                    <div class="form-group">\n' +
    '                                        <label>Desired Candidate Profile</label>\n' +
    '                                        <textarea name="desired_candidate_profile" ng-model="vm.store.getState().PostJob.desired_candidate_profile" ng-model-options="{debounce: 500}" ng-change="vm.store.dispatch({type: \'UPDATE_POST_JOB\', field: \'desired_candidate_profile\', data: vm.store.getState().PostJob.desired_candidate_profile})" class="form-control" placeholder="..." rows="4" required></textarea>\n' +
    '                                        <div class="errors">\n' +
    '                                            <span ng-show="vm.create_job_form.$submitted && vm.create_job_form.desired_candidate_profile.$error.required">Required</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row industry-wrapper">\n' +
    '                                <div class="col-sm-10">\n' +
    '                                    <div class="form-group">\n' +
    '                                        <label>Industries</label>\n' +
    '                                        <ui-select multiple ng-model="vm.store.getState().PostJob.industries" post-job-ui-select on-select="vm.onSelectIndustry(vm.store.getState().PostJob.industries, $model)" on-remove="vm.onRemoveIndustry(vm.store.getState().PostJob.industries, $model)">\n' +
    '                                            <ui-select-match placeholder="Select your industry">\n' +
    '                                                <span>{{ $item.name }}</span>\n' +
    '                                            </ui-select-match>\n' +
    '                                            <ui-select-choices repeat="industry in vm.store.getState().Industry.items">\n' +
    '                                                <div class="item">\n' +
    '                                                    <p ng-bind="industry.getName()"></p>\n' +
    '                                                </div>\n' +
    '                                            </ui-select-choices>\n' +
    '                                        </ui-select>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="col-sm-10">\n' +
    '                                    <div class="form-group">\n' +
    '                                        <label>Tags</label>\n' +
    '                                        <ui-select multiple ng-model="vm.store.getState().PostJob.tags" post-job-ui-select on-select="vm.onSelectTag(vm.store.getState().PostJob.tags, $model)" on-remove="vm.onRemoveTag(vm.store.getState().PostJob.tags, $model)">\n' +
    '                                            <ui-select-match placeholder="Select your tag">\n' +
    '                                                <span>{{ $item.name }}</span>\n' +
    '                                            </ui-select-match>\n' +
    '                                            <ui-select-choices repeat="tag in vm.store.getState().Tag.items">\n' +
    '                                                <div class="item">\n' +
    '                                                    <p ng-bind="tag.getName()"></p>\n' +
    '                                                </div>\n' +
    '                                            </ui-select-choices>\n' +
    '                                        </ui-select>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="col-md-4 col-sm-6">\n' +
    '                                    <div class="form-group">\n' +
    '                                        <label>Experience</label>\n' +
    '                                        <ui-select ng-model="vm.store.getState().PostJob.experience" post-job-ui-select on-select="vm.onSelectExperience($item, $model)">\n' +
    '                                            <ui-select-match placeholder="Select your experience">\n' +
    '                                                <span ng-bind="vm.store.getState().PostJob.experience.label"></span>\n' +
    '                                            </ui-select-match>\n' +
    '                                            <ui-select-choices repeat="experience in vm.store.getState().Experience.items">\n' +
    '                                                <div class="item">\n' +
    '                                                    <p ng-bind="experience.getLabel()"></p>\n' +
    '                                                </div>\n' +
    '                                            </ui-select-choices>\n' +
    '                                        </ui-select>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="col-md-4 col-sm-6">\n' +
    '                                    <div class="form-group">\n' +
    '                                        <label>Qualification</label>\n' +
    '                                        <ui-select ng-model="vm.store.getState().PostJob.qualification" post-job-ui-select on-select="vm.onSelectQualification($item, $model)">\n' +
    '                                            <ui-select-match placeholder="Select your qualification">\n' +
    '                                                <span ng-bind="vm.store.getState().PostJob.qualification.name"></span>\n' +
    '                                            </ui-select-match>\n' +
    '                                            <ui-select-choices repeat="qualification in vm.store.getState().Qualification.items">\n' +
    '                                                <div class="item">\n' +
    '                                                    <p ng-bind="qualification.getName()"></p>\n' +
    '                                                </div>\n' +
    '                                            </ui-select-choices>\n' +
    '                                        </ui-select>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="col-sm-10">\n' +
    '                                    <div class="form-group">\n' +
    '                                        <label>Nationality</label>\n' +
    '                                        <ui-select ng-model="vm.store.getState().PostJob.nationality" post-job-ui-select on-select="vm.onSelectNationality($item, $model)">\n' +
    '                                            <ui-select-match placeholder="Select your nationality">\n' +
    '                                                <span ng-bind="vm.store.getState().PostJob.nationality.name"></span>\n' +
    '                                            </ui-select-match>\n' +
    '                                            <ui-select-choices repeat="nationality in vm.store.getState().Nationality.items">\n' +
    '                                                <div class="item">\n' +
    '                                                    <p ng-bind="nationality.getName()"></p>\n' +
    '                                                </div>\n' +
    '                                            </ui-select-choices>\n' +
    '                                        </ui-select>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <h3>Job Type</h3>\n' +
    '                            <div class="checkbox-wrapper row">\n' +
    '                                <div class="col-sm-2">\n' +
    '                                    <div class="form-group check">\n' +
    '                                        <label>\n' +
    '                                            <input name="job_type" type="radio" name="language-required" ng-model="vm.store.getState().PostJob.type" ng-change="vm.store.dispatch({type: \'UPDATE_POST_JOB\', field: \'type\', data: vm.store.getState().PostJob.type})" ng-value="1">\n' +
    '                                            <span>Normal Job</span>\n' +
    '                                        </label>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="col-sm-3">\n' +
    '                                    <div class="form-group check">\n' +
    '                                        <label>\n' +
    '                                            <input name="job_type" type="radio" name="language-required" ng-model="vm.store.getState().PostJob.type" ng-change="vm.store.dispatch({type: \'UPDATE_POST_JOB\', field: \'type\', data: vm.store.getState().PostJob.type})" ng-value="2">\n' +
    '                                            <span>International Job</span>\n' +
    '                                        </label>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div ng-if="vm.store.getState().PostJob.type == 1">\n' +
    '                                <div class="row country-wrapper">\n' +
    '                                    <div class="col-md-4 col-sm-6">\n' +
    '                                        <div class="form-group">\n' +
    '                                            <label>Country</label>\n' +
    '                                            <ui-select ng-model="vm.store.getState().PostJob.country" post-job-ui-select on-select="vm.onSelectCountry($item, $model)">\n' +
    '                                                <ui-select-match placeholder="Select your country">\n' +
    '                                                    <span ng-bind="vm.store.getState().PostJob.country.name"></span>\n' +
    '                                                </ui-select-match>\n' +
    '                                                <ui-select-choices repeat="country in vm.store.getState().Country.items">\n' +
    '                                                    <div class="item">\n' +
    '                                                        <p ng-bind="country.getName()"></p>\n' +
    '                                                    </div>\n' +
    '                                                </ui-select-choices>\n' +
    '                                            </ui-select>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="row state-wrapper">\n' +
    '                                    <div class="col-md-4 col-sm-6">\n' +
    '                                        <div class="form-group">\n' +
    '                                            <label>State</label>\n' +
    '                                            <ui-select ng-model="vm.store.getState().PostJob.state" post-job-ui-select on-select="vm.onSelectState($item, $model)">\n' +
    '                                                <ui-select-match placeholder="Select your state">\n' +
    '                                                    <span ng-bind="vm.store.getState().PostJob.state.name"></span>\n' +
    '                                                </ui-select-match>\n' +
    '                                                <ui-select-choices repeat="state in vm.store.getState().State.items">\n' +
    '                                                    <div class="item">\n' +
    '                                                        <p ng-bind="state.getName()"></p>\n' +
    '                                                    </div>\n' +
    '                                                </ui-select-choices>\n' +
    '                                            </ui-select>\n' +
    '                                            <div class="no-state">\n' +
    '                                                <span ng-show="vm.store.getState().PostJob.country !== undefined && vm.store.getState().State.items.length === 0">{{ vm.store.getState().PostJob.country.name }} doesn\'t have any state</span>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="row city-wrapper">\n' +
    '                                    <div class="col-md-4 col-sm-6">\n' +
    '                                        <div class="form-group">\n' +
    '                                            <label>City</label>\n' +
    '                                            <ui-select ng-model="vm.store.getState().PostJob.city" post-job-ui-select on-select="vm.onSelectCity($item, $model)">\n' +
    '                                                <ui-select-match placeholder="Select your city">\n' +
    '                                                    <span ng-bind="vm.store.getState().PostJob.city.name"></span>\n' +
    '                                                </ui-select-match>\n' +
    '                                                <ui-select-choices repeat="city in vm.store.getState().City.items">\n' +
    '                                                    <div class="item">\n' +
    '                                                        <p ng-bind="city.getName()"></p>\n' +
    '                                                    </div>\n' +
    '                                                </ui-select-choices>\n' +
    '                                            </ui-select>\n' +
    '                                            <div class="no-city">\n' +
    '                                                <span ng-show="vm.store.getState().PostJob.country !== undefined && vm.store.getState().City.items.length === 0">{{ vm.store.getState().PostJob.state.name }} doesn\'t have any city</span>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div ng-if="vm.store.getState().PostJob.type == 2">\n' +
    '                                <div class="row">\n' +
    '                                    <div class="col-xs-12 col-sm-6">\n' +
    '                                        <div class="form-group location">\n' +
    '                                            <label>Location</label>\n' +
    '                                            <input autocomplete-address="vm.store.getState().PostJob.location" type="text" class="form-control" ng-model-options="{ debounce: 500 }" name="location" ng-model="vm.store.getState().PostJob.location" ng-change="vm.store.dispatch({type: \'UPDATE_JOB_ADDRESS\', data: vm.store.getState().PostJob.location})" required placeholder="Enter a location">\n' +
    '                                            <div class="errors">\n' +
    '                                                <span ng-show="vm.create_job_form.$submitted && vm.create_job_form.location.$error.required">Required</span>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="col-xs-12 col-sm-6">\n' +
    '                                    <div class="form-group location">\n' +
    '                                        <label>Address</label>\n' +
    '                                        <input autocomplete-address="vm.store.getState().PostJob.address" type="text" class="form-control" ng-model-options="{ debounce: 500 }" name="address" ng-model="vm.store.getState().PostJob.address" ng-change="vm.store.dispatch({type: \'UPDATE_JOB_ADDRESS\', data: vm.store.getState().PostJob.address})" placeholder="Enter a address" required>\n' +
    '                                        <div class="errors">\n' +
    '                                            <span ng-show="vm.create_job_form.$submitted && vm.create_job_form.address.$error.required">Required</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="col-xs-12 col-sm-6">\n' +
    '                                    <div class="form-group location">\n' +
    '                                        <label>Certificate</label>\n' +
    '                                        <input type="text" name="certificate" class="form-control" ng-model="vm.store.getState().PostJob.certificate" ng-model-options="{debounce: 500}" ng-change="vm.store.dispatch({type: \'UPDATE_POST_JOB\', field: \'certificate\', data: vm.store.getState().PostJob.certificate})">\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="col-xs-12 col-sm-3">\n' +
    '                                    <div class="form-group location">\n' +
    '                                        <label>Min Salary</label>\n' +
    '                                        <input type="text" name="min_salary" class="form-control" placeholder="Min Salary" ng-model="vm.store.getState().PostJob.min_salary" ng-model-options="{debounce: 500}" ng-change="vm.store.dispatch({type: \'UPDATE_POST_JOB\', field: \'min_salary\', data: vm.store.getState().PostJob.min_salary})" ng-pattern="/^\\d{1,8}$|^\\d{1,8}\\.\\d{1,2}$/" required>\n' +
    '                                        <div class="errors">\n' +
    '                                            <span ng-show="vm.create_job_form.$submitted && vm.create_job_form.min_salary.$error.required">Required</span>\n' +
    '                                            <span ng-show="vm.create_job_form.$submitted && vm.create_job_form.min_salary.$error.pattern">Please enter a valid number</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="col-xs-12 col-sm-3">\n' +
    '                                    <div class="form-group location">\n' +
    '                                        <label>Max Salary</label>\n' +
    '                                        <input type="text" name="max_salary" class="form-control" placeholder="Min Salary" ng-model="vm.store.getState().PostJob.max_salary" ng-model-options="{debounce: 500}" ng-change="vm.store.dispatch({type: \'UPDATE_POST_JOB\', field: \'max_salary\', data: vm.store.getState().PostJob.max_salary})" ng-pattern="/^\\d{1,8}$|^\\d{1,8}\\.\\d{1,2}$/" required>\n' +
    '                                        <div class="errors">\n' +
    '                                            <span ng-show="vm.create_job_form.$submitted && vm.create_job_form.max_salary.$error.required">Required</span>\n' +
    '                                            <span ng-show="vm.create_job_form.$submitted && vm.create_job_form.max_salary.$error.pattern">Please enter a valid number</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <h3>Job Requirements</h3>\n' +
    '                            <div class="checkbox-wrapper">\n' +
    '                                <!--  <div class="form-group check">\n' +
    '                                    <label>\n' +
    '                                        <input type="checkbox" name="language-required" ng-model="vm.store.getState().PostJob.language_required" ng-change="vm.store.dispatch({type: \'UPDATE_POST_JOB\', field: \'language_required\', data: vm.store.getState().PostJob.language_required})">\n' +
    '                                        <span>Additional Language</span>\n' +
    '                                    </label>\n' +
    '                                    <div class="row" ng-if="vm.store.getState().PostJob.language_required">\n' +
    '                                        <div class="col-xs-12 col-sm-6">\n' +
    '                                            <ui-select post-job-ui-select ng-model="vm.store.getState().PostJob.language" on-select="vm.onSelectLanguage($item, $model)">\n' +
    '                                                <ui-select-match>\n' +
    '                                                    <span ng-bind="vm.store.getState().PostJob.language.name"></span>\n' +
    '                                                </ui-select-match>\n' +
    '                                                <ui-select-choices repeat="language in vm.store.getState().PostJob.languages">\n' +
    '                                                    <div class="item ">\n' +
    '                                                        <p ng-bind="language.name"></p>\n' +
    '                                                    </div>\n' +
    '                                                </ui-select-choices>\n' +
    '                                            </ui-select>\n' +
    '                                            <div class="errors">\n' +
    '                                                <span ng-show="vm.create_job_form.$submitted && !vm.store.getState().PostJob.language">Please select a language.</span>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div> -->\n' +
    '                                <div class="form-group check">\n' +
    '                                    <label>\n' +
    '                                        <input type="checkbox" ng-model="vm.store.getState().PostJob.gender_required" ng-change="vm.store.dispatch({type: \'UPDATE_POST_JOB\', field: \'gender_required\', data: vm.store.getState().PostJob.gender_required})">\n' +
    '                                        <span>Gender</span>\n' +
    '                                    </label>\n' +
    '                                    <div class="row" ng-if="vm.store.getState().PostJob.gender_required">\n' +
    '                                        <div class="col-xs-12 col-sm-6">\n' +
    '                                            <ui-select post-job-ui-select ng-model="vm.store.getState().PostJob.gender" on-select="vm.onSelectGender($item, $model)">\n' +
    '                                                <ui-select-match>\n' +
    '                                                    <span ng-bind="vm.store.getState().PostJob.gender.name"></span>\n' +
    '                                                </ui-select-match>\n' +
    '                                                <ui-select-choices repeat="gender in vm.candidate_genders">\n' +
    '                                                    <div class="item ">\n' +
    '                                                        <p ng-bind="gender.name"></p>\n' +
    '                                                    </div>\n' +
    '                                                </ui-select-choices>\n' +
    '                                            </ui-select>\n' +
    '                                            <div class="errors">\n' +
    '                                                <span ng-show="vm.create_job_form.$submitted && !vm.store.getState().PostJob.gender">Please select a gender.</span>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <!-- <div class="form-group check">\n' +
    '                                    <label>\n' +
    '                                        <input type="checkbox" ng-model="vm.store.getState().PostJob.driving_license_required" ng-change="vm.store.dispatch({type: \'UPDATE_POST_JOB\', field: \'driving_license_required\', data: vm.store.getState().PostJob.driving_license_required})">\n' +
    '                                        <span>Driving License</span>\n' +
    '                                    </label>\n' +
    '                                    <div class="row" ng-if="vm.store.getState().PostJob.driving_license_required">\n' +
    '                                        <div class="col-xs-12 col-sm-6">\n' +
    '                                            <ui-select post-job-ui-select ng-model="vm.store.getState().PostJob.driving_license" on-select="vm.onSelectDrivingLicense($item, $model)">\n' +
    '                                                <ui-select-match>\n' +
    '                                                    <span ng-bind="vm.store.getState().PostJob.driving_license.label"></span>\n' +
    '                                                </ui-select-match>\n' +
    '                                                <ui-select-choices repeat="license in vm.driving_licenses">\n' +
    '                                                    <div class="item ">\n' +
    '                                                        <p ng-bind="license.label"></p>\n' +
    '                                                    </div>\n' +
    '                                                </ui-select-choices>\n' +
    '                                            </ui-select>\n' +
    '                                            <div class="errors">\n' +
    '                                                <span ng-show="vm.create_job_form.$submitted && !vm.store.getState().PostJob.driving_license">Please select a driving license.</span>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div> -->\n' +
    '                            </div>\n' +
    '                            <div class="row ">\n' +
    '                                <div class="col-sm-6 control-btns">\n' +
    '                                    <!-- <a href="#" class="btn btn-primary btn-grey">Delete Job Offer</a> -->\n' +
    '                                    <a ng-click="vm.saveAsDraft()" class="btn btn-primary btn-grey">Save as Draft</a>\n' +
    '                                    <button type="submit" class="btn btn-primary btn-orange">Post Job Offer</a>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </form>\n' +
    '                    </div>\n' +
    '                </section>\n' +
    '            </section>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/jobs/list/_list.tpl.html',
    '<div class="list-page list-jobs">\n' +
    '    <div class="job-filter">\n' +
    '        <div class="row filter">\n' +
    '            <div class="col-sm-2 col-xs-12 filter-status">\n' +
    '                <label class="ui-label" for="status">Status</label>\n' +
    '                <select class="ui-select" ng-model="vm.status" ng-change="vm.changeStatus(vm.status)" ng-options="options as options.name for options in vm.statuses"></select>\n' +
    '            </div>\n' +
    '            <div class="col-sm-3 col-sm-offset-7 text-right item-action new-record-wrapper">\n' +
    '                <ul>\n' +
    '                    <li class="active">\n' +
    '                        <a ng-click="vm.changeStatusAllItem(\'active\')" title="Active">\n' +
    '                        <i class="icon fa fa-check-circle"></i>\n' +
    '                    </a>\n' +
    '                    </li>\n' +
    '                    <li class="pending">\n' +
    '                        <a ng-click="vm.changeStatusAllItem(\'pending\')" title="Pending">\n' +
    '                        <i class="icon fa fa-pause-circle"></i>\n' +
    '                    </a>\n' +
    '                    </li>\n' +
    '                    <li class="new-item">\n' +
    '                        <a ui-sref="app.jobs.create" title="Create new Job">\n' +
    '                        <i class="icon fa fa-plus-circle"></i>\n' +
    '                    </a>\n' +
    '                    </li>\n' +
    '                </ul>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="jobs">\n' +
    '        <div class="row search-box">\n' +
    '            <div class="col-sm-6 col-xs-12 select-all">\n' +
    '                <input type="checkbox" id="select-all" ng-model="vm.selectedAll" ng-click="vm.selectAll()">\n' +
    '                <label for="select-all">Select all</label>\n' +
    '                <span class="number-select" ng-show="vm.selectedItem.length > 0"> ( All {{vm.selectedItem.length}} {{vm.selectedItem.length === 1 ? \'item\' : \'items\'}}  on this page are selected.)</span>\n' +
    '            </div>\n' +
    '            <div class="search col-sm-6 col-xs-12">\n' +
    '                <div class="search-wrapper card">\n' +
    '                    <input id="search" class="input-search form-control" ng-model="vm.params.search" placeholder="Search..." ng-keydown="vm.searchKeyEvent($event, vm.params.search)"><i class="icon-search fa fa-search" ng-click="vm.searchPage(vm.params.search)"></i>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '            <!-- <div class="row">\n' +
    '                <div class="col-sm-6 col-xs-12">\n' +
    '                    Sort By:\n' +
    '                    <sortable-attributes></sortable-attributes>\n' +
    '                </div>\n' +
    '            </div> -->\n' +
    '        </div>\n' +
    '        <preloader ng-if="vm.jobs | isUndefined"></preloader>\n' +
    '        <div class="row">\n' +
    '            <div class="col-xs-12">\n' +
    '                <table class="table table-striped ui-table table-items table-jobs" ng-if="vm.jobs !== undefined">\n' +
    '                    <thead>\n' +
    '                        <tr>\n' +
    '                            <th></th>\n' +
    '                            <th class="clickable id-col">ID <!-- <span sortable="id"></span> --> </th>\n' +
    '                            <th class="image-col">Image</th>\n' +
    '                            <th class="clickable">Title <!-- <span sortable="title"></span> --> </th>\n' +
    '                            <th>Company</th>\n' +
    '                            <th>Type <!-- <span sortable="type"></span> --> </th>\n' +
    '                            <th class="phone-col">Mobile</th>\n' +
    '                            <th class="clickable">Address <!-- <span sortable="address"></span> --> </th>\n' +
    '                            <th class="status-col">Active <!-- <span sortable="active"></span> --> </th>\n' +
    '                            <th class="actions-col text-right">Status</th>\n' +
    '                        </tr>\n' +
    '                    </thead>\n' +
    '                    <tbody>\n' +
    '                        <tr ng-repeat="item in vm.jobs">\n' +
    '                            <td>\n' +
    '                                <input type="checkbox" ng-model="item.selected" ng-change="vm.updateSelectedItem()">\n' +
    '                            </td>\n' +
    '                            <td ng-bind="item.getId()"></td>\n' +
    '                            <td class="image-col">\n' +
    '                                <div class="text-center">\n' +
    '                                    <user-profile-image image="item.employer.getImage()"></user-profile-image>\n' +
    '                                </div>\n' +
    '                            </td>\n' +
    '                            <td class="title-col">\n' +
    '                                <span>{{item.getTitle()}}</span>\n' +
    '                                <!-- <a class="hint" expand-data data-id="item.getId()" api="job" template="/webapp/js/admin/components/jobs/list/_list_expand.tpl.html">\n' +
    '                                <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="Show expand data"></i>\n' +
    '                            </a> -->\n' +
    '                            </td>\n' +
    '                            <td>\n' +
    '                                <p ng-bind="item.employer.getName()"></p>\n' +
    '                                <a ui-sref="app.companies.edit({id: item.employer.getId()})" ng-bind="item.employer.getEmail()"></a>\n' +
    '                            </td>\n' +
    '                            <td>\n' +
    '                                <p ng-bind="item.getJobType()"></p>\n' +
    '                            </td>\n' +
    '                            <td class="phone-col">\n' +
    '                                <a>{{item.employer.getFormattedPhoneNumber()}}</a>\n' +
    '                            </td>\n' +
    '                            <td ng-bind="item.getAddress()"></td>\n' +
    '                            <td>\n' +
    '                                {{ item.getActive() ? \'Active\' : \'Pending\' }}\n' +
    '                            </td>\n' +
    '                            <td class="text-right action">\n' +
    '                                <a ui-sref="app.jobs.update({slug: item.getSlug()})" class="edit">\n' +
    '                                    <p class="ui-p"><i class="icon fa fa-pencil-square-o"></i></p>\n' +
    '                                </a>\n' +
    '                                <a ng-click="vm.deleteItem(item)" class="delete">\n' +
    '                                    <p class="ui-p"><i class="icon fa fa-trash"></i></p>\n' +
    '                                </a>\n' +
    '                            </td>\n' +
    '                        </tr>\n' +
    '                    </tbody>\n' +
    '                </table>\n' +
    '                <pagination data="vm.pagination"></pagination>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/jobs/list/_list_expand.tpl.html',
    '<div class="job-expand">\n' +
    '    <div class="row">\n' +
    '        <div class="col s12">\n' +
    '            <h2 ng-bind="data.getTitle()"></h2>\n' +
    '            <p class="rate">{{data.getHourlyRate()}}$/hr</p>\n' +
    '            <div class="job-description" ng-if="data.getDescription() != \'\'">\n' +
    '                <h3 class="left-dash">Job Description</h3>\n' +
    '                <p ng-bind-html="data.getDescription()"></p>\n' +
    '            </div>\n' +
    '            <div class="job-requirements" ng-if="data.getRequirement() != \'\'">\n' +
    '                <h3 class="left-dash">Job Requirements</h3>\n' +
    '                <p ng-bind-html="data.getRequirement()"></p>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/jobs/update/_update.tpl.html',
    '<div class="update-job">\n' +
    '    <div class="container-fluid">\n' +
    '        <div class="div-right">\n' +
    '            <section id="main-content">\n' +
    '                <section id="post-my-job" class="animated fadeIn">\n' +
    '                    <div class="form-container">\n' +
    '                        <form ng-submit="isValidFormOrScrollToError(vm.update_job_form) && vm.update()" name="vm.update_job_form" novalidate>\n' +
    '                            <h3>Job Info</h3>\n' +
    '                            <div class="row">\n' +
    '                                <div class="col-xs-12 col-sm-6">\n' +
    '                                    <div class="form-group">\n' +
    '                                        <label>Employer</label>\n' +
    '                                        <ui-select ng-model="vm.store.getState().UpdateJob.employer" post-job-ui-select on-select="vm.onSelectEmployer($item, $model)">\n' +
    '                                            <ui-select-match placeholder="Select company">\n' +
    '                                                <span>{{vm.store.getState().UpdateJob.employer.getName()}} - {{vm.store.getState().UpdateJob.employer.getEmail()}}</span>\n' +
    '                                            </ui-select-match>\n' +
    '                                            <ui-select-choices repeat="employer in vm.employers | postJobEmployerSelectionFilter:$select.search">\n' +
    '                                                <div class="item ">\n' +
    '                                                    <p>{{employer.getName()}} - {{employer.getEmail()}}</p>\n' +
    '                                                </div>\n' +
    '                                            </ui-select-choices>\n' +
    '                                        </ui-select>\n' +
    '                                        <div class="errors">\n' +
    '                                            <span ng-show="vm.update_job_form.$submitted && vm.update_job_form.employer.$error.required">Required</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="col-md-4 col-sm-6">\n' +
    '                                    <div class="form-group">\n' +
    '                                        <label>Job Title</label>\n' +
    '                                        <ui-select ng-model="vm.store.getState().UpdateJob.job_title" post-job-ui-select on-select="vm.onSelectJobTitle($item, $model)">\n' +
    '                                            <ui-select-match placeholder="Select job title">\n' +
    '                                                <span ng-bind="vm.store.getState().UpdateJob.job_title.name"></span>\n' +
    '                                            </ui-select-match>\n' +
    '                                            <ui-select-choices repeat="job_title in vm.store.getState().JobTitle.items">\n' +
    '                                                <div class="item ">\n' +
    '                                                    <p ng-bind="job_title.getName()"></p>\n' +
    '                                                </div>\n' +
    '                                            </ui-select-choices>\n' +
    '                                        </ui-select>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="col-xs-12 col-sm-6">\n' +
    '                                    <div class="form-group">\n' +
    '                                        <label>Title</label>\n' +
    '                                        <input type="text" name="title" class="form-control" ng-model="vm.store.getState().UpdateJob.title" ng-model-options="{debounce: 500}" ng-change="vm.store.dispatch({type: \'UPDATE_POST_JOB\', field: \'title\', data: vm.store.getState().UpdateJob.title})" required>\n' +
    '                                        <div class="errors">\n' +
    '                                            <span ng-show="vm.update_job_form.$submitted && vm.update_job_form.title.$error.required">Required</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="col-xs-12 col-sm-6">\n' +
    '                                    <div class="form-group">\n' +
    '                                        <label>Short Description</label>\n' +
    '                                        <textarea name="short_desc" ng-model="vm.store.getState().UpdateJob.short_desc" ng-model-options="{debounce: 500}" ng-change="vm.store.dispatch({type: \'UPDATE_POST_JOB\', field: \'short_desc\', data: vm.store.getState().UpdateJob.short_desc})" class="form-control" placeholder="..." rows="4" required></textarea>\n' +
    '                                        <div class="errors">\n' +
    '                                            <span ng-show="vm.update_job_form.$submitted && vm.update_job_form.short_desc.$error.required">Required</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="col-xs-12 col-sm-6">\n' +
    '                                    <div class="form-group">\n' +
    '                                        <label>Description</label>\n' +
    '                                        <textarea name="description" ng-model="vm.store.getState().UpdateJob.description" ng-model-options="{debounce: 500}" ng-change="vm.store.dispatch({type: \'UPDATE_POST_JOB\', field: \'description\', data: vm.store.getState().UpdateJob.description})" class="form-control" placeholder="..." rows="4" required></textarea>\n' +
    '                                        <div class="errors">\n' +
    '                                            <span ng-show="vm.update_job_form.$submitted && vm.update_job_form.description.$error.required">Required</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="col-xs-12 col-sm-6">\n' +
    '                                    <div class="form-group">\n' +
    '                                        <label>Desired Candidate Profile</label>\n' +
    '                                        <textarea name="desired_candidate_profile" ng-model="vm.store.getState().UpdateJob.desired_candidate_profile" ng-model-options="{debounce: 500}" ng-change="vm.store.dispatch({type: \'UPDATE_POST_JOB\', field: \'desired_candidate_profile\', data: vm.store.getState().UpdateJob.desired_candidate_profile})" class="form-control" placeholder="..." rows="4" required></textarea>\n' +
    '                                        <div class="errors">\n' +
    '                                            <span ng-show="vm.update_job_form.$submitted && vm.update_job_form.desired_candidate_profile.$error.required">Required</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row industry-wrapper">\n' +
    '                                <div class="col-sm-10">\n' +
    '                                    <div class="form-group">\n' +
    '                                        <label>Industries</label>\n' +
    '                                        <ui-select multiple ng-model="vm.store.getState().UpdateJob.industries" post-job-ui-select on-select="vm.onSelectIndustry(vm.store.getState().UpdateJob.industries, $model)" on-remove="vm.onRemoveIndustry(vm.store.getState().UpdateJob.industries, $model)">\n' +
    '                                            <ui-select-match placeholder="Select your industry">\n' +
    '                                                <span>{{ $item.name }}</span>\n' +
    '                                            </ui-select-match>\n' +
    '                                            <ui-select-choices repeat="industry in vm.store.getState().Industry.items">\n' +
    '                                                <div class="item">\n' +
    '                                                    <p ng-bind="industry.getName()"></p>\n' +
    '                                                </div>\n' +
    '                                            </ui-select-choices>\n' +
    '                                        </ui-select>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="col-sm-10">\n' +
    '                                    <div class="form-group">\n' +
    '                                        <label>Tags</label>\n' +
    '                                        <ui-select multiple ng-model="vm.store.getState().UpdateJob.tags" post-job-ui-select on-select="vm.onSelectTag(vm.store.getState().UpdateJob.tags, $model)" on-remove="vm.onRemoveTag(vm.store.getState().UpdateJob.tags, $model)">\n' +
    '                                            <ui-select-match placeholder="Select your tag">\n' +
    '                                                <span>{{ $item.name }}</span>\n' +
    '                                            </ui-select-match>\n' +
    '                                            <ui-select-choices repeat="tag in vm.store.getState().Tag.items">\n' +
    '                                                <div class="item">\n' +
    '                                                    <p ng-bind="tag.getName()"></p>\n' +
    '                                                </div>\n' +
    '                                            </ui-select-choices>\n' +
    '                                        </ui-select>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="col-md-4 col-sm-6">\n' +
    '                                    <div class="form-group">\n' +
    '                                        <label>Experience</label>\n' +
    '                                        <ui-select ng-model="vm.store.getState().UpdateJob.experience" post-job-ui-select on-select="vm.onSelectExperience($item, $model)">\n' +
    '                                            <ui-select-match placeholder="Select your experience">\n' +
    '                                                <span ng-bind="vm.store.getState().UpdateJob.experience.label"></span>\n' +
    '                                            </ui-select-match>\n' +
    '                                            <ui-select-choices repeat="experience in vm.store.getState().Experience.items">\n' +
    '                                                <div class="item">\n' +
    '                                                    <p ng-bind="experience.getLabel()"></p>\n' +
    '                                                </div>\n' +
    '                                            </ui-select-choices>\n' +
    '                                        </ui-select>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="col-md-4 col-sm-6">\n' +
    '                                    <div class="form-group">\n' +
    '                                        <label>Qualification</label>\n' +
    '                                        <ui-select ng-model="vm.store.getState().UpdateJob.qualification" post-job-ui-select on-select="vm.onSelectQualification($item, $model)">\n' +
    '                                            <ui-select-match placeholder="Select your qualification">\n' +
    '                                                <span ng-bind="vm.store.getState().UpdateJob.qualification.name"></span>\n' +
    '                                            </ui-select-match>\n' +
    '                                            <ui-select-choices repeat="qualification in vm.store.getState().Qualification.items">\n' +
    '                                                <div class="item">\n' +
    '                                                    <p ng-bind="qualification.getName()"></p>\n' +
    '                                                </div>\n' +
    '                                            </ui-select-choices>\n' +
    '                                        </ui-select>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="col-sm-10">\n' +
    '                                    <div class="form-group">\n' +
    '                                        <label>Nationality</label>\n' +
    '                                        <ui-select ng-model="vm.store.getState().UpdateJob.nationality" post-job-ui-select on-select="vm.onSelectNationality($item, $model)">\n' +
    '                                            <ui-select-match placeholder="Select your nationality">\n' +
    '                                                <span ng-bind="vm.store.getState().UpdateJob.nationality.name"></span>\n' +
    '                                            </ui-select-match>\n' +
    '                                            <ui-select-choices repeat="nationality in vm.store.getState().Nationality.items">\n' +
    '                                                <div class="item">\n' +
    '                                                    <p ng-bind="nationality.getName()"></p>\n' +
    '                                                </div>\n' +
    '                                            </ui-select-choices>\n' +
    '                                        </ui-select>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <h3>Job Type</h3>\n' +
    '                            <div class="checkbox-wrapper row">\n' +
    '                                <div class="col-sm-2">\n' +
    '                                    <div class="form-group check">\n' +
    '                                        <label>\n' +
    '                                            <input name="job_type" type="radio" name="language-required" ng-model="vm.store.getState().UpdateJob.type" ng-change="vm.store.dispatch({type: \'UPDATE_POST_JOB\', field: \'type\', data: vm.store.getState().UpdateJob.type})" ng-value="1">\n' +
    '                                            <span>Normal Job</span>\n' +
    '                                        </label>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="col-sm-3">\n' +
    '                                    <div class="form-group check">\n' +
    '                                        <label>\n' +
    '                                            <input name="job_type" type="radio" name="language-required" ng-model="vm.store.getState().UpdateJob.type" ng-change="vm.store.dispatch({type: \'UPDATE_POST_JOB\', field: \'type\', data: vm.store.getState().UpdateJob.type})" ng-value="2">\n' +
    '                                            <span>International Job</span>\n' +
    '                                        </label>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div ng-if="vm.store.getState().UpdateJob.type == 1">\n' +
    '                                <div class="row country-wrapper">\n' +
    '                                    <div class="col-md-4 col-sm-6">\n' +
    '                                        <div class="form-group">\n' +
    '                                            <label>Country</label>\n' +
    '                                            <ui-select ng-model="vm.store.getState().UpdateJob.country" post-job-ui-select on-select="vm.onSelectCountry($item, $model)">\n' +
    '                                                <ui-select-match placeholder="Select your country">\n' +
    '                                                    <span ng-bind="vm.store.getState().UpdateJob.country.name"></span>\n' +
    '                                                </ui-select-match>\n' +
    '                                                <ui-select-choices repeat="country in vm.store.getState().Country.items">\n' +
    '                                                    <div class="item">\n' +
    '                                                        <p ng-bind="country.getName()"></p>\n' +
    '                                                    </div>\n' +
    '                                                </ui-select-choices>\n' +
    '                                            </ui-select>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="row state-wrapper">\n' +
    '                                    <div class="col-md-4 col-sm-6">\n' +
    '                                        <div class="form-group">\n' +
    '                                            <label>State</label>\n' +
    '                                            <ui-select ng-model="vm.store.getState().UpdateJob.state" post-job-ui-select on-select="vm.onSelectState($item, $model)">\n' +
    '                                                <ui-select-match placeholder="Select your state">\n' +
    '                                                    <span ng-bind="vm.store.getState().UpdateJob.state.name"></span>\n' +
    '                                                </ui-select-match>\n' +
    '                                                <ui-select-choices repeat="state in vm.store.getState().State.items">\n' +
    '                                                    <div class="item">\n' +
    '                                                        <p ng-bind="state.getName()"></p>\n' +
    '                                                    </div>\n' +
    '                                                </ui-select-choices>\n' +
    '                                            </ui-select>\n' +
    '                                            <div class="no-state">\n' +
    '                                                <span ng-show="vm.store.getState().UpdateJob.country !== undefined && vm.store.getState().State.items.length === 0">{{ vm.store.getState().UpdateJob.country.name }} doesn\'t have any state</span>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="row city-wrapper">\n' +
    '                                    <div class="col-md-4 col-sm-6">\n' +
    '                                        <div class="form-group">\n' +
    '                                            <label>City</label>\n' +
    '                                            <ui-select ng-model="vm.store.getState().UpdateJob.city" post-job-ui-select on-select="vm.onSelectCity($item, $model)">\n' +
    '                                                <ui-select-match placeholder="Select your city">\n' +
    '                                                    <span ng-bind="vm.store.getState().UpdateJob.city.name"></span>\n' +
    '                                                </ui-select-match>\n' +
    '                                                <ui-select-choices repeat="city in vm.store.getState().City.items">\n' +
    '                                                    <div class="item">\n' +
    '                                                        <p ng-bind="city.getName()"></p>\n' +
    '                                                    </div>\n' +
    '                                                </ui-select-choices>\n' +
    '                                            </ui-select>\n' +
    '                                            <div class="no-city">\n' +
    '                                                <span ng-show="vm.store.getState().UpdateJob.state !== undefined && vm.store.getState().City.items.length === 0">{{ vm.store.getState().UpdateJob.state.name }} doesn\'t have any city</span>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div ng-if="vm.store.getState().UpdateJob.type == 2">\n' +
    '                                <div class="row">\n' +
    '                                    <div class="col-xs-12 col-sm-6">\n' +
    '                                        <div class="form-group location">\n' +
    '                                            <label>Location</label>\n' +
    '                                            <input autocomplete-address="vm.store.getState().UpdateJob.location" type="text" class="form-control" ng-model-options="{ debounce: 500 }" name="location" ng-model="vm.store.getState().UpdateJob.location" ng-change="vm.store.dispatch({type: \'UPDATE_JOB_ADDRESS\', data: vm.store.getState().UpdateJob.location})" required placeholder="Enter a location">\n' +
    '                                            <div class="errors">\n' +
    '                                                <span ng-show="vm.update_job_form.$submitted && vm.update_job_form.location.$error.required">Required</span>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="col-xs-12 col-sm-6">\n' +
    '                                    <div class="form-group location">\n' +
    '                                        <label>Address</label>\n' +
    '                                        <input autocomplete-address="vm.store.getState().UpdateJob.address" type="text" class="form-control" ng-model-options="{ debounce: 500 }" name="address" ng-model="vm.store.getState().UpdateJob.address" ng-change="vm.store.dispatch({type: \'UPDATE_JOB_ADDRESS\', data: vm.store.getState().UpdateJob.address})" placeholder="Enter a address" required>\n' +
    '                                        <div class="errors">\n' +
    '                                            <span ng-show="vm.update_job_form.$submitted && vm.update_job_form.address.$error.required">Required</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="col-xs-12 col-sm-6">\n' +
    '                                    <div class="form-group location">\n' +
    '                                        <label>Certificate</label>\n' +
    '                                        <input type="text" name="certificate" class="form-control" ng-model="vm.store.getState().UpdateJob.certificate" ng-model-options="{debounce: 500}" ng-change="vm.store.dispatch({type: \'UPDATE_POST_JOB\', field: \'certificate\', data: vm.store.getState().UpdateJob.certificate})">\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <div class="row">\n' +
    '                                <div class="col-xs-12 col-sm-3">\n' +
    '                                    <div class="form-group location">\n' +
    '                                        <label>Min Salary</label>\n' +
    '                                        <input type="text" name="min_salary" class="form-control" placeholder="Min Salary" ng-model="vm.store.getState().UpdateJob.min_salary" ng-model-options="{debounce: 500}" ng-change="vm.store.dispatch({type: \'UPDATE_POST_JOB\', field: \'min_salary\', data: vm.store.getState().UpdateJob.min_salary})" ng-pattern="/^\\d{1,8}$|^\\d{1,8}\\.\\d{1,2}$/" required>\n' +
    '                                        <div class="errors">\n' +
    '                                            <span ng-show="vm.update_job_form.$submitted && vm.update_job_form.min_salary.$error.required">Required</span>\n' +
    '                                            <span ng-show="vm.update_job_form.$submitted && vm.update_job_form.min_salary.$error.pattern">Please enter a valid number</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="col-xs-12 col-sm-3">\n' +
    '                                    <div class="form-group location">\n' +
    '                                        <label>Max Salary</label>\n' +
    '                                        <input type="text" name="max_salary" class="form-control" placeholder="Min Salary" ng-model="vm.store.getState().UpdateJob.max_salary" ng-model-options="{debounce: 500}" ng-change="vm.store.dispatch({type: \'UPDATE_POST_JOB\', field: \'max_salary\', data: vm.store.getState().UpdateJob.max_salary})" ng-pattern="/^\\d{1,8}$|^\\d{1,8}\\.\\d{1,2}$/" required>\n' +
    '                                        <div class="errors">\n' +
    '                                            <span ng-show="vm.update_job_form.$submitted && vm.update_job_form.max_salary.$error.required">Required</span>\n' +
    '                                            <span ng-show="vm.update_job_form.$submitted && vm.update_job_form.max_salary.$error.pattern">Please enter a valid number</span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                            <h3>Job Requirements</h3>\n' +
    '                            <div class="checkbox-wrapper">\n' +
    '                                <!--  <div class="form-group check">\n' +
    '                                    <label>\n' +
    '                                        <input type="checkbox" name="language-required" ng-model="vm.store.getState().UpdateJob.language_required" ng-change="vm.store.dispatch({type: \'UPDATE_POST_JOB\', field: \'language_required\', data: vm.store.getState().UpdateJob.language_required})">\n' +
    '                                        <span>Additional Language</span>\n' +
    '                                    </label>\n' +
    '                                    <div class="row" ng-if="vm.store.getState().UpdateJob.language_required">\n' +
    '                                        <div class="col-xs-12 col-sm-6">\n' +
    '                                            <ui-select post-job-ui-select ng-model="vm.store.getState().UpdateJob.language" on-select="vm.onSelectLanguage($item, $model)">\n' +
    '                                                <ui-select-match>\n' +
    '                                                    <span ng-bind="vm.store.getState().UpdateJob.language.name"></span>\n' +
    '                                                </ui-select-match>\n' +
    '                                                <ui-select-choices repeat="language in vm.store.getState().UpdateJob.languages">\n' +
    '                                                    <div class="item ">\n' +
    '                                                        <p ng-bind="language.name"></p>\n' +
    '                                                    </div>\n' +
    '                                                </ui-select-choices>\n' +
    '                                            </ui-select>\n' +
    '                                            <div class="errors">\n' +
    '                                                <span ng-show="vm.update_job_form.$submitted && !vm.store.getState().UpdateJob.language">Please select a language.</span>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div> -->\n' +
    '                                <div class="form-group check">\n' +
    '                                    <label>\n' +
    '                                        <input type="checkbox" ng-model="vm.store.getState().UpdateJob.gender_required" ng-change="vm.store.dispatch({type: \'UPDATE_POST_JOB\', field: \'gender_required\', data: vm.store.getState().UpdateJob.gender_required})">\n' +
    '                                        <span>Gender</span>\n' +
    '                                    </label>\n' +
    '                                    <div class="row" ng-if="vm.store.getState().UpdateJob.gender_required">\n' +
    '                                        <div class="col-xs-12 col-sm-6">\n' +
    '                                            <ui-select post-job-ui-select ng-model="vm.store.getState().UpdateJob.gender" on-select="vm.onSelectGender($item, $model)">\n' +
    '                                                <ui-select-match>\n' +
    '                                                    <span ng-bind="vm.store.getState().UpdateJob.gender.name"></span>\n' +
    '                                                </ui-select-match>\n' +
    '                                                <ui-select-choices repeat="gender in vm.candidate_genders">\n' +
    '                                                    <div class="item ">\n' +
    '                                                        <p ng-bind="gender.name"></p>\n' +
    '                                                    </div>\n' +
    '                                                </ui-select-choices>\n' +
    '                                            </ui-select>\n' +
    '                                            <div class="errors">\n' +
    '                                                <span ng-show="vm.update_job_form.$submitted && !vm.store.getState().UpdateJob.gender">Please select a gender.</span>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <!-- <div class="form-group check">\n' +
    '                                    <label>\n' +
    '                                        <input type="checkbox" ng-model="vm.store.getState().UpdateJob.driving_license_required" ng-change="vm.store.dispatch({type: \'UPDATE_POST_JOB\', field: \'driving_license_required\', data: vm.store.getState().UpdateJob.driving_license_required})">\n' +
    '                                        <span>Driving License</span>\n' +
    '                                    </label>\n' +
    '                                    <div class="row" ng-if="vm.store.getState().UpdateJob.driving_license_required">\n' +
    '                                        <div class="col-xs-12 col-sm-6">\n' +
    '                                            <ui-select post-job-ui-select ng-model="vm.store.getState().UpdateJob.driving_license" on-select="vm.onSelectDrivingLicense($item, $model)">\n' +
    '                                                <ui-select-match>\n' +
    '                                                    <span ng-bind="vm.store.getState().UpdateJob.driving_license.label"></span>\n' +
    '                                                </ui-select-match>\n' +
    '                                                <ui-select-choices repeat="license in vm.driving_licenses">\n' +
    '                                                    <div class="item ">\n' +
    '                                                        <p ng-bind="license.label"></p>\n' +
    '                                                    </div>\n' +
    '                                                </ui-select-choices>\n' +
    '                                            </ui-select>\n' +
    '                                            <div class="errors">\n' +
    '                                                <span ng-show="vm.update_job_form.$submitted && !vm.store.getState().UpdateJob.driving_license">Please select a driving license.</span>\n' +
    '                                            </div>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                </div> -->\n' +
    '                            </div>\n' +
    '                            <div class="row ">\n' +
    '                                <div class="col-sm-6 control-btns">\n' +
    '                                    <!-- <a href="#" class="btn btn-primary btn-grey">Delete Job Offer</a> -->\n' +
    '                                    <a ng-click="vm.saveAsDraft()" class="btn btn-primary btn-grey">Save as Draft</a>\n' +
    '                                    <button type="submit" class="btn btn-primary btn-orange">Update Job Offer</a>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </form>\n' +
    '                    </div>\n' +
    '                </section>\n' +
    '            </section>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/nationality/create/_create.tpl.html',
    '<div class="create-page create-nationality">\n' +
    '    <div class="col-xs-12">\n' +
    '        <div class="create-form create-nationality-form">\n' +
    '            <h2 class="text-center">Create new Nationality</h2>\n' +
    '            <form name="vm.create_nationality_form" ng-submit="vm.create_nationality_form.$valid && vm.createNationality(vm.nationality)" novalidate>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12 col-sm-6">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Name *</label>\n' +
    '                            <input type="text" ng-focus="vm.create_nationality_form.name.$focused = true" class="form-control" ng-model="vm.nationality.name" name="name" required ng-maxlength="255">\n' +
    '                            <div class="icons" ng-show="vm.create_nationality_form.name.$focused">\n' +
    '                                <i ng-if="!vm.create_nationality_form.name.$error.required && !vm.create_nationality_form.name.$error.maxlength" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.create_nationality_form.name.$error.required || vm.create_nationality_form.name.$error.maxlength" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.create_nationality_form.$submitted && vm.create_nationality_form.name.$error.required">Required</span>\n' +
    '                                <span ng-show="vm.create_nationality_form.$submitted && vm.create_nationality_form.name.$error.maxlength">The value is too long</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="col-xs-12 col-sm-6">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Order</label>\n' +
    '                            <input type="text" ng-focus="vm.create_nationality_form.order.$focused = true" class="form-control" ng-model="vm.nationality.order" name="order" ng-pattern="/^\\d+$/">\n' +
    '                            <div class="icons" ng-show="vm.create_nationality_form.order.$focused">\n' +
    '                                <i ng-if="!vm.create_nationality_form.order.$error.pattern" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.create_nationality_form.order.$error.pattern" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.create_nationality_form.$submitted && vm.create_nationality_form.order.$error.pattern">Please enter a number</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12">\n' +
    '                        <div class="form-group">\n' +
    '                            <label class="active">Active</label>\n' +
    '                            <input type="checkbox" ng-focus="vm.create_nationality_form.status.$focused = true" ng-model="vm.nationality.status" name="status" ng-true-value="1" ng-false-value="0">\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="form-group">\n' +
    '                    <input ng-disabled="inProgress" type="submit" class="btn ui-btn btn-full-width orange-btn-sm" value="Add">\n' +
    '                </div>\n' +
    '            </form>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/nationality/edit/_edit.tpl.html',
    '<div class="edit-page edit-nationality">\n' +
    '    <div class="col-xs-12">\n' +
    '        <div class="edit-form edit-nationality-form">\n' +
    '            <h2 class="text-center">Edit Nationality</h2>\n' +
    '            <form name="vm.edit_nationality_form" ng-submit="vm.edit_nationality_form.$valid && vm.editNationality(vm.nationality)" novalidate>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12 col-sm-6">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Name *</label>\n' +
    '                            <input type="text" ng-focus="vm.edit_nationality_form.name.$focused = true" class="form-control" ng-model="vm.nationality.name" name="name" required ng-maxlength="255">\n' +
    '                            <div class="icons" ng-show="vm.edit_nationality_form.name.$focused">\n' +
    '                                <i ng-if="!vm.edit_nationality_form.name.$error.required && !vm.edit_nationality_form.name.$error.maxlength" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.edit_nationality_form.name.$error.required || vm.edit_nationality_form.name.$error.maxlength" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.edit_nationality_form.$submitted && vm.edit_nationality_form.name.$error.required">Required</span>\n' +
    '                                <span ng-show="vm.edit_nationality_form.$submitted && vm.edit_nationality_form.name.$error.maxlength">The value is too long</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="col-xs-12 col-sm-6">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Order</label>\n' +
    '                            <input type="text" ng-focus="vm.edit_nationality_form.order.$focused = true" class="form-control" ng-model="vm.nationality.order" name="order" ng-pattern="/^\\d+$/">\n' +
    '                            <div class="icons" ng-show="vm.edit_nationality_form.order.$focused">\n' +
    '                                <i ng-if="!vm.edit_nationality_form.order.$error.pattern" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.edit_nationality_form.order.$error.pattern" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.edit_nationality_form.$submitted && vm.edit_nationality_form.order.$error.pattern">Please enter a number</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12">\n' +
    '                        <div class="form-group">\n' +
    '                            <label class="active">Active</label>\n' +
    '                            <input type="checkbox" ng-focus="vm.edit_nationality_form.status.$focused = true" ng-model="vm.nationality.status" name="status" ng-true-value="1" ng-false-value="0">\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="form-group">\n' +
    '                    <input ng-disabled="inProgress" type="submit" class="btn ui-btn btn-full-width orange-btn-sm" value="Edit">\n' +
    '                </div>\n' +
    '            </form>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/nationality/list/_list.tpl.html',
    '<div class="list-page list-nationalities">\n' +
    '    <div class="row filter">\n' +
    '        <div class="col-sm-2 col-xs-12 filter-status">\n' +
    '            <label class="ui-label" for="status">Status</label>\n' +
    '            <select class="ui-select" ng-model="vm.status" ng-change="vm.filterStatus(vm.status)" ng-options="options as options.name for options in vm.statuses"></select>\n' +
    '        </div>\n' +
    '        <div class="col-sm-3 col-sm-offset-7 text-right item-action new-record-wrapper">\n' +
    '            <ul>\n' +
    '                <li class="active">\n' +
    '                    <a ng-click="vm.changeStatusAllItem(\'active\')" title="Active">\n' +
    '                        <i class="icon fa fa-check-circle"></i>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '                <li class="pending">\n' +
    '                    <a ng-click="vm.changeStatusAllItem(\'pending\')" title="Pending">\n' +
    '                        <i class="icon fa fa-pause-circle"></i>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '                <li class="new-item">\n' +
    '                    <a ui-sref="app.nationality.create" title="Create new Nationality">\n' +
    '                        <i class="icon fa fa-plus-circle"></i>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '            </ul>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row search-box">\n' +
    '        <div class="col-xs-12 col-sm-6 select-all">\n' +
    '            <input type="checkbox" id="select-all" ng-model="vm.selectedAll" ng-change="vm.selectAll()">\n' +
    '            <label for="select-all">Select all</label>\n' +
    '            <span class="number-select" ng-show="vm.selectedItem.length > 0"> ( All {{vm.selectedItem.length}} {{vm.selectedItem.length === 1 ? \'item\' : \'items\'}}  on this page are selected.)</span>\n' +
    '        </div>\n' +
    '        <div class="search col-xs-12 col-sm-6">\n' +
    '            <div class="search-wrapper card">\n' +
    '                <input id="search" class="input-search form-control" ng-model="vm.params.search" placeholder="Search..." ng-keydown="vm.searchKeyEvent($event, vm.params.search)"><i class="icon-search fa fa-search" ng-click="vm.searchPage(vm.params.search)"></i>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- <div class="row">\n' +
    '        <div class="col-xs-12 col-sm-6">\n' +
    '            Sort By:\n' +
    '            <sortable-attributes></sortable-attributes>\n' +
    '        </div>\n' +
    '    </div> -->\n' +
    '    <preloader ng-if="vm.nationalities | isUndefined"></preloader>\n' +
    '    <div class="row">\n' +
    '        <div class="col-xs-12">\n' +
    '            <table class="table table-striped ui-table table-items table-nationalities" ng-if="vm.nationalities !== undefined">\n' +
    '                <thead>\n' +
    '                    <tr>\n' +
    '                        <th></th>\n' +
    '                        <th>ID <!-- <span sortable="id"></span> --></th>\n' +
    '                        <th>Name <!-- <span sortable="name"></span> --></th>\n' +
    '                        <th>Order <!-- <span sortable="order"></span> --></th>\n' +
    '                        <th>Active <!-- <span sortable="status"></span> --></th>\n' +
    '                        <th>Status</th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody>\n' +
    '                    <tr ng-repeat="nationality in vm.nationalities">\n' +
    '                        <td>\n' +
    '                            <input type="checkbox" ng-model="nationality.selected" ng-change="vm.updateSelectedItem()">\n' +
    '                        </td>\n' +
    '                        <td ng-bind="nationality.getId()"></td>\n' +
    '                        <td ng-bind="nationality.getName()"></td>\n' +
    '                        <td ng-bind="nationality.getOrder()"></td>\n' +
    '                        <td>\n' +
    '                            {{ nationality.getStatus() === 1 ? \'Active\' : \'Pending\' }}\n' +
    '                        </td>\n' +
    '                        <td class="text-right action">\n' +
    '                            <a ui-sref="app.nationality.edit({id: nationality.getId()})" class="edit">\n' +
    '                                <p class="ui-p"><i class="icon fa fa-pencil-square-o"></i></p>\n' +
    '                            </a>\n' +
    '                            <a ng-click="vm.deleteItem(nationality)" class="delete">\n' +
    '                                <p class="ui-p"><i class="icon fa fa-trash"></i></p>\n' +
    '                            </a>\n' +
    '                        </td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '            <pagination data="vm.pagination"></pagination>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/payment/connected_accounts/_connected_account.tpl.html',
    '<div class="payment-history">\n' +
    '    <table class="striped table-history">\n' +
    '        <thead>\n' +
    '            <tr>\n' +
    '                <th>User ID</th>\n' +
    '                <th>First Name</th>\n' +
    '                <th>Last Name</th>\n' +
    '                <th>Email</th>\n' +
    '                <th>Account Balance</th>\n' +
    '                <th>Currency</th>\n' +
    '                <th>View Stripe Dashboard As</th>\n' +
    '                <th class="actions-col right-align">Action</th>\n' +
    '            </tr>\n' +
    '        </thead>\n' +
    '        <tbody>\n' +
    '            <tr ng-repeat="user in vm.users">\n' +
    '                <td ng-bind="user.getId()"></td>\n' +
    '                <td ng-bind="user.getFirstName()"></td>\n' +
    '                <td ng-bind="user.getLastName()"></td>\n' +
    '                <td>\n' +
    '                    <a ui-sref="app.employees.edit({id: user.getId()})" ng-bind="user.getEmail()"></a>\n' +
    '                </td>\n' +
    '                <td ng-bind="user.balance.getFormattedAmount()"></td>\n' +
    '                <td ng-bind="user.balance.currency"></td>\n' +
    '                <td><a href="https://dashboard.stripe.com/{{user.payment.id}}" target="_blank">Stripe Dashboard</a></td>\n' +
    '                <td class="actions-col center-align" ng-if="vm.user.can(\'update.transactions\')">\n' +
    '                    <a ui-sref="app.payment.transfers.detail({\'id\':transaction.getId()})">More</a>\n' +
    '                </td>\n' +
    '            </tr>\n' +
    '        </tbody>\n' +
    '    </table>\n' +
    '    <div class="right-align">\n' +
    '        <pagination data="vm.pagination"></pagination>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/payment/create/_create.tpl.html',
    '<div class="payment">\n' +
    '    <div class="container">\n' +
    '        <h4>Send money to employee</h4>\n' +
    '        <form name="vm.payment_form" ng-submit="vm.payment_form.$valid && vm.pay(vm.transaction)" novalidate>\n' +
    '            <div class="edit-item">\n' +
    '                <div class="row">\n' +
    '                    <div class="col s12">\n' +
    '                        <div class="form-group">\n' +
    '                            <label>Employee Email *</label>\n' +
    '                            <input type="email" class="form-control" ng-model="vm.transaction.email" name="email" required>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.payment_form.$submitted && vm.payment_form.email.$error.required">Required</span>\n' +
    '                                <span ng-show="vm.payment_form.$submitted && vm.payment_form.email.$error.email">Please enter a valid email</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col s12">\n' +
    '                        <div class="form-group">\n' +
    '                            <label>Amount in SGD * (Ex: 100.5)</label>\n' +
    '                            <input type="text" class="form-control" name="amount" ng-model="vm.transaction.amount" ng-pattern="/^\\d{1,8}$|^\\d{1,8}\\.\\d{1,2}$/" required>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.payment_form.$submitted && vm.payment_form.amount.$error.required">Required</span>\n' +
    '                                <span ng-show="vm.payment_form.$submitted && vm.payment_form.amount.$error.pattern">Please enter a valid amount</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col s12">\n' +
    '                        <div class="form-group">\n' +
    '                            <label>PIN</label>\n' +
    '                            <input type="password" class="form-control" name="pin" ng-model="vm.transaction.pin" required>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.payment_form.$submitted && vm.payment_form.pin.$error.required">Required</span>\n' +
    '                                <span ng-show="vm.payment_form.$submitted && vm.payment_form.pin.$error.pattern">Please enter a valid amount</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col s12">\n' +
    '                        <div class="form-group">\n' +
    '                            <input type="submit" class="btn btn-primary orange-btn-sm" value="Send Money">\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </form>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/qualification/create/_create.tpl.html',
    '<div class="create-page create-qualification">\n' +
    '    <div class="col-xs-12">\n' +
    '        <div class="create-form create-qualification-form">\n' +
    '            <h2 class="text-center">Create new Qualification</h2>\n' +
    '            <form name="vm.create_qualification_form" ng-submit="vm.create_qualification_form.$valid && vm.createQualification(vm.qualification)" novalidate>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12 col-sm-6">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Name *</label>\n' +
    '                            <input type="text" ng-focus="vm.create_qualification_form.name.$focused = true" class="form-control" ng-model="vm.qualification.name" name="name" required ng-maxlength="255">\n' +
    '                            <div class="icons" ng-show="vm.create_qualification_form.name.$focused">\n' +
    '                                <i ng-if="!vm.create_qualification_form.name.$error.required && !vm.create_qualification_form.name.$error.maxlength" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.create_qualification_form.name.$error.required || vm.create_qualification_form.name.$error.maxlength" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.create_qualification_form.$submitted && vm.create_qualification_form.name.$error.required">Required</span>\n' +
    '                                <span ng-show="vm.create_qualification_form.$submitted && vm.create_qualification_form.name.$error.maxlength">The value is too long</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="col-xs-12 col-sm-6">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Order</label>\n' +
    '                            <input type="text" ng-focus="vm.create_qualification_form.order.$focused = true" class="form-control" ng-model="vm.qualification.order" name="order" ng-pattern="/^\\d+$/">\n' +
    '                            <div class="icons" ng-show="vm.create_qualification_form.order.$focused">\n' +
    '                                <i ng-if="!vm.create_qualification_form.order.$error.pattern" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.create_qualification_form.order.$error.pattern" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.create_qualification_form.$submitted && vm.create_qualification_form.order.$error.pattern">Please enter a number</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12">\n' +
    '                        <div class="form-group">\n' +
    '                            <label class="active">Active</label>\n' +
    '                            <input type="checkbox" ng-focus="vm.create_qualification_form.status.$focused = true" ng-model="vm.qualification.status" name="status" ng-true-value="1" ng-false-value="0">\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="form-group">\n' +
    '                    <input ng-disabled="inProgress" type="submit" class="btn ui-btn btn-full-width orange-btn-sm" value="Add">\n' +
    '                </div>\n' +
    '            </form>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/qualification/edit/_edit.tpl.html',
    '<div class="edit-page edit-qualification">\n' +
    '    <div class="col-xs-12">\n' +
    '        <div class="edit-form edit-qualification-form">\n' +
    '            <h2 class="text-center">Edit Qualification</h2>\n' +
    '            <form name="vm.edit_qualification_form" ng-submit="vm.edit_qualification_form.$valid && vm.editQualification(vm.qualification)" novalidate>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12 col-sm-6">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Name *</label>\n' +
    '                            <input type="text" ng-focus="vm.edit_qualification_form.name.$focused = true" class="form-control" ng-model="vm.qualification.name" name="name" required ng-maxlength="255">\n' +
    '                            <div class="icons" ng-show="vm.edit_qualification_form.name.$focused">\n' +
    '                                <i ng-if="!vm.edit_qualification_form.name.$error.required && !vm.edit_qualification_form.name.$error.maxlength" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.edit_qualification_form.name.$error.required || vm.edit_qualification_form.name.$error.maxlength" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.edit_qualification_form.$submitted && vm.edit_qualification_form.name.$error.required">Required</span>\n' +
    '                                <span ng-show="vm.edit_qualification_form.$submitted && vm.edit_qualification_form.name.$error.maxlength">The value is too long</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="col-xs-12 col-sm-6">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Order</label>\n' +
    '                            <input type="text" ng-focus="vm.edit_qualification_form.order.$focused = true" class="form-control" ng-model="vm.qualification.order" name="order" ng-pattern="/^\\d+$/">\n' +
    '                            <div class="icons" ng-show="vm.edit_qualification_form.order.$focused">\n' +
    '                                <i ng-if="!vm.edit_qualification_form.order.$error.pattern" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.edit_qualification_form.order.$error.pattern" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.edit_qualification_form.$submitted && vm.edit_qualification_form.order.$error.pattern">Please enter a number</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12">\n' +
    '                        <div class="form-group">\n' +
    '                            <label class="active">Active</label>\n' +
    '                            <input type="checkbox" ng-focus="vm.edit_qualification_form.status.$focused = true" ng-model="vm.qualification.status" name="status" ng-true-value="1" ng-false-value="0">\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="form-group">\n' +
    '                    <input ng-disabled="inProgress" type="submit" class="btn ui-btn btn-full-width orange-btn-sm" value="Edit">\n' +
    '                </div>\n' +
    '            </form>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/qualification/list/_list.tpl.html',
    '<div class="list-page list-qualifications">\n' +
    '    <div class="row filter">\n' +
    '        <div class="col-sm-2 col-xs-12 filter-status">\n' +
    '            <label class="ui-label" for="status">Status</label>\n' +
    '            <select class="ui-select" ng-model="vm.status" ng-change="vm.filterStatus(vm.status)" ng-options="options as options.name for options in vm.statuses"></select>\n' +
    '        </div>\n' +
    '        <div class="col-sm-3 col-sm-offset-7 text-right item-action new-record-wrapper">\n' +
    '            <ul>\n' +
    '                <li class="active">\n' +
    '                    <a ng-click="vm.changeStatusAllItem(\'active\')" title="Active">\n' +
    '                        <i class="icon fa fa-check-circle"></i>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '                <li class="pending">\n' +
    '                    <a ng-click="vm.changeStatusAllItem(\'pending\')" title="Pending">\n' +
    '                        <i class="icon fa fa-pause-circle"></i>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '                <li class="new-item">\n' +
    '                    <a ui-sref="app.qualification.create" title="Create new Qualification">\n' +
    '                        <i class="icon fa fa-plus-circle"></i>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '            </ul>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row search-box">\n' +
    '        <div class="col-xs-12 col-sm-6 select-all">\n' +
    '            <input type="checkbox" id="select-all" ng-model="vm.selectedAll" ng-change="vm.selectAll()">\n' +
    '            <label for="select-all">Select all</label>\n' +
    '            <span class="number-select" ng-show="vm.selectedItem.length > 0"> ( All {{vm.selectedItem.length}} {{vm.selectedItem.length === 1 ? \'item\' : \'items\'}}  on this page are selected.)</span>\n' +
    '        </div>\n' +
    '        <div class="search col-xs-12 col-sm-6">\n' +
    '            <div class="search-wrapper card">\n' +
    '                <input id="search" class="input-search form-control" ng-model="vm.params.search" placeholder="Search..." ng-keydown="vm.searchKeyEvent($event, vm.params.search)"><i class="icon-search fa fa-search" ng-click="vm.searchPage(vm.params.search)"></i>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- <div class="row">\n' +
    '        <div class="col-xs-12 col-sm-6">\n' +
    '            Sort By:\n' +
    '            <sortable-attributes></sortable-attributes>\n' +
    '        </div>\n' +
    '    </div> -->\n' +
    '    <preloader ng-if="vm.qualifications | isUndefined"></preloader>\n' +
    '    <div class="row">\n' +
    '        <div class="col-xs-12">\n' +
    '            <table class="table table-striped ui-table table-items table-qualifications" ng-if="vm.qualifications !== undefined">\n' +
    '                <thead>\n' +
    '                    <tr>\n' +
    '                        <th></th>\n' +
    '                        <th>ID <!-- <span sortable="id"></span> --></th>\n' +
    '                        <th>Name <!-- <span sortable="name"></span> --></th>\n' +
    '                        <th>Order <!-- <span sortable="order"></span> --></th>\n' +
    '                        <th>Active <!-- <span sortable="status"></span> --></th>\n' +
    '                        <th>Status</th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody>\n' +
    '                    <tr ng-repeat="qualification in vm.qualifications">\n' +
    '                        <td>\n' +
    '                            <input type="checkbox" ng-model="qualification.selected" ng-change="vm.updateSelectedItem()">\n' +
    '                        </td>\n' +
    '                        <td ng-bind="qualification.getId()"></td>\n' +
    '                        <td ng-bind="qualification.getName()"></td>\n' +
    '                        <td ng-bind="qualification.getOrder()"></td>\n' +
    '                        <td>\n' +
    '                            {{ qualification.getStatus() === 1 ? \'Active\' : \'Pending\' }}\n' +
    '                        </td>\n' +
    '                        <td class="text-right action">\n' +
    '                            <a ui-sref="app.qualification.edit({id: qualification.getId()})" class="edit">\n' +
    '                                <p class="ui-p"><i class="icon fa fa-pencil-square-o"></i></p>\n' +
    '                            </a>\n' +
    '                            <a ng-click="vm.deleteItem(qualification)" class="delete">\n' +
    '                                <p class="ui-p"><i class="icon fa fa-trash"></i></p>\n' +
    '                            </a>\n' +
    '                        </td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '            <pagination data="vm.pagination"></pagination>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/tag/create/_create.tpl.html',
    '<div class="create-page create-tag">\n' +
    '    <div class="col-xs-12">\n' +
    '        <div class="create-form create-tag-form">\n' +
    '            <h2 class="text-center">Create new Tag</h2>\n' +
    '            <form name="vm.create_tag_form" ng-submit="vm.create_tag_form.$valid && vm.createTag(vm.tag)" novalidate>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12 col-sm-6">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Name *</label>\n' +
    '                            <input type="text" ng-focus="vm.create_tag_form.name.$focused = true" class="form-control" ng-model="vm.tag.name" name="name" required ng-maxlength="255">\n' +
    '                            <div class="icons" ng-show="vm.create_tag_form.name.$focused">\n' +
    '                                <i ng-if="!vm.create_tag_form.name.$error.required && !vm.create_tag_form.name.$error.maxlength" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.create_tag_form.name.$error.required || vm.create_tag_form.name.$error.maxlength" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.create_tag_form.$submitted && vm.create_tag_form.name.$error.required">Required</span>\n' +
    '                                <span ng-show="vm.create_tag_form.$submitted && vm.create_tag_form.name.$error.maxlength">The value is too long</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="col-xs-12 col-sm-6">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Order</label>\n' +
    '                            <input type="text" ng-focus="vm.create_tag_form.order.$focused = true" class="form-control" ng-model="vm.tag.order" name="order" ng-pattern="/^\\d+$/">\n' +
    '                            <div class="icons" ng-show="vm.create_tag_form.order.$focused">\n' +
    '                                <i ng-if="!vm.create_tag_form.order.$error.pattern" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.create_tag_form.order.$error.pattern" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.create_tag_form.$submitted && vm.create_tag_form.order.$error.pattern">Please enter a number</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12">\n' +
    '                        <div class="form-group">\n' +
    '                            <label class="active">Active</label>\n' +
    '                            <input type="checkbox" ng-focus="vm.create_tag_form.active.$focused = true" ng-model="vm.tag.active" name="active" ng-true-value="true" ng-false-value="false">\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="form-group">\n' +
    '                    <input ng-disabled="inProgress" type="submit" class="btn ui-btn btn-full-width orange-btn-sm" value="Add">\n' +
    '                </div>\n' +
    '            </form>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/tag/edit/_edit.tpl.html',
    '<div class="edit-page edit-tag">\n' +
    '    <div class="col-xs-12">\n' +
    '        <div class="edit-form edit-tag-form">\n' +
    '            <h2 class="text-center">Edit Tag</h2>\n' +
    '            <form name="vm.edit_tag_form" ng-submit="vm.edit_tag_form.$valid && vm.editTag(vm.tag)" novalidate>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12 col-sm-6">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Name *</label>\n' +
    '                            <input type="text" ng-focus="vm.edit_tag_form.name.$focused = true" class="form-control" ng-model="vm.tag.name" name="name" required ng-maxlength="255">\n' +
    '                            <div class="icons" ng-show="vm.edit_tag_form.name.$focused">\n' +
    '                                <i ng-if="!vm.edit_tag_form.name.$error.required && !vm.edit_tag_form.name.$error.maxlength" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.edit_tag_form.name.$error.required || vm.edit_tag_form.name.$error.maxlength" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.edit_tag_form.$submitted && vm.edit_tag_form.name.$error.required">Required</span>\n' +
    '                                <span ng-show="vm.edit_tag_form.$submitted && vm.edit_tag_form.name.$error.maxlength">The value is too long</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                    <div class="col-xs-12 col-sm-6">\n' +
    '                        <div class="form-group float-input">\n' +
    '                            <label class="active">Order</label>\n' +
    '                            <input type="text" ng-focus="vm.edit_tag_form.order.$focused = true" class="form-control" ng-model="vm.tag.order" name="order" ng-pattern="/^\\d+$/">\n' +
    '                            <div class="icons" ng-show="vm.edit_tag_form.order.$focused">\n' +
    '                                <i ng-if="!vm.edit_tag_form.order.$error.pattern" class="valid fa fa-check-circle-o" aria-hidden="true"></i>\n' +
    '                                <i ng-if="vm.edit_tag_form.order.$error.pattern" class="invalid fa fa-times-circle-o" aria-hidden="true"></i>\n' +
    '                            </div>\n' +
    '                            <div class="errors">\n' +
    '                                <span ng-show="vm.edit_tag_form.$submitted && vm.edit_tag_form.order.$error.pattern">Please enter a number</span>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="row">\n' +
    '                    <div class="col-xs-12">\n' +
    '                        <div class="form-group">\n' +
    '                            <label class="active">Active</label>\n' +
    '                            <input type="checkbox" ng-focus="vm.edit_tag_form.active.$focused = true" ng-model="vm.tag.active" name="active" ng-true-value="true" ng-false-value="false">\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <div class="form-group">\n' +
    '                    <input ng-disabled="inProgress" type="submit" class="btn ui-btn btn-full-width orange-btn-sm" value="Edit">\n' +
    '                </div>\n' +
    '            </form>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/tag/list/_list.tpl.html',
    '<div class="list-page list-tags">\n' +
    '    <div class="row filter">\n' +
    '        <div class="col-sm-2 col-xs-12 filter-status">\n' +
    '            <label class="ui-label" for="status">Status</label>\n' +
    '            <select class="ui-select" ng-model="vm.status" ng-change="vm.filterStatus(vm.status)" ng-options="options as options.name for options in vm.statuses"></select>\n' +
    '        </div>\n' +
    '        <div class="col-sm-3 col-sm-offset-7 text-right item-action new-record-wrapper">\n' +
    '            <ul>\n' +
    '                <li class="active">\n' +
    '                    <a ng-click="vm.changeStatusAllItem(\'active\')" title="Active">\n' +
    '                        <i class="icon fa fa-check-circle"></i>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '                <li class="pending">\n' +
    '                    <a ng-click="vm.changeStatusAllItem(\'pending\')" title="Pending">\n' +
    '                        <i class="icon fa fa-pause-circle"></i>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '                <li class="new-item">\n' +
    '                    <a ui-sref="app.tag.create" title="Create new Tag">\n' +
    '                        <i class="icon fa fa-plus-circle"></i>\n' +
    '                    </a>\n' +
    '                </li>\n' +
    '            </ul>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row search-box">\n' +
    '        <div class="col-xs-12 col-sm-6 select-all">\n' +
    '            <input type="checkbox" id="select-all" ng-model="vm.selectedAll" ng-change="vm.selectAll()">\n' +
    '            <label for="select-all">Select all</label>\n' +
    '            <span class="number-select" ng-show="vm.selectedItem.length > 0"> ( All {{vm.selectedItem.length}} {{vm.selectedItem.length === 1 ? \'item\' : \'items\'}}  on this page are selected.)</span>\n' +
    '        </div>\n' +
    '        <div class="search col-xs-12 col-sm-6">\n' +
    '            <div class="search-wrapper card">\n' +
    '                <input id="search" class="input-search form-control" ng-model="vm.params.search" placeholder="Search..." ng-keydown="vm.searchKeyEvent($event, vm.params.search)"><i class="icon-search fa fa-search" ng-click="vm.searchPage(vm.params.search)"></i>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <!-- <div class="row">\n' +
    '        <div class="col-xs-12 col-sm-6">\n' +
    '            Sort By:\n' +
    '            <sortable-attributes></sortable-attributes>\n' +
    '        </div>\n' +
    '    </div> -->\n' +
    '    <preloader ng-if="vm.tags | isUndefined"></preloader>\n' +
    '    <div class="row">\n' +
    '        <div class="col-xs-12">\n' +
    '            <table class="table table-striped ui-table table-items table-tags" ng-if="vm.tags !== undefined">\n' +
    '                <thead>\n' +
    '                    <tr>\n' +
    '                        <th></th>\n' +
    '                        <th>ID <!-- <span sortable="id"></span> --></th>\n' +
    '                        <th>Name <!-- <span sortable="name"></span> --></th>\n' +
    '                        <th>Order <!-- <span sortable="order"></span> --></th>\n' +
    '                        <th>Active <!-- <span sortable="status"></span> --></th>\n' +
    '                        <th>Status</th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody>\n' +
    '                    <tr ng-repeat="tag in vm.tags">\n' +
    '                        <td>\n' +
    '                            <input type="checkbox" ng-model="tag.selected" ng-change="vm.updateSelectedItem()">\n' +
    '                        </td>\n' +
    '                        <td ng-bind="tag.getId()"></td>\n' +
    '                        <td ng-bind="tag.getName()"></td>\n' +
    '                        <td ng-bind="tag.getOrder()"></td>\n' +
    '                        <td>\n' +
    '                            {{ tag.getActive() ? \'Active\' : \'Pending\' }}\n' +
    '                        </td>\n' +
    '                        <td class="text-right action">\n' +
    '                            <a ui-sref="app.tag.edit({id: tag.getId()})" class="edit">\n' +
    '                                <p class="ui-p"><i class="icon fa fa-pencil-square-o"></i></p>\n' +
    '                            </a>\n' +
    '                            <a ng-click="vm.deleteItem(tag)" class="delete">\n' +
    '                                <p class="ui-p"><i class="icon fa fa-trash"></i></p>\n' +
    '                            </a>\n' +
    '                        </td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '            <pagination data="vm.pagination"></pagination>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/acl/list/create/_create.tpl.html',
    '<div class="create-user">\n' +
    '    <div class="row">\n' +
    '        <div class="col s12">\n' +
    '            <div class="card">\n' +
    '                <div class="card-content">\n' +
    '                    <div class="row">\n' +
    '                        <div class="col s8 offset-s2">\n' +
    '                            <form name="vm.createUserForm" ng-submit="vm.createUserForm.$valid && vm.createUser()" update-text-fields novalidate class="form-create-user">\n' +
    '                                <div class="row">\n' +
    '                                    <div class="col s12">\n' +
    '                                        <md-input-container>\n' +
    '                                            <label>Role *</label>\n' +
    '                                            <md-select ng-model="vm.role" aria-label="role" name="role" required>\n' +
    '                                                <md-option ng-value="role" ng-repeat="role in vm.roles">{{ role.name }}</md-option>\n' +
    '                                            </md-select>\n' +
    '                                            <div class="errors">\n' +
    '                                                <span ng-show="vm.createUserForm.$submitted && vm.createUserForm.role.$error.required">Required</span>\n' +
    '                                            </div>\n' +
    '                                        </md-input-container>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="row">\n' +
    '                                    <div class="col s6">\n' +
    '                                        <md-input-container>\n' +
    '                                            <label class="required">First Name</label>\n' +
    '                                            <input type="text" ng-model="vm.user.first_name" name="first_name" required ng-maxlength="20">\n' +
    '                                            <div class="errors">\n' +
    '                                                <span ng-show="vm.createUserForm.$submitted && vm.createUserForm.first_name.$error.required">Required</span>\n' +
    '                                                <span ng-show="vm.createUserForm.$submitted && vm.createUserForm.first_name.$error.maxlength">The value is too long</span>\n' +
    '                                            </div>\n' +
    '                                        </md-input-container>\n' +
    '                                    </div>\n' +
    '                                    <div class="col s6">\n' +
    '                                        <md-input-container>\n' +
    '                                            <label class="required">Last Name</label>\n' +
    '                                            <input type="text" ng-model="vm.user.last_name" name="last_name" required ng-maxlength="20">\n' +
    '                                            <div class="errors">\n' +
    '                                                <span ng-show="vm.createUserForm.$submitted && vm.createUserForm.last_name.$error.required">Required</span>\n' +
    '                                                <span ng-show="vm.createUserForm.$submitted && vm.createUserForm.last_name.$error.maxlength">The value is too long</span>\n' +
    '                                            </div>\n' +
    '                                        </md-input-container>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="row">\n' +
    '                                    <div class="col s6">\n' +
    '                                        <md-input-container>\n' +
    '                                            <label class="required">Email</label>\n' +
    '                                            <input type="email" ng-model="vm.user.email" name="email" required ng-maxlength="40">\n' +
    '                                            <div class="errors">\n' +
    '                                                <span ng-show="vm.createUserForm.$submitted && vm.createUserForm.email.$error.required">Required</span>\n' +
    '                                                <span ng-show="vm.createUserForm.$submitted && vm.createUserForm.email.$error.email">Please enter a valid email</span>\n' +
    '                                                <span ng-show="vm.createUserForm.$submitted && vm.createUserForm.email.$error.maxlength">The value is too long</span>\n' +
    '                                            </div>\n' +
    '                                        </md-input-container>\n' +
    '                                    </div>\n' +
    '                                    <div class="col s6">\n' +
    '                                        <md-input-container>\n' +
    '                                            <label class="required">Password</label>\n' +
    '                                            <input type="password" ng-model="vm.user.password" name="password" required ng-maxlength="255">\n' +
    '                                            <div class="errors">\n' +
    '                                                <span ng-show="vm.createUserForm.$submitted && vm.createUserForm.password.$error.required">Required</span>\n' +
    '                                                <span ng-show="vm.createUserForm.$submitted && vm.createUserForm.password.$error.maxlength">The value is too long</span>\n' +
    '                                            </div>\n' +
    '                                        </md-input-container>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="row action-btn">\n' +
    '                                    <div class="col s12">\n' +
    '                                        <button class="btn waves-effect waves-light" ng-disabled="inProgress" ng-class="{\'disabled\': inProgress}" type="submit" name="action">create\n' +
    '                                            <i class="material-icons right">trending_flat</i>\n' +
    '                                            <i class="material-icons right icon-pending">replay</i>\n' +
    '                                        </button>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </form>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/acl/list/edit/_edit.tpl.html',
    '<div class="edit-user">\n' +
    '    <div class="row">\n' +
    '        <div class="col s12">\n' +
    '            <div class="card">\n' +
    '                <div class="card-content">\n' +
    '                    <div class="row">\n' +
    '                        <div class="col s8 offset-s2">\n' +
    '                            <form name="vm.editUserForm" ng-submit="vm.editUserForm.$valid && vm.editUser(vm.user)" update-text-fields novalidate class="form-edit-user">\n' +
    '                                <div class="row">\n' +
    '                                    <div class="col s12">\n' +
    '                                        <md-input-container>\n' +
    '                                            <label>Role *</label>\n' +
    '                                            <md-select ng-model="vm.role" aria-label="role" name="role" required>\n' +
    '                                                <md-option ng-value="role" ng-repeat="role in vm.roles">{{ role.name }}</md-option>\n' +
    '                                            </md-select>\n' +
    '                                            <div class="errors">\n' +
    '                                                <span ng-show="vm.editUserForm.$submitted && vm.editUserForm.role.$error.required">Required</span>\n' +
    '                                            </div>\n' +
    '                                        </md-input-container>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="row">\n' +
    '                                    <div class="col s6">\n' +
    '                                        <md-input-container>\n' +
    '                                            <label class="required">First Name</label>\n' +
    '                                            <input type="text" ng-model="vm.user.first_name" name="first_name" required ng-maxlength="20">\n' +
    '                                            <div class="errors">\n' +
    '                                                <span ng-show="vm.editUserForm.$submitted && vm.editUserForm.first_name.$error.required">Required</span>\n' +
    '                                                <span ng-show="vm.editUserForm.$submitted && vm.editUserForm.first_name.$error.maxlength">The value is too long</span>\n' +
    '                                            </div>\n' +
    '                                        </md-input-container>\n' +
    '                                    </div>\n' +
    '                                    <div class="col s6">\n' +
    '                                        <md-input-container>\n' +
    '                                            <label class="required">Last Name</label>\n' +
    '                                            <input type="text" ng-model="vm.user.last_name" name="last_name" required ng-maxlength="20">\n' +
    '                                            <div class="errors">\n' +
    '                                                <span ng-show="vm.editUserForm.$submitted && vm.editUserForm.last_name.$error.required">Required</span>\n' +
    '                                                <span ng-show="vm.editUserForm.$submitted && vm.editUserForm.last_name.$error.maxlength">The value is too long</span>\n' +
    '                                            </div>\n' +
    '                                        </md-input-container>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="row">\n' +
    '                                    <div class="col s12">\n' +
    '                                        <md-input-container>\n' +
    '                                            <label class="required">Email</label>\n' +
    '                                            <input type="email" ng-model="vm.user.email" name="email" required ng-maxlength="40">\n' +
    '                                            <div class="errors">\n' +
    '                                                <span ng-show="vm.editUserForm.$submitted && vm.editUserForm.email.$error.required">Required</span>\n' +
    '                                                <span ng-show="vm.editUserForm.$submitted && vm.editUserForm.email.$error.email">Please enter a valid email</span>\n' +
    '                                                <span ng-show="vm.editUserForm.$submitted && vm.editUserForm.email.$error.maxlength">The value is too long</span>\n' +
    '                                            </div>\n' +
    '                                        </md-input-container>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                                <div class="row action-btn">\n' +
    '                                    <div class="col s12">\n' +
    '                                        <button class="btn waves-effect waves-light" ng-disabled="inProgress" ng-class="{\'disabled\': inProgress}" type="submit" name="action">update\n' +
    '                                            <i class="material-icons right">trending_flat</i>\n' +
    '                                            <i class="material-icons right icon-pending">replay</i>\n' +
    '                                        </button>\n' +
    '                                    </div>\n' +
    '                                </div>\n' +
    '                            </form>\n' +
    '                        </div>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '            </div>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/payment/transactions/detail/_transaction_detail.tpl.html',
    '<div class="transaction-details">\n' +
    '    <div class="row">\n' +
    '        <div class="col s12">\n' +
    '            <p ng-bind="vm.transaction.getDescription()"></p>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row">\n' +
    '        <div class="col s12">\n' +
    '            <table class="list-transactions">\n' +
    '                <tr>\n' +
    '                    <td>Transaction ID</td>\n' +
    '                    <td ng-bind="vm.transaction.getTransactionId()"></td>\n' +
    '                </tr>\n' +
    '                <tr>\n' +
    '                    <td>Total Amount</td>\n' +
    '                    <td><span ng-bind="vm.transaction.getFormatedAmount()"></span> <span class="text-uppercase" ng-bind="vm.transaction.getCurrency()"></span></td>\n' +
    '                </tr>\n' +
    '                <tr>\n' +
    '                    <td>Service Fee</td>\n' +
    '                    <td><span ng-bind="vm.transaction.getFormatedServiceFee()"></span> <span class="text-uppercase" ng-bind="vm.transaction.getCurrency()"></span></td>\n' +
    '                </tr>\n' +
    '                <tr ng-if="vm.transaction.getFormatedRefundedAmount() > 0">\n' +
    '                    <td>Refunded</td>\n' +
    '                    <td><span ng-bind="vm.transaction.getFormatedRefundedAmount()"></span> <span class="text-uppercase" ng-bind="vm.transaction.getCurrency()"></span></td>\n' +
    '                </tr>\n' +
    '            </table>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row">\n' +
    '        <div class="col s12">\n' +
    '            <table class="striped table-history" ng-if="vm.transaction.refunds | hasItem">\n' +
    '                <thead>\n' +
    '                    <tr>\n' +
    '                        <th>Refunded ID</th>\n' +
    '                        <th>Refunded Amount</th>\n' +
    '                        <th>Currency</th>\n' +
    '                        <th>Date</th>\n' +
    '                    </tr>\n' +
    '                </thead>\n' +
    '                <tbody>\n' +
    '                    <tr ng-repeat="transaction in vm.transaction.refunds">\n' +
    '                        <td ng-bind="transaction.getTransactionId()"></td>\n' +
    '                        <td ng-bind="transaction.getFormatedAmount()"></td>\n' +
    '                        <td ng-bind="transaction.getCurrency()"></td>\n' +
    '                        <td ng-bind="transaction.getCreatedAt() | timeFormat:\'llll\'"></td>\n' +
    '                    </tr>\n' +
    '                </tbody>\n' +
    '            </table>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row" ng-if="vm.transaction.getFormatedAvailableRefundAmount() > 0">\n' +
    '        <div class="col s12">\n' +
    '            <a class="ui-btn btn-small btn-orange" ng-click="vm.toggleRefundForm = !vm.toggleRefundForm">\n' +
    '                <span ng-if="!vm.toggleRefundForm">Refund</span>\n' +
    '                <span ng-if="vm.toggleRefundForm">Hide</span>\n' +
    '            </a>\n' +
    '        </div>\n' +
    '        <div class="col s12 refund-form">\n' +
    '            <form ng-if="vm.toggleRefundForm" name="vm.refund_form" ng-submit="vm.transaction.getFormatedAvailableRefundAmount() >= vm.amount && vm.refund_form.$valid && vm.refund(vm.transaction, vm.amount)" novalidate>\n' +
    '                <div class="form-group">\n' +
    '                    <label>Amount in SGD * (Ex: 100.5)</label>\n' +
    '                    <input type="text" class="form-control" name="amount" ng-model="vm.amount" ng-pattern="/^\\d{1,8}$|^\\d{1,8}\\.\\d{1,2}$/" required>\n' +
    '                    <div class="errors">\n' +
    '                        <span ng-show="vm.refund_form.$submitted && vm.refund_form.amount.$error.required">Required</span>\n' +
    '                        <span ng-show="vm.refund_form.$submitted && vm.refund_form.amount.$error.pattern">Please enter a valid amount</span>\n' +
    '                        <span ng-show="vm.refund_form.$submitted && vm.transaction.getFormatedAvailableRefundAmount() < vm.amount">Can\'t refund with amount more than {{vm.transaction.getFormatedAvailableRefundAmount()}}</span>\n' +
    '                    </div>\n' +
    '                </div>\n' +
    '                <input type="submit" class="ui-btn btn-small btn-orange" value="Submit">\n' +
    '            </form>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/payment/transactions/list/_transaction_list.tpl.html',
    '<div class="payment-history">\n' +
    '    <table class="striped table-history">\n' +
    '        <thead>\n' +
    '            <tr>\n' +
    '                <th>ID</th>\n' +
    '                <th>Total Amount</th>\n' +
    '                <th>Service Fee</th>\n' +
    '                <th>Currency</th>\n' +
    '                <th>Description</th>\n' +
    '                <th>Customer</th>\n' +
    '                <th>Date</th>\n' +
    '                <th>Status</th>\n' +
    '                <th>Refunded Amount</th>\n' +
    '                <th class="actions-col center-align" ng-if="vm.user.can(\'update.transactions\')">Action</th>\n' +
    '            </tr>\n' +
    '        </thead>\n' +
    '        <tbody>\n' +
    '            <tr ng-repeat="transaction in vm.transactions">\n' +
    '                <td ng-bind="transaction.getId()"></td>\n' +
    '                <td ng-bind="transaction.getFormatedAmount()"></td>\n' +
    '                <td ng-bind="transaction.getFormatedServiceFee()"></td>\n' +
    '                <td ng-bind="transaction.getCurrency()"></td>\n' +
    '                <td ng-bind="transaction.getDescription()"></td>\n' +
    '                <td>\n' +
    '                    <a target="_blank" ui-sref="app.companies.edit({id: transaction.customer.getId()})" ng-bind="transaction.customer.getEmail()"></a>\n' +
    '                </td>\n' +
    '                <td ng-bind="transaction.getCreatedAt() | timeFormat:\'llll\'"></td>\n' +
    '                <td class="center-align">\n' +
    '                    <span ng-if="transaction.isRefunded()">Refunded</span>\n' +
    '                </td>\n' +
    '                <td ng-bind="transaction.getFormatedRefundedAmount()"></td>\n' +
    '                <td class="actions-col center-align" ng-if="vm.user.can(\'update.transactions\')">\n' +
    '                    <button ng-click="vm.refund(transaction)" class="ui-btn btn-small btn-white" ng-disabled="transaction.isRefunded()">Refund</button>\n' +
    '                    <a ui-sref="app.payment.transactions.detail({\'id\':transaction.getId()})">More</a>\n' +
    '                </td>\n' +
    '            </tr>\n' +
    '        </tbody>\n' +
    '    </table>\n' +
    '    <div class="right-align">\n' +
    '        <pagination data="vm.pagination"></pagination>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/payment/transfers/detail/_transaction_detail.tpl.html',
    '<div class="transaction-details">\n' +
    '    <div class="row">\n' +
    '        <div class="col s12">\n' +
    '            <p ng-bind="vm.transaction.getDescription()"></p>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '    <div class="row">\n' +
    '        <div class="col s12">\n' +
    '            <table>\n' +
    '                <tr>\n' +
    '                    <td>Transaction ID</td>\n' +
    '                    <td ng-bind="vm.transaction.getTransactionId()"></td>\n' +
    '                </tr>\n' +
    '                <tr>\n' +
    '                    <td>Amount</td>\n' +
    '                    <td ng-bind="vm.transaction.getFormatedAmount()"></td>\n' +
    '                </tr>\n' +
    '                <tr>\n' +
    '                    <td>Currency</td>\n' +
    '                    <td ng-bind="vm.transaction.getCurrency()"></td>\n' +
    '                </tr>\n' +
    '                <tr>\n' +
    '                    <td>Candidate</td>\n' +
    '                    <td>\n' +
    '                        <a ui-sref="app.employees.edit({id: vm.transaction.customer.getId()})" ng-bind="vm.transaction.customer.email"></a>\n' +
    '                    </td>\n' +
    '                </tr>\n' +
    '            </table>\n' +
    '        </div>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();

(function(module) {
try {
  module = angular.module('app.template');
} catch (e) {
  module = angular.module('app.template', []);
}
module.run(['$templateCache', function($templateCache) {
  $templateCache.put('/webapp/js/admin/components/payment/transfers/list/_transfers_list.tpl.html',
    '<div class="payment-history">\n' +
    '    <table class="striped table-history">\n' +
    '        <thead>\n' +
    '            <tr>\n' +
    '                <th></th>\n' +
    '                <th>Amount</th>\n' +
    '                <th>Currency</th>\n' +
    '                <th>Description</th>\n' +
    '                <th>Employee</th>\n' +
    '                <th>Date</th>\n' +
    '                <th class="actions-col right-align">Action</th>\n' +
    '            </tr>\n' +
    '        </thead>\n' +
    '        <tbody>\n' +
    '            <tr ng-repeat="transaction in vm.transactions">\n' +
    '                <td></td>\n' +
    '                <td ng-bind="transaction.getFormatedAmount()"></td>\n' +
    '                <td ng-bind="transaction.getCurrency()"></td>\n' +
    '                <td ng-bind="transaction.getDescription()"></td>\n' +
    '                <td>\n' +
    '                    <a target="_blank" ui-sref="app.employees.edit({id: transaction.customer.getId()})" ng-bind="transaction.customer.getEmail()"></a>\n' +
    '                </td>\n' +
    '                <td ng-bind="transaction.getCreatedAt() | timeFormat:\'llll\'"></td>\n' +
    '                <td class="actions-col center-align" ng-if="vm.user.can(\'update.transactions\')">\n' +
    '                    <a ui-sref="app.payment.transfers.detail({\'id\':transaction.getId()})">More</a>\n' +
    '                </td>\n' +
    '            </tr>\n' +
    '        </tbody>\n' +
    '    </table>\n' +
    '    <div class="right-align">\n' +
    '        <pagination data="vm.pagination"></pagination>\n' +
    '    </div>\n' +
    '</div>\n' +
    '');
}]);
})();
