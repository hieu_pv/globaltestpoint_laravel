var gulp = require('gulp');
var watch = require('gulp-watch');
module.exports = function() {
    watch('./resources/assets/js/**/*.js', function() {
        gulp.run([
            'jshint',
            'minify_js'
        ]);
    });
     watch('./resources/assets/angular/**/*.js', function() {
        gulp.run([
            'jshint',
            'browserify'
        ]);
    });

    gulp.watch('./resources/assets/sass/frontend/**/*.scss', ['sass']);
    gulp.watch('./resources/assets/sass/backend/**/*.scss', ['sass_backend']);
}