var gulp = require('gulp');
var minify = require('gulp-minify');
var rename = require('gulp-rename');
var concat = require("gulp-concat");
module.exports = function() {
    gulp.src([
            './public/bower_components/jquery/dist/jquery.min.js',
            './public/bower_components/bootstrap/dist/js/bootstrap.min.js',
            './public/bower_components/bootstrap-fileinput/js/fileinput.min.js',
            './public/bower_components/datatables/media/js/jquery.dataTables.min.js',
            './public/bower_components/datatables/media/js/dataTables.bootstrap.min.js',
            './public/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
            './public/bower_components/select2/dist/js/select2.full.js',
            './public/bower_components/iCheck/icheck.min.js',
            './public/bower_components/lodash/dist/lodash.js',
            './public/bower_components/owl-carousel/owl-carousel/owl.carousel.js',
            './public/assets/js/countdown.js',
            './resources/assets/js/**/*.js',
        ])
        .pipe(concat("app.js"))
        .pipe(gulp.dest('./public/assets/js/'))
}