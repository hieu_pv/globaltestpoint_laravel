var gulp = require('gulp');
var sass = require('gulp-sass');
var sassGlob = require('gulp-sass-glob');
module.exports = function() {
    return gulp.src('./resources/assets/sass/frontend/app.scss')
        .pipe(sassGlob())
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./public/assets/css'));
}