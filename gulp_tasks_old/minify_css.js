var gulp = require('gulp');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var concat = require("gulp-concat");

module.exports = function() {
    return gulp.src([
            './public/bower_components/bootstrap/dist/css/bootstrap.min.css',
            './public/bower_components/bootstrap/dist/css/bootstrap-theme.min.css',
            './public/bower_components/font-awesome/css/font-awesome.min.css',
            './public/bower_components/datatables/media/css/dataTables.bootstrap.min.css',
            './public/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css',
            './public/bower_components/bootstrap-fileinput/css/fileinput.css',
            './public/bower_components/angular-dialog-service/dist/dialogs.css',
            './public/bower_components/ng-notify/dist/ng-notify.min.css',
            './public/bower_components/select2/dist/css/select2.css',
            './public/bower_components/owl-carousel/owl-carousel/owl.carousel.css',
            './public/bower_components/owl-carousel/owl-carousel/owl.theme.css',
            './public/bower_components/owl-carousel/owl-carousel/owl.transitions.css',
        ])
        .pipe(cssmin())
        .pipe(rename({ suffix: '.min' }))
        .pipe(concat("libraries.min.css"))
        .pipe(gulp.dest('public/assets/css'));
};